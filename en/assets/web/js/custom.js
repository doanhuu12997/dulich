$(document).ready(function () {
	  $(window).scroll(function () {

    let scroll_top = $(document).scrollTop();
    if (scroll_top > 132) {
      $("#bttop").addClass("active");
    } else {
      $("#bttop").removeClass("active");
    }
  });
	$(".input-search-tour").click(function (e) {
		
		$(".advance-search").removeClass("hidden-cls");
		$(".mark-close").removeClass("hidden-cls");
	});
	$(".mark-close").click(function (e) {
		
		$(".advance-search").addClass("hidden-cls");
		$(".mark-close").addClass("hidden-cls");
	});
	$("li.formContact a").click(function (e) {
		e.preventDefault();
		$(".contentForm").removeClass("hidden-cls");
		$(".mark-closeheader").removeClass("hidden-cls");
	});

$(".mark-closeheader").click(function (e) {
		
		$(".contentForm").addClass("hidden-cls");
		$(".mark-closeheader").addClass("hidden-cls");
	});
	$('.tour1__wrapper').slick({

        slidesToShow:3,

        slidesToScroll: 1,

        autoplay: true,

        autoplaySpeed: 5000,

        speed:1000,

        arrows: true,

        responsive: [

            {

                breakpoint: 1024,

                settings: {

                    slidesToShow: 2,

                    slidesToScroll: 1

                }

            },

            {

                breakpoint: 850,

                settings: {

                    slidesToShow: 2,

                    slidesToScroll: 1

                }

            },

            {

                breakpoint: 768,

                settings: {

                    slidesToShow: 2,

                    slidesToScroll: 1

                }

            },

            {

                breakpoint: 500,

                settings: {

                    slidesToShow: 1,

                    slidesToScroll: 1

                }

            },

            {

                breakpoint: 415,

                settings: {

                    slidesToShow: 1,

                    slidesToScroll: 1

                }

            },

        ]

    });
$('.tour2__wrapper').slick({

        slidesToShow:3,

        slidesToScroll: 1,

        autoplay: true,

        autoplaySpeed: 5000,

        speed:1000,

        arrows: true,

        responsive: [

            {

                breakpoint: 1024,

                settings: {

                    slidesToShow: 2,

                    slidesToScroll: 1

                }

            },

            {

                breakpoint: 850,

                settings: {

                    slidesToShow: 2,

                    slidesToScroll: 1

                }

            },

            {

                breakpoint: 768,

                settings: {

                    slidesToShow: 2,

                    slidesToScroll: 1

                }

            },

            {

                breakpoint: 500,

                settings: {

                    slidesToShow: 1,

                    slidesToScroll: 1

                }

            },

            {

                breakpoint: 415,

                settings: {

                    slidesToShow: 1,

                    slidesToScroll: 1

                }

            },

        ]

    });
});