<link href="<?= base_url() ?>assets/web/css/main.css" rel="stylesheet" type="text/css" />

<div class="result"><?= $this->session->flashdata('message'); ?></div>

<div class="wap_slider vnt-banner">

    <?php foreach ($get_slider as $slider) { ?>

        <div class="item">

            <div class="img"><a href=""><img src="<?= base_url() ?>uploads/<?= $slider->slider_image; ?>" alt="<?= $slider->slider_title; ?>" title="<?= $slider->slider_title; ?>"></a>

                <div class="item__box">

                    <div class="container">

                        <h2><?= $slider->slider_tit; ?></h2>

                        <p> <?= $slider->slider_des; ?></p>

                        <a href="<?= $slider->slider_link; ?>" class="buttonCustomPrimary"> <?= $slider->button_text; ?></a>

                    </div>

                </div>

            </div>

        </div>

    <?php } ?>

</div>

<!--End wap_slider-->



<div class="container">

    <?php $this->load->view('web/inc/search_tour.php'); ?>

</div>

<div class="container">

    <div class="row">

        <div class="col-lg-12">



            <section class="tour1 hometour">





                <?php $tour_by_id = $this->web_model->get_product_cate(512);

                if (empty($tour_by_id)) { ?>

                <?php  } else { ?>
                    <h2 class="title">FOR CAMBODIA MARKET</h2>
                    <div class="tour1__wrapper">
                        <?php foreach ($tour_by_id as $t) { ?>

                            <div class="item">

                                <article>

                                    <div class="img">

                                        <img src="<?= base_url() ?>uploads/product/thumb/<?= $t->product_image; ?>" alt="<?= $t->product_title; ?>" title="<?= $t->product_title; ?>">

                                    </div>

                                    <div class="content">

                                        <h3><a href="<?= base_url($t->product_slug) . '.html'; ?>">

                                                <?= $t->product_title; ?></a></h3>

                                        <div class="details"> <a href="<?= base_url($t->product_slug) . '.html'; ?>" class="buttonCustomPrimary">Detail</a></div>

                                    </div>

                                    <div class="date">

                                        <p><?= $t->product_thoigian; ?></p>

                                    </div>

                                </article>

                            </div>

                        <?php }
                        ?>
                    </div>
                <?php } ?>



            </section>

            <section class="tour2 hometour">

                <?php $tour_by_id = $this->web_model->get_product_cate(513);

                if (empty($tour_by_id)) { ?>

                <?php  } else { ?>
                    <h2 class="title">FOR CAMBODIA MARKET</h2>
                    <div class="tour2__wrapper">
                        <?php foreach ($tour_by_id as $t) { ?>

                            <div class="item">

                                <article>

                                    <div class="img">

                                        <img src="<?= base_url() ?>uploads/product/thumb/<?= $t->product_image; ?>" alt="<?= $t->product_title; ?>" title="<?= $t->product_title; ?>">

                                    </div>

                                    <div class="content">

                                        <h3><a href="<?= base_url($t->product_slug) . '.html'; ?>">

                                                <?= $t->product_title; ?></a></h3>

                                        <div class="details"> <a href="<?= base_url($t->product_slug) . '.html'; ?>" class="buttonCustomPrimary">Detail</a></div>

                                    </div>

                                    <div class="date">

                                        <p><?= $t->product_thoigian; ?></p>

                                    </div>

                                </article>

                            </div>

                        <?php }
                        ?>
                    </div>
                <?php } ?>


            </section>



        </div>





    </div>

    <!--End row-->

</div>

<!--End container-->



<section class="destinations">

    <div class="container">

        <div class="destinations__title">

            <h2 class="title">Top destinations <i class="icon-hot"></i></h2>

        </div>

        <div class="row destinations__list ">

            <?php $destinations = $this->web_model->news_by_cat(93);





            foreach ($destinations as $des) { ?>

                <div class=" col-md-4 col-sm-12 col-xs-12">

                    <div class="destination__item">

                        <article>

                            <div class="destination__item-img">

                                <a href="" class=""><img src="<?= base_url('uploads/news/' . $des->dmtin_image) ?>" alt="">



                                </a>

                                <div class="destination__item-cat">

                                    <div class="text">Destination</div>

                                    <div class="cat"><?= $des->category_name; ?> </div>

                                </div>

                            </div>

                            <div class="destination__item-content">

                                <div class="detail-title">

                                    <a class="detail-title-text" href="<?= base_url($des->slug) . '.html'; ?>">

                                        New attractions



                                    </a>

                                </div>

                                <h3><a href="<?= base_url($des->dmtin_slug) . '.html'; ?>" class="detail-name-text"><?= $des->dmtin_title; ?></a></h3>

                            </div>



                        </article>

                    </div>

                </div>

            <?php } ?>

        </div>

    </div>



</section>



<section class="whiteSection">

    <div class="container">

        <div class="row">

            <div class="col-md-3 col-sm-6 col-xs-12">

                <div class="commitment-box">

                    <div class="commitment-image">

                        <img width="60px" height="60px" class="icon-commitment" src="<?= base_url() ?>assets/web/images/1234.svg" alt="image commitment">

                    </div>

                    <div class="commitment-content">

                        <span class="commitment-taitle">500+ Tours</span>

                        <p class="commitment-description">Over 500 tours across Vietnam.</p>

                    </div>

                </div>

            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">

                <div class="commitment-box">

                    <div class="commitment-image">

                        <img width="60px" height="60px" class="icon-commitment" src="<?= base_url() ?>assets/web/images/home1.svg" alt="image commitment">

                    </div>

                    <div class="commitment-content">

                        <span class="commitment-taitle">Best Prices</span>

                        <p class="commitment-description">Give the best rate for our land tour in Vietnam

                        </p>

                    </div>

                </div>

            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">

                <div class="commitment-box">

                    <div class="commitment-image">

                        <img width="60px" height="60px" class="icon-commitment" src="<?= base_url() ?>assets/web/images/home2.svg" alt="image commitment">

                    </div>

                    <div class="commitment-content">

                        <span class="commitment-taitle">Security</span>

                        <p class="commitment-description">All your information will be security </p>

                    </div>

                </div>

            </div>

            <div class="col-md-3 col-sm-6 col-xs-12">

                <div class="commitment-box">

                    <div class="commitment-image">

                        <img width="60px" height="60px" class="icon-commitment" src="<?= base_url() ?>assets/web/images/home3.svg" alt="image commitment">

                    </div>

                    <div class="commitment-content">

                        <span class="commitment-taitle">10 Years+ Experiences</span>

                        <p class="commitment-description">More than 10 years in celebrate land tours in Vienam</p>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section>

<section class="garely">

    <div class="">

        <div class="title-garely">

            <h2>Gallery</h2>

        </div>

        <div class="gallery_wrap">

            <?php

            foreach ($gallerys as $g) { ?>

                <div class="item">

                    <div class="img">

                        <img src="<?= base_url() ?>uploads/<?= $g->one_news_image; ?>" alt="">

                    </div>

                </div>

            <?php } ?>

        </div>

    </div>

</section>



<!-- <script>

    // set a cookie on the load event, which expires after a year

    window.onload = function() {

        var expiryTime = new Date();

        expiryTime.setFullYear(expiryTime.getFullYear() + 1);

        document.cookie = "visited=true; expires=" + expiryTime.toUTCString() + ";";

        console.log(document.cookie);

    }



    // function to hide the modal div



    window.hideModal = function() {

        document.getElementById("modal").style.display = "none";

    }



    // read the cookie and hide the modal div if the value of visitedKey is true



    function inspectCookie() {

        var visitedKey = "visited";

        var ca = document.cookie.split(';');

        for (var i = 0; i < ca.length; i++) {

            var c = ca[i];

            while (c.charAt(0) == ' ') c = c.substring(1, c.length);

            if (c.indexOf(visitedKey) !== -1) {

                window.hideModal()

            }

        }

    }



    // run the cookie checking function as soon as the parser hits it - rather than waiting for the whole page to render then hiding the modal div



    inspectCookie();

</script> -->