

    <!--===MODULE MAIN==-->
    <link href="<?php echo base_url() ?>assets/web/css/cart.css" rel="stylesheet" type="text/css" />
    <!--===MODULE MAIN==-->  

  <!--=== BEGIN: CONTENT ===-->
        <div id="vnt-content">
   

            <!--===BEGIN: BREADCRUMB===-->
            <div id="vnt-navation" class="breadcrumb" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <div class="wrapper">
                    <div class="navation">
                        <ul>
                            <li><a href="">Trang chủ</a></li>
                            <li><a href="">Thanh toán</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--===END: BREADCRUMB===-->
            <div class="wrapper">
                <div class="mod-content">
                                    <p><?php echo $this->session->flashdata('message'); ?></p>

                   <form method="post" action="<?php echo base_url('customer/save/shipping/address');?>">
                <table class="col-md-12 col-xs-12 form-address">
                    <tbody>
                        <tr>
                            <td class="col-md-6 col-xs-12">
                               <div class="form-group col-md-6 col-xs-12">
                                    <input class="form-control" type="text" name="shipping_name" placeholder="Họ và tên">
                                </div>

                                 <div class="form-group col-md-6 col-xs-12">                       
                                   <input class="form-control" type="text" name="shipping_phone" placeholder="Số điện thoại">
                                </div>

                                 <div class="form-group col-md-12 col-xs-12">
                                    <input class="form-control" type="text" name="shipping_content" placeholder="Yêu cầu khác (không bắt buộc)">
                                </div>

                              
                                <div class="form-group col-md-6 col-xs-12">
                                    <select id="city" name="shipping_city" class="frm-field required form-control">
                                        <option value="null">Thành phố</option>         
                                        <option value="TP.Hồ Chí Minh">TP.Hồ Chí Minh</option>
                                        <option value="Hà Nội">Hà Nội</option>
                                        <option value="Đà Nẵng">Đà Nẵng</option>
                                        <option value="Cần Thơ">Cần Thơ</option>
                                        <option value="Bình Dương">Bình Dương</option>
                                        <option value="Bà Rịa - Vũng Tàu">Bà Rịa - Vũng Tàu</option>
                                    </select>
                                </div>    

                                <div class="form-group col-md-6 col-xs-12">
                                    <select id="country" name="shipping_country" class="frm-field required form-control">
                                        <option value="null">Quận huyện</option>         
                                        <option value="Quận 1">Quận 1</option>
                                        <option value="Quận 2">Quận 2</option>
                                        <option value="Quận 3">Quận 3</option>
                                        <option value="Quận 4">Quận 4</option>
                                        <option value="Quận 5">Quận 5</option>
                                        <option value="Quận 6">Quận 6</option>
                                        <option value="Quận 7">Quận 7</option>
                                        <option value="Quận 8">Quận 8</option>
                                        <option value="Quận 9">Quận 9</option>
                                        <option value="Quận 10">Quận 10</option>
                                        <option value="Quận 11">Quận 11</option>
                                        <option value="Quận 12">Quận 12</option>
                                        <option value="Quận Tân Bình">Quận Tân Bình</option>
                                        <option value="Quận Tân Phú">Quận Tân Phú</option>
                                        <option value="Quận Phú Nhuận">Quận Phú Nhuận</option>
                                        <option value="Quận Gò Vấp">Quận Gò Vấp</option>
                                        <option value="Quận Bình Thạnh">Quận Bình Thạnh</option>
                                        <option value="Quận Thủ Đức">Quận Thủ Đức</option>
                                        <option value="Quận Bình Tân">Quận Bình Tân</option>
                                        <option value="Huyện Hóc Môn">Huyện Hóc Môn</option>
                                        <option value="Huyện Củ Chi">Huyện Củ Chi</option>
                                        <option value="Huyện Nhà Bè">Huyện Nhà Bè</option>
                                        <option value="Huyện Bình Chánh">Huyện Bình Chánh</option>
                                        <option value="Huyện Cần Giờ">Huyện Cần Giờ</option>
                                    </select>
                                </div>     

                                 <div class="form-group col-md-12 col-xs-12">
                                    <input class="form-control" type="text" name="shipping_email" placeholder="Email">
                                </div>
                                <div class="form-group col-md-12 col-xs-12">
                                    <input class="form-control" type="text" name="shipping_address" placeholder="Nhập phường ,Số nhà ,tên đường">
                                </div>

                                
                            </td>
                           <td class="col-md-6 col-xs-12"></td>
                         
                        </tr> 
                    </tbody></table> 
                <div class="search"><div><button  class="btn btn-primary">Thanh toán</button></div></div>
                <div class="clear"></div>
            </form>



 
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <!--=== END: CONTENT ===-->












