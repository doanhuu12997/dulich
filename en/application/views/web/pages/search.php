<link href="<?= base_url() ?>assets/web/css/tour_cate.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?= base_url() ?>assets/web/css/box.css">
<!--=== BEGIN: CONTENT ===-->
<div class="imageTour" style="background-image:url(<?= base_url() ?>assets/web/images/timkiem.jpg)">
    <div class="box__tour">
        <div class="container">
            <div class="box__tour-wrap">
                <h1 class="tour-banner-title">Search results: <?= $search; ?> </h1>
            </div>
        </div>
    </div>
</div>
<div id="vnt-content">
    <div class="container ">
        <div class="mod-content ">
            <!--===BEGIN: BOX MAIN===-->
            <div class="box_mid">
                <div class="mid-content">
                    <div class="main-content ">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="tab-tours">
                                    <div class="tab-content shadow" id="nav-tabContent">
                                        <div class="tab-pane  show active" id="nav-248" role="tabpanel" aria-labelledby="nav-248-tab">
                                            <ul class="list-tour-01">
                                                <?php foreach ($get_all_product as $product) {
                                                    $rs_dxp = $this->web_model->get_xuatphat($product->product_xuatphat);

                                                ?>
                                                    <li>
                                                        <div class="item row">
                                                            <div class="item-left col-md-4">
                                                                <a class="hvr-bounce-to-right shadow" href="<?= base_url($product->product_slug) . '.html'; ?>">
                                                                    <img src="<?= base_url('uploads/product/thumb/' . $product->product_image) ?>" alt="<?= $product->product_title; ?>" title="<?= $product->product_title; ?>" />
                                                                </a>
                                                            </div>
                                                            <div class="item-right col-md-8">
                                                                <div class="row">
                                                                    <div class="col-md-8">
                                                                        <h3 class="title">
                                                                            <a href="<?= base_url($product->product_slug) . '.html'; ?>"><?= $product->product_title; ?></a>
                                                                        </h3>
                                                                        <ul class="meta">
                                                                            <li>
                                                                                <i class="fa fa-clock-o"></i>Time:
                                                                                <?= $product->product_thoigian; ?>
                                                                            </li>

                                                                            <li> <i class="fa fa-plane"></i>Transport:
                                                                                <?= $product->product_phuongtien; ?> </li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <a href="<?= base_url($product->product_slug) . '.html'; ?>" class="btn btn-view-detail"> Detail </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                        <!--EnD tab-pane-->
                                    </div>
                                    <!--End tab-content-->
                                </div>
                                <!--End tab-tours-->
                                <div class="clear"></div>
                            </div>
                            <!--End md8-->
                            <div class="col-lg-4 siderbar">
                                <div class="widget news">
                                    <div class="widget-title">
                                        <h4>News</h4>
                                    </div>
                                    <div class="widget-content shadow">
                                        <?php
                                        $id_dm_news = 86;
                                        $limit_news = 5;
                                        $order_news = "asc";
                                        $get_news_right = $this->web_model->get_news_top_home($id_dm_news, $limit_news, $order_news);
                                        foreach ($get_news_right as $news) { ?>
                                            <div class="single-blog-post d-flex">
                                                <div class="post-thumbnail shadow"> <a href="<?= base_url($news->dmtin_slug) . '.html'; ?>">
                                                        <img src="<?= base_url('uploads/news/' . $news->dmtin_image) ?>" alt="<?= $news->dmtin_title ?>" title="<?= $news->dmtin_title ?>"></a> </div>
                                                <div class="post-content">
                                                    <h3> <a href="<?= base_url($news->dmtin_slug) . '.html'; ?>" class="post-title"><?= $news->dmtin_title ?></a></h3>
                                                    <div class="single-blog-bottom"> <a href="<?= base_url($news->dmtin_slug) . '.html'; ?>" class="btn btn-view-detail"> Xem chi tiết </a> </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <!--End single-blog-post-->
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <!--End md-4-->
                            </div>
                        </div>
                        <!--End main-content-->
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
        <!--===END: BOX MAIN===-->
    </div>
    <div class="clear"></div>
</div>
</div>
<!--=== END: CONTENT ===-->