<link href="<?php echo base_url() ?>assets/web/css/contact.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url() ?>assets/web/js/contact/contact.js"></script>
<!--=== BEGIN: CONTENT ===-->
<div id="vnt-content">
    <div id="result">
        <?php echo $this->session->flashdata('message'); ?>
    </div>
    <!--===BEGIN: BREADCRUMB===-->
    <div id="vnt-navation" class="breadcrumb">
        <div class="container">
            <div class="navation">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li>Liên hệ</li>
                </ul>
            </div>
        </div>
    </div>
    <!--===END: BREADCRUMB===-->
    <div class="container-fluid">
        <!--===BEGIN: BOX MAIN===-->
        <div class="box_mid">
            <div class="mid-title">
                <div class="titleR"></div>
                <div class="clear"></div>
            </div>
            <div class="mid-content">
                <div class="map">
                    <div class="contant_main container">
                        <?php echo get_option('company_location'); ?>
                        <div class="row">
                            <!--===BEGIN: FORM LIÊN HỆ==-->
                            <div class="formContact col-md-12 col-xs-12 col-sm-12">
                                <h3 class="title_contact">Leave a message for us</h3>

                                <div class="row">
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <form id="formContact" name="formContact" method="POST" action="<?php echo base_url('save_contact.html'); ?>" class="form validate">
                                            <div class="row-form col-md-6 col-xs-12 col-sm-6 ">
                                                <div class="rf_left">
                                                    <label for="name">Your Name <span>(*)</span></label>
                                                </div>
                                                <div class="rf_right">
                                                    <input type="text" name="contact_name" required id="contact_name" class="form-control" />
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="row-form col-md-6 col-xs-12 col-sm-6 ">
                                                <div class="rf_left">
                                                    <label for="email">Your Email <span>(*)</span></label>
                                                </div>
                                                <div class="rf_right">
                                                    <input type="email" name="contact_email" required class="form-control" />
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="row-form col-md-12 col-xs-12 col-sm-12 ">
                                                <div class="rf_left">
                                                    <label for="phone">Your Phone <span>(*)</span></label>
                                                </div>
                                                <div class="rf_right">
                                                    <input type="text" name="contact_phone" required class="form-control" />
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="row-form col-md-12 col-xs-12 col-sm-12 ">
                                                <div class="rf_left">
                                                    <label for="f-content">Content <span>(*)</span></label>
                                                </div>
                                                <div class="rf_right">
                                                    <textarea class="form-control" id="contact_content" required name="contact_content" rows="4"></textarea>
                                                </div>
                                                <div class="clear"></div>
                                            </div>

                                            <div class="row-form c-button col-md-12 col-xs-12 col-sm-12">
                                                <div class="rf_right">
                                                    <div class="fl">
                                                        <button id="b-submit" name="submit" type="submit" class="btn btnsubmit" value="Gửi"><span>Send us</span></button>
                                                    </div>

                                                    <div class="clear"></div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-6 col-xs-12 col-sm-12"><?php echo get_option('contact_description'); ?></div>
                                </div>
                                <div class="clear"></div>

                            </div>
                            <!--===END: FORM LIÊN HỆ==-->
                            <!--===BEGIN: THÔNG TIN LIÊN HỆ==-->
                            <div class="col-md-2 col-xs-12 col-sm-2">
                            </div>
                            <!--===END: THÔNG TIN LIÊN HỆ==-->
                        </div>
                    </div>

                </div><!--end map-->


                <div class="clear"></div>

            </div>
        </div>
        <!--===END: BOX MAIN===-->
    </div>
</div>
<style type="text/css">
    .breadcrumb ul li:first-child {}

    .breadcrumb ul li:last-child {
        position: relative;
        font-size: 14px;
    }

    .breadcrumb ul li~li:before {
        left: -12px;
        position: relative;
        top: 0px;
    }
</style>