


<link href="<?php echo base_url() ?>assets/web/css/product.css" rel="stylesheet" type="text/css" />


 <!--=== BEGIN: CONTENT ===-->
        <div id="vnt-content" class="container">
           <!--===BEGIN: BREADCRUMB===-->
            <div id="vnt-navation" class="breadcrumb">
                    <div class="navation">
                        <ul class="i_breadcrumb">
                            <li><a href="/">Trang chủ</a></li>
                            <li><a href="<?php echo base_url('tour-dang-hot.html') ?>">Tour đang hot </a></li>
                        </ul>
                    </div>
            </div>
            <!--===END: BREADCRUMB===-->

           
            <div class="clear"></div>
                <div class="mod-content">
                                                <span class="tittle_h1" style="padding-left: 10px;"><h1>Tour đang hot</h1></span> 
                                                <div class="clear"></div>

                        <!--===BEGIN: BOX MAIN===-->
                        <div class="box_mid row">
                            <div class="mid-content">
                                 <div class="gird_tour">
  <!--=== BEGIN: PRODUCT ===-->
            <div class="vnt-product">
                    <div class="prod-content">
                        <div class="grid-prod">
                      <div class="w_product w_thuonghieu">
            <?php foreach ($tour_hot as $product) {
               $get_daban= $this->web_model->get_daban($product->product_id); ?>
              <div class="item col-md-4 col-xs-12 col-sm-4">
                
                <div class="wrap-item">
                  <div class="i-top">
                    <div class="i-image">
                       <?php if(empty($product->product_donggoi)){ echo '';} else{?>            
                      <span class="product-promo"><?=$product->product_donggoi; ?></span>
                      <?php }?>
                      <?php if(floatValue($product->product_price_sale)==0) {echo "";} 
                        else {?>
                          <div class="i-saleoff">
                            <?php if(floatValue($product->product_price)==0 || floatValue($product->product_price_sale)==0){ ?>
                              <?php echo  ""; ?>
                            <?php } else { ?>
                              <?php $percent = (floatValue($product->product_price) - floatValue($product->product_price_sale)) /floatValue($product->product_price) *100; ?>
                              <?php $rs_percent=number_format($percent);
                              echo - round($rs_percent); ?>% 
                            <?php  }?>
                          </div>
                        <?php }?>
                     
                        <?php if(floatValue($product->product_price_sale)==0) {echo "";} else {?>
                      <label class="discount">GIẢM  <?php $price_sale = (floatValue($product->product_price) - floatValue($product->product_price_sale)); echo number_format($price_sale) .'₫';  ?></label> <?php }?>

                      <a href="<?=base_url($product->product_slug).'.html';?>">
                        <img src="<?=base_url('uploads/product/thumb/'.$product->product_image)?>" alt="<?=$product->product_title ?>" title="<?=$product->product_title ?>"/>
                      </a>
                    </div>
                    <div class="clear"></div>
                  </div>
                  <div class="i-desc">
                    <div class="desc-left">
                      <div class="i-title">
                        <a href="<?=base_url($product->product_slug).'.html';?>">
                          <h4><?=word_limiter($product->product_title,10) ?></h4>
                        </a>
                      </div> 
                   <p class="info-date"><i class="fa fa-clock-o"></i> Thời gian: <?=$product->product_thoigian;?></p>

             <?php $get_depart_row = $this->web_model->get_depart_row($product->product_id);?>
            <?php if(isset($get_depart_row->product_id) && !empty($get_depart_row->price_child) && !empty($get_depart_row->price_baby)){ ?>
                 <p class="info-date"><i class="fa fa-calendar"></i> Khởi hành: <?=nice_date($get_depart_row->date_begin,'d-m-Y');?></p>

            <?php } else {?>
                    <p class="not_calendar"> Chưa có lịch</p>
            <?php }?>

                      <div class="clear"></div>
                       <span class="i-rating">   Đã đặt <?=$get_daban->daban;?> </span>
                        <div class="clear"></div>
                      <div class="i-promotion">
                        <?php if(floatValue($product->product_price_sale)==0) { echo "";} else {?>
                          <span><?php echo $product->product_price_sale; ?></span>
                          <span>₫</span> 
                        <?php }?>
                        <?php if(floatValue($product->product_price)==0){echo "<span class='call_me'>Liên hệ </span>";} else if(floatValue($product->product_price_sale) ==0){echo ' <span>'. $product->product_price .' ₫' .'</span>';}else{ echo '<div class=i-price>' .  $product->product_price .'₫' ; ?></div>
                      <?php } ?>
                    </div><!--end i-promtion-->
                     <form  action="<?=base_url('save/cart');?>" method="post" class="form_tour">
                      <input type="hidden" name="qty"  value="1"  />
                      <input type="hidden" name="product_id" value="<?=$product->product_id?>"  />
                      <button type="submit" class="buy-now"name="submit">ĐẶT TOUR</button>  
                    </form>
                  
                  </div>
                </div>
              </div>
            </div><!--end item-->
          <?php }?>
                         <div class="clear"></div>

                      </div>

                           
                        </div>
                </div>
            </div>
            <!--=== END: PRODUCT ===-->

</div>
                               
                        </div>
                        <!--===END: BOX MAIN===-->
                    </div>
                    <div class="clear"></div>
                </div>
        </div>
        <!--=== END: CONTENT ===-->



