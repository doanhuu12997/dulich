<link href="<?=base_url() ?>assets/web/css/tour_cate.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?=base_url() ?>assets/web/css/box.css">
<input type="hidden" id="get_idcate" value="<?=$get_id_catelogy->id ; ?>">
<!--=== BEGIN: CONTENT ===-->
<?php if (empty($get_id_catelogy->category_img)) {?>
<?php echo "";} else {?>
<div class="i-slder">
    <img src="<?=base_url('uploads/'.$get_id_catelogy->category_img)?>" />
    <div class="banner_box">
        <div class="container">
            <div class="bnBox_wrap">
                <h3 class="tour-banner-title"><?=$get_id_catelogy->category_name;?></h3>
            </div>
        </div>
    </div>
</div>
<?php }?>
<section id="form_search">
    <div class="form__search-tour">
        <div class="container">
            <form onSubmit="return validateForm_search()" name="formSearchname" method="get" action="search.html"
                class="form-horizontal tour-form">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <!--start input-search-tour-->
                        <div class="input-search">
                            <input class="form-control search" type="text" placeholder="Search keyword" name="search" />
                        </div>
                        <!--end input-search-tour-->
                    </div>
                    <div class="col-sm-4 col-xs-12">
                        <div class="row">
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <div class="dateF">
                                    <input type="date" placeholder="Từ ngày"
                                        class="DateTours form-control form-readonly-control" value="" name="from_day">
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-6">
                                <div class="dateF">
                                    <input type="date" placeholder="Đến ngày"
                                        class="DateTours form-control form-readonly-control" value="" name="to_day">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2 col-xs-12"> <button type="submit"
                            class="btn buttonCustomPrimary">Search</button></div>
                </div>
</section>
<div id="vnt-content">
    <div class="container ">
        <div class="mod-content ">
            <!--===BEGIN: BOX MAIN===-->
            <div class="box_mid">
                <div class="mid-content">
                    <div class="clear"></div>
                    <div class="main-content ">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="tab-tours">
                                    <div class="tab-content" id="nav-tabContent">
                                        <div class="tab-pane  show active" id="nav-248" role="tabpanel"
                                            aria-labelledby="nav-248-tab">
                                            <ul class="list-tour-01">
                                                <?php foreach ($get_all_product as $product) {  
                    $rs_dxp= $this->web_model->get_xuatphat($product->product_xuatphat);
                    ?>
                                                <li>
                                                    <div class="item row">
                                                        <div class="item-left col-md-4">
                                                            <a class="hvr-bounce-to-right shadow"
                                                                href="<?=base_url($product->product_slug).'.html';?>">
                                                                <img src="<?=base_url('uploads/product/thumb/'.$product->product_image)?>"
                                                                    alt="<?=$product->product_title;?>"
                                                                    title="<?=$product->product_title;?>" />
                                                            </a>
                                                        </div>
                                                        <div class="item-right col-md-8">
                                                            <div class="row">
                                                                <div class="col-md-8">
                                                                    <h3 class="title">
                                                                        <a
                                                                            href="<?=base_url($product->product_slug).'.html';?>"><?=$product->product_title;?></a>
                                                                    </h3>
                                                                    <ul class="meta">
                                                                        <li>
                                                                            Time: <?=$product->product_thoigian;?>
                                                                        </li>

                                                                        <li>Transport:
                                                                            <?=$product->product_phuongtien;?> </li>
                                                                        <li class="short_description">
                                                                            <?=$product->product_short_description;?>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <a href="<?=base_url($product->product_slug).'.html';?>"
                                                                        class="btn btn-view-detail"> <span>Detail</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </li>
                                                <?php }?>
                                            </ul>
                                        </div>
                                        <!--EnD tab-pane-->
                                    </div>
                                    <!--End tab-content-->
                                </div>
                                <!--End tab-tours-->
                                <div class="clear"></div>
                                <div id="pagination">
                                    <ul class="tsc_pagination">
                                        <!-- Show pagination links -->
                                        <?php foreach ($links as $link) {
                            echo "<li>". $link."</li>";
                            } ?>
                                    </ul>
                                </div>
                            </div>
                            <!--End md8-->
                        </div>
                        <!--End main-content-->
                        <div class="clear"></div>
                        <div class="mid-title">
                            <div class="w_mid_title">
                                <div class="clear"></div>
                                <div class="i-desc">
                                    <?php if(empty($get_id_catelogy->category_description)){echo "";} else echo $get_id_catelogy->category_description?>
                                </div>
                            </div>
                            <!--end w_mid_title-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--===END: BOX MAIN===-->
    </div>
    <div class="clear"></div>
</div>
</div>
<!--=== END: CONTENT ===-->
<!-- Fix lỗi Product Schema by evergreen.edu.vn -->