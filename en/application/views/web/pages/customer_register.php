
<div class="main breadcrumb">
	<br/>
	<div class="content" style="text-align: center">
		<div class="register_account">
			<h1>Đăng ký</h1>
			<style type="text/css">
				#result{color:red;padding: 5px}
				#result p{color:red}
			</style>
			<div id="result"><?php echo $this->session->flashdata('message'); ?></div>

			<form action="<?php echo base_url('customer/save');?>" class="form-horizontal register-main" method="post">
				<div class="form-group">
					<!--<label for="name" class="cols-sm-2 control-label">Họ Và Tên</label>-->
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
							<input type="text" class="form-control" name="customer_name"  placeholder="Tên "/>
						</div>
					</div>
				</div>

				<div class="form-group">
					<!--<label for="email" class="cols-sm-2 control-label">Email</label>-->
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
							<input required type="email" class="form-control" name="customer_email" placeholder="Email"/>
						</div>
					</div>
				</div>

				<div class="form-group">
					<!--<label for="username" class="cols-sm-2 control-label">Số điện thoại</label>-->
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
							<input required type="tel" class="form-control" id="shipping_phone" name="customer_phone" placeholder="Số điện thoại"/>
						</div>
					</div>
				</div>

				<div class="form-group">
					<!--<label for="username" class="cols-sm-2 control-label">Địa chỉ</label>-->
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
							<input type="text" class="form-control" name="customer_address" placeholder="Nhập địa chỉ để thanh toán nhanh hơn"/>
						</div>
					</div>
				</div>

				<div class="form-group">
					<!--<label for="password" class="cols-sm-2 control-label">Password</label>-->
					<div class="cols-sm-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
							<input type="password" class="form-control" name="customer_password" placeholder="Mật khẩu từ 6 đến 32"/>
						</div>
					</div>
				</div>

				<div class="form-group ">
					<button type="submit" class="btn btn-primary btn-lg btn-block login-button btntt">Đăng ký</button>
				</div>
				<div class="login-register"> Bạn đã có tài khoản ?
					<a href="<?php echo base_url('/dang-nhap.html'); ?>">Đăng nhập ngay</a>
				</div>
			</form>
		</div>  	
		<div class="clear"></div>
	</div>
</div>