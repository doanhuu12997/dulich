<!--===MODULE MAIN==-->
<link href="<?php echo base_url() ?>assets/web/css/check_out.css" rel="stylesheet" type="text/css" />
<!--===MODULE MAIN==-->
<div id="result"></div>
<div class="content">
<div class="wrap">


   <?php if($this->cart->total_items()==0)  { 
                  echo "<div class='cart-empty'>";
                    echo "<figure class='cart-empty__image'><div class='adr_bg_cartempty'></div></figure>";
                    echo "<h3 class='cart-empty_title'>Bạn chưa có sản phẩm trong giỏ hàng</h3>";
                    echo "<a href='/' class='btn-cart-continue-shopping' id='js-cart-continue-shopping-button'>Tiếp tục mua sắm</a>";
                    echo "</div>";

              } else {?>

<?php foreach ($cart_contents as $cart_items) {?>
<div class="title">
    <h1 class="title_v1"><?php echo $cart_items['name'] ?></h1>
    <div class="bg-bottom-title"></div>
</div>
<?php }?>
   <div class="sidebar box-payment-v1">
   <p class="title-form"> Thông tin thanh toán </p>
      <div class="sidebar-content">
      <form name="mycity" onsubmit="buttonName.disabled=true; return true;"  method="post" action="save-cart.html">

         <div class="order-summary order-summary-is-collapsed">
            <div class="order-summary-sections">
               <div class="order-summary-section order-summary-section-product-list" data-order-summary-section="line-items">
                  <table class="product-table table_no_ajax">
                     
                     <tbody>
                        <?php
                        foreach ($cart_contents as $cart_items) {
                        ?>
         <div class="list-payment-form-v1 list-payment-form-v1a">
            <div class="item item-3">
            <?php if(empty($cart_items['options']['tour_date_begin'])) {?>
                  <p>Ngày khởi hành</p>
                  <div class="inp-payment-v1">
                     <div class="avt">
                     <i class="fa fa-calendar"></i>
                     </div>
                     <input type="date" id="datepicker" name="datego" autocomplete="off" placeholder="dd/mm/yy" class="hasDatepicker">
                  </div>
                 <?php } else {?>
                   <p>Ngày khởi hành: <span class="khoihanh"><?php echo nice_date($cart_items['options']['tour_date_begin'],'d-m-Y'); ?></span></p>
               <?php }?>
               </div><!--End item-->


            <div class="item item-4">
                  <p>Số lượng</p>
                  
<?php if(empty($cart_items['options']['tour_price_people']))  {?>
  <!-- <div  class="list-order"> Người lớn  <span class="color-red"> (*)</span> -->
    <input type="number" name="qty" min="1" max="500" data-rowid="<?php echo $cart_items['rowid'] ?>"  value="<?php echo $cart_items['qty'] ?>" class="qty"  data-pricepeople="<?=$cart_items['price'];?>" />  

    <?php } else {?>
        <!-- <div  class="list-order"> Người lớn  <span class="color-red"> (*)</span> -->
    <input type="number" name="qty" min="1" max="500" data-rowid="<?php echo $cart_items['rowid'] ?>"  value="<?php echo $cart_items['qty'] ?>" class="qty"  data-pricepeople="<?=$cart_items['options']['tour_price_people'];?>" />  
    <?php }?>       
                 
       </div><!--End item-->
        </div>
        <div class="clear"></div>

               <ul class="payment-price-v1">
                  <li>
                     <p class="left">Đơn giá</p>
                     <?php if(empty($cart_items['options']['tour_price_people']))  {?>
                     <p class="money"><?php echo $this->cart->format_number($cart_items['price']) ?> ₫</p>
                     <?php } else {?>
                        <p class="money"><?php echo number_format($cart_items['options']['tour_price_people']); ?> ₫</p>
                        <?php }?>        

                  </li>
               </ul>
               <div class="clear"></div>
               </div> <!--End list-order-->
     <?php if(empty($cart_items['options']['tour_price_child'])) { echo ""; } else {?>
<div  class="list-order">Trẻ Em : <span class="price_tour"><?php echo number_format($cart_items['options']['tour_price_child']); ?> ₫</span>
  <input type="number"  data-price_child="<?=$cart_items['options']['tour_price_child'];?>"  value="0"    min="0"  pattern="[0-9]*" max="100" class="qty_child" data-qty="<?php echo $cart_items['qty'] ?>" />

</div> <!--End list-order-->
     <?php }?>

  <?php if(empty($cart_items['options']['tour_price_baby'])) { echo ""; } else {?>
      <div  class="list-order">Em Bé : <span class="price_tour"><?php echo number_format($cart_items['options']['tour_price_baby']); ?> ₫</span>
  <input type="number"  data-price_baby="<?=$cart_items['options']['tour_price_baby'];?>"  value="0"    min="0"  pattern="[0-9]*" max="100" class="qty_baby" data-qty="<?php echo $cart_items['qty'] ?>"  />
</div> <!--End list-order-->
  <?php }?>
  <div class="clear"></div>


               <div class="order-summary-section order-summary-section-total-lines payment-lines" data-order-summary-section="payment-lines">
                  <table class="total-line-table">
                     <tbody>

                     <tr class="total-line total-line-subtotal">
                           <td class="total-line-name">Tổng tiền</td>
                           <td class="total-line-price">
                              <span class="order-summary-emphasis order_total">
                                 <?php echo $this->cart->format_number($this->cart->total()); ?>₫
                              </span>
                           </td>
                        </tr>
                        <tr class="total-line total-line-subtotal">
                           <td class="total-line-name">Thanh toán</td>
                           <td class="total-line-price">
                              <span class="order-summary-emphasis order_total">
                             <?php echo $this->cart->format_number($this->cart->total()); ?>₫
                              </span>
                        <input type="hidden" name="sumck"  value="<?php echo $this->cart->total(); ?>">
                           </td>
                        </tr>
                        <!-- <tr class="total-line total-line-shipping">
                           <td class="total-line-name">Phí vận chuyển</td>
                           <td class="total-line-price">
                              <span class="order-summary-emphasis " id="load_ship">
                              </span>
                              <span class="load_code"></span>
                           </td> -->
                        </tr>
                     </tbody>
                  </table>
                   <span id="order-summary-ajax"></span>
                   <span id="order_ajax_child"></span>
                   <span id="order_ajax_baby"></span>
         
          <table>

               </div><!--End order-summary-->
               <div class="clear"></div>

                    <span class="product-description-name"><?=$cart_items['name'] ?></span>
                        <tr class="product">
                           <td class="product-image">
                              <div class="product-thumbnail">
                                 <div class="product-thumbnail-wrapper">
                                    <img class="product-thumbnail-image" src="<?php echo base_url('uploads/product/' . $cart_items['options']['product_image_order']) ?>" />
                                 </div>
                              </div>

                           </td>
                           <td class="product-description">
                              <span class="order-summary-small-text">
                               <?php if(empty($cart_items['options']['tour_xp'])) { echo ""; } else {?>
                             <div class="list-order_tour"><i class="fa fa-map-marker"></i> Điểm khởi hành: <span class="tour_xp"><?php echo $cart_items['options']['tour_xp']; ?></span></div>
                               <?php }?>

                               <?php if(empty($cart_items['options']['tour_time'])) { echo ""; } else {?>
                             <div class="list-order_tour"><i class="fa fa-clock-o"></i> Thời gian: <span class="thoigian"><?php echo $cart_items['options']['tour_time']; ?></span></div>
                               <?php }?>

                                 <?php if(empty($cart_items['options']['tour_pt'])) { echo ""; } else {?>
                             <div class="list-order_tour"><i class="fa fa-car"></i> Phương tiện: <span class="tour_pt"><?php echo $cart_items['options']['tour_pt']; ?></span></div>
                               <?php }?>


                              </span>
                           </td>
      
                           <!-- <td class="product-price">

                              <span class="price_ajax"></span>
                         
                           </td> -->

                        </tr>
                      <?php  }?>
                     </tbody>
                  </table>
                  



               </div>
      <div class="clear"></div>

            
            </div>
           
         </div>
      </div>
   </div><!--End sidebar-->
   <div class="main box-payment-v1">
   <p class="title-form">Thông tin liên hệ </p>
     
      <div class="message-box-ss">
         <p>Amara Tourist sẽ liên hệ ngay với bạn để xác nhận Booking này!</p>
      </div>
      <div class="main-content">
         <div class="step">
            <div class="step-sections steps-onepage">

            <div class="list-payment-form-v1">
           <div class="item item-1">
               <p>Email</p> <span>Xác nhận giữ chỗ sẽ được gửi qua email này.</span>
               <div class="inp-payment-v1">
                   <div class="avt"><i class="fa fa-envelope-o"></i> </div> <input  placeholder="Email" type="email"  id="email"  name="shipping_email" value="<?php echo isset($get_info_member->customer_email) ? $get_info_member->customer_email : ''; ?>" required/>
               </div>
           </div>
           <div class="item item-1">
               <p>Số điện thoại</p>
               <p><span>Amara Tourist sẽ liên hệ với Quý khách qua SĐT này.</span></p>
               <div class="inp-payment-v1">
                   <div class="avt"> <i class="fa fa-mobile"></i> </div> <input type="text"  id="phone" placeholder="Số điện thoại"  name="shipping_phone" value="<?php echo isset($get_info_member->customer_phone) ? $get_info_member->customer_phone : ''; ?>"  required>
               </div>
           </div>
           <div class="item item-1">
               <p>Họ và tên </p>
               <div class="inp-payment-v1">
                   <div class="avt"> <i class="fa fa-user"></i> </div> <input type="text" placeholder="Họ và tên" id="fullname" name="shipping_name" value="<?php echo isset($get_info_member->customer_name) ? $get_info_member->customer_name : ''; ?>" required />
               </div>
           </div>
           <div class="item item-1">
               <p>Địa chỉ</p>
               <div class="inp-payment-v1">
                   <div class="avt"> <i class="fa fa-map-marker"></i> </div> <input type="text"  id="address" placeholder="Địa chỉ"  name="shipping_address" value="<?php echo isset($get_info_member->customer_address) ? $get_info_member->customer_address : ''; ?>" required/>
               </div>
           </div>
           <div class="item item-2">
               <p>Yêu cầu thêm</p>
               <div class="txt-area-payment-v1"> <textarea  name="shipping_content" value=""  id="note" rows="3" placeholder="Ví dụ: Nhà mình có 3 người lớn, 1 trẻ em 3 tuổi..."></textarea> </div>
           </div>
       </div>
       <div class="message-box-av">
           <div class="custom-checkbox"> <input id="checkbox-1" type="checkbox"> <label for="checkbox-1"></label> </div>
           <p>Đặt trước giữ chỗ, thanh toán sau. Dễ dàng, thuận tiện, nhanh chóng !</p>
       </div>


               <div class="section">
                  <div class="section-content section-customer-information no-mb">
                     <div class="fieldset">
              
             <!--          <div class="field field-show-floating-label field-required field-third">
                           <div class="field-input-wrapper field-input-wrapper-select">
                              <label class="field-label" > Tỉnh  </label>
                              <select class="field-input"  id="city" required name="shipping_city" onchange="document.getElementById('text_content').value=this.options[this.selectedIndex].text">
                                  <option value=''> Chọn Tỉnh/Thành</option>
                                         <?php 
                                          $query = $this->db->query("select * from tbl_city  where cate_status =1  order by tbl_city.city_stt asc");
                                           $rs_city=$query->result();
                                       foreach ($rs_city as $v) { 
                                           ?>
                                         <option  value="<?php echo $v->id ?>"> <?php echo $v->city_name ?></option>
                                         <?php } ?>
                              </select>
                              <input type="hidden" name="get_city" id="text_content" value="" />
                           </div>
                        </div> -->
                        <input type="hidden" name="shipping_city" value="Hồ chí minh">
                     </div>
                  </div>
                  <!-- <div class="section-content">
                     <div class="clear"></div>
                     <div id="change_pick_location_or_shipping">
                        <div id="section-payment-method" class="section">
                           <div class="section-header">
                              <h2 class="section-title">Phương thức thanh toán</h2>
                           </div>
                           <div class="section-content">
                              <div class="content-box">
                                 <div class="radio-wrapper content-box-row">
                                    <label class="radio-label">
                                       <div class="radio-input">
                                          <input class="input-radio" name="payment_method" type="radio" value="Thanh toán bằng tiền mặt" checked />
                                       </div>
                                       <span class="radio-label-primary">Thanh toán bằng tiền mặt</span>
                                    </label>
                                 </div>
                                 <div class="radio-wrapper content-box-row">

                                 <label class="radio-label">
                                       <div class="radio-input">
                                          <input class="input-radio" name="payment_method" type="radio" value="Thanh toán chuyển khoản"  />
                                       </div>
                                       <span class="radio-label-primary">Thanh toán chuyển khoản</span>
                                     
                                <span style="font-weight: 400;">  <?php echo get_option('chuyenkhoang');?></span>

                                    </label>
                                   </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div> -->
                  <div class="clear"></div>
               </div>
           
            </div><!--end steps-onepage-->

            <div class="step-footer">
                  <div id="form_next_step" >
                     <button type="submit" name="buttonName" class="btn-submit">
                     <span class="btn-content">HOÀN THÀNH</span>
                     <i class="btn-spinner icon icon-button-spinner"></i>
                     </button>
                  </div>
               </div>

             </form>
         </div>
      </div>
              <?php  }?>

   </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
  $('#submit_code').click(function(e){ 

 e.preventDefault();
    var code = $("#discount_codes").val();
  console.log(code);
  $.ajax({
  url: '<?=base_url()?>ajax/ajax_discount_codes',
   method : "POST",
     data :{ code: code },
      async: true,
      dataType : 'html',
   success:function(data){
    $('.load_code').html(data);

   }
    
  })
 });

});

  $(document).on('change', '.qty', function(e){
  e.preventDefault();
   var qty = $(this).val();
   var rowid = $(this).data("rowid");
   var price_people = $(this).data("pricepeople");
  console.log(price_people);
   $.ajax({
    url:"<?php echo base_url(); ?>update_cart_ajax",
    method:"POST",
    data:{qty:qty,price_people:price_people,rowid:rowid},
    success:function(data)
    {
     //alert("Bạn đã cập nhập sản phẩm thành công");
     $('#order-summary-ajax').html(data);
      var get_count_peo = parseInt($('#get_count_peo').val());
      console.log(get_count_peo);
      console.log(qty);

      if(get_count_peo<=qty)
      {
        $(".get_count_peo").remove();
        $('.adults').html(qty);
        $('.adultss').html('<input type="hidden" value='+qty+' id="get_adults">');
      }
   
    }
   });
   });



    $(document).on('change', '.qty_child', function(e){
     e.preventDefault();
   var qty_child = $(this).val();
   var price_child = $(this).data("price_child");
   $("#order-summary-ajax").addClass('price_child');
   console.log(qty_child);

   $.ajax({
    url:"<?php echo base_url(); ?>update_cart_price_child",
    method:"POST",
    data:{qty_child:qty_child, price_child:price_child},
    success:function(data)
    {
     $('#order_ajax_child').html(data);
     $('.child').html(qty_child);
     $('.childs').html('<input type="hidden" name="get_child" value='+qty_child+' id="get_child">');
    }
   });
   });



    $(document).on('change', '.qty_baby', function(e){
     e.preventDefault();
   var qty_baby = $(this).val();
   var price_baby = $(this).data("price_baby");
   var qty = $(this).data("qty");
   $("#order-summary-ajax").addClass('price_baby');
   $("#order_ajax_child").addClass('ajax_price_baby');
   $(".ajax_price_baby .total_ajax").remove();
   console.log(qty_baby);

   $.ajax({
    url:"<?php echo base_url(); ?>update_cart_price_baby",
    method:"POST",
    data:{qty_baby:qty_baby, price_baby:price_baby,qty:qty},
    success:function(data)
    {
     $('#order_ajax_baby').html(data);
     $('.baby').html(qty_baby);
     $('.babys').html('<input type="hidden" name="get_baby" value='+qty_baby+' id="get_baby">');
    var get_adults = parseInt($('#get_adults').val());
    var get_child = parseInt($('#get_child').val());
    var get_baby = parseInt($('#get_baby').val());

    var total_People=get_adults+get_child+get_baby;
    console.log(total_People);
     $('.total_People').html(total_People);
    }
   });
   });


    $(document).ready(function(){
     $('#city').change(function(e){ 
                e.preventDefault();
                var id=$(this).val();
                console.log(id);

       $.ajax({
                    url: '<?=base_url()?>ajax/ajax_phiship',
                    method : "POST",
                    data :{ id: id },
                    async: true,
                    dataType : 'html',
                    success: function(data){
                     
                        $('#load_ship').html(data);
                    }
                });
                return false;

});

});
</script>
