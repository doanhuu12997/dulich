<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml" lang="vi" xml:lang="vi">


<head>


    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


    <meta name="viewport" content="width=device-width,initial-scale=1">


    <title><?=get_option('contact_title') ?></title>


    <meta name="description" content="<?=get_option('contact_subtitle') ?>">


    <meta name="keywords" content="<?=get_option('site_keywords') ?>">


    <meta content="index,follow" name="googlebot">


    <meta name="copyright" content="<?=get_option('site_copyright') ?>">


    <meta name="robots" content="INDEX,FOLLOW">


    <?=get_option('metageo') ?>


    <?=get_option('schema_content')?>


    <meta name="DC.title" lang="vi" content="<?=get_option('contact_subtitle') ?>">


    <meta name="DC.creator" content="<?=get_option('site_website') ?>">


    <meta name="DCTERMS.issued" scheme="DCTERMS.W3CDTF" content="<?php date_default_timezone_set('Asia/Ho_Chi_Minh');echo date('l, d M Y');echo date(" h:i:sa ") ?>">


    <meta name="DC.identifier" scheme="DCTERMS.URI" content="<?=base_url() ?>">


    <link rel="DCTERMS.replaces" hreflang="vi" href="<?=base_url() ?>">


    <meta name="DCTERMS.abstract" content="<?=get_option('contact_subtitle') ?>">


    <meta name="DC.format" scheme="DCTERMS.IMT" content="text/html">


    <meta name="DC.type" scheme="DCTERMS.DCMIType" content="Text">


    <meta property="og:title" content="<?=get_option('contact_title') ?>">


    <meta property="og:description" content="<?=get_option('contact_subtitle') ?>">


    <meta property="og:type" content="article">


    <?php if(empty($get_slider)){}else foreach($get_slider as $key=>$v){ ?>


        <?php if($key==0){ ?>


     <meta property="og:image" content="<?=base_url() ?>uploads/<?=$v->slider_image  ?>">


        <?php } ?>


    <?php } ?>


    <meta property="og:url" content="<?=base_url() ?>">


    <link rel="canonical" href="<?=base_url() ?>">


    <meta name="twitter:card" content="summary">


    <meta name="twitter:site" content="<?=get_option('site_twitter_link') ?>">


    <meta name="twitter:title" content="<?=get_option('contact_title') ?>">


    <meta name="twitter:description" content="<?=get_option('contact_subtitle') ?>">


    <link rel="shortcut icon" href="<?=base_url('/'); ?><?=get_option('site_favicon') ?>" type="image/x-icon">


    <link rel="icon" href="<?=base_url('/'); ?><?=get_option('site_favicon') ?>" type="image/x-icon">


    <script type="application/ld+json">{


        "@context": "http://schema.org",


        "@type": "LocalBusiness",


        "@id":"<?=base_url(); ?>",


        "url": "<?=base_url(); ?>",


        "additionaltype": ["https://en.wikipedia.org/wiki/Swimsuit","https://vi.wikipedia.org/wiki/%C4%90%E1%BB%93_b%C6%A1i"],


        "logo": "<?=base_url('uploads/');  ?><?=get_option('site_logo'); ?>",


        "image":"<?=base_url() ?>uploads/banner/<?=$v->slider_image ;?>",


        "priceRange":"100,000 vnđ - 800,000 vnđ",


        "hasMap": "https://www.google.com/maps/place/202+B%C3%A0u+C%C3%A1t,+Ph%C6%B0%E1%BB%9Dng+13,+T%C3%A2n+B%C3%ACnh,+Th%C3%A0nh+ph%E1%BB%91+H%E1%BB%93+Ch%C3%AD+Minh/@10.7909246,106.6443132,18z/data=!4m5!3m4!1s0x31752eb325a17b21:0xb4cf7b94d10a2049!8m2!3d10.791441!4d106.644399?shorturl=1", 


        "email": "mailto:<?=get_option('company_email') ?>",


        "hasOfferCatalog":  {


        "@type": "OfferCatalog",


        "itemListElement": 


        <?php


         if(empty($get_all_category_show_menu)) {echo '';}else {


        foreach($get_all_category_show_menu as $item){


            $result[] = array(


                '@type'   => 'Offer',


                'itemOffered' => 'Product',


                'name' => $item->category_name,


                'url' => $item->slug.'.html'


            );


        }


        echo json_encode($result);


         }


        ?>


    },


    "address": {


    "@type": "PostalAddress",


    "addressLocality": "Tân Bình",


    "addressCountry": "VIỆT NAM",


    "addressRegion": "Hồ Chí Minh",


    "postalCode":"700000",


    "streetAddress": "<?=get_option('site_address') ?>"


},


"description": "<?=get_option('contact_subtitle') ?>",


"name": "<?=get_option('contact_title') ?>",


"telephone": "<?=get_option('company_hotline') ?>",


"geo": {


"@type": "GeoCoordinates",


"latitude": 10.791437,


"longitude": 106.644469


},


"openingHoursSpecification": [{


"@type": "OpeningHoursSpecification",


"dayOfWeek": [


"Monday",


"Wednesday",


"Tuesday",


"Thursday",


"Friday"


],


"opens": "08:30",


"closes": "21:00"


},{





"@type": "OpeningHoursSpecification",


"dayOfWeek": [


"Saturday",


"Sunday"


],


"opens": "08:30",


"closes": "22:00"


}],


"sameAs": [


"https://www.facebook.com/<?=get_option('site_facebook_link') ?>/",


"https://www.instagram.com/<?=get_option('site_google_plus_link')?>/"





]





}


</script>