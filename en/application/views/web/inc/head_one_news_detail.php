
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="vi" xml:lang="vi">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="keywords" content="<?=$get_single_one_news->keywords?>" />
    <meta name="description" content="<?=$get_single_one_news->one_news_des?>" />
    <title><?=$get_single_one_news->one_news_title_bar?></title>
    <meta content="index,follow" name="googlebot" />
    <meta name="copyright" content="<?=get_option('site_copyright');?>" />
    <meta name="robots" content="INDEX,FOLLOW"/>
    <?=get_option('metageo');?>
    <?=get_option('schema_content')?>

    <meta name="DC.title" lang="vi" content="<?=$get_single_one_news->one_news_des?>" />
    <meta name="DC.creator" content="<?=base_url();?>" />
    <meta name="DCTERMS.issued" scheme="DCTERMS.W3CDTF" content="<?php date_default_timezone_set('Asia/Ho_Chi_Minh'); echo date('l, d M Y') ;  echo date(" h:i:sa");?>" />
    <meta name="DC.identifier" scheme="DCTERMS.URI" content="<?=base_url($get_single_one_news->one_news_slug).'.html';?>" />
    <link rel="DCTERMS.replaces" hreflang="vi" href="<?=base_url($get_single_one_news->one_news_slug).'.html';?>" />
    <meta name="DCTERMS.abstract" content="<?=$get_single_one_news->one_news_des?>" />
    <meta name="DC.format" scheme="DCTERMS.IMT" content="text/html" />
    <meta name="DC.type" scheme="DCTERMS.DCMIType" content="Text" />
    <meta property="og:title" content="<?=$get_single_one_news->one_news_title_bar?>" />
    <meta property="og:description" content="<?=$get_single_one_news->one_news_des?>" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="<?=base_url($get_single_one_news->one_news_slug).'.html';?>" />
    <meta property="og:image" content="<?=base_url('uploads/'.$get_single_one_news->one_news_image)?>" />
    <link rel="canonical" href="<?=base_url($get_single_one_news->one_news_slug).'.html';?>"/>
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="<?=get_option('site_twitter_link');?>">
    <meta name="twitter:title" content="<?=$get_single_one_news->one_news_title_bar?>">
    <meta name="twitter:description" content="<?=$get_single_one_news->one_news_des?>">
    <link rel="shortcut icon" href="<?=get_option('site_favicon') ?>" type="image/x-icon">
    <link rel="icon" href="<?=get_option('site_favicon') ?>" type="image/x-icon">

 <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "Article",
      "headline": "<?=$get_single_one_news->one_news_title_bar?>",
      "mainEntityOfPage": "<?=base_url($get_single_one_news->one_news_slug).'.html';?>",
      "image": "<?=base_url('uploads/'.$get_single_one_news->one_news_image)?>",  
      "author": {
      "@type": "Person",
      "name": "admin",
      "url": "<?=base_url($get_single_one_news->one_news_slug).'.html';?>"
    },  

    "publisher": {
    "@type": "Organization",
    "name": "admin",
    "logo": {
    "@type": "ImageObject",
    "url": "<?=base_url('uploads/');  ?><?=get_option('site_logo'); ?>"
  }
},

"datePublished": "<?=$get_single_one_news->create_date?>",
"dateModified": "<?=$get_single_one_news->create_date?>"
}
</script>