<!-- DÙNG CHUNG CHO TÒAN SITE -->

<link
    href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=vietnamese"
    rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">

<link rel="stylesheet" href="<?= base_url() ?>assets/web/bootstrap-3.3.7-dist/css/bootstrap.min.css">

<link rel="stylesheet" href="<?= base_url() ?>assets/web/css/slick.css">

<link rel="stylesheet" href="<?= base_url() ?>assets/web/css/slick-theme.css">

<link rel="stylesheet" href="<?= base_url() ?>assets/web/css/style.css">

<link rel="stylesheet" href="<?= base_url() ?>assets/web/css/custom.css">

</head>



<body class=" <?php $page_home = base_url();

                $page_current = current_url();

                if ($page_home == $page_current) { ?>home<?php  } else { ?>subpage<?php } ?>">

    <script type="text/javascript" src="<?= base_url() ?>assets/web/js/jquery-2.1.3.min.js"></script>

    <script type="text/javascript" src="<?= base_url() ?>assets/web/js/style.js"></script>

    <script type="text/javascript" src="<?= base_url() ?>assets/web/js/custom.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <link href="<?= base_url() ?>assets/web/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet"
        type="text/css" />

    <div class="result"><?= $this->session->flashdata('message'); ?></div>

    <style>
    /*-----BEGIN SLICK----*/





    /*-----END SLICK----*/
    </style>

    <!--===MODULE MAIN==-->

    <link href="<?= base_url() ?>assets/web/js/slideSlick/css/slick-theme.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript" src="<?= base_url() ?>assets/web/js/slick.min.js"></script>

    <div id="PageOver"></div>

    <?php get_option('google_analytics_body'); ?>

    <div id="vnt-wrapper">

        <div id="vnt-container">

            <div class="head-top-menu">

                <div class="container-fluid">

                    <div class="row" style="    display: flex;flex-wrap: wrap;">

                        <div class=" col-md-7 col-xs-12 col-sm-7">

                            <div class="info_menu">

                                <ul>

                                    <li class="hotline__top"><a><?= get_option('khauhieu'); ?> <i
                                                class="fa fa-angle-down" aria-hidden="true"></i></a>

                                        <?php if (!empty($all_hotline)) { ?>

                                        <div class="list__hotsline ">

                                            <?php foreach ($all_hotline as $ht) { ?>

                                            <div class="hotline_item"><span class="name"><?= $ht->name ?></span> <span
                                                    class="phone"><?= $ht->phone ?></span></div>

                                            <?php } ?>

                                        </div>

                                        <?php  } else { ?>

                                        <div></div>

                                        <?php } ?>



                                    </li>

                                </ul>

                            </div>

                            <div class="clear"></div>

                        </div>

                        <!--end  logo-->

                        <div class="col-md-5 col-xs-12 col-sm-5">

                            <div class="info_hs">

                                <ul>



                                    <li><a href=""><i class="fa fa-address-card-o"></i> Capacity Profile </a></li>

                                    <li><a href="https://amaratours.vn/"><i class="flag-english"></i> Việt Nam</a></li>

                                </ul>

                            </div>

                            <div class="clear"></div>

                        </div>

                        <!--End md-8-->

                    </div>

                </div>

                <!--End container-->

            </div>

            <!--End head-top-menu-->

            <div class="header_mid">

                <div class="container-fluid">

                    <div class="row" style="display: flex;flex-wrap:wrap">

                        <div class="vnt-logo col-md-7 col-xs-12 col-sm-12">

                            <div class="wrap">

                                <a class="img_home" href="<?= base_url('/'); ?>"><img
                                        alt="<?= get_option('contact_title'); ?>"
                                        src="<?= base_url('uploads/');  ?><?= get_option('site_logo'); ?>">

                                    <div class="">

                                        <h1><?= get_option('contact_title'); ?></h1>

                                        <p style="color:#F58634 ;font-style:italic;font-size:18px;text-align: center;">
                                            <?php get_option('site_slogan'); ?></p>

                                    </div>

                                </a>

                                <div class="menu_mobile">



                                    <div class="icon_menu"><span class="style_icon"></span></div>

                                </div>

                            </div>

                        </div>

                        <div class="col-md-5 col-xs-12 col-sm-12">

                            <div class="info_menu_right" style="height: 100%; display:flex;align-items:center">

                                <ul>

                                    <li><a href="gioi-thieu.html">Introduce </a></li>

                                    <li><a href="#">News </a></li>

                                    <li><a href="our-privacy.html">Our Privacy</a></li>

                                    <li><a href="recruitment.html">Recruitment </a></li>

                                </ul>

                            </div>

                        </div>

                    </div>



                </div>

                <div class="wrap__menu">

                    <div class="divmm">

                        <div class="mmContent">

                            <div class="mmMenu">

                                <?php $id_top = 65;

                                $infor_top_left = $this->web_model->get_all_one_news_post_home($id_top);

                                echo $infor_top_left->one_news_long; ?>

                                <div class="clear"></div>

                                <div class="vnt-search-mobile">

                                    <div class="formSearch">

                                        <form method="get" action="search.html" class="box_search">

                                            <div id="au-mobile">

                                                <input name="search" type="text" autocomplete="off" id="searchs"
                                                    class="text_search form-control" placeholder="Tìm kiếm...">

                                                <span class="input-group-btn">

                                                    <button type="submit" class="btn">

                                                        <i class="fa fa-search"></i>

                                                    </button>

                                                </span>

                                            </div>

                                            <!--END autocomplete-->

                                        </form>

                                    </div>

                                </div>

                                <!--End vnt-search-moblie-->

                                <ul class="set_menu"></ul>

                            </div>

                            <div class="close-mmenu"></div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="wap-header">

                <div class="header_menu_bottom">

                    <div class="container-fluid">

                        <div class="row">

                            <div class="vnt-menutop col-md-12 col-xs-12 col-sm-12">



                                <ul class="get_menu">

                                    <li><a href="<?= base_url('/'); ?>"><i class="fa fa-home"></i></a> </li>

                                    <?php foreach ($get_all_category_show_menu as $menu) {

                                        $rs_2 = $this->web_model->sub_category($menu->id); ?>

                                    <li><a href="<?= base_url($menu->slug) . '.html'; ?>"> <?= $menu->category_name ?>

                                            <?php if (empty($rs_2)) {

                                                    echo "";
                                                } else { ?><i class="fa fa-angle-down"></i><?php } ?></a>

                                        <ul class="tutuc tour">

                                            <?php foreach ($rs_2 as $c2) {

                                                    $rs_3 = $this->web_model->sub_category($c2->id); ?>

                                            <li><a href="<?= $c2->slug . '.html'; ?>"><b>

                                                        <?= $c2->category_name ?></b></a>

                                                <?php if ($menu->id == 468) {

                                                            echo '';
                                                        } else { ?>

                                                <ul class="sub_3">

                                                    <?php foreach ($rs_3 as $c3) { ?>

                                                    <li><a href="<?= base_url($c3->slug) . '.html'; ?>">

                                                            <b><?= $c3->category_name ?> </b></a></li>

                                                    <?php } ?>

                                                </ul>

                                                <?php } ?>

                                            </li>

                                            <?php } ?>

                                        </ul>

                                    </li>

                                    <?php } ?>

                                    <?php

                                    foreach ($show_menu_dmtin as $menu_news) {

                                        $rs_c2 = $this->web_model->get_child_menu_news($menu_news->id); ?>

                                    <?php if (empty($rs_c2)) { ?>

                                    <li><a
                                            href="<?= base_url($menu_news->slug) . '.html'; ?>"><?= $menu_news->category_name ?></a>

                                    </li>

                                    <?php  } else { ?>

                                    <li><a href="<?= base_url($menu_news->slug) . '.html'; ?>"><?= $menu_news->category_name ?><i
                                                class="fa fa-angle-down"></i><?php } ?></a>

                                        <ul class="tutuc">

                                            <?php foreach ($rs_c2 as $c2) { ?>

                                            <li><a href="<?= base_url($c2->slug) . '.html'; ?>">

                                                    <b><?= $c2->category_name ?></b></a> </li>

                                            <?php } ?>

                                        </ul>

                                    </li>

                                    <?php } ?>

                                    <?php foreach ($get_one_news as $v) { ?>

                                    <li><a
                                            href="<?= base_url($v->one_news_slug) . '.html'; ?>"><b><?= $v->one_news_title ?></b></a>

                                    </li>

                                    <?php } ?>

                                    <li><a href="<?= base_url('contact') . '.html'; ?>"><b>Contact</b></a> </li>



                                </ul>

                            </div>

                            <!--END MENU-->

                        </div>

                    </div>

                </div>

                <!--End headmenu bootom-->

            </div>

            <!--End wap-header-->