

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="vi" xml:lang="vi">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>404 Not Found</title>
    <meta name="description" content="Tiếc quá, trang bạn muốn xem không tồn tại! Hãy thử sử dụng các tính năng bên dưới để tiếp tục nhé"/>
    <meta name="keywords" content=""/>

    <meta content="index,follow" name="googlebot" />
    <meta name="copyright" content="<?php echo get_option('site_copyright')?>" />
    <meta name="robots" content="INDEX,FOLLOW"/>
         <?php echo get_option('metageo')?>
        <?php echo get_option('schema_content')?>


    <meta name="DC.title" lang="vi" content="404 Not Found" />
    <meta name="DC.creator" content="Thiết kế web chuyên nghiệp - Buffseo" />
    <meta name="DCTERMS.issued" scheme="DCTERMS.W3CDTF" content="Tue, 09 Jul 2019 10:00:25 +0700" />
    <meta name="DC.identifier" scheme="DCTERMS.URI" content="<?php echo base_url()?>" />
    <link rel="DCTERMS.replaces" hreflang="vi" href="<?php echo base_url()?>" />
    <meta name="DCTERMS.abstract" content="Tiếc quá, trang bạn muốn xem không tồn tại! Hãy thử sử dụng các tính năng bên dưới để tiếp tục nhé" />
    <meta name="DC.format" scheme="DCTERMS.IMT" content="text/html" />
    <meta name="DC.type" scheme="DCTERMS.DCMIType" content="Text" />


    <meta property="og:title" content="404 Not Found" />
    <meta property="og:description" content="404 Not Found" />
    <meta property="og:type" content="article" />
    <?php if(empty($get_slider)){}else foreach ($get_slider as $key => $v) { ?>
    <?php if($key==0) { ?><meta property="og:image" content="<?php echo base_url() ?>uploads/<?php echo $v->slider_image ?>" />  <?php  }?>
    <?php }?>

    <meta property="og:url" content="<?php echo base_url()?>;" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="<?php echo get_option('site_twitter_link')?>">
    <meta name="twitter:title" content="<?php echo get_option('contact_title')?>">
    <meta name="twitter:description" content="Tiếc quá, trang bạn muốn xem không tồn tại! Hãy thử sử dụng các tính năng bên dưới để tiếp tục nhé">


 <link rel="shortcut icon" href="<?php echo base_url('/') ?><?php echo get_option('site_favicon') ?>" type="image/x-icon">
  <link rel="icon" href="<?php echo base_url('/') ?><?php echo get_option('site_favicon') ?>" type="image/x-icon">

