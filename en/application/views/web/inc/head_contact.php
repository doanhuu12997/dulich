
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="vi" xml:lang="vi">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <title><?=get_option('site_contact_num1');?></title>
    <meta name="description" content="<?=get_option('site_contact_num2')?>"/>
    <meta name="keywords" content="<?=get_option('site_keywords_contact')?>"/>
    <meta content="index,follow" name="googlebot" />
    <meta name="copyright" content="<?=get_option('site_copyright')?>" />
    <meta name="robots" content="INDEX,FOLLOW"/>
    <?=get_option('metageo');?>
    <?=get_option('schema_content')?>


    <meta name="DC.title" lang="vi" content="<?=get_option('site_contact_num1')?>" />
    <meta name="DC.creator" content="Thiết kế web chuyên nghiệp - Buffseo" />
    <meta name="DCTERMS.issued" scheme="DCTERMS.W3CDTF" content="<?php date_default_timezone_set('Asia/Ho_Chi_Minh'); echo date('l, d M Y') ;  echo date(" h:i:sa")?>" />
    <meta name="DC.identifier" scheme="DCTERMS.URI" content="<?=base_url()?>" />
    <link rel="DCTERMS.replaces" hreflang="vi" href="<?=base_url()?>" />
    <meta name="DCTERMS.abstract" content="<?=get_option('site_contact_num2')?>" />
    <meta name="DC.format" scheme="DCTERMS.IMT" content="text/html" />
    <meta name="DC.type" scheme="DCTERMS.DCMIType" content="Text" />


    <meta property="og:title" content="<?=get_option('site_contact_num1')?>" />
    <meta property="og:description" content="<?=get_option('site_contact_num2')?>" />
    <meta property="og:type" content="article" />
    <?php foreach ($get_slider as $key => $v) { ?>
    <?php if($key==0) { ?><meta property="og:image" content="<?=base_url() ?>uploads/<?=$v->slider_image ?>" />  <?php  }?>
    <?php }?>

    <meta property="og:url" content="<?=base_url()?>" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="<?=get_option('site_twitter_link')?>">
    <meta name="twitter:title" content="<?=get_option('site_contact_num1')?>">
    <meta name="twitter:description" content="<?=get_option('site_contact_num2')?>">


 <link rel="shortcut icon" href="<?=base_url('/') ?><?=get_option('site_favicon') ?>" type="image/x-icon">
  <link rel="icon" href="<?=base_url('/') ?><?=get_option('site_favicon') ?>" type="image/x-icon">
