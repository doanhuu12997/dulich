<div  class="col-md-10 col-xs-12">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard')?>">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="<?php echo base_url('add/brand')?>">Add Thương hiệu</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Thêm Thương hiệu</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message');?></p>
            </div>
            <div class="box-content">
                <form class="form-horizontal" action="<?php echo base_url('save/brand')?>" method="post" enctype="multipart/form-data">
                    <fieldset>
                        <div class="control-group col-md-10 col-xs-12">
                            <label>Tên Thương hiệu</label>
                            <div class="controls">
                                <input class="form-control" name="brand_name" id="fileInput" type="text"/>
                            </div>
                        </div>
                        <!-- <div class="control-group col-md-2 col-xs-12">
                            <label>STT</label>
                            <div class="controls">
                                <input class="form-control" name="brand_stt" type="text"/>
                            </div>
                        </div> -->
                        <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput">Banner page</label>
                            <div class="controls">
                                <input class="form-control" name="brand_banner" type="file"/>
                            </div>
                        </div>                 

                        <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput">Logo Thương hiệu</label>
                            <div class="controls">
                                <input class="form-control" name="brand_image" id="fileInput" type="file"/>
                            </div>
                        </div>                 
                        <div class="control-group col-md-12 col-xs-12">
                            <label class="control-label" for="textarea2"><b>GIỚI THIỆU VỀ Thương hiệu</b></label>
                            <div class="controls">
                                <textarea class="cleditor form-control" name="brand_description" rows="3"></textarea>
                            </div>
                        </div>

                        <div class="clear"></div>
                        <div class="wap-seo">
                            <label class="i_title" for="fileInput"> <b>NỘI DUNG SEO</b></label>
                            <div class="clear"></div>
                            <div class="control-group">
                            <label class="control-label" for="fileInput">Tiêu đề SEO</label>
                            <div class="controls">
                                <input class="form-control"   name="brand_title" type="text"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="fileInput">SEO slug</label>
                            <div class="controls">
                                <input class="form-control"  name="brand_slug" type="text"/>
                            </div>

                            <div class="control-group">
                            <label class="control-label" for="fileInput">Mô tả SEO</label>
                            <div class="controls">
                                <textarea class="i-des-sort form-control" name="brand_description_seo" rows="3"></textarea>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="textarea2">Meta Keywords</label>
                            <div class="controls">
                                <textarea class="i-des-sort form-control" name="brand_keywords" rows="2"></textarea>
                            </div>
                        </div>

                        <!--</div>end wapseo-->

                        <div class="control-group col-md-3 col-xs-12">
                            <label class="control-label" for="textarea2">Trạng thái</label>
                            <div class="controls">
                                <select class="form-control" name="brand_publication_status">
                                    <option value="1">Hiện</option>
                                    <option value="0">Ẩn</option>
                                </select>
                            </div>
                        </div>
                        
                        <!--<div class="control-group col-md-9 col-xs-12"></div>-->

                        <div class="control-group col-md-12 col-xs-12">
                            <button type="submit" class="btn btn-primary">Lưu thay đổi</button>
                            <button type="reset" class="btn">Hủy</button>
                        </div>
                    </fieldset>
                </form>   
            </div>
        </div><!--/span-->
    </div><!--/row-->
</div><!--/.fluid-container-->

<!-- end: Content