<!-- start: Content -->
<div  class="col-md-10 col-xs-12">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard')?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/brand')?>">Quản lý Thương hiệu</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Quản lý Thương hiệu</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>
            <div class="add_cate">
                <a href="<?php echo base_url('add/brand')?>">
                    <i class="fa fa-plus-circle"></i>
                    <span class="hidden-tablet"> Thêm</span>
                </a>
            </div>
            
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên Thương hiệu</th>
                            <th>Tiêu đề SEO</th>
                            <th>SEO slug</th>
                            <th>Mô tả SEO</th>
                            <th>Hiển thị</th>
                            <th>Sửa</th>
                            <th>Xóa</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php 
                        $i=0;
                        foreach($all_brand as $single_brand){
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td style="width: 15%;" class="center"><?php echo $single_brand->brand_name;?></td>
                                <td style="width: 22%;" class="center"><?php echo $single_brand->brand_title;?></td>
                                <td style="width: 12%;" class="center"><?php echo $single_brand->brand_slug;?></td>
                                <td style="width: 30%;" class="center"><?php echo $single_brand->brand_description_seo;?></td>
                                <td class="center">
                                    <?php if ($single_brand->brand_publication_status == 1) { ?>
                                        <a href="<?php echo base_url('unpublished/brand/' . $single_brand->brand_id); ?>">
                                            <div class="toast_check">
                                                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                                                </g></g>
                                                </svg>
                                            </div>
                                        </a>
                                    <?php } else {
                                    ?>
                                        <a href="<?php echo base_url('published/brand/' . $single_brand->brand_id); ?>">
                                            <div class="toast_un_check">
                                                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                    <g>
                                                        <g>
                                                            <path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0 c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7 C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </a>
                                    <?php }
                                    ?>
                                </td>
                                <td class="center">
                                    <a class="btn btn-info" href="<?php echo base_url('edit/brand/' . $single_brand->brand_id); ?>">
                                        <i class="halflings-icon white edit"></i>  
                                    </a>
                                </td>
                                <td class="center">
                                    <a class="confirmClick btn btn-danger" href="<?php echo base_url('delete/brand/' . $single_brand->brand_id); ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                </td>
                            </tr>
                        <?php }?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->
    </div><!--/row-->
</div><!--/.fluid-container-->

<!-- end: Content -->