<!-- start: Content -->


<div class="col-md-10 col-xs-12">

    <ul class="breadcrumb">


        <li>


            <i class="icon-home"></i>


            <a href="<?php echo base_url('dashboard') ?>">Home</a>


            <i class="icon-angle-right"></i>


        </li>


        <li><a href="<?php echo base_url('manage/thanhphan') ?>">Management hotline </a></li>


    </ul>





    <div class="row-fluid sortable">


        <div class="box span12">


            <div class="box-header" data-thanhphanal-title>


                <h2><i class="halflings-icon user"></i><span class="break"></span>Management hotline</h2>


                <div class="box-icon">


                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>


                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>


                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>


                </div>


            </div>





            <style type="text/css">
                #result {
                    color: red;
                    padding: 5px
                }


                #result p {
                    color: red
                }
            </style>


            <div id="result">


                <p><?php echo $this->session->flashdata('message'); ?></p>


            </div>


            <div class="add_cate"><a href="<?php echo base_url('add/hotline') ?>"><i class="fa fa-plus-circle"></i><span class="hidden-tablet"> Add</span></a></div>





            <div class="box-content">


                <table class="table table-striped table-bordered bootstrap-datatable datatable">


                    <thead>


                        <tr>


                            <th>Stt</th>


                            <th>Name</th>


                            <th>Phone</th>


                            <!--<th>Status</th>-->


                            <th>Actions</th>


                        </tr>


                    </thead>


                    <tbody>


                        <?php


                        $i = 0;


                        foreach ($all_hotline as $single_hotline) {


                            $i++;


                        ?>


                            <tr>


                                <td><?php echo $i; ?></td>


                                <td class="center"><?php echo $single_hotline->name; ?></td>


                                <td class="center"><?php echo $single_hotline->phone; ?></td>




                                <td class="center">


                                    <?php if ($single_hotline->publication_status == 0) { ?>


                                        <a class="btn btn-danger" href="<?php echo base_url('published/hotline/' . $single_hotline->id); ?>">

                                            <i class="halflings-icon white thumbs-down"></i>




                                        </a>


                                    <?php } else {


                                    ?>


                                        <a class=" btn btn-success" href="<?php echo base_url('unpublished/hotline/' . $single_hotline->id); ?>">

                                            <i class="halflings-icon white thumbs-up"></i>




                                        </a>


                                    <?php }


                                    ?>





                                    <a class="btn btn-info" href="<?php echo base_url('edit/hotline/' . $single_hotline->id); ?>">


                                        <i class="halflings-icon white edit"></i>


                                    </a>


                                    <a class="confirmClick btn btn-danger" href="<?php echo base_url('delete/hotline/' . $single_hotline->id); ?>">


                                        <i class="halflings-icon white trash"></i>


                                    </a>


                                </td>


                            </tr>


                        <?php } ?>


                    </tbody>


                </table>


            </div>


        </div><!--/span-->














    </div><!--/row-->











</div><!--/.fluid-container-->





<!-- end: Content -->