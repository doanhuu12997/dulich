<div  class="col-md-10 col-xs-12">


	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?php echo base_url('dashboard')?>">Home</a>
			<i class="icon-angle-right"></i> 
		</li>
		<li>
			<i class="icon-edit"></i>
			<a href="<?php echo base_url('edit/avd/'.$avd_info_by_id->avd_id)?>">Edit banner</a>
		</li>
	</ul>

	<div class="row-fluid sortable">
		<div class="box ">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon edit"></i><span class="break"></span>Edit banner</h2>
				<div class="box-icon">
					<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
					<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
					<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
				</div>
			</div>
			<style type="text/css">
				#result{color:red;padding: 5px}
				#result p{color:red}
			</style>
			<div id="result">
				<p><?php echo $this->session->flashdata('message');?></p>
			</div>
			<div class="box-content">
				<form class="form-horizontal" action="<?php echo base_url('update/avd/'.$avd_info_by_id->avd_id);?>" method="post" enctype="multipart/form-data">
					<fieldset>
						<div class="control-group col-md-8 col-xs-12">
							<label class="control-label" for="fileInput">Tên banner</label>
							<div class="controls">
								<input class="form-control" value="<?php echo $avd_info_by_id->avd_title;?>" name="avd_title" type="text"/>
							</div>
						</div> 
						<div class="control-group col-md-4 col-xs-12">
							<label class="control-label" for="fileInput">Vị trí đặt</label>
							<div class="controls">
								<select class="form-control"  name="id_cate_page">
									<option value="<?php  if($avd_info_by_id->id_cate_page==1){ echo "1"; } else if($avd_info_by_id->id_cate_page==2){ echo "2"; }?>">
										<?php  if($avd_info_by_id->id_cate_page==1){ echo "Right"; } else if($avd_info_by_id->id_cate_page==2){ echo "LEFT"; } ?>
									</option>      

									<option value="1">RIGHT</option>
									<option value="2">LEFT</option>
<!-- 									<option value="3">Được tin dùng</option>
 -->								<!-- 	<option value="4">Chi tiết sản phẩm</option>
									<option value="5">Chi tiết tin tức</option> -->
									<!--<option value="6">Banner Footer</option>-->
								</select>

							</div>
						</div> 
						<div class="clear"></div>

						<div class="control-group col-md-5 col-xs-12">
							<label class="control-label" for="fileInput">Upload banner <i style="color:#f00;font-size: 11px;">(900px x 285px)Rộng x Cao</i></label>
							<div class="controls">
								<input class="form-control" name="avd_image" type="file"/>
								<input class="form-control" value="<?php echo $avd_info_by_id->avd_image;?>" name="avd_delete_image" type="hidden"/>
							</div>
						</div>


						<div class="control-group col-md-7 col-xs-12">
							<label class="control-label" for="fileInput">Hình banner</label>
							<div class="controls">
								<img src="<?php echo base_url('uploads/'.$avd_info_by_id->avd_image);?>"/>
							</div>
						</div>

						<div class="clear"></div>

						<div class="control-group col-md-9 col-xs-12">
							<label class="control-label" for="fileInput">Link trang đích</label>
							<div class="controls">
								<input class="form-control" value="<?php echo $avd_info_by_id->avd_link;?>" name="avd_link" type="url"/>
							</div>
						</div>

						<div class="control-group col-md-3 col-xs-12">
							<label class="control-label" for="textarea2">Hiển thị</label>
							<div class="controls">
								<select name="publication_status" class="form-control">
									<option value="1">Yes</option>
									<option value="0">No</option>
								</select>
							</div>
						</div>

						<div class="control-group col-md-12 col-xs-12">
							<button type="submit" id="save_category" class="btn btn-primary">Lưu thay đổi</button>
							<button  class="btn"><a href="<?php echo base_url('manage/avd')?>">Hủy</a></button>
						</div>
					</fieldset>
				</form>   

			</div>
		</div><!--/span-->

	</div><!--/row-->



</div><!--/.fluid-container-->

<!-- end: Content