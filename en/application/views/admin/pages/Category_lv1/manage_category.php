<!-- start: Content -->
<div  class="col-md-10 col-xs-12">
    <!-- 
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard') ?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/category') ?>">Manage Category</a></li>
    </ul>-->

    <div class="row-fluid sortable">        
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="fa fa-book"></i><span class="break"></span>Quản lý danh mục</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="fa fa-cog fa-spin fa-1x fa-fw"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <?php if($role_user->role_add_lv1==1)
            {?>
                <div class="add_cate">
                    <a href="<?php echo base_url('add/category')?>">
                        <i class="fa fa-plus-circle"></i>
                        <span class="hidden-tablet">Thêm</span>
                    </a>
                </div>
            <?php }?>
            <style type="text/css">
                #result{color:red;padding:5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>

            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên danh mục</th>
                            <th>SEO Slug</th>
                            <th>Xem</th>
                            <th>Home</th>
                            <th>Menu</th>
                            <!--<th>Footer</th>-->
                            <th>Hiển thị</th>
                            <th>Chỉnh sửa</th>
                            <th>Xóa</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php $i=0;
                        foreach ($all_categroy as $single_category) {
                            $i++;
                            $query =$this->db->query("select * from tbl_category where parent_id='".$single_category->id."'");
                                $rs_catalogy = $query->result();

                            ?>
                            <tr>
                                <td><?php echo $i ; ?></td>
                                <td><i class="fa fa-folder-open"></i> <?php echo $single_category->category_name ?>
                                  <ul class="catelogy">
                                    <?php  foreach ($rs_catalogy as $key) { 
                                   $id=$key->id;
                                   $sub_cate= $this->category_model->sub_category($id); ?>
                                    <li>
                                        <a><i class="fa fa-folder-open"></i> <?php echo $key->category_name;?></a>
                                        <a  href="<?php echo base_url('edit/category/' . $key->id); ?>">
                                        <i class="fa fa-pencil"></i></a>
                                          <a class="btn btn-danger" href="<?php echo base_url('delete/category/' . $key->id); ?>">
                                        <i class="halflings-icon white trash"></i> </a>


                                        <ul>
                                            <?php foreach ($sub_cate as $sub) {?>
                                            <li>
                                                <a><i class="fa fa-folder-open"></i> <?php echo $sub->category_name;?></a>
                                                <a  href="<?php echo base_url('edit/category/' . $sub->id); ?>">
                                                <i class="fa fa-pencil"></i></a>
                                                  <a class="btn btn-danger" href="<?php echo base_url('delete/category/' . $sub->id); ?>">
                                                <i class="halflings-icon white trash"></i> </a>
                                            </li>
                                            <?php }?>
                                        </ul>
                                  </li>
                                   <?php }?>
                                </ul>
                            </td>
                                <td> 
                                    <i class="fa fa-pencil-square-o"></i> 
                                    <?php echo $single_category->slug ?>
                                </td>

                                <td>
                                    <a class="i-target" target="target" href="<?php echo base_url($single_category->slug).'.html'?>"><i class="fa fa-eye"></i>
                                    </a>
                                </td>
                                
                                <td class="center">
                                    <?php if ($single_category->publication_home == 1) 
                                    { ?>
                                        <a href="<?php echo base_url('unpublished_home_category/category/' . $single_category->id); ?>">
                                            <img class="i-gif" src="<?php echo base_url('assets/admin/img/active_1.png')?>"/>
                                            
                                        </a>
                                    <?php } else {?>
                                        <a href="<?php echo base_url('published_home_category/category/' . $single_category->id); ?>">
                                            <img class="i-gif" src="<?php echo base_url('assets/admin/img/active_0.png')?>"/>
                                        </a>
                                    <?php }?>
                                </td>
                                
                                <td class="center">
                                    <?php if ($single_category->publication_header == 1) { ?>
                                        <a href="<?php echo base_url('unpublished_header/category/' . $single_category->id); ?>">
                                            <img  class="i-gif" src="<?php echo base_url('assets/admin/img/active_1.png')?>"/>
                                        </a>

                                    <?php } else {?>
                                        <a href="<?php echo base_url('published_header/category/' . $single_category->id); ?>">
                                            <img class="i-gif" src="<?php echo base_url('assets/admin/img/active_0.png')?>"/>
                                        </a>
                                    <?php }?>
                                </td>
                                <!--
                                <td class="center">
                                    <?php if ($single_category->publication_footer == 1) { ?>
                                        <a href="<?php echo base_url('unpublished_footer/category/' . $single_category->id); ?>">
                                            <img class="i-gif" src="<?php echo base_url('assets/admin/img/active_1.png')?>"/>
                                        </a>
                                    <?php } else {?>
                                        <a href="<?php echo base_url('published_footer/category/' . $single_category->id); ?>">
                                            <img class="i-gif" src="<?php echo base_url('assets/admin/img/active_0.png')?>"/>
                                        </a>
                                    <?php }?>
                                </td>
                                -->
                                <td class="center">
                                    <?php if ($single_category->cate_status == 1) { ?>
                                        <a href="<?php echo base_url('unpublished/category/' . $single_category->id); ?>">
                                            <img class="i-gif" src="<?php echo base_url('assets/admin/img/active_1.png')?>"/>
                                        </a>
                                    <?php } else {?>
                                        <a href="<?php echo base_url('published/category/' . $single_category->id); ?>">
                                            <img class="i-gif" src="<?php echo base_url('assets/admin/img/active_0.png')?>"/>
                                        </a>
                                    <?php }?>
                                </td>

                                <td class="center">
                                    <?php if($role_user->role_edit_lv1==1){  ?>
                                        <a href="<?php echo base_url('edit/category/' . $single_category->id); ?>">
                                            <i class="fa fa-pencil"></i>                      
                                        </a>
                                    <?php }?>
                                </td>

                                <td class="center">
                                    <?php if($role_user->role_delete_lv1==1){  ?>
                                        <a class="confirmClick btn btn-danger" href="<?php echo base_url('delete/category/' . $single_category->id); ?>">
                                            <i class="halflings-icon white trash"></i> 
                                        </a>
                                    <?php }?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->
    </div><!--/row-->
</div><!--/.fluid-container-->

<!-- end: Content -->