<!-- start: Content -->


<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">


        <li>


            <i class="icon-home"></i>


            <a href="<?php echo base_url('dashboard')?>">Home</a>


            <i class="icon-angle-right"></i> 


        </li>


        <li>


            <i class="icon-edit"></i>


            <a href="<?php echo base_url('add/slider')?>">Add Banner Slider</a>


        </li>


    </ul>





    <div class="row-fluid sortable">


        <div class="box span12">


            <div class="box-header" data-original-title>


                <h2><i class="halflings-icon edit"></i><span class="break"></span>Add Banner Slider</h2>


                <div class="box-icon">


                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>


                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>


                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>


                </div>


            </div>


            <style type="text/css">


                #result{color:red;padding: 5px}


                #result p{color:red}


            </style>


            <div id="result"><?php echo $this->session->flashdata('message');?></div>


            <div class="box-content">


                <form class="form-horizontal" action="<?php echo base_url('save/slider');?>" method="post" enctype="multipart/form-data">


                    <fieldset>


                        <div class="control-group col-md-6 col-xs-12">


                            <label class="control-label" for="fileInput">Tên banner</label>


                            <div class="controls">


                                <input class="form-control" name="slider_title" type="text"/>


                            </div>


                        </div> 


                        


                        <div class="control-group col-md-6 col-xs-12">


                            <label class="control-label" for="fileInput">Banner <i style="color:#f00">(1600px x 450px)Dài x Cao</i></label>


                            <div class="controls">


                                <input class="form-control" name="slider_image" type="file"/>


                            </div>


                        </div>


                        <!--


                        <div class="control-group col-md-3 col-xs-12">


                            <label class="control-label" for="fileInput"> Banner Mobile  </label>


                            <div class="controls">


                                <input class="form-control" name="slider_image_moblie" type="file"/>


                            </div>


                        </div>


                        


                        <div class="control-group col-md-6 col-xs-12">


                            <label class="control-label" for="fileInput"> Upload video</label>


                            <input type="file" id="uploadVideoFile" name="slider_link_video" accept="video/*" />


                            <div id="videoSourceWrapper">


                                <video style="width: 100%;" controls>


                                    <source id="videoSource"/>


                                </video>


                                <div id="uploadVideoProgressBar" style="height: 5px; width: 1%; background: #2781e9; margin-top: -5px;"></div>   


                            </div>


                        </div>


                        -->


                        <div class="clear"></div>


                        
 <div class="control-group col-md-6 col-xs-12">


                            <label class="control-label" for="fileInput">Title</label>


                            <div class="controls">


                                <input class="form-control"  name="title" type="text"/>


                            </div>


                        </div>
                        <div class="control-group col-md-6 col-xs-12">


                            <label class="control-label" for="fileInput">Mô tả</label>


                            <div class="controls">
                                

                                <input class="form-control"  name="des" type="text"/>


                            </div>


                        </div>
                          <div class="control-group col-md-6 col-xs-12">


                            <label class="control-label" for="fileInput">Button</label>


                            <div class="controls">
                                

                                <input class="form-control"  name="button_text" type="text"/>


                            </div>


                        </div>

                        <div class="control-group col-md-6 col-xs-12">


                            <label class="control-label" for="fileInput">Link trang đích</label>


                            <div class="controls">


                                <input class="form-control"  name="slider_link" type="url"/>


                            </div>


                        </div>


                            


                        <div class="control-group col-md-6 col-xs-12">


                            <label class="control-label" for="textarea2">Trạng thái</label>


                            <div class="controls">


                                <select class="form-control" name="publication_status">


                                    <option value="1">Hiện</option>


                                    <option value="0">Ẩn</option>


                                </select>


                            </div>


                        </div>


                        


                        <div class="clear"></div>





                        <div class="control-group col-md-6 col-xs-12">


                            <button type="submit" id="save_category" class="btn btn-primary">Lưu thay đổi</button>


                            <button  class="btn"><a href="<?php echo base_url('manage/slider')?>">Hủy</a></button>


                        </div>


                    </fieldset>


                </form>   


            </div>


        </div><!--/span-->


    </div><!--/row-->


</div><!--/.fluid-container-->





<!-- end: Content -->


<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"> </script>





<script type="text/javascript">


    $(document).ready(function(){


        $("#videoSourceWrapper").hide();


    });








    $('#uploadVideoFile').on('change',


        function() {


            var fileInput = document.getElementById("uploadVideoFile");


            console.log('Trying to upload the video file: %O', fileInput);





            if ('files' in fileInput) {


                if (fileInput.files.length === 0) {


                    alert("Select a file to upload");


                } else {


                    var $source = $('#videoSource');


                    $source[0].src = URL.createObjectURL(this.files[0]);


                    $source.parent()[0].load();


                    $("#videoSourceWrapper").show();


                    UploadVideo(fileInput.files[0]);


                }


            } else {


                console.log('No found "files" property');


            }


        }


        );





    function UploadVideo(file) {


        var loaded = 0;


        var chunkSize = 500000;


        var total = file.size;


        var reader = new FileReader();


        var slice = file.slice(0, chunkSize);





    // Reading a chunk to invoke the 'onload' event


    reader.readAsBinaryString(slice); 


    console.log('Started uploading file "' + file.name + '"');


    $('#uploadVideoProgressBar').show();





    reader.onload = function (e) {


       //Just simulate API


       setTimeout(function(){


        loaded += chunkSize;


        var percentLoaded = Math.min((loaded / total) * 100, 100);


        console.log('Uploaded ' + Math.floor(percentLoaded) + '% of file "' + file.name + '"');


        $('#uploadVideoProgressBar').width(percentLoaded + "%");





        //Read the next chunk and call 'onload' event again


        if (loaded <= total) {


            slice = file.slice(loaded, loaded + chunkSize);


            reader.readAsBinaryString(slice);


        } else { 


            loaded = total;


            console.log('File "' + file.name + '" uploaded successfully!');


            $('#uploadVideoProgressBar').hide();


        }


    }, 250);


   }


}

















</script>