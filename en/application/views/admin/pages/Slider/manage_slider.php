<!-- start: Content -->
<div  class="col-md-10 col-xs-12">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard') ?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/brand') ?>">Manage Banner Slider</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="fa fa-book"></i><span class="break"></span>Manage Banner Slider</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="fa fa-cog fa-spin fa-1x fa-fw"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <?php if($role_user->role_add_slider==1){  ?>
                <div class="add_cate">
                    <a href="<?php echo base_url('add/slider')?>"> 
                        <i class="fa fa-plus-circle"></i>
                        <span class="hidden-tablet">Thêm</span>
                    </a>
                </div>
            <?php }?>

            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result"><?php echo $this->session->flashdata('message'); ?></div>

            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên banner</th>
                            <th>Hình ảnh</th>
                            <th>Hiển thị</th>
                            <th>Điều chỉnh</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($all_slider as $single_slider) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center">
                                    <i class="fa fa-cube"></i> <?php echo $single_slider->slider_title; ?>
                                </td>

                                <td class="center" style="width: 20%;">
                                    <?php if(empty($single_slider->slider_image)){?>
                                        <video width="400" controls>
                                            <source src="<?php echo base_url('uploads/video/'.$single_slider->slider_link_video);?>" type="video/mp4">
                                        </video>
                                    <?php } else {?>
                                        <img src="<?php echo base_url('uploads/'.$single_slider->slider_image);?>" style="max-height:200px"/>
                                    <?php } ?>
                                </td>
                                <td class="center">
                                    <?php if ($single_slider->publication_status == 1) { ?>
                                        <a class="btn btn-success" href="<?php echo base_url('unpublished/slider/' . $single_slider->slider_id); ?>">
                                            <i class="halflings-icon white thumbs-up"></i>  
                                        </a>
                                    <?php } 
                                    else {
                                    ?>
                                        <a class="btn btn-danger" href="<?php echo base_url('published/slider/' . $single_slider->slider_id); ?>">
                                            <i class="halflings-icon white thumbs-down"></i>  
                                        </a>
                                    <?php }
                                    ?>
                                </td>
                                
                                <td class="center">
                                    <?php if($role_user->role_edit_slider==1){  ?>
                                        <a class="btn btn-info" href="<?php echo base_url('edit/slider/' . $single_slider->slider_id); ?>">
                                            <i class="halflings-icon white edit"></i>
                                        </a>
                                    <?php }?>
                                    
                                    <?php if($role_user->role_delete_slider==1){  ?>
                                        <a class="btn btn-danger" href="<?php echo base_url('delete/slider/' . $single_slider->slider_id); ?>">
                                            <i class="halflings-icon white trash"></i> 
                                        </a>
                                    <?php }?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->
    </div><!--/row-->
</div><!--/.fluid-container-->
<!-- end: Content -->