<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard') ?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/commentnews') ?>">Manage comment news</a></li>
    </ul>

    <div class="row-fluid sortable">        
        <div class="box">
            <div class="box-header" data-original-title>
                <h2><i class="fa fa-commenting-o"></i><span class="break"></span>Manage comment news</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>

            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>

            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Nội dung</th>
                            <th>Trả lời</th>
                            <th>Name catelogy</th>
                            <th>Xem</th>
                            <th>action</th>

                        </tr>
                    </thead>   
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($all_comment as $single_comment) {
                            $i++;
                            ?>
                            <tr>
                                <td class="center">
                                    <?php echo $single_comment->comment_name; ?>
                                </td>
                                <td class="center"> <?php echo $single_comment->comment_email; ?></td> 
                                <td class="center"><i class="fa fa-comments"></i> <?php echo $single_comment->comment_content; ?>
                                      <?php if ($single_comment->comment_show == 1) { ?>
                                        <a class="btn btn-success" href="<?php echo base_url('unpublished/commentnews/' . $single_comment->cmt_id); ?>">
                                            <i class="halflings-icon white thumbs-up"></i>  
                                    </a>
                                    <?php } else {
                                        ?>
                                        <a class="btn btn-danger" href="<?php echo base_url('published/commentnews/' . $single_comment->cmt_id); ?>">
                                            <i class="halflings-icon white thumbs-down"></i>  
                                        </a>
                                        <?php }
                                    ?>
                            </td>
                                  <td class="center"><?php echo $single_comment->comment_answer; ?>
                                       <?php if ($single_comment->comment_show_answer == 1) { ?>
                                        <a class="btn btn-success" href="<?php echo base_url('unpublished_answer/commentnews/' . $single_comment->cmt_id); ?>">
                                            <i class="halflings-icon white thumbs-up"></i>  
                                    </a>
                                    
                                    <?php } else {
                                        ?>
                                        <a class="btn btn-danger" href="<?php echo base_url('published_answer/commentnews/' . $single_comment->cmt_id); ?>">
                                            <i class="halflings-icon white thumbs-down"></i>  
                                        </a>
                                        <?php }
                                    ?>

                                  </td>
                                <td class="center"><i class="fa fa-cube"></i> <?php echo $single_comment->dmtin_title; ?></td>
                                <td class="center"> <a class="i-target" target="target" 
                            href="<?php echo base_url($single_comment->dmtin_slug).'.html'?>"><i class="fa fa-eye"></i></a> </td>

                               
                                <td class="center" style="width: 100px;">
                                    <a class="btn btn-info" href="<?php echo base_url('edit/commentnews/' . $single_comment->cmt_id); ?>">
                                        <i class="halflings-icon white edit"></i>  
                                    </a>
                                    <a class="btn btn-danger" href="<?php echo base_url('delete/commentnews/' . $single_comment->cmt_id); ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->



    </div><!--/row-->



</div><!--/.fluid-container-->

<!-- end: Content -->