<!-- start: Content -->
<div class="col-md-10 col-xs-12">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashoard')?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="#">Dashboard</a></li>
    </ul>

    <div class="row-fluid">
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $( function() {
                var date = new Date();
                $('#datepicker').datepicker({
                    dateFormat: 'dd-mm-yy'
                });
            });
        </script>

        <?php
        /* Set the default timezone */
        date_default_timezone_set("America/Montreal");

        /* Set the date */
        $get_date= $this->input->get('datepicker');

        if($get_date!=''){
            $date = strtotime($get_date);
        } else {
            $date = strtotime(date('y-m-d'));
        } 

        $day = date('d', $date);
        $month = date('m', $date);
        $year = date('Y', $date);
        $firstDay = mktime(0,0,0,$month, 1, $year);
        $title = strftime('%B', $firstDay);
        $dayOfWeek = date('D', $firstDay);
         $daysInMonth = date('t', mktime(0, 0, 0, $month, 1, $year)); 
        /* Get the name of the week days */
        $timestamp = strtotime('next Sunday');
        $weekDays = array();
        for ($i = 0; $i < 7; $i++) {
            $weekDays[] = strftime('%a', $timestamp);
            $timestamp = strtotime('+1 day', $timestamp);
        }
        $blank = date('w', strtotime("{$year}-{$month}-01"));
        ?>
        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        <script type="text/javascript">
            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Thống kê truy cập tháng : <?php echo $month ?> - <?php echo $year ?> '
                },
                subtitle: {
                    text: 'Developer by : <a href="https://buffseo.com/">Buffseo </a>'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Số người truy cập'
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Tổng : <b>{point.y:.1f} Lượt truy cập</b>'
                },
                series: [{
                    name: 'Population',
                    data: [
                    <?php for($i = 1; $i <= 9; $i++):

                        $begin = $year.'-'.$month.'-0'.$i;

                        $query=$this->db->query("select count(*) as todayrecord from tbl_counter where tm='$begin'");
                        $todayrc=$query->row_array();
                        $today_visitors     =    $todayrc['todayrecord']; 

                        ?>
                        ['<?=$i?>', <?=$today_visitors?>],
                    <?php endfor; ?>

                    <?php for($j = 9; $j <= $daysInMonth; $j++):

                        $begin = $year.'-'.$month.'-'.$j;

                        $query=$this->db->query("select count(*) as todayrecord from tbl_counter where tm='$begin'");
                        $todayrc=$query->row_array();
                        $today_visitors     =    $todayrc['todayrecord']; 

                        ?>
                        ['<?=$j?>', <?=$today_visitors?>],
                    <?php endfor; ?>

                    ],
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });
        </script>

        <form name="supplier" class="form" method="get" enctype="multipart/form-data">
            <div class="widget">
                <div class="title">
                    <h6>Chào mừng bạn đến với Administrator - HỆ THỐNG QUẢN TRỊ NỘI DUNG WEBSITE - Powered by 
                        <a href="https://buffseo.com/" target="_blank">
                            <span style="color:#f00;">Buffseo</span>
                        </a>
                    </h6>
                    <div class="clear"></div>
                </div>
                <p>Nếu bạn có thắc mắc trong quá trình sử dụng, xin vui lòng gởi mail về địa chỉ <strong><a href="mailto:Buffseo.com@gmail.com">Buffseo.com@gmail.com</a></strong></p>

                <div class="clear"></div>

                <div class="formRow">
                    <label>Thống kê theo tháng</label>
                    <div class="formRight">
                        <input type="text" id="datepicker" name="datepicker" value="dd-mm-yyyy" >
                        <input type="submit" class="blueB xemthongke" value="Xem thống kê">
                    </div>
                </div>
                <div class="clear"></div>
                <!-- 2 columns widgets -->
            </div>
            <?php echo $today = date("F j, Y, g:i a");  ?>
        </form>
    </div>
</div>