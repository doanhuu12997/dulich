<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard') ?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/comment_news_rate') ?>">Manage đánh giá news</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="fa fa-book"></i><span class="break"></span>Manage đánh giá news</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="fa fa-cog fa-spin fa-1x fa-fw"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>

            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>

            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable" >
                    <thead>
                        <tr>
                            <th>Sr.</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Nội dung</th>
                            <th>Trả lời</th>
                            <th>Name product</th>
                            <th>Xem</th>
                            <th>Sao</th>
                            <th>action</th>

                        </tr>
                    </thead>   
                    <tbody>
                        <?php
                        $i= 0;
                        foreach ($all_comment_news_rate as $single_comment_news_rate) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center">
                                    <div class="media-left align-self-center">
                                    <img class="rounded-circle" src="https://gravatar.com/avatar/25b1fc64ba12614875c1e467d7e4c86e?s=512" alt=""/>
                                  </div><?php echo $single_comment_news_rate->comment_news_rate_name; ?>
                                </td>
                                <td class="center"><i class="fa fa-envelope-o"></i> <?php echo $single_comment_news_rate->comment_news_rate_email; ?></td>
                                <td class="center"><i class="fa fa-comment_news_rates"></i> <?php echo $single_comment_news_rate->comment_news_rate_content; ?></td>
                            <td class="center"> <?php echo $single_comment_news_rate->comment_news_rate_answer; ?>
                                 <?php if ($single_comment_news_rate->comment_news_rate_show_answer == 1) { ?>
                                        <a class="btn btn-success" href="<?php echo base_url('unpublished_answer/comment_news_rate/' . $single_comment_news_rate->comment_news_rate_id); ?>">
                                            <i class="halflings-icon white thumbs-up"></i>  
                                    </a>
                                    
                                    <?php } else {
                                        ?>
                                        <a class="btn btn-danger" href="<?php echo base_url('published_answer/comment_news_rate/' . $single_comment_news_rate->comment_news_rate_id); ?>">
                                            <i class="halflings-icon white thumbs-down"></i>  
                                        </a>
                                        <?php }
                                    ?>
                                
                            </td>

                                <td class="center"><i class="fa fa-cube"></i> <?php echo $single_comment_news_rate->dmtin_title; ?></td>
                                <td class="center"> <a class="i-target" target="target" 
                            href="<?php echo base_url($single_comment_news_rate->dmtin_slug).'.html'?>"><i class="fa fa-eye"></i></a> </td>

                                <td class="center"><?php echo $single_comment_news_rate->comment_news_rate_start; ?></td>
                               
                                <td class="center" style="width: 100px;">
                                     <?php if ($single_comment_news_rate->comment_news_rate_show == 1) { ?>
                                        <a class="btn btn-success" href="<?php echo base_url('unpublished/comment_news_rate/' . $single_comment_news_rate->comment_news_rate_id); ?>">
                                            <i class="halflings-icon white thumbs-up"></i>  
                                    </a>
                                    
                                    <?php } else {
                                        ?>
                                        <a class="btn btn-danger" href="<?php echo base_url('published/comment_news_rate/' . $single_comment_news_rate->comment_news_rate_id); ?>">
                                            <i class="halflings-icon white thumbs-down"></i>  
                                        </a>
                                        <?php }
                                    ?>
                                  <a class="btn btn-info" href="<?php echo base_url('edit/comment_news_rate/' . $single_comment_news_rate->comment_news_rate_id); ?>">
                                        <i class="halflings-icon white edit"></i>  
                                    </a>
                                    <a class="btn btn-danger" href="<?php echo base_url('delete/comment_news_rate/' . $single_comment_news_rate->comment_news_rate_id); ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->



    </div><!--/row-->



</div><!--/.fluid-container-->

<!-- end: Content -->