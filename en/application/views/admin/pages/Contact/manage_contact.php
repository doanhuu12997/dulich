<!-- start: Content -->
<div  class="col-md-10 col-xs-12">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard') ?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/brand') ?>">Manage contact</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="fa fa-book"></i><span class="break"></span>Manage contact</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="fa fa-cog fa-spin fa-1x fa-fw"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>

            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>

            <div class="box-content" style="height: 400px;">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Sr.</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Content</th>
                            <th>action</th>

                        </tr>
                    </thead>   
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($all_contact as $single_contact) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center"><div class="media-left align-self-center">
                                <img class="rounded-circle" src="https://randomuser.me/api/portraits/women/50.jpg">
                               </div><?php echo $single_contact->contact_name; ?></td>
                                <td class="center"><i class="fa fa-envelope-o"> <?php echo $single_contact->contact_email; ?></td>
                                <td class="center"><i class="fa fa-mobile"></i> <?php echo $single_contact->contact_phone; ?></td>
                                <td class="center"><?php echo $single_contact->contact_content; ?></td>

                               
                                <td class="center">
                                 <?php if($role_user->role_delete_contact==1){  ?>

                                    <a class="btn btn-danger" href="<?php echo base_url('delete/contact/' . $single_contact->contact_id); ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                     <?php }?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->



    </div><!--/row-->



</div><!--/.fluid-container-->

<!-- end: Content -->