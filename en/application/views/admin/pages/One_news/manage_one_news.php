<!-- start: Content -->
<div class="col-md-10 col-xs-12">
    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard') ?>">Home</a>
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/one_news') ?>">Manage 1 Bài viết</a></li>
    </ul>
    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Manage 1 Bài viết</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="fa fa-cog fa-spin fa-1x fa-fw"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <style type="text/css">
                #result {
                    color: red;
                    padding: 5px
                }

                #result p {
                    color: red
                }
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>
            <div class="box-content">

                <div class="add_cate"><a href="<?php echo base_url('add/one_news') ?>"><i class="fa fa-plus-circle"></i><span class="hidden-tablet"> Thêm </span></a></div>
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Stt.</th>
                            <th>Tên bài viết</th>
                            <th>Danh mục</th>
                            <!--  <th>Mô tả</th>-->
                            <th>Header</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($all_one_news as $single_one_news) {
                            $i++;
                        ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td style="width: 27%;" class="center"><i class="fa fa-cube"></i> <?php echo $single_one_news->one_news_title; ?></td>
                                <td style="width: 15%;" class="center"><?php if ($single_one_news->id_cate_page == 103) {
                                                                            echo "Chính sách";
                                                                        } else if ($single_one_news->id_cate_page == 104) {
                                                                            echo "Chương trình ưu đãi";
                                                                        } else if ($single_one_news->id_cate_page == 105) {
                                                                            echo "Video home";
                                                                        } else if ($single_one_news->id_cate_page == 106) {
                                                                            echo "Banner bottom home";
                                                                        } else if ($single_one_news->id_cate_page == 107) {
                                                                            echo "Gallery";
                                                                        } ?></td>
                                <!-- <td class="center"><i class="fa fa-pencil"></i> <?php echo $single_one_news->one_news_des; ?></td>
                        <td class="center"><img style="max-height: 200px;" src="<?php echo base_url('uploads/' . $single_one_news->one_news_image); ?>"/></td>-->
                                <td class="center">
                                    <?php if ($single_one_news->publication_header == 1) { ?>
                                        <a class="btn btn-success" href="<?php echo base_url('unpublished_header/one_news/' . $single_one_news->one_news_id); ?>">
                                            <i class="halflings-icon white thumbs-up"></i>
                                        </a>
                                    <?php } else {
                                    ?>
                                        <a class="btn btn-danger" href="<?php echo base_url('published_header/one_news/' . $single_one_news->one_news_id); ?>">
                                            <i class="halflings-icon white thumbs-down"></i>
                                        </a>
                                    <?php }
                                    ?>
                                </td>
                                <td class="center">
                                    <?php if ($single_one_news->publication_status == 1) { ?>
                                        <a class="btn btn-success" href="<?php echo base_url('unpublished/one_news/' . $single_one_news->one_news_id); ?>">
                                            <i class="halflings-icon white thumbs-up"></i>
                                        </a>
                                    <?php } else {
                                    ?>
                                        <a class="btn btn-danger" href="<?php echo base_url('published/one_news/' . $single_one_news->one_news_id); ?>">
                                            <i class="halflings-icon white thumbs-down"></i>
                                        </a>
                                    <?php }
                                    ?>
                                </td>
                                <td class="center" style="width:180px;">
                                    <a class="btn btn-info" href="<?php echo base_url('edit/one_news/' . $single_one_news->one_news_id); ?>">
                                        <i class="halflings-icon white edit"></i>
                                    </a>
                                    <a class="confirmClick btn btn-danger" href="<?php echo base_url('delete/one_news/' . $single_one_news->one_news_id); ?>">
                                        <i class="halflings-icon white trash"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div><!--/span-->
    </div><!--/row-->
</div><!--/.fluid-container-->
<!-- end: Content -->