<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard') ?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/brand') ?>">Quản lý Chính sách</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Quản lý Chính sách</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="fa fa-cog fa-spin fa-1x fa-fw"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>


         <div class="add_cate"><a href="<?php echo base_url('add/chinhsach')?>"> <i class="fa fa-plus-circle"></i><span class="hidden-tablet"> Thêm</span></a></div>


            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>

            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Sr.</th>
                            <th> Title</th>
                            <th> Des</th>
                           <th> Image</th>
                            <th> Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($all_chinhsach as $single_chinhsach) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                              <td class="center"><i class="fa fa-cube"></i> <?php echo $single_chinhsach->chinhsach_title; ?></td>
                                <td class="center" style="width: 50%;"><i class="fa fa-pencil"></i> <?php echo $single_chinhsach->chinhsach_des; ?></td>

                              <!--  <td class="center"><a target="_blank" href="<?php echo base_url($single_chinhsach->chinhsach_link);?>">Go To Link</a></td>-->
                        <td class="center"><img  style="max-height: 50px;" src="<?php echo base_url('uploads/'.$single_chinhsach->chinhsach_image);?>"/></td>
                                <td class="center">
                                     <?php if ($single_chinhsach->publication_status == 1) { ?>
                                        <a class="btn btn-success" href="<?php echo base_url('unpublished/chinhsach/' . $single_chinhsach->chinhsach_id); ?>">
                                            <i class="halflings-icon white thumbs-up"></i>  
                                        </a>
                                    <?php } else {
                                        ?>
                                        <a class="btn btn-danger" href="<?php echo base_url('published/chinhsach/' . $single_chinhsach->chinhsach_id); ?>">
                                            <i class="halflings-icon white thumbs-down"></i>  
                                        </a>
                                    <?php }
                                    ?>
                                </td>
                                <td class="center">
                                   

                                    <a class="btn btn-info" href="<?php echo base_url('edit/chinhsach/' . $single_chinhsach->chinhsach_id); ?>">
                                        <i class="halflings-icon white edit"></i>  
                                    </a>

                                    <a class="btn btn-danger" href="<?php echo base_url('delete/chinhsach/' . $single_chinhsach->chinhsach_id); ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

       

    </div><!--/row-->



</div><!--/.fluid-container-->

<!-- end: Content -->