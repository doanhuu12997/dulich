<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard')?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/product')?>">Quản lý đơn hàng</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="fa fa-book"></i><span class="break"></span>Quản lý đơn hàng</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="fa fa-cog fa-spin fa-1x fa-fw"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>
            
            <div class="box-content table-responsive">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th style="width: 3%;">STT</th>
                            <th style="width: 3%;">MÃ HD</th>
                             <th>Xem chi tiết</th>
                            <th style="width: 14%;">Họ tên</th>
                            <th>Email </th>
                            <th style="width: 8%;">Điện thoại</th>
                            <th style="width: 10%;">Ngày đặt</th>
                            <th style="width: 15%;">Ghi chú (bán hàng ghi)</th>
                            <th style="width: 10%;">Giá trị (vnđ)</th>
                            <th style="width: 10%;">Tình trạng</th>
                            <th>Xóa đơn hàng</th>

                        </tr>
                    </thead>   
                    <tbody>
                        <?php 
                        $i=0;
                        foreach($all_manage_order_info as $single_order){
                            $i++;
                            ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td><?php echo $single_order->order_id;?></td>
                               <td>
                                <?php if($role_user->role_edit_order==1){  ?>
                                <a class="btn btn-danger" href="<?php echo base_url('order/details/'.$single_order->order_id);?>"><i class="fa fa-eye"></i></a>
                                <?php }?>
                            </td>
                            <td>
                                 <!--<div class="media-left align-self-center">
                                    <img class="rounded-circle" src="https://gravatar.com/avatar/25b1fc64ba12614875c1e467d7e4c86e?s=512" alt="">
                                </div>-->
                                <?php echo $single_order->shipping_name?></td>
                            <td><!--<i class="fa fa-envelope-o">--></i> <?php echo $single_order->shipping_email?></td>
                            <td><!--<i class="fa fa-mobile"></i>--> <?php echo $single_order->shipping_phone?></td>
                        <td> <?=nice_date($single_order->order_day,'d-m-Y H:i:s');?></td>

                        <td><!--<i class="fa fa-pencil"></i>--> <?php echo $single_order->note?></td>
                          <td> <?php echo $this->cart->format_number($single_order->order_total)?></td>
                            <td>
                               <?php if($single_order->actions=="chưa thanh toán") { ?> <?php echo "<span class='xam'>Chưa thanh toán</span>" ;?><?php }  else if($single_order->actions=="Đã thanh toán") { ?> <?php echo "<span class='blue'>Đã thanh toán</span>" ;?><?php }  else if($single_order->actions=="Đơn hàng mới") { ?> <?php echo "<span class='red'>Đơn hàng mới</span>" ;?><?php } else if($single_order->actions=="Đang xử lý") { ?> <?php echo "<span class='black'>Đang xử lý</span>" ;?> <?php } else if($single_order->actions=="Hủy") { ?> <?php echo "<span class='red'>Hủy</span>";}?>
                             
                            </td>

                          

                              <td>
                                <a class="btn btn-danger" href="<?php echo base_url('delete/order/' . $single_order->shipping_id); ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                            </td>
                        </tr>
                        <?php }?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->

<!-- end: Content -->