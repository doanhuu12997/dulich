<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard')?>">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="<?php echo base_url('add/feel')?>">Thêm Cảm nhận</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Thêm Cảm nhận</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message');?></p>
            </div>
            <div class="box-content">
                <form class="form-horizontal" action="<?php echo base_url('save/feel');?>" method="post" enctype="multipart/form-data">
                    <fieldset>

                        <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput"> Tên</label>
                            <div class="controls">
                                <input class="form-control" name="feel_title" type="text"/>
                            </div>
                        </div> 

                        <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput"> Mô tả</label>
                            <div class="controls">
                                <textarea name="feel_des" rows="6" class="form-control i-des-sort"> </textarea>
                            </div>
                        </div> 
                        
                        <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput"> Image</label>
                            <div class="controls">
                                <input class="form-control" name="feel_image" type="file"/>
                            </div>
                        </div>
                        <!--
                         <div class="control-group">
                            <label class="control-label" for="fileInput">Feel Link</label>
                            <div class="controls">
                                <input class="span6 typeahead"  name="feel_link" type="url"/>
                            </div>
                        </div>-->
                        
                                
                        <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="textarea2">Trạng thái</label>
                            <div class="controls">
                                <select class="form-control" name="publication_status">
                                    <option value="1">Hiện</option>
                                    <option value="0">Ẩn</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="control-group col-md-12 col-xs-12">
                            <button type="submit" id="save_category" class="btn btn-primary">Lưu thay đổi</button>
                            <button  class="btn"><a href="<?php echo base_url('manage/feel')?>">Hủy</a></button>

                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

    
    
</div><!--/.fluid-container-->

<!-- end: Content -->