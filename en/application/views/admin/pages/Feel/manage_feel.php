<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard') ?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/brand') ?>">Quản lý Cảm nhận</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Quản lý Cảm nhận</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="fa fa-cog fa-spin fa-1x fa-fw"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>

              <?php if($role_user->role_add_feel==1){  ?>

         <div class="add_cate"><a href="<?php echo base_url('add/feel')?>"> <i class="fa fa-plus-circle"></i><span class="hidden-tablet"> Thêm</span></a></div>
         <?php }?>


            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>

            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Sr.</th>
                            <th> Title</th>
                            <th> Des</th>
                           <th> Image</th>
                            <th> Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($all_feel as $single_feel) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                              <td class="center"><i class="fa fa-cube"></i> <?php echo $single_feel->feel_title; ?></td>
                                <td class="center" style="width: 50%;"><i class="fa fa-pencil"></i> <?php echo $single_feel->feel_des; ?></td>

                              <!--  <td class="center"><a target="_blank" href="<?php echo base_url($single_feel->feel_link);?>">Go To Link</a></td>-->
                        <td class="center"><img  style="max-height: 50px;" src="<?php echo base_url('uploads/'.$single_feel->feel_image);?>"/></td>
                                <td class="center">
                                     <?php if ($single_feel->publication_status == 1) { ?>
                                        <a class="btn btn-success" href="<?php echo base_url('unpublished/feel/' . $single_feel->feel_id); ?>">
                                            <i class="halflings-icon white thumbs-up"></i>  
                                        </a>
                                    <?php } else {
                                        ?>
                                        <a class="btn btn-danger" href="<?php echo base_url('published/feel/' . $single_feel->feel_id); ?>">
                                            <i class="halflings-icon white thumbs-down"></i>  
                                        </a>
                                    <?php }
                                    ?>
                                </td>
                                <td class="center">
                                   
                                     <?php if($role_user->role_edit_feel==1){  ?>

                                    <a class="btn btn-info" href="<?php echo base_url('edit/feel/' . $single_feel->feel_id); ?>">
                                        <i class="halflings-icon white edit"></i>  
                                    </a>
                                  <?php }?>

                                   <?php if($role_user->role_delete_feel==1){  ?>
                                    <a class="btn btn-danger" href="<?php echo base_url('delete/feel/' . $single_feel->feel_id); ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                      <?php }?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

       

    </div><!--/row-->



</div><!--/.fluid-container-->

<!-- end: Content -->