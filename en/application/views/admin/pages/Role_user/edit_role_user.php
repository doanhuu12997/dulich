<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard')?>">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="<?php echo base_url('edit/role_user/'.$role_user_info_by_id->user_id)?>">Sửa Quyền user</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Sửa Quyền user</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message');?></p>
            </div>
            <div class="box-content">
                <form  name="formName" class="form-horizontal" action="<?php echo base_url('update/role_user/'.$role_user_info_by_id->user_id);?>" method="post"  enctype="multipart/form-data">
                    <fieldset>

                        <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput">User Name</label>
                            <div class="controls">
                                <input class="form-control" value="<?php echo $role_user_info_by_id->user_name;?>" id="user_name" name="user_name" type="text"/>
                            </div>
                        </div>  

                        <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput">User email</label>
                            <div class="controls">
                                <input class="form-control" value="<?php echo $role_user_info_by_id->user_email;?>" id="user_email" name="user_email" type="text"/>
                            </div>
                        </div>  
                        <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput">Password</label>
                            <div class="controls">
                                <input class="form-control" value="" id="user_password" name="user_password" type="Password"/>
                            </div>
                        </div>  
                
                        <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput"> Quyền</label>
                            <div class="controls">
                                <select class="form-control" id="user_role" name="user_role">
                                     <?php  
                                             $query =$this->db->query("select * from user_role where  role_id='".$role_user_info_by_id->user_role."'");
                                              $rs_quyen = $query->row();

 
                                         ?>
                                           <option value="<?php  if(empty($rs_quyen->role_id))  {?>
                                             <?php echo ""; ?>
                                              <?php } else  { echo $rs_quyen->role_id ;}?>
                                                ">
                                                <?php  if(empty($rs_quyen->role_id))  {?>
                                                  <?php echo "Chọn quyền"; ?><?php } else  { echo $rs_quyen->role_name ;}?>
                                           </option>      
                                    

                                    <?php foreach($get_role as $v){?>
                                    <option value="<?php echo $v->role_id;?>"><?php echo $v->role_name;?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div> 

                          <table class="table table-bordered table_role">
                        <thead>
                          <tr>
                            <th><i class="fa fa-book icon_role"></i>Name</th>
                            <th> <i class="fa fa-location-arrow icon_role"></i>Truy cập</th>
                            <th><i class="fa fa-cubes icon_role"></i>Thêm</th>
                            <th><i class="fa fa-pencil icon_role"></i>Sửa</th>
                            <th><i class="fa fa-trash icon_role"></i>Xóa</th>

                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Danh mục</td>
                            <td>  
                             <input class="" value="0" name="lv1" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="lv1" id="fileInput" type="radio" />   Yes
                            </td>
                            <td>  
                             <input class="" value="0" name="role_add_lv1" id="fileInput" type="radio" checked="checked"/> No
                            <input class="" value="1" name="role_add_lv1" id="fileInput" type="radio" />   Yes
                            </td>
                             <td>  
                             <input class="" value="0" name="role_edit_lv1" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_edit_lv1" id="fileInput" type="radio" />   Yes
                            </td>
                            <td>  
                             <input class="" value="0" name="role_delete_lv1" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_delete_lv1" id="fileInput" type="radio" />   Yes
                            </td>
                          </tr>
                        
                           <tr>
                            <td>Sản phẩm</td>
                            <td>  
                             <input class="" value="0" name="product" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="product" id="fileInput" type="radio" />   Yes
                            </td> 
                              <td>  
                             <input class="" value="0" name="role_add_product" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_add_product" id="fileInput" type="radio" />   Yes
                            </td>
                            <td>  
                             <input class="" value="0" name="role_edit_product" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_edit_product" id="fileInput" type="radio" />   Yes
                            </td>
                              <td>  
                             <input class="" value="0" name="role_delete_product" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_delete_product" id="fileInput" type="radio" />   Yes
                            </td>
                          </tr>

                       <tr>
                            <td>Thương hiệu</td>
                           <td>  
                             <input  value="0" name="thuonghieu"  type="radio" checked="true"/> No
                            <input  value="1" name="thuonghieu"  type="radio" />   Yes
                            </td> 
                               <td>  
                           
                            </td>
                              <td>  
                            
                            </td>
                              <td>  
                            </td>
                          </tr>


                             <tr>
                            <td>Thành phần</td>
                           <td>  
                             <input  value="0" name="thanhphan"  type="radio" checked="true"/> No
                            <input  value="1" name="thanhphan"  type="radio" />   Yes
                            </td> 
                               <td>  
                           
                            </td>
                              <td>  
                            
                            </td>
                              <td>  
                            </td>
                          </tr>

                           <tr>
                            <td>Điểm đến</td>
                           <td>  
                             <input  value="0" name="congdung"  type="radio" checked="true"/> No
                            <input  value="1" name="congdung"  type="radio" />   Yes
                            </td> 
                               <td>  
                           
                            </td>
                              <td>  
                            
                            </td>
                              <td>  
                            </td>
                          </tr>

                           <tr>
                            <td>Link 301</td>
                           <td>  
                             <input  value="0" name="xuatxu"  type="radio" checked="true"/> No
                            <input  value="1" name="xuatxu"  type="radio" />   Yes
                            </td> 
                               <td>  
                           
                            </td>
                              <td>  
                            
                            </td>
                              <td>  
                            </td>
                          </tr>

                           <tr>
                            <td>Tỉnh / Thành </td>
                           <td>  
                             <input  value="0" name="tinhthanh"  type="radio" checked="true"/> No
                            <input  value="1" name="tinhthanh"  type="radio" />   Yes
                            </td> 
                               <td>  
                           
                            </td>
                              <td>  
                            
                            </td>
                              <td>  
                            </td>
                          </tr>

                           <tr>
                            <td>Mã giảm giá </td>
                           <td>  
                             <input  value="0" name="magiamgia"  type="radio" checked="true"/> No
                            <input  value="1" name="magiamgia"  type="radio" />   Yes
                            </td> 
                               <td>  
                           
                            </td>
                              <td>  
                            
                            </td>
                              <td>  
                            </td>
                          </tr>

                           <tr>
                            <td>Pages </td>
                           <td>  
                             <input  value="0" name="page"  type="radio" checked="true"/> No
                            <input  value="1" name="page"  type="radio" />   Yes
                            </td> 
                               <td>  
                           
                            </td>
                              <td>  
                            
                            </td>
                              <td>  
                            </td>
                          </tr>
                          

                          
                          
                           <tr>
                            <td>Danh mục blog</td>
                           <td>  
                             <input class="" value="0" name="category_news" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="category_news" id="fileInput" type="radio" />   Yes
                            </td> 
                              <td>  
                             <input class="" value="0" name="role_add_category_news" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_add_category_news" id="fileInput" type="radio" />   Yes
                            </td>
                              <td>  
                             <input class="" value="0" name="role_edit_category_news" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_edit_category_news" id="fileInput" type="radio" />   Yes
                            </td>
                             <td>  
                             <input class="" value="0" name="role_delete_category_news" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_delete_category_news" id="fileInput" type="radio" />   Yes
                            </td>
                          </tr>

                           <tr>
                           <td>Bài viết blog</td>
                           <td>  
                             <input class="" value="0" name="news" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="news" id="fileInput" type="radio" />   Yes
                            </td> 
                              <td>  
                             <input class="" value="0" name="role_add_news" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_add_news" id="fileInput" type="radio" />   Yes
                            </td>
                           <td>  
                             <input class="" value="0" name="role_edit_news" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_edit_news" id="fileInput" type="radio" />   Yes
                            </td>
                             <td>  
                             <input class="" value="0" name="role_delete_news" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_delete_news" id="fileInput" type="radio" />   Yes
                            </td>
                          </tr>
                         <tr>
                            <td>Banner quảng cáo </td>
                           <td>  
                             <input  value="0" name="avd"  type="radio" checked="true"/> No
                            <input  value="1" name="avd"  type="radio" />   Yes
                            </td> 
                               <td>  
                           
                            </td>
                              <td>  
                            
                            </td>
                              <td>  
                            </td>
                          </tr>
                          <tr>
                            <td>Chính sách </td>
                           <td>  
                             <input  value="0" name="chinhsach"  type="radio" checked="true"/> No
                            <input  value="1" name="chinhsach"  type="radio" />   Yes
                            </td> 
                               <td>  
                           
                            </td>
                              <td>  
                            
                            </td>
                              <td>  
                            </td>
                          </tr>
                          
                          <tr>
                            <td>Đăng ký email </td>
                           <td>  
                             <input  value="0" name="dangkymail"  type="radio" checked="true"/> No
                            <input  value="1" name="dangkymail"  type="radio" />   Yes
                            </td> 
                               <td>  
                           
                            </td>
                              <td>  
                            
                            </td>
                              <td>  
                            </td>
                          </tr>
                          
                           <tr>
                            <td>Banner Sider</td>
                           <td>  
                             <input class="" value="0" name="slider" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="slider" id="fileInput" type="radio" />   Yes
                            </td> 
                              <td>  
                             <input class="" value="0" name="role_add_slider" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_add_slider" id="fileInput" type="radio" />   Yes
                            </td>
                             <td>  
                             <input class="" value="0" name="role_edit_slider" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_edit_slider" id="fileInput" type="radio" />   Yes
                            </td>
                              <td>  
                             <input class="" value="0" name="role_delete_slider" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_delete_slider" id="fileInput" type="radio" />   Yes
                            </td>
                          </tr>

                           <tr>
                            <td>Review Khách hàng</td>
                           <td>  
                             <input class="" value="0" name="feel" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="feel" id="fileInput" type="radio" />   Yes
                            </td> 
                              <td>  
                             <input class="" value="0" name="role_add_feel" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_add_feel" id="fileInput" type="radio" />   Yes
                            </td>
                              <td>  
                             <input class="" value="0" name="role_edit_feel" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_edit_feel" id="fileInput" type="radio" />   Yes
                            </td>
                           <td>  
                             <input class="" value="0" name="role_delete_feel" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_delete_feel" id="fileInput" type="radio" />   Yes
                            </td>
                          </tr>

                           <tr>
                            <td>Đối tác</td>
                           <td>  
                             <input class="" value="0" name="partner" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="partner" id="fileInput" type="radio" />   Yes
                            </td> 
                              <td>  
                             <input class="" value="0" name="role_add_partner" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_add_partner" id="fileInput" type="radio" />   Yes
                            </td>
                            <td>  
                             <input class="" value="0" name="role_edit_partner" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_edit_partner" id="fileInput" type="radio" />   Yes
                            </td>
                              <td>  
                             <input class="" value="0" name="role_delete_partner" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_delete_partner" id="fileInput" type="radio" />   Yes
                            </td>
                          </tr>
                           <tr>
                            <td>Thông tin công ty</td>
                            <td>  
                             <input class="" value="0" name="company" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="company" id="fileInput" type="radio" />   Yes
                            </td> 
                              <td>  
                             <input class="" value="0" name="role_add_company" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_add_company" id="fileInput" type="radio" />   Yes
                            </td>
                            <td>  
                             <input class="" value="0" name="role_edit_company" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_edit_company" id="fileInput" type="radio" />   Yes
                            </td>
                             <td>  
                             <input class="" value="0" name="role_delete_company" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_delete_company" id="fileInput" type="radio" />   Yes
                            </td>
                          </tr>

                           <tr>
                            <td>Feedback khách hàng</td>
                             <td>  
                             <input class="" value="0" name="contact" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="contact" id="fileInput" type="radio" />   Yes
                            </td> 
                              <td>  
                             <input class="" value="0" name="role_add_contact" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_add_contact" id="fileInput" type="radio" />   Yes
                            </td>
                             <td>  
                             <input class="" value="0" name="role_edit_contact" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_edit_contact" id="fileInput" type="radio" />   Yes
                            </td>
                             <td>  
                             <input class="" value="0" name="role_delete_contact" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_delete_contact" id="fileInput" type="radio" />   Yes
                            </td>
                          </tr>

                           <tr>
                            <td>Quản lý quyền</td>
                            <td>  
                             <input class="" value="0" name="role" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role" id="fileInput" type="radio" />   Yes
                            </td> 
                              <td>  
                             <input class="" value="0" name="role_add_role" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_add_role" id="fileInput" type="radio" />   Yes
                            </td>
                             <td>  
                             <input class="" value="0" name="role_edit_role" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_edit_role" id="fileInput" type="radio" />   Yes
                            </td>
                             <td>  
                             <input class="" value="0" name="role_delete_role" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_delete_role" id="fileInput" type="radio" />   Yes
                            </td>
                          </tr>

                           <tr>
                            <td>Quản lý User</td>
                            <td>  
                             <input class="" value="0" name="role_user" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_user" id="fileInput" type="radio" />   Yes
                            </td> 
                              <td>  
                             <input class="" value="0" name="role_add_role_user" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_add_role_user" id="fileInput" type="radio" />   Yes
                            </td>
                             <td>  
                             <input class="" value="0" name="role_edit_role_user" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_edit_role_user" id="fileInput" type="radio" />   Yes
                            </td>
                             <td>  
                             <input class="" value="0" name="role_delete_role_user" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_delete_role_user" id="fileInput" type="radio" />   Yes
                            </td>
                          </tr>

                           <tr>
                            <td>Đơn hàng</td>
                             <td>  
                             <input class="" value="0" name="order" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="order" id="fileInput" type="radio" />   Yes
                            </td> 
                             <td>  
                             <input class="" value="0" name="role_add_order" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_add_order" id="fileInput" type="radio" />   Yes
                            </td>
                            <td>  
                             <input class="" value="0" name="role_edit_order" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_edit_order" id="fileInput" type="radio" />   Yes
                            </td>
                            <td>  
                             <input class="" value="0" name="role_delete_order" id="fileInput" type="radio" checked="true"/> No
                            <input class="" value="1" name="role_delete_order" id="fileInput" type="radio" />   Yes
                            </td>
                          </tr>

                        
                        </tbody>
                      </table>

                        
                        <div class="form-actions">
                            <button type="submit" id="save_role_user" class="btn btn-primary">Save changes</button>
                            <button  class="btn"><a href="<?php echo base_url('manage/role_user')?>">Cancel</a></button>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

   
    
</div><!--/.fluid-container-->

<!-- end: Content -->

<script>
document.formName.lv1.value=<?php echo $role_user_info_by_id->lv1;?>;
document.formName.product.value=<?php echo $role_user_info_by_id->product;?>;
document.formName.category_news.value=<?php echo $role_user_info_by_id->category_news;?>;
document.formName.news.value=<?php echo $role_user_info_by_id->news;?>;
document.formName.slider.value=<?php echo $role_user_info_by_id->slider;?>;
document.formName.feel.value=<?php echo $role_user_info_by_id->feel;?>;
document.formName.partner.value=<?php echo $role_user_info_by_id->partner;?>;
document.formName.company.value=<?php echo $role_user_info_by_id->company;?>;
document.formName.contact.value=<?php echo $role_user_info_by_id->contact;?>;
document.formName.role.value=<?php echo $role_user_info_by_id->role;?>;
document.formName.role_user.value=<?php echo $role_user_info_by_id->role_user;?>;
document.formName.order.value=<?php echo $role_user_info_by_id->order;?>;
document.formName.thuonghieu.value=<?php echo $role_user_info_by_id->thuonghieu;?>;
document.formName.thanhphan.value=<?php echo $role_user_info_by_id->thanhphan;?>;
document.formName.congdung.value=<?php echo $role_user_info_by_id->congdung;?>;
document.formName.xuatxu.value=<?php echo $role_user_info_by_id->xuatxu;?>;
document.formName.magiamgia.value=<?php echo $role_user_info_by_id->magiamgia;?>;
document.formName.tinhthanh.value=<?php echo $role_user_info_by_id->tinhthanh;?>;
document.formName.page.value=<?php echo $role_user_info_by_id->page;?>;
document.formName.avd.value=<?php echo $role_user_info_by_id->avd;?>;
document.formName.chinhsach.value=<?php echo $role_user_info_by_id->chinhsach;?>;
document.formName.dangkymail.value=<?php echo $role_user_info_by_id->dangkymail;?>;

document.formName.role_add_lv1.value=<?php echo $role_user_info_by_id->role_add_lv1;?>;
document.formName.role_add_product.value=<?php echo $role_user_info_by_id->role_add_product;?>;
document.formName.role_add_category_news.value=<?php echo $role_user_info_by_id->role_add_category_news;?>;
document.formName.role_add_news.value=<?php echo $role_user_info_by_id->role_add_news;?>;
document.formName.role_add_slider.value=<?php echo $role_user_info_by_id->role_add_slider;?>;
document.formName.role_add_feel.value=<?php echo $role_user_info_by_id->role_add_feel;?>;
document.formName.role_add_partner.value=<?php echo $role_user_info_by_id->role_add_partner;?>;
document.formName.role_add_company.value=<?php echo $role_user_info_by_id->role_add_company;?>;
document.formName.role_add_contact.value=<?php echo $role_user_info_by_id->role_add_contact;?>;
document.formName.role_add_role.value=<?php echo $role_user_info_by_id->role_add_role;?>;
document.formName.role_add_role_user.value=<?php echo $role_user_info_by_id->role_add_role_user;?>;
document.formName.role_add_order.value=<?php echo $role_user_info_by_id->role_add_order;?>;


document.formName.role_edit_lv1.value=<?php echo $role_user_info_by_id->role_edit_lv1;?>;
document.formName.role_edit_product.value=<?php echo $role_user_info_by_id->role_edit_product;?>;
document.formName.role_edit_category_news.value=<?php echo $role_user_info_by_id->role_edit_category_news;?>;
document.formName.role_edit_news.value=<?php echo $role_user_info_by_id->role_edit_news;?>;
document.formName.role_edit_slider.value=<?php echo $role_user_info_by_id->role_edit_slider;?>;
document.formName.role_edit_feel.value=<?php echo $role_user_info_by_id->role_edit_feel;?>;
document.formName.role_edit_partner.value=<?php echo $role_user_info_by_id->role_edit_partner;?>;
document.formName.role_edit_company.value=<?php echo $role_user_info_by_id->role_edit_company;?>;
document.formName.role_edit_contact.value=<?php echo $role_user_info_by_id->role_edit_contact;?>;
document.formName.role_edit_role.value=<?php echo $role_user_info_by_id->role_edit_role;?>;
document.formName.role_edit_role_user.value=<?php echo $role_user_info_by_id->role_edit_role_user;?>;
document.formName.role_edit_order.value=<?php echo $role_user_info_by_id->role_edit_order;?>;

document.formName.role_delete_product.value=<?php echo $role_user_info_by_id->role_delete_product;?>;


document.formName.role_delete_lv1.value=<?php echo $role_user_info_by_id->role_delete_lv1;?>;
document.formName.role_delete_category_news.value=<?php echo $role_user_info_by_id->role_delete_category_news;?>;
document.formName.role_delete_news.value=<?php echo $role_user_info_by_id->role_delete_news;?>;
document.formName.role_delete_slider.value=<?php echo $role_user_info_by_id->role_delete_slider;?>;
document.formName.role_delete_feel.value=<?php echo $role_user_info_by_id->role_delete_feel;?>;
document.formName.role_delete_partner.value=<?php echo $role_user_info_by_id->role_delete_partner;?>;
document.formName.role_delete_company.value=<?php echo $role_user_info_by_id->role_delete_company;?>;
document.formName.role_delete_contact.value=<?php echo $role_user_info_by_id->role_delete_contact;?>;
document.formName.role_delete_role.value=<?php echo $role_user_info_by_id->role_delete_role;?>;
document.formName.role_delete_role_user.value=<?php echo $role_user_info_by_id->role_delete_role_user;?>;
document.formName.role_delete_order.value=<?php echo $role_user_info_by_id->role_delete_order;?>;


document.getElementById('user_role').value = <?php echo $role_user_info_by_id->user_role;?>;

</script>

