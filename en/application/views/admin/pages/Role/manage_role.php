<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard') ?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/role') ?>">Manage role</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Manage role</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="fa fa-cog fa-spin fa-1x fa-fw"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <style type="text/css">
                #result{color:red;padding:5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Sr.</th>
                            <th>role Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($all_categroy as $single_role) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><i class="fa fa-user"></i>
                                    <?php echo $single_role->role_name ?></td>
                                <td class="center">
                                    <?php if($role_user->role_edit_role==1){  ?>

                                    <a class="btn btn-info" href="<?php echo base_url('edit/role/' . $single_role->role_id); ?>">
                                        <i class="halflings-icon white edit"></i>  
                                    </a>
                                    <?php }?>

                                    <?php if($role_user->role_delete_role==1){  ?>

                                    <a class="btn btn-danger" href="<?php echo base_url('delete/role/' . $single_role->role_id); ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                    <?php }?>

                                </td>

                            </tr>
                        <?php } ?>
                    </tbody>
                </table>            
            </div>

        </div><!--/span-->
<?php if($role_user->role_add_role==1){  ?>

 <div style="float: left;"><a href="<?php echo base_url('add/role')?>"><i class="icon-envelope"></i><span class="hidden-tablet"> Add role</span></a></div>
<?php }?>
    </div><!--/row-->



</div><!--/.fluid-container-->

<!-- end: Content -->