<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard')?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/link301')?>">Quản lý Link 301</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header">
                <h2><i class="halflings-icon user"></i><span class="break"></span>Quản lý Link 301</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>
             <div class="add_cate"><a href="<?php echo base_url('add/link301')?>"><i class="fa fa-plus-circle"></i><span class="hidden-tablet"> Thêm</span></a></div>
            
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>STT.</th>
                            <th>Link nguồn</th>
                            <th>Link đích</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php 
                        $i=0;
                            foreach($all_link301 as $single_link301){
                                $i++;
                        ?>
                        <tr>
                            <td><?php echo $i;?></td>
                            <td class="center"><?php echo $single_link301->link301_nguon;?></td>
                            <td class="center"><?php echo $single_link301->link301_dich;?></td>

                            <td class="center">
                                    <?php if ($single_link301->publication_status == 1) { ?>
                                        <span class="label label-success">Yes</span>
                                    <?php } else {
                                        ?>
                                        <span class="label label-danger" style="background:red">No</span>
                                        <?php }
                                    ?>
                                </td>
                                <td class="center">
                                    <?php if ($single_link301->publication_status == 0) { ?>
                                        <a class="btn btn-success" href="<?php echo base_url('published/link301/' . $single_link301->link301_id); ?>">
                                            <i class="halflings-icon white thumbs-up"></i>  
                                        </a>
                                    <?php } else {
                                        ?>
                                        <a class="btn btn-danger" href="<?php echo base_url('unpublished/link301/' . $single_link301->link301_id); ?>">
                                            <i class="halflings-icon white thumbs-down"></i>  
                                        </a>
                                        <?php }
                                    ?>

                                    <a class="btn btn-info" href="<?php echo base_url('edit/link301/' . $single_link301->link301_id); ?>">
                                        <i class="halflings-icon white edit"></i>  
                                    </a>
                                    <a class="confirmClick btn btn-danger" href="<?php echo base_url('delete/link301/' . $single_link301->link301_id); ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                </td>
                        </tr>
                            <?php }?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->
                                   



    </div><!--/row-->



</div><!--/.fluid-container-->

<!-- end: Content -->