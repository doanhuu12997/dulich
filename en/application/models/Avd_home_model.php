<?php

class Avd_home_model extends CI_Model{
    
    public function save_avd_home_info($data){
        return $this->db->insert('tbl_avd_home', $data);
    }
    
    public function getall_avd_home_info(){
        $this->db->select('*');
        $this->db->from('tbl_avd_home');
        $info = $this->db->get();
        return $info->result();
    }
    
    
    public function edit_avd_home_info($id){
        $this->db->select('*');
        $this->db->from('tbl_avd_home');
        $this->db->where('avd_home_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function delete_avd_home_info($id){
        $this->db->where('avd_home_id', $id);
        return $this->db->delete('tbl_avd_home');
    }
    
    public function update_avd_home_info($data,$id){
        $this->db->where('avd_home_id', $id);
        return $this->db->update('tbl_avd_home', $data);
    }
    
    public function published_avd_home_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('avd_home_id', $id);
        return $this->db->update('tbl_avd_home');
    }
    
    public function unpublished_avd_home_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('avd_home_id', $id);
        return $this->db->update('tbl_avd_home');
    }
    
}