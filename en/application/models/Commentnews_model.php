<?php

class Commentnews_model extends CI_Model{
    
    public function save_comment_info($data){
        return $this->db->insert('tbl_comment_news', $data);
    }
    
    public function getall_comment_info(){
        $this->db->select('*');
        $this->db->from('tbl_comment_news');
        $this->db->join('tbl_dmtin','tbl_dmtin.dmtin_id=tbl_comment_news.comment_news_id');
        $info = $this->db->get();
        return $info->result();
    }


     public function edit_comment_info($id){
        $this->db->select('*');
        $this->db->from('tbl_comment_news');
        $this->db->where('cmt_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
        public function update_comment_info($data,$id){
        $this->db->where('cmt_id', $id);
        return $this->db->update('tbl_comment_news', $data);
    }

        public function comment_show_answer($id) {
        $this->db->set('comment_show_answer', 1);
        $this->db->where('cmt_id', $id);
        return $this->db->update('tbl_comment_news');
    }
    
        public function un_comment_show_answer($id) {
        $this->db->set('comment_show_answer', 0);
        $this->db->where('cmt_id', $id);
        return $this->db->update('tbl_comment_news');
    }
    
    
    public function delete_comment_info($id){
        $this->db->where('cmt_id', $id);
        return $this->db->delete('tbl_comment_news');
    }
    

    public function published_comment_info($id) {
        $this->db->set('comment_show', 1);
        $this->db->where('cmt_id', $id);
        return $this->db->update('tbl_comment_news');
    }
    
    public function unpublished_comment_info($id) {
        $this->db->set('comment_show', 0);
        $this->db->where('cmt_id', $id);
        return $this->db->update('tbl_comment_news');
    }
    
}