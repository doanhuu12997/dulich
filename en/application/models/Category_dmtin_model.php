<?php





class Category_dmtin_model extends CI_Model{


    public function save_category_dmtin_info($data){


        return $this->db->insert('tbl_category_dmtin', $data);


    }


    


    public function getall_category_dmtin_info(){


        $this->db->select('*');


        $this->db->from('tbl_category_dmtin');


        $this->db->where('tbl_category_dmtin.parent_id',0);


        $info = $this->db->get();


        return $info->result();


    }

       public function getall_category(){
        $this->db->select('*');
        $this->db->from('tbl_category_dmtin');
        $this->db->where('parent_id',0);
        $this->db->where('publication_status',1);
        $this->db->order_by('category_stt','asc');
              $parent = $this->db->get();
        
        $categories = $parent->result();
        $i=0;
        foreach($categories as $main_cat){
 
            $categories[$i]->sub = $this->sub_categorydmtin($main_cat->id);
            $i++;
        }
        return $categories;
     
    }
        public function sub_categorydmtin($id){
        $this->db->select('*');
        $this->db->from('tbl_category_dmtin');
        $this->db->where('parent_id', $id);
       

        $child = $this->db->get();
        $categoriesdmtin = $child->result();
        $i=0;
        foreach($categoriesdmtin as $sub_cat){
 
            $categoriesdmtin[$i]->sub = $this->sub_categorydmtin($sub_cat->id);
            $i++;
        }
        return $categoriesdmtin;       
    }
    


    


    public function edit_category_dmtin_info($id){


        $this->db->select('*');


        $this->db->from('tbl_category_dmtin');


        $this->db->where('id',$id);


        $info = $this->db->get();


        return $info->row();


    }


    


    public function delete_category_dmtin_info($id){


        $this->db->where('id', $id);


        return $this->db->delete('tbl_category_dmtin');


    }


    


    public function update_category_dmtin_info($data,$id){


        $this->db->where('id', $id);


        return $this->db->update('tbl_category_dmtin', $data);


    }


    


    public function published_category_dmtin_info($id) {


        $this->db->set('publication_status', 1);


        $this->db->where('id', $id);


        return $this->db->update('tbl_category_dmtin');


    }


    


    public function unpublished_category_dmtin_info($id) {


        $this->db->set('publication_status', 0);


        $this->db->where('id', $id);


        return $this->db->update('tbl_category_dmtin');


    }





    public function published_category_dmtin_header($id) {


        $this->db->set('publication_header', 1);


        $this->db->where('id', $id);


        return $this->db->update('tbl_category_dmtin');


    }


    


    public function unpublished_category_dmtin_header($id) {


        $this->db->set('publication_header', 0);


        $this->db->where('id', $id);


        return $this->db->update('tbl_category_dmtin');


    }


    


}