<?php

class Regiter_mail_model extends CI_Model{
    
    public function save_regiter_mail_info($data){
        return $this->db->insert('tbl_regiter_mail', $data);
    }
    
    public function getall_regiter_mail_info(){
        $this->db->select('*');
        $this->db->from('tbl_regiter_mail');
        $info = $this->db->get();
        return $info->result();
    }
    

    
    public function delete_regiter_mail_info($id){
        $this->db->where('regiter_mail_id', $id);
        return $this->db->delete('tbl_regiter_mail');
    }
    

    public function published_regiter_mail_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('regiter_mail_id', $id);
        return $this->db->update('tbl_regiter_mail');
    }
    
    public function unpublished_regiter_mail_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('regiter_mail_id', $id);
        return $this->db->update('tbl_regiter_mail');
    }
    
}