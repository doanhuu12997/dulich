<?php

class Origin_model extends CI_Model{
    
    public function save_origin_info($data){
        return $this->db->insert('tbl_origin', $data);
    }
    
    public function getall_origin_info(){
        $this->db->select('*');
        $this->db->from('tbl_origin');
        $info = $this->db->get();
        return $info->result();
    }
    
    
    public function edit_origin_info($id){
        $this->db->select('*');
        $this->db->from('tbl_origin');
        $this->db->where('origin_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function delete_origin_info($id){
        $this->db->where('origin_id', $id);
        return $this->db->delete('tbl_origin');
    }
    
    public function update_origin_info($data,$id){
        $this->db->where('origin_id', $id);
        return $this->db->update('tbl_origin', $data);
    }
    
    public function published_origin_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('origin_id', $id);
        return $this->db->update('tbl_origin');
    }
    
    public function unpublished_origin_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('origin_id', $id);
        return $this->db->update('tbl_origin');
    }
    
}