<?php





class Hotline_model extends CI_Model
{


    public function save_hotline_info($data)
    {


        return $this->db->insert('tbl_hotline', $data);
    }





    public function getall_hotline_info()
    {


        $this->db->select('*');


        $this->db->from('tbl_hotline');

        $this->db->order_by('tbl_hotline.location', 'ASC');


        $info = $this->db->get();


        return $info->result();
    }



    public function edit_hotline_info($id)
    {


        $this->db->select('*');


        $this->db->from('tbl_hotline');


        $this->db->where('id', $id);


        $info = $this->db->get();


        return $info->row();
    }





    public function delete_hotline_info($id)
    {


        $this->db->where('id', $id);


        return $this->db->delete('tbl_hotline');
    }





    public function update_hotline_info($data, $id)
    {


        $this->db->where('id', $id);


        return $this->db->update('tbl_hotline', $data);
    }





    public function published_hotline_info($id)
    {


        $this->db->set('publication_status', 1);


        $this->db->where('id', $id);


        return $this->db->update('tbl_hotline');
    }





    public function unpublished_hotline_info($id)
    {


        $this->db->set('publication_status', 0);


        $this->db->where('id', $id);


        return $this->db->update('tbl_hotline');
    }
    public function get_user()
    {





        $email = $this->session->userdata('user_email');


        $name = $this->session->userdata('user_name');


        $id = $this->session->userdata('user_id');





        if ($email == false) {


            redirect('admin');
        }
    }
}
