<?php

class Role_model extends CI_Model{
    public function save_role_info($data){
        return $this->db->insert('user_role', $data);
    }
    
    public function getall_role_info(){
        $this->db->select('*');
        $this->db->from('user_role');
        $info = $this->db->get();
        return $info->result();
    }
    
    
    public function edit_role_info($id){
        $this->db->select('*');
        $this->db->from('user_role');
        $this->db->where('role_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function delete_role_info($id){
        $this->db->where('role_id', $id);
        return $this->db->delete('user_role');
    }
    
    public function update_role_info($data,$id){
        $this->db->where('role_id', $id);
        return $this->db->update('user_role', $data);
    }

    
}