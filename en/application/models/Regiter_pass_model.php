<?php

class Regiter_pass_model extends CI_Model{
    
    public function save_regiter_pass_info($data){
        return $this->db->insert('tbl_regiter_pass', $data);
    }
    
    public function getall_regiter_pass_info(){
        $this->db->select('*');
        $this->db->from('tbl_regiter_pass');
        $info = $this->db->get();
        return $info->result();
    }
    

    
    public function delete_regiter_pass_info($id){
        $this->db->where('regiter_pass_id', $id);
        return $this->db->delete('tbl_regiter_pass');
    }
    

    public function published_regiter_pass_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('regiter_pass_id', $id);
        return $this->db->update('tbl_regiter_pass');
    }
    
    public function unpublished_regiter_pass_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('regiter_pass_id', $id);
        return $this->db->update('tbl_regiter_pass');
    }
    
}