<?php

class Request_model extends CI_Model{
    
    public function save_request_info($data){
        return $this->db->insert('tbl_request', $data);
    }
    
    public function getall_request_info(){
        $this->db->select('*');
        $this->db->from('tbl_request');
        $this->db->join('tbl_product','tbl_product.product_id=tbl_request.request_product_id');
        $info = $this->db->get();
        return $info->result();
    }
    
    
    public function delete_request_info($id){
        $this->db->where('request_id', $id);
        return $this->db->delete('tbl_request');
    }
    

    public function published_request_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('request_id', $id);
        return $this->db->update('tbl_request');
    }
    
    public function unpublished_request_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('request_id', $id);
        return $this->db->update('tbl_request');
    }
    
}