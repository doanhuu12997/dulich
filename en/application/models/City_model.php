<?php

class city_model extends CI_Model{
    public function save_city_info($data){
        return $this->db->insert('tbl_city', $data);
    }
    
   public function getall_city_info(){
        $this->db->select('*');
        $this->db->from('tbl_city');
        $this->db->order_by('tbl_city.city_stt','asc');
        $info = $this->db->get();
        return $info->result();
    }
    
    
    public function edit_city_info($id){
        $this->db->select('*');
        $this->db->from('tbl_city');
        $this->db->where('id',$id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function delete_city_info($id){
        $this->db->where('id', $id);
        return $this->db->delete('tbl_city');
        
    }
    
    public function update_city_info($data,$id){
        $this->db->where('id', $id);
        return $this->db->update('tbl_city', $data);
    }

   
    public function published_city_info($id) {
        $this->db->set('cate_status', 1);
        $this->db->where('id', $id);
        return $this->db->update('tbl_city');
    }
    
    public function unpublished_city_info($id) {
        $this->db->set('cate_status', 0);
        $this->db->where('id', $id);
        return $this->db->update('tbl_city');
    }

    
}