<?php





class One_news_model extends CI_Model{


    


    public function save_one_news_info($data){


        return $this->db->insert('tbl_one_news', $data);


    }


    


    public function getall_one_news_info(){


        $this->db->select('*');


        $this->db->from('tbl_one_news');


        $info = $this->db->get();


        return $info->result();


    }


    


    


    public function edit_one_news_info($id){


        $this->db->select('*');


        $this->db->from('tbl_one_news');


        $this->db->where('one_news_id',$id);


        $info = $this->db->get();


        return $info->row();


    }


    


    public function delete_one_news_info($id){


        $this->db->where('one_news_id', $id);


        return $this->db->delete('tbl_one_news');


    }


    


    public function update_one_news_info($data,$id){


        $this->db->where('one_news_id', $id);


        return $this->db->update('tbl_one_news', $data);


    }





    public function published_header($id) {


        $this->db->set('publication_header', 1);


        $this->db->where('one_news_id', $id);


        return $this->db->update('tbl_one_news');


    }


    


    public function unpublished_header($id) {


        $this->db->set('publication_header', 0);


        $this->db->where('one_news_id', $id);


        return $this->db->update('tbl_one_news');


    }


    


    public function published_one_news_info($id) {


        $this->db->set('publication_status', 1);


        $this->db->where('one_news_id', $id);


        return $this->db->update('tbl_one_news');


    }


    


    public function unpublished_one_news_info($id) {


        $this->db->set('publication_status', 0);


        $this->db->where('one_news_id', $id);


        return $this->db->update('tbl_one_news');


    }


    


}