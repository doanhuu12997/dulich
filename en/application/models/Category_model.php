<?php





class Category_model extends CI_Model
{


    public function save_category_info($data)
    {


        return $this->db->insert('tbl_category', $data);
    }





    public function getcategory()
    {





        $this->db->select('*');


        $this->db->from('tbl_category');


        $this->db->where('parent_id', 0);


        $this->db->order_by('id', 'asc');





        $parent = $this->db->get();





        $categories = $parent->result();


        $i = 0;


        foreach ($categories as $main_cat) {





            $categories[$i]->sub = $this->sub_category($main_cat->id);


            $i++;
        }


        return $categories;
    }














    public function sub_category($id)
    {





        $this->db->select('*');


        $this->db->from('tbl_category');


        $this->db->where('parent_id', $id);


        $this->db->order_by('id', 'asc');





        $child = $this->db->get();


        $categories = $child->result();


        $i = 0;


        foreach ($categories as $sub_cat) {





            $categories[$i]->sub = $this->sub_category($sub_cat->id);


            $i++;
        }


        return $categories;
    }





    public function sub_category_edit($id)
    {





        $this->db->select('*');


        $this->db->from('tbl_category');


        $this->db->where('parent_id', $id);


        $this->db->where('cate_status', 1);


        $this->db->order_by('id', 'asc');





        $child = $this->db->get();


        $categories = $child->result();








        return $categories;
    }





    public function getCategories($cat_id)
    {


        $this->db->select('*');


        $this->db->from('tbl_category');


        $this->db->where('cate_status', 1);


        $this->db->or_where('parent_id', $cat_id);


        $this->db->order_by('id', 'asc');





        $info = $this->db->get();


        return $info->result();
    }


    public function edit_category_info($id)
    {


        $this->db->select('*');


        $this->db->from('tbl_category');


        $this->db->where('id', $id);


        $info = $this->db->get();


        return $info->row();
    }





    public function delete_category_info($id)
    {


        $this->db->where('id', $id);


        return $this->db->delete('tbl_category');
    }





    public function update_category_info($data, $id)
    {


        $this->db->where('id', $id);


        return $this->db->update('tbl_category', $data);
    }





    public function published_home_category($id)
    {


        $this->db->set('publication_home', 1);


        $this->db->where('id', $id);


        return $this->db->update('tbl_category');
    }





    public function unpublished_home_category($id)
    {


        $this->db->set('publication_home', 0);


        $this->db->where('id', $id);


        return $this->db->update('tbl_category');
    }











    public function published_header_category_info($id)
    {


        $this->db->set('publication_header', 1);


        $this->db->where('id', $id);


        return $this->db->update('tbl_category');
    }





    public function unpublished_header_category_info($id)
    {


        $this->db->set('publication_header', 0);


        $this->db->where('id', $id);


        return $this->db->update('tbl_category');
    }





    public function published_footer_category_info($id)
    {


        $this->db->set('publication_footer', 1);


        $this->db->where('id', $id);


        return $this->db->update('tbl_category');
    }





    public function unpublished_footer_category_info($id)
    {


        $this->db->set('publication_footer', 0);


        $this->db->where('id', $id);


        return $this->db->update('tbl_category');
    }





    public function published_category_info($id)
    {


        $this->db->set('cate_status', 1);


        $this->db->where('id', $id);


        return $this->db->update('tbl_category');
    }





    public function unpublished_category_info($id)
    {


        $this->db->set('cate_status', 0);


        $this->db->where('id', $id);


        return $this->db->update('tbl_category');
    }





    public function get_all_published_brand()
    {


        $this->db->select('*');


        $this->db->from('tbl_brand');


        $this->db->where('brand_publication_status', 1);


        $info = $this->db->get();


        return $info->result();
    }
}
