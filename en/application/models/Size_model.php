<?php

class Size_model extends CI_Model{
    public function save_size_info($data){
        return $this->db->insert('tbl_size', $data);
    }
    
    public function getall_size_info(){
        $this->db->select('*');
        $this->db->from('tbl_size');
        $info = $this->db->get();
        return $info->result();
    }
    
    
    public function edit_size_info($id){
        $this->db->select('*');
        $this->db->from('tbl_size');
        $this->db->where('size_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function delete_size_info($id){
        $this->db->where('size_id', $id);
        return $this->db->delete('tbl_size');
        
    }
    
    public function update_size_info($data,$id){
        $this->db->where('size_id', $id);
        return $this->db->update('tbl_size', $data);
    }

   
    public function published_size_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('size_id', $id);
        return $this->db->update('tbl_size');
    }
    
    public function unpublished_size_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('size_id', $id);
        return $this->db->update('tbl_size');
    }
    
}