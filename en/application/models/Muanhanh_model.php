<?php

class muanhanh_model extends CI_Model{
    
    public function save_muanhanh_info($data){
        return $this->db->insert('tbl_muanhanh', $data);
    }
    
    public function getall_muanhanh_info(){
        $this->db->select('*');
        $this->db->from('tbl_muanhanh');
        $this->db->join('tbl_product','tbl_product.product_id=tbl_muanhanh.muanhanh_product_id');
        $info = $this->db->get();
        return $info->result();
    }


     public function edit_muanhanh_info($id){
        $this->db->select('*');
        $this->db->from('tbl_muanhanh');
        $this->db->where('muanhanh_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
        public function update_muanhanh_info($data,$id){
        $this->db->where('muanhanh_id', $id);
        return $this->db->update('tbl_muanhanh', $data);
    }

        
    public function delete_muanhanh_info($id){
        $this->db->where('muanhanh_id', $id);
        return $this->db->delete('tbl_muanhanh');
    }
    

    public function published_muanhanh_info($id) {
        $this->db->set('muanhanh_show', 1);
        $this->db->where('muanhanh_id', $id);
        return $this->db->update('tbl_muanhanh');
    }
    
    public function unpublished_muanhanh_info($id) {
        $this->db->set('muanhanh_show', 0);
        $this->db->where('muanhanh_id', $id);
        return $this->db->update('tbl_muanhanh');
    }
    
}