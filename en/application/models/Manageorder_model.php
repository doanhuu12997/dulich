<?php

class Manageorder_model extends CI_Model{
    
     
    public function manage_order_info(){
        $this->db->select('*');
        $this->db->from('tbl_order');
        $this->db->join('tbl_shipping', 'tbl_shipping.shipping_id = tbl_order.shipping_id','inner');
        $this->db->order_by('tbl_order.order_id', 'DESC');
        $result = $this->db->get();
        return $result->result();
    }

    public function manage_check_hd($mahd){
    $this->db->select('*');
    $this->db->from('tbl_order');
    $this->db->join('tbl_shipping', 'tbl_shipping.shipping_id = tbl_order.shipping_id','inner');
    $this->db->where('tbl_order.order_id',$mahd);

    $this->db->order_by('tbl_order.order_id', 'DESC');
    $result = $this->db->get();
    return $result->result();
    }
   public function get_count_order(){
        $this->db->select('count(*) as total');
        $this->db->from('tbl_order');
        $this->db->join('tbl_shipping', 'tbl_shipping.shipping_id = tbl_order.shipping_id','inner');
        $this->db->order_by('tbl_order.order_id', 'DESC');
        $this->db->where_in('tbl_order.actions','Đơn hàng mới');
        $result = $this->db->get();
        return $result->row();
    }

 
    public function delete_order_info($id)
    {
          $this->db->where('shipping_id',$id);
          return $this->db->delete('tbl_shipping');
    }
    
    public function update_status_order($data,$id){
        $this->db->where('order_id', $id);
        return $this->db->update('tbl_order', $data);
    }

    public function order_info_by_id($order_id){
        $this->db->select('*');
        $this->db->from('tbl_order');
        $this->db->join('tbl_city', 'tbl_city.id = tbl_order.city_id','left');
        $this->db->join('tbl_customer', 'tbl_customer.customer_id = tbl_order.cus_id','left');
        $this->db->where('order_id',$order_id);
        $result = $this->db->get();
        return $result->row();
    }
    
    public function customer_info_by_id($custoemr_id){
        $this->db->select('*');
        $this->db->from('tbl_customer');
        $this->db->where('customer_id',$custoemr_id);
        $result = $this->db->get();
        return $result->row();
    }
    
    public function shipping_info_by_id($shipping_id){
        $this->db->select('*');
        $this->db->from('tbl_shipping');
        $this->db->where('shipping_id',$shipping_id);
        $result = $this->db->get();
        return $result->row();
    }

       public function get_infor_city($shipping_id){
        $this->db->select('*');
        $this->db->from('tbl_shipping');
        $this->db->join('tbl_city', 'tbl_city.id = tbl_shipping.shipping_city','left');
        $this->db->where('shipping_id',$shipping_id);
        $result = $this->db->get();
        return $result->row();
    }
    
    public function payment_info_by_id($payment_id){
        $this->db->select('*');
        $this->db->from('tbl_payment');
        $this->db->where('payment_id',$payment_id);
        $result = $this->db->get();
        return $result->row();
    }
    
    public function orderdetails_info_by_id($order_id){
        $this->db->select('*');
        $this->db->from('tbl_order_details');
        $this->db->join('tbl_order', 'tbl_order.order_id = tbl_order_details.order_id','left');
        $this->db->where('tbl_order_details.order_id',$order_id);
        $result = $this->db->get();
        return $result->result();
    }
    
    
    
}