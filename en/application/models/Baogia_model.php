<?php

class Baogia_model extends CI_Model{
    
    public function save_baogia_info($data){
        return $this->db->insert('tbl_baogia', $data);
    }
    
    public function getall_baogia_info(){
        $this->db->select('*');
        $this->db->from('tbl_baogia');
        $info = $this->db->get();
        return $info->result();
    }
    

    
    public function delete_baogia_info($id){
        $this->db->where('baogia_id', $id);
        return $this->db->delete('tbl_baogia');
    }
    

    public function published_baogia_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('baogia_id', $id);
        return $this->db->update('tbl_baogia');
    }
    
    public function unpublished_baogia_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('baogia_id', $id);
        return $this->db->update('tbl_baogia');
    }
    
}