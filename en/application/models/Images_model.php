<?php

class Images_model extends CI_Model{
    
    public function save_images_info($data){
        return $this->db->insert('tbl_images', $data);
    }
    
    public function getall_images_info(){
        $this->db->select('*');
        $this->db->from('tbl_images');
        $this->db->join('tbl_product','tbl_product.product_id=tbl_images.images_product_id');
        $info = $this->db->get();
        return $info->result();
    }
    
    
    public function edit_images_info($id){
        $this->db->select('*');
        $this->db->from('tbl_images');
        $this->db->where('images_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function delete_images_info($id){
        $this->db->where('images_id', $id);
        return $this->db->delete('tbl_images');
    }
    
    public function update_images_info($data,$id){
        $this->db->where('images_id', $id);
        return $this->db->update('tbl_images', $data);
    }
    
    public function published_images_info($id) {
        $this->db->set('publication_status_img', 1);
        $this->db->where('images_id', $id);
        return $this->db->update('tbl_images');
    }
    
    public function unpublished_images_info($id) {
        $this->db->set('publication_status_img', 0);
        $this->db->where('images_id', $id);
        return $this->db->update('tbl_images');
    }
    
}