<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Comment extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    


    public function manage_comment() {
        $data= array();
        $data['all_comment'] = $this->comment_model->getall_comment_info();
        $data['maincontent']= $this->load->view('admin/pages/comment/manage_comment',$data,true);
        $this->load->view('admin/master',$data);
    }

    public function edit_comment($id){
        $data= array();
        $data['comment_info_by_id'] = $this->comment_model->edit_comment_info($id);
        $data['maincontent']= $this->load->view('admin/pages/comment/edit_comment',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function update_comment($id){
        $data = array();
        $data['comment_name'] = $this->input->post('comment_name');
        $data['comment_answer'] = $this->input->post('comment_answer');
        $data['comment_show'] = $this->input->post('comment_show');
      
            $result = $this->comment_model->update_comment_info($data,$id);

            if ($result) {
               $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> comment Updated Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/comment');   
            } else {
                $this->session->set_flashdata('message', 'comment Updated Failed');
                redirect('manage/comment');
            }
     
        
    }
    
  
    public function delete_comment($id){
        $result = $this->comment_model->delete_comment_info($id);
        if ($result) {
                 $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> comment Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/comment');      
        } else {
            $this->session->set_flashdata('message', 'comment Deleted Failed');
             redirect('manage/comment');
        }
    }

    public function comment_show_answer($id){
        $result = $this->comment_model->comment_show_answer($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published comment Sucessfully');
            redirect('manage/comment');
        } else {
            $this->session->set_flashdata('message', 'Published comment  Failed');
             redirect('manage/comment');
        }
    }
    
    public function un_comment_show_answer($id){
        $result = $this->comment_model->un_comment_show_answer($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished comment Sucessfully');
            redirect('manage/comment');
        } else {
            $this->session->set_flashdata('message', 'UnPublished comment  Failed');
             redirect('manage/comment');
        }
    }
    
    

   
    public function published_comment($id){
        $result = $this->comment_model->published_comment_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published comment Sucessfully');
            redirect('manage/comment');
        } else {
            $this->session->set_flashdata('message', 'Published comment  Failed');
             redirect('manage/comment');
        }
    }
    
    public function unpublished_comment($id){
        $result = $this->comment_model->unpublished_comment_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished comment Sucessfully');
            redirect('manage/comment');
        } else {
            $this->session->set_flashdata('message', 'UnPublished comment  Failed');
             redirect('manage/comment');
        }
    }
    

    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    

}
