<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Feel extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    
    public function add_feel() {
        $data= array();
        $data['maincontent']= $this->load->view('admin/pages/Feel/add_feel','',true);
        $this->load->view('admin/master',$data);
    }

    public function manage_feel() {
        $data= array();
        $data['all_feel'] = $this->feel_model->getall_feel_info();
        $data['role_user']=$this->role_user();
        $data['maincontent']= $this->load->view('admin/pages/Feel/manage_feel',$data,true);
        $this->load->view('admin/master',$data);
    }
      public function role_user()
    {
         $query =$this->db->query("select * from tbl_user where  user_id='".$this->session->userdata('user_id')."'");
         $rs_role_user = $query->row();
         return $rs_role_user;

    }
        function delete_imgfeel() 
      { 
        $id=$this->input->post('id');
         $query= $this->db->query("update tbl_feel set feel_image ='' where feel_id='".$id."'");
        return $query; 
      } 
    
    public function save_feel() {
        $data = array();
        $data['feel_title'] = $this->input->post('feel_title');
      //  $data['feel_link'] = $this->input->post('feel_link');
        $data['feel_des'] = $this->input->post('feel_des');
        $data['publication_status'] = $this->input->post('publication_status');


        if (!empty($_FILES['feel_image']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('feel_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/feel');
            }
            else{
                $post_image = $this->upload->data();
                $data['feel_image'] = $post_image['file_name'];
            }
        }
                    
            
            $result = $this->feel_model->save_feel_info($data);

            if ($result) {
                      $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> feel Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/feel');   
            } else {
                $this->session->set_flashdata('message', 'Feel Inserted Failed');
                redirect('manage/feel');
            }
     
    }
    
    public function delete_feel($id){
        $delete_image =$this->get_image_by_id($id);
        unlink('uploads/'.$delete_image->feel_image);
        $result = $this->feel_model->delete_feel_info($id);
        if ($result) {
                   $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> feel Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/feel');   
        } else {
            $this->session->set_flashdata('message', 'Feel Deleted Failed');
             redirect('manage/feel');
        }
    }
    
     public function edit_feel($id){
        $data= array();
        $data['feel_info_by_id'] = $this->feel_model->edit_feel_info($id);
        $data['maincontent']= $this->load->view('admin/pages/Feel/edit_feel',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function update_feel($id){
        $data = array();
        $data['feel_title'] = $this->input->post('feel_title');
  //      $data['feel_link'] = $this->input->post('feel_link');
        $data['feel_des'] = $this->input->post('feel_des');
        $data['publication_status'] = $this->input->post('publication_status');
        $delete_image = $this->input->post('feel_delete_image');
        
       

       

        if (!empty($_FILES['feel_image']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('feel_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/feel');
            }
            else{
                $post_image = $this->upload->data();
                $data['feel_image'] = $post_image['file_name'];
                unlink('uploads/'.$delete_image);
            }
        }
                    
            
            $result = $this->feel_model->update_feel_info($data,$id);

            if ($result) {
                  $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> feel Updated Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/feel');   
            } else {
                $this->session->set_flashdata('message', 'Feel Updated Failed');
                redirect('manage/feel');
            }
     
        
    }
    
    public function published_feel($id){
        $result = $this->feel_model->published_feel_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published Feel Sucessfully');
            redirect('manage/feel');
        } else {
            $this->session->set_flashdata('message', 'Published Feel  Failed');
             redirect('manage/feel');
        }
    }
    
    public function unpublished_feel($id){
        $result = $this->feel_model->unpublished_feel_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished Feel Sucessfully');
            redirect('manage/feel');
        } else {
            $this->session->set_flashdata('message', 'UnPublished Feel  Failed');
             redirect('manage/feel');
        }
    }
    
    private function get_image_by_id($id){
        $this->db->select('feel_image');
        $this->db->from('tbl_feel');
        $this->db->where('feel_id', $id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    

}
