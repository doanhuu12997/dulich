<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Baogia extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    


    public function manage_baogia() {
        $data= array();
        $data['all_baogia'] = $this->baogia_model->getall_baogia_info();
        $data['maincontent']= $this->load->view('admin/pages/baogia/manage_baogia',$data,true);
        $this->load->view('admin/master',$data);
    }
    
  
    public function delete_baogia($id){
        $result = $this->baogia_model->delete_baogia_info($id);
        if ($result) {
            $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> baogia Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/baogia');   
        } else {
            $this->session->set_flashdata('message', 'baogia Deleted Failed');
             redirect('manage/baogia');
        }
    }
    

   
    public function published_baogia($id){
        $result = $this->baogia_model->published_baogia_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published baogia Sucessfully');
            redirect('manage/baogia');
        } else {
            $this->session->set_flashdata('message', 'Published baogia  Failed');
             redirect('manage/baogia');
        }
    }
    
    public function unpublished_baogia($id){
        $result = $this->baogia_model->unpublished_baogia_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished baogia Sucessfully');
            redirect('manage/baogia');
        } else {
            $this->session->set_flashdata('message', 'UnPublished baogia  Failed');
             redirect('manage/baogia');
        }
    }
    

    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    

}
