<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Role_user extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    
    
    public function add_role_user() {
        $data= array();
        $data['get_role'] = $this->role_user_model->get_role();
        $data['maincontent']= $this->load->view('admin/pages/Role_user/add_role_user',$data,true);
        $this->load->view('admin/master',$data);

    }

    public function manage_role_user() {
        $data= array();
        $data['role_user']= $this->role_user();
        $data['all_categroy'] = $this->role_user_model->getall_role_user_info();
        $data['maincontent']= $this->load->view('admin/pages/Role_user/manage_role_user',$data,true);
        $this->load->view('admin/master',$data);
    }
    public function role_user()
    {
         $query =$this->db->query("select * from tbl_user where  user_id='".$this->session->userdata('user_id')."'");
         $rs_role_user = $query->row();
         return $rs_role_user;
    }
    public function save_role_user(){
        $data = array();
        $data['user_role']=$this->input->post('user_role');

        $data['user_name']=$this->input->post('user_name');
        $data['user_email']=$this->input->post('user_email');
        $data['user_password']=md5($this->input->post('user_password'));
        $data['user_role']=$this->input->post('user_role');
        $data['lv1']=$this->input->post('lv1');
        $data['product']=$this->input->post('product');
        $data['comment']=$this->input->post('comment');

        $data['product_home']=$this->input->post('product_home');
        $data['category_news']=$this->input->post('category_news');
        $data['news']=$this->input->post('news');
        $data['slider']=$this->input->post('slider');
        $data['feel']=$this->input->post('feel');
        $data['company']=$this->input->post('company');
        $data['partner']=$this->input->post('partner');
        $data['contact']=$this->input->post('contact');
        $data['role']=$this->input->post('role');
        $data['role_user']=$this->input->post('role_user');
        $data['order']=$this->input->post('order');
        $data['role_edit_lv1']=$this->input->post('role_edit_lv1');
        
        $data['role_edit_product']=$this->input->post('role_edit_product');
        $data['role_edit_product_home']=$this->input->post('role_edit_product_home');
        $data['role_edit_category_news']=$this->input->post('role_edit_category_news');
        $data['role_edit_news']=$this->input->post('role_edit_news');
        $data['role_edit_slider']=$this->input->post('role_edit_slider');
        $data['role_edit_feel']=$this->input->post('role_edit_feel');
        $data['role_edit_partner']=$this->input->post('role_edit_partner');
        $data['role_edit_company']=$this->input->post('role_edit_company');
        $data['role_edit_contact']=$this->input->post('role_edit_contact');
        $data['role_edit_role']=$this->input->post('role_edit_role');
        $data['role_edit_role_user']=$this->input->post('role_edit_role_user');
        $data['role_edit_order']=$this->input->post('role_edit_order');

        $data['role_add_lv1']=$this->input->post('role_add_lv1');
     
        $data['role_add_product']=$this->input->post('role_add_product');
        $data['role_add_product_home']=$this->input->post('role_add_product_home');
        $data['role_add_category_news']=$this->input->post('role_add_category_news');
        $data['role_add_news']=$this->input->post('role_add_news');
        $data['role_add_slider']=$this->input->post('role_add_slider');
        $data['role_add_feel']=$this->input->post('role_add_feel');
        $data['role_add_partner']=$this->input->post('role_add_partner');
        $data['role_add_company']=$this->input->post('role_add_company');
        $data['role_add_contact']=$this->input->post('role_add_contact');
        $data['role_add_role']=$this->input->post('role_add_role');
        $data['role_add_role_user']=$this->input->post('role_add_role_user');
        $data['role_add_order']=$this->input->post('role_add_order');

        $data['role_delete_lv1']=$this->input->post('role_delete_lv1');
        $data['role_delete_product']=$this->input->post('role_delete_product');
        $data['role_delete_product_home']=$this->input->post('role_delete_product_home');
        $data['role_delete_category_news']=$this->input->post('role_delete_category_news');
        $data['role_delete_news']=$this->input->post('role_delete_news');
        $data['role_delete_slider']=$this->input->post('role_delete_slider');
        $data['role_delete_feel']=$this->input->post('role_delete_feel');
        $data['role_delete_partner']=$this->input->post('role_delete_partner');
        $data['role_delete_company']=$this->input->post('role_delete_company');
        $data['role_delete_contact']=$this->input->post('role_delete_contact');
        $data['role_delete_role']=$this->input->post('role_delete_role');
        $data['role_delete_role_user']=$this->input->post('role_delete_role_user');
        $data['role_delete_order']=$this->input->post('role_delete_order');
        $data['role_delete_comment']=$this->input->post('role_delete_comment');

        $data['thuonghieu']=$this->input->post('thuonghieu');
        $data['thanhphan']=$this->input->post('thanhphan');
        $data['congdung']=$this->input->post('congdung');
        $data['xuatxu']=$this->input->post('xuatxu');
        $data['tinhthanh']=$this->input->post('tinhthanh');
        $data['magiamgia']=$this->input->post('magiamgia');
        $data['page']=$this->input->post('page');
        $data['avd']=$this->input->post('avd');
        $data['chinhsach']=$this->input->post('chinhsach');
        $data['dangkymail']=$this->input->post('dangkymail');




            $result = $this->role_user_model->save_role_user_info($data);
            if($result){
                 $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> role_user Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/role_user');   
            }
            else{
                $this->session->set_flashdata('message','role_user Inserted Failed');
                redirect('manage/role_user');
            }
     
        
    }
    
    public function delete_role_user($id){
        $result = $this->role_user_model->delete_role_user_info($id);
        if ($result) {
              $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> role_user Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/role_user');   
        } else {
            $this->session->set_flashdata('message', 'role_user Deleted Failed');
             redirect('manage/role_user');
        }
    }
    
     public function edit_role_user($id){
        $data= array();
        $data['get_role'] = $this->role_user_model->get_role();
        $data['role_user_info_by_id'] = $this->role_user_model->edit_role_user_info($id);
        $data['maincontent']= $this->load->view('admin/pages/Role_user/edit_role_user',$data,true);
        $this->load->view('admin/master',$data);
    }

    
    public function update_role_user($id){
        $data = array();
        $data['user_role']=$this->input->post('user_role');
        $data['user_name']=$this->input->post('user_name');
        $data['user_email']=$this->input->post('user_email');
        $data['user_role']=$this->input->post('user_role');
        $data['lv1']=$this->input->post('lv1');
        $data['product']=$this->input->post('product');
        $data['comment']=$this->input->post('comment');
        $data['product_home']=$this->input->post('product_home');
        $data['category_news']=$this->input->post('category_news');
        $data['news']=$this->input->post('news');
        $data['slider']=$this->input->post('slider');
        $data['feel']=$this->input->post('feel');
        $data['company']=$this->input->post('company');
        $data['partner']=$this->input->post('partner');
        $data['contact']=$this->input->post('contact');
        $data['role']=$this->input->post('role');
        $data['role_user']=$this->input->post('role_user');
        $data['order']=$this->input->post('order');
        $data['role_edit_lv1']=$this->input->post('role_edit_lv1');
        $data['role_edit_product']=$this->input->post('role_edit_product');
        $data['role_edit_product_home']=$this->input->post('role_edit_product_home');
        $data['role_edit_category_news']=$this->input->post('role_edit_category_news');
        $data['role_edit_news']=$this->input->post('role_edit_news');
        $data['role_edit_slider']=$this->input->post('role_edit_slider');
        $data['role_edit_feel']=$this->input->post('role_edit_feel');
        $data['role_edit_partner']=$this->input->post('role_edit_partner');
        $data['role_edit_company']=$this->input->post('role_edit_company');
        $data['role_edit_contact']=$this->input->post('role_edit_contact');
        $data['role_edit_role']=$this->input->post('role_edit_role');
        $data['role_edit_role_user']=$this->input->post('role_edit_role_user');
        $data['role_edit_order']=$this->input->post('role_edit_order');

        $data['role_add_lv1']=$this->input->post('role_add_lv1');
        $data['role_add_product']=$this->input->post('role_add_product');
        $data['role_add_product_home']=$this->input->post('role_add_product_home');
        $data['role_add_category_news']=$this->input->post('role_add_category_news');
        $data['role_add_news']=$this->input->post('role_add_news');
        $data['role_add_slider']=$this->input->post('role_add_slider');
        $data['role_add_feel']=$this->input->post('role_add_feel');
        $data['role_add_partner']=$this->input->post('role_add_partner');
        $data['role_add_company']=$this->input->post('role_add_company');
        $data['role_add_contact']=$this->input->post('role_add_contact');
        $data['role_add_role']=$this->input->post('role_add_role');
        $data['role_add_role_user']=$this->input->post('role_add_role_user');
        $data['role_add_order']=$this->input->post('role_add_order');

        $data['role_delete_lv1']=$this->input->post('role_delete_lv1');
        $data['role_delete_product']=$this->input->post('role_delete_product');
        $data['role_delete_product_home']=$this->input->post('role_delete_product_home');
        $data['role_delete_category_news']=$this->input->post('role_delete_category_news');
        $data['role_delete_news']=$this->input->post('role_delete_news');
        $data['role_delete_slider']=$this->input->post('role_delete_slider');
        $data['role_delete_feel']=$this->input->post('role_delete_feel');
        $data['role_delete_partner']=$this->input->post('role_delete_partner');
        $data['role_delete_company']=$this->input->post('role_delete_company');
        $data['role_delete_contact']=$this->input->post('role_delete_contact');
        $data['role_delete_role']=$this->input->post('role_delete_role');
        $data['role_delete_role_user']=$this->input->post('role_delete_role_user');
        $data['role_delete_order']=$this->input->post('role_delete_order');
        $data['role_delete_comment']=$this->input->post('role_delete_comment');
        
        $data['thuonghieu']=$this->input->post('thuonghieu');
        $data['thanhphan']=$this->input->post('thanhphan');
        $data['congdung']=$this->input->post('congdung');
        $data['xuatxu']=$this->input->post('xuatxu');
        $data['tinhthanh']=$this->input->post('tinhthanh');
        $data['magiamgia']=$this->input->post('magiamgia');
        $data['page']=$this->input->post('page');
        $data['avd']=$this->input->post('avd');
        $data['chinhsach']=$this->input->post('chinhsach');
        $data['dangkymail']=$this->input->post('dangkymail');
         $get_pass=$this->input->post('user_password');

           if(empty($get_pass))
        {
            $xxx['user_password'];

        }
        else{ 
         $xxx['user_password'] =md5($get_pass);
          $this->role_user_model->update_role_user_info($xxx,$id);

         }
      
      
        $result = $this->role_user_model->update_role_user_info($data,$id);
            if($result){
                $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> role_user Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/role_user');   
            }
            else{
                $this->session->set_flashdata('message','role_user Update Failed');
                redirect('manage/role_user');
            }
      
        
    }


    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    

}
