<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Regiter_pass extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    


    public function manage_regiter_pass() {
        $data= array();
        $data['all_regiter_pass'] = $this->regiter_pass_model->getall_regiter_pass_info();
        $data['maincontent']= $this->load->view('admin/pages/Regiter_pass/manage_regiter_pass',$data,true);
        $this->load->view('admin/master',$data);
    }
    
  
    public function delete_regiter_pass($id){
        $result = $this->regiter_pass_model->delete_regiter_pass_info($id);
        if ($result) {
            $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> regiter_pass Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/regiter_pass');  
        } else {
            $this->session->set_flashdata('message', 'regiter_pass Deleted Failed');
             redirect('manage/regiter_pass');
        }
    }
    

   
    public function published_regiter_pass($id){
        $result = $this->regiter_pass_model->published_regiter_pass_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published regiter_pass Sucessfully');
            redirect('manage/regiter_pass');
        } else {
            $this->session->set_flashdata('message', 'Published regiter_pass  Failed');
             redirect('manage/regiter_pass');
        }
    }
    
    public function unpublished_regiter_pass($id){
        $result = $this->regiter_pass_model->unpublished_regiter_pass_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished regiter_pass Sucessfully');
            redirect('manage/regiter_pass');
        } else {
            $this->session->set_flashdata('message', 'UnPublished regiter_pass  Failed');
             redirect('manage/regiter_pass');
        }
    }
    

    public function get_user(){
       
       $epass = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($epass==false){
          redirect('admin'); 
       }
        
    }
    

}
