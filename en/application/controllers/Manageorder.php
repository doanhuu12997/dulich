<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manageorder extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    
    
    public function manage_order(){
        $data= array();
        $data['all_manage_order_info'] =$this->manageorder_model->manage_order_info();
        $data['role_user']=$this->role_user();
        $data['maincontent']= $this->load->view('admin/pages/Order/manage_order',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function role_user()
    {
         $query =$this->db->query("select * from tbl_user where  user_id='".$this->session->userdata('user_id')."'");
         $rs_role_user = $query->row();
         return $rs_role_user;

    }

      public function delete_order($id){
        $result = $this->manageorder_model->delete_order_info($id);
       if($result){
               $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> Order Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/order');   
            
            } else {
            $this->session->set_flashdata('message', 'Order Deleted Failed');
             redirect('manage/order');
        }
    }

    public function update_status_order($id){
        $data = array();
        $data['actions']=$this->input->post('actions');
        $data['note']=$this->input->post('note');
            $result = $this->manageorder_model->update_status_order($data,$id);
            if($result){
               $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> order Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/order');   
            }
            else{
                $this->session->set_flashdata('message','order Update Failed');
                redirect('manage/order');
            }
      
        
    }
    
    public function order_details($order_id){
        $data= array();
        $order_info =$this->manageorder_model->order_info_by_id($order_id);
        $shipping_id = $order_info->shipping_id;
        $payment_id = $order_info->payment_id;
        $customer_id = $order_info->customer_id;
        $data['shipping_info'] =$this->manageorder_model->shipping_info_by_id($shipping_id);
        $data['get_infor_city'] =$this->manageorder_model->get_infor_city($shipping_id);

        $data['customer_info'] =$this->manageorder_model->customer_info_by_id($customer_id);
        $data['payment_info'] =$this->manageorder_model->payment_info_by_id($payment_id);
        $data['order_details_info'] =$this->manageorder_model->orderdetails_info_by_id($order_id);
        $data['order_info'] =$this->manageorder_model->order_info_by_id($order_id);

        $data['maincontent']= $this->load->view('admin/pages/Order/order_details',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    
}
