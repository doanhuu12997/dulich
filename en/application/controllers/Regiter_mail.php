<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Regiter_mail extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    


    public function manage_regiter_mail() {
        $data= array();
        $data['all_regiter_mail'] = $this->regiter_mail_model->getall_regiter_mail_info();
        $data['maincontent']= $this->load->view('admin/pages/Regiter_mail/manage_regiter_mail',$data,true);
        $this->load->view('admin/master',$data);
    }
    
  
    public function delete_regiter_mail($id){
        $result = $this->regiter_mail_model->delete_regiter_mail_info($id);
        if ($result) {
            $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> regiter_mail Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/regiter_mail');  
        } else {
            $this->session->set_flashdata('message', 'regiter_mail Deleted Failed');
             redirect('manage/regiter_mail');
        }
    }
    

   
    public function published_regiter_mail($id){
        $result = $this->regiter_mail_model->published_regiter_mail_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published regiter_mail Sucessfully');
            redirect('manage/regiter_mail');
        } else {
            $this->session->set_flashdata('message', 'Published regiter_mail  Failed');
             redirect('manage/regiter_mail');
        }
    }
    
    public function unpublished_regiter_mail($id){
        $result = $this->regiter_mail_model->unpublished_regiter_mail_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished regiter_mail Sucessfully');
            redirect('manage/regiter_mail');
        } else {
            $this->session->set_flashdata('message', 'UnPublished regiter_mail  Failed');
             redirect('manage/regiter_mail');
        }
    }
    

    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    

}
