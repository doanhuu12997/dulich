<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    


    public function manage_contact() {
        $data= array();
        $data['all_contact'] = $this->contact_model->getall_contact_info();
        $data['role_user']=$this->role_user();
        $data['maincontent']= $this->load->view('admin/pages/Contact/manage_contact',$data,true);
        $this->load->view('admin/master',$data);
    }
          public function role_user()
    {
         $query =$this->db->query("select * from tbl_user where  user_id='".$this->session->userdata('user_id')."'");
         $rs_role_user = $query->row();
         return $rs_role_user;

    }
  
    public function delete_contact($id){
        $result = $this->contact_model->delete_contact_info($id);
        if ($result) {
           $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> contact Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/contact');      
        } else {
            $this->session->set_flashdata('message', 'contact Deleted Failed');
             redirect('manage/contact');
        }
    }
    

   
    public function published_contact($id){
        $result = $this->contact_model->published_contact_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published contact Sucessfully');
            redirect('manage/contact');
        } else {
            $this->session->set_flashdata('message', 'Published contact  Failed');
             redirect('manage/contact');
        }
    }
    
    public function unpublished_contact($id){
        $result = $this->contact_model->unpublished_contact_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished contact Sucessfully');
            redirect('manage/contact');
        } else {
            $this->session->set_flashdata('message', 'UnPublished contact  Failed');
             redirect('manage/contact');
        }
    }
    

    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    

}
