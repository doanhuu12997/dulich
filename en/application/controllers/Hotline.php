<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Hotline extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->get_user();
        $this->load->model('hotline_model'); // Load the User_model

    }



    public function add_hotline()
    {
        $data = array();
        // $data['all_categroydmtin'] = $this->category_dmtin_model->getall_category_dmtin_info();
        $data['maincontent'] = $this->load->view('admin/pages/Hotline/add_hotline', $data, true);
        $this->load->view('admin/master', $data);
    }
    public function manage_hotline()
    {
        $data = array();
        $data['all_hotline'] = $this->hotline_model->getall_hotline_info();
        $data['maincontent'] = $this->load->view('admin/pages/Hotline/manage_hotline', $data, true);
        $this->load->view('admin/master', $data);
    }
    public function role_user()
    {
        $query = $this->db->query("select * from tbl_user where  user_id='" . $this->session->userdata('user_id') . "'");
        $rs_role_user = $query->row();
        return $rs_role_user;
    }
    function delete_cateimgnew()
    {
        $id = $this->input->post('id');
        $query = $this->db->query("update tbl_category_dmtin set category_image ='' where id='" . $id . "'");
        return $query;
    }

    public function save_hotline()
    {
        $data = array();
        $data['name'] = $this->input->post('hotline_name');
        $data['phone'] = $this->input->post('hotline_phone');
        $data['location'] = $this->input->post('hotline_location');
        $data['publication_status'] = $this->input->post('publication_status');

        $result = $this->hotline_model->save_hotline_info($data);
        if ($result) {
            $this->session->set_flashdata('message', '<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> Hotline Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
            redirect('manage/hotline');
        } else {
            $this->session->set_flashdata('message', 'Category Inserted Failed');
            redirect('manage/hotline');
        }
    }

    public function delete_hotline($id)
    {
        $result = $this->hotline_model->delete_hotline_info($id);
        if ($result) {
            $this->session->set_flashdata('message', '<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> Category Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
            redirect('manage/hotline');
        } else {
            $this->session->set_flashdata('message', 'Category Deleted Failed');
            redirect('manage/hotline');
        }
    }

    public function edit_hotline($id)
    {
        $data = array();

        $data['hotline_by_id'] = $this->hotline_model->edit_hotline_info($id);
        $data['maincontent'] = $this->load->view('admin/pages/Hotline/edit_hotline', $data, true);
        $this->load->view('admin/master', $data);
    }

    public function update_category_dmtin($id)
    {
        $data = array();
        $data['name'] = $this->input->post('hotline_name');
        $data['phone'] = $this->input->post('hotline_phone');
        $data['location'] = $this->input->post('hotline_location');
        $data['publication_status'] = $this->input->post('publication_status');

        $result = $this->hotline_model->update_hotline_info($data, $id);
        if ($result) {
            $this->session->set_flashdata('message', '<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> Category Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
            redirect('manage/hotline');
        } else {
            $this->session->set_flashdata('message', 'Category Update Failed');
            redirect('manage/hotline');
        }
    }

    public function published_hotline($id)
    {
        $result = $this->hotline_model->published_hotline_info($id);
        if ($result) {
            $this->session->set_flashdata('message', '<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> Published Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
            redirect('manage/hotline');
        } else {
            $this->session->set_flashdata('message', 'Published Category  Failed');
            redirect('manage/hotline');
        }
    }
    public function unpublished_hotline($id)
    {
        $result = $this->hotline_model->unpublished_hotline_info($id);
        if ($result) {
            $this->session->set_flashdata('message', '<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> Unpublished Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
            redirect('manage/hotline');
        } else {
            $this->session->set_flashdata('message', 'Unpublished Category  Failed');
            redirect('manage/hotline');
        }
    }
    public function get_user()
    {





        $email = $this->session->userdata('user_email');


        $name = $this->session->userdata('user_name');


        $id = $this->session->userdata('user_id');





        if ($email == false) {


            redirect('admin');
        }
    }
}
