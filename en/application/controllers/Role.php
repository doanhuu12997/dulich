<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    
    
    public function add_role() {
        $data= array();
        $data['maincontent']= $this->load->view('admin/pages/Role/add_role','',true);
        $this->load->view('admin/master',$data);
    }

    public function manage_role() {
        $data= array();
        $data['role_user']= $this->role_user();
        $data['all_categroy'] = $this->role_model->getall_role_info();
        $data['maincontent']= $this->load->view('admin/pages/Role/manage_role',$data,true);
        $this->load->view('admin/master',$data);
    }
    public function role_user()
    {
         $query =$this->db->query("select * from tbl_user where  user_id='".$this->session->userdata('user_id')."'");
         $rs_role_user = $query->row();
         return $rs_role_user;
    }
    public function save_role(){
        $data = array();
        $data['role_name']=$this->input->post('role_name');
      
            $result = $this->role_model->save_role_info($data);
            if($result){
               $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> role Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/role');  
            }
            else{
                $this->session->set_flashdata('message','role Inserted Failed');
                redirect('manage/role');
            }
     
        
    }
    
    public function delete_role($id){
        $result = $this->role_model->delete_role_info($id);
        if ($result) {
            $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> role Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/role');  
        } else {
            $this->session->set_flashdata('message', 'role Deleted Failed');
             redirect('manage/role');
        }
    }
    
     public function edit_role($id){
        $data= array();
        $data['role_info_by_id'] = $this->role_model->edit_role_info($id);
        $data['maincontent']= $this->load->view('admin/pages/Role/edit_role',$data,true);
        $this->load->view('admin/master',$data);
    }

    
    public function update_role($id){
        $data = array();
        $data['role_name']=$this->input->post('role_name');
            $result = $this->role_model->update_role_info($data,$id);
            if($result){
                $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> role Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/role');  
            }
            else{
                $this->session->set_flashdata('message','role Update Failed');
                redirect('manage/role');
            }
      
        
    }


    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    

}
