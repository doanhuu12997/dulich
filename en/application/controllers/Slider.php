<?php





defined('BASEPATH') OR exit('No direct script access allowed');





class Slider extends CI_Controller {


    


    public function __construct() {


        parent::__construct();


        $this->get_user();


    }


    


    public function add_slider() {


        $data= array();


        $data['maincontent']= $this->load->view('admin/pages/Slider/add_slider','',true);


        $this->load->view('admin/master',$data);


    }





    public function manage_slider() {


        $data= array();


        $data['all_slider'] = $this->slider_model->getall_slider_info();


        $data['role_user']=$this->role_user();


        $data['maincontent']= $this->load->view('admin/pages/Slider/manage_slider',$data,true);


        $this->load->view('admin/master',$data);


    }


    function delete_imgslider() 


      { 


        $id=$this->input->post('id');


         $query= $this->db->query("update tbl_slider set slider_image ='' where slider_id='".$id."'");


        return $query; 


      } 





       function delete_video() 


      { 


        $id=$this->input->post('id');


         $query= $this->db->query("update tbl_slider set slider_link_video ='' where slider_id='".$id."'");


        return $query; 


      } 





     public function role_user()


    {


         $query =$this->db->query("select * from tbl_user where  user_id='".$this->session->userdata('user_id')."'");


         $rs_role_user = $query->row();


         return $rs_role_user;





    }


    public function save_slider() {


        $data = array();


        $data['slider_title'] = $this->input->post('slider_title');
         $data['slider_tit'] = $this->input->post('title');

        $data['slider_des'] = $this->input->post('des');
           $data['button_text'] = $this->input->post('button_text');
        $data['slider_link'] = $this->input->post('slider_link');


        $data['publication_status'] = $this->input->post('publication_status');








        if (!empty($_FILES['slider_image']['name'])) {


            $config['upload_path'] = './uploads/';


            $config['allowed_types'] = '*';


            $config['max_size'] = 40964;


            $config['max_width'] = 55000;


            $config['max_height'] = 55000;





            $this->upload->initialize($config);





            if (!$this->upload->do_upload('slider_image')) {


                $error = $this->upload->display_errors();


                $this->session->set_flashdata('message', $error);


                redirect('add/slider');


            }


            else{


                $post_image = $this->upload->data();


                $data['slider_image'] = $post_image['file_name'];


            }


        }


           if (!empty($_FILES['slider_image_moblie']['name'])) {


            $config['upload_path'] = './uploads/';


            $config['allowed_types'] = 'gif|jpg|png';


            $config['max_size'] = 40964;


            $config['max_width'] = 55000;


            $config['max_height'] = 55000;





            $this->upload->initialize($config);





            if (!$this->upload->do_upload('slider_image_moblie')) {


                $error = $this->upload->display_errors();


                $this->session->set_flashdata('message', $error);


                redirect('add/slider');


            }


            else{


                $post_image = $this->upload->data();


                $data['slider_image_moblie'] = $post_image['file_name'];


            }


        }





         if (!empty($_FILES['slider_link_video']['name'])) {


            $config['upload_path'] = './uploads/video/';


            $config['allowed_types'] = 'mp4';


            $config['max_size'] = 40964;


            $config['max_width'] = 55000;


            $config['max_height'] = 55000;





            $this->upload->initialize($config);





            if (!$this->upload->do_upload('slider_link_video')) {


                $error = $this->upload->display_errors();


                $this->session->set_flashdata('message', $error);


                redirect('add/slider');


            }


            else{


                $post_image = $this->upload->data();


                $data['slider_link_video'] = $post_image['file_name'];


            }


        }


            


                    


            


            $result = $this->slider_model->save_slider_info($data);





            if ($result) {


                        $this->session->set_flashdata('message','<div class="alert alert-success">


            <div class="toast_icon">


                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">


                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>


                </g></g>


                </svg>


                </div>


                  <strong>Success!</strong> Slider Inserted Sucessfully


                  <button type="button" class="close" data-dismiss="alert">&times;</button>


                </div>');


                redirect('manage/slider');  


            } else {


                $this->session->set_flashdata('message', 'Slider Inserted Failed');


                redirect('manage/slider');


            }


      


    }


    


    public function delete_slider($id){


        $delete_image =$this->get_image_by_id($id);


        unlink('uploads/'.$delete_image->slider_image);


        $result = $this->slider_model->delete_slider_info($id);


        if ($result) {


           $this->session->set_flashdata('message','<div class="alert alert-success">


            <div class="toast_icon">


                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">


                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>


                </g></g>


                </svg>


                </div>


                  <strong>Success!</strong> Slider Deleted Sucessfully


                  <button type="button" class="close" data-dismiss="alert">&times;</button>


                </div>');


                redirect('manage/slider');  


        } else {


            $this->session->set_flashdata('message', 'Slider Deleted Failed');


             redirect('manage/slider');


        }


    }


    


     public function edit_slider($id){


        $data= array();


        $data['slider_info_by_id'] = $this->slider_model->edit_slider_info($id);


        $data['maincontent']= $this->load->view('admin/pages/Slider/edit_slider',$data,true);


        $this->load->view('admin/master',$data);


    }


    


    public function update_slider($id){


        $data = array();


        $data['slider_title'] = $this->input->post('slider_title');
        $data['slider_tit'] = $this->input->post('title');

        $data['slider_des'] = $this->input->post('des');
 $data['button_text'] = $this->input->post('button_text');

        $data['slider_link'] = $this->input->post('slider_link');


        $data['publication_status'] = $this->input->post('publication_status');


        $delete_image = $this->input->post('slider_delete_image');


        





        if (!empty($_FILES['slider_image']['name'])) {


            $config['upload_path'] = './uploads/';


            $config['allowed_types'] = '*';


            $config['max_size'] = 55000;


            $config['max_width'] = 55000;


            $config['max_height'] = 55000;





            $this->upload->initialize($config);





            if (!$this->upload->do_upload('slider_image')) {


                $error = $this->upload->display_errors();


                $this->session->set_flashdata('message', $error);


                redirect('add/slider');


            }


            else{


                $post_image = $this->upload->data();


                $data['slider_image'] = $post_image['file_name'];


                unlink('uploads/'.$delete_image);


            }


        }


        if (!empty($_FILES['slider_image_moblie']['name'])) {


            $config['upload_path'] = './uploads/';


            $config['allowed_types'] = 'gif|jpg|png|mp4';


            $config['max_size'] = 55000;


            $config['max_width'] = 55000;


            $config['max_height'] = 55000;





            $this->upload->initialize($config);





            if (!$this->upload->do_upload('slider_image_moblie')) {


                $error = $this->upload->display_errors();


                $this->session->set_flashdata('message', $error);


                redirect('add/slider');


            }


            else{


                $post_image = $this->upload->data();


                $data['slider_image_moblie'] = $post_image['file_name'];


                unlink('uploads/'.$delete_image);


            }


        }





          if (!empty($_FILES['slider_link_video']['name'])) {


            $config['upload_path'] = './uploads/video/';


            $config['allowed_types'] = 'mp4';


            $config['max_size'] = 40964;


            $config['max_width'] = 55000;


            $config['max_height'] = 55000;





            $this->upload->initialize($config);





            if (!$this->upload->do_upload('slider_link_video')) {


                $error = $this->upload->display_errors();


                $this->session->set_flashdata('message', $error);


                redirect('add/slider');


            }


            else{


                $post_image = $this->upload->data();


                $data['slider_link_video'] = $post_image['file_name'];


            }


        }


            


                    


            


            $result = $this->slider_model->update_slider_info($data,$id);





            if ($result) {


                   $this->session->set_flashdata('message','<div class="alert alert-success">


            <div class="toast_icon">


                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">


                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>


                </g></g>


                </svg>


                </div>


                  <strong>Success!</strong> Slider Updated Sucessfully


                  <button type="button" class="close" data-dismiss="alert">&times;</button>


                </div>');


                redirect('manage/slider');  


            } else {


                $this->session->set_flashdata('message', 'Slider Updated Failed');


                redirect('manage/slider');


            }


      


        


    }


    


    public function published_slider($id){


        $result = $this->slider_model->published_slider_info($id);


        if ($result) {


            $this->session->set_flashdata('message', 'Published Slider Sucessfully');


            redirect('manage/slider');


        } else {


            $this->session->set_flashdata('message', 'Published Slider  Failed');


             redirect('manage/slider');


        }


    }


    


    public function unpublished_slider($id){


        $result = $this->slider_model->unpublished_slider_info($id);


        if ($result) {


            $this->session->set_flashdata('message', 'UnPublished Slider Sucessfully');


            redirect('manage/slider');


        } else {


            $this->session->set_flashdata('message', 'UnPublished Slider  Failed');


             redirect('manage/slider');


        }


    }


    


    private function get_image_by_id($id){


        $this->db->select('slider_image');


        $this->db->from('tbl_slider');


        $this->db->where('slider_id', $id);


        $info = $this->db->get();


        return $info->row();


    }


    


    public function get_user(){


       


       $email = $this->session->userdata('user_email');


       $name = $this->session->userdata('user_name');


       $id = $this->session->userdata('user_id');


       


       if($email==false){


          redirect('admin'); 


       }


        


    }


    





}


