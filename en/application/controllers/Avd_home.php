<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Avd_home extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    
    public function add_avd_home() {
        $data= array();
        $data['maincontent']= $this->load->view('admin/pages/Avd_home/add_avd_home','',true);
        $this->load->view('admin/master',$data);
    }

    public function manage_avd_home() {
        $data= array();
        $data['all_avd_home'] = $this->avd_home_model->getall_avd_home_info();
        $data['role_user']=$this->role_user();
        $data['maincontent']= $this->load->view('admin/pages/Avd_home/manage_avd_home',$data,true);
        $this->load->view('admin/master',$data);
    }
      public function role_user()
    {
         $query =$this->db->query("select * from tbl_user where  user_id='".$this->session->userdata('user_id')."'");
         $rs_role_user = $query->row();
         return $rs_role_user;

    }
    
    public function save_avd_home() {
        $data = array();
        $data['avd_home_title'] = $this->input->post('avd_home_title');
        $data['avd_home_link'] = $this->input->post('avd_home_link');
        $data['publication_status'] = $this->input->post('publication_status');


        if (!empty($_FILES['avd_home_image']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('avd_home_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/avd_home');
            }
            else{
                $post_image = $this->upload->data();
                $data['avd_home_image'] = $post_image['file_name'];
            }
        }
                    
            
            $result = $this->avd_home_model->save_avd_home_info($data);

            if ($result) {
                   $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> avd_home Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/avd_home');  
            } else {
                $this->session->set_flashdata('message', 'avd_home Inserted Failed');
                redirect('manage/avd_home');
            }
     
    }
    
    public function delete_avd_home($id){
        $delete_image =$this->get_image_by_id($id);
        unlink('uploads/'.$delete_image->avd_home_image);
        $result = $this->avd_home_model->delete_avd_home_info($id);
        if ($result) {
                 $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> avd_home Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/avd_home');  
        } else {
            $this->session->set_flashdata('message', 'avd_home Deleted Failed');
             redirect('manage/avd_home');
        }
    }
    
     public function edit_avd_home($id){
        $data= array();
        $data['avd_home_info_by_id'] = $this->avd_home_model->edit_avd_home_info($id);
        $data['maincontent']= $this->load->view('admin/pages/Avd_home/edit_avd_home',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function update_avd_home($id){
        $data = array();
        $data['avd_home_title'] = $this->input->post('avd_home_title');
        $data['avd_home_link'] = $this->input->post('avd_home_link');
        $data['publication_status'] = $this->input->post('publication_status');
        $delete_image = $this->input->post('avd_home_delete_image');
        
       

       

        if (!empty($_FILES['avd_home_image']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('avd_home_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/avd_home');
            }
            else{
                $post_image = $this->upload->data();
                $data['avd_home_image'] = $post_image['file_name'];
                unlink('uploads/'.$delete_image);
            }
        }
                    
            
            $result = $this->avd_home_model->update_avd_home_info($data,$id);

            if ($result) {
                $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> avd_home Updated Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/avd_home');  
            } else {
                $this->session->set_flashdata('message', 'avd_home Updated Failed');
                redirect('manage/avd_home');
            }
     
        
    }
    
    public function published_avd_home($id){
        $result = $this->avd_home_model->published_avd_home_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published avd_home Sucessfully');
            redirect('manage/avd_home');
        } else {
            $this->session->set_flashdata('message', 'Published avd_home  Failed');
             redirect('manage/avd_home');
        }
    }
    
    public function unpublished_avd_home($id){
        $result = $this->avd_home_model->unpublished_avd_home_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished avd_home Sucessfully');
            redirect('manage/avd_home');
        } else {
            $this->session->set_flashdata('message', 'UnPublished avd_home  Failed');
             redirect('manage/avd_home');
        }
    }
    
    private function get_image_by_id($id){
        $this->db->select('avd_home_image');
        $this->db->from('tbl_avd_home');
        $this->db->where('avd_home_id', $id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    

}
