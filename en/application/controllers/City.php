<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class City extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    
    
    public function add_city() {
        $data= array();
        $data['maincontent']= $this->load->view('admin/pages/City/add_city','',true);
        $this->load->view('admin/master',$data);
    }

    public function manage_city() {
        $data= array();
        $data['all_city'] = $this->city_model->getall_city_info();
        $data['role_user']=$this->role_user();
        $data['maincontent']= $this->load->view('admin/pages/City/manage_city',$data,true);
        $this->load->view('admin/master',$data);
    }
     public function role_user()
    {
         $query =$this->db->query("select * from tbl_user where  user_id='".$this->session->userdata('user_id')."'");
         $rs_role_user = $query->row();
         return $rs_role_user;

    }

    

    

    public function save_city(){

        $data = array();
        $data['city_name']=$this->input->post('city_name');
        $data['cate_status']=$this->input->post('cate_status');
        $data['city_stt']=$this->input->post('city_stt');
        $data['price_ship']=$this->input->post('price_ship');

        
        $result = $this->city_model->save_city_info($data);
      
            if($result){
                 $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> city Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/city');   
            }
            else{
                $this->session->set_flashdata('message','city Inserted Failed');
                redirect('manage/city');
            }
     
        
    }
    
    public function delete_city($id){
        $result = $this->city_model->delete_city_info($id);
       if($result){
               $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> city Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/city');   
            
            } else {
            $this->session->set_flashdata('message', 'city Deleted Failed');
             redirect('manage/city');
        }
    }
    
     public function edit_city($id){
        $data= array();
        $data['city_info_by_id'] = $this->city_model->edit_city_info($id);
        $data['maincontent']= $this->load->view('admin/pages/City/edit_city',$data,true);
        $this->load->view('admin/master',$data);
    }

    
    public function update_city($id){
        $data = array();
         $data['city_name']=$this->input->post('city_name');
        $data['cate_status']=$this->input->post('cate_status');
        $data['city_stt']=$this->input->post('city_stt');
        $data['price_ship']=$this->input->post('price_ship');

            $result = $this->city_model->update_city_info($data,$id);
            if($result){
               $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> city Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/city');   
            }
            else{
                $this->session->set_flashdata('message','city Update Failed');
                redirect('manage/city');
            }
      
        
    }


    
    public function published_city($id){
        $result = $this->city_model->published_city_info($id);
        if ($result) {
            $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> Published city Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/city'); 
        } else {
            $this->session->set_flashdata('message', 'Published city  Failed');
             redirect('manage/city');
        }
    }
    
    public function unpublished_city($id){
        $result = $this->city_model->unpublished_city_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished city Sucessfully');
            redirect('manage/city');
        } else {
            $this->session->set_flashdata('message', 'UnPublished city  Failed');
             redirect('manage/city');
        }
    }
    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    

}
