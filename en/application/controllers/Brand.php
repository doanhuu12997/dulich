<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Brand extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }

    public function add_brand() {
        $data= array();
        $data['all_published_category']=$this->brand_model->get_all_published_category();
        $data['maincontent']= $this->load->view('admin/pages/brand/add_brand',$data,true);
        $this->load->view('admin/master',$data);
    }

    public function manage_brand() {
        $data= array();
        $data['all_brand'] = $this->brand_model->getall_brand_info();
        $data['maincontent']= $this->load->view('admin/pages/brand/manage_brand',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function save_brand(){
        $data = array();
        $data['brand_name']=$this->input->post('brand_name');
        $data['brand_stt']=$this->input->post('brand_stt');
        $data['brand_description']=$this->input->post('brand_description');
        $data['brand_publication_status']=$this->input->post('brand_publication_status');
        $brand_slug=$this->input->post('brand_name');
        $data['brand_slug'] =create_slug($brand_slug);
          $data['brand_title']=$this->input->post('brand_title');
        $data['brand_keywords']=$this->input->post('brand_keywords');
        $data['brand_description_seo']=$this->input->post('brand_description_seo');

           if (!empty($_FILES['brand_image']['name'])) {
            $config['upload_path'] = './uploads/thuonghieu/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 40960;
            $config['max_width'] = 20000;
            $config['max_height'] = 20000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('brand_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/brand');
            }
            else{
                $post_image = $this->upload->data();
               $data['brand_image'] = $post_image['file_name'];
            }
        }

          if (!empty($_FILES['brand_banner']['name'])) {
            $config['upload_path'] = './uploads/thuonghieu/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 40960;
            $config['max_width'] = 20000;
            $config['max_height'] = 20000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('brand_banner')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/brand');
            }
            else{
                $post_image = $this->upload->data();
               $data['brand_banner'] = $post_image['file_name'];
            }
        }
            $result = $this->brand_model->save_brand_info($data);
            if($result){
                 $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> Brand Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/brand');   
            }
            else{
                $this->session->set_flashdata('message','Brand Inserted Failed');
                redirect('manage/brand');
            }
        
   
        
    }
    
    public function delete_brand($id){
        $result = $this->brand_model->delete_brand_info($id);
        if ($result) {
             $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> Brand Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/brand');   
        } else {
            $this->session->set_flashdata('message', 'Brand Deleted Failed');
             redirect('manage/brand');
        }
    }
    
     public function edit_brand($id){
        $data= array();
        $data['all_published_category']=$this->brand_model->get_all_published_category();
        $data['brand_info_by_id'] = $this->brand_model->edit_brand_info($id);
        $data['maincontent']= $this->load->view('admin/pages/brand/edit_brand',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function update_brand($id){
        $data = array();
        $data['brand_name']=$this->input->post('brand_name');
        $data['brand_stt']=$this->input->post('brand_stt');
        $data['brand_description']=$this->input->post('brand_description');
        $data['brand_publication_status']=$this->input->post('brand_publication_status');
        $data['brand_slug']=$this->input->post('brand_slug');
        $data['brand_title']=$this->input->post('brand_title');
        $data['brand_keywords']=$this->input->post('brand_keywords');
        $data['brand_description_seo']=$this->input->post('brand_description_seo');

         $brand_delete_image = $this->input->post('brand_delete_image');
         $delete_image = substr($brand_delete_image, strlen(base_url()));
             if (!empty($_FILES['brand_image']['name'])){
            $config['upload_path'] = './uploads/thuonghieu/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 40960;
            $config['max_width'] = 20000;
            $config['max_height'] = 20000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('brand_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/brand');
            }
            else{
                $post_image = $this->upload->data();
                $data['brand_image'] = $post_image['file_name'];
                unlink($delete_image);
            }
           }


            if (!empty($_FILES['brand_banner']['name'])){
            $config['upload_path'] = './uploads/thuonghieu/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 40960;
            $config['max_width'] = 20000;
            $config['max_height'] = 20000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('brand_banner')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/brand');
            }
            else{
                $post_image = $this->upload->data();
                $data['brand_banner'] = $post_image['file_name'];
            }
           }

     
            $result = $this->brand_model->update_brand_info($data,$id);
            if($result){
                $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> Brand Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/brand');   
            }
            else{
                $this->session->set_flashdata('message','Brand Update Failed');
                redirect('manage/brand');
            }
    
        
    }
    
    public function published_brand($id){
        $result = $this->brand_model->published_brand_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published Brand Sucessfully');
            redirect('manage/brand');
        } else {
            $this->session->set_flashdata('message', 'Published Brand  Failed');
             redirect('manage/brand');
        }
    }
    
    public function unpublished_brand($id){
        $result = $this->brand_model->unpublished_brand_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished Brand Sucessfully');
            redirect('manage/brand');
        } else {
            $this->session->set_flashdata('message', 'UnPublished Brand  Failed');
             redirect('manage/brand');
        }
    }
    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    
    

}
