-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 06, 2023 at 05:44 PM
-- Server version: 5.7.40-log
-- PHP Version: 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `10.buffdemo.com`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_avd`
--

CREATE TABLE `tbl_avd` (
  `avd_id` int(11) NOT NULL,
  `avd_title` varchar(255) NOT NULL,
  `avd_des` text NOT NULL,
  `avd_image` varchar(255) NOT NULL,
  `avd_link` varchar(255) NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `id_cate_page` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_avd`
--

INSERT INTO `tbl_avd` (`avd_id`, `avd_title`, `avd_des`, `avd_image`, `avd_link`, `publication_status`, `id_cate_page`) VALUES
(29, 'Banner left home', '', 'vu-lan-bao-hieu-banner-nho.jpg', '', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_avd_home`
--

CREATE TABLE `tbl_avd_home` (
  `avd_home_id` int(11) NOT NULL,
  `avd_home_title` varchar(255) NOT NULL,
  `avd_home_des` text NOT NULL,
  `avd_home_image` varchar(255) NOT NULL,
  `avd_home_link` varchar(255) NOT NULL,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_baogia`
--

CREATE TABLE `tbl_baogia` (
  `baogia_id` int(11) NOT NULL,
  `baogia_name` varchar(255) NOT NULL,
  `baogia_gia` varchar(255) NOT NULL,
  `baogia_phone` varchar(11) NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `baogia_content` text NOT NULL,
  `baogia_email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_brand`
--

CREATE TABLE `tbl_brand` (
  `brand_id` int(11) NOT NULL,
  `brand_stt` text,
  `brand_slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `brand_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `brand_description` text CHARACTER SET utf8 NOT NULL,
  `brand_banner` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `brand_publication_status` tinyint(4) NOT NULL,
  `brand_title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `brand_keywords` text CHARACTER SET utf8 NOT NULL,
  `brand_description_seo` text CHARACTER SET utf8 NOT NULL,
  `brand_image` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `category_img` varchar(255) CHARACTER SET utf8 NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8 NOT NULL,
  `category_description` text CHARACTER SET utf8 NOT NULL,
  `keywords` text CHARACTER SET utf8 NOT NULL,
  `category_description_seo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `category_icon` varchar(255) CHARACTER SET utf8 NOT NULL,
  `publication_header` tinyint(4) NOT NULL DEFAULT '1',
  `publication_footer` tinyint(4) NOT NULL,
  `publication_home` int(11) NOT NULL,
  `cate_status` tinyint(4) NOT NULL COMMENT 'Published=1,Unpublished=0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `category_stt` varchar(255) DEFAULT NULL,
  `category_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `noibat` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `category_name`, `category_img`, `title`, `slug`, `category_description`, `keywords`, `category_description_seo`, `category_icon`, `publication_header`, `publication_footer`, `publication_home`, `cate_status`, `parent_id`, `category_stt`, `category_datetime`, `noibat`) VALUES
(479, 'Tour trong nước', 'danang3.jpg', 'Tour trong nước', 'tour-trong-nuoc', '<p>Tour trong nước</p>\r\n', 'Tour trong nước', 'Tour trong nước', '', 1, 0, 1, 1, 0, NULL, '2023-06-27 04:33:24', 1),
(480, 'Tour nước ngoài', '1257-2_9-le.jpg', 'Tour nước ngoài', 'tour-nuoc-ngoai', '<p>Tour nước ngo&agrave;i</p>\r\n', 'Tour nước ngoài', 'Tour nước ngoài', '', 1, 0, 1, 1, 0, NULL, '2023-06-27 07:50:15', 1),
(482, 'Tour đông bắc', '', 'Tour đông bắc', 'tour-dong-bac', '', 'Tour đông bắc', 'Tour đông bắc', '', 1, 0, 0, 1, 479, NULL, '2023-06-27 07:56:34', 0),
(483, 'Tour tây bắc', '', 'Tour tây bắc', 'tour-tay-bac', '<p><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Times New Roman&quot;,&quot;serif&quot;\"><span style=\"color:black\">Tour t&acirc;y bắc</span></span></span></p>\r\n', 'Tour tây bắc', 'Tour tây bắc', '', 1, 0, 0, 1, 479, NULL, '2023-06-27 07:56:50', 0),
(484, 'Tour hạ long', '', 'Tour hạ long', 'tour-ha-long', '', 'Tour hạ long', 'Tour hạ long', 'tour-halong.jpg', 1, 0, 0, 1, 479, NULL, '2023-06-27 07:57:05', 1),
(485, 'Tour miền trung', '', 'Tour miền trung', 'tour-mien-trung', '', 'Tour miền trung', 'Tour miền trung', 'tour-mien-trung.jpg', 1, 0, 0, 1, 479, NULL, '2023-06-27 07:57:19', 1),
(486, 'Tour an giang', '', 'Tour an giang', 'tour-an-giang', '', 'Tour an giang', 'Tour an giang', '', 1, 0, 0, 1, 479, NULL, '2023-06-27 07:57:45', 0),
(487, 'Tour phú quốc', '', 'Tour phú quốc', 'tour-phu-quoc', '', 'Tour phú quốc', 'Tour phú quốc', 'tour-phu-quoc-2-ngay-1-dem-1_1642346814.jpg', 1, 0, 0, 1, 479, NULL, '2023-06-27 07:58:04', 0),
(488, 'Tour cần thơ', '', 'Tour cần thơ', 'tour-can-tho', '', 'Tour cần thơ', 'Tour cần thơ', '', 1, 0, 0, 1, 479, NULL, '2023-06-27 07:58:15', 0),
(489, 'Tour cà mau', '', 'Tour cà mau', 'tour-ca-mau', '', 'Tour cà mau', 'Tour cà mau', '', 1, 0, 0, 1, 479, NULL, '2023-06-27 07:58:26', 0),
(491, 'Tour hàn quốc', '', 'Tour hàn quốc', 'tour-han-quoc', '', 'Tour hàn quốc', 'Tour hàn quốc', 'tou-han-quoc.jpg', 1, 0, 0, 1, 497, NULL, '2023-06-27 07:58:50', 1),
(492, 'Tour nhật bản', '', 'Tour nhật bản', 'tour-nhat-ban', '', 'Tour nhật bản', 'Tour nhật bản', 'tour-nhat-ban.jpg', 1, 0, 0, 1, 497, NULL, '2023-06-27 07:59:03', 1),
(493, 'Tour đài loan', '', 'Tour đài loan', 'tour-dai-loan', '', 'Tour đài loan', 'Tour đài loan', '', 1, 0, 0, 1, 497, NULL, '2023-06-27 07:59:18', 0),
(494, 'Tour châu âu', '', 'Tour châu âu', 'tour-chau-au', '', 'Tour châu âu', 'Tour châu âu', 'tour-chau-au.jpg', 1, 0, 0, 1, 480, NULL, '2023-06-27 07:59:30', 1),
(495, 'Tour châu mỹ ', '', 'Tour châu mỹ ', 'tour-chau-my', '', 'Tour châu mỹ ', 'Tour châu mỹ ', 'tour-chau-my.jpg', 1, 0, 0, 1, 480, NULL, '2023-06-27 07:59:43', 1),
(497, 'Tour châu á', '', 'Tour châu á', 'tour-chau-a', '', 'Tour châu á', 'Tour châu á', 'tour-chau-a.jpg', 1, 0, 0, 1, 480, NULL, '2023-07-06 02:31:25', 1),
(498, 'Tour châu úc', '', 'Tour châu úc', 'tour-chau-uc', '', 'Tour châu úc', 'Tour châu úc', 'tour-chau-uc.jpeg', 1, 0, 0, 1, 480, NULL, '2023-07-06 02:33:41', 1),
(499, 'Tour campuchia', '', 'Tour campuchia', 'tour-campuchia', '', 'Tour campuchia', 'Tour campuchia', 'tour-campuchia.jpg', 1, 0, 0, 1, 497, NULL, '2023-07-06 02:34:34', 1),
(500, 'Tour thái lan', '', 'Tour thái lan', 'tour-thai-lan', '', 'Tour thái lan', 'Tour thái lan', 'tour-thailan.jpg', 1, 0, 0, 1, 497, NULL, '2023-07-06 02:34:52', 1),
(501, 'Tour Singapore - Malaysia', '', 'Tour Singapore - Malaysia', 'tour-singapore-malaysia', '', 'Tour Singapore - Malaysia', 'Tour Singapore - Malaysia', 'tour-malaysia.jpg', 1, 0, 0, 1, 497, NULL, '2023-07-06 02:35:25', 1),
(502, 'Tour Bali - Indonesia', '', 'Tour Bali - Indonesia', 'tour-bali-indonesia', '', 'Tour Bali - Indonesia', 'Tour Bali - Indonesia', 'tour-indonedia.png', 1, 0, 0, 1, 497, NULL, '2023-07-06 02:35:59', 1),
(503, 'Tour dubai', '', 'Tour dubai', 'tour-dubai', '', 'Tour dubai', 'Tour dubai', 'tour-dubao.jpg', 1, 0, 0, 1, 497, NULL, '2023-07-06 02:36:20', 1),
(504, 'Tour Trung Quốc', '', 'Tour Trung Quốc', 'tour-trung-quoc', '', 'Tour Trung Quốc', 'Tour Trung Quốc', 'tour-trungquoc.jpeg', 1, 0, 0, 1, 497, NULL, '2023-07-06 02:36:37', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category_dmtin`
--

CREATE TABLE `tbl_category_dmtin` (
  `id` int(11) NOT NULL,
  `category_name` varchar(100) NOT NULL,
  `category_image` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `category_description` text NOT NULL,
  `keywords` text NOT NULL,
  `category_description_seo` varchar(255) NOT NULL,
  `publication_status` tinyint(4) NOT NULL COMMENT 'Published=1,Unpublished=0',
  `publication_header` tinyint(4) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `category_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category_dmtin`
--

INSERT INTO `tbl_category_dmtin` (`id`, `category_name`, `category_image`, `title`, `slug`, `category_description`, `keywords`, `category_description_seo`, `publication_status`, `publication_header`, `parent_id`, `category_datetime`) VALUES
(84, 'Visa - vé máy bay', '', 'Visa - vé máy bay', 'visa-ve-may-bay', '', 'Visa - vé máy bay', 'Visa - vé máy bay', 1, 1, 0, '2023-06-27 07:53:50'),
(85, 'Combo khách sạn', '', 'Combo khách sạn', 'combo-khach-san', '', 'Combo khách sạn', 'Combo khách sạn', 1, 1, 0, '2023-06-27 07:54:07'),
(86, 'Tin tức - sự kiện', '', 'Tin tức - sự kiện', 'tin-tuc-su-kien', '', 'Tin tức - sự kiện', 'Tin tức - sự kiện', 1, 1, 0, '2023-06-27 07:54:16'),
(87, 'Land tour', '', 'Land tour', 'land-tour', '', 'Land tour', 'Land tour', 1, 1, 0, '2023-06-27 07:54:30'),
(88, ' Mice - teambuilding', '', ' Mice - teambuilding', '-mice-teambuilding', '', ' Mice - teambuilding\r\n', ' Mice - teambuilding\r\n', 1, 1, 0, '2023-07-06 02:15:50'),
(89, 'Mice', '', 'Mice', 'mice', '', 'Mice', 'Mice', 1, 0, 88, '2023-07-06 02:22:47'),
(90, 'Teambuilding', '', 'Teambuilding', 'teambuilding', '', 'Teambuilding', 'Teambuilding', 1, 0, 88, '2023-07-06 02:23:06'),
(91, 'Visa', '', 'Visa', 'visa', '', 'Visa', 'Visa', 1, 0, 84, '2023-07-06 02:24:40'),
(92, 'Vé máy bay', '', 'Vé máy bay', 've-may-bay', '', 'Vé máy bay', 'Vé máy bay', 1, 0, 84, '2023-07-06 02:25:12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chinhsach`
--

CREATE TABLE `tbl_chinhsach` (
  `chinhsach_id` int(11) NOT NULL,
  `chinhsach_title` varchar(255) NOT NULL,
  `chinhsach_des` text NOT NULL,
  `chinhsach_image` varchar(255) NOT NULL,
  `chinhsach_link` varchar(255) NOT NULL,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_chuongtrinh`
--

CREATE TABLE `tbl_chuongtrinh` (
  `id` int(11) UNSIGNED NOT NULL,
  `date` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `thoigian` text CHARACTER SET utf8,
  `product_id` int(11) NOT NULL,
  `ansang` varchar(255) CHARACTER SET utf8 DEFAULT 'no',
  `antrua` varchar(255) CHARACTER SET utf8 DEFAULT 'no',
  `antoi` varchar(255) CHARACTER SET utf8 DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_chuongtrinh`
--

INSERT INTO `tbl_chuongtrinh` (`id`, `date`, `thoigian`, `product_id`, `ansang`, `antrua`, `antoi`) VALUES
(665, 'HCM & Các tỉnh thành - NỘI BÀI HÀ NỘI - QUẢNG BẠ - HÀ GIANG (Ăn tối )', '<p>Chiều: Qu&yacute; kh&aacute;ch tập trung tại Ga Quốc Nội - S&acirc;n bay T&acirc;n Sơn Nhất. Nh&acirc;n Vi&ecirc;n S&acirc;n Bay Viettourist l&agrave;m thủ tục đ&aacute;p chuyến bay đi H&agrave; Nội. Đến S&acirc;n Bay Nội B&agrave;i: Hướng Dẫn Vi&ecirc;n + Xe Viettourist đ&oacute;n Qu&yacute; kh&aacute;ch khởi h&agrave;nh theo đường cao tốc đi H&agrave; Giang.</p>\r\n\r\n<p>Đến H&agrave; Giang, đo&agrave;n - khởi h&agrave;nh đi Cao nguy&ecirc;n Đ&aacute; Đồng Văn. Tr&ecirc;n đường đi Qu&yacute; kh&aacute;ch thỏa sức ngắm nh&igrave;n, v&agrave; chụp ảnh tr&ecirc;n những c&aacute;nh đồng hoa Cải, hoa đ&agrave;o, hoa mận, hoa tam gi&aacute;c mạch bạt ng&agrave;n, đua nhau khoe sắc, n&uacute;i non tr&ugrave;ng điệp h&ugrave;ng vĩ thơ mộng (t&ugrave;y theo m&ugrave;a hoa nở đặc trưng).</p>\r\n\r\n<p>Đến Đồng Văn đo&agrave;n dừng ch&acirc;n tại Phố C&aacute;o, Sủng L&agrave; tại đ&acirc;y c&oacute; nhiều trẻ nhỏ g&ugrave;i hoa rất đẹp cho Qu&yacute; kh&aacute;ch chụp h&igrave;nh. Ăn tối tại nh&agrave; h&agrave;ng v&agrave; nhận ph&ograve;ng kh&aacute;ch sạn, nghỉ ngơi.</p>\r\n', 870, 'no', 'no', 'yes'),
(666, 'HÀ GIANG - PHỐ CÁO - CỘT CỜ LŨNG CÚ - MÃ PÌ LÈNG - SÔNG NHO QUẾ - YÊN MINH', '<p>S&aacute;ng: Đo&agrave;n ăn s&aacute;ng, khởi h&agrave;nh đi tham quan Cột cờ Lũng C&uacute; Nơi địa đầu Cực Bắc Tổ Quốc, hay c&ograve;n được mi&ecirc;u tả l&agrave;: &ldquo;Nơi c&uacute;i mặt s&aacute;t đất, ngẩng mặt đụng trời&rdquo;, chụp h&igrave;nh lưu niệm cột cờ Lũng C&uacute;. Qu&yacute; kh&aacute;ch c&oacute; thể ngắm phong cảnh ruộng bậc thang &amp; một trong những c&aacute;nh đồng hoa đ&agrave;o, hoa mận, hoa cải, tam gi&aacute;c mạch đẹp mắt xen kẽ những nh&agrave; tr&igrave;nh tường của d&acirc;n tộc L&ocirc; L&ocirc; trong bản S&eacute;o Lủng b&ecirc;n dưới.</p>\r\n\r\n<p>Quang cảnh v&ocirc; c&ugrave;ng cho&aacute;ng ngợp khiến bạn kh&ocirc;ng th&ocirc;i thảng thốt. Bạn sẽ bắt gặp Mỏm đ&aacute; sống ảo &ndash; Sky Walk nơi bạn c&oacute; thể leo m&igrave;nh tr&ecirc;n đỉnh, chi&ecirc;m ngưỡng cảnh sắc thơ mộng d&ograve;ng s&ocirc;ng Nho Quế xanh ngắt xa xa. Trải nghiệm tr&ecirc;n con đường xuống hẻm Tu Sản thực sự th&aacute;ch thức tất cả ch&uacute;ng ta với độ dốc từ 70 &ndash; 90 độ v&agrave; chiều d&agrave;i 1,7 km. Đăng k&yacute; tour ngồi tr&ecirc;n thuyền dạo s&ocirc;ng Nho Quế ngước nh&igrave;n l&ecirc;n những v&aacute;ch n&uacute;i dựng đứng cao hun h&uacute;t tưởng như chạm tới tận đỉnh trời hoặc chơi kayak ch&egrave;o ngược d&ograve;ng nước lững lờ ẩn hiện trong sương kh&oacute;i (Chi ph&iacute; tự t&uacute;c).</p>\r\n\r\n<p>Chiều về kh&aacute;ch sạn nhận ph&ograve;ng nghỉ ngơi. Qu&yacute; kh&aacute;ch tự do tham quan v&agrave;o l&agrave;ng văn h&oacute;a Lũng Cẩm nơi lấy bối cảnh những thước phim nổi tiếng &ldquo;Chuyện của Pao&rdquo; &ndash; v&agrave;o m&ugrave;a đ&ocirc;ng, m&ugrave;a xu&acirc;n nở rộ những c&aacute;nh đồng Tam Gi&aacute;c Mạch, Hoa Đ&agrave;o Tết, qu&aacute; th&iacute;ch hợp cho những thước h&igrave;nh l&atilde;ng mạn m&agrave; đậm n&eacute;t văn h&oacute;a&hellip; Đo&agrave;n ăn tối tại nh&agrave; h&agrave;ng - về kh&aacute;ch sạn.</p>\r\n\r\n<p>Qu&yacute; kh&aacute;ch tự do tham quan v&agrave; kh&aacute;m ph&aacute; phố cổ Đồng Văn. Được h&igrave;nh th&agrave;nh từ thế kỷ 20, mang đậm dấu ấn kiến tr&uacute;c của người Hoa, với những ng&ocirc;i nh&agrave; hai tầng lợp ng&oacute;i &acirc;m dương, những chiếc đ&egrave;n lồng đỏ treo cao&hellip;.kh&aacute;m ph&aacute; đặc sản, thưởng thức caf&eacute; Phố Cổ Đồng Văn&hellip;nghỉ đ&ecirc;m tại Đồng Văn (ở nh&agrave; s&agrave;n tập thể, homestay theo ti&ecirc;u chuẩn địa phương</p>\r\n', 870, 'no', 'yes', 'yes'),
(667, 'YÊN MINH - QUẢNG BẠ - HÀ GIANG - TUYÊN QUANG - HÀ NỘI', '<p>Buổi s&aacute;ng: Ăn s&aacute;ng tại kh&aacute;ch sạn, đo&agrave;n trả ph&ograve;ng Trở về H&agrave; Giang - Đến Quản Bạ dừng ch&acirc;n chụp h&igrave;nh tại Cổng Trời Quản Bạ v&agrave; chi&ecirc;m ngưỡng (N&Uacute;I Đ&Ocirc;I) N&uacute;i Đ&ocirc;i C&ocirc; Ti&ecirc;n &ldquo;t&aacute;c phẩm nghệ thuật&rdquo; của tạo ho&aacute; ban tặng cho v&ugrave;ng đất n&agrave;y.</p>\r\n\r\n<p>Đến H&agrave; Giang - Check-in điểm tại cột mốc km số 0 - Ăn trưa tại nh&agrave; h&agrave;ng &amp; Đo&agrave;n mua sắm đặc sản H&agrave; Giang v&ugrave;ng đất c&oacute; ch&egrave; san, rượu mật ong v&agrave; thắng cố, xứ sở của đ&agrave;o phai, hoa l&ecirc;... về l&agrave;m qu&agrave; cho gia đ&igrave;nh v&agrave; người th&acirc;n.</p>\r\n\r\n<p>Đo&agrave;n khởi h&agrave;nh về Nội B&agrave;i H&agrave; Nội nghỉ ngơi tr&ecirc;n xe - Dừng ch&acirc;n Tuy&ecirc;n Quang, xe đưa đo&agrave;n ra s&acirc;n bay Nội B&agrave;i đ&aacute;p chuyến bay về HCM hoặc c&aacute;c tỉnh th&agrave;nh kh&aacute;c trong cả nước. VIETTOURIST | Chia tay hẹn gặp lại tại những chương tr&igrave;nh sau của Viettourist. Kết th&uacute;c chương tr&igrave;nh</p>\r\n\r\n<p>Chương tr&igrave;nh Tour c&oacute; thể thay đổi trật tự c&aacute;c điểm tham quan do c&aacute;c yếu tố kh&aacute;ch quan. Trong trường hợp điểm tham quan bị đ&oacute;ng cửa do nguy&ecirc;n nh&acirc;n kh&aacute;ch quan như thời tiết, dịch bệnh&hellip;cty sẽ thay thế bằng điểm tham quan kh&aacute;c ph&ugrave; hợp</p>\r\n', 870, 'no', 'yes', 'yes'),
(668, 'HCM & HÀ NỘI – MAI CHÂU (HÒA BÌNH) - MỘC CHÂU (SƠN LA)', '<p>Qu&yacute; kh&aacute;ch tập trung tại Ga Quốc Nội - S&acirc;n bay T&acirc;n Sơn Nhất. Nh&acirc;n Vi&ecirc;n S&acirc;n Bay Viettourist hướng dẫn l&agrave;m thủ tục đáp chuyến bay đi H&agrave; Nội. Đến S&acirc;n Bay Nội B&agrave;i: Hướng Dẫn Vi&ecirc;n + Xe Viettourist đ&oacute;n Qu&yacute; kh&aacute;ch tại s&acirc;n bay &ndash; Khởi h&agrave;nh theo đường cao tốc đi&nbsp;<strong>Mộc Ch&acirc;u</strong>. Tr&ecirc;n đường đi qu&yacute; kh&aacute;ch sẽ được chi&ecirc;m ngưỡng cảnh đẹp của n&uacute;i rừng T&acirc;y Bắc với c&aacute;c l&agrave;ng Th&aacute;i ven đ&ocirc;i bờ s&ocirc;ng Đ&agrave;.&nbsp;</p>\r\n\r\n<p><br />\r\nĐo&agrave;n dừng ch&acirc;n tr&ecirc;n cung đường chữ S huyền thoại, qu&yacute; kh&aacute;ch chụp ảnh lưu niệm tại&nbsp;<strong>đ&egrave;o Thung Khe&nbsp;&ndash; H&ograve;a B&igrave;nh</strong>&nbsp;hay c&ograve;n được gọi l&agrave;&nbsp;<strong>đ&egrave;o Đ&aacute; Trắng</strong>, được&nbsp;ca ngợi l&agrave; &ldquo;&nbsp;<strong>Ch&acirc;u &Acirc;u của Tỉnh H&ograve;a B&igrave;nh</strong>&rdquo;.&nbsp; Điểm đặc biệt l&agrave; tr&ecirc;n đ&egrave;o Đ&aacute; Trắng, một ng&agrave;y c&oacute; thể được trải nghiệm đủ 4 m&ugrave;a trong năm.&nbsp;</p>\r\n\r\n<p><br />\r\nĐo&agrave;n tiếp tục khởi h&agrave;nh đi&nbsp;<strong>Mộc Ch&acirc;u</strong>, đến Mộc Ch&acirc;u đo&agrave;n ăn tối - thưởng thức đặc sản Lợn m&aacute;n, g&agrave; đồi, x&ocirc;i nếp nương&hellip;.&nbsp;nhận ph&ograve;ng kh&aacute;ch sạn nghỉ ngơi.</p>\r\n', 871, 'no', 'no', 'yes'),
(669, 'MỘC CHÂU - TÚ LỆ - ĐÈO KHAU PẠ - MÙ CANG CHẢI (Ăn 03 Bữa)', '<p><strong>Buổi s&aacute;ng:</strong>&nbsp;Sau bữa s&aacute;ng, xe v&agrave; hướng dẫn vi&ecirc;n sẽ đưa đo&agrave;n đi tham quan những vườn hoa cải V&agrave;ng nở rộ th&aacute;ng 10-11, Hoa D&atilde; Q&ugrave;y th&aacute;ng 12, Qu&yacute; kh&aacute;ch sẽ được ch&igrave;m đắm trong kh&ocirc;ng gian hoa trắng muốt của n&uacute;i rừng T&acirc;y Bắc Hoa mận, hoa mơ, hoa đ&agrave;o&nbsp;của người d&acirc;n nơi đ&acirc;y...trong thời gian trước v&agrave; sau tết. Th&aacute;ng 3 M&ugrave;a Hoa Ban nở&nbsp;-&nbsp;Qu&yacute; kh&aacute;ch chụp ảnh c&ugrave;ng c&aacute;nh đồng&nbsp;<em>hoa bạt ng&agrave;n&hellip;</em></p>\r\n\r\n<p>Tham quan<strong>&nbsp;Th&aacute;c Dải Yếm&nbsp;</strong>&ndash; một th&aacute;c nước tuyệt đẹp được thi&ecirc;n nhi&ecirc;n ưu đ&atilde;i d&agrave;nh ri&ecirc;ng cho nơi đ&acirc;y. Nh&igrave;n từ xa, th&aacute;c như một &lsquo;dải yếm&rsquo; hững hờ nối trời v&agrave; đất.</p>\r\n\r\n<p><br />\r\nĐến giờ xe v&agrave; HDV đ&oacute;n v&agrave; đưa đo&agrave;n khởi h&agrave;nh đi&nbsp;M&ugrave; Cang Chải - Đo&agrave;n dừng ch&acirc;n tại&nbsp;<strong>T&uacute; Lệ</strong>&nbsp;mảnh đất nổi tiếng với gạo nếp mềm dẻo,thơm ngon v&agrave; người con g&aacute;i Th&aacute;i Ăn trưa tại nh&agrave; h&agrave;ng tại T&uacute; Lệ.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Chinh phục đ&egrave;o Khau Pạ&nbsp;chi&ecirc;m ngưỡng cảnh đẹp khi dừng ch&acirc;n tr&ecirc;n đỉnh đ&egrave;o Khau Phạ với độ cao khoảng 2100m.&nbsp;<strong>Đ&egrave;o Khau Phạ</strong>&nbsp;theo tiếng của người d&acirc;n địa phương c&oacute; nghĩa l&agrave; Sừng Trời nơi giao h&ograve;a giữa trời v&agrave; đất. Đứng tr&ecirc;n đỉnh đ&egrave;o ngắm to&agrave;n bộ khu vực thung lũng Khau Phạ, xa xa l&agrave; c&aacute;c<strong>&nbsp;bản L&igrave;m Th&aacute;i,L&igrave;m M&ocirc;ng.<em>Tại đ&acirc;y,qu&yacute; kh&aacute;ch c&ograve;n c&oacute; cơ hội ngắm nh&igrave;n c&aacute;c vận động vi&ecirc;n chuy&ecirc;n nghiệp nhảy d&ugrave; với t&ecirc;n gọi &ldquo;bay tr&ecirc;n m&ugrave;a v&agrave;ng&rdquo;&nbsp;</em></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Đến M&ugrave; Cang Chải đo&agrave;n tham quan những c&aacute;nh đồng thửa ruộng bậc thang đẹp m&ecirc; hồn v&agrave;o m&ugrave;a L&uacute;a ch&iacute;n th&aacute;ng 09,10,11 v&agrave; m&ugrave;a nước đổ th&aacute;ng 3,4.&nbsp;<strong>Ruộng bậc thang Chế Cu Nha</strong>&nbsp;- nơi đ&acirc;y c&oacute; nếp sống đặc trưng của người H&rsquo;M&ocirc;ng. Những c&ocirc; g&aacute;i H&rsquo;M&ocirc;ng, đầu quấn khăn, da trắng n&otilde;n.&nbsp;<strong>Ruộng bậc thang Chế X&ugrave; Ph&igrave;nh, La P&aacute;n Tẩn</strong>&nbsp;&ndash; danh thắng du lịch quốc gia. Đồi th&ocirc;ng vi vu gi&oacute; ng&agrave;n l&agrave; sự l&atilde;ng mạn của đ&ocirc;i lứa y&ecirc;u nhau. C&aacute;c điểm du lịch như&nbsp;<strong>ruộng bậc thang M&acirc;m X&ocirc;i, Đồi M&oacute;ng Ngựa, ruộng bậc thang h&igrave;nh mũi giầy...&nbsp;</strong>Tham quan quanh năm đều c&oacute; vẻ đẹp ri&ecirc;ng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Ăn tối v&agrave; nhận ph&ograve;ng homestay/ kh&aacute;ch sạn nghỉ ngơi tại M&ugrave; Cang Chải.</p>\r\n', 871, 'yes', 'yes', 'yes'),
(670, 'MÙA CANG CHẢI - ĐIỆN BIÊN PHỦ - DI TÍCH LỊCH SỬ ( Ăn 3 bữa)', '<p>Đo&agrave;n ăn s&aacute;ng v&agrave; trả ph&ograve;ng khởi h&agrave;nh đi Tp. Điện Bi&ecirc;n Phủ - Đến Th&agrave;nh phố đo&agrave;n ăn trưa v&agrave; tham quan &nbsp;cụm di t&iacute;ch Mường Phăng thăm Đền Ho&agrave;ng C&ocirc;ng Chất, Bản Noọng Nhai, Sở chỉ huy chiến dịch Điện Bi&ecirc;n Phủ, hầm Đại Tướng V&otilde; Nguy&ecirc;n Gi&aacute;p, Ho&agrave;ng Văn Th&aacute;i.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tham quan Bảo T&agrave;ng lịch sử Điện Bi&ecirc;n Phủ &ndash; Bảo t&agrave;ng c&oacute; 5 khu trưng bầy với 274 hiện vật v&agrave; 122 bức tranh theo từng chủ đề: Vị tr&iacute; chiến lược, Tập đo&agrave;n cứ điểm của địch, Đường lối chỉ đạo của Đảng, Ảnh hưởng của chiến thắng Điện Bi&ecirc;n Phủ v&agrave; Điện Bi&ecirc;n Phủ ng&agrave;y nay.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Viếng Nghĩa trang liệt sỹ A1, đồi A1 &ndash; Nơi đ&acirc;y c&oacute; 644 ng&ocirc;i mộ l&agrave; những chiến sỹ qu&acirc;n d&acirc;n ta đ&atilde; hy sinh anh dũng trong chiến dịch Điện Bi&ecirc;n Phủ.&nbsp;Hầm Đờ C&aacute;t, Cầu Mường Thanh, s&ocirc;ng Nậm Rốn, Tượng đ&agrave;i chiến thắng Điện Bi&ecirc;n Phủ &ndash; Những địa danh vẫn c&ograve;n mang đầy dư &acirc;m oai h&ugrave;ng của cuộc chiến mang &yacute; nghĩa lịch sử của to&agrave;n d&acirc;n tộc.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Về nhận ph&ograve;ng kh&aacute;ch sạn nghỉ ngơi. Qu&yacute; kh&aacute;ch ăn tối tại nh&agrave; h&agrave;ng&nbsp;sau đ&oacute;&nbsp;tham quan Triển L&atilde;m Văn H&oacute;a Truyền Thống C&aacute;c D&acirc;n Tộc Tiểu Số Tỉnh Điện Bi&ecirc;n, thưởng thức d&acirc;n ca, d&acirc;n vũ, &acirc;m nhạc truyền thống c&aacute;c d&acirc;n tộc.&nbsp;Nghỉ đ&ecirc;m tại kh&aacute;ch sạn ở Điện Bi&ecirc;n.</p>\r\n', 871, 'yes', 'yes', 'yes'),
(671, 'Tp. HCM – VÂN ĐỒN – BẮC KẠN (Ăn tối)', '<p>Q&uacute;y kh&aacute;ch tập trung tại Ga Quốc Nội &ndash; S&acirc;n bay T&acirc;n Sơn Nhất. Nh&acirc;n vi&ecirc;n s&acirc;n bay Viettourist l&agrave;m thủ tục đ&aacute;p chuyến bay đi V&acirc;n Đồn (Quảng Ninh). Bay thẳng TP.HCM - V&acirc;n Đồn thời gian bay 2h10p. Đến S&acirc;n bay V&acirc;n Đồn, Xe v&agrave; hướng dẫn vi&ecirc;n đ&oacute;n đo&agrave;n khởi đi TP Bắc Kạn, Ăn tối tại nh&agrave; h&agrave;ng. Đo&agrave;n nhận ph&ograve;ng kh&aacute;ch sạn nghỉ ngơi. Nghỉ đ&ecirc;m tại TP Bắc Kạn.</p>\r\n', 872, 'no', 'no', 'yes'),
(672, 'BẮC KẠN – HỒ BA BỂ - ĐỒNG VĂN (Ăn sáng, trưa, tối)', '<p>Đo&agrave;n ăn s&aacute;ng tại kh&aacute;ch sạn, trả ph&ograve;ng, đo&agrave;n khởi h&agrave;nh tham quan Hồ Ba Bể - Đo&agrave;n dừng ch&acirc;n ở bến Buốc Lốm l&ecirc;n thuyền m&aacute;y đi v&agrave;o hồ - Thuyền dừng ở động Pu&ocirc;ng để qu&yacute; kh&aacute;ch l&ecirc;n thăm động &amp; tiếp tục xu&ocirc;i d&ograve;ng s&ocirc;ng Năng để đi v&agrave;o hồ Ba Bể. Thưởng thức đời sống thường nhật của người d&acirc;n tộc T&agrave;y ở 2 b&ecirc;n bờ s&ocirc;ng Năng. Qu&yacute; kh&aacute;ch được chi&ecirc;m ngưỡng vẻ đẹp h&ugrave;ng vĩ của s&ocirc;ng nước v&agrave; n&uacute;i rừng Ba Bể, cũng như c&aacute;c điểm thăm quan nổi tiếng như đền An M&atilde;, đảo B&agrave; G&oacute;a...Q&uacute;y kh&aacute;ch c&oacute; thể tự do bơi lội tại hồ Ba Bể, c&acirc;u c&aacute; thư gi&atilde;n hoặc tham gia chương tr&igrave;nh ch&egrave;o b&egrave; tre, chương tr&igrave;nh độc đ&aacute;o v&agrave; duy nhất tại hồ Ba Bể. Thuyền m&aacute;y tiếp tục đưa qu&yacute; kh&aacute;ch về bến thuyền của l&agrave;ng Pắc Ng&ograve;i. Từ đ&acirc;y qu&yacute; kh&aacute;ch đi bộ 1 km để tận hưởng vẻ đẹp của vườn quốc gia Ba Bể cũng như c&aacute;nh đồng l&uacute;a xanh b&aacute;t ng&aacute;t của người d&acirc;n tộc T&agrave;y. Đo&agrave;n ăn trưa tại nh&agrave; h&agrave;ng. Xe v&agrave; HDV đưa đo&agrave;n khởi h&agrave;nh đi Đồng Văn. Đến Đồng Văn, Qu&yacute; kh&aacute;ch d&ugrave;ng bữa tối tại nh&agrave; h&agrave;ng. Sau bữa tối, Qu&yacute; kh&aacute;ch tự do tản bộ v&agrave; dạo chơi, kh&aacute;m ph&aacute; đặc sản, thưởng thức caf&eacute; Phố Cổ Đồng Văn&hellip;nghỉ đ&ecirc;m tại Đồng Văn.</p>\r\n', 872, 'yes', 'yes', 'yes'),
(673, 'BẢO LẠC – ĐÈO MẺ PIA – PẮC PÓ – CAO BẰNG', '<p>Đo&agrave;n ăn s&aacute;ng, trả ph&ograve;ng. Khởi h&agrave;nh Chinh phục Đ&egrave;o MẺ PIA - Đ&egrave;o nằm tr&ecirc;n quốc lộ 4A đoạn nối từ x&atilde; Xu&acirc;n Trường đến trung t&acirc;m huyện bi&ecirc;n giới Bảo Lạc, d&agrave;i khoảng 2,5 km. Đ&acirc;y l&agrave; cung đường nổi tiếng bởi sự h&ugrave;ng vĩ bậc nhất n&uacute;i rừng Đ&ocirc;ng Bắc. Check in Top view chụp h&igrave;nh trọn vẹn Đ&egrave;o Mẻ p&iacute;a c&ograve;n gọi Đ&egrave;o Kh&acirc;u Cốc Ch&agrave; hay Dốc 14 tầng. Đo&agrave;n ăn trưa tại nh&agrave; h&agrave;ng.</p>\r\n\r\n<p>Xe đưa đo&agrave;n tham quan khu di t&iacute;ch Quốc gia đặc biệt Pắc P&oacute;, Suối L&ecirc; Nin - N&uacute;i C&aacute;c M&aacute;c. - Thăm nơi ở v&agrave; hoạt động của Chủ tịch Hồ Ch&iacute; Minh trong những năm kh&aacute;ng chiến từ 1941 đến 1945. Đo&agrave;n v&agrave;o viếng v&agrave; thắp hương tưởng niệm anh h&ugrave;ng liệt sỹ N&ocirc;ng Văn D&egrave;n (tức Kim Đồng).</p>\r\n\r\n<p>Đo&agrave;n di chuyển về th&agrave;nh phố Cao Bằng, ăn tối v&agrave; nhận ph&ograve;ng kh&aacute;ch sạn nghỉ ngơi, hoặc tự do kh&aacute;m ph&aacute; Cao Bằng về đ&ecirc;m.</p>\r\n', 872, 'yes', 'yes', 'yes'),
(674, 'TP HCM – ĐÀ NẴNG – NÚI NGŨ HÀNH SƠN – HỘI AN (Ăn trưa/ tối tự túc)', '<p>Q&uacute;y kh&aacute;ch tập trung tại&nbsp;<strong>S&Acirc;N BAY T&Acirc;N SƠN NHẤT</strong>&nbsp;&ndash; ga Quốc Nội l&agrave;m thủ tục đ&aacute;p chuyến bay đi&nbsp;<strong>Đ&Agrave; NẴNG</strong>. Đến s&acirc;n bay Đ&agrave; Nẵng, xe v&agrave; HDV đưa đo&agrave;n tham quan&nbsp;<strong>N&Uacute;I NGŨ H&Agrave;NH SƠN, CH&Ugrave;A TAM THAI, ĐỘNG HUYỀN KH&Ocirc;NG, ĐỘNG T&Agrave;NG CHƠN, L&Agrave;NG Đ&Aacute; NON NƯỚC. (Kh&ocirc;ng bao gồm v&eacute; tham quan)</strong></p>\r\n\r\n<p>Đo&agrave;n d&ugrave;ng bữa trưa tại nh&agrave; h&agrave;ng, sau đ&oacute; tự do tham quan&nbsp;<strong>kh&aacute;m ph&aacute; PHỐ CỔ HỘI AN</strong>&nbsp;di sản văn h&oacute;a thế giới được UNESCO c&ocirc;ng nhận. Tham quan&nbsp;<strong>NH&Agrave; CỔ PH&Ugrave;NG HƯNG, CH&Ugrave;A CẦU, HỘI QU&Aacute;N PH&Uacute;C KIẾN, HỘI QU&Aacute;N QUẢNG Đ&Ocirc;NG</strong>.</p>\r\n\r\n<p>Q&uacute;y kh&aacute;ch tự t&uacute;c kh&aacute;m ph&aacute; đặc sản địa phương tại&nbsp;<strong>HỘI AN</strong>. Sau đ&oacute; l&ecirc;n xe về lại&nbsp;<strong>Đ&Agrave; NẴNG</strong>&nbsp;nhận ph&ograve;ng kh&aacute;ch sạn tại Đ&agrave; Nẵng để nghỉ ngơi.</p>\r\n', 873, 'no', 'yes', 'no'),
(675, 'BÀ NÀ HILLS (Ăn sáng/ trưa tự túc/ tối)', '<p>Buổi s&aacute;ng; Đo&agrave;n ăn s&aacute;ng tại kh&aacute;ch sạn, xe v&agrave; HDV đưa đo&agrave;n đi tham quan&nbsp;<strong>KHU DU LỊCH B&Agrave; N&Agrave; HILLS (chi ph&iacute; tự t&uacute;c, gi&aacute; v&eacute; l&agrave; 1,300,000đ/kh&aacute;ch)</strong></p>\r\n\r\n<p>Du kh&aacute;ch c&oacute; mặt tại khu ga Hội An, l&agrave;m thủ tục l&ecirc;n c&aacute;p treo. Tr&ecirc;n c&aacute;p du kh&aacute;ch sẽ được ngắm cảnh n&uacute;i rừng B&agrave; N&agrave; đẹp như tranh từ tr&ecirc;n cao. Sau 15 ph&uacute;t c&aacute;p sẽ đưa du kh&aacute;ch đến GA MARSEILLE.</p>\r\n\r\n<p>Tại đ&acirc;y, Qu&yacute; kh&aacute;ch Checkin CẦU V&Agrave;NG (Golden bridge) v&agrave; tham quan c&aacute;c cảnh điểm nổi tiếng tại B&agrave; N&agrave; như:</p>\r\n\r\n<ul>\r\n	<li><strong>Trải nghiệm T&Agrave;U HỎA LEO N&Uacute;I</strong></li>\r\n	<li><strong>Thăm quan VƯỜN HOA LE JARDIN D&rsquo;AMOUR</strong></li>\r\n	<li><strong>Khu HẦM RƯỢU DEBAY thưởng thức c&aacute;c m&oacute;n rượu vang hảo hạng</strong></li>\r\n	<li><strong>Viếng thăm TƯỢNG PHẬT TH&Iacute;CH CA, CH&Ugrave;A LINH ỨNG c&ugrave;ng c&aacute;c điểm t&acirc;m linh kh&aacute;c</strong></li>\r\n	<li><strong>CON ĐƯỜNG B&Iacute;CH HỌA (d&agrave;i hơn 1 km bậc thang dẫn đến Vườn Thi&ecirc;ng rực rỡ mu&ocirc;n sắc m&agrave;u)</strong></li>\r\n</ul>\r\n\r\n<p>Qu&yacute; kh&aacute;ch sẽ thỏa th&iacute;ch trải nghiệm tuyến&nbsp;<strong>T&Agrave;U HỎA LEO N&Uacute;I&nbsp;</strong>SỐ 2 đi qua l&acirc;u đ&agrave;i mặt trăng.. Chuyến t&agrave;u n&agrave;y c&oacute; tổng chiều d&agrave;i 430m sẽ đưa du kh&aacute;ch v&agrave;o chuyến du ngoạn kh&ocirc;ng gian kỳ vĩ v&agrave; tr&aacute;ng lệ tr&ecirc;n&nbsp;ĐỈNH N&Uacute;I CH&Uacute;A. Xuất ph&aacute;t từ GA GIẾNG THẦN v&agrave; dừng lại ở GA HANG RỒNG, mở ra một h&agrave;nh tr&igrave;nh kh&aacute;m ph&aacute; xứ sở cổ t&iacute;ch diệu kỳ tại VƯƠNG QUỐC MẶT TRĂNG</p>\r\n\r\n<p>Qu&yacute; kh&aacute;ch tập trung tại ga Marseille, khởi h&agrave;nh l&ecirc;n GA LOUVRE. Qu&yacute; kh&aacute;ch tham quan&nbsp;<strong>KHU L&Agrave;NG PH&Aacute;P &ndash; FRENCH VILLAGE</strong>&nbsp;t&aacute;i hiện một nước Ph&aacute;p cổ điển v&agrave; l&atilde;ng mạn với tổ hợp c&ocirc;ng tr&igrave;nh kiến tr&uacute;c độc đ&aacute;o: quảng trường, nh&agrave; thờ, thị trấn, l&agrave;ng mạc, kh&aacute;ch sạn,&hellip; Đến với L&agrave;ng Ph&aacute;p, du kh&aacute;ch như ngược d&ograve;ng thời gian, trải nghiệm kh&ocirc;ng gian sống tinh tế v&agrave; đậm chất thơ của một trong những quốc gia l&acirc;u đời nhất thế giới.</p>\r\n\r\n<p>Buổi trưa: Qu&yacute; kh&aacute;ch sẽ d&ugrave;ng bữa trưa tại nh&agrave; h&agrave;ng Buffet Việt ti&ecirc;u chuẩn 4 sao tr&ecirc;n đỉnh B&agrave; N&agrave; ở độ cao 1.489 m với thực đơn buffet gần 100 m&oacute;n theo phong c&aacute;ch 3 miền.</p>\r\n\r\n<p>Buổi chiều: HDV sẽ đưa đo&agrave;n v&agrave;o tham gia 2 v&ograve;ng xo&aacute;y k&eacute;p duy nhất tại Việt Nam, đ&acirc;y l&agrave; lựa chọn h&agrave;ng đầu của hầu hết du kh&aacute;ch khi tham quan B&agrave; N&agrave; Hills c&ugrave;ng c&aacute;c tr&ograve; chơi tại c&ocirc;ng vi&ecirc;n với hơn 105 tr&ograve; chơi ho&agrave;n to&agrave;n miễn ph&iacute;:</p>\r\n\r\n<ul>\r\n	<li><strong>RỒNG RẮN L&Ecirc;N MẤY</strong></li>\r\n	<li><strong>V&Ograve;NG ĐUA TỬ THẦN 4D</strong></li>\r\n	<li><strong>MIỀN T&Acirc;Y HOANG D&Atilde; 5D</strong></li>\r\n	<li><strong>NG&Ocirc;I NH&Agrave; KINH DỊ</strong></li>\r\n	<li><strong>CUỘC DU H&Agrave;NH V&Agrave;O L&Ograve;NG ĐẤT</strong></li>\r\n	<li><strong>TH&Aacute;P RƠI TỰ DO</strong></li>\r\n	<li><strong>Với c&aacute;c TR&Ograve; CHƠI CẢM GI&Aacute;C MẠNH, RẠP PHIM 3D-5D, C&Ocirc;NG VI&Ecirc;N KHỦNG LONG c&ugrave;ng nhiều tr&ograve; chơi kh&aacute;c&hellip;</strong></li>\r\n</ul>\r\n\r\n<p>Sau 1 ng&agrave;y vui chơi thảo th&iacute;ch tại&nbsp;<strong>B&agrave; N&agrave; Hills</strong>. Đo&agrave;n tập trung tại tuyến c&aacute;p treo Louvre xuống lại ch&acirc;n n&uacute;i B&agrave; N&agrave; l&ecirc;n xe về lại Đ&agrave; Nẵng. Đến Đ&agrave; Nẵng đo&agrave;n d&ugrave;ng bữa tối tại nh&agrave; h&agrave;ng, nghỉ đ&ecirc;m tại kh&aacute;ch sạn.</p>\r\n', 873, 'yes', 'yes', 'no'),
(676, 'NGHỈ DƯỠNG – CITY TOUR – MUA SẮM - SÂN BAY – HCM (Ăn sáng/ trưa)', '<p>Buổi s&aacute;ng : Đo&agrave;n d&ugrave;ng bữa tại kh&aacute;ch sạn, tự do nghỉ ngơi hoặc tắm BIỂN MỸ KH&Ecirc;. Đến giờ xe v&agrave; HDV đưa đo&agrave;n tham quan&nbsp;<strong>B&Aacute;N ĐẢO SƠN TR&Agrave;, VIẾNG CH&Ugrave;A LINH ỨNG,</strong>&nbsp;<strong>TƯỢNG PHẬT NGỌC B&Iacute;CH, TƯỢNG MẸ QUAN THẾ &Acirc;M.</strong></p>\r\n\r\n<p>Ăn trưa&nbsp;<strong>ĐẶC SẢN Đ&Agrave; NẴNG</strong>&nbsp;: Thưởng thức B&aacute;nh tr&aacute;ng cuốn thịt heo hai đầu da + M&igrave; Quảng.</p>\r\n\r\n<p>Buổi chiều: Đo&agrave;n kh&aacute;m ph&aacute;<strong>&nbsp;Đ&agrave; Nẵng &ldquo;CITY TOUR&rdquo;</strong>&nbsp;được mệnh danh &ldquo;Th&agrave;nh phố đ&aacute;ng sống&rdquo; c&aacute;c điểm kh&ocirc;ng thể bỏ qua khi đến Đ&agrave; Nẵng.</p>\r\n\r\n<p>Dạo phố chụp h&igrave;nh tại c&ocirc;ng vi&ecirc;n&nbsp;<strong>C&Aacute; CH&Eacute;P H&Oacute;A RỒNG</strong>&nbsp;cạnh bờ s&ocirc;ng H&agrave;n. Check in chụp h&igrave;nh&nbsp;<strong>CẦU T&Igrave;NH Y&Ecirc;U</strong>&nbsp;với khung cảnh tr&ecirc;n cầu v&ocirc; c&ugrave;ng l&atilde;ng mạn, với những c&acirc;y cột đ&egrave;n h&igrave;nh tr&aacute;i tim, những chiếc kh&oacute;a b&ecirc;n hai lan can cầu minh chứng cho t&igrave;nh y&ecirc;u của rất nhiều cặp đ&ocirc;i từ mọi miền đất nước.</p>\r\n\r\n<p><strong>VƯỜN TƯỢNG APEC</strong>&nbsp;- c&ocirc;ng vi&ecirc;n 700 tỷ b&ecirc;n bờ s&ocirc;ng H&agrave;n, điểm nhấn ấn tượng l&agrave; c&ocirc;ng tr&igrave;nh m&aacute;i v&ograve;m h&igrave;nh c&aacute;nh diều uốn lượn như s&oacute;ng biển m&agrave;u trắng nằm ch&iacute;nh giữa c&ocirc;ng vi&ecirc;n. Đến giờ hẹn xe v&agrave; HDV đưa đo&agrave;n đi MUA SẮM v&agrave; ra s&acirc;n bay Đ&agrave; Nẵng. L&agrave;m thủ tục đ&aacute;p chuyến bay về lại TPHCM.</p>\r\n\r\n<p>Kết th&uacute;c h&agrave;nh tr&igrave;nh tour du lịch Đ&agrave; Nẵng 3 ng&agrave;y 2 đ&ecirc;m. Hướng dẫn vi&ecirc;n c&ocirc;ng ty Du Lịch Viettourist chia tay ch&uacute;c sức khỏe, tạm biệt v&agrave; hẹn ng&agrave;y gặp lại.</p>\r\n\r\n<p><em><strong>Chương tr&igrave;nh Tour c&oacute; thể thay đổi trật tự c&aacute;c điểm tham quan do c&aacute;c yếu tố kh&aacute;ch quan, Nhưng vẫn cam kết tham quan đầy đủ chương tr&igrave;nh.Trong trường hợp điểm tham quan bị đ&oacute;ng cửa do nguy&ecirc;n nh&acirc;n kh&aacute;ch quan như thời tiết, dịch bệnh&hellip;Cty sẽ thay thế bằng điểm tham quan kh&aacute;c ph&ugrave; hợp.</strong></em></p>\r\n', 873, 'yes', 'yes', 'no'),
(677, 'HCM – NARITA AIRPORT - Nghỉ đêm trên máy bay', '<p>Trưởng đo&agrave;n c&ocirc;ng ty đ&oacute;n Qu&yacute; kh&aacute;ch ở điểm hẹn tại s&acirc;n bay&nbsp;<strong>T&acirc;n Sơn Nhất&nbsp;</strong>đ&aacute;p chuyến bay thẳng đi thủ đ&ocirc;&nbsp;<strong>Tokyo &ndash; Nhật Bản</strong>. Đo&agrave;n nghỉ đ&ecirc;m tr&ecirc;n m&aacute;y bay.</p>\r\n\r\n<p><strong>Chuyến đi: SGN - NTR (S&acirc;n bay NARITA - TOKYO): Giờ đi l&uacute;c 23:40</strong></p>\r\n\r\n<p><strong>Chuyến về: NTR (S&acirc;n bay NARITA - TOKYO) - SGN : Giờ đi l&uacute;c 08:55</strong></p>\r\n', 874, 'no', 'no', 'no'),
(678, 'NARITA - KHÁM PHÁ PHỐ CỔ NARITASAN - MUA SẮM AEON MALL (Ăn buffet 4sao phong cách Nhật Bản)', '<p>S&aacute;ng đến s&acirc;n bay, đo&agrave;n l&agrave;m thủ tục nhập cảnh, xe v&agrave; HDV đưa đo&agrave;n về kh&aacute;ch sạn vệ sinh c&aacute; nh&acirc;n &amp; ăn buffet tiểu chuẩn 4Sao cao cấp Nhật Bản l&uacute;c 10h s&aacute;ng - Sau bữa buffet Xe v&agrave; HDV đưa đo&agrave;n đi tham quan&nbsp;<strong>phố cổ Naritasan</strong>&nbsp;(&nbsp;Trong tiếng Nhật, Naritasan c&oacute; nghĩa l&agrave; n&uacute;i narita, thể hiện cho độ cao của ng&ocirc;i ch&ugrave;a, c&ograve;n Shinsho-ji c&oacute; nghĩa l&agrave; &ldquo; Ng&ocirc;i ch&ugrave;a chiến thắng&rdquo;, l&agrave; một quần thể Phật gi&aacute;o đồ sộ với nhiều c&ocirc;ng tr&igrave;nh kiến tr&uacute;c được x&acirc;y dựng qua nhiều thời đại. Ng&ocirc;i ch&ugrave;a ch&iacute;nh được khởi c&ocirc;ng x&acirc;y dựng v&agrave;o năm 940 để tưởng nhớ chiến thắng của một vị tướng qu&acirc;n.)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Narita được gh&eacute;p lại từ t&ecirc;n hai ng&ocirc;i l&agrave;ng Narimune v&agrave; Tabata. Trong suốt thời kỳ Edo (1603 &ndash; 1868), đ&acirc;y l&agrave; v&ugrave;ng đất của c&aacute;c v&otilde; sĩ đạo samurai v&agrave; c&aacute;c tướng qu&acirc;n shogun dồng thời cũng l&agrave; khu vực bu&ocirc;n b&aacute;n sầm uất. Nhưng lịch sử h&igrave;nh th&agrave;nh của v&ugrave;ng đất Narita được biết đến từ xa x&ocirc;i hơn, một trong những dấu t&iacute;ch c&ograve;n lại l&agrave; ng&ocirc;i ch&ugrave;a ng&agrave;n năm tuổi Naritasan Shinsho-ji được x&acirc;y dựng từ năm 940 đến nay vẫn l&agrave; một trong những h&agrave;nh hương quan trọng của đất nước mặt trời mọc.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Chi&ecirc;m b&aacute;i&nbsp;<strong>ch&ugrave;a ng&agrave;n năm tuổi Naritasan Shinshoji&nbsp;</strong>- Q&uacute;y kh&aacute;ch thong thả rảo bước tr&ecirc;n con đường nhỏ Omote-Sando dẫn đến ch&ugrave;a Naritasan xung quanh l&agrave; những ng&ocirc;i nh&agrave; gỗ được x&acirc;y dựng theo kiểu truyền thống, những cửa h&agrave;ng b&aacute;n đồ lưu niệm, những tiệm ăn với kiểu trang tr&iacute; mang đậm m&agrave;u sắc Nhật Bản c&ugrave;ng với hương vị đặc trưng c&aacute;c loại đặc sản v&ugrave;ng Narita.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Q&uacute;y kh&aacute;ch tự t&uacute;c thưởng thức đặc sản của v&ugrave;ng Narita, ch&iacute;nh l&agrave; lươn. Những cửa h&agrave;ng cơm lươn s&aacute;t tr&ecirc;n phố Omote-Santo. N&eacute;t độc đ&aacute;o của&nbsp;cơm lươn ở đ&acirc;y l&agrave; thực kh&aacute;ch được nh&igrave;n tận mắt m&oacute;n ăn của m&igrave;nh được chế biến như thế n&agrave;o. Ch&eacute;n cơm lươn nướng sốt teriyaki n&oacute;ng hổi, thơm lừng</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Chiều đo&agrave;n mua sắm tại trung t&acirc;m thương mại h&agrave;ng NHẬT &amp; về kh&aacute;ch sạn 4 sao nhận ph&ograve;ng nghỉ ngơi &amp; buổi tối c&ugrave;ng HDV kh&aacute;m ph&aacute; phố cổ Narita về đ&ecirc;m tự do thưởng thức ẩm thực truyền thống Nhật Bản ( chi ph&iacute; tự t&uacute;c)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><a href=\"https://www.mystays.com/en-us/hotel-mystays-premier-narita-chiba/\">Kh&aacute;ch sạn 4SAO MYSTAYS</a>&nbsp;hoặc tương đương</strong></p>\r\n', 874, 'no', 'no', 'no'),
(679, 'NARITA - NÚI PHÚ SĨ - LÀNG CỔ OSHINO HAKKAI - THỦ ĐÔ TOKYO', '<p>Sau bữa s&aacute;ng Buffet - Xe v&agrave; HDV đ&oacute;n đo&agrave;n khởi h&agrave;nh đi&nbsp;<strong>Yamanashi&nbsp;</strong>c&aacute;ch Tokyo hơn 2h xe &ndash; Nổi tiếng bởi đ&acirc;y l&agrave; qu&ecirc; hương của N&uacute;i Ph&uacute; Sĩ. Đến Yamanashi đo&agrave;n&nbsp;tham quan:<strong>&nbsp;Cảnh sắc 4 m&ugrave;a hồ Kawaguchi, C&ocirc;ng vi&ecirc;n HANANOMIYAKO</strong>.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Cảnh sắc 4 m&ugrave;a hồ Kawaguchi</strong>&nbsp;m&ugrave;a xu&acirc;n c&oacute; thể ngắm hoa anh đ&agrave;o, hoa&nbsp;<strong>Mitsubatsutsuji</strong>&nbsp;l&agrave;m kh&ocirc;ng gian nhuốm sắc đỏ, sắc hồng, V&agrave;o m&ugrave;a h&egrave;, hoa Lavandula (oải hương H&agrave; Lan) m&agrave;u t&iacute;m khoe sắc đua nở ven bờ hồ, tạo ra một quang cảnh thật dễ chịu v&agrave; thoải m&aacute;i. V&agrave;o m&ugrave;a thu c&oacute; l&aacute; đỏ với nhiều m&agrave;u sắc kh&aacute;c nhau tạo ra một quang cảnh thật đẹp được nhuộm bởi m&agrave;u đỏ, m&agrave;u v&agrave;ng&hellip;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Tham quan Oshino Hakkai</strong>&nbsp;l&agrave; ng&ocirc;i l&agrave;ng cổ nằm y&ecirc;n b&igrave;nh dưới ch&acirc;n n&uacute;i Ph&uacute; Sĩ mang n&eacute;t kiến tr&uacute;c xa xưa &amp; tham quan&nbsp;<strong>Bảo T&agrave;ng Động Đất</strong>, mua sắm h&agrave;ng miễn thuế.&nbsp;Sau bữa trưa, đo&agrave;n khởi h&agrave;nh về Tokyo tham quan :</p>\r\n\r\n<p><strong>- Tượng Nữ Thần Tự Do</strong>&nbsp;&ndash; m&ocirc;t bản sao biểu tươṇg của New York được ch&iacute;nh quyền Ph&aacute;p mang đến trong dịp kỉ ni&ecirc;m năm Ph&aacute;p ở Nhật</p>\r\n\r\n<p><strong>- Ho&agrave;ng cung Tokyo&nbsp;</strong>nơi cư tr&uacute; ch&iacute;nh của Ho&agrave;ng gia Nhật Bản &amp; c&ocirc;ng vi&ecirc;n ngắm hoa Anh Đ&agrave;o ( đo&agrave;n tham quan b&ecirc;n ngo&agrave;i)</p>\r\n\r\n<p><strong>- Asakusa Kannon -&nbsp;</strong>ng&ocirc;i đền linh thi&ecirc;ng cổ xưa nhất của Tokyo: Asakusa hay c&ograve;n gọi l&agrave; đền Sensoji đặc biệt thu h&uacute;t người d&acirc;n Nhật Bản tới cầu nguyện v&agrave;o đ&ecirc;m giao thừa cũng như trong những ng&agrave;y đầu năm mới</p>\r\n\r\n<p><strong>- Th&aacute;p truyền h&igrave;nh Tokyo Skytree</strong>&nbsp;(chỉ chụp h&igrave;nh)</p>\r\n\r\n<p>Đo&agrave;n ăn tối tại nh&agrave; h&agrave;ng v&agrave; về kh&aacute;ch sạn nghỉ ngơi hoặc c&ugrave;ng HDV trải nghiệm kh&aacute;m ph&aacute; c&aacute;c con phố nhộn nhịp&nbsp;mua sắm, giải tr&iacute; về đ&ecirc;m theo phong c&aacute;ch người Nhật - di chuyển bằng c&aacute;c loại phương tiện c&ocirc;ng cộng như Xe bus, t&agrave;u điện ngầm... để cảm nhận cuộc sống Nhật Bản.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Đo&agrave;n nghỉ đ&ecirc;m tại&nbsp;<strong><a href=\"https://www.mystays.com/en-us/hotel-mystays-premier-narita-chiba/\">Kh&aacute;ch sạn 4SAO MYSTAYS</a>&nbsp;hoặc tương đương</strong></p>\r\n', 874, 'yes', 'yes', 'yes'),
(680, 'TP HCM/HÀ NỘI – OSAKA (Nghỉ đêm trên máy bay)', '<p>Hướng dẫn vi&ecirc;n c&ocirc;ng ty Viettourist đ&oacute;n qu&yacute; kh&aacute;ch tại điểm hẹn khởi h&agrave;nh ra s&acirc;n bay đ&aacute;p chuyến bay đi&nbsp;<strong>Osaka</strong>&nbsp;điểm giao thương đường thủy của nhiều quốc gia, n&ecirc;n c&ograve;n được mệnh danh l&agrave; &ldquo;th&agrave;nh phố nước&rdquo;, th&agrave;nh phố ph&aacute;t triển cực kỳ hưng thịnh, l&agrave; trung t&acirc;m kinh tế, dịch vụ, gi&aacute;o dục cấp cao ở Nhật Bản.&nbsp;<strong>Osaka</strong>&nbsp;hấp dẫn du kh&aacute;ch bởi vẻ đẹp độc đ&aacute;o mang đậm phong c&aacute;ch truyền thống kết hợp hiện đại&hellip;tham quan&nbsp;<strong>Osaka</strong>&nbsp;để thấy được yếu tố phong thủy kết hợp hai yếu tố n&uacute;i v&agrave; s&ocirc;ng nước (sơn thủy) khắp nơi từ nh&agrave; ở cho đến c&ocirc;ng vi&ecirc;n, vườn hoa, l&acirc;u đ&agrave;i v&agrave; c&aacute;c c&ocirc;ng tr&igrave;nh lớn kh&aacute;c h&agrave;i h&ograve;a với thi&ecirc;n nhi&ecirc;n, m&ocirc;i trường sống xung quanh.</p>\r\n\r\n<p>Trung t&acirc;m của v&ugrave;ng&nbsp;<strong>Kansai</strong>, v&ugrave;ng n&agrave;y bao gồm bảy tỉnh:&nbsp;<strong>Nara, Wakayama, Kyoto, Osaka, Hyogo, Mie, Shiga</strong>. Chương tr&igrave;nh tour độc đ&aacute;o đưa q&uacute;y kh&aacute;ch tham quan 5/7 tỉnh th&agrave;nh phố của&nbsp;<strong>Kansai</strong>&nbsp;gồm : Osaka + Kyoto +Kyoto +Nara + Wakayama&nbsp;(thuộc tỉnh Hyogo). Q&uacute;y kh&aacute;ch nghỉ đ&ecirc;m tr&ecirc;n m&aacute;y bay.</p>\r\n\r\n<p>Chuyến bay dự kiến h&agrave;ng kh&ocirc;ng VJ:</p>\r\n\r\n<p>Chuyến đi:&nbsp;<strong>SGN - KIX ( OSAKA)</strong>&nbsp;: Giờ đi l&uacute;c 01:00&nbsp; - Thời gian bay 05h30 - Giờ đến 08h30&nbsp;</p>\r\n\r\n<p>Chuyến về:&nbsp;<strong>KIX (OSAKA) - SGN</strong>&nbsp;:&nbsp;Giờ đi l&uacute;c 09:30&nbsp; - Thời gian bay 05h30 - Giờ đến 13h00&nbsp;</p>\r\n', 875, 'no', 'no', 'no'),
(681, 'LÂU ĐÀI OSAKA - CỐ ĐÔ KYOTO (Ăn 3 bữa)', '<p>S&aacute;ng đến s&acirc;n bay&nbsp;<strong>Kansai&nbsp;</strong>được x&acirc;y dựng tr&ecirc;n h&ograve;n đảo nh&acirc;n tạo với tổng diện t&iacute;ch 511 ha. H&ograve;n đảo nằm tr&ecirc;n vịnh&nbsp;<strong>Osaka&nbsp;</strong>c&aacute;ch đất liền 5 km, Để ho&agrave;n th&agrave;nh c&ocirc;ng tr&igrave;nh tạo đảo v&agrave; x&acirc;y dựng s&acirc;n bay, người Nhật đ&atilde; phải mất 20 năm trời với 1500 tỉ Y&ecirc;n ph&iacute; tổn. Đ&acirc;y l&agrave; một trong những s&acirc;n bay đầu ti&ecirc;n tr&ecirc;n biển &amp; nằm trong danh s&aacute;ch 10 c&ocirc;ng tr&igrave;nh kiến tr&uacute;c ấn tượng nhất thi&ecirc;n ni&ecirc;n kỷ.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Đo&agrave;n l&agrave;m thủ tục nhập cảnh v&agrave; ăn nhẹ tại s&acirc;n bay - Xe v&agrave; HDV địa phương đ&oacute;n v&agrave; đưa đo&agrave;n đi&nbsp;Osaka tham quan:&nbsp;<strong>L&acirc;u đ&agrave;i Osaka</strong>&nbsp;hay c&ograve;n gọi l&agrave;&nbsp;<strong>l&acirc;u đ&agrave;i Hạc Trắng.</strong>&nbsp;Đ&acirc;y l&agrave; một điểm du lịch v&ocirc; c&ugrave;ng hấp dẫn v&agrave; thu h&uacute;t h&agrave;ng trăm ngh&igrave;n kh&aacute;ch du lịch Nhật Bản đến đ&acirc;y mỗi năm. Toạ lạc giữa những khu vườn thơ mộng 4 m&ugrave;a hoa nở.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Tham quan&nbsp;<strong>Shinsaibashi&nbsp;</strong>khu phố sầm uất<strong>&nbsp;</strong>l&agrave; điểm mua sắm h&agrave;ng đầu ở Osaka, với sự pha trộn của cửa h&agrave;ng b&aacute;ch h&oacute;a v&agrave; cửa h&agrave;ng b&aacute;n lẻ v&agrave; bạn c&oacute; thể t&igrave;m thấy c&aacute;c vật phẩm ở tất cả c&aacute;c mức gi&aacute; - Đo&agrave;n ăn trưa tại nh&agrave; h&agrave;ng</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Sau bữa trưa đo&agrave;n khởi h&agrave;nh đi&nbsp;<strong>cố đ&ocirc; Kyoto</strong>&nbsp;th&agrave;nh phố c&oacute; sự kết hợp ho&agrave;n hảo giữa cổ điển v&agrave; hiện đại, những con phố mua sắm nhộn nhịp v&agrave; h&igrave;nh ảnh c&aacute;c geisha y&ecirc;u kiều&hellip; đo&agrave;n check in&nbsp;cầu&nbsp;<strong>Togetsu&nbsp;</strong>-&nbsp;cảnh sắc xung quanh thay đổi&nbsp;theo từng m&ugrave;a như hoa anh đ&agrave;o v&agrave;o m&ugrave;a xu&acirc;n, rừng c&acirc;y lộc biếc v&agrave;o m&ugrave;a h&egrave;, l&aacute; phong đỏ v&agrave;o m&ugrave;a thu v&agrave; tuyết trắng v&agrave;o m&ugrave;a đ&ocirc;ng, đ&acirc;y đ&iacute;ch thị l&agrave; một địa điểm l&yacute; tưởng để du kh&aacute;ch vừa thong thả tản bộ thư gi&atilde;n - Tiếp đến đo&agrave;n tham quan :&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Ch&ugrave;a Nước (Kyomizu)</strong>&nbsp;c&ograve;n gọi l&agrave; Ch&ugrave;a Thanh Thủy được x&acirc;y dựng v&agrave;o đầu thời kỳ Nara (năm 778) Một trong những ngồi ch&ugrave;a linh thi&ecirc;ng nổi tiếng nhất Nhật Bản. H&agrave;ng năm ch&agrave;o đ&oacute;n khoảng 3 triệu người đến c&uacute;ng b&aacute;i. Nơi n&agrave;y c&oacute; 3 ngọn th&aacute;c m&agrave; người Nhật rất linh nghiệm về &quot;trường thọ&quot;, &quot;t&igrave;nh duy&ecirc;n&quot;, &quot;học h&agrave;nh th&agrave;nh đạt&quot;, nếu người chi&ecirc;m b&aacute;i uống một ngụm nước ở một trong ba d&ograve;ng nước tr&ecirc;n th&igrave; điềm may sẽ đến. Ngược lại, nếu uống 2 ngụm th&igrave; sự linh ứng sẽ giảm đi một nữa, uống 3 ngụm th&igrave; vận tốt chỉ c&ograve;n một phần ba.&nbsp;Hơn nữa, nếu tham lam m&agrave; uống nước ở cả 3 d&ograve;ng th&igrave; ho&agrave;n to&agrave;n sẽ kh&ocirc;ng linh nghiệm. Điều n&agrave;y được đ&uacute;c kết th&agrave;nh lời răn dạy từ xa xưa. Trước khi uống nước thi&ecirc;ng ở d&ograve;ng th&aacute;c, h&atilde;y chắp tay khấn v&aacute;i cư sĩ Gyoei đang được thờ ph&iacute;a sau th&aacute;c nước để thể hiện l&ograve;ng t&ocirc;n trọng trang nghi&ecirc;m v&agrave; th&agrave;nh t&acirc;m muốn xin d&ograve;ng nước tinh khiết n&agrave;y.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Ch&ugrave;a V&agrave;ng (Kinkaku-ji)</strong>, được mệnh danh l&agrave; T&acirc;y Kinh của nước Nhật, ng&ocirc;i ch&ugrave;a v&agrave;ng Kinkakuji được x&acirc;y dựng từ thế kỷ 14 v&agrave; Ch&ugrave;a c&ograve;n được gọi với một t&ecirc;n gọi kh&aacute;c l&agrave; Ch&ugrave;a Rokounjin- Du kh&aacute;ch đến đ&acirc;y đều phải sửng sốt.&nbsp;To&agrave;n bộ mặt trong v&agrave; mặt ngo&agrave;i của khối kiến tr&uacute;c ba tầng đồ sộ n&agrave;y đều được d&aacute;t v&agrave;ng &oacute;ng &aacute;nh, biến nơi đ&acirc;y trở th&agrave;nh một trong những thắng cảnh hấp dẫn nhất xứ Ph&ugrave; Tang. Từng được coi l&agrave; quốc bảo v&agrave; được Unesco c&ocirc;ng nhận l&agrave; di sản văn h&oacute;a thế giới . N&eacute;t đặc trưng nhất về sự tinh xảo v&agrave; cầu k&igrave; của ng&ocirc;i ch&ugrave;a v&agrave;ng&nbsp;<strong>Kinkakuji</strong>&nbsp;n&agrave;y ch&iacute;nh l&agrave; một vị thế rất ấn tượng giữa những t&aacute;n xanh của c&acirc;y l&aacute; v&agrave; &aacute;nh s&aacute;ng tinh khiết phản chiếu của hồ nước tĩnh lặng.&nbsp;Ch&ugrave;a v&agrave;ng ch&iacute;nh l&agrave; một biểu tượng c&oacute; gi&aacute; trị về tinh thần, đ&atilde; từng l&agrave; một&nbsp;<strong>Shariden (Đền X&aacute; lị)&nbsp;</strong>&ndash; di t&iacute;ch của Phật gi&aacute;o. Ng&agrave;y nay, to&agrave;n bộ ng&ocirc;i ch&ugrave;a ngoại trừ tầng trệt đều được bọc bằng những l&aacute; v&agrave;ng nguy&ecirc;n chất, khiến cho ng&ocirc;i ch&ugrave;a c&oacute; gi&aacute; trị v&ocirc; c&ugrave;ng lớn. Nơi du kh&aacute;ch thập phương d&ugrave; l&agrave; người Nhật hay người nước ngo&agrave;i đều một lần muốn gh&eacute; thăm.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Đo&agrave;n Ăn tối nhận ph&ograve;ng nghỉ ngơi tại kh&aacute;ch sạn KYOTO - Tự do kh&aacute;m ph&aacute; văn h&oacute;a, ẩm thực truyền thống cố đ&ocirc; về đ&ecirc;m c&ugrave;ng HDV.</p>\r\n', 875, 'yes', 'yes', 'yes'),
(682, 'KYOTO - YAMANASHI - NÚI PHÚ SĨ - YAMANASHI (Ăn 3 bữa)', '<p>Đo&agrave;n ăn s&aacute;ng, trả ph&ograve;ng - khởi h&agrave;nh đi&nbsp; YAMANASHI &ndash; Nổi tiếng bởi đ&acirc;y l&agrave; Qu&ecirc; hương của N&Uacute;I PH&Uacute; SỸ. Th&agrave;nh phố Yamanashi mệnh danh l&agrave; &ldquo;vương quốc tr&aacute;i c&acirc;y&rdquo;, bốn m&ugrave;a c&acirc;y xanh l&aacute;, tr&aacute;i ch&iacute;n ngọt c&agrave;nh. Tr&aacute;i c&acirc;y hầu như mọc quanh năm, m&ugrave;a n&agrave;o thức ấy, mỗi loại mang một hương vị ri&ecirc;ng, Yamanashi, nơi nổi tiếng với sản lượng đ&agrave;o, l&ecirc; v&agrave; nho lớn nhất Nhật Bản.&nbsp;Trong đ&oacute; đặc biệt nhất l&agrave; c&acirc;y anh đ&agrave;o &ldquo;Sakuranbo&rdquo;. Đến đ&acirc;y v&agrave;o khoảng th&aacute;ng 7, 8, bạn sẽ cho&aacute;ng ngợp trước những khu vườn anh đ&agrave;o rực rỡ m&agrave;u đỏ ch&iacute;n mọng trong veo như ngọc nở rộ&nbsp;-&nbsp;Đo&agrave;n ăn trưa tại nh&agrave; h&agrave;ng &amp; tham quan:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Cảnh sắc 4 m&ugrave;a hồ Kawaguchi</strong>&nbsp;m&ugrave;a xu&acirc;n c&oacute; thể ngắm hoa anh đ&agrave;o, hoa&nbsp;<strong>Mitsubatsutsuji</strong>&nbsp;l&agrave;m kh&ocirc;ng gian nhuốm sắc đỏ, sắc hồng, V&agrave;o m&ugrave;a h&egrave;, hoa Lavandula (oải hương H&agrave; Lan) m&agrave;u t&iacute;m khoe sắc đua nở ven bờ hồ, tạo ra một quang cảnh thật dễ chịu v&agrave; thoải m&aacute;i. V&agrave;o m&ugrave;a thu c&oacute; l&aacute; đỏ với nhiều m&agrave;u sắc kh&aacute;c nhau tạo ra một quang cảnh thật đẹp được nhuộm bởi m&agrave;u đỏ, m&agrave;u v&agrave;ng&hellip;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Oshino Hakkai</strong>&nbsp;l&agrave; ng&ocirc;i l&agrave;ng cổ nằm y&ecirc;n b&igrave;nh dưới ch&acirc;n n&uacute;i Ph&uacute; Sĩ Tuyết trắng bao phủ ng&ocirc;i l&agrave;ng v&agrave;o m&ugrave;a đ&ocirc;ng v&agrave; rực rỡ hoa anh Đ&agrave;o v&agrave;o m&ugrave;a xu&acirc;n. Cho đến ng&agrave;y nay, những ng&ocirc;i nh&agrave; ở l&agrave;ng Oshino Hakkai vẫn mang n&eacute;t kiến tr&uacute;c truyền thống xa xưa Nhật Bản.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Về kh&aacute;ch sạn khu vực&nbsp;<strong>Hakone</strong>&nbsp;&ndash; Thi&ecirc;n đường du lịch của Nhật Bản Được mệnh l&agrave; Venice của Nhật Bản, Điểm đến tuyệt vời sở hữu rất nhiều cảnh quan l&agrave;m&nbsp; m&ecirc;&nbsp; đắm&nbsp; l&ograve;ng&nbsp; người.&nbsp;Đến&nbsp;<strong>Hakone&nbsp;</strong>nhận ph&ograve;ng nghỉ ngơi tắm suối kho&aacute;ng n&oacute;ng Osen kiểu Nhật thi&ecirc;n nhi&ecirc;n tại resort chuẩn 4 sao. Đo&agrave;n&nbsp;ăn tối buffet tại kh&aacute;ch sạn - Nghỉ đ&ecirc;m tại&nbsp;<strong>Hakone</strong></p>\r\n', 875, 'yes', 'yes', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_city`
--

CREATE TABLE `tbl_city` (
  `id` int(11) NOT NULL,
  `city_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `cate_status` tinyint(4) NOT NULL COMMENT 'Published=1,Unpublished=0',
  `city_stt` int(11) DEFAULT NULL,
  `price_ship` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_color`
--

CREATE TABLE `tbl_color` (
  `color_id` int(11) NOT NULL,
  `color_name` varchar(255) NOT NULL,
  `color_code` varchar(255) NOT NULL,
  `color_description` text,
  `color_image` varchar(255) DEFAULT NULL,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment`
--

CREATE TABLE `tbl_comment` (
  `comment_id` int(11) NOT NULL,
  `comment_start` int(11) NOT NULL,
  `comment_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `comment_email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `comment_content` longtext CHARACTER SET utf8 NOT NULL,
  `comment_answer` text CHARACTER SET utf8 NOT NULL,
  `comment_product_id` int(11) NOT NULL,
  `comment_show` tinyint(4) NOT NULL,
  `comment_show_answer` tinyint(4) NOT NULL,
  `comment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comment_images` text CHARACTER SET utf8,
  `comment_title` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_comment`
--

INSERT INTO `tbl_comment` (`comment_id`, `comment_start`, `comment_name`, `comment_email`, `comment_content`, `comment_answer`, `comment_product_id`, `comment_show`, `comment_show_answer`, `comment_date`, `comment_images`, `comment_title`) VALUES
(85, 5, 'Nguyễn văn quốc', 'nguyenvanmup@gmail.com', 'Tour chất lượng', '', 873, 1, 0, '2023-06-07 11:01:55', NULL, NULL),
(87, 5, 'Thu Hà', 'thuha556@gmail.com', 'Ok', '', 873, 1, 0, '2023-06-07 11:01:55', NULL, NULL),
(89, 5, 'Bùi rực rỡ', 'buirucro@gmail.com', 'tôi thích Đặt tour tại đây', '<p>Cám ơn anh đã để lại phản hồi cho công ty, được phục vụ tốt nhất cho anh là điều anh em trong công ty luôn cố gắng không ngừng. Rất vui được sự ủng hộ cùng anh</p>', 873, 1, 1, '2023-06-07 11:01:55', NULL, NULL),
(108, 5, 'Nguyễn Thị Đẹp', '', 'Cảm ơn các hướng dẫn viên rất nhiều. Nhờ các bạn mà mình đã kết thúc chuyến đi một cách vui vẻ và thoải mái.', '', 875, 1, 0, '2023-07-01 02:02:54', NULL, NULL),
(109, 5, 'Lê Thị Triêu Anh', '', 'Tư vấn nhiệt tình. Thanh toán nhanh gọn', '', 875, 1, 0, '2023-07-01 02:03:21', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment_news`
--

CREATE TABLE `tbl_comment_news` (
  `cmt_id` int(11) NOT NULL,
  `comment_name` varchar(255) NOT NULL,
  `comment_email` varchar(255) NOT NULL,
  `comment_content` text NOT NULL,
  `comment_answer` text NOT NULL,
  `comment_count` int(11) NOT NULL,
  `comment_news_id` int(11) NOT NULL,
  `comment_show` tinyint(4) NOT NULL,
  `comment_show_answer` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_comment_news`
--

INSERT INTO `tbl_comment_news` (`cmt_id`, `comment_name`, `comment_email`, `comment_content`, `comment_answer`, `comment_count`, `comment_news_id`, `comment_show`, `comment_show_answer`) VALUES
(65, 'Cát Tường', 'cattuong.achau@gmail.com', 'Bài viết rất hay và bổ ích', '<p>Cám ơn anh đã để lại phản hồi cho công ty, được phục vụ tốt nhất cho anh là điều anh em trong công ty luôn cố gắng không ngừng. Rất vui được sự ủng hộ cùng anh</p>', 0, 386, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment_news_rate`
--

CREATE TABLE `tbl_comment_news_rate` (
  `comment_news_rate_id` int(11) NOT NULL,
  `comment_news_rate_start` int(11) NOT NULL,
  `comment_news_rate_name` varchar(255) NOT NULL,
  `comment_news_rate_email` varchar(255) NOT NULL,
  `comment_news_rate_content` varchar(255) NOT NULL,
  `comment_news_rate_answer` text NOT NULL,
  `comment_news_rateid` int(11) NOT NULL,
  `comment_news_rate_show` tinyint(4) NOT NULL,
  `comment_news_rate_show_answer` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment_product`
--

CREATE TABLE `tbl_comment_product` (
  `cmt_id` int(11) NOT NULL,
  `comment_name` varchar(255) NOT NULL,
  `comment_email` varchar(255) NOT NULL,
  `comment_content` text NOT NULL,
  `comment_answer` text NOT NULL,
  `comment_count` int(11) NOT NULL,
  `comment_product_id` int(11) NOT NULL,
  `comment_show` tinyint(4) NOT NULL,
  `comment_show_answer` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_comment_product`
--

INSERT INTO `tbl_comment_product` (`cmt_id`, `comment_name`, `comment_email`, `comment_content`, `comment_answer`, `comment_count`, `comment_product_id`, `comment_show`, `comment_show_answer`) VALUES
(69, 'Văn thanh', 'dangthanh.ctrlmedia@gmail.com', 'Tôi rất hài lòng khi đặt tour tại đây', '', 0, 873, 1, 0),
(70, 'Pham Diem', 'diempham20121997@gmail.com', 'chất lượng', '', 0, 873, 1, 0),
(75, 'Nguyễn Thị Thơ', 'nguyenthitho@gmail.com', 'Tôi rất hài lòng khi đặt tour tại đây', '', 0, 875, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_congdung`
--

CREATE TABLE `tbl_congdung` (
  `congdung_id` int(11) NOT NULL,
  `congdung_slug` varchar(255) NOT NULL,
  `congdung_name` varchar(255) NOT NULL,
  `congdung_description` text,
  `congdung_image` varchar(255) DEFAULT NULL,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_congdung`
--

INSERT INTO `tbl_congdung` (`congdung_id`, `congdung_slug`, `congdung_name`, `congdung_description`, `congdung_image`, `publication_status`) VALUES
(67, 'han-quoc', 'HÀN QUỐC', NULL, NULL, 1),
(70, 'dai-loan', 'ĐÀI LOAN', NULL, NULL, 1),
(71, 'nhat-ban', 'NHẬT BẢN', NULL, NULL, 1),
(73, 'singapore', 'SINGAPORE', NULL, NULL, 1),
(74, 'malaysia', 'MALAYSIA', NULL, NULL, 1),
(75, 'indonesia', 'INDONESIA', NULL, NULL, 1),
(76, 'thai-lan', 'THÁI LAN', NULL, NULL, 1),
(77, 'campuchia', 'CAMPUCHIA', NULL, NULL, 1),
(78, 'lao', 'LÀO', NULL, NULL, 1),
(79, 'trung-quoc', 'TRUNG QUỐC', NULL, NULL, 1),
(80, 'hong-kong', 'HONG KONG', NULL, NULL, 1),
(81, 'macau', 'MACAU', NULL, NULL, 1),
(82, 'an-do', 'ẤN ĐỘ', NULL, NULL, 1),
(83, 'dubai-uae', 'DUBAI - UAE', NULL, NULL, 1),
(84, 'my', 'MỸ', NULL, NULL, 1),
(85, 'uc', 'ÚC', NULL, NULL, 1),
(86, 'new-zealand', 'NEW ZEALAND', NULL, NULL, 1),
(87, 'canada', 'CANADA', NULL, NULL, 1),
(88, 'phap', 'PHÁP', NULL, NULL, 1),
(89, 'duc', 'ĐỨC', NULL, NULL, 1),
(90, 'y', 'Ý', NULL, NULL, 1),
(91, 'thuy-sy', 'THỤY SỸ', NULL, NULL, 1),
(92, 'tay-ban-nha', 'TÂY BAN NHA', NULL, NULL, 1),
(93, 'bo-dao-nha', 'BỒ ĐÀO NHA', NULL, NULL, 1),
(94, 'anh', 'ANH', NULL, NULL, 1),
(95, 'ireland', 'IRELAND', NULL, NULL, 1),
(96, 'iceland', 'ICELAND', NULL, NULL, 1),
(97, 'ha-lan', 'HÀ LAN', NULL, NULL, 1),
(98, 'bi', 'BỈ', NULL, NULL, 1),
(99, 'luxembourg-', 'LUXEMBOURG ', NULL, NULL, 1),
(100, 'dan-mach', 'ĐAN MẠCH', NULL, NULL, 1),
(101, 'thuy-dien', 'THỤY ĐIỂN', NULL, NULL, 1),
(102, 'na-uy', 'NA UY', NULL, NULL, 1),
(103, 'phan-lan', 'PHẦN LAN', NULL, NULL, 1),
(104, 'nga', 'NGA', NULL, NULL, 1),
(105, 'c-h-sec', 'C.H. SÉC', NULL, NULL, 1),
(106, 'ao', 'ÁO', NULL, NULL, 1),
(107, 'ba-lan', 'BA LAN', NULL, NULL, 1),
(108, 'slovakia', 'SLOVAKIA', NULL, NULL, 1),
(109, 'khanh-hoa', 'KHÁNH HOÀ', NULL, NULL, 1),
(110, 'tho-nhi-ky', 'THỔ NHĨ KỲ', NULL, NULL, 1),
(111, 'lao-cai', 'Lào Cai', NULL, NULL, 1),
(112, 'phu-yen', 'PHÚ YÊN', NULL, NULL, 1),
(113, 'ha-noi', 'Hà Nội', NULL, NULL, 1),
(114, 'da-nang', 'ĐÀ NẴNG', NULL, NULL, 1),
(115, 'dak-nong', 'ĐẮK NÔNG', NULL, NULL, 1),
(116, 'can-tho', 'CẦN THƠ', NULL, NULL, 1),
(117, 'phu-quoc', 'PHÚ QUỐC', NULL, NULL, 1),
(118, 'quang-ninh', 'Quảng Ninh', NULL, NULL, 1),
(119, 'kien-giang', 'KIÊN GIANG', NULL, NULL, 1),
(120, 'ha-giang', 'Hà Giang', NULL, NULL, 1),
(121, 'son-la', 'Sơn La', NULL, NULL, 1),
(122, 'yen-bai', 'Yên Bái', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_contact`
--

CREATE TABLE `tbl_contact` (
  `contact_id` int(11) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `contact_phone` varchar(11) NOT NULL,
  `contact_content` varchar(255) DEFAULT NULL,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_counter`
--

CREATE TABLE `tbl_counter` (
  `id` int(11) NOT NULL,
  `tm` varchar(255) NOT NULL,
  `ip` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT '0.0.0.0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_counter`
--

INSERT INTO `tbl_counter` (`id`, `tm`, `ip`) VALUES
(1, '2023-06-27', '123.20.82.244'),
(2, '2023-06-27', '179.43.169.181'),
(3, '2023-06-27', '123.20.82.244'),
(4, '2023-06-27', '123.20.82.244'),
(5, '2023-06-27', '123.20.82.244'),
(6, '2023-06-27', '123.20.82.244'),
(7, '2023-06-27', '123.20.82.244'),
(8, '2023-06-27', '123.20.82.244'),
(9, '2023-06-27', '123.20.82.244'),
(10, '2023-06-27', '123.20.82.244'),
(11, '2023-06-27', '123.20.82.244'),
(12, '2023-06-27', '123.20.82.244'),
(13, '2023-06-27', '123.20.82.244'),
(14, '2023-06-27', '123.20.82.244'),
(15, '2023-06-27', '123.20.82.244'),
(16, '2023-06-27', '123.20.82.244'),
(17, '2023-06-27', '123.20.82.244'),
(18, '2023-06-27', '123.20.82.244'),
(19, '2023-06-27', '123.20.82.244'),
(20, '2023-06-27', '123.20.82.244'),
(21, '2023-06-27', '123.20.82.244'),
(22, '2023-06-27', '123.20.82.244'),
(23, '2023-06-27', '123.20.82.244'),
(24, '2023-06-27', '123.20.82.244'),
(25, '2023-06-27', '123.20.82.244'),
(26, '2023-06-27', '123.20.82.244'),
(27, '2023-06-27', '123.20.82.244'),
(28, '2023-06-27', '34.254.53.125'),
(29, '2023-06-27', '34.254.53.125'),
(30, '2023-06-27', '123.20.82.244'),
(31, '2023-06-27', '123.20.82.244'),
(32, '2023-06-27', '123.20.82.244'),
(33, '2023-06-27', '123.20.82.244'),
(34, '2023-06-27', '123.20.82.244'),
(35, '2023-06-27', '123.20.82.244'),
(36, '2023-06-27', '123.20.82.244'),
(37, '2023-06-27', '123.20.82.244'),
(38, '2023-06-27', '123.20.82.244'),
(39, '2023-06-27', '123.20.82.244'),
(40, '2023-06-27', '171.67.70.233'),
(41, '2023-06-27', '123.20.82.244'),
(42, '2023-06-27', '123.20.82.244'),
(43, '2023-06-27', '123.20.82.244'),
(44, '2023-06-27', '123.20.82.244'),
(45, '2023-06-27', '123.20.82.244'),
(46, '2023-06-27', '15.235.162.47'),
(47, '2023-06-27', '15.235.162.70'),
(48, '2023-06-27', '15.235.162.70'),
(49, '2023-06-27', '15.235.162.70'),
(50, '2023-06-27', '15.235.162.47'),
(51, '2023-06-27', '14.187.99.10'),
(52, '2023-06-27', '123.20.82.244'),
(53, '2023-06-27', '123.20.82.244'),
(54, '2023-06-27', '123.20.82.244'),
(55, '2023-06-27', '123.20.82.244'),
(56, '2023-06-27', '123.20.82.244'),
(57, '2023-06-27', '123.20.82.244'),
(58, '2023-06-27', '47.242.105.176'),
(59, '2023-06-27', '47.251.13.32'),
(60, '2023-06-28', '167.94.138.34'),
(61, '2023-06-28', '167.248.133.189'),
(62, '2023-06-28', '14.187.111.238'),
(63, '2023-06-28', '14.187.111.238'),
(64, '2023-06-28', '14.187.111.238'),
(65, '2023-06-28', '14.187.111.238'),
(66, '2023-06-28', '14.187.111.238'),
(67, '2023-06-28', '14.187.111.238'),
(68, '2023-06-28', '14.187.111.238'),
(69, '2023-06-28', '14.187.111.238'),
(70, '2023-06-28', '14.187.111.238'),
(71, '2023-06-28', '14.187.111.238'),
(72, '2023-06-28', '14.187.111.238'),
(73, '2023-06-28', '65.154.226.171'),
(74, '2023-06-28', '14.187.111.238'),
(75, '2023-06-28', '14.187.111.238'),
(76, '2023-06-28', '14.187.111.238'),
(77, '2023-06-28', '14.187.111.238'),
(78, '2023-06-28', '14.187.111.238'),
(79, '2023-06-28', '14.187.111.238'),
(80, '2023-06-28', '123.20.222.114'),
(81, '2023-06-28', '123.20.222.114'),
(82, '2023-06-28', '123.20.222.114'),
(83, '2023-06-28', '123.20.222.114'),
(84, '2023-06-28', '123.20.222.114'),
(85, '2023-06-28', '123.20.222.114'),
(86, '2023-06-28', '123.20.222.114'),
(87, '2023-06-28', '123.20.222.114'),
(88, '2023-06-28', '123.20.222.114'),
(89, '2023-06-28', '123.20.222.114'),
(90, '2023-06-28', '123.20.222.114'),
(91, '2023-06-28', '123.20.222.114'),
(92, '2023-06-28', '123.20.222.114'),
(93, '2023-06-28', '123.20.222.114'),
(94, '2023-06-28', '123.20.222.114'),
(95, '2023-06-28', '123.20.222.114'),
(96, '2023-06-28', '66.249.82.6'),
(97, '2023-06-28', '66.249.82.6'),
(98, '2023-06-28', '123.20.222.114'),
(99, '2023-06-28', '123.20.222.114'),
(100, '2023-06-28', '123.20.222.114'),
(101, '2023-06-28', '123.20.222.114'),
(102, '2023-06-28', '66.249.82.4'),
(103, '2023-06-28', '66.249.82.4'),
(104, '2023-06-28', '66.249.82.4'),
(105, '2023-06-28', '66.249.82.3'),
(106, '2023-06-28', '123.20.222.114'),
(107, '2023-06-28', '123.20.222.114'),
(108, '2023-06-28', '123.20.222.114'),
(109, '2023-06-28', '123.20.222.114'),
(110, '2023-06-28', '123.20.222.114'),
(111, '2023-06-28', '123.20.222.114'),
(112, '2023-06-28', '123.20.222.114'),
(113, '2023-06-28', '123.20.222.114'),
(114, '2023-06-28', '123.20.222.114'),
(115, '2023-06-28', '123.20.222.114'),
(116, '2023-06-28', '123.20.222.114'),
(117, '2023-06-28', '123.20.222.114'),
(118, '2023-06-28', '123.20.222.114'),
(119, '2023-06-28', '123.20.222.114'),
(120, '2023-06-28', '123.20.222.114'),
(121, '2023-06-28', '123.20.222.114'),
(122, '2023-06-28', '65.154.226.166'),
(123, '2023-06-28', '123.20.222.114'),
(124, '2023-06-28', '123.20.222.114'),
(125, '2023-06-28', '123.20.222.114'),
(126, '2023-06-28', '123.20.222.114'),
(127, '2023-06-28', '123.20.222.114'),
(128, '2023-06-28', '123.20.222.114'),
(129, '2023-06-28', '123.20.222.114'),
(130, '2023-06-28', '123.20.222.114'),
(131, '2023-06-28', '123.20.222.114'),
(132, '2023-06-28', '123.20.222.114'),
(133, '2023-06-28', '123.20.222.114'),
(134, '2023-06-28', '123.20.222.114'),
(135, '2023-06-28', '123.20.222.114'),
(136, '2023-06-28', '123.20.222.114'),
(137, '2023-06-28', '123.20.222.114'),
(138, '2023-06-28', '123.20.222.114'),
(139, '2023-06-28', '123.20.222.114'),
(140, '2023-06-28', '123.20.222.114'),
(141, '2023-06-28', '123.20.222.114'),
(142, '2023-06-28', '123.20.222.114'),
(143, '2023-06-28', '123.20.222.114'),
(144, '2023-06-28', '123.20.222.114'),
(145, '2023-06-28', '146.190.67.63'),
(146, '2023-06-28', '123.20.222.114'),
(147, '2023-06-28', '123.20.222.114'),
(148, '2023-06-28', '123.20.222.114'),
(149, '2023-06-28', '123.20.222.114'),
(150, '2023-06-28', '123.20.222.114'),
(151, '2023-06-28', '123.20.222.114'),
(152, '2023-06-28', '123.20.222.114'),
(153, '2023-06-28', '123.20.222.114'),
(154, '2023-06-28', '123.20.222.114'),
(155, '2023-06-28', '123.20.222.114'),
(156, '2023-06-28', '123.20.222.114'),
(157, '2023-06-28', '123.20.222.114'),
(158, '2023-06-28', '123.20.222.114'),
(159, '2023-06-28', '42.113.218.221'),
(160, '2023-06-28', '42.113.218.221'),
(161, '2023-06-28', '42.113.218.221'),
(162, '2023-06-28', '42.113.218.221'),
(163, '2023-06-28', '42.113.218.221'),
(164, '2023-06-28', '42.113.218.221'),
(165, '2023-06-29', '87.236.176.235'),
(166, '2023-06-29', '183.129.153.157'),
(167, '2023-06-29', '87.236.176.46'),
(168, '2023-06-29', '14.187.106.26'),
(169, '2023-06-29', '14.187.106.26'),
(170, '2023-06-29', '14.187.106.26'),
(171, '2023-06-29', '14.187.106.26'),
(172, '2023-06-29', '14.187.106.26'),
(173, '2023-06-29', '14.187.106.26'),
(174, '2023-06-29', '14.187.106.26'),
(175, '2023-06-29', '14.187.106.26'),
(176, '2023-06-29', '14.187.106.26'),
(177, '2023-06-29', '14.187.106.26'),
(178, '2023-06-29', '14.187.106.26'),
(179, '2023-06-29', '14.187.106.26'),
(180, '2023-06-29', '14.187.106.26'),
(181, '2023-06-29', '14.187.106.26'),
(182, '2023-06-29', '14.187.106.26'),
(183, '2023-06-29', '14.187.106.26'),
(184, '2023-06-29', '14.187.106.26'),
(185, '2023-06-29', '14.187.106.26'),
(186, '2023-06-29', '14.187.106.26'),
(187, '2023-06-29', '14.187.106.26'),
(188, '2023-06-29', '14.187.106.26'),
(189, '2023-06-29', '14.187.106.26'),
(190, '2023-06-29', '14.187.106.26'),
(191, '2023-06-29', '14.187.106.26'),
(192, '2023-06-29', '14.187.106.26'),
(193, '2023-06-29', '14.187.106.26'),
(194, '2023-06-29', '14.187.106.26'),
(195, '2023-06-29', '14.187.106.26'),
(196, '2023-06-29', '14.187.106.26'),
(197, '2023-06-29', '14.187.106.26'),
(198, '2023-06-29', '14.187.106.26'),
(199, '2023-06-29', '14.187.106.26'),
(200, '2023-06-29', '14.187.106.26'),
(201, '2023-06-29', '14.187.106.26'),
(202, '2023-06-29', '14.187.106.26'),
(203, '2023-06-29', '14.187.106.26'),
(204, '2023-06-29', '14.187.106.26'),
(205, '2023-06-29', '14.187.106.26'),
(206, '2023-06-29', '14.187.106.26'),
(207, '2023-06-29', '14.187.106.26'),
(208, '2023-06-29', '14.187.106.26'),
(209, '2023-06-29', '14.187.106.26'),
(210, '2023-06-29', '14.187.106.26'),
(211, '2023-06-29', '14.187.106.26'),
(212, '2023-06-29', '14.187.106.26'),
(213, '2023-06-29', '14.187.106.26'),
(214, '2023-06-29', '14.187.106.26'),
(215, '2023-06-29', '14.187.106.26'),
(216, '2023-06-29', '14.187.106.26'),
(217, '2023-06-29', '14.187.106.26'),
(218, '2023-06-29', '14.187.106.26'),
(219, '2023-06-29', '14.187.106.26'),
(220, '2023-06-29', '14.187.106.26'),
(221, '2023-06-29', '14.187.106.26'),
(222, '2023-06-29', '14.187.106.26'),
(223, '2023-06-29', '14.187.106.26'),
(224, '2023-06-29', '14.187.106.26'),
(225, '2023-06-29', '14.187.106.26'),
(226, '2023-06-29', '14.187.106.26'),
(227, '2023-06-29', '14.187.106.26'),
(228, '2023-06-29', '14.187.106.26'),
(229, '2023-06-29', '14.187.106.26'),
(230, '2023-06-29', '14.187.106.26'),
(231, '2023-06-29', '14.187.106.26'),
(232, '2023-06-29', '14.187.106.26'),
(233, '2023-06-29', '14.187.106.26'),
(234, '2023-06-29', '14.187.106.26'),
(235, '2023-06-29', '14.187.106.26'),
(236, '2023-06-29', '14.187.106.26'),
(237, '2023-06-29', '14.187.106.26'),
(238, '2023-06-29', '14.187.106.26'),
(239, '2023-06-29', '14.187.106.26'),
(240, '2023-06-29', '14.187.106.26'),
(241, '2023-06-29', '14.187.106.26'),
(242, '2023-06-29', '14.187.106.26'),
(243, '2023-06-29', '14.187.106.26'),
(244, '2023-06-29', '14.187.106.26'),
(245, '2023-06-29', '14.187.106.26'),
(246, '2023-06-29', '14.187.106.26'),
(247, '2023-06-29', '14.187.106.26'),
(248, '2023-06-29', '14.187.106.26'),
(249, '2023-06-29', '14.187.106.26'),
(250, '2023-06-29', '14.187.106.26'),
(251, '2023-06-29', '14.187.106.26'),
(252, '2023-06-29', '14.187.106.26'),
(253, '2023-06-29', '15.235.162.47'),
(254, '2023-06-29', '116.118.52.117'),
(255, '2023-06-29', '116.118.52.117'),
(256, '2023-06-29', '116.118.52.117'),
(257, '2023-06-29', '123.20.154.39'),
(258, '2023-06-29', '123.20.154.39'),
(259, '2023-06-29', '123.20.154.39'),
(260, '2023-06-29', '116.118.52.117'),
(261, '2023-06-29', '116.118.52.117'),
(262, '2023-06-29', '123.20.154.39'),
(263, '2023-06-29', '14.187.106.26'),
(264, '2023-06-29', '14.187.106.26'),
(265, '2023-06-29', '14.187.106.26'),
(266, '2023-06-29', '14.187.106.26'),
(267, '2023-06-29', '14.187.106.26'),
(268, '2023-06-29', '14.187.106.26'),
(269, '2023-06-29', '14.187.106.26'),
(270, '2023-06-29', '116.118.52.117'),
(271, '2023-06-29', '116.118.52.117'),
(272, '2023-06-29', '14.187.106.26'),
(273, '2023-06-29', '14.187.106.26'),
(274, '2023-06-29', '14.187.106.26'),
(275, '2023-06-29', '14.187.106.26'),
(276, '2023-06-29', '14.187.106.26'),
(277, '2023-06-29', '14.187.106.26'),
(278, '2023-06-29', '14.187.106.26'),
(279, '2023-06-29', '14.187.106.26'),
(280, '2023-06-29', '14.187.106.26'),
(281, '2023-06-29', '14.187.106.26'),
(282, '2023-06-29', '14.187.106.26'),
(283, '2023-06-29', '14.187.106.26'),
(284, '2023-06-29', '14.187.106.26'),
(285, '2023-06-29', '14.187.106.26'),
(286, '2023-06-29', '169.48.198.40'),
(287, '2023-06-29', '14.187.106.26'),
(288, '2023-06-29', '14.187.106.26'),
(289, '2023-06-29', '14.187.106.26'),
(290, '2023-06-29', '14.187.106.26'),
(291, '2023-06-29', '169.60.2.107'),
(292, '2023-06-29', '14.187.106.26'),
(293, '2023-06-29', '14.187.106.26'),
(294, '2023-06-29', '14.187.106.26'),
(295, '2023-06-29', '14.187.106.26'),
(296, '2023-06-29', '14.187.106.26'),
(297, '2023-06-29', '14.187.106.26'),
(298, '2023-06-29', '14.187.106.26'),
(299, '2023-06-29', '14.187.106.26'),
(300, '2023-06-29', '14.187.106.26'),
(301, '2023-06-29', '14.187.106.26'),
(302, '2023-06-29', '14.187.106.26'),
(303, '2023-06-29', '14.187.106.26'),
(304, '2023-06-29', '14.187.106.26'),
(305, '2023-06-29', '14.187.106.26'),
(306, '2023-06-29', '14.187.106.26'),
(307, '2023-06-29', '14.187.106.26'),
(308, '2023-06-29', '14.187.106.26'),
(309, '2023-06-29', '14.187.106.26'),
(310, '2023-06-29', '14.187.106.26'),
(311, '2023-06-29', '14.187.106.26'),
(312, '2023-06-29', '14.187.106.26'),
(313, '2023-06-29', '14.187.106.26'),
(314, '2023-06-29', '14.187.106.26'),
(315, '2023-06-29', '14.187.106.26'),
(316, '2023-06-29', '14.187.106.26'),
(317, '2023-06-29', '14.187.106.26'),
(318, '2023-06-29', '14.187.106.26'),
(319, '2023-06-29', '14.187.106.26'),
(320, '2023-06-29', '14.187.106.26'),
(321, '2023-06-29', '14.187.106.26'),
(322, '2023-06-29', '14.187.106.26'),
(323, '2023-06-29', '14.187.106.26'),
(324, '2023-06-29', '14.187.106.26'),
(325, '2023-06-29', '14.187.106.26'),
(326, '2023-06-29', '14.187.106.26'),
(327, '2023-06-29', '14.187.106.26'),
(328, '2023-06-29', '14.187.106.26'),
(329, '2023-06-29', '14.187.106.26'),
(330, '2023-06-29', '14.187.106.26'),
(331, '2023-06-29', '14.187.106.26'),
(332, '2023-06-29', '14.187.106.26'),
(333, '2023-06-29', '14.187.106.26'),
(334, '2023-06-29', '169.48.198.35'),
(335, '2023-06-29', '14.187.106.26'),
(336, '2023-06-29', '116.118.52.117'),
(337, '2023-06-30', '14.187.114.225'),
(338, '2023-06-30', '14.187.114.225'),
(339, '2023-06-30', '14.187.114.225'),
(340, '2023-06-30', '15.235.162.55'),
(341, '2023-06-30', '14.187.114.225'),
(342, '2023-06-30', '14.187.114.225'),
(343, '2023-06-30', '14.187.114.225'),
(344, '2023-06-30', '14.187.114.225'),
(345, '2023-06-30', '14.187.114.225'),
(346, '2023-06-30', '14.187.114.225'),
(347, '2023-06-30', '14.187.114.225'),
(348, '2023-06-30', '14.187.114.225'),
(349, '2023-06-30', '14.187.114.225'),
(350, '2023-06-30', '14.187.114.225'),
(351, '2023-06-30', '14.187.114.225'),
(352, '2023-06-30', '14.187.114.225'),
(353, '2023-06-30', '14.187.114.225'),
(354, '2023-06-30', '14.187.114.225'),
(355, '2023-06-30', '14.187.114.225'),
(356, '2023-06-30', '14.187.114.225'),
(357, '2023-06-30', '14.187.114.225'),
(358, '2023-06-30', '14.187.114.225'),
(359, '2023-06-30', '14.187.114.225'),
(360, '2023-06-30', '14.187.114.225'),
(361, '2023-06-30', '14.187.114.225'),
(362, '2023-06-30', '14.187.114.225'),
(363, '2023-06-30', '14.187.114.225'),
(364, '2023-06-30', '14.187.114.225'),
(365, '2023-06-30', '14.187.114.225'),
(366, '2023-06-30', '14.187.114.225'),
(367, '2023-06-30', '14.187.114.225'),
(368, '2023-06-30', '14.187.114.225'),
(369, '2023-06-30', '14.187.114.225'),
(370, '2023-06-30', '14.187.114.225'),
(371, '2023-06-30', '14.187.114.225'),
(372, '2023-06-30', '14.187.114.225'),
(373, '2023-06-30', '14.187.114.225'),
(374, '2023-06-30', '14.187.114.225'),
(375, '2023-06-30', '14.187.114.225'),
(376, '2023-06-30', '14.187.114.225'),
(377, '2023-06-30', '14.187.114.225'),
(378, '2023-06-30', '14.187.114.225'),
(379, '2023-06-30', '14.187.114.225'),
(380, '2023-06-30', '14.187.114.225'),
(381, '2023-06-30', '14.187.114.225'),
(382, '2023-06-30', '14.187.114.225'),
(383, '2023-06-30', '14.187.114.225'),
(384, '2023-06-30', '14.187.114.225'),
(385, '2023-06-30', '14.187.114.225'),
(386, '2023-06-30', '14.187.114.225'),
(387, '2023-06-30', '116.118.52.117'),
(388, '2023-06-30', '14.187.114.225'),
(389, '2023-06-30', '14.187.114.225'),
(390, '2023-06-30', '14.187.114.225'),
(391, '2023-06-30', '14.187.114.225'),
(392, '2023-06-30', '14.187.114.225'),
(393, '2023-06-30', '14.187.114.225'),
(394, '2023-06-30', '14.187.114.225'),
(395, '2023-06-30', '14.187.114.225'),
(396, '2023-06-30', '14.187.114.225'),
(397, '2023-06-30', '14.187.114.225'),
(398, '2023-06-30', '14.187.114.225'),
(399, '2023-06-30', '14.187.114.225'),
(400, '2023-06-30', '14.187.114.225'),
(401, '2023-06-30', '14.187.114.225'),
(402, '2023-06-30', '14.187.114.225'),
(403, '2023-06-30', '14.187.114.225'),
(404, '2023-06-30', '14.187.114.225'),
(405, '2023-06-30', '14.187.114.225'),
(406, '2023-06-30', '14.187.114.225'),
(407, '2023-06-30', '14.187.114.225'),
(408, '2023-06-30', '14.187.114.225'),
(409, '2023-06-30', '14.187.114.225'),
(410, '2023-06-30', '14.187.114.225'),
(411, '2023-06-30', '14.187.114.225'),
(412, '2023-06-30', '14.187.114.225'),
(413, '2023-06-30', '14.187.114.225'),
(414, '2023-06-30', '14.187.114.225'),
(415, '2023-06-30', '14.187.114.225'),
(416, '2023-06-30', '14.187.114.225'),
(417, '2023-06-30', '14.187.114.225'),
(418, '2023-06-30', '14.187.114.225'),
(419, '2023-06-30', '14.187.114.225'),
(420, '2023-06-30', '14.187.114.225'),
(421, '2023-06-30', '14.187.114.225'),
(422, '2023-06-30', '14.187.114.225'),
(423, '2023-06-30', '14.187.114.225'),
(424, '2023-06-30', '14.187.114.225'),
(425, '2023-06-30', '14.187.114.225'),
(426, '2023-06-30', '14.187.114.225'),
(427, '2023-06-30', '14.187.114.225'),
(428, '2023-06-30', '113.173.233.125'),
(429, '2023-06-30', '113.173.233.125'),
(430, '2023-06-30', '113.173.233.125'),
(431, '2023-06-30', '113.173.233.125'),
(432, '2023-06-30', '113.173.233.125'),
(433, '2023-06-30', '113.173.233.125'),
(434, '2023-06-30', '113.173.233.125'),
(435, '2023-06-30', '113.173.233.125'),
(436, '2023-06-30', '113.173.233.125'),
(437, '2023-06-30', '113.173.233.125'),
(438, '2023-06-30', '113.173.233.125'),
(439, '2023-06-30', '113.173.233.125'),
(440, '2023-06-30', '113.173.233.125'),
(441, '2023-06-30', '113.173.233.125'),
(442, '2023-06-30', '113.173.233.125'),
(443, '2023-06-30', '113.173.233.125'),
(444, '2023-06-30', '113.173.233.125'),
(445, '2023-06-30', '113.173.233.125'),
(446, '2023-06-30', '113.173.233.125'),
(447, '2023-06-30', '113.173.233.125'),
(448, '2023-06-30', '113.173.233.125'),
(449, '2023-06-30', '113.173.233.125'),
(450, '2023-06-30', '113.173.233.125'),
(451, '2023-06-30', '113.173.233.125'),
(452, '2023-06-30', '113.173.233.125'),
(453, '2023-06-30', '113.173.233.125'),
(454, '2023-06-30', '113.173.233.125'),
(455, '2023-06-30', '113.173.233.125'),
(456, '2023-06-30', '113.173.233.125'),
(457, '2023-06-30', '113.173.233.125'),
(458, '2023-06-30', '113.173.233.125'),
(459, '2023-06-30', '113.173.233.125'),
(460, '2023-06-30', '113.173.233.125'),
(461, '2023-06-30', '113.173.233.125'),
(462, '2023-06-30', '113.173.233.125'),
(463, '2023-06-30', '113.173.233.125'),
(464, '2023-06-30', '113.173.233.125'),
(465, '2023-06-30', '113.173.233.125'),
(466, '2023-06-30', '113.173.233.125'),
(467, '2023-06-30', '113.173.233.125'),
(468, '2023-06-30', '113.173.233.125'),
(469, '2023-06-30', '113.173.233.125'),
(470, '2023-06-30', '113.173.233.125'),
(471, '2023-06-30', '113.173.233.125'),
(472, '2023-06-30', '113.173.233.125'),
(473, '2023-06-30', '113.173.233.125'),
(474, '2023-06-30', '113.173.233.125'),
(475, '2023-06-30', '113.173.233.125'),
(476, '2023-06-30', '113.173.233.125'),
(477, '2023-06-30', '113.173.233.125'),
(478, '2023-06-30', '113.173.233.125'),
(479, '2023-06-30', '113.173.233.125'),
(480, '2023-06-30', '113.173.233.125'),
(481, '2023-06-30', '113.173.233.125'),
(482, '2023-06-30', '113.173.233.125'),
(483, '2023-06-30', '113.173.233.125'),
(484, '2023-06-30', '113.173.233.125'),
(485, '2023-06-30', '113.173.233.125'),
(486, '2023-06-30', '113.173.233.125'),
(487, '2023-06-30', '113.173.233.125'),
(488, '2023-06-30', '113.173.233.125'),
(489, '2023-06-30', '113.173.233.125'),
(490, '2023-06-30', '113.173.233.125'),
(491, '2023-06-30', '113.173.233.125'),
(492, '2023-06-30', '113.173.233.125'),
(493, '2023-06-30', '113.173.233.125'),
(494, '2023-06-30', '113.173.233.125'),
(495, '2023-06-30', '113.173.233.125'),
(496, '2023-06-30', '113.173.233.125'),
(497, '2023-06-30', '113.173.233.125'),
(498, '2023-06-30', '113.173.233.125'),
(499, '2023-06-30', '113.173.233.125'),
(500, '2023-06-30', '113.173.233.125'),
(501, '2023-06-30', '113.173.233.125'),
(502, '2023-06-30', '113.173.233.125'),
(503, '2023-06-30', '113.173.233.125'),
(504, '2023-06-30', '113.173.233.125'),
(505, '2023-06-30', '113.173.233.125'),
(506, '2023-06-30', '113.173.233.125'),
(507, '2023-06-30', '113.173.233.125'),
(508, '2023-06-30', '113.173.233.125'),
(509, '2023-06-30', '113.173.233.125'),
(510, '2023-06-30', '113.173.233.125'),
(511, '2023-06-30', '113.173.233.125'),
(512, '2023-06-30', '113.173.233.125'),
(513, '2023-06-30', '113.173.233.125'),
(514, '2023-06-30', '113.173.233.125'),
(515, '2023-06-30', '113.173.233.125'),
(516, '2023-06-30', '113.173.233.125'),
(517, '2023-06-30', '113.173.233.125'),
(518, '2023-06-30', '113.173.233.125'),
(519, '2023-06-30', '113.173.233.125'),
(520, '2023-06-30', '113.173.233.125'),
(521, '2023-06-30', '113.173.233.125'),
(522, '2023-06-30', '113.173.233.125'),
(523, '2023-06-30', '113.173.233.125'),
(524, '2023-06-30', '113.173.233.125'),
(525, '2023-06-30', '113.173.233.125'),
(526, '2023-06-30', '113.173.233.125'),
(527, '2023-06-30', '113.173.233.125'),
(528, '2023-06-30', '113.173.233.125'),
(529, '2023-06-30', '113.173.233.125'),
(530, '2023-06-30', '113.173.233.125'),
(531, '2023-06-30', '113.173.233.125'),
(532, '2023-06-30', '113.173.233.125'),
(533, '2023-06-30', '113.173.233.125'),
(534, '2023-06-30', '113.173.233.125'),
(535, '2023-06-30', '113.173.233.125'),
(536, '2023-06-30', '113.173.233.125'),
(537, '2023-06-30', '113.173.233.125'),
(538, '2023-06-30', '113.173.233.125'),
(539, '2023-06-30', '113.173.233.125'),
(540, '2023-06-30', '113.173.233.125'),
(541, '2023-06-30', '113.173.233.125'),
(542, '2023-06-30', '113.173.233.125'),
(543, '2023-06-30', '113.173.233.125'),
(544, '2023-06-30', '113.173.233.125'),
(545, '2023-06-30', '113.173.233.125'),
(546, '2023-06-30', '113.173.233.125'),
(547, '2023-06-30', '113.173.233.125'),
(548, '2023-06-30', '113.173.233.125'),
(549, '2023-06-30', '113.173.233.125'),
(550, '2023-06-30', '113.173.233.125'),
(551, '2023-06-30', '113.173.233.125'),
(552, '2023-06-30', '113.173.233.125'),
(553, '2023-06-30', '113.173.233.125'),
(554, '2023-07-01', '14.187.103.205'),
(555, '2023-07-01', '14.187.103.205'),
(556, '2023-07-01', '14.187.103.205'),
(557, '2023-07-01', '14.187.103.205'),
(558, '2023-07-01', '14.187.103.205'),
(559, '2023-07-01', '14.187.103.205'),
(560, '2023-07-01', '14.187.103.205'),
(561, '2023-07-01', '14.187.103.205'),
(562, '2023-07-01', '14.187.103.205'),
(563, '2023-07-01', '14.187.103.205'),
(564, '2023-07-01', '14.187.103.205'),
(565, '2023-07-01', '14.187.103.205'),
(566, '2023-07-01', '14.187.103.205'),
(567, '2023-07-01', '14.187.103.205'),
(568, '2023-07-01', '14.187.103.205'),
(569, '2023-07-01', '14.187.103.205'),
(570, '2023-07-01', '14.187.103.205'),
(571, '2023-07-01', '14.187.103.205'),
(572, '2023-07-01', '14.187.103.205'),
(573, '2023-07-01', '14.187.103.205'),
(574, '2023-07-01', '14.187.103.205'),
(575, '2023-07-01', '14.187.103.205'),
(576, '2023-07-01', '14.187.103.205'),
(577, '2023-07-01', '14.187.103.205'),
(578, '2023-07-01', '14.187.103.205'),
(579, '2023-07-01', '14.187.103.205'),
(580, '2023-07-01', '14.187.103.205'),
(581, '2023-07-01', '14.187.103.205'),
(582, '2023-07-01', '14.187.103.205'),
(583, '2023-07-01', '14.187.103.205'),
(584, '2023-07-01', '14.187.103.205'),
(585, '2023-07-01', '14.187.103.205'),
(586, '2023-07-01', '14.187.103.205'),
(587, '2023-07-01', '14.187.103.205'),
(588, '2023-07-01', '14.187.103.205'),
(589, '2023-07-01', '14.187.103.205'),
(590, '2023-07-01', '14.187.110.39'),
(591, '2023-07-01', '14.187.110.39'),
(592, '2023-07-01', '14.187.110.39'),
(593, '2023-07-01', '14.187.110.39'),
(594, '2023-07-01', '116.118.54.195'),
(595, '2023-07-01', '116.118.54.195'),
(596, '2023-07-01', '15.235.162.57'),
(597, '2023-07-01', '210.245.33.130'),
(598, '2023-07-01', '210.245.33.130'),
(599, '2023-07-01', '210.245.33.130'),
(600, '2023-07-01', '3.101.146.43'),
(601, '2023-07-01', '210.245.33.130'),
(602, '2023-07-01', '210.245.33.130'),
(603, '2023-07-01', '169.60.172.124'),
(604, '2023-07-01', '210.245.33.130'),
(605, '2023-07-01', '210.245.33.130'),
(606, '2023-07-01', '167.94.138.35'),
(607, '2023-07-02', '167.248.133.187'),
(608, '2023-07-03', '113.173.255.68'),
(609, '2023-07-03', '113.173.255.68'),
(610, '2023-07-03', '113.173.255.68'),
(611, '2023-07-03', '113.173.255.68'),
(612, '2023-07-03', '116.118.54.195'),
(613, '2023-07-03', '116.118.54.195'),
(614, '2023-07-03', '116.118.54.195'),
(615, '2023-07-03', '116.118.54.195'),
(616, '2023-07-03', '116.118.54.195'),
(617, '2023-07-03', '123.20.137.100'),
(618, '2023-07-03', '113.173.255.68'),
(619, '2023-07-03', '113.173.255.68'),
(620, '2023-07-03', '113.173.255.68'),
(621, '2023-07-03', '113.173.255.68'),
(622, '2023-07-03', '113.173.255.68'),
(623, '2023-07-03', '113.173.255.68'),
(624, '2023-07-03', '113.173.255.68'),
(625, '2023-07-03', '113.173.255.68'),
(626, '2023-07-03', '113.173.255.68'),
(627, '2023-07-03', '113.173.255.68'),
(628, '2023-07-03', '113.173.255.68'),
(629, '2023-07-03', '116.118.54.195'),
(630, '2023-07-03', '113.173.255.68'),
(631, '2023-07-03', '113.173.255.68'),
(632, '2023-07-03', '113.173.255.68'),
(633, '2023-07-03', '113.173.255.68'),
(634, '2023-07-03', '113.173.255.68'),
(635, '2023-07-03', '113.173.255.68'),
(636, '2023-07-03', '113.173.255.68'),
(637, '2023-07-03', '113.173.255.68'),
(638, '2023-07-03', '113.173.255.68'),
(639, '2023-07-03', '113.173.255.68'),
(640, '2023-07-03', '113.173.255.68'),
(641, '2023-07-03', '3.88.157.92'),
(642, '2023-07-03', '3.88.157.92'),
(643, '2023-07-03', '113.173.255.68'),
(644, '2023-07-03', '113.173.255.68'),
(645, '2023-07-03', '113.173.255.68'),
(646, '2023-07-03', '113.173.255.68'),
(647, '2023-07-03', '113.173.255.68'),
(648, '2023-07-03', '113.173.255.68'),
(649, '2023-07-03', '113.173.255.68'),
(650, '2023-07-03', '113.173.255.68'),
(651, '2023-07-03', '113.173.255.68'),
(652, '2023-07-03', '113.173.255.68'),
(653, '2023-07-03', '113.173.255.68'),
(654, '2023-07-03', '113.173.255.68'),
(655, '2023-07-03', '113.173.255.68'),
(656, '2023-07-03', '113.173.255.68'),
(657, '2023-07-03', '113.173.255.68'),
(658, '2023-07-03', '113.173.255.68'),
(659, '2023-07-03', '113.173.255.68'),
(660, '2023-07-03', '113.173.255.68'),
(661, '2023-07-03', '113.173.255.68'),
(662, '2023-07-03', '113.173.255.68'),
(663, '2023-07-03', '113.173.255.68'),
(664, '2023-07-03', '113.173.255.68'),
(665, '2023-07-03', '113.173.255.68'),
(666, '2023-07-03', '113.173.255.68'),
(667, '2023-07-03', '113.173.255.68'),
(668, '2023-07-03', '113.173.255.68'),
(669, '2023-07-03', '113.173.255.68'),
(670, '2023-07-03', '113.173.255.68'),
(671, '2023-07-03', '113.173.255.68'),
(672, '2023-07-03', '113.173.255.68'),
(673, '2023-07-03', '113.173.255.68'),
(674, '2023-07-03', '113.173.255.68'),
(675, '2023-07-03', '113.173.255.68'),
(676, '2023-07-03', '113.173.255.68'),
(677, '2023-07-03', '113.173.255.68'),
(678, '2023-07-03', '113.173.255.68'),
(679, '2023-07-03', '113.173.255.68'),
(680, '2023-07-03', '113.173.255.68'),
(681, '2023-07-03', '113.173.255.68'),
(682, '2023-07-03', '113.173.255.68'),
(683, '2023-07-03', '113.173.255.68'),
(684, '2023-07-03', '113.173.255.68'),
(685, '2023-07-03', '113.173.255.68'),
(686, '2023-07-03', '113.173.255.68'),
(687, '2023-07-03', '113.173.255.68'),
(688, '2023-07-03', '113.173.255.68'),
(689, '2023-07-03', '113.173.255.68'),
(690, '2023-07-03', '113.173.255.68'),
(691, '2023-07-03', '113.173.255.68'),
(692, '2023-07-03', '113.173.255.68'),
(693, '2023-07-03', '113.173.255.68'),
(694, '2023-07-03', '113.173.255.68'),
(695, '2023-07-03', '113.173.255.68'),
(696, '2023-07-03', '113.173.255.68'),
(697, '2023-07-03', '113.173.255.68'),
(698, '2023-07-03', '113.173.255.68'),
(699, '2023-07-03', '113.173.255.68'),
(700, '2023-07-03', '113.173.255.68'),
(701, '2023-07-03', '113.173.255.68'),
(702, '2023-07-03', '113.173.255.68'),
(703, '2023-07-03', '113.173.255.68'),
(704, '2023-07-03', '113.173.255.68'),
(705, '2023-07-03', '113.173.255.68'),
(706, '2023-07-03', '113.173.255.68'),
(707, '2023-07-03', '113.173.255.68'),
(708, '2023-07-03', '113.173.255.68'),
(709, '2023-07-03', '113.173.255.68'),
(710, '2023-07-03', '113.173.255.68'),
(711, '2023-07-03', '113.173.255.68'),
(712, '2023-07-03', '113.173.255.68'),
(713, '2023-07-03', '113.173.255.68'),
(714, '2023-07-03', '113.173.255.68'),
(715, '2023-07-03', '113.173.255.68'),
(716, '2023-07-03', '113.173.255.68'),
(717, '2023-07-03', '113.173.255.68'),
(718, '2023-07-03', '113.173.255.68'),
(719, '2023-07-03', '113.173.255.68'),
(720, '2023-07-03', '113.173.255.68'),
(721, '2023-07-03', '113.173.255.68'),
(722, '2023-07-03', '113.173.255.68'),
(723, '2023-07-03', '113.173.255.68'),
(724, '2023-07-03', '113.173.255.68'),
(725, '2023-07-03', '113.173.255.68'),
(726, '2023-07-03', '113.173.255.68'),
(727, '2023-07-03', '113.173.255.68'),
(728, '2023-07-03', '113.173.255.68'),
(729, '2023-07-03', '113.173.255.68'),
(730, '2023-07-03', '113.173.255.68'),
(731, '2023-07-03', '113.173.255.68'),
(732, '2023-07-03', '113.173.255.68'),
(733, '2023-07-03', '113.173.255.68'),
(734, '2023-07-03', '113.173.255.68'),
(735, '2023-07-03', '113.173.255.68'),
(736, '2023-07-03', '113.173.255.68'),
(737, '2023-07-03', '113.173.255.68'),
(738, '2023-07-03', '113.173.255.68'),
(739, '2023-07-03', '113.173.255.68'),
(740, '2023-07-03', '113.173.255.68'),
(741, '2023-07-03', '113.173.255.68'),
(742, '2023-07-03', '113.173.255.68'),
(743, '2023-07-03', '113.173.255.68'),
(744, '2023-07-03', '113.173.255.68'),
(745, '2023-07-03', '113.173.255.68'),
(746, '2023-07-03', '113.173.255.68'),
(747, '2023-07-03', '113.173.255.68'),
(748, '2023-07-03', '113.173.255.68'),
(749, '2023-07-03', '113.173.255.68'),
(750, '2023-07-03', '113.173.255.68'),
(751, '2023-07-03', '113.173.255.68'),
(752, '2023-07-03', '113.173.255.68'),
(753, '2023-07-03', '113.173.255.68'),
(754, '2023-07-03', '113.173.255.68'),
(755, '2023-07-03', '113.173.255.68'),
(756, '2023-07-03', '113.173.255.68'),
(757, '2023-07-03', '113.173.255.68'),
(758, '2023-07-03', '113.173.255.68'),
(759, '2023-07-03', '113.173.255.68'),
(760, '2023-07-03', '113.173.255.68'),
(761, '2023-07-03', '113.173.255.68'),
(762, '2023-07-03', '113.173.255.68'),
(763, '2023-07-03', '113.173.255.68'),
(764, '2023-07-03', '113.173.255.68'),
(765, '2023-07-03', '113.173.255.68'),
(766, '2023-07-03', '113.173.255.68'),
(767, '2023-07-04', '123.20.235.112'),
(768, '2023-07-04', '123.20.235.112'),
(769, '2023-07-04', '123.20.235.112'),
(770, '2023-07-04', '123.20.235.112'),
(771, '2023-07-04', '123.20.235.112'),
(772, '2023-07-04', '123.20.235.112'),
(773, '2023-07-04', '123.20.235.112'),
(774, '2023-07-04', '123.20.235.112'),
(775, '2023-07-04', '123.20.235.112'),
(776, '2023-07-04', '123.20.235.112'),
(777, '2023-07-04', '123.20.235.112'),
(778, '2023-07-04', '123.20.235.112'),
(779, '2023-07-04', '123.20.235.112'),
(780, '2023-07-04', '123.20.235.112'),
(781, '2023-07-04', '123.20.235.112'),
(782, '2023-07-04', '123.20.235.112'),
(783, '2023-07-04', '123.20.235.112'),
(784, '2023-07-04', '123.20.235.112'),
(785, '2023-07-04', '123.20.235.112'),
(786, '2023-07-04', '123.20.235.112'),
(787, '2023-07-04', '123.20.235.112'),
(788, '2023-07-04', '123.20.235.112'),
(789, '2023-07-04', '123.20.235.112'),
(790, '2023-07-04', '123.20.235.112'),
(791, '2023-07-04', '123.20.235.112'),
(792, '2023-07-04', '123.20.235.112'),
(793, '2023-07-04', '123.20.235.112'),
(794, '2023-07-04', '123.20.235.112'),
(795, '2023-07-04', '123.20.235.112'),
(796, '2023-07-04', '123.20.235.112'),
(797, '2023-07-04', '123.20.235.112'),
(798, '2023-07-04', '123.20.235.112'),
(799, '2023-07-04', '123.20.235.112'),
(800, '2023-07-04', '123.20.235.112'),
(801, '2023-07-04', '123.20.235.112'),
(802, '2023-07-04', '123.20.235.112'),
(803, '2023-07-04', '123.20.235.112'),
(804, '2023-07-04', '123.20.235.112'),
(805, '2023-07-04', '123.20.235.112'),
(806, '2023-07-04', '123.20.235.112'),
(807, '2023-07-04', '123.20.235.112'),
(808, '2023-07-04', '123.20.235.112'),
(809, '2023-07-04', '123.20.235.112'),
(810, '2023-07-04', '123.20.235.112'),
(811, '2023-07-04', '123.20.235.112'),
(812, '2023-07-04', '123.20.235.112'),
(813, '2023-07-04', '123.20.235.112'),
(814, '2023-07-04', '123.20.235.112'),
(815, '2023-07-04', '123.20.235.112'),
(816, '2023-07-04', '123.20.235.112'),
(817, '2023-07-04', '123.20.235.112'),
(818, '2023-07-04', '123.20.235.112'),
(819, '2023-07-04', '123.20.235.112'),
(820, '2023-07-04', '123.20.235.112'),
(821, '2023-07-04', '123.20.235.112'),
(822, '2023-07-04', '123.20.235.112'),
(823, '2023-07-04', '123.20.235.112'),
(824, '2023-07-04', '123.20.235.112'),
(825, '2023-07-04', '123.20.235.112'),
(826, '2023-07-04', '123.20.235.112'),
(827, '2023-07-04', '123.20.235.112'),
(828, '2023-07-04', '123.20.235.112'),
(829, '2023-07-04', '116.118.54.195'),
(830, '2023-07-04', '123.20.235.112'),
(831, '2023-07-04', '123.20.235.112'),
(832, '2023-07-04', '123.20.235.112'),
(833, '2023-07-04', '123.20.235.112'),
(834, '2023-07-04', '123.20.235.112'),
(835, '2023-07-04', '123.20.235.112'),
(836, '2023-07-04', '123.20.235.112'),
(837, '2023-07-04', '123.20.235.112'),
(838, '2023-07-04', '123.20.235.112'),
(839, '2023-07-04', '123.20.235.112'),
(840, '2023-07-04', '123.20.235.112'),
(841, '2023-07-04', '123.20.235.112'),
(842, '2023-07-04', '123.20.235.112'),
(843, '2023-07-04', '123.20.235.112'),
(844, '2023-07-04', '123.20.235.112'),
(845, '2023-07-04', '123.20.235.112'),
(846, '2023-07-04', '123.20.235.112'),
(847, '2023-07-04', '123.20.235.112'),
(848, '2023-07-04', '116.118.54.195'),
(849, '2023-07-04', '123.20.235.112'),
(850, '2023-07-04', '123.20.235.112'),
(851, '2023-07-04', '123.20.235.112'),
(852, '2023-07-04', '123.20.235.112'),
(853, '2023-07-04', '123.20.235.112'),
(854, '2023-07-04', '123.20.235.112'),
(855, '2023-07-04', '123.20.235.112'),
(856, '2023-07-04', '123.20.235.112'),
(857, '2023-07-04', '123.20.235.112'),
(858, '2023-07-04', '123.20.235.112'),
(859, '2023-07-04', '123.20.235.112'),
(860, '2023-07-04', '123.20.235.112'),
(861, '2023-07-04', '123.20.235.112'),
(862, '2023-07-04', '123.20.235.112'),
(863, '2023-07-04', '123.20.235.112'),
(864, '2023-07-04', '123.20.235.112'),
(865, '2023-07-04', '123.20.235.112'),
(866, '2023-07-04', '123.20.235.112'),
(867, '2023-07-04', '123.20.235.112'),
(868, '2023-07-04', '123.20.235.112'),
(869, '2023-07-04', '123.20.235.112'),
(870, '2023-07-04', '123.20.235.112'),
(871, '2023-07-04', '123.20.235.112'),
(872, '2023-07-04', '123.20.235.112'),
(873, '2023-07-04', '123.20.235.112'),
(874, '2023-07-04', '123.20.235.112'),
(875, '2023-07-04', '123.20.235.112'),
(876, '2023-07-04', '123.20.235.112'),
(877, '2023-07-04', '123.20.235.112'),
(878, '2023-07-04', '123.20.235.112'),
(879, '2023-07-04', '123.20.235.112'),
(880, '2023-07-04', '123.20.235.112'),
(881, '2023-07-04', '123.20.235.112'),
(882, '2023-07-04', '123.20.235.112'),
(883, '2023-07-04', '123.20.235.112'),
(884, '2023-07-04', '123.20.235.112'),
(885, '2023-07-04', '123.20.235.112'),
(886, '2023-07-04', '123.20.235.112'),
(887, '2023-07-04', '123.20.235.112'),
(888, '2023-07-04', '103.199.57.189'),
(889, '2023-07-04', '103.199.56.155'),
(890, '2023-07-04', '123.20.235.112'),
(891, '2023-07-04', '123.20.235.112'),
(892, '2023-07-04', '123.20.235.112'),
(893, '2023-07-04', '123.20.235.112'),
(894, '2023-07-04', '123.20.235.112'),
(895, '2023-07-04', '123.20.235.112'),
(896, '2023-07-04', '123.20.235.112'),
(897, '2023-07-04', '123.20.235.112'),
(898, '2023-07-04', '123.20.235.112'),
(899, '2023-07-04', '123.20.235.112'),
(900, '2023-07-04', '123.20.235.112'),
(901, '2023-07-04', '123.20.235.112'),
(902, '2023-07-04', '123.20.235.112'),
(903, '2023-07-04', '123.20.235.112'),
(904, '2023-07-04', '123.20.235.112'),
(905, '2023-07-04', '123.20.235.112'),
(906, '2023-07-04', '123.20.235.112'),
(907, '2023-07-04', '123.20.235.112'),
(908, '2023-07-04', '116.118.54.195'),
(909, '2023-07-04', '116.118.54.195'),
(910, '2023-07-04', '123.20.235.112'),
(911, '2023-07-04', '123.20.235.112'),
(912, '2023-07-04', '116.118.54.195'),
(913, '2023-07-04', '116.118.54.195'),
(914, '2023-07-04', '116.118.54.195'),
(915, '2023-07-04', '116.118.54.195'),
(916, '2023-07-04', '116.118.54.195'),
(917, '2023-07-04', '116.118.54.195'),
(918, '2023-07-04', '116.118.54.195'),
(919, '2023-07-04', '116.118.54.195'),
(920, '2023-07-04', '123.20.235.112'),
(921, '2023-07-04', '123.20.235.112'),
(922, '2023-07-04', '123.20.235.112'),
(923, '2023-07-04', '123.20.235.112'),
(924, '2023-07-04', '123.20.235.112'),
(925, '2023-07-04', '123.20.235.112'),
(926, '2023-07-04', '123.20.235.112'),
(927, '2023-07-04', '123.20.235.112'),
(928, '2023-07-04', '123.20.235.112'),
(929, '2023-07-04', '123.20.235.112'),
(930, '2023-07-04', '123.20.235.112'),
(931, '2023-07-04', '123.20.235.112'),
(932, '2023-07-04', '123.20.235.112'),
(933, '2023-07-04', '123.20.235.112'),
(934, '2023-07-04', '123.20.235.112'),
(935, '2023-07-04', '123.20.235.112'),
(936, '2023-07-04', '123.20.235.112'),
(937, '2023-07-04', '123.20.235.112'),
(938, '2023-07-04', '123.20.235.112'),
(939, '2023-07-04', '116.118.54.195'),
(940, '2023-07-04', '123.20.235.112'),
(941, '2023-07-04', '123.20.235.112'),
(942, '2023-07-04', '123.20.235.112'),
(943, '2023-07-04', '123.20.235.112'),
(944, '2023-07-04', '123.20.235.112'),
(945, '2023-07-04', '123.20.235.112'),
(946, '2023-07-04', '123.20.235.112'),
(947, '2023-07-04', '123.20.235.112'),
(948, '2023-07-04', '123.20.235.112'),
(949, '2023-07-04', '123.20.235.112'),
(950, '2023-07-04', '123.20.235.112'),
(951, '2023-07-04', '123.20.235.112'),
(952, '2023-07-04', '123.20.235.112'),
(953, '2023-07-04', '123.20.235.112'),
(954, '2023-07-04', '123.20.235.112'),
(955, '2023-07-04', '123.20.235.112'),
(956, '2023-07-04', '123.20.235.112'),
(957, '2023-07-04', '123.20.235.112'),
(958, '2023-07-04', '123.20.235.112'),
(959, '2023-07-04', '123.20.235.112'),
(960, '2023-07-04', '123.20.235.112'),
(961, '2023-07-04', '123.20.235.112'),
(962, '2023-07-04', '123.20.235.112'),
(963, '2023-07-04', '123.20.235.112'),
(964, '2023-07-04', '123.20.235.112'),
(965, '2023-07-04', '123.20.235.112'),
(966, '2023-07-04', '123.20.235.112'),
(967, '2023-07-04', '123.20.235.112'),
(968, '2023-07-04', '123.20.235.112'),
(969, '2023-07-04', '123.20.235.112'),
(970, '2023-07-04', '123.20.235.112'),
(971, '2023-07-04', '123.20.235.112'),
(972, '2023-07-04', '123.20.235.112'),
(973, '2023-07-04', '123.20.235.112'),
(974, '2023-07-04', '123.20.235.112'),
(975, '2023-07-04', '123.20.235.112'),
(976, '2023-07-04', '123.20.235.112'),
(977, '2023-07-04', '123.20.235.112'),
(978, '2023-07-05', '14.187.113.77'),
(979, '2023-07-05', '14.187.113.77'),
(980, '2023-07-05', '14.187.113.77'),
(981, '2023-07-05', '14.187.113.77'),
(982, '2023-07-05', '14.187.115.21'),
(983, '2023-07-05', '14.187.115.21'),
(984, '2023-07-05', '14.187.115.21'),
(985, '2023-07-05', '14.187.115.21'),
(986, '2023-07-05', '14.187.115.21'),
(987, '2023-07-05', '14.187.115.21'),
(988, '2023-07-05', '14.187.115.21'),
(989, '2023-07-05', '14.187.115.21'),
(990, '2023-07-05', '14.187.115.21'),
(991, '2023-07-05', '14.187.115.21'),
(992, '2023-07-05', '14.187.115.21'),
(993, '2023-07-05', '14.187.115.21'),
(994, '2023-07-05', '14.187.115.21'),
(995, '2023-07-05', '14.187.115.21'),
(996, '2023-07-05', '14.187.115.21'),
(997, '2023-07-05', '14.187.115.21'),
(998, '2023-07-05', '14.187.115.21'),
(999, '2023-07-05', '14.187.115.21'),
(1000, '2023-07-05', '14.187.115.21'),
(1001, '2023-07-05', '14.187.115.21'),
(1002, '2023-07-05', '14.187.115.21'),
(1003, '2023-07-05', '14.187.115.21'),
(1004, '2023-07-05', '14.187.115.21'),
(1005, '2023-07-05', '14.187.115.21'),
(1006, '2023-07-05', '14.187.115.21'),
(1007, '2023-07-05', '14.187.115.21'),
(1008, '2023-07-05', '14.187.115.21'),
(1009, '2023-07-05', '14.187.115.21'),
(1010, '2023-07-05', '14.187.115.21'),
(1011, '2023-07-05', '14.187.115.21'),
(1012, '2023-07-05', '14.187.115.21'),
(1013, '2023-07-05', '14.187.115.21'),
(1014, '2023-07-05', '14.187.115.21'),
(1015, '2023-07-05', '14.187.115.21'),
(1016, '2023-07-05', '14.187.115.21'),
(1017, '2023-07-05', '14.187.115.21'),
(1018, '2023-07-05', '14.187.115.21'),
(1019, '2023-07-05', '14.187.115.21'),
(1020, '2023-07-05', '14.187.115.21'),
(1021, '2023-07-05', '14.187.115.21'),
(1022, '2023-07-05', '14.187.115.21'),
(1023, '2023-07-05', '14.187.115.21'),
(1024, '2023-07-05', '14.187.115.21'),
(1025, '2023-07-05', '14.187.115.21'),
(1026, '2023-07-05', '14.187.115.21'),
(1027, '2023-07-05', '14.187.115.21'),
(1028, '2023-07-05', '14.187.115.21'),
(1029, '2023-07-05', '14.187.115.21'),
(1030, '2023-07-05', '14.187.115.21'),
(1031, '2023-07-05', '14.187.115.21'),
(1032, '2023-07-05', '14.187.115.21'),
(1033, '2023-07-05', '14.187.115.21'),
(1034, '2023-07-05', '14.187.115.21'),
(1035, '2023-07-05', '14.187.115.21'),
(1036, '2023-07-05', '14.187.115.21'),
(1037, '2023-07-05', '14.187.115.21'),
(1038, '2023-07-05', '14.187.115.21'),
(1039, '2023-07-05', '14.187.115.21'),
(1040, '2023-07-05', '14.187.115.21'),
(1041, '2023-07-05', '14.187.115.21'),
(1042, '2023-07-05', '14.187.115.21'),
(1043, '2023-07-05', '14.187.115.21'),
(1044, '2023-07-05', '14.187.115.21'),
(1045, '2023-07-05', '14.187.115.21'),
(1046, '2023-07-05', '14.187.115.21'),
(1047, '2023-07-05', '14.187.115.21'),
(1048, '2023-07-05', '14.187.115.21'),
(1049, '2023-07-05', '14.187.115.21'),
(1050, '2023-07-05', '14.187.115.21'),
(1051, '2023-07-05', '14.187.115.21'),
(1052, '2023-07-05', '14.187.115.21'),
(1053, '2023-07-05', '14.187.115.21'),
(1054, '2023-07-05', '14.187.115.21'),
(1055, '2023-07-05', '14.187.115.21'),
(1056, '2023-07-05', '14.187.115.21'),
(1057, '2023-07-05', '14.187.115.21'),
(1058, '2023-07-05', '14.187.115.21'),
(1059, '2023-07-05', '14.187.115.21'),
(1060, '2023-07-05', '14.187.115.21'),
(1061, '2023-07-05', '14.187.115.21'),
(1062, '2023-07-05', '14.187.115.21'),
(1063, '2023-07-05', '14.187.115.21'),
(1064, '2023-07-05', '14.187.115.21'),
(1065, '2023-07-05', '14.187.115.21'),
(1066, '2023-07-05', '14.187.115.21'),
(1067, '2023-07-05', '14.187.115.21'),
(1068, '2023-07-05', '14.187.115.21'),
(1069, '2023-07-05', '14.187.115.21'),
(1070, '2023-07-05', '14.187.115.21'),
(1071, '2023-07-05', '14.187.115.21'),
(1072, '2023-07-05', '14.187.115.21'),
(1073, '2023-07-05', '14.187.115.21'),
(1074, '2023-07-05', '14.187.115.21'),
(1075, '2023-07-05', '14.187.115.21'),
(1076, '2023-07-05', '14.187.115.21'),
(1077, '2023-07-05', '14.187.115.21'),
(1078, '2023-07-05', '14.187.115.21'),
(1079, '2023-07-05', '14.187.115.21'),
(1080, '2023-07-05', '14.187.115.21'),
(1081, '2023-07-05', '14.187.115.21'),
(1082, '2023-07-05', '14.187.115.21'),
(1083, '2023-07-05', '14.187.115.21'),
(1084, '2023-07-05', '14.187.115.21'),
(1085, '2023-07-05', '14.187.115.21'),
(1086, '2023-07-05', '14.187.115.21'),
(1087, '2023-07-05', '14.187.115.21'),
(1088, '2023-07-05', '14.187.115.21'),
(1089, '2023-07-05', '14.187.115.21'),
(1090, '2023-07-05', '15.235.162.57'),
(1091, '2023-07-05', '14.187.115.21'),
(1092, '2023-07-05', '14.187.115.21'),
(1093, '2023-07-05', '14.187.115.21'),
(1094, '2023-07-05', '14.187.115.21'),
(1095, '2023-07-05', '14.187.115.21'),
(1096, '2023-07-05', '14.187.115.21'),
(1097, '2023-07-05', '14.187.115.21'),
(1098, '2023-07-05', '14.187.115.21'),
(1099, '2023-07-05', '14.187.115.21'),
(1100, '2023-07-05', '14.187.115.21'),
(1101, '2023-07-05', '14.187.115.21'),
(1102, '2023-07-05', '14.187.113.77'),
(1103, '2023-07-05', '14.187.113.77'),
(1104, '2023-07-05', '14.187.113.77'),
(1105, '2023-07-05', '14.187.113.77'),
(1106, '2023-07-05', '14.187.113.77'),
(1107, '2023-07-05', '14.187.113.77'),
(1108, '2023-07-05', '14.187.113.77'),
(1109, '2023-07-05', '14.187.113.77'),
(1110, '2023-07-05', '14.187.113.77'),
(1111, '2023-07-05', '14.187.113.77'),
(1112, '2023-07-05', '14.187.113.77'),
(1113, '2023-07-05', '14.187.113.77'),
(1114, '2023-07-05', '14.187.113.77'),
(1115, '2023-07-05', '14.187.113.77'),
(1116, '2023-07-05', '14.187.113.77'),
(1117, '2023-07-05', '14.187.113.77'),
(1118, '2023-07-05', '14.187.113.77'),
(1119, '2023-07-05', '14.187.113.77'),
(1120, '2023-07-05', '14.187.113.77'),
(1121, '2023-07-05', '14.187.113.77'),
(1122, '2023-07-05', '14.187.113.77'),
(1123, '2023-07-05', '14.187.113.77'),
(1124, '2023-07-05', '14.187.113.77'),
(1125, '2023-07-05', '14.187.113.77'),
(1126, '2023-07-05', '14.187.113.77'),
(1127, '2023-07-05', '14.187.113.77'),
(1128, '2023-07-05', '14.187.113.77'),
(1129, '2023-07-05', '14.187.113.77'),
(1130, '2023-07-05', '14.187.113.77'),
(1131, '2023-07-05', '14.187.113.77'),
(1132, '2023-07-05', '14.187.113.77'),
(1133, '2023-07-05', '14.187.113.77'),
(1134, '2023-07-05', '14.187.113.77'),
(1135, '2023-07-05', '14.187.113.77'),
(1136, '2023-07-05', '14.187.113.77'),
(1137, '2023-07-05', '14.187.113.77'),
(1138, '2023-07-05', '14.187.113.77'),
(1139, '2023-07-05', '14.187.113.77'),
(1140, '2023-07-05', '14.187.113.77'),
(1141, '2023-07-05', '14.187.113.77'),
(1142, '2023-07-05', '14.187.113.77'),
(1143, '2023-07-05', '14.187.113.77'),
(1144, '2023-07-05', '14.187.113.77'),
(1145, '2023-07-05', '14.187.113.77'),
(1146, '2023-07-05', '14.187.113.77'),
(1147, '2023-07-05', '14.187.113.77'),
(1148, '2023-07-05', '14.187.113.77'),
(1149, '2023-07-05', '14.187.113.77'),
(1150, '2023-07-05', '14.187.113.77'),
(1151, '2023-07-05', '14.187.113.77'),
(1152, '2023-07-05', '14.187.113.77'),
(1153, '2023-07-05', '14.187.113.77'),
(1154, '2023-07-05', '14.187.113.77'),
(1155, '2023-07-05', '14.187.113.77'),
(1156, '2023-07-05', '14.187.113.77'),
(1157, '2023-07-05', '14.187.113.77'),
(1158, '2023-07-05', '14.187.113.77'),
(1159, '2023-07-05', '14.187.113.77'),
(1160, '2023-07-05', '14.187.113.77'),
(1161, '2023-07-05', '14.187.113.77'),
(1162, '2023-07-05', '14.187.113.77'),
(1163, '2023-07-05', '14.187.113.77'),
(1164, '2023-07-05', '14.187.113.77'),
(1165, '2023-07-05', '14.187.113.77'),
(1166, '2023-07-05', '14.187.113.77'),
(1167, '2023-07-05', '14.187.113.77'),
(1168, '2023-07-05', '14.187.113.77'),
(1169, '2023-07-05', '14.187.113.77'),
(1170, '2023-07-05', '14.187.113.77'),
(1171, '2023-07-05', '14.187.113.77'),
(1172, '2023-07-05', '14.187.113.77'),
(1173, '2023-07-05', '14.187.113.77'),
(1174, '2023-07-05', '14.187.113.77'),
(1175, '2023-07-05', '14.187.113.77'),
(1176, '2023-07-05', '14.187.113.77'),
(1177, '2023-07-05', '14.187.113.77'),
(1178, '2023-07-05', '14.187.113.77'),
(1179, '2023-07-05', '14.187.113.77'),
(1180, '2023-07-05', '14.187.113.77'),
(1181, '2023-07-05', '14.187.113.77'),
(1182, '2023-07-05', '14.187.113.77'),
(1183, '2023-07-05', '14.187.113.77'),
(1184, '2023-07-05', '14.187.113.77'),
(1185, '2023-07-05', '14.187.113.77'),
(1186, '2023-07-05', '14.187.113.77'),
(1187, '2023-07-05', '14.187.113.77'),
(1188, '2023-07-05', '14.187.113.77'),
(1189, '2023-07-05', '14.187.113.77'),
(1190, '2023-07-05', '14.187.113.77'),
(1191, '2023-07-05', '14.187.113.77'),
(1192, '2023-07-05', '14.187.113.77'),
(1193, '2023-07-05', '14.187.113.77'),
(1194, '2023-07-05', '14.187.113.77'),
(1195, '2023-07-05', '14.187.113.77'),
(1196, '2023-07-05', '14.187.113.77'),
(1197, '2023-07-05', '14.187.113.77'),
(1198, '2023-07-05', '14.187.113.77'),
(1199, '2023-07-05', '14.187.113.77'),
(1200, '2023-07-05', '14.187.113.77'),
(1201, '2023-07-05', '14.187.113.77'),
(1202, '2023-07-05', '14.187.113.77'),
(1203, '2023-07-05', '14.187.113.77'),
(1204, '2023-07-05', '14.187.113.77'),
(1205, '2023-07-05', '14.187.113.77'),
(1206, '2023-07-05', '14.187.113.77'),
(1207, '2023-07-05', '14.187.113.77'),
(1208, '2023-07-05', '14.187.113.77'),
(1209, '2023-07-05', '14.187.113.77'),
(1210, '2023-07-05', '14.187.113.77'),
(1211, '2023-07-05', '14.187.113.77'),
(1212, '2023-07-05', '14.187.113.77'),
(1213, '2023-07-05', '14.187.113.77'),
(1214, '2023-07-05', '14.187.113.77'),
(1215, '2023-07-05', '14.187.113.77'),
(1216, '2023-07-05', '14.187.113.77'),
(1217, '2023-07-05', '14.187.113.77'),
(1218, '2023-07-05', '14.187.113.77'),
(1219, '2023-07-05', '14.187.113.77'),
(1220, '2023-07-05', '14.187.113.77'),
(1221, '2023-07-05', '14.187.113.77'),
(1222, '2023-07-05', '14.187.113.77'),
(1223, '2023-07-05', '14.187.113.77'),
(1224, '2023-07-05', '14.187.113.77'),
(1225, '2023-07-05', '14.187.113.77'),
(1226, '2023-07-05', '14.187.113.77'),
(1227, '2023-07-05', '14.187.113.77'),
(1228, '2023-07-05', '14.187.113.77'),
(1229, '2023-07-05', '14.187.113.77'),
(1230, '2023-07-05', '14.187.113.77'),
(1231, '2023-07-05', '14.187.113.77'),
(1232, '2023-07-05', '14.187.113.77'),
(1233, '2023-07-05', '14.187.113.77'),
(1234, '2023-07-05', '14.187.113.77'),
(1235, '2023-07-05', '14.187.113.77'),
(1236, '2023-07-05', '14.187.113.77'),
(1237, '2023-07-05', '14.187.113.77'),
(1238, '2023-07-05', '14.187.113.77'),
(1239, '2023-07-05', '14.187.113.77'),
(1240, '2023-07-05', '14.187.113.77'),
(1241, '2023-07-05', '14.187.113.77'),
(1242, '2023-07-05', '14.187.113.77'),
(1243, '2023-07-05', '14.187.113.77'),
(1244, '2023-07-05', '14.187.113.77'),
(1245, '2023-07-05', '14.187.113.77'),
(1246, '2023-07-05', '14.187.113.77'),
(1247, '2023-07-05', '14.187.113.77'),
(1248, '2023-07-05', '14.187.113.77'),
(1249, '2023-07-05', '14.187.113.77'),
(1250, '2023-07-05', '14.187.113.77'),
(1251, '2023-07-05', '14.187.113.77'),
(1252, '2023-07-05', '14.187.113.77'),
(1253, '2023-07-05', '14.187.113.77'),
(1254, '2023-07-05', '14.187.113.77'),
(1255, '2023-07-05', '14.187.113.77'),
(1256, '2023-07-05', '14.187.113.77'),
(1257, '2023-07-05', '14.187.113.77'),
(1258, '2023-07-05', '14.187.113.77'),
(1259, '2023-07-05', '14.187.113.77'),
(1260, '2023-07-05', '14.187.113.77'),
(1261, '2023-07-05', '14.187.113.77'),
(1262, '2023-07-05', '14.187.113.77'),
(1263, '2023-07-05', '14.187.113.77'),
(1264, '2023-07-05', '14.187.113.77'),
(1265, '2023-07-05', '14.187.113.77'),
(1266, '2023-07-05', '14.187.113.77'),
(1267, '2023-07-05', '14.187.113.77'),
(1268, '2023-07-05', '14.187.113.77'),
(1269, '2023-07-05', '14.187.113.77'),
(1270, '2023-07-05', '14.187.113.77'),
(1271, '2023-07-05', '14.187.113.77'),
(1272, '2023-07-05', '14.187.113.77'),
(1273, '2023-07-05', '14.187.113.77'),
(1274, '2023-07-05', '14.187.113.77'),
(1275, '2023-07-05', '14.187.113.77'),
(1276, '2023-07-05', '14.187.113.77'),
(1277, '2023-07-05', '14.187.113.77'),
(1278, '2023-07-05', '14.187.113.77'),
(1279, '2023-07-05', '14.187.113.77'),
(1280, '2023-07-05', '14.187.113.77'),
(1281, '2023-07-05', '14.187.113.77'),
(1282, '2023-07-05', '14.187.113.77'),
(1283, '2023-07-05', '14.187.113.77'),
(1284, '2023-07-05', '14.187.113.77'),
(1285, '2023-07-05', '14.187.113.77'),
(1286, '2023-07-05', '14.187.113.77'),
(1287, '2023-07-05', '14.187.113.77'),
(1288, '2023-07-05', '14.187.113.77'),
(1289, '2023-07-05', '14.187.113.77'),
(1290, '2023-07-05', '14.187.113.77'),
(1291, '2023-07-05', '14.187.113.77'),
(1292, '2023-07-05', '14.187.113.77'),
(1293, '2023-07-05', '14.187.113.77'),
(1294, '2023-07-05', '14.187.113.77'),
(1295, '2023-07-05', '14.187.113.77'),
(1296, '2023-07-05', '14.187.113.77'),
(1297, '2023-07-05', '14.187.113.77'),
(1298, '2023-07-05', '14.187.113.77'),
(1299, '2023-07-05', '14.187.113.77'),
(1300, '2023-07-05', '14.187.113.77'),
(1301, '2023-07-05', '14.187.113.77'),
(1302, '2023-07-05', '14.187.113.77'),
(1303, '2023-07-05', '14.187.113.77'),
(1304, '2023-07-05', '14.187.113.77'),
(1305, '2023-07-05', '14.187.113.77'),
(1306, '2023-07-05', '14.187.113.77'),
(1307, '2023-07-05', '14.187.113.77'),
(1308, '2023-07-05', '14.187.113.77'),
(1309, '2023-07-05', '14.187.113.77'),
(1310, '2023-07-05', '14.187.113.77'),
(1311, '2023-07-05', '14.187.113.77'),
(1312, '2023-07-05', '14.187.113.77'),
(1313, '2023-07-05', '14.187.113.77'),
(1314, '2023-07-05', '14.187.113.77'),
(1315, '2023-07-05', '14.187.113.77'),
(1316, '2023-07-05', '14.187.113.77'),
(1317, '2023-07-05', '14.187.113.77'),
(1318, '2023-07-05', '14.187.113.77'),
(1319, '2023-07-05', '14.187.113.77'),
(1320, '2023-07-05', '14.187.113.77'),
(1321, '2023-07-05', '66.249.82.7'),
(1322, '2023-07-05', '66.249.82.5'),
(1323, '2023-07-05', '66.249.82.5'),
(1324, '2023-07-05', '66.249.82.4'),
(1325, '2023-07-05', '66.249.82.5'),
(1326, '2023-07-05', '66.249.82.4'),
(1327, '2023-07-05', '14.187.113.77'),
(1328, '2023-07-05', '14.187.113.77'),
(1329, '2023-07-05', '14.187.113.77'),
(1330, '2023-07-05', '14.187.113.77'),
(1331, '2023-07-05', '14.187.113.77'),
(1332, '2023-07-05', '14.187.113.77'),
(1333, '2023-07-05', '14.187.113.77'),
(1334, '2023-07-05', '14.187.113.77'),
(1335, '2023-07-05', '14.187.113.77'),
(1336, '2023-07-05', '14.187.113.77'),
(1337, '2023-07-05', '14.187.113.77'),
(1338, '2023-07-05', '14.187.113.77'),
(1339, '2023-07-06', '14.187.106.39'),
(1340, '2023-07-06', '14.187.106.39'),
(1341, '2023-07-06', '14.187.106.39'),
(1342, '2023-07-06', '14.187.106.39'),
(1343, '2023-07-06', '14.187.106.39'),
(1344, '2023-07-06', '14.187.106.39'),
(1345, '2023-07-06', '14.187.106.39'),
(1346, '2023-07-06', '14.187.106.39'),
(1347, '2023-07-06', '14.187.106.39'),
(1348, '2023-07-06', '14.187.106.39'),
(1349, '2023-07-06', '14.187.106.39'),
(1350, '2023-07-06', '14.187.106.39'),
(1351, '2023-07-06', '14.187.106.39'),
(1352, '2023-07-06', '14.187.106.39'),
(1353, '2023-07-06', '14.187.106.39'),
(1354, '2023-07-06', '14.187.106.39'),
(1355, '2023-07-06', '14.187.106.39'),
(1356, '2023-07-06', '14.187.106.39'),
(1357, '2023-07-06', '14.187.106.39');
INSERT INTO `tbl_counter` (`id`, `tm`, `ip`) VALUES
(1358, '2023-07-06', '14.187.106.39'),
(1359, '2023-07-06', '14.187.106.39'),
(1360, '2023-07-06', '14.187.106.39'),
(1361, '2023-07-06', '14.187.106.39'),
(1362, '2023-07-06', '14.187.106.39'),
(1363, '2023-07-06', '14.187.106.39'),
(1364, '2023-07-06', '14.187.106.39'),
(1365, '2023-07-06', '14.187.106.39'),
(1366, '2023-07-06', '14.187.106.39'),
(1367, '2023-07-06', '14.187.106.39'),
(1368, '2023-07-06', '14.187.106.39'),
(1369, '2023-07-06', '14.187.106.39'),
(1370, '2023-07-06', '14.187.106.39'),
(1371, '2023-07-06', '14.187.106.39'),
(1372, '2023-07-06', '14.187.106.39'),
(1373, '2023-07-06', '14.187.106.39'),
(1374, '2023-07-06', '14.187.106.39'),
(1375, '2023-07-06', '14.187.106.39'),
(1376, '2023-07-06', '14.187.106.39'),
(1377, '2023-07-06', '14.187.106.39'),
(1378, '2023-07-06', '14.187.106.39'),
(1379, '2023-07-06', '14.187.106.39'),
(1380, '2023-07-06', '14.187.106.39'),
(1381, '2023-07-06', '14.187.106.39'),
(1382, '2023-07-06', '14.187.106.39'),
(1383, '2023-07-06', '14.187.106.39'),
(1384, '2023-07-06', '14.187.106.39'),
(1385, '2023-07-06', '14.187.106.39'),
(1386, '2023-07-06', '14.187.106.39'),
(1387, '2023-07-06', '14.187.106.39'),
(1388, '2023-07-06', '14.187.106.39'),
(1389, '2023-07-06', '14.187.106.39'),
(1390, '2023-07-06', '14.187.106.39'),
(1391, '2023-07-06', '14.187.106.39'),
(1392, '2023-07-06', '14.187.106.39'),
(1393, '2023-07-06', '14.187.106.39'),
(1394, '2023-07-06', '14.187.106.39'),
(1395, '2023-07-06', '14.187.106.39'),
(1396, '2023-07-06', '14.187.106.39'),
(1397, '2023-07-06', '14.187.106.39'),
(1398, '2023-07-06', '14.187.106.39'),
(1399, '2023-07-06', '14.187.106.39'),
(1400, '2023-07-06', '14.187.106.39'),
(1401, '2023-07-06', '14.187.106.39'),
(1402, '2023-07-06', '14.187.106.39'),
(1403, '2023-07-06', '14.187.106.39'),
(1404, '2023-07-06', '14.187.106.39'),
(1405, '2023-07-06', '14.187.106.39'),
(1406, '2023-07-06', '14.187.106.39'),
(1407, '2023-07-06', '14.187.106.39'),
(1408, '2023-07-06', '14.187.106.39'),
(1409, '2023-07-06', '14.187.106.39'),
(1410, '2023-07-06', '14.187.106.39'),
(1411, '2023-07-06', '14.187.106.39'),
(1412, '2023-07-06', '14.187.106.39'),
(1413, '2023-07-06', '14.187.106.39'),
(1414, '2023-07-06', '14.187.106.39'),
(1415, '2023-07-06', '14.187.106.39'),
(1416, '2023-07-06', '14.187.106.39'),
(1417, '2023-07-06', '14.187.106.39'),
(1418, '2023-07-06', '14.187.106.39'),
(1419, '2023-07-06', '14.187.106.39'),
(1420, '2023-07-06', '14.187.106.39'),
(1421, '2023-07-06', '14.187.106.39'),
(1422, '2023-07-06', '14.187.106.39'),
(1423, '2023-07-06', '14.187.106.39'),
(1424, '2023-07-06', '14.187.106.39'),
(1425, '2023-07-06', '14.187.106.39'),
(1426, '2023-07-06', '14.187.106.39'),
(1427, '2023-07-06', '14.187.106.39'),
(1428, '2023-07-06', '14.187.106.39'),
(1429, '2023-07-06', '14.187.106.39'),
(1430, '2023-07-06', '14.187.106.39'),
(1431, '2023-07-06', '14.187.106.39'),
(1432, '2023-07-06', '14.187.106.39'),
(1433, '2023-07-06', '14.187.106.39'),
(1434, '2023-07-06', '14.187.106.39'),
(1435, '2023-07-06', '14.187.106.39'),
(1436, '2023-07-06', '14.187.106.39'),
(1437, '2023-07-06', '14.187.106.39'),
(1438, '2023-07-06', '14.187.106.39'),
(1439, '2023-07-06', '14.187.106.39'),
(1440, '2023-07-06', '14.187.106.39'),
(1441, '2023-07-06', '14.187.106.39'),
(1442, '2023-07-06', '14.187.106.39'),
(1443, '2023-07-06', '14.187.106.39'),
(1444, '2023-07-06', '14.187.106.39'),
(1445, '2023-07-06', '14.187.106.39'),
(1446, '2023-07-06', '14.187.106.39'),
(1447, '2023-07-06', '14.187.106.39'),
(1448, '2023-07-06', '14.187.106.39'),
(1449, '2023-07-06', '14.187.106.39'),
(1450, '2023-07-06', '14.187.106.39'),
(1451, '2023-07-06', '14.187.106.39'),
(1452, '2023-07-06', '14.187.106.39'),
(1453, '2023-07-06', '14.187.106.39'),
(1454, '2023-07-06', '14.187.106.39'),
(1455, '2023-07-06', '14.187.106.39'),
(1456, '2023-07-06', '14.187.106.39'),
(1457, '2023-07-06', '14.187.106.39'),
(1458, '2023-07-06', '14.187.106.39'),
(1459, '2023-07-06', '14.187.106.39'),
(1460, '2023-07-06', '14.187.106.39'),
(1461, '2023-07-06', '14.187.106.39'),
(1462, '2023-07-06', '14.187.106.39'),
(1463, '2023-07-06', '14.187.106.39'),
(1464, '2023-07-06', '14.187.106.39'),
(1465, '2023-07-06', '14.187.106.39'),
(1466, '2023-07-06', '14.187.106.39'),
(1467, '2023-07-06', '14.187.106.39'),
(1468, '2023-07-06', '14.187.106.39'),
(1469, '2023-07-06', '14.187.106.39'),
(1470, '2023-07-06', '14.187.106.39'),
(1471, '2023-07-06', '14.187.106.39'),
(1472, '2023-07-06', '14.187.106.39'),
(1473, '2023-07-06', '14.187.106.39'),
(1474, '2023-07-06', '14.187.106.39'),
(1475, '2023-07-06', '14.187.106.39'),
(1476, '2023-07-06', '14.187.106.39'),
(1477, '2023-07-06', '14.187.106.39'),
(1478, '2023-07-06', '14.187.106.39'),
(1479, '2023-07-06', '14.187.106.39'),
(1480, '2023-07-06', '14.187.106.39'),
(1481, '2023-07-06', '14.187.106.39'),
(1482, '2023-07-06', '14.187.106.39'),
(1483, '2023-07-06', '14.187.106.39'),
(1484, '2023-07-06', '14.187.106.39'),
(1485, '2023-07-06', '14.187.106.39'),
(1486, '2023-07-06', '14.187.106.39'),
(1487, '2023-07-06', '14.187.106.39'),
(1488, '2023-07-06', '14.187.106.39'),
(1489, '2023-07-06', '14.187.106.39'),
(1490, '2023-07-06', '14.187.106.39'),
(1491, '2023-07-06', '14.187.106.39'),
(1492, '2023-07-06', '14.187.106.39'),
(1493, '2023-07-06', '14.187.106.39'),
(1494, '2023-07-06', '14.187.106.39'),
(1495, '2023-07-06', '14.187.106.39'),
(1496, '2023-07-06', '14.187.106.39'),
(1497, '2023-07-06', '14.187.106.39'),
(1498, '2023-07-06', '14.187.106.39'),
(1499, '2023-07-06', '14.187.106.39'),
(1500, '2023-07-06', '14.187.106.39'),
(1501, '2023-07-06', '14.187.106.39'),
(1502, '2023-07-06', '14.187.106.39'),
(1503, '2023-07-06', '14.187.106.39'),
(1504, '2023-07-06', '14.187.106.39'),
(1505, '2023-07-06', '14.187.106.39'),
(1506, '2023-07-06', '14.187.106.39'),
(1507, '2023-07-06', '14.187.106.39'),
(1508, '2023-07-06', '14.187.106.39'),
(1509, '2023-07-06', '14.187.106.39'),
(1510, '2023-07-06', '14.187.106.39'),
(1511, '2023-07-06', '14.187.106.39'),
(1512, '2023-07-06', '14.187.106.39'),
(1513, '2023-07-06', '14.187.106.39'),
(1514, '2023-07-06', '14.187.106.39'),
(1515, '2023-07-06', '14.187.106.39'),
(1516, '2023-07-06', '14.187.106.39'),
(1517, '2023-07-06', '14.187.106.39'),
(1518, '2023-07-06', '14.187.106.39'),
(1519, '2023-07-06', '14.187.106.39'),
(1520, '2023-07-06', '14.187.106.39'),
(1521, '2023-07-06', '14.187.106.39'),
(1522, '2023-07-06', '14.187.106.39'),
(1523, '2023-07-06', '14.187.106.39'),
(1524, '2023-07-06', '14.187.106.39'),
(1525, '2023-07-06', '14.187.106.39'),
(1526, '2023-07-06', '14.187.106.39'),
(1527, '2023-07-06', '14.187.106.39'),
(1528, '2023-07-06', '14.187.106.39'),
(1529, '2023-07-06', '14.187.106.39'),
(1530, '2023-07-06', '14.187.106.39'),
(1531, '2023-07-06', '14.187.106.39'),
(1532, '2023-07-06', '14.187.106.39'),
(1533, '2023-07-06', '14.187.106.39'),
(1534, '2023-07-06', '14.187.106.39'),
(1535, '2023-07-06', '14.187.106.39'),
(1536, '2023-07-06', '14.187.106.39'),
(1537, '2023-07-06', '14.187.106.39'),
(1538, '2023-07-06', '14.187.106.39'),
(1539, '2023-07-06', '14.187.106.39'),
(1540, '2023-07-06', '14.187.106.39'),
(1541, '2023-07-06', '14.187.106.39'),
(1542, '2023-07-06', '14.187.106.39'),
(1543, '2023-07-06', '14.187.106.39'),
(1544, '2023-07-06', '14.187.106.39'),
(1545, '2023-07-06', '14.187.106.39'),
(1546, '2023-07-06', '14.187.106.39'),
(1547, '2023-07-06', '14.187.106.39'),
(1548, '2023-07-06', '14.187.106.39'),
(1549, '2023-07-06', '14.187.106.39'),
(1550, '2023-07-06', '14.187.106.39'),
(1551, '2023-07-06', '14.187.106.39'),
(1552, '2023-07-06', '14.187.106.39'),
(1553, '2023-07-06', '14.187.106.39'),
(1554, '2023-07-06', '14.187.106.39'),
(1555, '2023-07-06', '14.187.106.39');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_customer`
--

CREATE TABLE `tbl_customer` (
  `customer_id` int(11) NOT NULL,
  `customer_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `customer_email` varchar(100) NOT NULL,
  `customer_password` varchar(32) NOT NULL,
  `customer_address` text CHARACTER SET utf8 NOT NULL,
  `customer_phone` varchar(20) NOT NULL,
  `customer_active` tinyint(4) NOT NULL COMMENT 'Active=1,Unactive=0',
  `customer_avatar` text CHARACTER SET utf8,
  `customer_city` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `customer_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_customer`
--

INSERT INTO `tbl_customer` (`customer_id`, `customer_name`, `customer_email`, `customer_password`, `customer_address`, `customer_phone`, `customer_active`, `customer_avatar`, `customer_city`, `customer_time`) VALUES
(45, 'Đặng văn múp', 'dangvanmup@gmail.com', '46f94c8de14fb36680850768ff1b7f2a', '17 huỳnh thiên lộc quận tân phú', '0986524576', 0, NULL, NULL, '2021-11-01 04:50:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_depart`
--

CREATE TABLE `tbl_depart` (
  `id` int(11) UNSIGNED NOT NULL,
  `date_begin` text CHARACTER SET utf8,
  `price_child` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `price_baby` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `price_people` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `move` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_depart`
--

INSERT INTO `tbl_depart` (`id`, `date_begin`, `price_child`, `price_baby`, `product_id`, `price_people`, `move`, `status`) VALUES
(806, '2023-08-15', '3900000', '1500000', 870, '5900000', 'Vietnam Airlines', 'Còn chỗ'),
(807, '2023-08-16', '3500000', '1500000', 870, '5879000', 'Vietnam Airlines', 'Còn chỗ'),
(808, '2023-08-17', '3300000', '1300000', 870, '5300000', 'Vietnam Airlines', 'Còn chỗ'),
(809, '2023-08-18', '3300000', '1300000', 870, '5300000', 'Vietnam Airlines', 'Hết chỗ'),
(810, '2023-08-19', '3200000', '1200000', 870, '5200000', 'Vietnam Airlines', 'Còn chỗ'),
(811, '', '0', '0', 871, '0', '', ''),
(812, '2023-09-21', '3900000', '1500000', 873, '5900000', 'Bamboo Airways', 'Còn chỗ'),
(813, '2023-09-22', '3900000', '1500000', 873, '5600000', 'Viet Jet Airlines', 'Còn chỗ'),
(814, '2023-09-23', '2400000', '1200000', 873, '4400000', 'Viet Jet Airlines', 'Hết chỗ'),
(815, '2023-09-24', '2300000', '1200000', 873, '5200000', 'Viet Jet Airlines', 'Còn chỗ'),
(816, '2023-09-25', '2300000', '1200000', 873, '5200000', 'Viet Jet Airlines', 'Còn chỗ'),
(817, '2023-09-01', '3900000', '1500000', 874, '19000000', 'Viet Jet Airlines', 'Còn chỗ'),
(818, '2023-09-02', '3900000', '1500000', 874, '16000000', 'Viet Jet Airlines', 'Còn chỗ'),
(819, '2023-09-03', '3900000', '1500000', 874, '16000000', 'Viet Jet Airlines', 'Còn chỗ'),
(820, '2023-09-04', '3900000', '1500000', 874, '16000000', 'Viet Jet Airlines', 'Còn chỗ'),
(821, '2023-12-24', '0', '0', 875, '16000000', 'Bamboo Airways', 'Hết chỗ'),
(822, '2023-12-25', '3000000', '2000000', 875, '19000000', 'Vietnam Airlines', 'Còn chỗ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_discount_codes`
--

CREATE TABLE `tbl_discount_codes` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(10) NOT NULL,
  `code` varchar(10) NOT NULL,
  `code_auto` varchar(10) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `valid_to_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `publication_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_discount_codes`
--

INSERT INTO `tbl_discount_codes` (`id`, `type`, `code`, `code_auto`, `amount`, `valid_to_date`, `publication_status`) VALUES
(19, 'percent', 'DESHA2010', 'Zp8DxaqO3', '20', '2020-11-27 08:42:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dmtin`
--

CREATE TABLE `tbl_dmtin` (
  `dmtin_id` int(11) NOT NULL,
  `dmtin_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dmtin_title_bar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dmtin_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dmtin_long_description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `dmtin_image` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dmtin_short_description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `dmtin_keywords` text CHARACTER SET utf8 NOT NULL,
  `dmtin_short_description_seo` text CHARACTER SET utf8 NOT NULL,
  `dmtin_quantam` tinyint(4) DEFAULT NULL,
  `dmtin_author` int(11) NOT NULL,
  `dmtin_category` int(11) NOT NULL,
  `dmtin_tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `edit_date` timestamp NULL DEFAULT NULL,
  `user_create` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `user_edit` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `index_google` tinyint(4) NOT NULL DEFAULT '1',
  `publication_home` int(11) DEFAULT NULL,
  `dmtin_banner` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `dmtin_header` int(11) NOT NULL,
  `dmtin_stt` int(11) DEFAULT NULL,
  `dmtin_nv_ql` int(11) NOT NULL,
  `view` int(11) NOT NULL,
  `dmtin_feature` int(11) NOT NULL,
  `id_dd` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_dmtin`
--

INSERT INTO `tbl_dmtin` (`dmtin_id`, `dmtin_title`, `dmtin_title_bar`, `dmtin_slug`, `dmtin_long_description`, `dmtin_image`, `dmtin_short_description`, `dmtin_keywords`, `dmtin_short_description_seo`, `dmtin_quantam`, `dmtin_author`, `dmtin_category`, `dmtin_tag`, `create_date`, `edit_date`, `user_create`, `user_edit`, `publication_status`, `index_google`, `publication_home`, `dmtin_banner`, `dmtin_header`, `dmtin_stt`, `dmtin_nv_ql`, `view`, `dmtin_feature`, `id_dd`) VALUES
(411, 'MÙA HÈ HOKKAIDO CÓ GÌ ĐẶC BIỆT', 'MÙA HÈ HOKKAIDO CÓ GÌ ĐẶC BIỆT', 'mua-he-hokkaido-co-gi-dac-biet', '<p>Hokkaido (北海道, Hokkaidō) l&agrave; đảo lớn thứ hai nằm ở ph&iacute;a Bắc&nbsp;v&agrave; &iacute;t ph&aacute;t triển nhất trong bốn h&ograve;n đảo ch&iacute;nh của Nhật Bản.&nbsp;Hokkaido được thi&ecirc;n nhi&ecirc;n v&ocirc; c&ugrave;ng ưu &aacute;i với vẻ đẹp diệu kỳ của cảnh sắc non cao, s&ocirc;ng hồ, biển cả v&agrave; bầu trời trong vắt. Nơi đ&acirc;y l&agrave; một trong những địa danh nổi tiếng của Nhật Bản với sự thay đổi của khung cảnh thi&ecirc;n nhi&ecirc;n bốn m&ugrave;a.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4><img alt=\"\" src=\"https://viettourist.com/resources/images/BLOG-PIC/blog-dong-bac-a/hokkaido-tim.jpg\" /></h4>\r\n\r\n<h4>M&ugrave;a h&egrave; Hokkaido c&oacute; g&igrave; đặc biệt?</h4>\r\n\r\n<p>M&ugrave;a h&egrave; ở Hokkaido bắt đầu từ th&aacute;ng 6 v&agrave; kết th&uacute;c từ th&aacute;ng 8 hằng năm. Đ&acirc;y l&agrave; thời điểm m&agrave; ở h&ograve;n đảo n&agrave;y kh&ocirc;ng c&oacute; mưa với nhiệt độ trung b&igrave;nh nằm ở ngưỡng 20 độ C. Tuy nhi&ecirc;n, d&ugrave; l&agrave; m&ugrave;a h&egrave; nhưng v&agrave;o buổi tối, nhiệt độ của Hokkaido cũng c&oacute; thể xuống đến 10 độ. N&ecirc;n&nbsp;bạn cần phải chuẩn bị quần &aacute;o thật ấm khi đến tham quan du lịch tại đ&acirc;y.&nbsp;Ở Hokkaido m&ugrave;a h&egrave; diễn ra kh&aacute; ngắn. Tuy nhi&ecirc;n, đ&acirc;y lại l&agrave; thời điểm m&agrave; trăm hoa đua nở với cảnh sắc thi&ecirc;n nhi&ecirc;n đẹp động l&ograve;ng người. Do vậy, nếu muốn du lịch Hokkaido th&igrave; đ&acirc;y cũng được xem l&agrave; thời điểm l&yacute; tưởng để du kh&aacute;ch c&oacute; những trải nghiệm th&uacute; vị.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://viettourist.com/resources/images/BLOG-PIC/blog-dong-bac-a/hokkaido-2.jpg\" /></p>\r\n\r\n<p>Một khi đ&atilde; đến Hokkaido v&agrave;o m&ugrave;a h&egrave;, bạn cũng kh&ocirc;ng thể kh&ocirc;ng đến tham quan c&aacute;nh đồng Farm Tomita với những b&ocirc;ng hoa oải hương rực rỡ. Ở đ&acirc;y c&oacute; những c&aacute;nh đồng hoa trải d&agrave;i v&ocirc; tận khiến ai một khi đ&atilde; nh&igrave;n thấy đều kinh ngạc v&agrave; ngỡ ng&agrave;ng kh&ocirc;ng n&oacute;i n&ecirc;n lời. Đ&acirc;y kh&ocirc;ng chỉ l&agrave; địa điểm gi&uacute;p bạn c&oacute; những bức h&igrave;nh ho&agrave;n hảo m&agrave; c&ograve;n được dịp thu trọng cảnh sắc thi&ecirc;n nhi&ecirc;n xinh đẹp, h&ugrave;ng vĩ v&agrave;o trong tầm mắt.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://viettourist.com/resources/images/NHATBAN/nhatban-hokkaido/ho-Kawaguchi7.jpg\" /></p>\r\n\r\n<p>M&ugrave;a h&egrave; Hokkaido với những trải nghiệm thật tuyệt vời như: đường hầm bằng thủy tinh qua hồ bơi chim c&aacute;nh cụt ở Sở th&uacute; Asahiyama thuộc khu vực Asahikawa, ngắm nh&igrave;n một trong ba cảnh đ&ecirc;m đẹp nhất thế giới ở khu vực Hokkaido.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://viettourist.com/resources/images/BLOG-PIC/blog-dong-bac-a/shikotsuko4.jpg\" /></p>\r\n\r\n<p>M&ugrave;a h&egrave; Hokkaido c&ograve;n cho bạn được h&ograve;a m&igrave;nh giữa thi&ecirc;n nhi&ecirc;n rộng lớn chỉ c&oacute; ở v&ugrave;ng biển ph&iacute;a Bắc khi đặt ch&acirc;n đến khu vực Shiretoko, vườn quốc gia đẹp v&agrave; hoang sơ nhất Nhật Bản hay khu vực Ph&iacute;a đ&ocirc;ng Hokkaido với quang cảnh tuyệt vời của Hồ Mashu, Hồ Akan.</p>\r\n\r\n<p>Ăn g&igrave; ở Hokkaido?</p>\r\n\r\n<p>- Jingisukan: Đ&acirc;y l&agrave; m&oacute;n ăn được đặt theo t&ecirc;n của người s&aacute;ng lập ra Đế Chế M&ocirc;ng Cổ Th&agrave;nh C&aacute;t Tư H&atilde;n. Jingisukan l&agrave; m&oacute;n ăn được chế biến từ thịt cừu, nh&uacute;ng trong nước sốt v&agrave; nước ch&iacute;n ăn k&egrave;m rau cải.</p>\r\n\r\n<p>- C&aacute;c sản phẩm chế biến từ sữa: Hokkaido sở hữu những thảo nguy&ecirc;n rộng lớn n&ecirc;n c&oacute; ng&agrave;nh c&ocirc;ng nghiệp sản xuất sữa v&ocirc; c&ugrave;ng ph&aacute;t triển. Sản lượng sữa tại đ&acirc;y chiếm khoảng &frac12; tổng lượng sữa ở Nhật Bản. Do đ&oacute;, bạn c&oacute; thể thấy người d&acirc;n ở đ&acirc;y sử dụng sữa nhiều trong việc chế biến thức ăn.</p>\r\n\r\n<p>- Bia Sapporo: Đ&acirc;y l&agrave; loại bia được d&ugrave;ng phổ biến từ thời Minh Trị v&agrave; cũng l&agrave; loại bia l&acirc;u đời, nổi tiếng tại Nhật Bản. Du kh&aacute;ch c&oacute; thể đến thăm bảo t&agrave;ng bia ở Hokkaido v&agrave; thưởng thức hương vị bia đặc trưng ngay tại đ&acirc;y.</p>\r\n\r\n<p>- Ramen Hokkaido: M&oacute;n m&igrave; Ramen tại Hokkaido cực kỳ ngon v&agrave; hấp dẫn hơn rất nhiều so với c&aacute;c nơi kh&aacute;c tại Nhật Bản. C&oacute; đến 3 hương vị s&uacute;p Ramen cho du kh&aacute;ch lựa chọn l&agrave; sup Miso, s&uacute;p mang vị muối Sapporo, s&uacute;p nước tương,... Nước s&uacute;p tuyệt vời kết hợp với sợi m&igrave; đặc trưng của v&ugrave;ng n&agrave;y vừa c&oacute; vị b&eacute;o, vị dai tạo n&ecirc;n một hương vị ri&ecirc;ng biệt khiến người ăn nhớ m&atilde;i kh&ocirc;ng qu&ecirc;n.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://viettourist.com/resources/images/BLOG-PIC/blog-dong-bac-a/mua-he-1.png\" /></p>\r\n', 'ho-Kawaguchi1.jpg', '', 'MÙA HÈ HOKKAIDO CÓ GÌ ĐẶC BIỆT\r\n', 'MÙA HÈ HOKKAIDO CÓ GÌ ĐẶC BIỆT\r\n', 1, 1, 86, '', '2023-06-27 08:04:56', '2023-06-29 03:12:25', 'admin', 'admin', 1, 1, 1, NULL, 0, NULL, 1, 2, 1, NULL),
(412, '6 LỄ HỘI MÙA HÈ NHẬT BẢN', '6 LỄ HỘI MÙA HÈ NHẬT BẢN', '6-le-hoi-mua-he-nhat-ban', '<p>Ngo&agrave;i 2 m&ugrave;a hoa anh đ&agrave;o v&agrave; m&ugrave;a thu l&aacute; đỏ, th&igrave; Nhật Bản cũng c&oacute; nhiều điều th&uacute; vị để bạn kh&aacute;m ph&aacute; v&agrave;o m&ugrave;a h&egrave;, một trong những số đ&oacute; l&agrave; c&aacute;c lễ hội m&ugrave;a h&egrave; đậm bản sắc văn ho&aacute; v&agrave; v&ocirc; c&ugrave;ng th&uacute; vị.&nbsp;</p>\r\n\r\n<h4>1. Lễ hội Gion Matsuri - Kyoto</h4>\r\n\r\n<p>Lễ hội Gion Matsuri l&agrave; một trong những lễ hội l&acirc;u đời v&agrave; nổi tiếng nhất tại Nhật Bản. Hoạt động đặc biệt của lễ hội n&agrave;y l&agrave; những chiếc kiệu d&ugrave;ng để diễu h&agrave;nh - nặng 12 tấn v&agrave; cao đến 25 m&eacute;t. Kiệu sẽ được diễu h&agrave;nh dọc theo những con phố ch&iacute;nh của Kyoto. Trong Lễ hội Gion c&oacute; hai loại kiệu: yama v&agrave; hoko, cả 2 loại đều được trang ho&agrave;ng rất c&ocirc;ng phu v&agrave; tinh xảo.&nbsp;</p>\r\n\r\n<p>Điều đặc biệt: trước mỗi cuộc diễu h&agrave;nh sẽ c&oacute; 3 buổi tiệc đ&ecirc;m để ăn mừng. Kh&aacute;ch du lịch c&oacute; thể mặc yukata v&agrave; c&ugrave;ng ho&agrave; v&agrave;o những buổi tiệc, mua v&ograve;ng may mắn chimaki v&agrave; thưởng thức những ẩm thực đặc sắc của địa phương.</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Thời gian: th&aacute;ng 7</p>\r\n	</li>\r\n	<li>\r\n	<p>Địa điểm: Kyoto</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://viettourist.com/resources/images/BLOG-PIC/blog-dong-bac-a/le-hoi-gion.jpg\" /></p>\r\n\r\n<h4>2. Lễ hội Tenjin</h4>\r\n\r\n<p>Lễ hội Tenjin phản &aacute;nh đậm n&eacute;t ảnh hưởng của Thần đạo v&agrave;o cuộc sống người d&acirc;n Nhật Bản thời xa xưa. V&igrave; theo nghi lễ của Thần đạo, n&ecirc;n lễ hội Tenjin sẽ c&oacute; nhiều n&eacute;t giống lễ hội Sanno.Vẫn c&oacute; nhiều nghi thức, vũ điệu v&agrave; &acirc;m nhạc với m&agrave;n rước đền thờ. Cuối lễ sẽ c&oacute; m&agrave;n tr&igrave;nh diễn ph&aacute;o hoa rực rỡ k&eacute;o d&agrave;i hơn một tiếng rưỡi. Sau đ&oacute;, du kh&aacute;ch c&ograve;n được chi&ecirc;m ngưỡng đ&aacute;m rước bằng thuyền, chở theo đền thờ di động v&agrave; một số thuyền c&ograve;n lại sẽ đốt lửa. Kh&aacute;ch du lịch c&oacute; thể thoải m&aacute;i mặc yukata v&agrave; c&ugrave;ng tham gia lễ hội với d&acirc;n địa phương.</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Thời gian: ng&agrave;y 24-25/7 dương lịch</p>\r\n	</li>\r\n	<li>\r\n	<p>Địa điểm: Osaka</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://viettourist.com/resources/images/BLOG-PIC/blog-dong-bac-a/Tenjin%20Matsuri.JPG\" /></p>\r\n\r\n<h4>3. Lễ hội Obon - lễ vu lan Nhật Bản</h4>\r\n\r\n<p>Lễ hội Obon c&oacute; nguồn gốc từ Phật gi&aacute;o, đ&acirc;y l&agrave; dịp để con c&aacute;i thể hiện l&ograve;ng k&iacute;nh trọng v&agrave; y&ecirc;u thương đối với bậc sinh th&agrave;nh. Lễ Obon sẽ được tổ chức tuỳ theo thời gian kh&aacute;c nhau ở c&aacute;c v&ugrave;ng miền. Ở v&ugrave;ng Kanto c&oacute; Tokyo, Yokohama v&agrave; Tonhoku sẽ được tổ chức v&agrave;o 15/7 dương lịch. Tuy ng&agrave;y lễ v&agrave;o ng&agrave;y 15, nhưng từ ng&agrave;y 12 đ&atilde; c&oacute; những hoạt động mừng dịp lễ n&agrave;y. V&agrave;o ng&agrave;y 14, 15 người d&acirc;n sẽ thăm viếng mộ, lau ch&ugrave;i, dọn vệ sinh phần mộ v&agrave; c&uacute;ng với mục đ&iacute;ch mời người th&acirc;n qu&aacute; cố quay về thăm nh&agrave;.&nbsp;</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Thời gian: 15/7, 15/8 dương lịch&nbsp;</p>\r\n	</li>\r\n	<li>\r\n	<p>Địa điểm: to&agrave;n quốc</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;<img alt=\"\" src=\"https://viettourist.com/resources/images/DULICHNHATBAN/mua-le-hoi-nhat-ban.jpg\" /></p>\r\n\r\n<h4>4. Lễ hội Sanno</h4>\r\n\r\n<p>Lễ hội Sanno l&agrave; 1 trong 3 lễ hội lớn từ thời Edo v&agrave; c&ograve;n tồn tại cho đến ng&agrave;y nay. Lễ hội được tổ chức từ ng&agrave;y 7 đến 17 th&aacute;ng 6 của năm chẵn, mang &yacute; nghĩa b&agrave;y tỏ sự biết ơn đối với c&aacute;c vị thần bảo hộ th&agrave;nh phố. Người ta sẽ mời c&aacute;c vị thần ngự v&agrave;o những chiếc kiệu v&agrave; sẽ diễu h&agrave;nh xung quanh th&agrave;nh phố. Với thời gian rước lễ gần 9 tiếng v&agrave; đi xung quanh khắp con phố, bạn sẽ thấy được sự ho&agrave;nh tr&aacute;ng của hơn 500 vũ c&ocirc;ng v&agrave; c&aacute;c người tham gia mặc những trang phục đặc biệt sẽ tạo n&ecirc;n khung cảnh v&ocirc; c&ugrave;ng đặc sắc. Lễ hội n&agrave;y cũng thu h&uacute;t rất nhiều du kh&aacute;ch đến tham dự h&agrave;ng năm.</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Thời gian: th&aacute;ng 6</p>\r\n	</li>\r\n	<li>\r\n	<p>Địa điểm: Tokyo</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://viettourist.com/resources/images/DULICHNHATBAN/le-hoi-Sanno.jpg\" /></p>\r\n\r\n<h4>5. Lễ Thất tịch</h4>\r\n\r\n<p>Nếu đ&atilde; quen thuộc c&acirc;u chuyện Ngưu Lang - Chức Nữ v&agrave;o th&aacute;ng 7, bạn cũng sẽ kh&ocirc;ng xa lạ g&igrave; với lễ hội Tanabata tại Nhật Bản. Lễ hội Tanabata để t&ocirc;n vinh về c&acirc;u chuyện t&igrave;nh của n&agrave;ng Orihime v&agrave; ch&agrave;ng chăn b&ograve; Hikoboshi với 2 ch&ograve;m sao Ngưu Lang - Chức Nữ, cầu mong ước trở th&agrave;nh những người phụ nữ kh&eacute;o l&eacute;o cả nữ c&ocirc;ng gia ch&aacute;nh v&agrave; lĩnh vực thư ph&aacute;p.&nbsp;</p>\r\n\r\n<p>Hằng năm, v&agrave;o lễ hội n&agrave;y, người Nhật sẽ viết những điều ước v&agrave; treo l&ecirc;n những c&agrave;nh tr&uacute;c để để cầu khấn Orihime sẽ gi&uacute;p họ kh&eacute;o l&eacute;o trong việc may v&aacute;, viết chữ đẹp cũng như mong muốn Hikoboshi sẽ mang đến cho họ những vụ m&ugrave;a bội thu v&agrave; sự thịnh vượng. C&aacute;c trẻ em cũng sẽ ghi những điều ước v&agrave; treo l&ecirc;n c&agrave;nh tr&uacute;c v&agrave; ghi về ước mơ của m&igrave;nh. Chỉ c&oacute; 3 nơi tổ chức lễ hội lớn l&agrave; Sendai, Hiratsuka v&agrave; Anjou, trong đ&oacute; th&agrave;nh phố Sendai tổ chức lớn v&agrave; đặc sắc nhất</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Thời gian: trong khoảng 6-8/7 dương lịch</p>\r\n	</li>\r\n	<li>\r\n	<p>Địa điểm: th&agrave;nh phố Sendai</p>\r\n	</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://viettourist.com/resources/images/BLOG-PIC/blog-dong-bac-a/den-long.jpg\" /></p>\r\n', 'chua-vang-kinkakuji6.jpg', '', '6 LỄ HỘI MÙA HÈ NHẬT BẢN\r\n', '6 LỄ HỘI MÙA HÈ NHẬT BẢN\r\n', 1, 1, 86, '', '2023-06-27 08:05:42', NULL, 'admin', NULL, 1, 1, 1, NULL, 0, NULL, 1, 2, 1, NULL),
(413, 'MÙA ĐÔNG NƯỚC ÚC CÓ BUỒN KHÔNG?', 'MÙA ĐÔNG NƯỚC ÚC CÓ BUỒN KHÔNG?', 'mua-dong-nuoc-uc-co-buon-khong', '<p>M&ugrave;a đ&ocirc;ng nước &Uacute;c c&oacute; buồn kh&ocirc;ng? C&acirc;u trả lời l&agrave; kh&ocirc;ng nh&eacute;! M&ugrave;a đ&ocirc;ng nước &Uacute;c kh&ocirc;ng ảm đạm, lạnh lẽ m&agrave; thay v&agrave;o đ&oacute; đ&acirc;y l&agrave; m&ugrave;a m&agrave; mọi người tr&ocirc;ng đợi nhất năm bởi những hoạt động v&agrave; những lễ hội th&uacute; vị!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://viettourist.com/resources/images/BLOG-PIC/Blog-uc/thuat-bloh-3.jpg\" /></p>\r\n\r\n<p>Th&aacute;ng 6 đến th&aacute;ng 8 l&agrave; l&uacute;c m&ugrave;a đ&ocirc;ng g&otilde; cửa nước &Uacute;c. Đ&acirc;y l&agrave; l&uacute;c thời tiết ở &Uacute;c lạnh nhất trong năm. Hai th&agrave;nh phố lạnh nhất ở &Uacute;c v&agrave;o thời gian n&agrave;y l&agrave; thủ đ&ocirc; Canberra v&agrave; Tasmania. Khi m&ugrave;a đ&ocirc;ng chạm ng&otilde;, nhiệt độ trung b&igrave;nh ở Tasmania khoảng từ 6⁰C (ban đ&ecirc;m) đến 15⁰C (ban ng&agrave;y). C&aacute;c th&agrave;nh phố lớn như Sydney v&agrave; Melbourne cũng kh&aacute; lạnh nhưng kh&ocirc;ng bằng hai nơi kia. Tuy kh&ocirc;ng thường xuống mức &acirc;m như ở nhiều quốc gia Ch&acirc;u &Acirc;u, nhưng đối với những ai đến từ c&aacute;c quốc gia xứ n&oacute;ng (như ở Việt Nam) th&igrave; sẽ kh&oacute; để th&iacute;ch nghi ngay.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://viettourist.com/resources/images/BLOG-PIC/Blog-uc/thuat-bloh-2.jpg\" /></p>\r\n\r\n<p>M&ugrave;a đ&ocirc;ng &Uacute;c c&oacute; tuyết phủ d&agrave;y đặc tr&ecirc;n một số đỉnh n&uacute;i cao v&agrave; trượt tuyết ch&iacute;nh l&agrave; hoạt động kh&ocirc;ng thể thiếu trong đời sống cũng như giải tr&iacute; &Uacute;c.&nbsp;Trong suốt m&ugrave;a đ&ocirc;ng, tr&ecirc;n c&aacute;c sườn n&uacute;i tuyết của bang Victoria mở ra khung cảnh tuyệt đẹp. Trong đ&oacute;, khu Mt.Buller nằm tr&ecirc;n độ cao 1800m so với mực nước biển l&agrave; điểm đến l&yacute; tưởng với những sườn đồi, c&aacute;nh rừng phủ tuyết trắng x&oacute;a. Ngo&agrave;i trượt tuyết tr&ecirc;n những con đường d&agrave;i thoai thoải, bạn c&ograve;n c&oacute; thể chơi đắp tuyết, n&eacute;m tuyết, nhảy từ tr&ecirc;n cao xuống với d&acirc;y l&ograve; xo, hoặc đi c&aacute;p treo ngắm to&agrave;n cảnh Mt.Buller đẹp như tranh.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://viettourist.com/resources/images/BLOG-PIC/Blog-uc/truot-tuyet-7.jpg\" /></p>\r\n\r\n<p>Đ&acirc;y c&ograve;n l&agrave; thời điểm của c&aacute;c lễ hội đặc sắc diễn ra như lễ hội Gi&aacute;ng sinh th&aacute;ng 7, lễ hội &aacute;nh s&aacute;ng VIVID Live &ndash; Sydney, ngắm c&aacute; voi tại Coast bang New South Wales, lễ hội ẩm thực S&ocirc;ng Margaret &ndash; Bang T&acirc;y &Uacute;c, Lễ Hội M&ugrave;a Đ&ocirc;ng cực kỳ nổi tiếng ở Blue Mountains, triển l&atilde;m nghệ thuật độc đ&aacute;o Lễ Hội M&ugrave;a Đ&ocirc;ng Alpine,..</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://viettourist.com/resources/images/BLOG-PIC/Blog-uc/hinh-blog7-18.jpg\" /></p>\r\n', 'thuat-bloh-2.jpg', '', 'MÙA ĐÔNG NƯỚC ÚC CÓ BUỒN KHÔNG?\r\n', 'MÙA ĐÔNG NƯỚC ÚC CÓ BUỒN KHÔNG?\r\n', NULL, 1, 86, '', '2023-06-27 08:06:16', NULL, 'admin', NULL, 1, 1, 1, NULL, 0, NULL, 1, 2, 1, NULL),
(414, 'VIETTOURIST THÔNG BÁO VỀ VIỆC BAMBOO AIRWAYS NGỪNG BAY CHUYẾN HỒ CHÍ MINH', 'VIETTOURIST THÔNG BÁO VỀ VIỆC BAMBOO AIRWAYS NGỪNG BAY CHUYẾN HỒ CHÍ MINH', 'viettourist-thong-bao-ve-viec-bamboo-airways-ngung-bay-chuyen-ho-chi-minh', '<p><strong>[ TH&Ocirc;NG B&Aacute;O ]<br />\r\n= = = = = = = = = = = = =<br />\r\nC&ocirc;ng ty du lịch Viettourist xin được gửi Th&ocirc;ng B&aacute;o tới Qu&yacute; kh&aacute;ch h&agrave;ng về việc NGỪNG (hủy) chuyến bay của h&atilde;ng H&agrave;ng kh&ocirc;ng Bamboo từ Hồ Ch&iacute; Minh đến TRƯƠNG GIA GIỚI từ ng&agrave;y 08/06/2023 (c&oacute; văn bản ngừng bay).&nbsp;<br />\r\n- V&igrave; vậy, C&ocirc;ng ty ch&uacute;ng t&ocirc;i rất lấy l&agrave;m tiếc v&agrave; xin lỗi qu&yacute; kh&aacute;ch h&agrave;ng về sự cố kh&ocirc;ng mong muốn khi đ&atilde; đăng k&yacute; tour&nbsp;TRƯƠNG GIA GIỚI - PHƯỢNG HO&Agrave;NG CỔ TRẤN&nbsp;5 ng&agrave;y 4 đ&ecirc;m v&agrave; 6 ng&agrave;y 5 đ&ecirc;m bay của h&atilde;ng Bamboo c&oacute; ng&agrave;y khởi h&agrave;nh từ ng&agrave;y 7/6 trở đi sẽ kh&ocirc;ng thực hiện được bởi h&atilde;ng NGỪNG BAY.&nbsp;<br />\r\n- Để đảm bảo thực hiện được chương tr&igrave;nh tour TGG - PHCT C&ocirc;ng ty đ&atilde; b&aacute;n cho kh&aacute;ch h&agrave;ng, ch&uacute;ng t&ocirc;i đ&atilde; mua lại v&eacute; của h&atilde;ng Vietjet đang bay charter đến Trương Gia Giới để tiếp tục thực hiện chương tr&igrave;nh tour du lịch. Rất mong nhận được sự th&ocirc;ng cảm v&agrave; mong được tiếp tục đồng h&agrave;nh c&ugrave;ng qu&yacute; kh&aacute;ch h&agrave;ng.</strong></p>\r\n\r\n<p><br />\r\n<em>- Mời qu&yacute; kh&aacute;ch tham khảo th&ecirc;m ch&ugrave;m tour Dulichtrunghoa.com - Khởi h&agrave;nh h&agrave;ng tuần, lễ, Tết, h&egrave;...<br />\r\n* Tour trọn g&oacute;i gồm v&eacute; m&aacute;y bay &amp; dịch vụ từ a-z<br />\r\n* Thủ tục đơn giản: CHỈ CẦN HỘ CHIẾU C&Ograve;N HẠN TR&Ecirc;N 6 TH&Aacute;NG</em></p>\r\n\r\n<p><em><img alt=\"\" src=\"https://viettourist.com/resources/images/phuong-hoang-co-tran/lich-khoi-hanh-trunghoa.jpg\" /></em></p>\r\n', 'thucte-phct-moi-10.jpg', '', 'VIETTOURIST THÔNG BÁO VỀ VIỆC BAMBOO AIRWAYS NGỪNG BAY CHUYẾN HỒ CHÍ MINH', 'VIETTOURIST THÔNG BÁO VỀ VIỆC BAMBOO AIRWAYS NGỪNG BAY CHUYẾN HỒ CHÍ MINH', NULL, 1, 86, '', '2023-06-27 08:08:55', NULL, 'admin', NULL, 1, 1, 1, NULL, 0, NULL, 1, 2, 1, NULL),
(415, 'VISA DU LỊCH NHẬT BẢN: BẠN CẦN CHUẨN BỊ GÌ?', 'VISA DU LỊCH NHẬT BẢN: BẠN CẦN CHUẨN BỊ GÌ?', 'visa-du-lich-nhat-ban-ban-can-chuan-bi-gi', '<p>Với những th&agrave;nh phố hiện đại, s&ocirc;i động, phong cảnh n&ocirc;ng th&ocirc;n xinh đẹp v&agrave; nền ẩm thực nổi tiếng thế giới,&nbsp;<a href=\"https://viettourist.com/tours/du-lich-nhat-ban-cid-220.html\"><strong>Nhật Bản</strong></a>&nbsp;l&agrave; một trong những điểm dừng ch&acirc;n h&agrave;ng đầu cho những du kh&aacute;ch đang t&igrave;m kiếm một trải nghiệm ch&acirc;u &Aacute; trọn vẹn. Những th&ocirc;ng tin sau đ&acirc;y sẽ t&oacute;m tắt về c&aacute;c y&ecirc;u cầu nhập cảnh mới nhất của Nhật Bản v&agrave; một số thủ tục, giấy tờ cần chuẩn bị trước chuyến đi để gi&uacute;p bạn nhập cảnh v&agrave;o Nhật Bản diễn ra dễ d&agrave;ng hơn.</p>\r\n\r\n<h4><strong>Ai Sẽ Cần Xin Visa Du Lịch Nhật Bản?</strong></h4>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><img alt=\"\" src=\"https://viettourist.com/resources/images/BLOG-PIC/blog-dong-bac-a/chuavang-5.jpg\" /></strong></p>\r\n\r\n<p>Nếu bạn muốn nhập cảnh v&agrave;o Nhật Bản với mục đ&iacute;ch du lịch, thăm gia đ&igrave;nh hoặc bạn b&egrave;, hoặc c&aacute;c mục đ&iacute;ch giải tr&iacute; v&agrave; ngắn hạn kh&aacute;c m&agrave; kh&ocirc;ng y&ecirc;u cầu bạn tham gia v&agrave;o c&aacute;c hoạt động được trả tiền, bạn cần c&oacute; visa du lịch Nhật Bản.</p>\r\n\r\n<p>C&ocirc;ng d&acirc;n của 68 quốc gia được miễn visa nhập cảnh v&agrave;o Nhật Bản với mục đ&iacute;ch du lịch. C&aacute;c quốc gia n&agrave;y bao gồm Anh, Mỹ, Canada, Mexico, &Uacute;c, Singapore, Malaysia v&agrave; hầu hết c&aacute;c nước ở ch&acirc;u &Acirc;u, với thời gian lưu tr&uacute; được cấp tại thời điểm hạ c&aacute;nh c&oacute; thể l&ecirc;n đến 90 ng&agrave;y.</p>\r\n\r\n<p>C&ocirc;ng d&acirc;n Việt Nam, Trung Quốc, Philippines v&agrave; một số quốc gia kh&aacute;c sẽ cần phải đăng k&yacute; visa du lịch th&ocirc;ng qua Đại sứ qu&aacute;n, Tổng l&atilde;nh sự qu&aacute;n Nhật Bản gần nhất tại quốc gia của m&igrave;nh, hoặc c&aacute;c Đại l&yacute; ủy th&aacute;c được chỉ định bởi Đại sứ qu&aacute;n Nhật Bản.</p>\r\n\r\n<p>Về nguy&ecirc;n tắc, visa ngắn hạn Nhật bản chỉ c&oacute; hiệu lực nhập cảnh 1 lần. Tuy nhi&ecirc;n, dựa v&agrave;o mục đ&iacute;ch nhập cảnh m&agrave; Đại sứ qu&aacute;n Nhật Bản c&oacute; thể cấp loại visa để bạn c&oacute; thể nhập cảnh nhiều lần, với thời gian ở Nhật Bản tổng cộng kh&ocirc;ng qu&aacute; nửa năm, d&ugrave; l&agrave; theo mục đ&iacute;ch thăm th&acirc;n, thăm bạn b&egrave; hay du lịch, c&ocirc;ng t&aacute;c v.v.</p>\r\n\r\n<h4><strong>Visa Du Lịch Nhật Bản C&oacute; Thời Hạn Bao L&acirc;u?</strong></h4>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><img alt=\"\" src=\"https://viettourist.com/resources/images/BLOG-PIC/blog-dong-bac-a/chuanuoc-2.jpg\" /></strong></p>\r\n\r\n<p>Visa du lịch tạm thời cho ph&eacute;p bạn lưu tr&uacute; tại Nhật Bản tối đa 90 ng&agrave;y với c&aacute;c mục đ&iacute;ch như: c&ocirc;ng t&aacute;c ngắn hạn, thăm th&acirc;n/người quen hoặc tham quan du lịch.</p>\r\n\r\n<p>Lưu &yacute;, đơn xin visa cho mục đ&iacute;ch &ldquo;du lịch&rdquo; c&oacute; nghĩa l&agrave; đơn xin visa cho mục đ&iacute;ch tham quan, đi chơi, ngắm cảnh, kh&ocirc;ng được ph&eacute;p thực hiện bất kỳ hoạt động kinh doanh n&agrave;o tạo ra doanh thu hoặc nhận th&ugrave; lao.</p>\r\n\r\n<p>Thời hạn của visa l&agrave; 3 th&aacute;ng v&agrave; kh&ocirc;ng được gia hạn.</p>\r\n\r\n<h4><strong>Đơn Xin Visa Du Lịch Nhật Bản Cần Những Giấy Tờ G&igrave;?</strong></h4>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><img alt=\"\" src=\"https://viettourist.com/resources/images/BLOG-PIC/blog-dong-bac-a/lau-dai%202.jpg\" /></strong></p>\r\n', 'chuanuoc-5.jpg', '', 'VISA DU LỊCH NHẬT BẢN: BẠN CẦN CHUẨN BỊ GÌ?\r\n', 'VISA DU LỊCH NHẬT BẢN: BẠN CẦN CHUẨN BỊ GÌ?\r\n', 1, 1, 86, '', '2023-06-27 08:09:46', NULL, 'admin', NULL, 1, 1, 1, NULL, 0, NULL, 1, 4, 1, NULL),
(416, 'VÌ SAO BHUTAN LÀ QUỐC GIA HẠNH PHÚC NHẤT THẾ GIỚI?', 'VÌ SAO BHUTAN LÀ QUỐC GIA HẠNH PHÚC NHẤT THẾ GIỚI?', 'vi-sao-bhutan-la-quoc-gia-hanh-phuc-nhat-the-gioi', '<p>Thời gian gần đ&acirc;y, du lịch Bhutan đang dần nổi l&ecirc;n như một hiện tượng trong cộng đồng người y&ecirc;u kh&aacute;m ph&aacute;. Bhutan nằm giữa Trung Quốc v&agrave; Ấn độ với diện t&iacute;ch 47.500m2, d&acirc;n số khoảng&nbsp; 750.000 người. Kinh tế Bhutan chủ yếu l&agrave; khai th&aacute;c thủy điện v&agrave; b&aacute;n cho Ấn Độ. Mặc d&ugrave; kinh tế Bhutan xếp v&agrave;o hạng thấp nhất thế giới, nạn m&ugrave; chữ v&agrave; đ&oacute;i ngh&egrave;o vẫn c&ograve;n l&agrave; vấn đề nan giải nhưng người d&acirc;n nơi đ&acirc;y lại c&oacute; cuộc sống hạnh ph&uacute;c nhất h&agrave;nh tinh.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://viettourist.com/resources/images/BHUTAN-5N4D/Punakha-Dzong24.jpg\" /></p>\r\n\r\n<p>Với phong cảnh thi&ecirc;n nhi&ecirc;n đẹp tuyệt vời, kh&ocirc;ng kh&iacute; tươi m&aacute;t, d&acirc;n số kh&ocirc;ng qu&aacute; đ&ocirc;ng c&ugrave;ng với &yacute; thức v&agrave; l&ograve;ng tự t&ocirc;n d&acirc;n tộc m&agrave; người d&acirc;n Bhutan lu&ocirc;n cảm thấy h&agrave;i l&ograve;ng với cuộc sống nơi đ&acirc;y. Bhutan nằm kề d&atilde;y Himalaya, gần 60% diện t&iacute;ch c&ograve;n rất hoang sơ v&agrave; một nửa diện t&iacute;ch được bảo vệ trong c&aacute;c khu vườn quốc gia do đ&oacute; m&agrave; cảnh vật, thi&ecirc;n nhi&ecirc;n Bhutan c&ograve;n rất hoang sơ v&agrave; trong l&agrave;nh. Đặc biệt, d&ugrave; kinh tế c&ograve;n chưa ph&aacute;t triển nhưng &yacute; thức bảo vệ m&ocirc;i trường của người d&acirc;n nơi đ&acirc;y rất cao.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://viettourist.com/resources/images/BHUTAN-5N4D/Thimphu-Chorten1.jpg\" /></p>\r\n\r\n<p>Kh&ocirc;ng chỉ quan t&acirc;m đến vấn đề m&ocirc;i trường m&agrave; cả người d&acirc;n v&agrave; ch&iacute;nh phủ đều xem trong vấn đề sức khỏe, đề cao một cuộc sống l&agrave;nh mạnh. Đến Bhutan du lịch, du kh&aacute;ch c&oacute; thể sẽ ngạc nhi&ecirc;n v&igrave; người d&acirc;n ở đ&acirc;y hầu như chẳng quan t&acirc;m đến internet, truyền h&igrave;nh hay smartphone. Họ c&oacute; lối sống cũng rất l&agrave;nh mạnh v&agrave; điều độ, ngủ nghỉ v&agrave; sinh hoạt đ&uacute;ng giờ. Thậm ch&iacute; để bảo vệ sức khỏe người d&acirc;n, Bhutan l&agrave; ch&iacute;nh phủ đầu ti&ecirc;n tr&ecirc;n thế giới cấm b&aacute;n v&agrave; h&uacute;t thuốc l&aacute; triệt để từ năm 2004. Đảm bảo vệ sinh an to&agrave;n thực phẩm cũng l&agrave; ưu ti&ecirc;n h&agrave;ng đầu của ch&iacute;nh phủ nước n&agrave;y khi m&agrave; việc nhập khẩu c&aacute;c loại ph&acirc;n b&oacute;n h&oacute;a học đều bị cấm. B&ecirc;n cạnh đ&oacute;, việc hầu hết người d&acirc;n đều theo đạo Phật, v&agrave; ăn chay n&ecirc;n ẩm thực của họ cũng thanh đạm v&agrave; rất tốt cho sức khỏe.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://viettourist.com/resources/images/BHUTAN-5N4D/paro-taktsang16.jpg\" />.</p>\r\n\r\n<p>X&atilde; hội Bhutan l&agrave; một x&atilde; hội đ&aacute;ng mơ ước khi m&agrave; người d&acirc;n sống rất th&acirc;n thiện, t&acirc;m lu&ocirc;n hướng về những gi&aacute;o l&yacute; Phật gi&aacute;o, l&agrave;m điều thiện, tr&aacute;nh điều &aacute;c, kh&ocirc;ng ph&acirc;n biệt gia cấp gi&agrave;u ngh&egrave;o. Đối với họ, một cuộc sống hạnh ph&uacute;c l&agrave; khi trong l&ograve;ng mỗi người kh&ocirc;ng c&oacute; hờn ghen, hơn thua. &nbsp;Ch&iacute;nh v&igrave; vậy, m&agrave; người d&acirc;n Bhutan lu&ocirc;n cảm thấy m&atilde;n nguyện, h&agrave;i l&ograve;ng với cuộc sống m&agrave; họ đang c&oacute;.</p>\r\n\r\n<p><img alt=\"\" src=\"https://viettourist.com/resources/images/BHUTAN-5N4D/Punakha-Dzong23.jpg\" /></p>\r\n\r\n<p>Du lịch Bhutan&nbsp;l&agrave; mơ ước của rất nhiều du kh&aacute;ch, bởi kh&ocirc;ng chỉ để chi&ecirc;m ngưỡng một đất nước xinh đẹp m&agrave; c&ograve;n thấy tận mắt, nghe tận tai một x&atilde; hội hạnh ph&uacute;c trong mơ m&agrave; bất cứ quốc gia n&agrave;o cũng phải ao ước. Bhutan tuy kh&ocirc;ng gi&agrave;u về vật chất, nhưng người d&acirc;n nơi đ&acirc;y lu&ocirc;n gi&agrave;u c&oacute; về l&ograve;ng tin y&ecirc;u v&agrave; sự thấu hiểu cho nhau.</p>\r\n\r\n<p>Ch&ugrave;m tour Quốc gia hạnh phục Bhutan đ&atilde; c&oacute; tại Viettourist:&nbsp;<a href=\"https://viettourist.com/tours/du-lich-bhutan-cid-557.html?fbclid=IwAR1MVSjODUmkvBS6R0cyWYjhOCm3jiVJAQ79rRYFsfnKr7XgtDhlrWLnulg\" rel=\"noopener nofollow\" target=\"_blank\">https://viettourist.com/tours/du-lich-bhutan-cid-557.html</a></p>\r\n\r\n<p>- G&iacute;a Tour trọn g&oacute;i gồm v&eacute; m&aacute;y bay khứ hồi (nếu c&oacute;), ăn uống, ngủ nghỉ, tham quan, hướng dẫn vi&ecirc;n suốt tuyến, xe lu&acirc;n chuyển....<br />\r\n- Khởi h&agrave;nh từ HCM &amp; c&aacute;c tỉnh th&agrave;nh tr&ecirc;n to&agrave;n quốc.</p>\r\n', 'paro-taktsang15.jpg', '', 'VÌ SAO BHUTAN LÀ QUỐC GIA HẠNH PHÚC NHẤT THẾ GIỚI?\r\n', 'VÌ SAO BHUTAN LÀ QUỐC GIA HẠNH PHÚC NHẤT THẾ GIỚI?\r\n', NULL, 1, 86, '', '2023-06-27 08:10:34', NULL, 'admin', NULL, 1, 1, 1, NULL, 0, NULL, 1, 6, 1, NULL),
(417, 'TEAMBUILDING là gì', 'TEAMBUILDING là gì', 'teambuilding-la-gi', '<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/03/TEAM-BUILDING.jpg\" style=\"height:500px; width:1200px\" /></p>\r\n\r\n<p>Teambuilding&nbsp;được xem l&agrave; dịp quan trọng gi&uacute;p nh&acirc;n sự lấy lại năng lượng, tăng th&ecirc;m gắn kết v&agrave; mang đến nhiều nguồn cảm hứng, &yacute; tưởng mới mẻ.&nbsp;&nbsp;</p>\r\n\r\n<p>Teambuilding&nbsp;l&agrave; một chuỗi c&aacute;c hoạt động, trong đ&oacute; bao gồm nghỉ dưỡng, du lịch, c&aacute;c tr&ograve; chơi tập thể, gala dinner tổng kết&hellip; của c&aacute;c c&ocirc;ng ty, tổ chức, doanh nghiệp nhằm kết nối tập thể c&ocirc;ng ty. Mỗi năm, c&aacute;c tổ chức, c&ocirc;ng ty th&ocirc;ng thường sẽ tổ chức 1 đến 2 đợt&nbsp;Teambuilding&nbsp;để thay mới kh&ocirc;ng kh&iacute;, tạo động lực l&agrave;m việc cho mọi th&agrave;nh vi&ecirc;n.</p>\r\n\r\n<h3>&Yacute; NGHĨA CỦA&nbsp;TEAMBUILDING</h3>\r\n\r\n<p>Teambuilding&nbsp;c&oacute; &yacute; nghĩa l&agrave; x&acirc;y dựng m&ocirc; h&igrave;nh T.E.A.M với c&aacute;c yếu tố cần c&oacute; gồm:</p>\r\n\r\n<ul>\r\n	<li><strong>T &ndash; Trust &ndash; Tin tưởng:&nbsp;</strong>sự t&ocirc;n trọng v&agrave; tin tưởng lẫn nhau giữa c&aacute;c th&agrave;nh vi&ecirc;n trong team l&agrave; điều quan trọng, cần thiết.</li>\r\n	<li><strong>E &ndash; Engagement &ndash; Tương t&aacute;c, hợp t&aacute;c:&nbsp;</strong>tất cả th&agrave;nh vi&ecirc;n cần đ&oacute;ng g&oacute;p &yacute; kiến để hướng đến mục ti&ecirc;u chung trong c&aacute;c tr&ograve; chơi. Nhờ đ&oacute; tạo được sự tương t&aacute;c v&agrave; thống nhất &yacute; kiến của mọi người trong team.</li>\r\n	<li><strong>A &ndash; Aspiration &ndash; Sự kh&aacute;t khao:</strong>&nbsp;c&aacute;c tr&ograve; chơi trong team gi&uacute;p người chơi c&oacute; chung mục ti&ecirc;u để c&ugrave;ng nhau cố gắng đạt th&agrave;nh quả như mong đợi.</li>\r\n	<li><strong>M &ndash; Mentoring &ndash; Sự cố vấn:&nbsp;</strong>hoạt động trong&nbsp;Teambuilding&nbsp;gi&uacute;p c&aacute;c th&agrave;nh vi&ecirc;n c&oacute; cơ hội tốt để giao lưu, học hỏi v&agrave; hỗ trợ lẫn nhau. Từ đ&oacute;, mối quan hệ giữa c&aacute;c nh&acirc;n vi&ecirc;n với nhau v&agrave; với ban l&atilde;nh đạo cũng cởi mở, gần gũi hơn.</li>\r\n</ul>\r\n\r\n<p>C&aacute;c hoạt động tại&nbsp;Teambuilding&nbsp;sẽ cải thiện được c&aacute;c mối quan hệ trong c&ocirc;ng ty, x&acirc;y dựng t&iacute;nh s&aacute;ng tạo, độc lập, sự tự tin cho nh&acirc;n vi&ecirc;n, cải thiện khả năng giao tiếp, khả năng hiểu nhau của c&aacute;c bạn nh&acirc;n vi&ecirc;n trong c&ocirc;ng ty, tạo n&ecirc;n sự gắn kết trong nội bộ v&agrave; lan tỏa văn h&oacute;a c&ocirc;ng ty.</p>\r\n\r\n<h3>C&Aacute;C H&Igrave;NH THỨC, &Yacute; TƯỞNG TRONG&nbsp;TEAMBUILDING</h3>\r\n\r\n<p>Để c&oacute; được &yacute; tưởng, chủ đề&nbsp;Teambuilding, doanh nghiệp cần x&aacute;c định được mục đ&iacute;ch của&nbsp;Teambuilding&nbsp;lần n&agrave;y l&agrave; g&igrave;? Loại h&igrave;nh cho hoạt động lần n&agrave;y l&agrave; như thế n&agrave;o?</p>\r\n\r\n<p>C&aacute;c h&igrave;nh thức&nbsp;Teambuilding&nbsp;thường thấy l&agrave;:</p>\r\n\r\n<h4>Theo địa điểm tổ chức&nbsp;Teambuilding:</h4>\r\n\r\n<p>1.&nbsp;<strong><em>TeamBuilding&nbsp;Trong Nh&agrave;:</em></strong></p>\r\n\r\n<p>Teambuilding&nbsp;trong nh&agrave; (Team buiding Indoor) l&agrave; c&aacute;c hoạt động vui chơi tập thể diễn ra trong kh&ocirc;ng gian nhỏ hẹp, thường được tổ chức với thời gian ngắn, nội dung đơn giản, dễ hiểu.</p>\r\n\r\n<p>Teambuilding&nbsp;trong nh&agrave; thường được kết hợp với tổ chức sự kiện hội nghị, hội thảo nhằm hướng đến mục ti&ecirc;u đ&agrave;o tạo, ph&aacute;t triển c&aacute;c kỹ năng l&agrave;m việc nh&oacute;m.Nh&igrave;n chung, tr&ograve; chơi&nbsp;Teambuilding&nbsp;trong nh&agrave; đều được tổ chức với mục đ&iacute;ch mang tới một kh&ocirc;ng gian giải tr&iacute; tr&agrave;n đầy những ph&uacute;t gi&acirc;y thư gi&atilde;n, x&oacute;a nh&ograve;a mọi khoảng c&aacute;ch trước đ&acirc;y.</p>\r\n\r\n<p>Một số c&aacute;c tr&ograve; chơi&nbsp;Teambuilding&nbsp;trong nh&agrave; phổ biến như:<strong>&nbsp;Đuổi h&igrave;nh bắt chữ, Chuyền Chanh, Đố Vui, Thật Giả, Nối Từ, Xếp Th&aacute;p, Chiếc b&agrave;n ki&ecirc;n cố,..</strong></p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/03/team-building-trong-nha-2.jpg\" style=\"height:450px; width:800px\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/03/team-building-3.jpg\" style=\"height:390px; width:650px\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/03/teambuilding-trong-nha-2.webp\" style=\"height:400px; width:800px\" /></p>\r\n\r\n<p>2.&nbsp;<strong><em>Teambuilding&nbsp;Ngo&agrave;i Trời:</em></strong></p>\r\n\r\n<p>C&aacute;c tr&ograve; chơi được sử dụng để tổ chức&nbsp;Teambuilding&nbsp;ngo&agrave;i trời như<strong><em>&nbsp;Giải Mật Thư, Đua thuyền tr&ecirc;n cạn, b&aacute;nh xe thần tốc,</em></strong>&hellip; v&agrave; thường được tổ chức ở c&aacute;c địa điểm rộng r&atilde;i, tho&aacute;ng m&aacute;t như b&atilde;i biển, c&ocirc;ng vi&ecirc;n, khu du lịch sinh th&aacute;i, resort,&hellip;</p>\r\n\r\n<p>Teambuilding&nbsp;ngo&agrave;i trời thường được kết hợp với c&aacute;c chương tr&igrave;nh du lịch ngắn ng&agrave;y hoặc d&agrave;i ng&agrave;y</p>\r\n\r\n<p>Teambuilding&nbsp;trong nh&agrave; hay ngo&agrave;i trời đều c&oacute; thể kết hợp với Gala Dinner để tạo hiệu ứng tốt nhất, cũng như l&agrave; buổi tổng kết cho c&aacute;c hoạt động của c&ocirc;ng ty.</p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/03/ninh-chu872837578072.jpg\" style=\"height:450px; width:700px\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/03/to-chuc-team-building.jpeg\" style=\"height:480px; width:640px\" /></p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/03/team-building-mui-ne-phan-thiet-1-e1679043121727.jpg\" style=\"height:450px; width:800px\" /></p>\r\n\r\n<h4>Theo h&igrave;nh thức tổ chức:</h4>\r\n\r\n<p><strong><em>1. Tổ Chức Teambuilding Gặp Gỡ/ Hội Thảo (Teambuilding Meeting/ Workshop/ Conferences)</em></strong></p>\r\n\r\n<p>Đ&acirc;y l&agrave; kiểu&nbsp;Teambuilding&nbsp;thường được sử dụng trong c&aacute;c buổi tổ chức hội nghị, hội thảo. Trong g&oacute;i&nbsp;Teambuilding&nbsp;đặc trưng n&agrave;y, c&aacute;c buổi hội nghị, hội thảo thường được kết hợp c&ugrave;ng với việc ph&aacute;t triển đội nh&oacute;m, đẩy mạnh triển khai c&aacute;c chiến lược, nhiệm vụ của từng bộ phận hay từng cấp l&atilde;nh đạo kh&aacute;c nhau. Hầu hết c&aacute;c sự kiện n&agrave;y ch&uacute; trọng đến việc ph&aacute;t triển mối quan hệ; tạo ra bầu kh&ocirc;ng kh&iacute; vui vẻ, cởi mở trong buổi họp.</p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/03/team-building-trong-nha.jpg\" style=\"height:400px; width:600px\" /></p>\r\n\r\n<p><strong><em>2. Tổ Chức&nbsp;Teambuilding&nbsp;Tr&ograve; Chơi (<strong><em>Teambuilding</em></strong>&nbsp;Games)</em></strong><br />\r\nTeambuilding&nbsp;games l&agrave; hoạt động x&acirc;y dựng nh&oacute;m c&ugrave;ng nhau đo&agrave;n kết, n&acirc;ng cao kỹ năng l&agrave;m việc nh&oacute;m th&ocirc;ng qua việc tổ chức tr&ograve; chơi team building tập thể tr&iacute; tuệ.</p>\r\n\r\n<p>3.&nbsp;<strong><em>Tổ Chức&nbsp;<strong><em>Teambuilding</em></strong>&nbsp;Sự Kiện (<strong><em>Teambuilding</em></strong>&nbsp;Events)</em></strong><br />\r\nĐ&acirc;y l&agrave; c&aacute;c hoạt động x&acirc;y dựng đội nh&oacute;m th&ocirc;ng qua việc tổ chức sự kiện. Nhờ đ&oacute; m&agrave; chương tr&igrave;nh được lồng gh&eacute;p nhiều gi&aacute; trị mang &yacute; nghĩa s&acirc;u sắc.</p>\r\n\r\n<p>4.&nbsp;<strong><em>Tổ Chức&nbsp;<strong><em>Teambuilding</em></strong>&nbsp;Luyện Tập, Thực H&agrave;nh (<strong><em>Teambuilding</em></strong>&nbsp;Exercise)</em></strong></p>\r\n\r\n<p>Loại h&igrave;nh&nbsp;Teambuilding&nbsp;điển h&igrave;nh n&agrave;y thường được kết hợp trong c&aacute;c buổi đ&agrave;o tạo kỹ năng cho nh&acirc;n vi&ecirc;n với nhiều b&agrave;i tập thực h&agrave;nh kỹ năng xử l&yacute; t&igrave;nh huống. Th&ocirc;ng qua hoạt động n&agrave;y, c&aacute;c tổ chức muốn ph&aacute;t triển kỹ năng giải quyết vấn đề, x&acirc;y dựng khả năng l&atilde;nh đạo, n&acirc;ng cao kỹ năng giao tiếp cho nh&acirc;n vi&ecirc;n.</p>\r\n\r\n<h4>Ph&acirc;n Loại Theo Đối Tượng Tổ Chức</h4>\r\n\r\n<p>1.<strong><em>Tổ Chức&nbsp;<strong><em>Teambuilding</em></strong>&nbsp;Cho C&ocirc;ng Ty, Doanh Nghiệp</em></strong><br />\r\nĐ&acirc;y l&agrave; c&aacute;c hoạt động&nbsp;Teambuilding&nbsp;được x&acirc;y dựng d&agrave;nh ri&ecirc;ng cho c&aacute;c c&ocirc;ng ty, cơ quan, x&iacute; nghiệp, c&aacute;c c&acirc;u lạc bộ hay c&aacute;c đo&agrave;n thể.</p>\r\n\r\n<p><strong><em>2. Tổ Chức&nbsp;<strong><em>Teambuilding</em></strong>&nbsp;Cho C&aacute;c Tổ Chức</em></strong><br />\r\nHoạt động&nbsp;Teambuilding&nbsp;n&agrave;y được chia th&agrave;nh 2 loại: tổ chức&nbsp;Teambuilding&nbsp;cho ch&iacute;nh phủ v&agrave; phi ch&iacute;nh phủ. Mục đ&iacute;ch ch&iacute;nh của loại n&agrave;y ch&iacute;nh l&agrave; x&acirc;y dựng đội ngũ ng&acirc;n vi&ecirc;n gắn kết v&agrave; c&ugrave;ng nhau hướng đến mục ti&ecirc;u chung của tổ chức</p>\r\n\r\n<h4>C&aacute;c Loại H&igrave;nh Teambuilding Đặc Biệt Kh&aacute;c</h4>\r\n\r\n<p>B&ecirc;n cạnh c&aacute;c loại h&igrave;nh tổ chức&nbsp;Teambuilding&nbsp;phổ biến tr&ecirc;n đ&acirc;y, c&aacute;c tr&ograve; chơi&nbsp;Teambuilding&nbsp;c&ograve;n được s&aacute;ng tạo theo nhiều h&igrave;nh thức kh&aacute;c nhau như:</p>\r\n\r\n<p>1.<strong><em>Gameshow</em></strong><br />\r\nX&acirc;y dựng tinh thần đo&agrave;n kết đồng đội th&ocirc;ng qua c&aacute;c tr&ograve; chơi&nbsp;Teambuilding&nbsp;s&aacute;ng tạo, h&agrave;i hước v&agrave; vui nhộn.</p>\r\n\r\n<p><strong><em>2. Lửa Trại</em></strong><br />\r\nĐ&acirc;y l&agrave; một h&igrave;nh thức mới của hoạt động&nbsp;Teambuilding&nbsp;ngo&agrave;i trời, lửa trại thường được tổ chức tr&ecirc;n c&aacute;c resort, khu du lịch hay b&atilde;i biển&hellip;</p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/03/dot-lua-trai.jpg\" style=\"height:484px; width:768px\" /></p>\r\n\r\n<p><strong><em>3. Amazing Race</em></strong><br />\r\nNgười chơi sẽ được tham gia nhiều thử th&aacute;ch tr&ograve; chơi sở hữu độ kh&oacute; kh&aacute;c nhau tr&ecirc;n c&aacute;c địa h&igrave;nh phức tạp từ s&ocirc;ng hồ, đồi n&uacute;i hay rừng c&acirc;y&hellip;</p>\r\n\r\n<p><strong><em>4. Gala Dinner</em></strong><br />\r\nL&agrave; bữa tiệc chi&ecirc;u đ&atilde;i độc đ&aacute;o kết hợp giữa ẩm thực, &acirc;m thanh, &aacute;nh s&aacute;ng, gala dinner được xem l&agrave; hoạt động kh&ocirc;ng thể thiếu của tổ chức&nbsp;Teambuilding.</p>\r\n\r\n<p>5.&nbsp;<strong><em>Hội Thao</em></strong><br />\r\nHoạt động thể dục, thể thao mang t&iacute;nh thi đấu nhằm n&acirc;ng cao sức khỏe, r&egrave;n luyện thể lực v&agrave; n&acirc;ng cao khả năng l&agrave;m việc nh&oacute;m.</p>\r\n\r\n<h3>MỘT SỐ TR&Ograve; CHƠI THAM KHẢO TRONG&nbsp;TEAMBUILDING</h3>\r\n\r\n<ol>\r\n	<li><strong>Tr&ograve; chơi Gắn Kết &ndash; Mảnh Gh&eacute;p B&iacute; Mật</strong></li>\r\n</ol>\r\n\r\n<ul>\r\n	<li>Mỗi đội sẽ c&ugrave;ng nhau gh&eacute;p c&aacute;c mảnh gh&eacute;p th&agrave;nh một h&igrave;nh khối ho&agrave;n chỉnh với c&aacute;c gợi &yacute; của chương tr&igrave;nh</li>\r\n	<li>Sau khi ho&agrave;n th&agrave;nh xong c&aacute;c đội sẽ nhận được mật thư, giải mật thư v&agrave; sẽ nhận được những y&ecirc;u cầu tiếp theo của chương tr&igrave;nh.</li>\r\n	<li>Đội n&agrave;o thực hiện xong thử th&aacute;ch nhanh nhất sẽ nhận được lợi thế trong tr&ograve; chơi lớn cuối c&ugrave;ng.</li>\r\n</ul>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/03/team-building-phan-thiet.jpg\" style=\"height:465px; width:700px\" /></p>\r\n\r\n<ol start=\"2\">\r\n	<li><strong>Tr&ograve; chơi B&aacute;nh Xe Thần Tốc &ndash; Tăng Khả Năng Hợp T&aacute;c, Tin Tưởng&nbsp;</strong></li>\r\n</ol>\r\n\r\n<ul>\r\n	<li>Mỗi đội chơi sẽ được chuẩn bị 1 b&aacute;nh xe bằng bạt.</li>\r\n	<li>Địa điểm vui chơi thường được tổ chức ở khu vực c&oacute; kh&ocirc;ng gian rộng lớn.</li>\r\n	<li>Mỗi đội chơi sẽ lần lượt qua c&aacute;c thử th&aacute;ch vượt chướng ngại vật của chương tr&igrave;nh, v&agrave; sẽ c&ugrave;ng nhau di chuyển b&aacute;nh xe bằng bạt một c&aacute;ch nhanh nhất về đ&iacute;ch m&agrave; kh&ocirc;ng bị rớt khỏi &ldquo;b&aacute;nh xe&rdquo;</li>\r\n	<li>Tr&ograve; chơi n&agrave;y sẽ tăng khả năng tin tưởng, sự kh&eacute;o l&eacute;o, nhanh nhạy của cả team.&nbsp;</li>\r\n</ul>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/03/team-building-amara-6.jpg\" style=\"height:490px; width:735px\" /></p>\r\n\r\n<p><em>B&aacute;nh Xe Thần Tốc l&agrave; một trong những tr&ograve; chơi team building được nhiều c&ocirc;ng ty lựa chọn</em></p>\r\n\r\n<ol start=\"3\">\r\n	<li><strong>Tr&ograve; chơi Thử Th&aacute;ch Li&ecirc;n Ho&agrave;n &ndash; Tăng T&iacute;nh Nhẫn Nại, sự phối hợp ăn &yacute; của cả đội.</strong></li>\r\n</ol>\r\n\r\n<ul>\r\n	<li>C&aacute;c th&agrave;nh vi&ecirc;n sẽ thực hiện nhiều thử th&aacute;ch li&ecirc;n tiếp từ dễ đến kh&oacute;.</li>\r\n	<li>V&agrave; đ&acirc;y l&agrave; khoảng thời gian cho c&aacute;c đội thể hiện sự đo&agrave;n kết c&ugrave;ng tinh thần kh&ocirc;ng bỏ cuộc.</li>\r\n</ul>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/03/team-building-amara-8.jpg\" style=\"height:477px; width:645px\" /></p>\r\n\r\n<ol start=\"4\">\r\n	<li><strong>Tr&ograve; chơi Chung Sức:</strong></li>\r\n</ol>\r\n\r\n<ul>\r\n	<li>C&aacute;c th&agrave;nh vi&ecirc;n sẽ tham gia một số tr&ograve; chơi như ph&oacute;ng phi ti&ecirc;u, n&eacute;m v&ograve;ng, giải c&acirc;u đố&hellip; để c&oacute; thể đưa được đồng đội của m&igrave;nh về đ&iacute;ch.</li>\r\n</ul>\r\n\r\n<ol start=\"5\">\r\n	<li><strong>&nbsp;Tr&ograve; chơi X&acirc;y Cầu Gỗ</strong></li>\r\n</ol>\r\n\r\n<ul>\r\n	<li>Mỗi nh&oacute;m cần chuẩn bị khoảng 20 &ndash; 30 thanh gỗ lớn nhỏ kh&aacute;c nhau</li>\r\n	<li>C&aacute;c đội cử đại diện l&ecirc;n xem mẫu c&acirc;y cầu cần x&acirc;y.</li>\r\n	<li>Mỗi đội chơi cần b&agrave;n bạc để l&ecirc;n &yacute; tưởng x&acirc;y dựng c&acirc;y cầu theo mẫu theo nguy&ecirc;n tắc về điểm tựa, kh&ocirc;ng sử dụng chất kết d&iacute;nh.</li>\r\n	<li>Đội x&acirc;y nhanh, đẹp v&agrave; kh&ocirc;ng bị đổ sẽ gi&agrave;nh chiến thắng.</li>\r\n</ul>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/03/team-building-amara-4.jpg\" style=\"height:430px; width:750px\" /></p>\r\n\r\n<p><em>X&acirc;y Cầu Gỗ &ndash; một tr&ograve; chơi y&ecirc;u cầu t&iacute;nh nhẫn nại, sự kh&eacute;o l&eacute;o v&agrave; tin tưởng của cả đội</em></p>\r\n\r\n<ol start=\"6\">\r\n	<li><strong>Tr&ograve; chơi Con Rết</strong></li>\r\n</ol>\r\n\r\n<ul>\r\n	<li>Đ&acirc;y l&agrave; tr&ograve; chơi thường được sử dụng đối với&nbsp;Teambuilding ngo&agrave;i trời.&nbsp;</li>\r\n	<li>C&aacute;c th&agrave;nh vi&ecirc;n ngồi th&agrave;nh 1 h&agrave;ng dọc, người đằng sau nối ch&acirc;n v&agrave; &ocirc;m lấy người đằng trước tạo th&agrave;nh con rết. Trong qu&aacute; tr&igrave;nh di chuyển, th&agrave;nh vi&ecirc;n của đội n&agrave;o bị t&aacute;ch ra th&igrave; đội đ&oacute; l&agrave; đội thua cuộc v&agrave; đội về đầu ti&ecirc;n gi&agrave;nh chiến thắng.</li>\r\n</ul>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/03/team-building-amara-7.jpg\" style=\"height:486px; width:730px\" /></p>\r\n\r\n<ol start=\"8\">\r\n	<li><strong>&nbsp;Tr&ograve; chơi Thần Giao C&aacute;ch Cảm</strong></li>\r\n</ol>\r\n\r\n<ul>\r\n	<li>Đ&acirc;y l&agrave; tr&ograve; chơi thường được sử dụng đối với&nbsp;Teambuilding trong nh&agrave;.</li>\r\n	<li>C&aacute;c th&agrave;nh vi&ecirc;n sẽ đứng c&aacute;ch nhau bởi một tấm bảng dựng cao. Th&agrave;nh vi&ecirc;n đầu ti&ecirc;n sẽ nh&igrave;n thấy gợi &yacute; v&agrave; diễn tả lại cho th&agrave;nh vi&ecirc;n thứ 2 bằng h&agrave;nh động tay ch&acirc;n(kh&ocirc;ng sử dụng miệng)</li>\r\n	<li>Tiếp tục cho đến th&agrave;nh vi&ecirc;n cuối c&ugrave;ng. Th&agrave;nh vi&ecirc;n cuối c&ugrave;ng sẽ phải n&oacute;i l&ecirc;n gợi &yacute;.</li>\r\n	<li>Đội n&agrave;o trả lời đ&uacute;ng nhiều nhất th&igrave; sẽ l&agrave; đội thắng.</li>\r\n</ul>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/03/cac-tro-choi-team-building-trong-nha-8000-min-1200x800-1.jpg\" /></p>\r\n\r\n<p>C&aacute;c tr&ograve; chơi trong teambuilding được sử dụng để tăng sự gắn kết, t&iacute;nh đo&agrave;n kết, sự thấu hiểu, khả năng kh&eacute;o l&eacute;o của c&aacute;c th&agrave;nh vi&ecirc;n trong c&ocirc;ng ty.</p>\r\n\r\n<p>Th&ocirc;ng thường hoạt động&nbsp;Teambuilding&nbsp;sẽ được tổ chức v&agrave;o c&aacute;c th&aacute;ng h&egrave; hoặc th&aacute;ng cuối năm để kết hợp c&aacute;c hoạt động tổng kết năm. V&igrave; vậy, với những th&ocirc;ng tin th&uacute; vị vừa rồi phần n&agrave;o gi&uacute;p bạn hiểu r&otilde; hơn về&nbsp;Teambuilding&nbsp;l&agrave; g&igrave; v&agrave; c&aacute;c hoạt động &yacute; nghĩa, thiết thực m&agrave; hoạt động n&agrave;y mang lại cho mỗi doanh nghiệp.</p>\r\n', 'cac-tro-choi-team-building-trong-nha-8000-min-1200x800-1.jpg', '', 'TEAMBUILDING là gì', 'TEAMBUILDING là gìTEAMBUILDING là gì', 1, 1, 90, '', '2023-07-06 04:00:09', NULL, 'admin', NULL, 1, 1, NULL, NULL, 0, NULL, 1, 2, 1, NULL);
INSERT INTO `tbl_dmtin` (`dmtin_id`, `dmtin_title`, `dmtin_title_bar`, `dmtin_slug`, `dmtin_long_description`, `dmtin_image`, `dmtin_short_description`, `dmtin_keywords`, `dmtin_short_description_seo`, `dmtin_quantam`, `dmtin_author`, `dmtin_category`, `dmtin_tag`, `create_date`, `edit_date`, `user_create`, `user_edit`, `publication_status`, `index_google`, `publication_home`, `dmtin_banner`, `dmtin_header`, `dmtin_stt`, `dmtin_nv_ql`, `view`, `dmtin_feature`, `id_dd`) VALUES
(418, 'Dịch vụ hộ chiếu visa', 'Dịch vụ hộ chiếu visa', 'dich-vu-ho-chieu-visa', '<h1>&nbsp;</h1>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/04/20220928_153613.jpg\" style=\"height:521px; width:800px\" /></p>\r\n\r\n<h5>VỀ DU LỊCH AMARA</h5>\r\n\r\n<p>Du Lịch Amara chuy&ecirc;n cung cấp dịch vụ tư vấn, hỗ trợ thủ tục visa xuất cảnh v&agrave; nhập cảnh cho người Việt Nam v&agrave; người nước ngo&agrave;i. Với đội ngũ chuy&ecirc;n vi&ecirc;n tư vấn gi&agrave;u kinh nghiệm, ch&uacute;ng t&ocirc;i lu&ocirc;n cập nhật th&ocirc;ng tin mới nhất v&agrave; hỗ trợ kịp thời cho kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<p>Du Lịch Amara chuy&ecirc;n hỗ trợ l&agrave;m thủ tục xuất cảnh, nhập cảnh Việt Nam đi c&aacute;c nước Nhật Bản, Đ&agrave;i Loan, Mỹ, Canada, H&agrave;n Quốc, Trung Quốc &hellip;, l&agrave; đối t&aacute;c uy t&iacute;n của nhiều đơn vị doanh nghiệp trong v&agrave; ngo&agrave;i nước.</p>\r\n\r\n<h5>DỊCH VỤ VISA</h5>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2022/12/nhat-ban-ho.webp\" style=\"height:400px; width:600px\" /></p>\r\n\r\n<h4><strong>Visa Nhật Bản</strong></h4>\r\n\r\n<p>Hỗ trợ thủ tục xin Visa Nhật Bản du lịch tự t&uacute;c, thăm th&acirc;n, c&ocirc;ng t&aacute;c, gia hạn,&hellip; hướng dẫn điền mẫu đơn v&agrave; c&aacute;c tip khi đi phỏng vấn</p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2022/12/tour-seoul-nami-everland-09.jpg\" style=\"height:300px; width:500px\" /></p>\r\n\r\n<h4><strong>Visa H&agrave;n Quốc</strong></h4>\r\n\r\n<p>Hỗ trợ thủ tục xin Visa H&agrave;n Quốc,&nbsp; du lịch, thăm th&acirc;n, c&ocirc;ng t&aacute;c, gia hạn,&hellip; hướng dẫn điền mẫu đơn v&agrave; c&aacute;c tip khi đi phỏng vấn</p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2022/12/tour-dai-loan-thien-cung.jpg\" style=\"height:400px; width:600px\" /></p>\r\n\r\n<h4><strong>Visa Đ&agrave;i Loan</strong></h4>\r\n\r\n<p>Hỗ trợ thủ tục xin Visa Đ&agrave;i Loan du lịch, thăm th&acirc;n, c&ocirc;ng t&aacute;c, gia hạn,&hellip; hướng dẫn điền mẫu đơn v&agrave; c&aacute;c tip khi đi phỏng vấn</p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/04/visa-my.jpg\" style=\"height:504px; width:800px\" /></p>\r\n\r\n<h4><strong>Visa Mỹ</strong></h4>\r\n\r\n<p>Hỗ trợ thủ tục xin Visa Mỹ du lịch tự t&uacute;c, thăm th&acirc;n, c&ocirc;ng t&aacute;c, gia hạn,&hellip; hướng dẫn điền mẫu đơn v&agrave; c&aacute;c tip khi đi phỏng vấn</p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2022/10/tour-chau-au-e-031020232123.jpg\" style=\"height:500px; width:800px\" /></p>\r\n\r\n<h4><strong>Visa Ch&acirc;u &Acirc;u</strong></h4>\r\n\r\n<p>Hỗ trợ thủ tục xin Visa c&aacute;c nước Ch&acirc;u &Acirc;u,&nbsp; du lịch, thăm th&acirc;n, c&ocirc;ng t&aacute;c, &hellip; hướng dẫn l&agrave;m hồ sơ, đặt lịch hẹn, lấy nhận diện sinh trắc học</p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/04/canada.jpg\" style=\"height:682px; width:1024px\" /></p>\r\n\r\n<h4><strong>Visa Canada</strong></h4>\r\n\r\n<p>Hỗ trợ thủ tục xin Visa Canada du lịch, thăm th&acirc;n, c&ocirc;ng t&aacute;c, &hellip; hướng dẫn điền mẫu l&agrave;m hồ sơ, đặt lịch hẹn, lấy nhận diện sinh trắc học</p>\r\n\r\n<ol>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n</ol>\r\n\r\n<h5>DỊCH VỤ KH&Aacute;C&nbsp;</h5>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2022/12/nhat-ban-ho.webp\" style=\"height:400px; width:600px\" /></p>\r\n\r\n<h4><strong>Combo Du Lịch</strong></h4>\r\n\r\n<p>Combo c&aacute;c dịch vụ du lịch kh&aacute;ch sạn, v&eacute; m&aacute;y bay, v&eacute; v&agrave;o c&aacute;c điểm tham quan,&hellip;</p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2022/12/tour-seoul-nami-everland-09.jpg\" style=\"height:300px; width:500px\" /></p>\r\n\r\n<h4><strong>V&eacute; M&aacute;y Bay</strong></h4>\r\n\r\n<p>Dịch vụ đặt v&eacute; m&aacute;y bay khứ hồi, v&eacute; m&aacute;y bay du lịch gi&aacute; rẻ, du học, lao động, c&ocirc;ng t&aacute;c trong nước v&agrave; quốc tế.</p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2022/12/tour-dai-loan-thien-cung.jpg\" style=\"height:400px; width:600px\" /></p>\r\n\r\n<h4><strong>Teambuilding &ndash; Events</strong></h4>\r\n\r\n<p>Dịch vụ chương tr&igrave;nh Teambuilding, Events, Hội nghị, Hội thảo theo y&ecirc;u cầu của doanh nghiệp, tổ chức</p>\r\n\r\n<ol>\r\n	<li>&nbsp;</li>\r\n</ol>\r\n\r\n<h5>QUY TR&Igrave;NH L&Agrave;M VISA TẠI DU LỊCH AMARA</h5>\r\n\r\n<p><strong>Bước 1: Li&ecirc;n hệ tư vấn</strong></p>\r\n\r\n<p>Bạn c&oacute; thể gọi điện trực tiếp qua&nbsp;<strong>HOTLINE 0889 000 886</strong>&nbsp;hoặc để lại tin nhắn qua Zalo/Messenger, đội ngũ tư vấn của Du Lịch Amara sẽ nhanh ch&oacute;ng li&ecirc;n hệ để hỗ trợ.</p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2022/05/telephone-400x400.png\" style=\"height:400px; width:400px\" /></p>\r\n\r\n<p>&brvbar;<br />\r\n&brvbar;<br />\r\n&brvbar;<br />\r\n&brvbar;<br />\r\n&brvbar;</p>\r\n\r\n<p><strong>Bước 2: Đ&aacute;nh gi&aacute; hồ sơ</strong></p>\r\n\r\n<h4>&nbsp; &nbsp; Visa cho người Việt Nam</h4>\r\n\r\n<ol>\r\n	<li>Đ&aacute;nh gi&aacute; khả năng đậu visa của kh&aacute;ch h&agrave;ng.</li>\r\n	<li>Tư vấn giải ph&aacute;p cải thiện hồ sơ để tăng tỷ lệ đậu visa.</li>\r\n	<li>B&aacute;o ph&iacute; dịch vụ visa trọn g&oacute;i (kh&ocirc;ng ph&aacute;t sinh th&ecirc;m chi ph&iacute;).</li>\r\n</ol>\r\n\r\n<h4>&nbsp; &nbsp; &nbsp;Visa cho người nước ngo&agrave;i</h4>\r\n\r\n<ul>\r\n	<li>Kiểm tra hồ sơ dựa tr&ecirc;n th&ocirc;ng tin kh&aacute;ch h&agrave;ng cung cấp v&agrave; b&aacute;o gi&aacute; dịch vụ trọn g&oacute;i (kh&ocirc;ng ph&aacute;t sinh th&ecirc;m chi ph&iacute;).</li>\r\n</ul>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/04/assept-document-1.png\" style=\"height:256px; width:256px\" /></p>\r\n\r\n<p>&brvbar;<br />\r\n&brvbar;<br />\r\n&brvbar;<br />\r\n&brvbar;<br />\r\n&brvbar;<br />\r\n&brvbar;</p>\r\n\r\n<p><strong>Bước 3: Gửi hồ sơ</strong></p>\r\n\r\n<p>Nh&acirc;n vi&ecirc;n Du Lịch Amara sẽ hướng dẫn bạn chuẩn bị đầy đủ hồ sơ giấy tờ trước khi gửi về văn ph&ograve;ng.</p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/04/layers.png\" style=\"height:256px; width:256px\" /></p>\r\n\r\n<p>&brvbar;<br />\r\n&brvbar;<br />\r\n&brvbar;<br />\r\n&brvbar;<br />\r\n&brvbar;</p>\r\n\r\n<p><strong>Bước 4: Nhận kết quả</strong></p>\r\n\r\n<p>Sau thời gian cam kết, nh&acirc;n vi&ecirc;n Du Lịch Amara sẽ th&ocirc;ng b&aacute;o kết quả v&agrave; giao kết quả đến tận tay qu&yacute; kh&aacute;ch.&nbsp;</p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/04/result.png\" style=\"height:256px; width:256px\" /></p>\r\n\r\n<p>&brvbar;<br />\r\n&brvbar;<br />\r\n&brvbar;<br />\r\n&brvbar;<br />\r\n&brvbar;</p>\r\n\r\n<h5>H&Atilde;Y ĐỂ DU LỊCH AMARA HỖ TRỢ BẠN&nbsp;</h5>\r\n\r\n<h3>Gọi điện trực tiếp</h3>\r\n\r\n<p>Chuy&ecirc;n vi&ecirc;n tư vấn sẽ nhanh ch&oacute;ng giải đ&aacute;p thắc mắc của bạn qua Hotline/Zalo.</p>\r\n\r\n<p><a href=\"https://zalo.me/889000886\" target=\"_self\">0889 000 886</a></p>\r\n\r\n<h3>Messenger</h3>\r\n\r\n<p>Nh&acirc;n vi&ecirc;n trực fanpage sẽ nhanh ch&oacute;ng phản hồi v&agrave; hỗ trợ qu&yacute; kh&aacute;ch.</p>\r\n\r\n<p><a href=\"http://m.me/102245909181260\" target=\"_self\">Li&ecirc;n hệ qua facebook</a></p>\r\n', 'dich-vu-ho-chieu-visa.jpg', '', 'Dịch vụ hộ chiếu visa', 'Dịch vụ hộ chiếu visa', 1, 1, 91, '', '2023-07-06 04:04:04', '2023-07-06 04:04:37', 'admin', 'admin', 1, 1, NULL, NULL, 0, NULL, 1, 5, 1, NULL),
(419, 'Đặt vé máy bay giá rẻ', 'Đặt vé máy bay giá rẻ', 'dat-ve-may-bay-gia-re', '<h4>V&eacute; M&aacute;y Bay</h4>\r\n\r\n<p><strong>Du Lịch Amara</strong>&nbsp;tự tin l&agrave; một trong những c&ocirc;ng ty du lịch cung cấp dịch vụ v&eacute; m&aacute;y bay gi&aacute; rẻ, l&agrave; đối t&aacute;c của nhiều h&atilde;ng bay trong v&agrave; ngo&agrave;i nước. Qu&yacute; kh&aacute;ch c&oacute; thể&nbsp;<strong>đặt v&eacute; m&aacute;y bay trực tiếp</strong>&nbsp;tại C&ocirc;ng Ty, hoặc&nbsp;<strong>đặt v&eacute; m&aacute;y bay trực tuyến th&ocirc;ng qua Hotline v&agrave; website của Du Lịch Amara.&nbsp;</strong></p>\r\n\r\n<p>Du Lịch Amara chuy&ecirc;n cung cấp v&eacute; m&aacute;y bay trọn g&oacute;i, một chiều, v&eacute; m&aacute;y bay gi&aacute; rẻ ph&ugrave; hợp cho nhiều mục đ&iacute;ch, nhu cầu kh&aacute;c nhau như nghỉ dưỡng, c&ocirc;ng t&aacute;c, &hellip;</p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/07/2021_Facebook_icon.svg-280x280.webp\" style=\"height:40px; width:40px\" /><strong>&nbsp; &nbsp; https://www.facebook.com/dulichamara</strong></p>\r\n\r\n<p><img alt=\"\" src=\"https://toursamara.vn/wp-content/uploads/2023/07/mail.jpg\" style=\"height:40px; width:40px\" /><strong>&nbsp; &nbsp; info@</strong></p>\r\n', 'may-bay-vietnam-airlines-600x450.jpg', '', 'Đặt vé máy bay giá rẻ', 'Đặt vé máy bay giá rẻ', 1, 1, 92, '', '2023-07-06 04:07:02', NULL, 'admin', NULL, 1, 1, NULL, NULL, 0, NULL, 1, 1, 1, NULL),
(420, 'COMBO 4N3Đ ĐÀ NẴNG SEKONG HOTEL', 'COMBO 4N3Đ ĐÀ NẴNG SEKONG HOTEL', 'combo-4n3d-da-nang-sekong-hotel', '<p><strong>&ndash; 0</strong><strong>3 đ&ecirc;m nghỉ dưỡng tại&nbsp;</strong><strong>Kh&aacute;ch sạn Sekong&nbsp;</strong>(2 người/ph&ograve;ng)<br />\r\n&ndash; Hạng ph&ograve;ng:&nbsp;<strong>Standard&nbsp;</strong><br />\r\n&ndash; Miễn ph&iacute; ăn s&aacute;ng tại Kh&aacute;ch Sạn<br />\r\n&ndash; Miễn ph&iacute; đưa đ&oacute;n S&acirc;n Bay Kh&aacute;ch Sạn Đ&agrave; Nẵng<br />\r\n&ndash;&nbsp;<strong>Miễn ph&iacute;</strong>&nbsp;nước suối h&agrave;ng ng&agrave;y<br />\r\n&ndash;&nbsp;<strong>Miễn ph&iacute;</strong>&nbsp;sử dụng hồ bơi ngo&agrave;i trời,&nbsp; tại kh&aacute;ch sạn<br />\r\n&ndash;&nbsp;<strong>Miễn ph&iacute;</strong>&nbsp;Tr&agrave;, c&agrave; ph&ecirc;, v&agrave; 02 chai nước suối mỗi ng&agrave;y<br />\r\n&ndash;&nbsp;<strong>V&eacute; C&aacute;p Treo B&agrave; N&agrave; Hills, c&aacute;p treo d&agrave;i nhất thế giới&nbsp;</strong><br />\r\n<strong>&ndash; Thưởng thức buffet trưa tr&ecirc;n đỉnh B&agrave; N&agrave; Hills</strong><br />\r\n<strong>&ndash; Tham quan Đồi Vọng Nguyệt, ch&ugrave;a Linh Ứng, Th&iacute;ch Ca Phật Đ&agrave;i, khu chuồng ngựa cũ của Ph&aacute;p, vườn tịnh t&acirc;m v&agrave; đỉnh nh&agrave; r&ocirc;ng.</strong><br />\r\n<strong>&ndash; Miễn ph&iacute; vui chơi tại Fansipan Park</strong><br />\r\n&ndash;<strong>&nbsp;Bảo hiểm du lịch mức bồi thường 20.000.000 đồng/ trường hợp</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Điều kiện &aacute;p dụng:</strong><br />\r\n&nbsp;&ndash; &Aacute;p dụng khi đặt tối thiểu 02 kh&aacute;ch/combo<br />\r\n&nbsp;&ndash; D&agrave;nh cho kh&aacute;ch Việt Nam hoặc kh&aacute;ch ngoại quốc c&oacute; giấy tr&uacute; h&agrave;nh tại Việt Nam</p>\r\n\r\n<p><strong>Lưu &yacute;:</strong><br />\r\n&ndash; Thời gian nhận ph&ograve;ng:&nbsp;<strong>14:00</strong><br />\r\n&ndash; Thời gian trả ph&ograve;ng:&nbsp;<strong>12:00</strong><br />\r\n&ndash; Qu&yacute; kh&aacute;ch vui l&ograve;ng xuất tr&igrave;nh giấy tờ t&ugrave;y th&acirc;n tr&ugrave;ng t&ecirc;n người đại diện trong&nbsp;<strong>Phiếu X&aacute;c Nhận Đặt Ph&ograve;ng</strong>&nbsp;của C&ocirc;ng Ty để thủ tục nhận ph&ograve;ng được nhanh ch&oacute;ng.<br />\r\n&ndash; Tại thời điểm nhận ph&ograve;ng, Kh&aacute;ch sạn c&oacute; thể sẽ y&ecirc;u cầu&nbsp;<strong>đặt cọc 1,000,000 VNĐ/đ&ecirc;m/ph&ograve;ng</strong>&nbsp;cho c&aacute;c dịch vụ ph&aacute;t sinh trong thời gian lưu tr&uacute; của Qu&yacute; kh&aacute;ch.<br />\r\n&ndash; Chương tr&igrave;nh khuyến m&atilde;i phụ thuộc v&agrave;o t&igrave;nh h&igrave;nh ph&ograve;ng trống của kh&aacute;ch sạn, trường hợp nếu kh&aacute;ch sạn đ&atilde; hết ph&ograve;ng hoặc loại ph&ograve;ng như y&ecirc;u cầu v&agrave;o ng&agrave;y qu&yacute; kh&aacute;ch đặt, nh&acirc;n vi&ecirc;n book v&eacute; sẽ tư vấn đổi sang một loại ph&ograve;ng kh&aacute;c hoặc một kh&aacute;ch sạn kh&aacute;c cho qu&yacute; kh&aacute;ch.<br />\r\n&ndash; Combo đảm bảo khởi h&agrave;nh từ 02 kh&aacute;ch trở l&ecirc;n, trường hợp kh&ocirc;ng đủ kh&aacute;ch khởi h&agrave;nh, ch&uacute;ng t&ocirc;i sẽ tư vấn dời tour sang ng&agrave;y khởi h&agrave;nh gần nhất.<br />\r\n&ndash; Trong trường hợp bất khả kh&aacute;ng, như trước ng&agrave;y tour khởi h&agrave;nh tại điểm đến xảy ra dịch bệnh. Khi c&oacute; th&ocirc;ng b&aacute;o v&agrave; c&ocirc;ng văn của ban chỉ đạo ph&ograve;ng chống dịch của tỉnh hoặc điểm đến, c&ocirc;ng ty sẽ linh hoạt xử l&iacute; theo từng trường hợp cho qu&yacute; kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Gợi &Yacute; Những Điểm Tham Quan Hấp Dẫn Tại Đ&agrave; Nẵng</strong></p>\r\n\r\n<table border=\"2px\" cellspacing=\"2px\">\r\n	<tbody>\r\n		<tr>\r\n			<td><strong>Địa Điểm Tham Quan</strong></td>\r\n			<td><strong>Gi&aacute; V&eacute; Người Lớn</strong></td>\r\n			<td><strong>Gi&aacute; V&eacute; Trẻ Em</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td>Phố Cổ Hội An</td>\r\n			<td>Miễn Ph&iacute;</td>\r\n			<td>Miễn ph&iacute;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ch&ugrave;a Linh Ứng</td>\r\n			<td>Miễn Ph&iacute;</td>\r\n			<td>Miễn ph&iacute;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ngũ H&agrave;nh Sơn</td>\r\n			<td>Miễn Ph&iacute;</td>\r\n			<td>Miễn Ph&iacute;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>VinPearl Nam Hội An</td>\r\n			<td>600,000&nbsp;</td>\r\n			<td>450,000&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;B&agrave; N&agrave; Hills<br />\r\n			(&Aacute;p Dụng tới ng&agrave;y 31/3)</td>\r\n			<td>1,140,000&nbsp;</td>\r\n			<td>860,000&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>B&aacute;n Đảo Sơn Tr&agrave;</td>\r\n			<td>Miễn Ph&iacute;</td>\r\n			<td>Miễn ph&iacute;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Biển Mỹ Kh&ecirc;</td>\r\n			<td>Miễn Ph&iacute;</td>\r\n			<td>Miễn Ph&iacute;</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<blockquote>\r\n<p>Đ&agrave; Nẵng l&agrave; một địa điểm du lịch nổi tiếng, thu h&uacute;t du kh&aacute;ch trong v&agrave; ngo&agrave;i nước. Với bờ biển Mỹ Kh&ecirc; được mệnh danh l&agrave; một trong những bờ biển đẹp nhất Ch&acirc;u &Aacute;, B&agrave; N&agrave; Hills với tuyến C&aacute;p Treo d&agrave;i nhất thế giới, C&acirc;y Cầu V&agrave;ng được vinh danh với lối kiến tr&uacute;c độc đ&aacute;o.</p>\r\n</blockquote>\r\n\r\n<p>&nbsp;</p>\r\n', 'sekong-hotel-da-nang.jpg', '', 'COMBO 4N3Đ ĐÀ NẴNG SEKONG HOTEL', 'COMBO 4N3Đ ĐÀ NẴNG SEKONG HOTEL', NULL, 1, 85, '', '2023-07-06 04:09:15', '2023-07-06 04:11:15', 'admin', 'admin', 1, 1, NULL, NULL, 0, NULL, 1, 1, 1, NULL),
(421, 'COMBO 4N3Đ PHÚ QUỐC RESORT 4*', 'COMBO 4N3Đ PHÚ QUỐC RESORT 4*', 'combo-4n3d-phu-quoc-resort-4', '<p><strong>&ndash; 0</strong><strong>3 đ&ecirc;m nghỉ dưỡng tại&nbsp;</strong><strong>Resort 4*</strong>(2 người/ph&ograve;ng)<br />\r\n&ndash; Hạng ph&ograve;ng:&nbsp;<strong>Standard&nbsp;</strong><br />\r\n&ndash;&nbsp;<strong>Tour Cano 3 Đảo: H&ograve;n M&oacute;ng Tay, H&ograve;n M&acirc;y R&uacute;t, H&ograve;n Gầm Gh&igrave;, C&ocirc;ng Vi&ecirc;n San H&ocirc;.</strong><br />\r\n<strong>&ndash;&nbsp;</strong>Vui chơi B&atilde;i Biển Ri&ecirc;ng của Resort<br />\r\n&ndash; Miễn ph&iacute; ăn s&aacute;ng tại Resort<br />\r\n<strong>&ndash;&nbsp;</strong>Miễn ph&iacute;&nbsp;<strong>xe đưa đ&oacute;n&nbsp;</strong>S&acirc;n Bay Ph&uacute; Quốc<br />\r\n<strong>&ndash;&nbsp;</strong>Miễn ph&iacute; c&aacute;c hoạt động thể thao b&atilde;i biển<br />\r\n&ndash;&nbsp;<strong>Miễn ph&iacute;</strong>&nbsp;nhận ph&ograve;ng sớm trước giờ quy định nếu resort c&oacute; sẵn ph&ograve;ng<br />\r\n&ndash;&nbsp;<strong>Miễn ph&iacute;</strong>&nbsp;nước suối h&agrave;ng ng&agrave;y<br />\r\n&ndash;&nbsp;<strong>Miễn ph&iacute;</strong>&nbsp;sử dụng hồ bơi ngo&agrave;i trời, v&agrave; c&aacute;c c&acirc;u lạc bộ sức khỏe tại kh&aacute;ch sạn<br />\r\n&ndash;&nbsp;<strong>Miễn ph&iacute;</strong>&nbsp;Tr&agrave;, c&agrave; ph&ecirc;, v&agrave; 02 chai nước suối mỗi ng&agrave;y<br />\r\n&ndash;<strong>&nbsp;Bảo hiểm du lịch mức bồi thường 20.000.000 đồng/ trường hợp</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Điều kiện &aacute;p dụng:</strong><br />\r\n&nbsp;&ndash; &Aacute;p dụng khi đặt tối thiểu 02 kh&aacute;ch/combo<br />\r\n&nbsp;&ndash; D&agrave;nh cho kh&aacute;ch Việt Nam hoặc kh&aacute;ch ngoại quốc c&oacute; giấy tr&uacute; h&agrave;nh tại Việt Nam</p>\r\n\r\n<p><strong>Lưu &yacute;:</strong><br />\r\n&ndash; Thời gian nhận ph&ograve;ng:&nbsp;<strong>14:00</strong><br />\r\n&ndash; Thời gian trả ph&ograve;ng:&nbsp;<strong>12:00</strong><br />\r\n&ndash; Qu&yacute; kh&aacute;ch vui l&ograve;ng xuất tr&igrave;nh giấy tờ t&ugrave;y th&acirc;n tr&ugrave;ng t&ecirc;n người đại diện trong&nbsp;<strong>Phiếu X&aacute;c Nhận Đặt Ph&ograve;ng</strong>&nbsp;của C&ocirc;ng Ty để thủ tục nhận ph&ograve;ng được nhanh ch&oacute;ng.<br />\r\n&ndash; Tại thời điểm nhận ph&ograve;ng, Kh&aacute;ch sạn c&oacute; thể sẽ y&ecirc;u cầu&nbsp;<strong>đặt cọc 1,000,000 VNĐ/đ&ecirc;m/ph&ograve;ng</strong>&nbsp;cho c&aacute;c dịch vụ ph&aacute;t sinh trong thời gian lưu tr&uacute; của Qu&yacute; kh&aacute;ch.<br />\r\n&ndash; Chương tr&igrave;nh khuyến m&atilde;i phụ thuộc v&agrave;o t&igrave;nh h&igrave;nh ph&ograve;ng trống của kh&aacute;ch sạn, trường hợp nếu kh&aacute;ch sạn đ&atilde; hết ph&ograve;ng hoặc loại ph&ograve;ng như y&ecirc;u cầu v&agrave;o ng&agrave;y qu&yacute; kh&aacute;ch đặt, nh&acirc;n vi&ecirc;n book v&eacute; sẽ tư vấn đổi sang một loại ph&ograve;ng kh&aacute;c hoặc một kh&aacute;ch sạn kh&aacute;c cho qu&yacute; kh&aacute;ch.<br />\r\n&ndash; Combo đảm bảo khởi h&agrave;nh từ 02 kh&aacute;ch trở l&ecirc;n, trường hợp kh&ocirc;ng đủ kh&aacute;ch khởi h&agrave;nh, ch&uacute;ng t&ocirc;i sẽ tư vấn dời tour sang ng&agrave;y khởi h&agrave;nh gần nhất.<br />\r\n&ndash; Trong trường hợp bất khả kh&aacute;ng, như trước ng&agrave;y tour khởi h&agrave;nh tại điểm đến xảy ra dịch bệnh. Khi c&oacute; th&ocirc;ng b&aacute;o v&agrave; c&ocirc;ng văn của ban chỉ đạo ph&ograve;ng chống dịch của tỉnh hoặc điểm đến, c&ocirc;ng ty sẽ linh hoạt xử l&iacute; theo từng trường hợp cho qu&yacute; kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Gợi &Yacute; Những Điểm Tham Quan Hấp Dẫn Tại Ph&uacute; Quốc</strong></p>\r\n\r\n<table border=\"2px\" cellspacing=\"2px\">\r\n	<tbody>\r\n		<tr>\r\n			<td><strong>Địa Điểm Tham Quan</strong></td>\r\n			<td><strong>Gi&aacute; V&eacute; Người Lớn</strong></td>\r\n			<td><strong>Gi&aacute; V&eacute; Trẻ Em</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td>VinWonder Ph&uacute; Quốc</td>\r\n			<td>880.000VND</td>\r\n			<td>660.000VND/Trẻ Em từ 1m &ndash; 1m4</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Vinpearl Safari Ph&uacute; Quốc</td>\r\n			<td>650.000VND</td>\r\n			<td>490.000VND/Trẻ Em từ 1m &ndash; 1m4</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Combo VinWonder + Safari</td>\r\n			<td>1.200.000VND</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"3\">Grand World Ph&uacute; Quốc &ndash; Th&agrave;nh Phố Kh&ocirc;ng Ngủ</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Show Tinh hoa Việt Nam &ndash; Show diễn triệu đ&ocirc;</td>\r\n			<td>100.000VND</td>\r\n			<td>70.000VND</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Bảo t&agrave;ng gấu Teddy Bear</td>\r\n			<td>200.000 VND</td>\r\n			<td>150.000VND</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Chu du tr&ecirc;n con thuyền Gondola độc đ&aacute;o</td>\r\n			<td colspan=\"2\">200.000VND/ 2 chiều s&ocirc;ng Venice</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<blockquote>\r\n<p>Ph&uacute; Quốc l&agrave; một trong những địa điểm du lịch Hot nhất năm 2022. Ph&uacute; Quốc thu h&uacute;t du kh&aacute;ch trong v&agrave; ngo&agrave;i nước bởi c&aacute;c loại h&igrave;nh du lịch đa dạng, với t&agrave;i nguy&ecirc;n biển, đảo phong ph&uacute;; hệ sinh th&aacute;i rừng, biển đa dạng.</p>\r\n</blockquote>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'combo-phuquoc-amara.jpg', '', 'COMBO 4N3Đ PHÚ QUỐC RESORT 4*', 'COMBO 4N3Đ PHÚ QUỐC RESORT 4*', NULL, 1, 85, '', '2023-07-06 04:10:43', NULL, 'admin', NULL, 1, 1, NULL, NULL, 0, NULL, 1, 1, 1, NULL),
(422, 'COMBO RADISON BLU PHÚ QUỐC 5*', 'COMBO RADISON BLU PHÚ QUỐC 5*', 'combo-radison-blu-phu-quoc-5', '<p><strong>&ndash; 0</strong><strong>2 đ&ecirc;m nghỉ dưỡng tại&nbsp;</strong><strong>Resort 5* Radison Blu</strong>(2 người/ph&ograve;ng)<br />\r\n&ndash; Hạng ph&ograve;ng:&nbsp;<strong>Deluxe</strong><br />\r\n&ndash;&nbsp;<strong>Tour Cano 3 Đảo: H&ograve;n M&oacute;ng Tay, H&ograve;n M&acirc;y R&uacute;t, H&ograve;n Gầm Gh&igrave;, C&ocirc;ng Vi&ecirc;n San H&ocirc;.</strong><br />\r\n<strong>&ndash;&nbsp;</strong>Vui chơi B&atilde;i Biển Ri&ecirc;ng của Resort<br />\r\n&ndash; Miễn ph&iacute; ăn s&aacute;ng tại Resort<br />\r\n<strong>&ndash;&nbsp;</strong>Miễn ph&iacute;&nbsp;<strong>xe đưa đ&oacute;n&nbsp;</strong>S&acirc;n Bay Ph&uacute; Quốc<br />\r\n<strong>&ndash; Miễn ph&iacute; c&aacute;c hoạt động thể thao b&atilde;i biển, ch&egrave;o SUP, ch&egrave;o thuyền Kayak, trượt v&aacute;n nước, xe đạ, yoga, khu vực vui chơi cho trẻ em tại kh&aacute;ch sạn</strong><br />\r\n&ndash;<strong>&nbsp;Miễn ph&iacute; xe đưa đ&oacute;n đến thị trấn Dương Đ&ocirc;ng</strong>, chợ đ&ecirc;m Ph&uacute; Quốc theo lịch tr&igrave;nh sẵn c&oacute; của Resort<br />\r\n&ndash;&nbsp;<strong>Miễn ph&iacute;</strong>&nbsp;nước suối h&agrave;ng ng&agrave;y<br />\r\n&ndash;&nbsp;<strong>Miễn ph&iacute;</strong>&nbsp;sử dụng hồ bơi ngo&agrave;i trời, v&agrave; c&aacute;c c&acirc;u lạc bộ sức khỏe tại kh&aacute;ch sạn<br />\r\n&ndash;&nbsp;<strong>Miễn ph&iacute; xe buggies đến Grand World.</strong><br />\r\n<strong>&ndash;&nbsp;Tặng một ly welcome cocktail từ thứ 5 &ndash; chủ nhật h&agrave;ng tuần khung giờ từ 20:30 &ndash; 3:00 tại Red Light Bar &ndash; Corona Casino<br />\r\n&ndash; Miễn ph&iacute; v&eacute; v&agrave;o cổng Corona Casino xem show ca nhạc định kỳ h&agrave;ng th&aacute;ng với nhiều chủ đề kh&aacute;c nhau c&ugrave;ng thức ăn nhẹ v&agrave; nước uống cho kh&aacute;ch lưu tr&uacute; tại Radisson Blu Resort Ph&uacute; Quốc</strong><br />\r\n&ndash;<strong>&nbsp;Bảo hiểm du lịch mức bồi thường 20.000.000 đồng/ trường hợp</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Điều kiện &aacute;p dụng:</strong><br />\r\n&nbsp;&ndash; &Aacute;p dụng khi đặt tối thiểu 02 kh&aacute;ch/combo<br />\r\n&nbsp;&ndash; D&agrave;nh cho kh&aacute;ch Việt Nam hoặc kh&aacute;ch ngoại quốc c&oacute; giấy tr&uacute; h&agrave;nh tại Việt Nam</p>\r\n\r\n<p><strong>Lưu &yacute;:</strong><br />\r\n&ndash; Thời gian nhận ph&ograve;ng:&nbsp;<strong>14:00</strong><br />\r\n&ndash; Thời gian trả ph&ograve;ng:&nbsp;<strong>12:00</strong><br />\r\n&ndash; Qu&yacute; kh&aacute;ch vui l&ograve;ng xuất tr&igrave;nh giấy tờ t&ugrave;y th&acirc;n tr&ugrave;ng t&ecirc;n người đại diện trong&nbsp;<strong>Phiếu X&aacute;c Nhận Đặt Ph&ograve;ng</strong>&nbsp;của C&ocirc;ng Ty để thủ tục nhận ph&ograve;ng được nhanh ch&oacute;ng.<br />\r\n&ndash; Tại thời điểm nhận ph&ograve;ng, Kh&aacute;ch sạn c&oacute; thể sẽ y&ecirc;u cầu&nbsp;<strong>đặt cọc 1,000,000 VNĐ/đ&ecirc;m/ph&ograve;ng</strong>&nbsp;cho c&aacute;c dịch vụ ph&aacute;t sinh trong thời gian lưu tr&uacute; của Qu&yacute; kh&aacute;ch.<br />\r\n&ndash; Chương tr&igrave;nh khuyến m&atilde;i phụ thuộc v&agrave;o t&igrave;nh h&igrave;nh ph&ograve;ng trống của kh&aacute;ch sạn, trường hợp nếu kh&aacute;ch sạn đ&atilde; hết ph&ograve;ng hoặc loại ph&ograve;ng như y&ecirc;u cầu v&agrave;o ng&agrave;y qu&yacute; kh&aacute;ch đặt, nh&acirc;n vi&ecirc;n book v&eacute; sẽ tư vấn đổi sang một loại ph&ograve;ng kh&aacute;c hoặc một kh&aacute;ch sạn kh&aacute;c cho qu&yacute; kh&aacute;ch.<br />\r\n&ndash; Combo đảm bảo khởi h&agrave;nh từ 02 kh&aacute;ch trở l&ecirc;n, trường hợp kh&ocirc;ng đủ kh&aacute;ch khởi h&agrave;nh, ch&uacute;ng t&ocirc;i sẽ tư vấn dời tour sang ng&agrave;y khởi h&agrave;nh gần nhất.<br />\r\n&ndash; Trong trường hợp bất khả kh&aacute;ng, như trước ng&agrave;y tour khởi h&agrave;nh tại điểm đến xảy ra dịch bệnh. Khi c&oacute; th&ocirc;ng b&aacute;o v&agrave; c&ocirc;ng văn của ban chỉ đạo ph&ograve;ng chống dịch của tỉnh hoặc điểm đến, c&ocirc;ng ty sẽ linh hoạt xử l&iacute; theo từng trường hợp cho qu&yacute; kh&aacute;ch h&agrave;ng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Gợi &Yacute; Những Điểm Tham Quan Hấp Dẫn Tại Ph&uacute; Quốc</strong></p>\r\n\r\n<table border=\"2px\" cellspacing=\"2px\">\r\n	<tbody>\r\n		<tr>\r\n			<td><strong>Địa Điểm Tham Quan</strong></td>\r\n			<td><strong>Gi&aacute; V&eacute; Người Lớn</strong></td>\r\n			<td><strong>Gi&aacute; V&eacute; Trẻ Em</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td>VinWonder Ph&uacute; Quốc</td>\r\n			<td>880.000VND</td>\r\n			<td>660.000VND/Trẻ Em từ 1m &ndash; 1m4</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Vinpearl Safari Ph&uacute; Quốc</td>\r\n			<td>650.000VND</td>\r\n			<td>490.000VND/Trẻ Em từ 1m &ndash; 1m4</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Combo VinWonder + Safari</td>\r\n			<td>1.200.000VND</td>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td colspan=\"3\">Grand World Ph&uacute; Quốc &ndash; Th&agrave;nh Phố Kh&ocirc;ng Ngủ</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Show Tinh hoa Việt Nam &ndash; Show diễn triệu đ&ocirc;</td>\r\n			<td>100.000VND</td>\r\n			<td>70.000VND</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Bảo t&agrave;ng gấu Teddy Bear</td>\r\n			<td>200.000 VND</td>\r\n			<td>150.000VND</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Chu du tr&ecirc;n con thuyền Gondola độc đ&aacute;o</td>\r\n			<td colspan=\"2\">200.000VND/ 2 chiều s&ocirc;ng Venice</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<blockquote>\r\n<p>Ph&uacute; Quốc l&agrave; một trong những địa điểm du lịch Hot nhất năm 2022. Ph&uacute; Quốc thu h&uacute;t du kh&aacute;ch trong v&agrave; ngo&agrave;i nước bởi c&aacute;c loại h&igrave;nh du lịch đa dạng, với t&agrave;i nguy&ecirc;n biển, đảo phong ph&uacute;; hệ sinh th&aacute;i rừng, biển đa dạng.</p>\r\n</blockquote>\r\n\r\n<p>&nbsp;</p>\r\n', 'Radison-blu.jpg', '', 'COMBO RADISON BLU PHÚ QUỐC 5*', 'COMBO RADISON BLU PHÚ QUỐC 5*', NULL, 1, 85, '', '2023-07-06 04:12:30', NULL, 'admin', NULL, 1, 1, NULL, NULL, 0, NULL, 1, 0, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_feel`
--

CREATE TABLE `tbl_feel` (
  `feel_id` int(11) NOT NULL,
  `feel_title` varchar(255) NOT NULL,
  `feel_des` text NOT NULL,
  `feel_image` varchar(255) NOT NULL,
  `feel_link` varchar(255) NOT NULL,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_indexsp`
--

CREATE TABLE `tbl_indexsp` (
  `indexsp_id` int(11) NOT NULL,
  `indexsp_title` varchar(255) NOT NULL,
  `indexsp_des` text NOT NULL,
  `indexsp_image` varchar(255) NOT NULL,
  `indexsp_image1` varchar(255) NOT NULL,
  `indexsp_image2` varchar(255) NOT NULL,
  `indexsp_image3` varchar(255) NOT NULL,
  `indexsp_image4` varchar(255) NOT NULL,
  `indexsp_image5` varchar(255) NOT NULL,
  `indexsp_link` varchar(255) NOT NULL,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_link301`
--

CREATE TABLE `tbl_link301` (
  `link301_id` int(11) NOT NULL,
  `link301_nguon` varchar(255) DEFAULT NULL,
  `link301_dich` text,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_muanhanh`
--

CREATE TABLE `tbl_muanhanh` (
  `muanhanh_id` int(11) NOT NULL,
  `muanhanh_product_id` int(11) NOT NULL,
  `muanhanh_phone` int(11) NOT NULL,
  `muanhanh_time` timestamp NULL DEFAULT NULL,
  `muanhanh_trangthai` int(11) NOT NULL DEFAULT '0',
  `daykh` varchar(255) DEFAULT NULL,
  `daydk` varchar(255) DEFAULT NULL,
  `soluong` int(11) DEFAULT NULL,
  `note` text CHARACTER SET utf8mb4,
  `muanhanh_email` varchar(255) DEFAULT NULL,
  `muanhanh_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_muanhanh`
--

INSERT INTO `tbl_muanhanh` (`muanhanh_id`, `muanhanh_product_id`, `muanhanh_phone`, `muanhanh_time`, `muanhanh_trangthai`, `daykh`, `daydk`, `soluong`, `note`, `muanhanh_email`, `muanhanh_name`) VALUES
(81, 873, 396451567, '2023-06-29 22:24:58', 0, '21/09/2023 - 5,900,000₫', '2023-07-07', 2, 'Tôi muốn đặt tour này', 'vanthanh@gmail.com', 'Nguyễn văn thanh');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_muti_news`
--

CREATE TABLE `tbl_muti_news` (
  `id` int(11) UNSIGNED NOT NULL,
  `image` text NOT NULL,
  `news_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_one_news`
--

CREATE TABLE `tbl_one_news` (
  `one_news_id` int(11) NOT NULL,
  `one_news_title` varchar(255) NOT NULL,
  `one_news_title_bar` varchar(255) NOT NULL,
  `one_news_des` text NOT NULL,
  `one_news_long` longtext NOT NULL,
  `one_news_slug` varchar(255) NOT NULL,
  `one_news_image` varchar(255) NOT NULL,
  `one_news_link` varchar(255) NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `publication_header` tinyint(11) NOT NULL,
  `id_cate_page` int(11) NOT NULL,
  `ma_youtube` varchar(255) DEFAULT NULL,
  `kichthuoc` varchar(255) DEFAULT NULL,
  `keywords` text,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_one_news`
--

INSERT INTO `tbl_one_news` (`one_news_id`, `one_news_title`, `one_news_title_bar`, `one_news_des`, `one_news_long`, `one_news_slug`, `one_news_image`, `one_news_link`, `publication_status`, `publication_header`, `id_cate_page`, `ma_youtube`, `kichthuoc`, `keywords`, `create_date`) VALUES
(39, 'Giới thiệu', 'GIỚI THIỆU ADENZ TRAVEL', 'Công ty là nơi tập hợp những người cùng chí hướng, với sự gắn kết và tương đồng về tính cách và nhận thức, làm việc cùng nhau với mong muốn mang đến những dịch vụ có chất lượng tốt, đáp ứng nhanh nhất, tối đa nhất các yêu cầu của khách hàng. ', '<p> </p>\r\n<p><strong>AdenZ Travel mang lại những giá trị nào?</strong></p>\r\n<ul>\r\n<li>AdenZ Travel là thương hiệu của Công ty cổ phần du lịch ATOZ – một doanh nghiệp trẻ của những người đam mê và gắn bó với du lịch trên 18 năm tại Thành phố Hồ Chí Minh.</li>\r\n<li>Bằng kinh nghiệm tích lũy sau hơn 18 năm làm việc trong môi trường lữ hành chuyên nghiệp quốc tế, va chạm thực tế và thấu hiểu nhu cầu của khách hàng, AdenZ Travel đảm bảo xây dựng nên những sản phẩm, dịch vụ lữ hành đáp ứng tối đa mong muốn, thị hiếu của khách hàng. Tất cả các sản phẩm, dịch vụ được chăm chút kỹ lưỡng nhằm mục đích làm hài lòng những khách hàng sành điệu và khó tính nhất.</li>\r\n<li>Lấy mục tiêu phát triển thương hiệu một cách bền vững, AdenZ Travel đặt vấn đề chất lượng lên hàng đầu và chất lượng chính là một giá trị cốt lõi của công ty. AdenZ Travel nỗ lực để xây dựng một thương hiệu gắn liền với chất lượng vượt trội.</li>\r\n<li>AdenZ Travel không ngừng cải tiến các sản phẩm, dịch vụ để mang đến cho khách hàng những trải nghiệm khó quên qua những chuyến đi thú vị và bổ ích, làm phong phú và sảng khoái tinh thần, giảm đi những căng thẳng mệt mỏi trong công việc và cuộc sống.</li>\r\n</ul>\r\n<h3><strong>Quỹ từ thiện: San yêu thương – Sẻ ngọt bùi AdenZ Travel</strong></h3>\r\n<p>AdenZ Travel là một tổ chức kinh doanh, nhưng công ty san sẻ trách nhiệm với xã hội trong việc chung tay gánh vác các công tác từ thiện nhằm giúp đỡ một phần những cá nhân gặp khó khăn trong cuộc sống như: trẻ em mồ côi, người già, người tàn tật không nơi nương tựa, học sinh nghèo hiếu học, vv. Công ty thành lập quỹ từ thiện “San yêu thương – Sẻ ngọt bùi AdenZ Travel” bằng cách trích một phần lợi nhuận bán hàng theo đơn giá 5,000 đ/sản phẩm/khách hàng (tại thời điểm 2016) và có thể được điều chỉnh tùy theo biến động chỉ số giá tiêu dùng từng thời kỳ.</p>\r\n<h3><strong>Tầm nhìn của AdenZ Travel</strong></h3>\r\n<p>Công ty là nơi tập hợp những người cùng chí hướng, với sự gắn kết và tương đồng về tính cách và nhận thức, làm việc cùng nhau với mong muốn mang đến những dịch vụ có chất lượng tốt, đáp ứng nhanh nhất, tối đa nhất các yêu cầu của khách hàng và phát triển công ty đến năm 2025 trở thành một trong 10 doanh nghiệp lữ hành tốt nhất tại Việt Nam và đến năm 2030 trở thành một trong 20 doanh nghiệp lữ hành tốt nhất Châu Á.</p>\r\n<h3><strong>Mục đích hoạt động</strong></h3>\r\n<ul>\r\n<li>Luôn mang đến cho khách hàng những sản phẩm, dịch vụ tốt, nhằm thỏa mãn tối đa các nhu cầu về lĩnh vực lữ hành, du lịch và thương mại.</li>\r\n<li>Cải tiến cách người tiêu dùng lựa chọn, đặt mua các dịch vụ lữ hành theo hướng ứng dụng công nghệ và tiện ích từ thương mại điện tử.</li>\r\n<li> Phát triển công ty lớn mạnh, mang lại lợi tức cao cho những cổ đông, tạo nên việc làm và thu nhập cho người lao động và sự nhìn nhận của xã hội đối với toàn thể công ty và từng cá nhân cổ đông cũng như nhân viên công ty.</li>\r\n</ul>\r\n<h3><strong>Giá trị cốt lõi của AdenZ Travel</strong></h3>\r\n<p>1. Chất lượng dịch vụ,</p>\r\n<p>2. Sự phong phú của các sản phẩm, </p>\r\n<p>3. Uy tín thương hiệu, </p>\r\n<p>4. Sự trung thực, </p>\r\n<p>5. Tận tụy cống hiến,</p>\r\n<p>6. Đoàn kết nội bộ.</p>\r\n<p class=\"\"><img class=\"b-lazy b-loaded\" src=\"https://adenztravel.vn/image/62b3cf0316c3a00f1f4a3028/big\" alt=\"GIỚI THIỆU\" /></p>\r\n<p class=\"\"><img class=\"b-lazy b-loaded\" src=\"https://adenztravel.vn/image/62b3cf0216c3a00f1f4a301f/big\" alt=\"GIỚI THIỆU\" /></p>\r\n<p><img class=\"b-lazy b-loaded\" src=\"https://adenztravel.vn/image/62b3cf0116c3a00f1f4a3016/big\" alt=\"GIỚI THIỆU\" data-src-small=\"/image/62b3cf0116c3a00f1f4a3016/thumbnail\" /></p>\r\n<p class=\"\"><img class=\"b-lazy b-loaded\" src=\"https://adenztravel.vn/image/62b3cf0016c3a00f1f4a300d/big\" alt=\"GIỚI THIỆU\" /></p>\r\n<p class=\"\"><img class=\"b-lazy b-loaded\" src=\"https://adenztravel.vn/image/62b3ceff16c3a00f1f4a3004/big\" alt=\"GIỚI THIỆU\" /></p>\r\n<p class=\"\"><img class=\"b-lazy b-loaded\" src=\"https://adenztravel.vn/image/62b3cf4716c3a00f1f4a303c/big\" alt=\"GIỚI THIỆU\" /></p>\r\n<p class=\"\"><img class=\"b-lazy b-loaded\" src=\"https://adenztravel.vn/image/62b3cf4416c3a00f1f4a3032/big\" alt=\"GIỚI THIỆU\" /></p>', 'gioi-thieu', '', '', 1, 0, 0, NULL, NULL, 'tour du lịch, atoztravel, công ty du lịch', '2022-06-21 07:57:35'),
(65, 'Infor top mobile', 'Infor top mobile', 'Infor top mobile', '<p style=\"float: left; font-size: 27px; text-align: center; line-height: 30px; margin-top: 10px; color: #e74d31;\"><span style=\"color: #000000;\"><strong>Amara Travel </strong> </span><span class=\"tlcty\" style=\"float: left; width: 100%; font-size: 12pt; color: #555555;\">Là thương hiệu của Công ty cổ phần du lịch Amara</span></p>', 'infor-top-mobile', '', '', 1, 0, 0, NULL, NULL, 'Infor top mobile', '2022-06-21 07:57:35'),
(68, 'Banner QC home 1', 'Banner QC home 1', 'Banner QC home 1', '', 'https://viettourist.com/', 'banner-topp1.jpg', '', 1, 0, 106, NULL, NULL, 'Banner QC home 1', '2023-07-03 06:39:10'),
(69, 'Banner QC home 2', 'Banner QC home 1', 'Banner QC home 1', '', 'https://viettourist.com/', 'banner-top1-ttg-phct.jpg', '', 1, 0, 106, NULL, NULL, 'Banner QC home 1', '2023-07-03 06:39:31'),
(70, 'Banner QC home 3', 'Banner QC home 1', 'Banner QC home 1', '', 'https://viettourist.com/', 'banner-top1-11.jpg', '', 1, 0, 106, NULL, NULL, 'Banner QC home 1', '2023-07-03 06:39:44'),
(71, 'Văn phòng amara', '', '', '<ul class=\"inform-contact\">\r\n<li><strong><u>Viettourist Hà Nội</u></strong></li>\r\n<li>58 Nguyễn Trường Tộ - Ba Đình - Hà Nội</li>\r\n<li><a href=\"mailto:hanoi@viettourist.com\" target=\"_blank\" rel=\"noopener\">hanoi@viettourist.com</a></li>\r\n<li>Liên hệ: 1900 1868 - Nhánh 2 - 0931883688</li>\r\n</ul>', 'van-phong-amara', '', '', 1, 0, 0, NULL, NULL, '', '2023-07-04 07:12:30'),
(72, 'Thanh toán FOOTER', '', '', '<ol>\r\n<li>Thanh toán trực tiếp tại văn phòng công ty và các chi nhánh</li>\r\n<li>Thanh toán tận nơi : HDV sẽ thu tiền tận nơi gần khu vực trung tâm</li>\r\n<li>Thanh toán chuyển khoản qua ngân hàng</li>\r\n<li>Thanh toán qua cổng thanh toán trực tuyến</li>\r\n<li>Chính sách bảo mật và điều khoản sử dụng</li>\r\n</ol>', 'thanh-toan-footer', '', '', 1, 0, 0, NULL, NULL, '', '2023-07-04 07:24:28'),
(73, 'Quảng cáo page home', 'Quảng cáo page home', 'Quảng cáo page home', '', 'quang-cao-page-home', 'quang-cao-singapoer.jpg', '', 1, 0, 0, NULL, NULL, 'Quảng cáo page home', '2023-07-06 07:05:31');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_option`
--

CREATE TABLE `tbl_option` (
  `option_id` int(11) NOT NULL,
  `site_logo` text CHARACTER SET utf8 NOT NULL,
  `site_website` varchar(255) CHARACTER SET utf8 NOT NULL,
  `site_address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `site_keywords` text CHARACTER SET utf8 NOT NULL,
  `company_hotline` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `site_keywords_contact` text CHARACTER SET utf8 NOT NULL,
  `site_favicon` text CHARACTER SET utf8 NOT NULL,
  `site_copyright` varchar(255) CHARACTER SET utf8 NOT NULL,
  `site_contact_num1` varchar(100) CHARACTER SET utf8 NOT NULL,
  `site_contact_num2` text CHARACTER SET utf8 NOT NULL,
  `site_facebook_link` varchar(100) NOT NULL,
  `site_twitter_link` varchar(100) NOT NULL,
  `site_link_googlemap` text CHARACTER SET utf8 NOT NULL,
  `show_link_facebook` tinyint(4) NOT NULL,
  `site_google_plus_link` varchar(100) NOT NULL,
  `show_link_google_plus` tinyint(4) NOT NULL,
  `show_link_twitter` tinyint(4) NOT NULL,
  `metageo` text CHARACTER SET utf8 NOT NULL,
  `contact_title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `contact_subtitle` text CHARACTER SET utf8 NOT NULL,
  `contact_description` text CHARACTER SET utf8 NOT NULL,
  `google_analytics_header` text CHARACTER SET utf8 NOT NULL,
  `google_analytics_body` text CHARACTER SET utf8 NOT NULL,
  `show_youtube` tinyint(4) NOT NULL,
  `site_youtube` varchar(255) CHARACTER SET utf8 NOT NULL,
  `google_analytics_body_bottom` text CHARACTER SET utf8 NOT NULL,
  `company_location` text CHARACTER SET utf8 NOT NULL,
  `company_number` varchar(100) NOT NULL,
  `company_email` varchar(100) NOT NULL,
  `company_facebook` varchar(100) NOT NULL,
  `company_twitter` varchar(100) NOT NULL,
  `navbar` text CHARACTER SET utf8 NOT NULL,
  `text_cuahang` varchar(255) CHARACTER SET utf8 NOT NULL,
  `banner_top` varchar(400) CHARACTER SET utf8 DEFAULT NULL,
  `ma_youtube` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `site_daidien` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `site_sitemap` text CHARACTER SET utf8,
  `chinhanh1` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `chinhanh2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `company_number2` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `khauhieu` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `schema_content` text CHARACTER SET utf8,
  `id_dm_service` int(11) DEFAULT NULL,
  `id_dm_news` int(11) DEFAULT NULL,
  `col_service` int(11) DEFAULT NULL,
  `limit_service` int(11) DEFAULT NULL,
  `string_desc_service` int(11) DEFAULT '0',
  `order_service` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `col_news` int(11) DEFAULT NULL,
  `limit_news` int(11) DEFAULT NULL,
  `string_desc_news` int(11) DEFAULT '0',
  `order_news` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `id_dm_about` int(11) DEFAULT NULL,
  `string_desc_about` int(11) DEFAULT NULL,
  `time` timestamp NULL DEFAULT NULL,
  `smtp_user` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `smtp_pass` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `chuyenkhoang` text CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_option`
--

INSERT INTO `tbl_option` (`option_id`, `site_logo`, `site_website`, `site_address`, `site_keywords`, `company_hotline`, `site_keywords_contact`, `site_favicon`, `site_copyright`, `site_contact_num1`, `site_contact_num2`, `site_facebook_link`, `site_twitter_link`, `site_link_googlemap`, `show_link_facebook`, `site_google_plus_link`, `show_link_google_plus`, `show_link_twitter`, `metageo`, `contact_title`, `contact_subtitle`, `contact_description`, `google_analytics_header`, `google_analytics_body`, `show_youtube`, `site_youtube`, `google_analytics_body_bottom`, `company_location`, `company_number`, `company_email`, `company_facebook`, `company_twitter`, `navbar`, `text_cuahang`, `banner_top`, `ma_youtube`, `site_daidien`, `site_sitemap`, `chinhanh1`, `chinhanh2`, `company_number2`, `khauhieu`, `schema_content`, `id_dm_service`, `id_dm_news`, `col_service`, `limit_service`, `string_desc_service`, `order_service`, `col_news`, `limit_news`, `string_desc_news`, `order_news`, `id_dm_about`, `string_desc_about`, `time`, `smtp_user`, `smtp_pass`, `chuyenkhoang`) VALUES
(1, 'LOGO_PNG_KHONG_NEN.png', 'amaratravel.vn', '18-20 Hoa Trà, Phường 7, Quận Phú Nhuận, Tp. HCM', 'du lịch nước ngoài, tour du lịch nước ngoài, du lịch nước ngoài giá rẻ, công ty du lịch nước ngoài', '0938146118', '', 'favicon.ico', 'Copyright © 2023', 'Liên hệ với chúng tôi', 'Công ty chuyên tổ chức các tour du lịch nước ngoài, chi phí hợp lý. Mỗi chuyến đi quý khách sẽ cảm nhận được sự khác biệt, tận hưởng những khoảnh khắc vui vẻ, hạnh phúc', 'amaratravel', '', '#', 1, '#', 0, 0, '', 'CÔNG TY CỔ PHẦN DU LỊCH ATOZ TRAVEL', 'Là công ty chuyên tổ chức các tour du lịch nước ngoài, chi phí hợp lý. Mỗi chuyến đi quý khách sẽ cảm nhận được sự khác biệt, tận hưởng những khoảnh khắc vui vẻ, hạnh phúc bên người thân và bạn bè. ', '<ul>\r\n<li>93 Quốc Hưng, P.12, Q.4, TP.HCM</li>\r\n<li><a href=\"#\">viettourist@icloud.com</a></li>\r\n<li>viettourist.com</li>\r\n<li>Liên hệ: 19001868 - 0909886688</li>\r\n<li>Mã số thuế: 0311854004</li>\r\n<li><a href=\"#\">GPKD lữ hành quốc tế số 79-400/2015/TCDL-GPLHQT</a></li>\r\n</ul>', '', '', 1, '#', '', '<p><iframe style=\"border: 0;\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.961181962385!2d106.70823127394438!3d10.737475259913259!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f9b25250ebd%3A0xcb35572c17f757a!2zNDQgxJDGsOG7nW5nIHPhu5EgNDEsIFTDom4gUGhvbmcsIFF14bqtbiA3LCBUaMOgbmggcGjhu5EgSOG7kyBDaMOtIE1pbmgsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1688455484595!5m2!1svi!2s\" width=\"100%\" height=\"450\" allowfullscreen=\"allowfullscreen\" loading=\"lazy\"></iframe></p>', '0938146118', 'cskh@amara.vn', '', '', '#1f62e0', '', 'lifesport-giao-hang-tan-nha.jpg', NULL, NULL, '', NULL, NULL, NULL, 'Tổng đài: 028 7300 2227 - Hotline: 0913.179.227', '', NULL, 86, NULL, NULL, NULL, NULL, 3, 6, 0, '0', 39, 400, NULL, 'dangthanh.ctrlmedia@gmail.com', 'eonadnrqphwfkaiz', '<p>Số t&agrave;i khoản nhận tiền: 222101479</p>\r\n\r\n<p>T&ecirc;n t&agrave;i khoản: C&ocirc;ng ty cổ phần du lịch ATOZ</p>\r\n\r\n<p>Ng&acirc;n h&agrave;ng &Aacute; Ch&acirc;u (ACB) &ndash; Chi nh&aacute;nh Tp. HCM</p>\r\n\r\n<p>Qu&yacute; kh&aacute;ch lưu &yacute;, gi&aacute; tour chưa bao gồm thuế VAT</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `order_id` int(11) NOT NULL,
  `cus_id` int(11) DEFAULT NULL,
  `shipping_id` int(11) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `order_total` float DEFAULT NULL,
  `order_sumsale` float DEFAULT NULL,
  `note` text CHARACTER SET utf8 NOT NULL,
  `actions` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'Đơn hàng mới',
  `order_day` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `qty_child` int(11) DEFAULT NULL,
  `qty_baby` int(11) DEFAULT NULL,
  `payment_method` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `datego` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`order_id`, `cus_id`, `shipping_id`, `payment_id`, `city_id`, `order_total`, `order_sumsale`, `note`, `actions`, `order_day`, `qty_child`, `qty_baby`, `payment_method`, `datego`) VALUES
(435, NULL, 505, 0, NULL, 7800000, NULL, '', 'Đơn hàng mới', '2023-07-04 04:20:19', NULL, NULL, NULL, '2023-08-04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order_details`
--

CREATE TABLE `tbl_order_details` (
  `order_details_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id_order` int(11) NOT NULL,
  `product_name` text CHARACTER SET utf8 NOT NULL,
  `tour_date_begin` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `product_price_order` float NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `product_image_order` longtext CHARACTER SET utf8,
  `ngaydat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tour_price_child` int(11) DEFAULT NULL,
  `tour_price_baby` int(11) DEFAULT NULL,
  `tour_time` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `tour_price_people` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order_details`
--

INSERT INTO `tbl_order_details` (`order_details_id`, `order_id`, `product_id_order`, `product_name`, `tour_date_begin`, `product_price_order`, `product_quantity`, `product_image_order`, `ngaydat`, `tour_price_child`, `tour_price_baby`, `tour_time`, `tour_price_people`) VALUES
(472, 435, 873, 'Tour du lịch ĐÀ NẴNG   HỘI AN   BÀ NÀ HILLS 3 NGÀY 2 ĐÊM', NULL, 3900000, 2, '722-danang-3988.jpg', '2023-07-04 04:20:19', NULL, NULL, ' 3 ngày 2 đêm', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_origin`
--

CREATE TABLE `tbl_origin` (
  `origin_id` int(11) NOT NULL,
  `origin_slug` varchar(255) DEFAULT NULL,
  `origin_name` varchar(255) NOT NULL,
  `origin_description` text,
  `origin_image` varchar(255) DEFAULT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `origin_sales` varchar(255) NOT NULL,
  `origin_percent` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_partner`
--

CREATE TABLE `tbl_partner` (
  `partner_id` int(11) NOT NULL,
  `partner_title` varchar(255) NOT NULL,
  `partner_des` text NOT NULL,
  `partner_image` varchar(255) NOT NULL,
  `partner_link` varchar(255) NOT NULL,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE `tbl_payment` (
  `payment_id` int(11) NOT NULL,
  `payment_type` varchar(50) NOT NULL,
  `actions` varchar(255) CHARACTER SET utf8 DEFAULT 'chưa thanh toán'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_title` longtext CHARACTER SET utf8,
  `product_title_bar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_long_description` longtext COLLATE utf8_unicode_ci,
  `product_image` longtext CHARACTER SET utf8,
  `product_image_maps` longtext CHARACTER SET utf8 NOT NULL,
  `product_keywords` text CHARACTER SET utf8,
  `product_slug` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `product_feature` tinyint(4) DEFAULT NULL,
  `product_quantity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_view` int(11) NOT NULL DEFAULT '0',
  `product_hot` int(11) NOT NULL DEFAULT '0',
  `product_short_description` text COLLATE utf8_unicode_ci,
  `product_tag` text COLLATE utf8_unicode_ci NOT NULL,
  `product_tag_root` text CHARACTER SET utf8 NOT NULL,
  `product_category` int(11) NOT NULL,
  `product_price_sale` text COLLATE utf8_unicode_ci,
  `product_price` text COLLATE utf8_unicode_ci,
  `product_price_deal` text CHARACTER SET utf8,
  `published_date_end` timestamp NULL DEFAULT NULL,
  `product_brand` int(11) DEFAULT NULL,
  `product_color` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `product_size` text CHARACTER SET utf8,
  `product_size_price` text CHARACTER SET utf8,
  `product_size_price_sale` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `product_author` int(11) NOT NULL,
  `publication_status` int(11) NOT NULL,
  `star` int(11) DEFAULT NULL,
  `publication_home_product` int(11) NOT NULL,
  `publication_new_product` int(11) NOT NULL,
  `publication_selling_product` int(11) NOT NULL,
  `publication_sale_product` int(11) NOT NULL,
  `product_xuatxu` text CHARACTER SET utf8,
  `tinhtrang` int(11) DEFAULT NULL,
  `ma` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `product_thanhphan` text CHARACTER SET utf8,
  `product_diemden` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `link_video` text CHARACTER SET utf8,
  `priority` int(11) DEFAULT NULL,
  `product_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `product_thoigian` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `product_phuongtien` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `product_dieukien` text CHARACTER SET utf8,
  `product_luuykhac` text CHARACTER SET utf8,
  `diemden_search` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `product_xuatphat` int(11) NOT NULL,
  `xuatphat_search` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `download` text CHARACTER SET utf8
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`product_id`, `product_title`, `product_title_bar`, `product_long_description`, `product_image`, `product_image_maps`, `product_keywords`, `product_slug`, `product_feature`, `product_quantity`, `product_view`, `product_hot`, `product_short_description`, `product_tag`, `product_tag_root`, `product_category`, `product_price_sale`, `product_price`, `product_price_deal`, `published_date_end`, `product_brand`, `product_color`, `product_size`, `product_size_price`, `product_size_price_sale`, `product_author`, `publication_status`, `star`, `publication_home_product`, `publication_new_product`, `publication_selling_product`, `publication_sale_product`, `product_xuatxu`, `tinhtrang`, `ma`, `product_thanhphan`, `product_diemden`, `user_id`, `link_video`, `priority`, `product_datetime`, `product_thoigian`, `product_phuongtien`, `product_dieukien`, `product_luuykhac`, `diemden_search`, `product_xuatphat`, `xuatphat_search`, `download`) VALUES
(870, 'Tour du lịch Hà Giang Đồng Văn - Mèo Vạc - Hoa tam giác mạch giá rẻ 3n2đ trọn gói gồm vé máy bay từ HCM', 'Tour du lịch Hà Giang Đồng Văn - Mèo Vạc - Hoa tam giác mạch giá rẻ 3n2đ trọn gói gồm vé máy bay từ HCM', '<p><u><strong>Gi&aacute;&nbsp;tour d&agrave;nh cho trẻ em:&nbsp;</strong></u></p>\r\n\r\n<ul>\r\n	<li>Trẻ em dưới 2 tuổi: Thu ph&iacute; 500.000đ/TE (Kh&ocirc;ng bao gồm dịch vụ. Phụ&nbsp;huynh tự t&uacute;c c&aacute;c chi ph&iacute; ph&aacute;t sinh nếu c&oacute;).</li>\r\n	<li>Hai người lớn chỉ được k&egrave;m 01 trẻ em, em thứ hai trở l&ecirc;n phải mua &frac12; gi&aacute; v&eacute;..</li>\r\n	<li>Trẻ em từ 2-5tuổi: thanh to&aacute;n 85% gi&aacute; tour. Ngủ gh&eacute;p với gia đ&igrave;nh.</li>\r\n	<li>Hai người lớn chỉ được k&egrave;m 01 trẻ em, em thứ hai t&iacute;nh như người lớn Trẻ em &gt;6 tuổi: 100% gi&aacute; tour.</li>\r\n</ul>\r\n\r\n<p><strong>Điều kiện hủy tour:</strong></p>\r\n\r\n<p><strong>a) Đối với ng&agrave;y thường:</strong></p>\r\n\r\n<ul>\r\n	<li>Hủy v&eacute; trước 15 ng&agrave;y khởi h&agrave;nh, chịu phạt 100% tiền tour</li>\r\n	<li>Hủy v&eacute; trước 25 ng&agrave;y khởi h&agrave;nh, chịu phạt 75% tiền tour</li>\r\n	<li>Hủy v&eacute; trước 30 ng&agrave;y khởi h&agrave;nh, chịu phạt 50% tiền tour</li>\r\n</ul>\r\n\r\n<p><strong>b) Đối với dịp Lễ, Tết:&nbsp;</strong>Hủy v&eacute; chịu phạt 100% tiền tour</p>\r\n\r\n<p><strong>c)&nbsp;</strong>Sau khi hủy tour, du kh&aacute;ch vui l&ograve;ng đến nhận tiền trong v&ograve;ng 10 ng&agrave;y kể từ ng&agrave;y kết th&uacute;c tour Ch&uacute;ng t&ocirc;i chỉ thanh to&aacute;n trong khỏang thời gian n&oacute;i tr&ecirc;n Qu&yacute; kh&aacute;ch c&oacute; thể chuyển đổi ng&agrave;y tour 01 lần nếu hủy tour trước 2 ng&agrave;y khởi h&agrave;nh (kh&ocirc;ng bao gồm chuyển v&eacute; m&aacute;y bay)</p>\r\n\r\n<p><strong>d)</strong>&nbsp;Trường hợp hủy tour do kh&ocirc;ng đủ kh&aacute;ch khởi h&agrave;nh th&igrave; Viettourist chủ động th&ocirc;ng b&aacute;o cho qu&yacute; kh&aacute;ch trước từ 24-48h khởi h&agrave;nh V&agrave; sự cố kh&aacute;ch quan như thi&ecirc;n tai, dịch bệnh hoặc do t&agrave;u thủy, xe lửa, m&aacute;y bay ho&atilde;n/hủy chuyến, Cty sẽ kh&ocirc;ng chịu tr&aacute;ch nhiệm bồi thường th&ecirc;m bất kỳ chi ph&iacute; n&agrave;o kh&aacute;c ngo&agrave;i việc ho&agrave;n trả chi ph&iacute; những dịch vụ chưa được sử dụng của tour đ&oacute;</p>\r\n\r\n<p><u><strong>ĐIỀU KIỆN THAM GIA TOUR:</strong></u></p>\r\n\r\n<p>Nhận kh&aacute;ch theo quy định từ địa phương được đ&aacute;nh gi&aacute; c&oacute; nguy cơ thấp v&agrave; trung b&igrave;nh.</p>\r\n\r\n<p>- Khai b&aacute;o y tế hoặc qu&eacute;t m&atilde; QR theo hướng dẫn.</p>\r\n\r\n<p>- Cam kết v&agrave; chấp h&agrave;nh quy định ph&ograve;ng chống dịch, thực hiện &ldquo;Th&ocirc;ng điệp 5K&rdquo; của bộ y tế.</p>\r\n\r\n<p>- Trong chương tr&igrave;nh tour, tại điểm đến, kh&aacute;ch sạn lưu tr&uacute; &hellip; c&oacute; y&ecirc;u cầu x&eacute;t nghiệm nhanh covid 19, Q&uacute;y kh&aacute;ch tu&acirc;n thủ theo y&ecirc;u cầu của địa phương (Q&uacute;y kh&aacute;ch n&ecirc;n test trước khi đo&agrave;n khởi h&agrave;nh, c&oacute; giấy x&aacute;c nhận &acirc;m t&iacute;nh với sars-cov-2 nhằm giảm bớt thời gian chờ đợi trong h&agrave;nh tr&igrave;nh tham quan khi c&oacute; y&ecirc;u cầu test nhanh)</p>\r\n\r\n<p><strong>Lưu &yacute;:</strong></p>\r\n\r\n<p>- Việc điều chỉnh y&ecirc;u cầu x&eacute;t nghiệm covid v&agrave; mũi ti&ecirc;m c&oacute; thể sẽ thay đổi theo quy định hiện h&agrave;nh của Cơ quan Quản l&yacute; Nh&agrave; nước c&oacute; thẩm quyền.</p>\r\n\r\n<p>- Trường hợp trước ng&agrave;y khởi h&agrave;nh Q&uacute;y kh&aacute;ch ph&aacute;t hiện bị nhiễm covid v&agrave; c&oacute; giấy x&aacute;c nhận dương t&iacute;nh với sars-cov-2 th&igrave; th&ocirc;ng b&aacute;o với c&ocirc;ng ty Viettourist biết để l&agrave;m thủ tục bảo lưu tiền tour trong 365 ng&agrave;y, bảo lưu định danh đ&uacute;ng người đ&atilde; đăng k&yacute; tham gia tour &ndash; kh&ocirc;ng thay thế bởi người kh&aacute;c.</p>\r\n\r\n<p>Trong 365 ng&agrave;y bảo lưu, Q&uacute;y kh&aacute;ch chọn ng&agrave;y đăng k&yacute; tham gia tour do Viettourist tổ chức ph&ugrave; hợp (Kh&ocirc;ng ho&agrave;n lại tiền tour). Tất cả kh&aacute;ch h&agrave;ng tham gia tour nếu kh&ocirc;ng thuộc trường hợp F0 th&igrave; sẽ kh&ocirc;ng được hỗ trợ giải quyết bảo lưu v&eacute; m&aacute;y bay/bảo lưu tour theo qui định của H&atilde;ng h&agrave;ng kh&ocirc;ng.</p>\r\n\r\n<p>- Trong trường hợp trong đo&agrave;n c&oacute; ph&aacute;t sinh ca nhiễm covid 19, kh&aacute;ch h&agrave;ng c&oacute; tr&aacute;ch nhiệm phối hợp với Viettourist v&agrave; ch&iacute;nh quyền địa phương để thực hiện c&aacute;c biện ph&aacute;p c&aacute;ch ly/ điều trị theo quy định hiện h&agrave;nh.</p>\r\n\r\n<p>- C&aacute;c chi ph&iacute; ph&aacute;t sinh trong qu&aacute; tr&igrave;nh c&aacute;ch ly điều trị, c&aacute;c chi ph&iacute; x&eacute;t nghiệm, c&aacute;ch ly y tế, vận chuyển, lưu tr&uacute;, bữa ăn, điều trị, chi ph&iacute; c&aacute; nh&acirc;n kh&aacute;c&hellip; sẽ kh&ocirc;ng nằm trong chi ph&iacute; tour, dịch vụ v&agrave; kh&aacute;ch h&agrave;ng tự chịu tr&aacute;ch nhiệm chi trả</p>\r\n', '722-hagiang-dongvan-lungcu-4tr9881.jpg', '', 'Tour du lịch Hà Giang Đồng Văn - Mèo Vạc - Hoa tam giác mạch giá rẻ 3n2đ trọn gói gồm vé máy bay từ HCM', 'tour-du-lich-ha-giang-dong-van-meo-vac-hoa-tam-giac-mach-gia-re-3n2d-tron-goi-gom-ve-may-bay-tu-hcm', NULL, NULL, 15, 0, '<p>Tour du lịch H&agrave; Giang - kh&aacute;m ph&aacute; c&aacute;c địa danh nổi tiếng với lượt check in cực cao trong những năm vừa qua, thu h&uacute;t giới trẻ&nbsp;với nhiều thắng cảnh được thi&ecirc;n nhi&ecirc;n kỳ tạo: cao nguy&ecirc;n Đồng Văn,&nbsp; M&atilde; P&iacute; L&egrave;ng, tour gi&aacute; rẻ trọn g&oacute;i từ A-Z gồm v&eacute; m&aacute;y bay.&nbsp;Hotline: 0909886688 - 19001868</p>\r\n\r\n<ul>\r\n</ul>\r\n', 'Tour du lịch Hà Giang Đồng Văn - Mèo Vạc - Hoa tam giác mạch giá rẻ 3n2đ trọn gói gồm vé máy bay từ HCM', '', 0, '4,900,000', '5,900,000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, 1, 0, NULL, NULL, NULL, NULL, 120, 0, NULL, NULL, '2023-06-28 02:03:31', ' 3 ngày 2 đêm', 'MÁY BAY - Xe du lịch', NULL, NULL, 'Hà Giang', 21, 'Hồ Chí Minh', 'https://drive.google.com/file/d/1dLJtTAuU7l7nUhtKZB6q39S9v23KTCPG/view?pli=1'),
(871, 'Tour du lịch Miền Bắc liên tuyến Đông Tây Bắc và Đồng bằng bắc bộ đặc sắc', 'Tour du lịch Miền Bắc liên tuyến Đông Tây Bắc và Đồng bằng bắc bộ đặc sắc', '<p><strong>Ch&uacute; &yacute; khi đăng k&yacute; tour:</strong></p>\r\n\r\n<ul>\r\n	<li>Qu&yacute; kh&aacute;ch vui l&ograve;ng cung cấp đầy đủ th&ocirc;ng tin ch&iacute;nh x&aacute;c: họ t&ecirc;n theo cmnd, ng&agrave;y th&aacute;ng năm sinh. Nếu cung cấp sai qu&yacute; kh&aacute;ch sẽ chịu chi ph&iacute; phạt của h&atilde;ng h&agrave;nh kh&ocirc;ng theo luật định.</li>\r\n	<li>Giấy khai sinh (trẻ em dưới 14 tuổi). 14 tuổi trở l&ecirc;n phải c&oacute; CMND hoặc Passport (hộ chiếu).</li>\r\n	<li>Khi đăng k&yacute; tour, qu&yacute; kh&aacute;ch từ 60 tuổi trở l&ecirc;n mang theo CMND v&agrave; b&aacute;o cho nh&acirc;n vi&ecirc;n Viettourist để nhận được ưu đ&atilde;i.</li>\r\n	<li><u><strong>G&iacute;a tour d&agrave;nh cho trẻ em:&nbsp;</strong></u>\r\n	<p>Trẻ em dưới 2 tuổi: Thu ph&iacute; 500.000đ/TE (Kh&ocirc;ng bao gồm dịch vụ. Phụ&nbsp;huynh tự t&uacute;c c&aacute;c chi ph&iacute; ph&aacute;t sinh nếu c&oacute;).<br />\r\n	Hai người lớn chỉ được k&egrave;m 01 trẻ em, em thứ hai trở l&ecirc;n phải mua &frac12; gi&aacute; v&eacute;..<br />\r\n	Trẻ em từ 2-5tuổi: thanh to&aacute;n 85% gi&aacute; tour. Ngủ gh&eacute;p với gia đ&igrave;nh.<br />\r\n	Hai người lớn chỉ được k&egrave;m 01 trẻ em, em thứ hai t&iacute;nh như người lớn Trẻ em &gt;6 tuổi: 100% gi&aacute; tour.</p>\r\n	</li>\r\n</ul>\r\n\r\n<table align=\"left\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>&nbsp;GHI CH&Uacute;:</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<ul>\r\n				<li>Hướng dẫn vi&ecirc;n Viettourist sẽ li&ecirc;n lạc v&agrave; cung cấp số điện thoại cho Qu&yacute; Kh&aacute;ch v&agrave;o buổi chiều trước ng&agrave;y khởi h&agrave;nh 1 ng&agrave;y.</li>\r\n			</ul>\r\n\r\n			<ul>\r\n				<li>Trong những trường hợp bất khả kh&aacute;ng như: khủng bố, bạo động, thi&ecirc;n tai, sự thay đổi lịch tr&igrave;nh của h&agrave;ng kh&ocirc;ng, t&agrave;u hỏa&hellip;&nbsp;<em>Viettourist sẽ chủ động thay đổi lộ tr&igrave;nh đi hoặc hủy tour v&igrave; sự thuận tiện v&agrave; an to&agrave;n của du kh&aacute;ch v&agrave; kh&ocirc;ng chịu tr&aacute;ch nhiệm bồi thường cho những chi ph&iacute; ph&aacute;t sinh</em>.</li>\r\n			</ul>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p><strong>TH&Ocirc;NG TIN CẦN BIẾT KHI ĐI DU LỊCH:</strong></p>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<ul>\r\n				<li>Thời gian v&agrave; địa điểm tập trung:</li>\r\n			</ul>\r\n\r\n			<p><em>Qu&yacute; kh&aacute;ch vui l&ograve;ng tập trung điểm hẹn tại s&acirc;n bay trước giờ&nbsp;</em><em>bay 90 ph&uacute;t</em><em>&nbsp;m&aacute;y. Mọi sư đến trễ của qu&yacute; kh&aacute;ch c&ocirc;ng ty kh&ocirc;ng chịu tr&aacute;ch niệm.</em></p>\r\n\r\n			<ul>\r\n				<li>Giờ tập trung: &hellip;&hellip;. Tại: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;...</li>\r\n				<li>Chuyến bay: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</li>\r\n				<li>Th&ocirc;ng tin li&ecirc;n hệ: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</li>\r\n				<li>Hướng dẫn vi&ecirc;n Viettourist: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</li>\r\n				<li>Điều h&agrave;nh tour: &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\r\n			</ul>\r\n\r\n			<p>b.&nbsp; Chuẩn bị trước chuyến đi:</p>\r\n\r\n			<ul>\r\n				<li>Khi đi tour, Qu&yacute; Kh&aacute;ch nhớ mang theo giấy tờ c&ograve;n gi&aacute; trị để đi m&aacute;y bay như: Chứng minh nh&acirc;n d&acirc;n, Hộ Chiếu, giấy khai sinh c&oacute; c&ocirc;ng chứng.</li>\r\n				<li>Kh&aacute;ch lớn tuổi (từ 70 tuổi trở l&ecirc;n), kh&aacute;ch t&agrave;n tật tham gia tour, phải c&oacute; th&acirc;n nh&acirc;n đi k&egrave;m v&agrave; cam kết đảm bảo đủ sức khỏe khi tham gia tour du lịch.</li>\r\n				<li>Du kh&aacute;ch mang theo h&agrave;nh l&yacute; gọn nhẹ v&agrave; phải tự bảo quản h&agrave;nh l&yacute;, tiền bạc, tư trang trong suốt thời gian đi du lịch.</li>\r\n				<li>Kh&aacute;ch Việt Nam ở c&ugrave;ng ph&ograve;ng với kh&aacute;ch Quốc tế hoặc Việt kiều y&ecirc;u cầu phải c&oacute; giấy h&ocirc;n th&uacute;.</li>\r\n				<li>Lưu &yacute; chung:</li>\r\n				<li>Qu&yacute; kh&aacute;ch đi tour cần đem theo CMND (c&ograve;n hạn 15 năm)/Hộ chiếu(c&ograve;n hạn 10 năm)/ Giấy khai sinh bản ch&iacute;nh (trẻ em). Trẻ em kh&ocirc;ng đi c&ugrave;ng cha mẹ phải c&oacute; giấy cam kết (hoặc giấy chứng thuận) của cha hoặc mẹ c&oacute; x&aacute;c nhận của ch&iacute;nh quyền địa phương cho người dẫn trẻ em đi.</li>\r\n				<li>Gi&aacute; v&eacute;, giờ bay c&oacute; thể thay đổi theo H&atilde;ng H&agrave;ng Kh&ocirc;ng (Vietnam Airlines, Vasco, Air Mekong). Trong trường hợp qu&yacute; kh&aacute;ch cung cấp t&ecirc;n sai vui l&ograve;ng chịu ph&iacute; ho&agrave;n v&eacute; v&agrave; phải mua lại v&eacute; mới theo quy định của H&atilde;ng H&agrave;ng kh&ocirc;ng (nếu chuyến bay c&ograve;n chỗ).</li>\r\n				<li>Qu&yacute; kh&aacute;ch tham khảo kỹ c&aacute;c Điều Kiện B&aacute;n V&eacute; trước khi đăng k&yacute; chuyến du lịch.</li>\r\n				<li>Để giữ g&igrave;n m&ocirc;i trường du lịch được trong sạch, bền vững, Qu&yacute; Kh&aacute;ch vui l&ograve;ng kh&ocirc;ng xả r&aacute;c nơi c&ocirc;ng cộng, leo tr&egrave;o, viết vẽ l&ecirc;n di t&iacute;ch. Lu&ocirc;n t&ocirc;n trọng v&agrave; giữ g&igrave;n bản sắc văn h&oacute;a nơi m&igrave;nh đang đến.</li>\r\n			</ul>\r\n\r\n			<p><strong>ĐIỀU KIỆN HUỶ TOUR:</strong></p>\r\n\r\n			<p><strong>Sau khi đăng k&yacute; tour thanh to&aacute;n 50% tiền cọc<br />\r\n			* Trước 30 ng&agrave;y khởi h&agrave;nh: Chịu phạt 50% tiền tour.<br />\r\n			* Trước 25 ng&agrave;y khởi h&agrave;nh: Chịu phạt 75% tiền tour.<br />\r\n			* Trước 15 ng&agrave;y khởi h&agrave;nh: Chịu phạt 100% tiền tour.<br />\r\n			Sau khi hủy tour, du kh&aacute;ch vui l&ograve;ng đến nhận tiền trong v&ograve;ng 10 ng&agrave;y kể từ ng&agrave;y kết th&uacute;c tour Ch&uacute;ng t&ocirc;i chỉ thanh to&aacute;n trong khỏang thời gian n&oacute;i tr&ecirc;n. Qu&yacute; kh&aacute;ch c&oacute; thể chuyển đổi ng&agrave;y tour 01 lần nếu hủy tour trước 2 ng&agrave;y khởi h&agrave;nh . Trường hợp hủy tour do kh&ocirc;ng đủ kh&aacute;ch khởi h&agrave;nh th&igrave; Viettourist chủ động th&ocirc;ng b&aacute;o cho qu&yacute; kh&aacute;ch trước từ 24 đến 48h khởi h&agrave;nh. V&agrave; sự cố kh&aacute;ch quan như thi&ecirc;n tai, dịch bệnh hoặc do t&agrave;u thủy, xe lửa, m&aacute;y bay ho&atilde;n /hủy chuyến, Cty sẽ kh&ocirc;ng chịu tr&aacute;ch nhiệm bồi thường th&ecirc;m bất kỳ chi ph&iacute; n&agrave;o kh&aacute;c ngo&agrave;i việc ho&agrave;n trả chi ph&iacute; những dịch vụ chưa được sử dụng của tour đ&oacute;</strong></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '722-lientuyen-dongtaybac-12n.jpg', '', 'Tour du lịch Miền Bắc liên tuyến Đông Tây Bắc và Đồng bằng bắc bộ đặc sắc\r\n', 'tour-du-lich-mien-bac-lien-tuyen-dong-tay-bac-va-dong-bang-bac-bo-dac-sac', NULL, NULL, 16, 0, '<p>Tour du lịch li&ecirc;n tuyến ĐẶC SẮC - DUY NHẤT |&nbsp;trải nghiệm&nbsp;bản sắc d&acirc;n tộc - văn h&oacute;a nghệ thuật v&ugrave;ng T&acirc;y Bắc &amp; Đ&ocirc;ng Bắc v&agrave; Đồng Bằng S&ocirc;ng Hồng mang đậm n&eacute;t văn h&oacute;a d&acirc;n gian với c&aacute;c địa danh nổi tiếng khiến du kh&aacute;ch cũng phải ngỡ ng&agrave;ng .. .Tour bao gồm dịch vụ trọn g&oacute;i từ A-&gt;Z, khởi h&agrave;nh tr&ecirc;n to&agrave;n Quốc, lu&ocirc;n c&oacute; tour c&aacute;c ng&agrave;y lễ tết... Hotline : 19001868 - 0909886688 :: viettourist.com</p>\r\n', 'Tour du lịch Miền Bắc liên tuyến Đông Tây Bắc và Đồng bằng bắc bộ đặc sắc\r\n', '', 0, '16,000,000', '19,000,000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 5, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 121, 0, NULL, NULL, '2023-06-28 02:21:10', '12 ngày 11 đêm', ' MÁY BAY - Xe du lịch', '', NULL, 'Sơn La', 21, 'Hồ Chí Minh', 'https://drive.google.com/file/d/1dLJtTAuU7l7nUhtKZB6q39S9v23KTCPG/view?pli=1'),
(872, 'Tour du lịch liên tuyến Đông Bắc 8N7Đ ( bay thăng HCM)', 'Tour du lịch liên tuyến Đông Bắc 8N7Đ ( bay thăng HCM)', '<p><strong>ĐIỀU KIỆN THAM GIA TOUR:</strong></p>\r\n\r\n<p>Q&uacute;y kh&aacute;ch chỉ cần Đ&aacute;p ứng đủ 01 trong 03 điều kiện (ĐK) sau đ&acirc;y:</p>\r\n\r\n<p>- ĐK1: C&oacute; chứng nhận ti&ecirc;m đủ mũi &iacute;t nhất 14 ng&agrave;y ko qu&aacute; 12 th&aacute;ng</p>\r\n\r\n<p>- ĐK2: C&oacute; giấy chứng nhận f0 kh&ocirc;ng qu&aacute; 6 th&aacute;ng</p>\r\n\r\n<p>- ĐK3: C&oacute; x&eacute;t nghiệm &acirc;m t&iacute;nh trong 72h</p>\r\n\r\n<p>- CH&Iacute;NH THỨC: Bỏ quy định kh&aacute;ch h&agrave;ng bay từ HCM bắt buộc x&eacute;t nghiệm &acirc;m t&iacute;nh nếu thoả m&atilde;n ĐK1 hoặc ĐK2. (Kh&aacute;ch h&agrave;ng ở v&ugrave;ng dịch cấp độ 4 hoặc đang c&aacute;ch ly y tế v&ugrave;ng th&igrave; mới bắt buộc cần x&eacute;t nghiệm &acirc;m t&iacute;nh trong 72h.)</p>\r\n\r\n<p><strong>ĐIỀU KIỆN ĐĂNG K&Yacute; V&Agrave; THANH TO&Aacute;N:</strong><br />\r\n- Khi đăng k&yacute; đặt cọc 50% số tiền tour. Nếu tour khuyến m&atilde;i, đăng k&yacute; thanh to&aacute;n 100% số tiền tour. Q&uacute;y kh&aacute;ch c&oacute; thể linh động chọn h&igrave;nh thức thanh to&aacute;n bằng tiền mặt hoặc chuyển khoản hoặc c&agrave; thẻ&hellip; - Thanh to&aacute;n phần c&ograve;n lại trước ng&agrave;y khởi h&agrave;nh từ 7-10 ng&agrave;y (tour ng&agrave;y thường), 15-20 ng&agrave;y (tour lễ tết).</p>\r\n\r\n<p><strong>ĐIỀU KIỆN HUỶ TOUR (KH&Ocirc;NG T&Iacute;NH THỨ 7, CHỦ NHẬT, NG&Agrave;Y LỄ TẾT):</strong></p>\r\n\r\n<p>Nếu hủy tour, Qu&yacute; kh&aacute;ch thanh to&aacute;n c&aacute;c khoản lệ ph&iacute; hủy tour sau : + Trước ng&agrave;y đi 30 Ng&agrave;y: Thanh to&aacute;n 50% gi&aacute; tour. + Trước ng&agrave;y đi 25 Ng&agrave;y: Thanh to&aacute;n 75% gi&aacute; tour. + Trước ng&agrave;y đi 15 Ng&agrave;y : Thanh to&aacute;n 100% gi&aacute; tour.</p>\r\n\r\n<p>- Đối với tour khởi h&agrave;nh v&agrave;o Lễ, Tết: Nếu hủy tour mất ph&iacute; 100% tiền tour. Sau khi hủy tour, du kh&aacute;ch vui l&ograve;ng đến nhận tiền trong v&ograve;ng 10 ng&agrave;y kể từ ng&agrave;y kết th&uacute;c tour. Ch&uacute;ng t&ocirc;i chỉ thanh to&aacute;n trong khỏang thời gian n&oacute;i tr&ecirc;n.</p>\r\n\r\n<p>- Trường hợp sự cố kh&aacute;ch quan như thi&ecirc;n tai, dịch bệnh hoặc do t&agrave;u thủy, xe lửa, m&aacute;y bay ho&atilde;n /hủy chuyến, C&ocirc;ng ty sẽ kh&ocirc;ng chịu tr&aacute;ch nhiệm bồi thường th&ecirc;m bất kỳ chi ph&iacute; n&agrave;o kh&aacute;c ngo&agrave;i việc ho&agrave;n trả chi ph&iacute; những dịch vụ chưa được sử dụng của tour đ&oacute;. Nếu c&oacute; dịch bệnh covid xảy ra tại thời điểm thực hiện tour, C&ocirc;ng ty Viettourist sẽ nghi&ecirc;m t&uacute;c thực hiện theo hướng dẫn của Ch&iacute;nh Phủ/ c&aacute;c cơ quan chức năng li&ecirc;n quan, kh&ocirc;ng đơn phương thực hiện c&aacute;c y&ecirc;u cầu hủy tour/chuyển tour của kh&aacute;ch h&agrave;ng. nếu kh&ocirc;ng c&oacute; văn bản ch&iacute;nh thống về việc tạm dừng thực hiện - dừng bay của h&atilde;ng h&agrave;ng kh&ocirc;ng. C&aacute;c chi ph&iacute; li&ecirc;n quan đến b&ecirc;n thứ 3 (như l&agrave; v&eacute; m&aacute;y bay, xe, kh&aacute;ch sạn ...) Viettourist sẽ &aacute;p dụng ch&iacute;nh s&aacute;ch cụ thể của b&ecirc;n thứ 3 v&agrave; th&ocirc;ng b&aacute;o chi tiết tới kh&aacute;ch h&agrave;ng tại thời điểm c&oacute; sự cố xảy ra.</p>\r\n\r\n<p>Trường hợp hủy tour do kh&ocirc;ng đủ kh&aacute;ch khởi h&agrave;nh th&igrave; Viettourist chủ động th&ocirc;ng b&aacute;o cho qu&yacute; kh&aacute;ch trước từ 24 đến 48h khởi h&agrave;nh.</p>\r\n\r\n<p><strong>ĐIỀU KIỆN LƯU &Yacute; KHI ĐĂNG K&Yacute; TOUR:</strong></p>\r\n\r\n<p>Q&uacute;y kh&aacute;ch cần hiểu r&otilde; chương tr&igrave;nh (T&igrave;m hiểu chi tiết về gi&aacute; tour &amp; dịch vụ) li&ecirc;n hệ nh&acirc;n vi&ecirc;n tư vấn Hotline: 1900.1868 - 0909.88.66.88. Cung cấp email + điện thoại + Zalo cho nh&acirc;n vi&ecirc;n tư vấn. Đảm bảo qu&yacute; kh&aacute;ch đ&atilde; nhận chương tr&igrave;nh, hiểu chương tr&igrave;nh v&agrave; c&aacute;c điều kiện đi c&ugrave;ng trước khi x&aacute;c nhận đặt tour.</p>\r\n\r\n<p>Qu&yacute; kh&aacute;ch đi tour cần đem theo CMND c&ograve;n hạn /Hộ chiếu c&ograve;n hạn/ Thẻ CCCD c&ograve;n hạn/ Giấy khai sinh bản ch&iacute;nh (trẻ em dưới 14 tuổi). Lưu &yacute; trẻ em tr&ecirc;n 14 tuổi bắt buộc phải c&oacute; CMND/Hộ chiếu l&agrave;m thủ tục l&ecirc;n m&aacute;y bay hoặc Giấy x&aacute;c nhận nh&acirc;n th&acirc;n theo mẫu quy định v&agrave; chỉ c&oacute; gi&aacute; trị trong v&ograve;ng 30 ng&agrave;y kể từ ng&agrave;y x&aacute;c nhận. Trẻ em kh&ocirc;ng đi c&ugrave;ng cha mẹ phải c&oacute; giấy cam kết (hoặc giấy chứng thuận) của cha hoặc mẹ c&oacute; x&aacute;c nhận của ch&iacute;nh quyền địa phương cho người dẫn trẻ em đi. C&aacute;c giấy tờ li&ecirc;n quan đến ti&ecirc;m vắc xin/chứng nhận F0 đ&atilde; khỏi bệnh kh&ocirc;ng qu&aacute; 06 th&aacute;ng.</p>\r\n\r\n<p>Kh&aacute;ch quốc tịch nước ngo&agrave;i hoặc l&agrave; Việt kiều: Khi đi tour vui l&ograve;ng mang theo hộ chiếu bản ch&iacute;nh (Passport) đ&iacute;nh k&egrave;m thị thực nhập cảnh (visa) hoặc thẻ xanh k&egrave;m thị thực nhập cảnh (visa d&aacute;n v&agrave;o hộ chiếu hoặc tờ rời hoặc cuốn miễn thị thực, c&aacute;c loại giấy tờ n&agrave;y phải c&oacute; dấu nhập cảnh Việt Nam v&agrave; c&ograve;n gi&aacute; trị sử dụng theo quy định khi đi tour).</p>\r\n\r\n<p>Trong trường hợp qu&yacute; kh&aacute;ch cung cấp t&ecirc;n sai vui l&ograve;ng chịu ph&iacute; ho&agrave;n v&eacute; v&agrave; phải mua lại v&eacute; mới theo quy định của H&atilde;ng H&agrave;ng kh&ocirc;ng (nếu chuyến bay c&ograve;n chỗ) Qu&yacute; kh&aacute;ch tham khảo kỹ c&aacute;c Điều Kiện mua tour du lịch trước khi đăng k&yacute; chuyến du lịch</p>\r\n\r\n<p>Qu&yacute; kh&aacute;ch c&oacute; nhu cầu xuất h&oacute;a đơn VAT, vui l&ograve;ng b&aacute;o trước với nh&acirc;n vi&ecirc;n tư vấn</p>\r\n\r\n<p>Do c&aacute;c chuyến bay phụ thuộc v&agrave;o c&aacute;c h&atilde;ng H&agrave;ng Kh&ocirc;ng n&ecirc;n trong một số trường hợp gi&aacute; v&eacute;, chuyến bay, giờ bay c&oacute; thể thay đổi m&agrave; kh&ocirc;ng được b&aacute;o trước. T&ugrave;y v&agrave;o t&igrave;nh h&igrave;nh thực tế, chương tr&igrave;nh v&agrave; điểm tham quan c&oacute; thể linh động thay đổi thứ tự c&aacute;c điểm ph&ugrave; hợp điều kiện giờ bay v&agrave; thời tiết thực tế. Viettourist kh&ocirc;ng chịu tr&aacute;ch nhiệm bảo đảm c&aacute;c điểm tham quan trong trường hợp: + Xảy ra thi&ecirc;n tai: b&atilde;o lụt, hạn h&aacute;n, động đất&hellip; + Sự cố về an ninh: khủng bố, biểu t&igrave;nh + Sự cố về h&agrave;ng kh&ocirc;ng: trục trặc kỹ thuật, an ninh, dời, hủy, ho&atilde;n chuyến bay. Nếu những trường hợp tr&ecirc;n xảy ra, Viettourist sẽ xem x&eacute;t để ho&agrave;n trả chi ph&iacute; kh&ocirc;ng tham quan cho kh&aacute;ch trong điều kiện c&oacute; thể (sau khi đ&atilde; trừ lại c&aacute;c dịch vụ đ&atilde; thực hiện&hellip;.v&agrave; kh&ocirc;ng chịu tr&aacute;ch nhiệm bồi thường th&ecirc;m bất kỳ chi ph&iacute; n&agrave;o kh&aacute;c).</p>\r\n', '35fe05ff3309f857a11812.jpg', '', 'Tour du lịch liên tuyến Đông Bắc 8N7Đ ( bay thăng HCM)\r\n', 'tour-du-lich-lien-tuyen-dong-bac-8n7d-bay-thang-hcm-', NULL, NULL, 14, 0, '', 'Tour du lịch liên tuyến Đông Bắc 8N7Đ ( bay thăng HCM)\r\n', '', 0, '9,000,000', '10,000,000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 5, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 122, 0, NULL, NULL, '2023-06-28 02:33:45', '8 ngày 7 đêm', 'Bamboo Airways', NULL, NULL, 'Yên Bái', 21, 'Hồ Chí Minh', 'https://drive.google.com/file/d/1dLJtTAuU7l7nUhtKZB6q39S9v23KTCPG/view?pli=1'),
(873, 'Tour du lịch ĐÀ NẴNG – HỘI AN - BÀ NÀ HILLS 3 NGÀY 2 ĐÊM', 'Tour du lịch ĐÀ NẴNG – HỘI AN - BÀ NÀ HILLS 3 NGÀY 2 ĐÊM', '<p><strong>ĐIỀU KIỆN THAM GIA TOUR:</strong></p>\r\n\r\n<p>Nhận kh&aacute;ch theo quy định từ địa phương được đ&aacute;nh gi&aacute; c&oacute; nguy cơ thấp v&agrave; trung b&igrave;nh. Đ&aacute;p ứng đủ 01 trong 03 điều kiện (ĐK) sau đ&acirc;y:</p>\r\n\r\n<p>- ĐK1: C&oacute; chứng nhận ti&ecirc;m đủ mũi &iacute;t nhất 14 ng&agrave;y ko qu&aacute; 12 th&aacute;ng</p>\r\n\r\n<p>- ĐK2: C&oacute; giấy chứng nhận f0 kh&ocirc;ng qu&aacute; 6 th&aacute;ng</p>\r\n\r\n<p>- ĐK3: C&oacute; x&eacute;t nghiệm &acirc;m t&iacute;nh trong 72h</p>\r\n\r\n<p><strong>CH&Iacute;NH THỨC</strong>: Bỏ quy định kh&aacute;ch h&agrave;ng bay từ HCM bắt buộc x&eacute;t nghiệm &acirc;m t&iacute;nh nếu thoả m&atilde;n ĐK1 hoặc ĐK2. Kh&aacute;ch h&agrave;ng ở v&ugrave;ng dịch cấp độ 4 hoặc đang c&aacute;ch ly y tế v&ugrave;ng th&igrave; mới bắt buộc cần x&eacute;t nghiệm &acirc;m t&iacute;nh trong 72h.</p>\r\n\r\n<p>- Khai b&aacute;o y tế hoặc qu&eacute;t m&atilde; QR theo hướng dẫn.</p>\r\n\r\n<p>- Trẻ em, học sinh đi c&ugrave;ng người th&acirc;n chưa ti&ecirc;m vaccine th&igrave; cần c&oacute; kết quả x&eacute;t nghiệm &acirc;m t&iacute;nh covid trước ng&agrave;y khởi h&agrave;nh 01 ng&agrave;y v&agrave; c&oacute; g&iacute;a trị trong 72h.</p>\r\n\r\n<p>- Cam kết v&agrave; chấp h&agrave;nh quy định ph&ograve;ng chống dịch, thực hiện &ldquo;Th&ocirc;ng điệp 5K&rdquo; của bộ y tế.</p>\r\n\r\n<p><strong>Lưu &yacute;:</strong></p>\r\n\r\n<p>- Việc điều chỉnh y&ecirc;u cầu x&eacute;t nghiệm covid v&agrave; mũi ti&ecirc;m c&oacute; thể sẽ thay đổi theo quy định hiện h&agrave;nh của Cơ quan Quản l&yacute; Nh&agrave; nước c&oacute; thẩm quyền. - Trường hợp trước ng&agrave;y khởi h&agrave;nh Q&uacute;y kh&aacute;ch ph&aacute;t hiện bị nhiễm covid v&agrave; c&oacute; giấy x&aacute;c nhận dương t&iacute;nh với sars-cov-2 th&igrave; th&ocirc;ng b&aacute;o với c&ocirc;ng ty Viettourist biết để l&agrave;m thủ tục bảo lưu tiền tour trong 365 ng&agrave;y, bảo lưu định danh đ&uacute;ng người đ&atilde; đăng k&yacute; tham gia tour &ndash; kh&ocirc;ng thay thế bởi người kh&aacute;c. Trong 365 ng&agrave;y bảo lưu, Q&uacute;y kh&aacute;ch chọn ng&agrave;y đăng k&yacute; tham gia tour do Viettourist tổ chức ph&ugrave; hợp (Kh&ocirc;ng ho&agrave;n lại tiền tour).</p>\r\n\r\n<p>- Trong trường hợp trong đo&agrave;n c&oacute; ph&aacute;t sinh ca nhiễm covid 19, kh&aacute;ch h&agrave;ng c&oacute; tr&aacute;ch nhiệm phối hợp với Viettourist v&agrave; ch&iacute;nh quyền địa phương để thực hiện c&aacute;c biện ph&aacute;p c&aacute;ch ly/ điều trị theo quy định hiện h&agrave;nh.</p>\r\n\r\n<p>- C&aacute;c chi ph&iacute; ph&aacute;t sinh trong qu&aacute; tr&igrave;nh c&aacute;ch ly điều trị, c&aacute;c chi ph&iacute; x&eacute;t nghiệm, c&aacute;ch ly y tế, vận chuyển, lưu tr&uacute;, bữa ăn, điều trị, chi ph&iacute; c&aacute; nh&acirc;n kh&aacute;c&hellip; sẽ kh&ocirc;ng nằm trong chi ph&iacute; tour, dịch vụ v&agrave; kh&aacute;ch h&agrave;ng tự chịu tr&aacute;ch nhiệm chi trả.</p>\r\n\r\n<p><u><strong>ĐIỀU KIỆN NHẬN KH&Aacute;CH:&nbsp;</strong></u><br />\r\nNhận kh&aacute;ch theo quy định từ địa phương được đ&aacute;nh gi&aacute; c&oacute; nguy cơ thấp v&agrave; trung b&igrave;nh. Đ&aacute;p ứng đủ điều kiện về đường bay h&agrave;ng kh&ocirc;ng v&agrave; đường bộ.<br />\r\n- C&oacute; kết quả x&eacute;t nghiệm &acirc;m t&iacute;nh với sars-cov-2 trước ng&agrave;y khởi h&agrave;nh 01 ng&agrave;y.<br />\r\n- Đ&atilde; ti&ecirc;m 2 mũi vắc xin ph&ograve;ng covid / hoặc giấy tờ x&aacute;c nhận F0 đ&atilde; khỏi bệnh kh&ocirc;ng qu&aacute; 06 th&aacute;ng.<br />\r\n- Khai b&aacute;o y tế hoặc qu&eacute;t m&atilde; QR theo hướng dẫn.<br />\r\n- Trẻ em, học sinh đi c&ugrave;ng người th&acirc;n chưa ti&ecirc;m vaccine th&igrave; cần c&oacute; kết quả x&eacute;t nghiệm&nbsp;&acirc;m t&iacute;nh covid trước ng&agrave;y khởi h&agrave;nh 01 ng&agrave;y.<br />\r\n- Cam kết v&agrave; chấp h&agrave;nh quy định ph&ograve;ng chống dịch, thực hiện &ldquo;Th&ocirc;ng điệp 5K&rdquo; của bộ y tế về ph&ograve;ng chống dịch covid.</p>\r\n\r\n<p><u><strong>Quy định:</strong></u><br />\r\n- Việc điều chỉnh y&ecirc;u cầu x&eacute;t nghiệm covid v&agrave; mũi ti&ecirc;m c&oacute; thể sẽ thay đổi theo quy định hiện h&agrave;nh của Cơ quan Quản l&yacute; Nh&agrave; nước c&oacute; thẩm quyền.<br />\r\n- Trường hợp trước ng&agrave;y khởi h&agrave;nh Q&uacute;y kh&aacute;ch ph&aacute;t hiện bị nhiễm covid v&agrave; c&oacute; giấy x&aacute;c nhận dương t&iacute;nh với sars-cov-2 th&igrave; th&ocirc;ng b&aacute;o với c&ocirc;ng ty Viettourist biết để l&agrave;m thủ tục bảo lưu tiền tour trong 365 ng&agrave;y, bảo lưu định danh đ&uacute;ng người đ&atilde; đăng k&yacute; tham gia tour &ndash; kh&ocirc;ng thay thế bởi người kh&aacute;c. Trong 365 ng&agrave;y bảo lưu, Q&uacute;y kh&aacute;ch chọn ng&agrave;y đăng k&yacute; tham gia tour do Viettourist tổ chức ph&ugrave; hợp (Kh&ocirc;ng ho&agrave;n lại tiền tour).<br />\r\n- Trong trường hợp trong đo&agrave;n c&oacute; ph&aacute;t sinh ca nhiễm covid 19, kh&aacute;ch h&agrave;ng c&oacute; tr&aacute;ch nhiệm phối hợp với Viettourist v&agrave; ch&iacute;nh quyền địa phương để thực hiện c&aacute;c biện ph&aacute;p c&aacute;ch ly/ điều trị theo quy định hiện h&agrave;nh.<br />\r\n- C&aacute;c chi ph&iacute; ph&aacute;t sinh trong qu&aacute; tr&igrave;nh c&aacute;ch ly điều trị, c&aacute;c chi ph&iacute; x&eacute;t nghiệm, c&aacute;ch ly y tế, vận chuyển, lưu tr&uacute;, bữa ăn, điều trị, chi ph&iacute; c&aacute; nh&acirc;n kh&aacute;c&hellip; sẽ kh&ocirc;ng nằm trong chi ph&iacute; tour, dịch vụ v&agrave; kh&aacute;ch h&agrave;ng tự chịu tr&aacute;ch nhiệm chi trả.</p>\r\n\r\n<p><br />\r\n<strong>ĐIỀU KIỆN ĐĂNG K&Yacute; V&Agrave; THANH TO&Aacute;N:</strong><br />\r\n- Khi đăng k&yacute; đặt cọc 50% số tiền tour. Nếu tour khuyến m&atilde;i, đăng k&yacute; thanh to&aacute;n 100% số tiền tour. Q&uacute;y kh&aacute;ch c&oacute; thể linh động chọn h&igrave;nh thức thanh to&aacute;n bằng tiền mặt hoặc chuyển khoản hoặc c&agrave; thẻ&hellip;<br />\r\n- Thanh to&aacute;n phần c&ograve;n lại trước ng&agrave;y khởi h&agrave;nh từ 7-10 ng&agrave;y (tour ng&agrave;y thường), 15-20 ng&agrave;y (tour lễ tết).&nbsp;<br />\r\n<strong>ĐIỀU KIỆN HUỶ TOUR (KH&Ocirc;NG T&Iacute;NH THỨ 7, CHỦ NHẬT, NG&Agrave;Y LỄ TẾT):</strong><br />\r\n- V&eacute; kh&ocirc;ng ho&agrave;n kh&ocirc;ng hủy.<br />\r\n- Trường hợp sự cố kh&aacute;ch quan như thi&ecirc;n tai, dịch bệnh hoặc do t&agrave;u thủy, xe lửa, m&aacute;y bay ho&atilde;n /hủy chuyến, C&ocirc;ng ty sẽ kh&ocirc;ng chịu tr&aacute;ch nhiệm bồi thường th&ecirc;m bất kỳ chi ph&iacute; n&agrave;o kh&aacute;c ngo&agrave;i việc ho&agrave;n trả chi ph&iacute; những dịch vụ chưa được sử dụng của tour đ&oacute;. Nếu c&oacute; dịch bệnh covid xảy ra tại thời điểm thực hiện tour, C&ocirc;ng ty Viettourist sẽ nghi&ecirc;m t&uacute;c thực hiện theo hướng dẫn của Ch&iacute;nh Phủ/ c&aacute;c cơ quan chức năng li&ecirc;n quan, kh&ocirc;ng đơn phương thực hiện c&aacute;c y&ecirc;u cầu hủy tour/chuyển tour của kh&aacute;ch h&agrave;ng. nếu kh&ocirc;ng c&oacute; văn bản ch&iacute;nh thống về việc tạm dừng thực hiện - dừng bay của h&atilde;ng h&agrave;ng kh&ocirc;ng. C&aacute;c chi ph&iacute; li&ecirc;n quan đến b&ecirc;n thứ 3 (như l&agrave; v&eacute; m&aacute;y bay, xe, kh&aacute;ch sạn ...) Viettourist sẽ &aacute;p dụng ch&iacute;nh s&aacute;ch cụ thể của b&ecirc;n thứ 3 v&agrave; th&ocirc;ng b&aacute;o chi tiết tới kh&aacute;ch h&agrave;ng tại thời điểm c&oacute; sự cố xảy ra.<br />\r\n- Trường hợp hủy tour do kh&ocirc;ng đủ kh&aacute;ch khởi h&agrave;nh th&igrave; Viettourist chủ động th&ocirc;ng b&aacute;o cho qu&yacute; kh&aacute;ch trước từ 24 đến 48h khởi h&agrave;nh.<br />\r\n<br />\r\n<strong>ĐIỀU KIỆN LƯU &Yacute; KHI ĐĂNG K&Yacute; TOUR:</strong><br />\r\n- Q&uacute;y kh&aacute;ch cần hiểu r&otilde; chương tr&igrave;nh (T&igrave;m hiểu chi tiết về gi&aacute; tour &amp; dịch vụ) li&ecirc;n hệ nh&acirc;n vi&ecirc;n tư vấn Hotline: 1900.1868 - 0909.88.66.88. Cung cấp email + điện thoại + Zalo cho nh&acirc;n vi&ecirc;n tư vấn. Đảm bảo qu&yacute; kh&aacute;ch đ&atilde; nhận chương tr&igrave;nh, hiểu chương tr&igrave;nh v&agrave; c&aacute;c điều kiện đi c&ugrave;ng trước khi x&aacute;c nhận đặt tour.&nbsp;<br />\r\n- Qu&yacute; kh&aacute;ch đi tour cần đem theo CMND c&ograve;n hạn /Hộ chiếu c&ograve;n hạn/ Thẻ CCCD c&ograve;n hạn/ Giấy khai sinh bản ch&iacute;nh (trẻ em dưới 14 tuổi). Lưu &yacute; trẻ em tr&ecirc;n 14 tuổi bắt buộc phải c&oacute; CMND/Hộ chiếu l&agrave;m thủ tục l&ecirc;n m&aacute;y bay hoặc Giấy x&aacute;c nhận nh&acirc;n th&acirc;n theo mẫu quy định v&agrave; chỉ c&oacute; gi&aacute; trị trong v&ograve;ng 30 ng&agrave;y kể từ ng&agrave;y x&aacute;c nhận. Trẻ em kh&ocirc;ng đi c&ugrave;ng cha mẹ phải c&oacute; giấy cam kết (hoặc giấy chứng thuận) của cha hoặc mẹ c&oacute; x&aacute;c nhận của ch&iacute;nh quyền địa phương cho người dẫn trẻ em đi. C&aacute;c giấy tờ li&ecirc;n quan đến ti&ecirc;m vắc xin/chứng nhận F0 đ&atilde; khỏi bệnh kh&ocirc;ng qu&aacute; 06 th&aacute;ng.<br />\r\n- Kh&aacute;ch quốc tịch nước ngo&agrave;i hoặc l&agrave; Việt kiều: Khi đi tour vui l&ograve;ng mang theo hộ chiếu bản ch&iacute;nh (Passport) đ&iacute;nh k&egrave;m thị thực nhập cảnh (visa) hoặc thẻ xanh k&egrave;m thị thực nhập cảnh (visa d&aacute;n v&agrave;o hộ chiếu hoặc tờ rời hoặc cuốn miễn thị thực, c&aacute;c loại giấy tờ n&agrave;y phải c&oacute; dấu nhập cảnh Việt Nam v&agrave; c&ograve;n gi&aacute; trị sử dụng theo quy định khi đi tour).&nbsp;<br />\r\n- Trong trường hợp qu&yacute; kh&aacute;ch cung cấp t&ecirc;n sai vui l&ograve;ng chịu ph&iacute; ho&agrave;n v&eacute; v&agrave; phải mua lại v&eacute; mới theo quy định của H&atilde;ng H&agrave;ng kh&ocirc;ng (nếu chuyến bay c&ograve;n chỗ) Qu&yacute; kh&aacute;ch tham khảo kỹ c&aacute;c Điều Kiện mua tour du lịch trước khi đăng k&yacute; chuyến du lịch<br />\r\n- Qu&yacute; kh&aacute;ch c&oacute; nhu cầu xuất h&oacute;a đơn VAT, vui l&ograve;ng b&aacute;o trước với nh&acirc;n vi&ecirc;n tư vấn&nbsp;<br />\r\n- Do c&aacute;c chuyến bay phụ thuộc v&agrave;o c&aacute;c h&atilde;ng H&agrave;ng Kh&ocirc;ng n&ecirc;n trong một số trường hợp gi&aacute; v&eacute;, chuyến bay, giờ bay c&oacute; thể thay đổi m&agrave; kh&ocirc;ng được b&aacute;o trước. T&ugrave;y v&agrave;o t&igrave;nh h&igrave;nh thực tế, chương tr&igrave;nh v&agrave; điểm tham quan c&oacute; thể linh động thay đổi thứ tự c&aacute;c điểm ph&ugrave; hợp điều kiện giờ bay v&agrave; thời tiết thực tế. Viettourist kh&ocirc;ng chịu tr&aacute;ch nhiệm bảo đảm c&aacute;c điểm tham quan trong trường hợp:<br />\r\n+ Xảy ra thi&ecirc;n tai: b&atilde;o lụt, hạn h&aacute;n, động đất&hellip;<br />\r\n+ Sự cố về an ninh: khủng bố, biểu t&igrave;nh<br />\r\n+ Sự cố về h&agrave;ng kh&ocirc;ng: trục trặc kỹ thuật, an ninh, dời, hủy, ho&atilde;n chuyến bay.<br />\r\nNếu những trường hợp tr&ecirc;n xảy ra, Viettourist sẽ xem x&eacute;t để ho&agrave;n trả chi ph&iacute; kh&ocirc;ng tham quan cho kh&aacute;ch trong điều kiện c&oacute; thể (sau khi đ&atilde; trừ lại c&aacute;c dịch vụ đ&atilde; thực hiện&hellip;.v&agrave; kh&ocirc;ng chịu tr&aacute;ch nhiệm bồi thường th&ecirc;m bất kỳ chi ph&iacute; n&agrave;o kh&aacute;c).</p>\r\n', '722-danang-3988.jpg', '', 'Tour du lịch ĐÀ NẴNG – HỘI AN - BÀ NÀ HILLS 3 NGÀY 2 ĐÊM\r\n', 'tour-du-lich-da-nang-hoi-an-ba-na-hills-3-ngay-2-dem', NULL, NULL, 183, 0, '<p>Đ&Agrave; NẴNG &ndash; HỘI AN - B&Agrave; N&Agrave; HILLS 3 NG&Agrave;Y 2 Đ&Ecirc;M</p>\r\n\r\n<p>Tour trọn g&oacute;i đ&atilde; bao gồm v&eacute; m&aacute;y bay - Khởi h&agrave;nh h&agrave;ng tuần</p>\r\n\r\n<p>ALO ĐẶT V&Eacute; : 19001868 - 0909886688&nbsp;</p>\r\n', 'Tour du lịch ĐÀ NẴNG – HỘI AN - BÀ NÀ HILLS 3 NGÀY 2 ĐÊM\r\n', '', 0, '3,900,000', '4,900,000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 5, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 114, 0, NULL, NULL, '2023-06-28 02:49:15', ' 3 ngày 2 đêm', 'MÁY BAY', NULL, NULL, 'ĐÀ NẴNG', 21, 'Hồ Chí Minh', 'https://drive.google.com/file/d/1dLJtTAuU7l7nUhtKZB6q39S9v23KTCPG/view?pli=1'),
(874, 'Tour du lịch Nhật Bản Thủ đô Tokyo - Núi Phú Sỹ - Phố cổ Naritasan 4N3Đ', 'Tour du lịch Nhật Bản Thủ đô Tokyo - Núi Phú Sỹ - Phố cổ Naritasan 4N3Đ', '<p>+ Khi đăng k&yacute; tour vui l&ograve;ng đặt cọc 50% gi&aacute; tour, sẽ m&acirc;́t cọc n&ecirc;́u hủy tour. V&agrave; thanh to&aacute;n ph&acirc;̀n c&ograve;n lại trước 15 ng&agrave;y khởi h&agrave;nh tour. (C&aacute;c ng&agrave;y Lễ, Tết thanh to&aacute;n trước 25 ng&agrave;y )</p>\r\n\r\n<p>+&nbsp;Vui l&ograve;ng ki&ecirc;̉m tra hạn h&ocirc;̣ chi&ecirc;́u c&ograve;n 6 th&aacute;ng t&iacute;nh đ&ecirc;́n ng&agrave;y đi tour v&ecirc;̀. H&ocirc;̣ chi&ecirc;́u đảm bảo c&aacute;c y&ecirc;́u t&ocirc;́ sau:</p>\r\n\r\n<p>H&igrave;nh ảnh kh&ocirc;ng bị hư hỏng, mờ nh&ograve;e, th&ocirc;ng tin đ&acirc;̀y đủ, d&ugrave; c&ograve;n hạn sử dụng nhưng n&ecirc;́u h&igrave;nh ảnh bị mờ nh&ograve;e, v&acirc;̃n kh&ocirc;ng được xu&acirc;́t hay nh&acirc;̣p cảnh ....trường hợp v&agrave;o ng&agrave;y khởi h&agrave;nh, Qu&yacute; kh&aacute;ch kh&ocirc;ng được xu&acirc;́t hoặc nh&acirc;̣p cảnh trong nước v&agrave; ngo&agrave;i nước v&igrave; l&yacute; do c&aacute; nh&acirc;n c&ocirc;ng ty sẽ kh&ocirc;ng chịu tr&aacute;ch nhi&ecirc;̣m v&agrave; kh&ocirc;ng ho&agrave;n ti&ecirc;̀n c&aacute;c khoản chi ph&iacute; li&ecirc;n quan kh&aacute;c.</p>\r\n\r\n<p>+&nbsp;Kh&aacute;ch mang quốc tịch Nước ngo&agrave;i phải c&oacute; Visa (Thị thực) nhập cảnh Việt Nam c&ograve;n hạn sử dụng t&iacute;nh đến ng&agrave;y khởi h&agrave;nh.( NẾU CHƯA C&Oacute; VUI L&Ograve;NG TH&Ocirc;NG B&Aacute;O ĐỂ ĐƯỢC TƯ VẤN )</p>\r\n\r\n<p>+&nbsp;Khi kh&aacute;ch h&agrave;ng đăng k&yacute; tour, vui l&ograve;ng cung c&acirc;́p th&ocirc;ng tin c&aacute; nh&acirc;n tr&ecirc;n h&ocirc;̣ chi&ecirc;́u : Họ &amp; T&ecirc;n ch&iacute;nh x&aacute;c, ng&agrave;y c&acirc;́p, ng&agrave;y h&ecirc;́t hạn h&ocirc;̣ chi&ecirc;́u, s&ocirc;́ đi&ecirc;̣n thoại li&ecirc;n lạc ,địa chỉ li&ecirc;n lạc&hellip;&hellip;.</p>\r\n\r\n<p>+&nbsp;N&ecirc;́u v&agrave;o ng&agrave;y khởi h&agrave;nh v&igrave; l&yacute; do kh&ocirc;ng đủ s&ocirc;́ lượng kh&aacute;ch, hay b&acirc;́t cứ l&yacute; do kh&aacute;ch quan n&agrave;o kh&aacute;c&hellip; Tour kh&ocirc;ng khởi h&agrave;nh đ&uacute;ng ng&agrave;y, C&ocirc;ng ty sẽ th&ocirc;ng b&aacute;o cho Qu&yacute; Kh&aacute;ch ng&agrave;y mở tour v&agrave;o đợt k&ecirc;́ ti&ecirc;́p, hoặc ho&agrave;n ti&ecirc;̀n lại cho qu&yacute; kh&aacute;ch v&agrave; C&ocirc;ng ty sẽ kh&ocirc;ng chi trả th&ecirc;m b&acirc;́t kỳ chi ph&iacute; ph&aacute;t sinh kh&aacute;c.</p>\r\n\r\n<p>+&nbsp;Nếu Qu&yacute; kh&aacute;ch l&agrave; Việt Kiều hoặc kh&aacute;ch Nước Ngo&agrave;i v&agrave; visa VN ghi trong hộ chiếu l&agrave; loại visa nhập cảnh 1 lần, sẽ phụ thu th&ecirc;m ph&iacute; l&agrave;m visa t&aacute;i nhập VN l&agrave; 1.480.000vnđ/kh&aacute;ch (nộp hộ chiếu gốc + 1 h&igrave;nh 4x6 nền trắng). Ri&ecirc;ng Visa t&aacute;i nhập VN l&agrave;m tại cửa khẩu l&agrave; 650.000vnđ/kh&aacute;ch (nộp hộ chiếu gốc hoặc photo nộp k&egrave;m 1 h&igrave;nh 4x6 nền trắng).</p>\r\n\r\n<p>Viettourist sẽ kh&ocirc;ng chịu tr&aacute;ch nhiệm nếu Qu&yacute; kh&aacute;ch bị từ chối xuất cảnh của Cơ quan QLXNC Việt Nam v&agrave; nhập cảnh của Cơ quan QLXNC nước sở tại với bất cứ l&yacute; do n&agrave;o.</p>\r\n\r\n<p>+&nbsp;Khi đăng k&yacute; tour du lịch, Qu&yacute; kh&aacute;ch vui l&ograve;ng đọc kỹ chương tr&igrave;nh, gi&aacute; tour, c&aacute;c khoản bao gồm cũng như kh&ocirc;ng bao gồm trong chương tr&igrave;nh, c&aacute;c điều kiện hủy tour tr&ecirc;n bi&ecirc;n nhận đ&oacute;ng tiền. Trong trường hợp Qu&yacute; kh&aacute;ch kh&ocirc;ng trực tiếp đến đăng k&yacute; tour m&agrave; do người kh&aacute;c đến đăng k&yacute; th&igrave; Qu&yacute; kh&aacute;ch vui l&ograve;ng t&igrave;m hiểu kỹ chương tr&igrave;nh từ người đăng k&yacute; cho m&igrave;nh.</p>\r\n\r\n<p>+&nbsp;Kh&ocirc;ng sử dụng thẻ xanh, Nếu l&agrave; Sổ du lịch (cần l&agrave;m visa nước cần nhập cảnh) Cần th&ocirc;ng b&aacute;o cho nh&acirc;n vi&ecirc;n nhận tour nếu Qu&yacute; kh&aacute;ch sử dụng c&aacute;c hồ sơ kh&aacute;c nhập kh&ocirc;ng phải hộ chiếu.</p>\r\n\r\n<p>+&nbsp;Qu&yacute; kh&aacute;ch mang 2 Quốc tịch hoặc Travel document (chưa nhập quốc tịch) vui l&ograve;ng th&ocirc;ng b&aacute;o với nh&acirc;n vi&ecirc;n b&aacute;n tour ngay thời điểm đăng k&yacute; tour v&agrave; nộp bản gốc k&egrave;m c&aacute;c giấy tờ c&oacute; li&ecirc;n quan (nếu c&oacute;).</p>\r\n\r\n<p>+&nbsp;Qu&yacute; kh&aacute;ch chỉ mang thẻ xanh (thẻ tạm tr&uacute; tại nước ngo&agrave;i) v&agrave; kh&ocirc;ng c&ograve;n hộ chiếu VN c&ograve;n hiệu lực th&igrave; kh&ocirc;ng đăng k&yacute; du lịch sang nước thứ ba được.</p>\r\n\r\n<p>+&nbsp;Qu&yacute; kh&aacute;ch đăng k&yacute; tour vui l&ograve;ng theo đo&agrave;n về đ&uacute;ng ng&agrave;y kết th&uacute;c tour v&agrave; kh&ocirc;ng t&aacute;ch đo&agrave;n.</p>\r\n\r\n<p>+&nbsp;Nếu kh&aacute;ch l&agrave; Việt Kiều hoặc nước ngo&agrave;i c&oacute; visa rời phải mang theo l&uacute;c đi tour.</p>\r\n\r\n<p>+&nbsp;Nếu y&ecirc;u cầu ở ph&ograve;ng đơn, Qu&yacute; kh&aacute;ch vui l&ograve;ng thanh to&aacute;n tiền phụ thu.</p>\r\n\r\n<p>+&nbsp;Trẻ em dưới 24 tuổi phải c&oacute; bố mẹ đi c&ugrave;ng hoặc người được uỷ quyền phải c&oacute; giấy uỷ quyền từ bố mẹ.</p>\r\n\r\n<p>+&nbsp;V&igrave; mục đ&iacute;ch &amp; tr&aacute;ch nhiệm an to&agrave;n, c&ocirc;ng ty chỉ tổ chức tour du lịch tham quan thuần t&uacute;y. Qu&yacute; kh&aacute;ch kh&ocirc;ng được tự &yacute; t&aacute;ch v&agrave; bỏ đo&agrave;n trong thời gian tham gia chương tr&igrave;nh tour của c&ocirc;ng ty.</p>\r\n', '722-nhatban-tokyo-nuiphusy-narita-16tr.jpg', '', 'Tour du lịch Nhật Bản Thủ đô Tokyo - Núi Phú Sỹ - Phố cổ Naritasan 4N3Đ', 'tour-du-lich-nhat-ban-thu-do-tokyo-nui-phu-sy-pho-co-naritasan-4n3d', NULL, NULL, 97, 0, '<p>Tour du lịch Nhật Bản Thủ đ&ocirc; Tokyo - N&uacute;i Ph&uacute; Sỹ - Phố cổ Naritasan 4 Ng&agrave;y 3 Đ&ecirc;m khởi h&agrave;nh h&agrave;ng tuần từ HCM &amp; H&Agrave; NỘI chất lượng kh&aacute;ch sạn 4SAO buffet chuẩn Nhật Bản trọn g&oacute;i gồm v&eacute; m&aacute;y bay &amp; từ A-Z | Cam kết gi&aacute; rẻ - li&ecirc;n hệ 19001868 - hotline: 0909886688</p>\r\n', 'Tour du lịch Nhật Bản Thủ đô Tokyo - Núi Phú Sỹ - Phố cổ Naritasan 4N3Đ', '', 0, '16,000,000', '19,000,000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 5, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 71, 0, NULL, NULL, '2023-06-28 03:03:24', '4 ngày 3 đêm', 'VietJet', '<p><span style=\"color:#ff0000\"><strong>&Aacute;p dụng đồng gi&aacute; kh&aacute;ch thứ 2,3,4 đi c&ugrave;ng</strong></span></p>\r\n', NULL, 'NHẬT BẢN', 21, 'Hồ Chí Minh', 'https://drive.google.com/file/d/1dLJtTAuU7l7nUhtKZB6q39S9v23KTCPG/view?pli=1'),
(875, 'Du lịch Nhật Bản Cung Đường Vàng Osaka - Kyoto - Núi Phú Sỹ - Tokyo - Naritasan | 5N4Đ', 'Du lịch Nhật Bản Cung Đường Vàng Osaka - Kyoto - Núi Phú Sỹ - Tokyo - Naritasan | 5N4Đ', '<p>+ Khi đăng k&yacute; tour vui l&ograve;ng đặt cọc 50% gi&aacute; tour, sẽ m&acirc;́t cọc n&ecirc;́u hủy tour. V&agrave; thanh to&aacute;n ph&acirc;̀n c&ograve;n lại trước 15 ng&agrave;y khởi h&agrave;nh tour. (C&aacute;c ng&agrave;y Lễ, Tết thanh to&aacute;n trước 25 ng&agrave;y )</p>\r\n\r\n<p>+&nbsp;Vui l&ograve;ng ki&ecirc;̉m tra hạn h&ocirc;̣ chi&ecirc;́u c&ograve;n 6 th&aacute;ng t&iacute;nh đ&ecirc;́n ng&agrave;y đi tour v&ecirc;̀. H&ocirc;̣ chi&ecirc;́u đảm bảo c&aacute;c y&ecirc;́u t&ocirc;́ sau:</p>\r\n\r\n<p>H&igrave;nh ảnh kh&ocirc;ng bị hư hỏng, mờ nh&ograve;e, th&ocirc;ng tin đ&acirc;̀y đủ, d&ugrave; c&ograve;n hạn sử dụng nhưng n&ecirc;́u h&igrave;nh ảnh bị mờ nh&ograve;e, v&acirc;̃n kh&ocirc;ng được xu&acirc;́t hay nh&acirc;̣p cảnh ....trường hợp v&agrave;o ng&agrave;y khởi h&agrave;nh, Qu&yacute; kh&aacute;ch kh&ocirc;ng được xu&acirc;́t hoặc nh&acirc;̣p cảnh trong nước v&agrave; ngo&agrave;i nước v&igrave; l&yacute; do c&aacute; nh&acirc;n c&ocirc;ng ty sẽ kh&ocirc;ng chịu tr&aacute;ch nhi&ecirc;̣m v&agrave; kh&ocirc;ng ho&agrave;n ti&ecirc;̀n c&aacute;c khoản chi ph&iacute; li&ecirc;n quan kh&aacute;c.</p>\r\n\r\n<p>+&nbsp;Kh&aacute;ch mang quốc tịch Nước ngo&agrave;i phải c&oacute; Visa (Thị thực) nhập cảnh Việt Nam c&ograve;n hạn sử dụng t&iacute;nh đến ng&agrave;y khởi h&agrave;nh.( NẾU CHƯA C&Oacute; VUI L&Ograve;NG TH&Ocirc;NG B&Aacute;O ĐỂ ĐƯỢC TƯ VẤN )</p>\r\n\r\n<p>+&nbsp;Khi kh&aacute;ch h&agrave;ng đăng k&yacute; tour, vui l&ograve;ng cung c&acirc;́p th&ocirc;ng tin c&aacute; nh&acirc;n tr&ecirc;n h&ocirc;̣ chi&ecirc;́u : Họ &amp; T&ecirc;n ch&iacute;nh x&aacute;c, ng&agrave;y c&acirc;́p, ng&agrave;y h&ecirc;́t hạn h&ocirc;̣ chi&ecirc;́u, s&ocirc;́ đi&ecirc;̣n thoại li&ecirc;n lạc ,địa chỉ li&ecirc;n lạc&hellip;&hellip;.</p>\r\n\r\n<p>+&nbsp;N&ecirc;́u v&agrave;o ng&agrave;y khởi h&agrave;nh v&igrave; l&yacute; do kh&ocirc;ng đủ s&ocirc;́ lượng kh&aacute;ch, hay b&acirc;́t cứ l&yacute; do kh&aacute;ch quan n&agrave;o kh&aacute;c&hellip; Tour kh&ocirc;ng khởi h&agrave;nh đ&uacute;ng ng&agrave;y, C&ocirc;ng ty sẽ th&ocirc;ng b&aacute;o cho Qu&yacute; Kh&aacute;ch ng&agrave;y mở tour v&agrave;o đợt k&ecirc;́ ti&ecirc;́p, hoặc ho&agrave;n ti&ecirc;̀n lại cho qu&yacute; kh&aacute;ch v&agrave; C&ocirc;ng ty sẽ kh&ocirc;ng chi trả th&ecirc;m b&acirc;́t kỳ chi ph&iacute; ph&aacute;t sinh kh&aacute;c.</p>\r\n\r\n<p>+&nbsp;Nếu Qu&yacute; kh&aacute;ch l&agrave; Việt Kiều hoặc kh&aacute;ch Nước Ngo&agrave;i v&agrave; visa VN ghi trong hộ chiếu l&agrave; loại visa nhập cảnh 1 lần, sẽ phụ thu th&ecirc;m ph&iacute; l&agrave;m visa t&aacute;i nhập VN l&agrave; 1.480.000vnđ/kh&aacute;ch (nộp hộ chiếu gốc + 1 h&igrave;nh 4x6 nền trắng). Ri&ecirc;ng Visa t&aacute;i nhập VN l&agrave;m tại cửa khẩu l&agrave; 650.000vnđ/kh&aacute;ch (nộp hộ chiếu gốc hoặc photo nộp k&egrave;m 1 h&igrave;nh 4x6 nền trắng).</p>\r\n\r\n<p>Viettourist sẽ kh&ocirc;ng chịu tr&aacute;ch nhiệm nếu Qu&yacute; kh&aacute;ch bị từ chối xuất cảnh của Cơ quan QLXNC Việt Nam v&agrave; nhập cảnh của Cơ quan QLXNC nước sở tại với bất cứ l&yacute; do n&agrave;o.</p>\r\n\r\n<p>+&nbsp;Khi đăng k&yacute; tour du lịch, Qu&yacute; kh&aacute;ch vui l&ograve;ng đọc kỹ chương tr&igrave;nh, gi&aacute; tour, c&aacute;c khoản bao gồm cũng như kh&ocirc;ng bao gồm trong chương tr&igrave;nh, c&aacute;c điều kiện hủy tour tr&ecirc;n bi&ecirc;n nhận đ&oacute;ng tiền. Trong trường hợp Qu&yacute; kh&aacute;ch kh&ocirc;ng trực tiếp đến đăng k&yacute; tour m&agrave; do người kh&aacute;c đến đăng k&yacute; th&igrave; Qu&yacute; kh&aacute;ch vui l&ograve;ng t&igrave;m hiểu kỹ chương tr&igrave;nh từ người đăng k&yacute; cho m&igrave;nh.</p>\r\n\r\n<p>+&nbsp;Kh&ocirc;ng sử dụng thẻ xanh, Nếu l&agrave; Sổ du lịch (cần l&agrave;m visa nước cần nhập cảnh) Cần th&ocirc;ng b&aacute;o cho nh&acirc;n vi&ecirc;n nhận tour nếu Qu&yacute; kh&aacute;ch sử dụng c&aacute;c hồ sơ kh&aacute;c nhập kh&ocirc;ng phải hộ chiếu.</p>\r\n\r\n<p>+&nbsp;Qu&yacute; kh&aacute;ch mang 2 Quốc tịch hoặc Travel document (chưa nhập quốc tịch) vui l&ograve;ng th&ocirc;ng b&aacute;o với nh&acirc;n vi&ecirc;n b&aacute;n tour ngay thời điểm đăng k&yacute; tour v&agrave; nộp bản gốc k&egrave;m c&aacute;c giấy tờ c&oacute; li&ecirc;n quan (nếu c&oacute;).</p>\r\n\r\n<p>+&nbsp;Qu&yacute; kh&aacute;ch chỉ mang thẻ xanh (thẻ tạm tr&uacute; tại nước ngo&agrave;i) v&agrave; kh&ocirc;ng c&ograve;n hộ chiếu VN c&ograve;n hiệu lực th&igrave; kh&ocirc;ng đăng k&yacute; du lịch sang nước thứ ba được.</p>\r\n\r\n<p>+&nbsp;Qu&yacute; kh&aacute;ch đăng k&yacute; tour vui l&ograve;ng theo đo&agrave;n về đ&uacute;ng ng&agrave;y kết th&uacute;c tour v&agrave; kh&ocirc;ng t&aacute;ch đo&agrave;n.</p>\r\n\r\n<p>+&nbsp;Nếu kh&aacute;ch l&agrave; Việt Kiều hoặc nước ngo&agrave;i c&oacute; visa rời phải mang theo l&uacute;c đi tour.</p>\r\n\r\n<p>+&nbsp;Nếu y&ecirc;u cầu ở ph&ograve;ng đơn, Qu&yacute; kh&aacute;ch vui l&ograve;ng thanh to&aacute;n tiền phụ thu.</p>\r\n\r\n<p>+&nbsp;Trẻ em dưới 24 tuổi phải c&oacute; bố mẹ đi c&ugrave;ng hoặc người được uỷ quyền phải c&oacute; giấy uỷ quyền từ bố mẹ.</p>\r\n\r\n<p>+&nbsp;V&igrave; mục đ&iacute;ch &amp; tr&aacute;ch nhiệm an to&agrave;n, c&ocirc;ng ty chỉ tổ chức tour du lịch tham quan thuần t&uacute;y. Qu&yacute; kh&aacute;ch kh&ocirc;ng được tự &yacute; t&aacute;ch v&agrave; bỏ đo&agrave;n trong thời gian tham gia chương tr&igrave;nh tour của c&ocirc;ng ty.</p>\r\n', '722-osaka-cungduongvang-22tr888.jpg', '', 'Du lịch Nhật Bản Cung Đường Vàng Osaka - Kyoto - Núi Phú Sỹ - Tokyo - Naritasan | 5N4Đ\r\n', 'du-lich-nhat-ban-cung-duong-vang-osaka-kyoto-nui-phu-sy-tokyo-naritasan-5n4d', NULL, NULL, 122, 0, '<p><strong>Du lịch Nhật Bản Cung Đường V&agrave;ng Osaka - Kyoto - N&uacute;i Ph&uacute; Sỹ - Tokyo - Naritasan | 5N4Đ bay thẳng to&agrave;n chặng</strong>. Khởi h&agrave;nh từ HCM/H&Agrave; NỘI&nbsp;<strong>-&nbsp;</strong>Tour trọn g&oacute;i đ&atilde; bao&nbsp;gồm v&eacute; m&aacute;y bay khứ hồi khởi h&agrave;nh trong c&aacute;c dịp lễ Giỗ&nbsp;tổ H&ugrave;ng Vương - Tour lễ 30/4 - 1/5 - Du lịch h&egrave; th&aacute;ng 6.7.8 + Du lịch m&ugrave;a Noel , Năm mới, Tết Nguy&ecirc;n Đ&aacute;n...Li&ecirc;n hệ hotline: 19001868 - 0909886688.</p>\r\n', 'Du lịch Nhật Bản Cung Đường Vàng Osaka - Kyoto - Núi Phú Sỹ - Tokyo - Naritasan | 5N4Đ\r\n', '', 0, '22,000,000', '26,000,000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 5, 1, 0, 1, 0, NULL, NULL, NULL, NULL, 71, 0, NULL, NULL, '2023-06-28 03:06:58', ' 3 ngày 2 đêm', 'MÁY BAY', '<p><span style=\"color:#ff3300\"><strong>Khởi h&agrave;nh định kỳ suốt năm</strong></span></p>\r\n', NULL, 'NHẬT BẢN', 21, 'Hồ Chí Minh', 'https://drive.google.com/file/d/1dLJtTAuU7l7nUhtKZB6q39S9v23KTCPG/view?pli=1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pro_center`
--

CREATE TABLE `tbl_pro_center` (
  `pro_id` int(10) UNSIGNED NOT NULL,
  `pro_catelogy_id` int(11) NOT NULL,
  `pro_product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_pro_center`
--

INSERT INTO `tbl_pro_center` (`pro_id`, `pro_catelogy_id`, `pro_product_id`) VALUES
(917, 482, 872),
(918, 482, 870),
(920, 485, 873),
(933, 482, 871),
(934, 492, 874),
(935, 480, 874),
(936, 492, 875),
(937, 480, 875);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_regiter_mail`
--

CREATE TABLE `tbl_regiter_mail` (
  `regiter_mail_id` int(11) NOT NULL,
  `regiter_mail_name` varchar(255) DEFAULT NULL,
  `regiter_mail_email` varchar(255) NOT NULL,
  `regiter_mail_phone` varchar(11) DEFAULT NULL,
  `regiter_mail_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `publication_status` tinyint(4) NOT NULL,
  `regiter_mail_content` varchar(555) DEFAULT NULL,
  `regiter_mail_address` varchar(255) DEFAULT NULL,
  `regiter_service` varchar(255) DEFAULT NULL,
  `regiter_gioitinh` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_regiter_pass`
--

CREATE TABLE `tbl_regiter_pass` (
  `regiter_pass_id` int(11) NOT NULL,
  `regiter_pass_phone` varchar(11) NOT NULL,
  `regiter_pass_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_request`
--

CREATE TABLE `tbl_request` (
  `request_id` int(11) NOT NULL,
  `request_name` varchar(255) NOT NULL,
  `request_address` varchar(255) NOT NULL,
  `request_phone` varchar(11) NOT NULL,
  `request_email` varchar(255) NOT NULL,
  `request_content` varchar(255) NOT NULL,
  `request_product_id` int(11) NOT NULL,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_request`
--

INSERT INTO `tbl_request` (`request_id`, `request_name`, `request_address`, `request_phone`, `request_email`, `request_content`, `request_product_id`, `publication_status`) VALUES
(24, 'văn long', '24 huỳnh thiên lộc quận tân phú', '097564333', 'vanlong@gmail.com', 'tôi muốn tư vấn', 151, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_shipping`
--

CREATE TABLE `tbl_shipping` (
  `shipping_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `shipping_name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `shipping_email` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `shipping_address` text CHARACTER SET utf8,
  `shipping_city` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `shipping_country` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `shipping_phone` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `shipping_content` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `shipping_radioac` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `shipping_zipcode` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_shipping`
--

INSERT INTO `tbl_shipping` (`shipping_id`, `customer_id`, `shipping_name`, `shipping_email`, `shipping_address`, `shipping_city`, `shipping_country`, `shipping_phone`, `shipping_content`, `shipping_radioac`, `shipping_zipcode`) VALUES
(505, 0, 'đặng văn test', 'dangthanh.test@gmail.com', '17 huỳnh thiên lộc quận tân phú', 'Hồ chí minh', NULL, '0396564560', 'tôi muốn đặt tour này', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_size`
--

CREATE TABLE `tbl_size` (
  `size_id` int(11) NOT NULL,
  `size_name` varchar(255) NOT NULL,
  `size_price` text NOT NULL,
  `size_description` text,
  `size_image` varchar(255) DEFAULT NULL,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_size`
--

INSERT INTO `tbl_size` (`size_id`, `size_name`, `size_price`, `size_description`, `size_image`, `publication_status`) VALUES
(9, 'FREESHIP', '', NULL, NULL, 1),
(10, 'GIAO NGAY', '', NULL, NULL, 1),
(11, 'FREESHIP với đơn hàng chỉ từ 149.000₫', '', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `slider_id` int(11) NOT NULL,
  `slider_title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `slider_image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `slider_link` varchar(255) CHARACTER SET utf8 NOT NULL,
  `slider_link_video` text CHARACTER SET utf8 NOT NULL,
  `publication_status` tinyint(4) NOT NULL,
  `slider_image_moblie` varchar(255) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`slider_id`, `slider_title`, `slider_image`, `slider_link`, `slider_link_video`, `publication_status`, `slider_image_moblie`) VALUES
(69, 'Banner 1', 'banner2.jpg', '', '', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_thanhphan`
--

CREATE TABLE `tbl_thanhphan` (
  `thanhphan_id` int(11) NOT NULL,
  `thanhphan_slug` varchar(255) NOT NULL,
  `thanhphan_name` varchar(255) NOT NULL,
  `thanhphan_description` text,
  `thanhphan_image` varchar(255) DEFAULT NULL,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_thanhphan`
--

INSERT INTO `tbl_thanhphan` (`thanhphan_id`, `thanhphan_slug`, `thanhphan_name`, `thanhphan_description`, `thanhphan_image`, `publication_status`) VALUES
(111, 'tour-trong-nuoc', 'Tour trong nước', NULL, NULL, 1),
(112, 'tour-nuoc-ngoai', 'Tour nước ngoài', NULL, NULL, 1),
(113, 'tour-mien-trung', 'Tour miền trung', NULL, NULL, 1),
(114, 'tour-han-quoc', 'Tour hàn quốc', NULL, NULL, 1),
(115, 'tour-nhat-ban', 'Tour nhật bản', NULL, NULL, 1),
(116, 'tour-phu-quoc', 'Tour phú quốc', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_thumb`
--

CREATE TABLE `tbl_thumb` (
  `id` int(11) UNSIGNED NOT NULL,
  `image` text NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_thumb`
--

INSERT INTO `tbl_thumb` (`id`, `image`, `product_id`) VALUES
(939, '1687918404hg-hoa-tam-giac-mach-1.jpg', 870),
(940, '1687918404hg-hoa-tam-giac-mach-3.jpg', 870),
(941, '1687918404hg-hoa-tam-giac-mach-4.jpg', 870),
(942, '1687918404hg-hoa-tam-giac-mach-5.jpg', 870),
(943, '1687918404hg-hoa-tam-giac-mach-6.jpg', 870),
(944, '1687918404hg-hoa-tam-giac-mach-7.jpg', 870),
(945, '1687918404hg-hoa-tam-giac-mach-8.jpg', 870),
(946, '16879188704.jpg', 871),
(947, '16879188705.jpg', 871),
(948, '16879188707.jpg', 871),
(949, '16879188709.jpg', 871),
(950, '168791887015.jpg', 871),
(951, '168791887017.jpg', 871),
(952, '168791887018.jpg', 871),
(953, '168791887022.jpg', 871),
(954, '1687920555banahill-2.jpg', 873),
(955, '1687920555banahill-11.jpg', 873),
(956, '1687920555banahill-12.jpg', 873),
(957, '1687920555banahill-19.jpg', 873),
(958, '1687920555banahill-20.jpg', 873),
(959, '1687920555banahill-27.jpg', 873),
(960, '1687920555bandaosontra-12.jpg', 873),
(961, '1687921404AsakusaKannon1.jpg', 874),
(962, '1687921404AsakusaKannon3.jpg', 874),
(963, '1687921404ho-Kawaguchi1.jpg', 874),
(964, '1687921404ho-Kawaguchi5.jpg', 874),
(965, '1687921404ho-Kawaguchi8.jpg', 874),
(966, '1687921404ho-Kawaguchi9.jpg', 874),
(967, '1687921404tokyo.jpg', 874),
(968, '1688091872AsakusaKannon1.jpg', 875),
(969, '1688091872AsakusaKannon3.jpg', 875),
(970, '1688091872ho-Kawaguchi1.jpg', 875),
(971, '1688091872ho-Kawaguchi5.jpg', 875),
(972, '1688091872ho-Kawaguchi8.jpg', 875),
(973, '1688091872ho-Kawaguchi9.jpg', 875),
(974, '1688091872tokyo.jpg', 875);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `user_email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_role` tinyint(4) NOT NULL,
  `lv1` tinyint(11) NOT NULL,
  `product` tinyint(4) NOT NULL,
  `product_home` tinyint(4) DEFAULT NULL,
  `gallery` tinyint(4) NOT NULL,
  `category_news` tinyint(4) NOT NULL,
  `news` tinyint(4) NOT NULL,
  `slider` tinyint(4) NOT NULL,
  `feel` tinyint(11) NOT NULL,
  `company` tinyint(4) NOT NULL,
  `partner` tinyint(4) NOT NULL,
  `contact` tinyint(4) NOT NULL,
  `comment` tinyint(4) DEFAULT NULL,
  `order` tinyint(4) NOT NULL,
  `role_edit_lv1` tinyint(4) DEFAULT NULL,
  `role_edit_product` tinyint(4) DEFAULT NULL,
  `role_edit_slider` tinyint(4) DEFAULT NULL,
  `role_edit_order` tinyint(4) DEFAULT NULL,
  `role_edit_feel` tinyint(4) DEFAULT NULL,
  `role_edit_contact` tinyint(4) DEFAULT NULL,
  `role_edit_company` tinyint(4) DEFAULT NULL,
  `role_edit_role` tinyint(4) DEFAULT NULL,
  `role_edit_product_home` tinyint(4) DEFAULT NULL,
  `role_edit_partner` tinyint(4) DEFAULT NULL,
  `role_edit_gallery` tinyint(4) DEFAULT NULL,
  `role_edit_role_user` tinyint(4) DEFAULT NULL,
  `role_edit_news` tinyint(4) DEFAULT NULL,
  `role_add_lv1` tinyint(4) DEFAULT NULL,
  `role_add_product` tinyint(4) DEFAULT NULL,
  `role_add_product_home` tinyint(4) DEFAULT NULL,
  `role_add_order` tinyint(4) DEFAULT NULL,
  `role_add_gallery` tinyint(4) DEFAULT NULL,
  `role_edit_category_news` tinyint(4) DEFAULT NULL,
  `role_add_category_news` tinyint(4) DEFAULT NULL,
  `role_add_news` tinyint(4) DEFAULT NULL,
  `role_add_slider` tinyint(4) DEFAULT NULL,
  `role_add_feel` tinyint(4) DEFAULT NULL,
  `role_add_partner` tinyint(4) DEFAULT NULL,
  `role_add_company` tinyint(4) DEFAULT NULL,
  `role_add_contact` tinyint(4) DEFAULT NULL,
  `role_add_role` tinyint(4) DEFAULT NULL,
  `role_add_role_user` tinyint(4) DEFAULT NULL,
  `role_delete_lv1` tinyint(4) DEFAULT NULL,
  `role_delete_product` tinyint(4) DEFAULT NULL,
  `role_delete_product_home` tinyint(4) DEFAULT NULL,
  `role_delete_gallery` tinyint(4) DEFAULT NULL,
  `role_delete_category_news` tinyint(4) DEFAULT NULL,
  `role_delete_news` tinyint(4) DEFAULT NULL,
  `role_delete_slider` tinyint(4) DEFAULT NULL,
  `role_delete_feel` tinyint(4) DEFAULT NULL,
  `role_delete_partner` tinyint(4) DEFAULT NULL,
  `role_delete_company` tinyint(4) DEFAULT NULL,
  `role_delete_contact` tinyint(4) DEFAULT NULL,
  `role_delete_role` tinyint(4) DEFAULT NULL,
  `role_delete_role_user` tinyint(4) DEFAULT NULL,
  `role_delete_order` tinyint(4) DEFAULT NULL,
  `role_delete_comment` tinyint(4) DEFAULT NULL,
  `role_delete_thumb` tinyint(4) DEFAULT NULL,
  `role_edit_thumb` tinyint(4) DEFAULT NULL,
  `role` tinyint(4) DEFAULT NULL,
  `role_user` tinyint(4) DEFAULT NULL,
  `created_time` timestamp NULL DEFAULT NULL,
  `updated_time` timestamp NULL DEFAULT NULL,
  `thuonghieu` int(11) NOT NULL,
  `thanhphan` int(11) NOT NULL,
  `congdung` int(11) NOT NULL,
  `xuatxu` int(11) NOT NULL,
  `tinhthanh` int(11) NOT NULL,
  `magiamgia` int(11) NOT NULL,
  `page` int(11) NOT NULL,
  `avd` int(11) NOT NULL,
  `chinhsach` int(11) NOT NULL,
  `dangkymail` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_name`, `user_email`, `user_password`, `user_role`, `lv1`, `product`, `product_home`, `gallery`, `category_news`, `news`, `slider`, `feel`, `company`, `partner`, `contact`, `comment`, `order`, `role_edit_lv1`, `role_edit_product`, `role_edit_slider`, `role_edit_order`, `role_edit_feel`, `role_edit_contact`, `role_edit_company`, `role_edit_role`, `role_edit_product_home`, `role_edit_partner`, `role_edit_gallery`, `role_edit_role_user`, `role_edit_news`, `role_add_lv1`, `role_add_product`, `role_add_product_home`, `role_add_order`, `role_add_gallery`, `role_edit_category_news`, `role_add_category_news`, `role_add_news`, `role_add_slider`, `role_add_feel`, `role_add_partner`, `role_add_company`, `role_add_contact`, `role_add_role`, `role_add_role_user`, `role_delete_lv1`, `role_delete_product`, `role_delete_product_home`, `role_delete_gallery`, `role_delete_category_news`, `role_delete_news`, `role_delete_slider`, `role_delete_feel`, `role_delete_partner`, `role_delete_company`, `role_delete_contact`, `role_delete_role`, `role_delete_role_user`, `role_delete_order`, `role_delete_comment`, `role_delete_thumb`, `role_edit_thumb`, `role`, `role_user`, `created_time`, `updated_time`, `thuonghieu`, `thanhphan`, `congdung`, `xuatxu`, `tinhthanh`, `magiamgia`, `page`, `avd`, `chinhsach`, `dangkymail`) VALUES
(1, 'admin', 'admin@gmail.com', 'eb7f9ddec2f69ba98b6b592497d51ace', 1, 1, 1, NULL, 1, 1, 1, 1, 0, 1, 1, 1, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, 1, 1, 1, 1, 1, 1, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, 0, 0, 1, 1, '2017-11-13 18:31:36', '2017-11-13 18:31:36', 0, 1, 1, 1, 0, 0, 1, 0, 0, 1),
(12, 'content', 'content@gmail.com', 'eb7f9ddec2f69ba98b6b592497d51ace', 1, 1, 1, NULL, 1, 1, 1, 0, 0, 0, 0, 0, NULL, 0, 1, 1, 1, 1, 1, 1, 1, 1, NULL, 1, 1, 1, 1, 1, 1, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, NULL, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, NULL, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0),
(17, 'tts@gmail.com', 'tts@gmail.com', 'ef73781effc5774100f87fe2f437a435', 1, 1, 1, NULL, 0, 1, 1, 1, 1, 1, 1, 1, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, 1, NULL, 1, 1, 1, 1, NULL, 1, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(18, 'admin', 'dung.buffseo@gmail.com', '58cafa76aae0c8328fa6f0fd3e367e8e', 0, 1, 1, NULL, 0, 1, 1, 0, 0, 0, 0, 0, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, NULL, 0, NULL, 0, 1, 1, 1, NULL, 0, NULL, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `oauth_provider` varchar(255) NOT NULL,
  `oauth_uid` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `locale` varchar(10) NOT NULL,
  `picture_url` varchar(255) NOT NULL,
  `profile_url` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_xuatxu`
--

CREATE TABLE `tbl_xuatxu` (
  `xuatxu_id` int(11) NOT NULL,
  `xuatxu_slug` varchar(255) DEFAULT NULL,
  `xuatxu_name` varchar(255) NOT NULL,
  `xuatxu_description` text,
  `xuatxu_image` varchar(255) DEFAULT NULL,
  `publication_status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_xuatxu`
--

INSERT INTO `tbl_xuatxu` (`xuatxu_id`, `xuatxu_slug`, `xuatxu_name`, `xuatxu_description`, `xuatxu_image`, `publication_status`) VALUES
(19, 'ha-noi', 'Hà Nội', NULL, NULL, 1),
(20, 'da-nang', 'Đà Nẵng', NULL, NULL, 1),
(21, 'ho-chi-minh', 'Hồ Chí Minh', NULL, NULL, 1),
(22, 'can-tho', 'Cần Thơ', NULL, NULL, 1),
(23, 'ca-mau', 'Cà Mau', NULL, NULL, 1),
(24, 'quy-nhon', 'Quy Nhơn', NULL, NULL, 1),
(25, 'phu-yen', 'Phú yên', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`role_id`, `role_name`) VALUES
(1, 'Admin'),
(2, 'Author'),
(3, 'Editor');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_avd`
--
ALTER TABLE `tbl_avd`
  ADD PRIMARY KEY (`avd_id`);

--
-- Indexes for table `tbl_avd_home`
--
ALTER TABLE `tbl_avd_home`
  ADD PRIMARY KEY (`avd_home_id`);

--
-- Indexes for table `tbl_baogia`
--
ALTER TABLE `tbl_baogia`
  ADD PRIMARY KEY (`baogia_id`);

--
-- Indexes for table `tbl_brand`
--
ALTER TABLE `tbl_brand`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_category_dmtin`
--
ALTER TABLE `tbl_category_dmtin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_chinhsach`
--
ALTER TABLE `tbl_chinhsach`
  ADD PRIMARY KEY (`chinhsach_id`);

--
-- Indexes for table `tbl_chuongtrinh`
--
ALTER TABLE `tbl_chuongtrinh`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tbl_city`
--
ALTER TABLE `tbl_city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_color`
--
ALTER TABLE `tbl_color`
  ADD PRIMARY KEY (`color_id`);

--
-- Indexes for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `tbl_comment_news`
--
ALTER TABLE `tbl_comment_news`
  ADD PRIMARY KEY (`cmt_id`);

--
-- Indexes for table `tbl_comment_news_rate`
--
ALTER TABLE `tbl_comment_news_rate`
  ADD PRIMARY KEY (`comment_news_rate_id`);

--
-- Indexes for table `tbl_comment_product`
--
ALTER TABLE `tbl_comment_product`
  ADD PRIMARY KEY (`cmt_id`);

--
-- Indexes for table `tbl_congdung`
--
ALTER TABLE `tbl_congdung`
  ADD PRIMARY KEY (`congdung_id`);

--
-- Indexes for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `tbl_counter`
--
ALTER TABLE `tbl_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `tbl_depart`
--
ALTER TABLE `tbl_depart`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `tbl_discount_codes`
--
ALTER TABLE `tbl_discount_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_dmtin`
--
ALTER TABLE `tbl_dmtin`
  ADD PRIMARY KEY (`dmtin_id`);

--
-- Indexes for table `tbl_feel`
--
ALTER TABLE `tbl_feel`
  ADD PRIMARY KEY (`feel_id`);

--
-- Indexes for table `tbl_indexsp`
--
ALTER TABLE `tbl_indexsp`
  ADD PRIMARY KEY (`indexsp_id`);

--
-- Indexes for table `tbl_link301`
--
ALTER TABLE `tbl_link301`
  ADD PRIMARY KEY (`link301_id`);

--
-- Indexes for table `tbl_muanhanh`
--
ALTER TABLE `tbl_muanhanh`
  ADD PRIMARY KEY (`muanhanh_id`);

--
-- Indexes for table `tbl_muti_news`
--
ALTER TABLE `tbl_muti_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_one_news`
--
ALTER TABLE `tbl_one_news`
  ADD PRIMARY KEY (`one_news_id`);

--
-- Indexes for table `tbl_option`
--
ALTER TABLE `tbl_option`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
  ADD PRIMARY KEY (`order_details_id`);

--
-- Indexes for table `tbl_origin`
--
ALTER TABLE `tbl_origin`
  ADD PRIMARY KEY (`origin_id`);

--
-- Indexes for table `tbl_partner`
--
ALTER TABLE `tbl_partner`
  ADD PRIMARY KEY (`partner_id`);

--
-- Indexes for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_pro_center`
--
ALTER TABLE `tbl_pro_center`
  ADD PRIMARY KEY (`pro_id`);

--
-- Indexes for table `tbl_regiter_mail`
--
ALTER TABLE `tbl_regiter_mail`
  ADD PRIMARY KEY (`regiter_mail_id`);

--
-- Indexes for table `tbl_regiter_pass`
--
ALTER TABLE `tbl_regiter_pass`
  ADD PRIMARY KEY (`regiter_pass_id`);

--
-- Indexes for table `tbl_request`
--
ALTER TABLE `tbl_request`
  ADD PRIMARY KEY (`request_id`);

--
-- Indexes for table `tbl_shipping`
--
ALTER TABLE `tbl_shipping`
  ADD PRIMARY KEY (`shipping_id`);

--
-- Indexes for table `tbl_size`
--
ALTER TABLE `tbl_size`
  ADD PRIMARY KEY (`size_id`);

--
-- Indexes for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `tbl_thanhphan`
--
ALTER TABLE `tbl_thanhphan`
  ADD PRIMARY KEY (`thanhphan_id`);

--
-- Indexes for table `tbl_thumb`
--
ALTER TABLE `tbl_thumb`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_xuatxu`
--
ALTER TABLE `tbl_xuatxu`
  ADD PRIMARY KEY (`xuatxu_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_avd`
--
ALTER TABLE `tbl_avd`
  MODIFY `avd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `tbl_avd_home`
--
ALTER TABLE `tbl_avd_home`
  MODIFY `avd_home_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_baogia`
--
ALTER TABLE `tbl_baogia`
  MODIFY `baogia_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_brand`
--
ALTER TABLE `tbl_brand`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=505;

--
-- AUTO_INCREMENT for table `tbl_category_dmtin`
--
ALTER TABLE `tbl_category_dmtin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `tbl_chinhsach`
--
ALTER TABLE `tbl_chinhsach`
  MODIFY `chinhsach_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_chuongtrinh`
--
ALTER TABLE `tbl_chuongtrinh`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=683;

--
-- AUTO_INCREMENT for table `tbl_city`
--
ALTER TABLE `tbl_city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_color`
--
ALTER TABLE `tbl_color`
  MODIFY `color_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `tbl_comment_news`
--
ALTER TABLE `tbl_comment_news`
  MODIFY `cmt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `tbl_comment_news_rate`
--
ALTER TABLE `tbl_comment_news_rate`
  MODIFY `comment_news_rate_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_comment_product`
--
ALTER TABLE `tbl_comment_product`
  MODIFY `cmt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `tbl_congdung`
--
ALTER TABLE `tbl_congdung`
  MODIFY `congdung_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `tbl_contact`
--
ALTER TABLE `tbl_contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_counter`
--
ALTER TABLE `tbl_counter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1556;

--
-- AUTO_INCREMENT for table `tbl_customer`
--
ALTER TABLE `tbl_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `tbl_depart`
--
ALTER TABLE `tbl_depart`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=823;

--
-- AUTO_INCREMENT for table `tbl_discount_codes`
--
ALTER TABLE `tbl_discount_codes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tbl_dmtin`
--
ALTER TABLE `tbl_dmtin`
  MODIFY `dmtin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=423;

--
-- AUTO_INCREMENT for table `tbl_feel`
--
ALTER TABLE `tbl_feel`
  MODIFY `feel_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_indexsp`
--
ALTER TABLE `tbl_indexsp`
  MODIFY `indexsp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_link301`
--
ALTER TABLE `tbl_link301`
  MODIFY `link301_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_muanhanh`
--
ALTER TABLE `tbl_muanhanh`
  MODIFY `muanhanh_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `tbl_muti_news`
--
ALTER TABLE `tbl_muti_news`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_one_news`
--
ALTER TABLE `tbl_one_news`
  MODIFY `one_news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `tbl_option`
--
ALTER TABLE `tbl_option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=436;

--
-- AUTO_INCREMENT for table `tbl_order_details`
--
ALTER TABLE `tbl_order_details`
  MODIFY `order_details_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=473;

--
-- AUTO_INCREMENT for table `tbl_origin`
--
ALTER TABLE `tbl_origin`
  MODIFY `origin_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_partner`
--
ALTER TABLE `tbl_partner`
  MODIFY `partner_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `product_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=876;

--
-- AUTO_INCREMENT for table `tbl_pro_center`
--
ALTER TABLE `tbl_pro_center`
  MODIFY `pro_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=938;

--
-- AUTO_INCREMENT for table `tbl_regiter_mail`
--
ALTER TABLE `tbl_regiter_mail`
  MODIFY `regiter_mail_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_regiter_pass`
--
ALTER TABLE `tbl_regiter_pass`
  MODIFY `regiter_pass_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_request`
--
ALTER TABLE `tbl_request`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tbl_shipping`
--
ALTER TABLE `tbl_shipping`
  MODIFY `shipping_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=506;

--
-- AUTO_INCREMENT for table `tbl_size`
--
ALTER TABLE `tbl_size`
  MODIFY `size_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_slider`
--
ALTER TABLE `tbl_slider`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `tbl_thanhphan`
--
ALTER TABLE `tbl_thanhphan`
  MODIFY `thanhphan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT for table `tbl_thumb`
--
ALTER TABLE `tbl_thumb`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=975;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_xuatxu`
--
ALTER TABLE `tbl_xuatxu`
  MODIFY `xuatxu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
