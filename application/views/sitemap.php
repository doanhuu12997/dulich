<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <?php date_default_timezone_set('Asia/Ho_Chi_Minh');?>
    <url>
        <loc><?=base_url();?></loc>
        <lastmod><?=date('c',time());?> </lastmod>
        <changefreq>daily</changefreq>
        <priority>1.00</priority>
    </url>
     <?php foreach($all_categroy as $cate){?>
    <url>
        <loc><?=base_url($cate->slug).'.html';?></loc>
        <lastmod><?=nice_date($cate->category_datetime,'c');?></lastmod>
        <changefreq>daily</changefreq>
        <priority>0.80</priority>
    </url>
    <?php }?>
    <?php foreach($products as $product){?>
    <url>
        <loc><?=base_url('/').$product->product_slug.'.html';?></loc>
        <lastmod><?=nice_date($product->product_datetime,'Y-m-d\TH:i:sP');?></lastmod>
        <changefreq>daily</changefreq>
        <priority>0.60</priority>
    </url>
    <?php }?>
     <?php foreach($all_categroy_news as $cate_news){?>
    <url>
        <loc><?=base_url($cate_news->slug).'.html';?></loc>
        <lastmod><?=nice_date($cate_news->category_datetime,'Y-m-d\TH:i:sP');?></lastmod>
        <changefreq>daily</changefreq>
        <priority>0.80</priority>
    </url>
    <?php }?>
    <?php foreach($news as $n){?>
    <url>
        <loc><?=base_url('/').$n->dmtin_slug.'.html';?></loc>
        <lastmod><?=nice_date($n->create_date,'Y-m-d\TH:i:sP');?></lastmod>
        <changefreq>daily</changefreq>
        <priority>0.60</priority>
    </url>
    <?php }?>
</urlset>