
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="vi" xml:lang="vi">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>404 Not Found</title>
    <meta name="description" content="Tiếc quá, trang bạn muốn xem không tồn tại! Hãy thử sử dụng các tính năng bên dưới để tiếp tục nhé"/>
    <meta name="keywords" content=""/>

    <meta content="index,follow" name="googlebot" />
    <meta name="copyright" content="Copyright © 2022" />
    <meta name="robots" content="INDEX,FOLLOW"/>
         

    <meta name="DC.title" lang="vi" content="404 Not Found" />
    <meta name="DC.creator" content="Thiết kế web chuyên nghiệp Buffseo" />
    <meta name="DCTERMS.issued" scheme="DCTERMS.W3CDTF" content="Tue, 09 Jul 2019 10:00:25 +0700" />
    <meta name="DCTERMS.abstract" content="Tiếc quá, trang bạn muốn xem không tồn tại! Hãy thử sử dụng các tính năng bên dưới để tiếp tục nhé" />
    <meta name="DC.format" scheme="DCTERMS.IMT" content="text/html" />
    <meta name="DC.type" scheme="DCTERMS.DCMIType" content="Text" />


    <meta property="og:title" content="404 Not Found" />
    <meta property="og:description" content="404 Not Found" />
    <meta property="og:type" content="article" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="Tiếc quá, trang bạn muốn xem không tồn tại! Hãy thử sử dụng các tính năng bên dưới để tiếp tục nhé">


     <meta name="geo.region" content="VN-Hồ Chí Minh" />

    <link rel="stylesheet" href="/assets/web/bootstrap-3.3.7-dist/css/bootstrap.min.css" >
   
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

<div class="main">
    <div class="content">
        <div class="error col-md-12 col-xs-12 col-sm-12">   
            <h2>404</h2>
            <h3>Xin lỗi, trang bạn muốn xem không tồn tại!</h3>
            <p>Hãy thử sử dụng các tính năng bên dưới để tiếp tục nhé</p>
        </div>  
          
          <div class="col-sm-12 col-md-12 col-xs-12">
                <div class="block-404-navigation">
                    <a href="/">
                        <div class="btn__round__icon"><i class="adr-icon icon-red-home"></i></div><span>Trang chủ</span></a>
                    <a href="lien-he.html">
                        <div class="btn__round__icon"><i class="adr-icon icon-phone-2"></i></div><span>Liên hệ</span></a>
                </div>
            </div>
        <div class="clear"></div>
       

    </div>
</div>
<style>
    .video-container{
        margin: 0 auto;
        max-width: 800px;
    }
    .error{ text-align: center;margin-bottom: 15px;}
.error h2 {
    font-size: 50px;
    margin: 50px 0px 0px;
}  
 .error p {
    color: #000;
    text-align: center;
    font-size: 14px;
    line-height: 24px;
    margin-bottom: 0px;
}
 .block-404-navigation {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-pack: center;
    justify-content: center;
    margin-top: 10px;
}
 .block-404-navigation>a {
    margin: 16px;
    text-align: center;
}
 .block-404-navigation .btn__round__icon {
    width: 64px;
    height: 64px;
    border-radius: 50%;
    background: #e8eaee;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-pack: center;
    justify-content: center;
    -ms-flex-align: center;
    align-items: center;
    margin: 0 auto;
    margin-bottom: 12px;
}
 .block-404-navigation .btn__round__icon .adr-icon {
    width: 20px;
    height: 20px;
    font-size: 20px;
    margin-bottom: 0;
    color: #333;
    font-style: initial;
}
.adr-icon.icon-red-home:before {
    content: "\f015";
    font-family: "FontAwesome";
    color: #000;
    font-weight: 600;
}

.adr-icon.icon-phone-2:before {
    content: "\f095";
    font-family: "FontAwesome";
    color: #000;
    font-weight: 600;
}
.block-404-navigation>a span
{
    color: #000;
}
@media screen and (max-width: 1024px) {
    .error h3{
        font-size: 17px;
    }
    .video-container video
    {
        max-width: 100%;
    }
}
</style>
</body>
</html>
        <script> setTimeout(function(){ window.location.href = '/'; }, 5000); </script>