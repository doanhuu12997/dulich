
<div id="carousel-slider-doingu" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
<div class="vnt-doingu">
        <div class="slider-doingu wrap">
             <div class="wrap-header">
            <h2 class="wrap-title">Đội ngũ bác sĩ của Dr.Huệ</h2>
         <p class="wrap-desc">Tập hợp những hình ảnh thể hiện sự thay đổi rõ nét của khách hàng khi sử dụng dịch vụ tại Trung Tâm Dr.Huệ</p>
        </div>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
        
                <div class="item active">
                  <?php
                    $id_dm_news=68;
                    $limit_news=4;
                    $order_news="asc";
                    $news_doingu = $this->web_model->get_news_noibat_home($id_dm_news,$limit_news,$order_news);
                      foreach ($news_doingu as $key =>$v ) {
                        ?>
                        <div class="col-md-3 col-xs-6 col-sm-3">
                    <div class="i-image"><a><img src="<?php echo base_url() ?>uploads/news/<?php echo $v->dmtin_image ?>" alt="<?php echo $v->dmtin_title ?>"></a></div>
                    <div class="i-name"><span><?php echo $v->dmtin_title ?></span></div>
                    <div class="i-content"><?php echo $v->dmtin_short_description ?> </div>
                  </div><!--Emd6-->
                <?php }?>

                </div>

  </div>

 
 </div><!--End slider-feeling-->
</div>
</div>
