
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="vi" xml:lang="vi">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="keywords" content="<?=$get_id_catelogy_dmtin->keywords ?>" />
    <meta name="description" content="<?=$get_id_catelogy_dmtin->category_description_seo?>" />
    <title><?=$get_id_catelogy_dmtin->title?></title>
    <meta content="index,follow" name="googlebot" />
    <meta name="copyright" content="<?=get_option('site_copyright')?>" />
    <meta name="robots" content="INDEX,FOLLOW"/>
    <?=get_option('metageo')?>
    <?=get_option('schema_content')?>

    <meta name="DC.title" lang="vi" content="<?=$get_id_catelogy_dmtin->category_description_seo?>" />
    <meta name="DC.creator" content="Thiết kế web chuyên nghiệp - Buffseo" />
    <meta name="DCTERMS.issued" scheme="DCTERMS.W3CDTF" content="<?php date_default_timezone_set('Asia/Ho_Chi_Minh'); echo date('l, d M Y') ;  echo date(" h:i:sa")?>" />
    <meta name="DC.identifier" scheme="DCTERMS.URI" content="<?=base_url($get_id_catelogy_dmtin->slug).'.html'?>" />
    <link rel="DCTERMS.replaces" hreflang="vi" href="<?=base_url($get_id_catelogy_dmtin->slug).'.html';?>" />
    <meta name="DCTERMS.abstract" content="<?=$get_id_catelogy_dmtin->category_description_seo?>" />
    <meta name="DC.format" scheme="DCTERMS.IMT" content="text/html" />
    <meta name="DC.type" scheme="DCTERMS.DCMIType" content="Text" />
    <meta property="og:title" content="<?=$get_id_catelogy_dmtin->title?>" />
    <meta property="og:description" content="<?=$get_id_catelogy_dmtin->category_description_seo?>" />
    <meta property="og:type" content="article" />
    <?php foreach ($get_all_dmtin as $key => $v) { ?>
    <?php if($key==0) { ?><meta property="og:image" content="<?=base_url('uploads/news/'.$v->dmtin_image)?>" />  <?php  }?>
    <?php }?>
    <meta property="og:url" content="<?=base_url($get_id_catelogy_dmtin->slug).'.html'?>" />
    <link rel="canonical" href="<?php if(empty($get_id_catelogy_dmtin->slug)){} else echo base_url($get_id_catelogy_dmtin->slug).'.html'?>"/>
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="<?=get_option('site_twitter_link')?>">
    <meta name="twitter:title" content="<?=$get_id_catelogy_dmtin->title?>">
    <meta name="twitter:description" content="<?=$get_id_catelogy_dmtin->category_description_seo?>">
    <link rel="shortcut icon" href="/<?=get_option('site_favicon') ?>" type="image/x-icon">
    <link rel="icon" href="/<?=get_option('site_favicon') ?>" type="image/x-icon">
    <script type="application/ld+json">
      {
        "@context": "https://schema.org",
        "@type": "NewsArticle",
        "headline": "<?=$get_id_catelogy_dmtin->title?>",
        "mainEntityOfPage": "<?=base_url($get_id_catelogy_dmtin->slug).'.html';?>",
        "image": [
        <?php
        foreach($get_all_dmtin as $key =>$v){ if($key==0){?>
          "<?=base_url('uploads/news/'.$v->dmtin_image)?>",
        <?php } else if($key==1){ ?>
          "<?=base_url('uploads/news/'.$v->dmtin_image)?>",
        <?php } else if($key==2){ ?>
          "<?=base_url('uploads/news/'.$v->dmtin_image)?>"
        <?php } }?>
        ],
        "author": {
        "@type": "Person",
        "name": "<?=$get_user_name->user_name;?>",
        "url": "<?=base_url($get_id_catelogy_dmtin->slug).'.html';?>"
      },  
      "publisher": {
      "@type": "Organization",
      "name": "<?=$get_user_name->user_name;?>",
      "logo": {
      "@type": "ImageObject",
      "url": "<?=base_url('uploads/');  ?><?=get_option('site_logo'); ?>"
    }
  },
  "datePublished": "<?=$get_id_catelogy_dmtin->category_datetime?>",
  "dateModified": "<?=$get_id_catelogy_dmtin->category_datetime?>"
}
</script>