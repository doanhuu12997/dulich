<!--=== BEGIN: FUNCTION ĐỐI TÁC ===-->
<div class="vnt-function">
	<div class="wrapper">
		<div class="pf-title"><h2><b></b><span class="title-main">Đối tác tin cậy</span><b></b></h2></div>
		<div class="slider-function">
			<div id="slider-function">
				<?php
				$get_partner = $this->web_model->get_all_partner_post();
				foreach ($get_partner as $key) {
					?>
					<div class="item">
						<div class="i-images"><a title="<?php echo $key->partner_title ?>"><img
										src="<?php echo base_url() ?>uploads/<?php echo $key->partner_image ?>"
										alt="<?php echo $key->partner_title ?>"></a></div>
					</div>
				<?php } ?>

			</div>
		</div>
	</div>
</div>
<!--=== END: FUNCTION ĐỐI TÁC===-->
