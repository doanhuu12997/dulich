<div class="search-tour">
<div class="form-search-tour">
<div id="search">
    <form onSubmit="return validateForm_search()" name="formSearchname" method="get" action="tim-kiem.html" class="form-horizontal tour-form">
    <div class="col-md-6 col-xs-12 col-sm-6">
    <label>Nơi Đến</label>
    <div class="clear"></div>
    <div class="input-search-tour">
        <input class="form-control" type="text" placeholder="Nơi đến..." name="search" /> 
    </div><!--end input-search-tour-->
    </div>
        <!--End col-md-4-->
        <div class="col-md-2 col-xs-12 col-sm-2">
        <label>Từ ngày</label>
        <div class="clear"></div>
            <input type="date"   placeholder="Từ ngày" class="DateTours form-control form-readonly-control" value="" name="from_day">
        </div>
        <!--End col-md-3-->
        <div class="col-md-2 col-xs-12 col-sm-2">
        <label>Đến ngày</label>
       <div class="clear"></div>
        <input type="date"  placeholder="Đến ngày" class="DateTours form-control form-readonly-control" value="" name="to_day">
         </div>
        <!--End col-md-3-->
    
        <div class="col-md-2 col-xs-12 col-sm-2"> <button type="submit" class="btn btn-primary">TÌM KIẾM</button> </div>
        <!--End col-md-2-->

        <div class="clear"></div>
        <div class="tabs-select-travel">
          <ul class="nav nav-tabs" id="menu-select-tab" role="tablist">
              <li class="nav-item active"> <a class="nav-link active" id="ms-1-tab" data-toggle="tab" href="#ms-1" role="tab" aria-controls="ms-1" aria-selected="true"> <span>Điểm đến hàng đầu</span> </a> </li>
              <?php
               $get_all_cate_tour = $this->web_model->get_all_cate_tour();
              foreach ($get_all_cate_tour as $tour) { ?>
              <li class="nav-item"> <a class="nav-link" id="<?=$tour->id;?>-tab" data-toggle="tab" href="#<?=$tour->id;?>" role="tab" aria-controls="<?=$tour->id;?>" aria-selected="false"> <span><?=$tour->category_name;?></span> </a> </li>
              <?php }?>
          
            </ul>
          <div class="tab-content" id="content-select-tab">
              <div class="tab-pane fade  active" id="ms-1" role="tabpanel" aria-labelledby="ms-1-tab">
                  <div class="list-tour_v1">
                <?php   $rs_tour_nb = $this->web_model->getCate_tour_nb();
                        foreach($rs_tour_nb as $nb_tour){?>
                      <div class="item"> 
                        <a href="<?=$nb_tour->slug.'.html';?>" class="avt">
                             <img src="<?=base_url('uploads/'.$nb_tour->category_icon)?>" alt="<?=$nb_tour->category_name ?>">
                              <div class="overlay-item"></div> <span><?=$nb_tour->category_name ?></span>
                          </a>
                      </div>
                      <?php }?>
          
                  </div><!--End ist-tour-->
              </div><!--End tab-pane-->
            <?php  $get_all_cate_tour = $this->web_model->get_all_cate_tour();
              foreach ($get_all_cate_tour as $tour) { 
                $id_tour=$tour->id;
                $rs_sub_tour = $this->web_model->sub_category_tour($id_tour);
                ?>
              <div class="tab-pane fade" id="<?=$tour->id;?>" role="tabpanel" aria-labelledby="<?=$tour->id;?>-tab">
                  <div class="list-tour_v1">
                         <?php  foreach($rs_sub_tour as $sub_tour){?>
                         <div class="item"> 
                            <a href="<?=$sub_tour->slug.'.html';?>" class="avt"> <img src="<?=base_url('uploads/'.$sub_tour->category_icon)?>" alt="<?=$sub_tour->category_name;?>">
                              <div class="overlay-item"></div> <span><?=$sub_tour->category_name;?></span>
                          </a> 
                        </div><!--End item-->
                        <?php }?>
                          </div><!--End ist-tour-->
            </div><!--End tab-pane-->
            <?php }?>
            
            
          </div>
      </div><!--End tabs-select-travel-->
    </form>
</div>
<!--End vnt-search-->
</div><!--end form-search-tour-->

</div><!--end search-tour-->

