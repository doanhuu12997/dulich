<!--=== BEGIN: CẢM NHẬN KHÁCH HÀNG ===-->
<div class="vnt-feelling wrap">
  <div class="wrap-header">
      <h2 class="wrap-title">Đánh giá từ khách hàng</h2>
      <p class="wrap-desc">Hạnh phúc của Khách hàng là sự thành công lớn nhất của chúng tôi</p>
  </div>
        <div class="slider-feelling">
            <div id="slider-feelling">
              <?php $get_feel =$this->web_model->get_all_feel_post();
              foreach ($get_feel as $key ) {?>
                <div class="item col-md-12">
                   <div class="i-image"><a href="<?=$key->feel_link;?>" title="<?=$key->feel_title;?>"><img src="<?=base_url() ?>uploads/<?=$key->feel_image ?>" alt="<?=$key->feel_title;?>"></a></div>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <div class="i-name"><span><?=$key->feel_title;?></span></div>
                    <div class="i-content"><?=$key->feel_des;?> </div>
                </div>
            <?php } ?>
        </div>
</div>
</div>
            <!--=== END: CẢM NHẬN KHÁCH HÀNG ===-->