
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="vi" xml:lang="vi">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta name="keywords" content="<?=$get_single_product->product_keywords?>" />
  <meta name="description" content="<?=$get_single_product->product_tag?>" />
  <title><?=$get_single_product->product_title_bar?></title>
  <meta content="index,follow" name="googlebot" />
  <meta name="copyright" content="<?=get_option('site_copyright')?>" />
  <?php echo get_option('metageo')?>
  <?php echo get_option('schema_content')?>

  <!-- DÙNG CHUNG CHO TÒAN SITE -->
<meta name="DC.title" lang="vi" content="<?=$get_single_product->product_tag?>" />
  <meta name="DC.creator" content="Thiết kế web chuyên nghiệp - Buffseo" />
  <meta name="DCTERMS.issued" scheme="DCTERMS.W3CDTF" content="<?php date_default_timezone_set('Asia/Ho_Chi_Minh'); echo date('l, d M Y') ;  echo date(" h:i:sa")?>" />
  <meta name="DC.identifier" scheme="DCTERMS.URI" content="<?=base_url($get_single_product->slug.'/'.$get_single_product->product_slug).'.html';?>" />
  <link rel="DCTERMS.replaces" hreflang="vi" href="<?=base_url($get_single_product->product_slug).'.html';?>" />
  <meta name="DCTERMS.abstract" content="<?=$get_single_product->product_tag?>" />
  <meta name="DC.format" scheme="DCTERMS.IMT" content="text/html" />
  <meta name="DC.type" scheme="DCTERMS.DCMIType" content="Text" />
  <meta property="og:title" content="<?=$get_single_product->product_title?>" />
  <meta property="og:description" content="<?=$get_single_product->product_tag?>" />
  <meta property="og:type" content="article" />
  <meta property="og:image" content="<?=base_url('uploads/product/'.$get_single_product->product_image)?>" />
  <meta property="og:url" content="<?=base_url($get_single_product->product_slug).'.html';?>" />
  <link rel="canonical" href="<?php if(empty($get_single_product->product_slug)){} else echo base_url($get_single_product->product_slug).'.html';?>"/>
  <meta name="twitter:card" content="summary" />
  <meta name="twitter:site" content="<?=get_option('site_twitter_link')?>">
  <meta name="twitter:title" content="<?=$get_single_product->product_title_bar?>">
  <meta name="twitter:description" content="<?=$get_single_product->product_tag?>">
  <link rel="shortcut icon" href="/<?=get_option('site_favicon') ?>" type="image/x-icon">
  <link rel="icon" href="/<?=get_option('site_favicon') ?>" type="image/x-icon">

  <?php
  $rs_count =$this->web_model->rs_count_total_star($get_single_product->product_id);

  $rs_comment_star =$this->web_model->rs_comment_star($get_single_product->product_id);
  if($rs_comment_star->sum=="" || $rs_count->count==0) { echo "";}
  else {
    $tatol_star =$rs_comment_star->sum;
    $tatol_people =$rs_count->count;
    $tatol=$tatol_star/$tatol_people;
  }
  $rs_start =$this->web_model->show_result_star($get_single_product->product_id);
  ?>

   <?php  $query = $this->db->query("select * from tbl_brand where brand_publication_status =1 and brand_id ='".$get_single_product->product_brand."' ");
              $rs_brand= $query->row(); ?>
  <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Product",
        "description": "<?php echo $get_single_product->product_tag?>",
        "name": "<?php echo $get_single_product->product_title;?>",
        "image": "<?php echo $get_single_product->product_image;?>",
        "sku" : "<?=$get_single_product->product_id;?>",
        "gtin8" : "30",
        "brand": {
            "@type": "Brand",
            "name": "<?php if(empty($rs_brand)){echo 'Brand';}else{echo $rs_brand->brand_name;}?>"
          },
        "offers": 
        {
          "@type": "Offer",
          "url": "<?php echo base_url($get_single_product->product_slug).'.html';?>",
          "availability": "http://schema.org/InStock",
          "price": "<?php echo floatValue($get_single_product->product_price);?>",
          "priceCurrency": "VND",
          "priceValidUntil": "2020-11-20"
        },
       "review": {
        "@type": "Review",
          "reviewRating": {
            "@type": "Rating",
            "ratingValue": "4",
            "bestRating": "5"
          },
          "author": {
            "@type": "Person",
            "name": "Admin"
          }
        },
        "aggregateRating": {
          "@type": "AggregateRating",
          "ratingValue": "4.4",
          "reviewCount": "1"
        }
      }
      
    </script>