
 <div class="clear"></div>
     <section class="section section-gallery">
        <div class="container">
            <div class="list-gallery">
                <button class="arrow-prev left">
                    <svg width="40px" height="60px" viewBox="0 0 50 80" xml:space="preserve">
                        <polyline fill="none" stroke="#FFFFFF" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="
                    45.63,75.8 0.375,38.087 45.63,0.375 "/>
                    </svg>  
                </button>
                <button class="arrow-next right">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="40px" height="60px" viewBox="0 0 50 80" xml:space="preserve">
                    <polyline fill="none" stroke="#FFFFFF" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" points="
                    0.375,0.375 45.63,38.087 0.375,75.8 "/>
                    </svg>
                </button>
                <ul id="i_gallery">
                    <?php 
                    $camnhan = $this->web_model->get_id_post(59);
                    foreach($camnhan as $v){ ?>
                    <li class="item col-md-2">
                        <div class="gallery_item">
                            <picture><img src="<?php echo base_url() ?>uploads/news/<?php echo $v->dmtin_image  ?>" alt="<?php echo $v->dmtin_title  ;?>" title="<?php echo $v->dmtin_title  ?>">
                            </picture>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </section>
   <div class="clear"></div>
