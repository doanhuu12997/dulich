<?= get_option('google_analytics_body_bottom');

$rs_id = $this->web_model->rs_id();

$id_dm_about = $rs_id->id_dm_about;

$about_home = $this->web_model->get_all_one_news_post_home($id_dm_about);

$vp_amara = $this->web_model->get_all_one_news_post_home(71);

$tt_amara = $this->web_model->get_all_one_news_post_home(72);



?>



<!-- start Footer Wrapper -->

<footer id="footer" class="footer footer-wrapper scrollspy-stopper ">

    <div class="container">

        <div class="row">

            <div class="col-12 col-lg-4 col-md-4  office-head go-right">

                <div class="block-footer ">

                    <h4 class="title">AMARA</h4>

                    <div class="content">

                        <?php echo get_option('contact_description'); ?>

                    </div>

                </div>

                </ul>

            </div>

            <div class="col-12 col-lg-4 col-md-4  office-head go-right">

                <div class="block-footer ">

                    <h4 class="title">LIÊN HỆ AMARA </h4>

                    <div class="content">

                        <?= $vp_amara->one_news_long; ?>

                    </div>

                </div>

            </div>

            <div class="col-12 col-lg-4 col-md-4 social-head  " style="">

                <div class="block-footer ">

                    <h4 class="title">Kết nối với AMARA</h4>

                    <div class="content">

                        <ul class="navbar-social">

                            <?php $query = $this->db->query("select * from tbl_option ");

                            $rs_show_link = $query->row(); ?>

                            <?php if ($rs_show_link->show_link_facebook == 1) { ?>

                                <li class="social">

                                    <a href="https://www.facebook.com/<?= get_option('site_facebook_link'); ?>" rel="nofollow" target="_blank">

                                        <img src="<?= base_url() ?>assets/web/images/icons-facebook.svg" alt="">

                                    </a>

                                </li>

                            <?php } else {

                                echo "";
                            } ?>



                            <?php if ($rs_show_link->show_youtube == 1) { ?>



                                <li>

                                    <a href="<?= get_option('site_youtube'); ?>" rel="nofollow" target="_blank">

                                        <img src="<?= base_url() ?>assets/web/images/icons-youtube.svg" alt="">



                                    </a>

                                </li>

                            <?php } else {

                                echo "";
                            } ?>

                            <?php if ($rs_show_link->show_link_google_plus == 1) { ?>



                                <li><a href="mailto:<?= get_option('site_google_plus_link'); ?>" rel="nofollow"> <img src="<?= base_url() ?>assets/web/images/icons-google-plus.svg" alt=""></a>

                                </li>

                            <?php } else {

                                echo "";
                            } ?>

                            <?php if ($rs_show_link->show_whatapp == 1) { ?>

                                <li><a href="https://api.whatsapp.com/send?phone=<?= get_option('company_whatapp'); ?>" rel="nofollow" target="_blank"> <img src="<?= base_url() ?>assets/web/images/icons-whatsapp.svg" alt=""></a>

                                </li>

                            <?php } else {

                                echo "";
                            } ?>

                            <?php if ($rs_show_link->show_telegram == 1) { ?>

                                <li><a href="https://t.me/<?= get_option('company_telegram'); ?>" rel="nofollow" target="_blank"><img src="<?= base_url() ?>assets/web/images/icons-telegram.svg" alt=""></a>

                                </li>

                            <?php } else {

                                echo "";
                            } ?>

                            <?php if ($rs_show_link->show_skype == 1) { ?>

                                <li><a href="skype:<?= get_option('company_skype'); ?>?chat" rel="nofollow"><img src="<?= base_url() ?>assets/web/images/icons-skype.svg" alt=""></a>

                                </li>

                            <?php } else {

                                echo "";
                            } ?>



                        </ul>

                        <div class="item-newsletter">

                            <div class="clear"></div>

                            <p>Đăng ký nhận tin khuyến mãi</p>

                            <div class="block newsletter">

                                <div class="content">

                                    <form action="dang-ky-nhan-mail.html" method="post" class="form subscribe space_bottom_10">

                                        <div class="field newsletter">

                                            <div class="control">

                                                <input name="email" type="email" placeholder="Email của bạn" data-validate="{required:true, 'validate-email':true}" class="form-control">

                                                <!-- <label class="radio-inline">

                                                <input type="radio" name="regiter_gioitinh" value="Nam" checked="checked">Nam

                                                </label> 

                                                <label class="radio-inline">

                                                <input type="radio" name="regiter_gioitinh" value="Nữ">Nữ

                                                </label>  -->

                                            </div>

                                        </div>

                                        <div class="actions">

                                            <button class="btn btn-secondary" type="submit"> <span>Đăng

                                                    ký</span></button>

                                        </div>

                                    </form>

                                </div>

                            </div>



                        </div>

                        <!--End item-newletter-->

                    </div>

                </div>

            </div>

        </div>

        <div class="row">



            <div class="col-12 col-lg-4 col-md-4  office-head go-right">

                <div class="block-footer ">

                    <h4 class="title">CHÙM TOUR HOT</h4>

                    <div class="content">

                        <ul class="news">

                            <?php

                            $product_selling = $this->web_model->product_selling();

                            foreach ($product_selling as $tour_hot) { ?>

                                <li>

                                    <a href="<?= base_url($tour_hot->product_slug) . '.html'; ?>">

                                        <div class="news-releas">

                                            <div class="image"><img src="<?= base_url('uploads/product/thumb/' . $tour_hot->product_image) ?>" alt="<?= $tour_hot->product_title; ?>" title="<?= $tour_hot->product_title; ?>" /></div>

                                            <div class="content">

                                                <p><?= word_limiter($tour_hot->product_title, 15); ?></p>

                                            </div>

                                        </div>

                                    </a>

                                </li>

                            <?php  } ?>

                        </ul>

                    </div>

                    <!--End content-->

                </div>

            </div>



            <div class="col-12 col-lg-4 col-md-4  office-head go-right">

                <div class="block-footer ">

                    <h4 class="title">Bản đồ</h4>

                    <div class="map-responsive">

                        <?= get_option('company_location'); ?>

                    </div>

                </div>

            </div>





            <div class="col-12 col-lg-4 col-md-4  office-head go-right">

                <div class="block-footer ">

                    <h4 class="title">FANPAGE DU LỊCH AMARA </h4>

                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fdulichamara%2F%3Fref%3Dembed_page&amp;tabs=messages&amp;width=340&amp;height=400&amp;small_header=false&amp;adapt_container_width=false&amp;hide_cover=false&amp;show_facepile=false&amp;appId" width="100%" height="300" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>

                </div>

            </div>

        </div>

        <!-- ********************   Removing powered by linkback will result to cancellation of your support service.    ********************  -->

        <div class="hidden-xs hidden-sm" style="padding:10px;position:relative">

            <div class="container"> </div>

        </div>

        <!-- ********************   Removing powered by linkback will result to cancellation of your support service.    ********************  -->

    </div>

    <div class="post-footer">

        <div class="container">

            <p class="footer-copy-right">Copyright © 2023 <?= get_option('contact_title'); ?> Designed by Buffseo</p>

        </div>

    </div>

</footer>









<div class="fixedposicon">

    <div class="support-hotline">

        <div class="div_title">

            <span class="icon"><i class="fa fa-whatsapp"></i></span>

            <!-- <span class="text">Hotline</span> -->

        </div>

        <div class="div_content">

            <div class="title_hotline">Hotline:</div>

            <div class="number_phone"><a href="tel:<?= get_option('company_number'); ?>">

                    <?= get_option('company_number'); ?> </a></div>

        </div>

    </div>

    <a href="https://zalo.me/<?= get_option('company_hotline'); ?>" class="btnzalo" target="_blank">

        <img src="<?= base_url() ?>/assets/web/images/icon/widget_icon_zalo.svg" alt="chat zalo" class="alignnone">

        <span class="label">Chat với chúng tôi qua zalo </span>

    </a>

    <a href="https://m.me/<?= get_option('site_facebook_link'); ?>" class="btnfacebook" target="_blank">

        <img src="<?= base_url() ?>assets/web/images/facebook-messenger.svg" alt="chat messenger">

        <span class="label">Chat bằng facebook messenger</span>

    </a>

</div>

<div id="bttop" href="#">

    <i class="fa fa-arrow-up" aria-hidden="true"></i>

</div>





<?php

$now = time();

$ip = $_SERVER['REMOTE_ADDR'];

$data['id'] = '';

$newformat = date('Y-m-d', $now);

$data['tm'] = $newformat;

$data['ip'] = $ip;

$this->db->insert('tbl_counter', $data);

?>



<!--Start of Tawk.to Script-->

<script type="text/javascript">
    var Tawk_API = Tawk_API || {},

        Tawk_LoadStart = new Date();

    (function() {

        var s1 = document.createElement("script"),

            s0 = document.getElementsByTagName("script")[0];

        s1.async = true;

        s1.src = 'https://embed.tawk.to/64a6366bcc26a871b026a2a4/1h4kk95rb';

        s1.charset = 'UTF-8';

        s1.setAttribute('crossorigin', '*');

        s0.parentNode.insertBefore(s1, s0);

    })();
</script>

<!--End of Tawk.to Script-->

</body>



</html>