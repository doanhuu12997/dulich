<!-- DÙNG CHUNG CHO TÒAN SITE -->
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=vietnamese" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
<link rel="stylesheet" href="<?= base_url() ?>assets/web/bootstrap-3.3.7-dist/css/bootstrap.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/web/css/slick.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/web/css/slick-theme.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/web/css/style.css">
</head>
<body>
    <script type="text/javascript" src="<?= base_url() ?>assets/web/js/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/web/js/style.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link href="<?= base_url() ?>assets/web/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <div class="result"><?= $this->session->flashdata('message'); ?></div>
    <!--===MODULE MAIN==-->
    <link href="<?= base_url() ?>assets/web/js/slideSlick/css/slick-theme.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript" src="<?= base_url() ?>assets/web/js/slick.min.js"></script>
    <div id="PageOver"></div>
    <?php get_option('google_analytics_body'); ?>
    <div id="vnt-wrapper">
        <div id="vnt-container">
            <div class="head-top-menu">
                <div class="container-fluid">
                    <div class="row">
                        <div class="vnt-logo col-md-7 col-xs-12 col-sm-4">
                            <div class="info_menu">
                                <ul>
                                    <li class="hotline__top"><a><?= get_option('khauhieu'); ?><i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                        <?php if (!empty($all_hotline)) { ?>
                                            <div class="list__hotsline ">
                                                <?php foreach ($all_hotline as $ht) { ?>
                                                    <div class="hotline_item"><span class="name"><?= $ht->name ?></span> <span class="phone"><?= $ht->phone ?></span></div>
                                                <?php } ?>
                                            </div>
                                        <?php  } else { ?>
                                            <div></div>
                                        <?php } ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="clear"></div>
                            <div class="i-logo">
                                <a class="img_home" href="<?= base_url('/'); ?>"><img alt="<?= get_option('contact_title'); ?>" src="<?= base_url('uploads/');  ?><?= get_option('site_logo'); ?>"></a>
                            </div>
                            <!--End ilogo-->
                            <div class="title-h1-home">
                                <h1 style="   margin-bottom: 5px; display: inline-block;   ">
                                    <?= get_option('contact_title'); ?></h1>
                            </div>
                            <div class="slogan-home">
                                <p style="color:#F58634 ;font-size: 18px;font-style:italic;margin-left: 80px;text-align:center;    display: inline-block;">
                                    <?= get_option('site_slogan'); ?></p>
                            </div>
                        </div>
                        <!--end  logo-->
                        <div class="col-md-5 col-xs-12 col-sm-8">
                            <div class="info_hs">
                                <ul>
                                    <li><a href=""><i style="color:#EF6A25" class="fa fa-clock-o"></i> Lịch khởi hành
                                        </a></li>
                                    <li><a href=""><i class="fa fa-address-card-o"></i> Hồ sơ năng lực </a></li>
                                    <li><a href="/en"><i class="flag-english"></i> English </a></li>
                                </ul>
                            </div>
                            <div class="clear"></div>
                            <div class="info_menu_right">
                                <ul class="cate">
                                    <li><a href="<?= base_url('gioi-thieu' . '.html') ?>">Giới thiệu </a></li>
                                    <!-- <li><a href="<?= base_url('lien-he' . '.html') ?>">Liên hệ </a></li> -->
                                    <li><a href="<?= base_url('tin-tuc-su-kien' . '.html') ?>">Tin Tức </a></li>
                                    <li><a href="<?= base_url('tuyen-dung' . '.html') ?>">Tuyển dụng</a></li>
                                    <li><a href="<?= base_url('chinh-sach-thanh-toan' . '.html') ?>">Chinh sách thanh
                                            toán </a></li>
                                </ul>
                                <div class="info_left" style="display:flex;align-items: center;">
                                    <div class="wap-login">
                                        <div class="btn-group show-on-hover vnt-login ">
                                            <?php
                                            $customer_id = $this->session->userdata('customer_id');
                                            $query = $this->db->query("select * from tbl_customer where  customer_id='" . $customer_id . "'");
                                            $rs_member = $query->row();
                                            if (isset($rs_member)) : ?><button type="button" class="btn bnt-login dropdown-toggle" data-toggle="dropdown">
                                                    <?php if (empty($rs_member->customer_avatar)) {
                                                        echo "";
                                                    } else { ?>
                                                        <img class="media-object dp img-circle" src="<?php echo base_url('uploads/' . $rs_member->customer_avatar); ?>">
                                                    <?php } ?>
                                                    <div class="name_login"><?php echo $rs_member->customer_name; ?><span class="caret"></span></div>
                                                </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="<?php echo base_url("thong-tin-tai-khoan.html"); ?>">Thông
                                                            tin tài
                                                            khoản</a></li>
                                                    <li><a href="<?php echo base_url("lich-su-mua-hang.html"); ?>">Lịch sử
                                                            mua
                                                            hàng</a></li>
                                                    <li><a href="<?php echo base_url("danh-sach-theo-doi.html"); ?>">Danh
                                                            sách theo
                                                            dõi</a></li>
                                                    <li><a href="<?php echo base_url('thoat.html'); ?>">Thoát</a></li>
                                                </ul>
                                            <?php else : ?><button type="button" class="btn bnt-login dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> </button>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li><a href="<?php echo base_url('dang-nhap.html'); ?>">Đăng nhập</a>
                                                    </li>
                                                    <li><a href="<?php echo base_url('dang-ky.html'); ?>">Đăng ký</a></li>
                                                </ul>
                                            <?php endif; ?>
                                        </div>
                                        <!--end vnt-login-->
                                    </div>
                                    <!--End wap-login-->
                                    <div class="cart_moblie">
                                        <div class="cart-title"><a href="<?= base_url('gio-hang.html'); ?>"><i class="fa fa-shopping-cart"></i></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End md-8-->
                    </div>
                </div>
                <!--End container-->
            </div>
            <!--End head-top-menu-->
            <div class="wap-header">
                <div class="header_main">
                    <div class="container">
                        <div class="col-md-2 col-sm-12 col-xs-12">
                            <div class="menu_mobile">
                                <div class="icon_menu"><span class="style_icon"></span></div>
                                <div class="divmm">
                                    <div class="mmContent">
                                        <div class="mmMenu">
                                            <div class="logo-m">
                                                <a class="img_home" href="<?= base_url('/'); ?>"><img alt="<?= get_option('contact_title'); ?>" src="<?= base_url('uploads/');  ?><?= get_option('site_logo'); ?>"></a>
                                            </div>
                                            <div class="clear"></div>
                                            <?php $id_top = 65;
                                            $infor_top_left = $this->web_model->get_all_one_news_post_home($id_top);
                                            echo $infor_top_left->one_news_long; ?>
                                            <div class="clear"></div>
                                            <div class="vnt-search-mobile">
                                                <div class="formSearch">
                                                    <form method="get" action="tim-kiem.html" class="box_search">
                                                        <div id="au-mobile">
                                                            <input name="search" type="text" autocomplete="off" id="searchs" class="text_search form-control" placeholder="Tìm kiếm...">
                                                            <span class="input-group-btn">
                                                                <button type="submit" class="btn">
                                                                    <i class="fa fa-search"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <!--END autocomplete-->
                                                        <div class="clear"></div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!--End vnt-search-moblie-->
                                            <ul class="set_menu"></ul>
                                        </div>
                                        <div class="close-mmenu"></div>
                                    </div>
                                    <div class="divmmbg"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <!--End menu_mobile-->
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <!--end header_main container-->
                <div class="header_menu_bottom">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="vnt-menutop col-md-12 col-xs-12 col-sm-12">
                                <ul class="get_menu">
                                    <li><a href="<?= base_url('/'); ?>"><i class="fa fa-home"></i></a> </li>
                                    <?php foreach ($get_all_category_show_menu as $menu) {
                                        $rs_2 = $this->web_model->sub_category($menu->id); ?>
                                        <li><a href="<?= base_url($menu->slug) . '.html'; ?>"> <?= $menu->category_name ?>
                                                <?php if (empty($rs_2)) {
                                                    echo "";
                                                } else { ?><i class="fa fa-angle-down"></i><?php } ?></a>
                                            <ul class="tutuc">
                                                <?php foreach ($rs_2 as $c2) {
                                                    $rs_3 = $this->web_model->sub_category($c2->id); ?>
                                                    <li><a href="<?= $c2->slug . '.html'; ?>"><b>
                                                                <?= $c2->category_name ?></b></a>
                                                        <?php if ($menu->id == 468) {
                                                            echo '';
                                                        } else { ?>
                                                            <ul class="sub_3">
                                                                <?php foreach ($rs_3 as $c3) { ?>
                                                                    <li><a href="<?= base_url($c3->slug) . '.html'; ?>">
                                                                            <?= $c3->category_name ?></a></li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php } ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                    <?php foreach ($get_one_news as $v) { ?>
                                        <li><a href="<?= base_url($v->one_news_slug) . '.html'; ?>"><?= $v->one_news_title ?></a>
                                        </li>
                                    <?php } ?>
                                    <?php
                                    $show_tem = $this->web_model->cate_tembulding_show_menu();
                                    foreach ($show_tem as $tem) {
                                        $rs_c2_tem = $this->web_model->get_child_menu_news_write($tem->id); ?>
                                        <li><a href="<?= base_url($tem->slug) . '.html'; ?>"><?= $tem->category_name ?>
                                                <?php if (empty($rs_c2_tem)) {
                                                    echo "";
                                                } else { ?><i class="fa fa-angle-down"></i><?php } ?></a>
                                            <ul class="tutuc">
                                                <?php foreach ($rs_c2_tem as $ab) { ?>
                                                    <li><a href="<?= base_url($ab->dmtin_slug) . '.html'; ?>">
                                                            <?= $ab->dmtin_title ?></a> </li>
                                                <?php } ?>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                    <?php
                                    $show_visa_mayb = $this->web_model->cate_visa_vemaybay_show_menu();
                                    foreach ($show_visa_mayb as $mayb) {
                                        $rs_c2_mayb = $this->web_model->get_child_menu_news_write($mayb->id); ?>
                                        <li><a href="<?= base_url($mayb->slug) . '.html'; ?>"><?= $mayb->category_name ?>
                                                <?php if (empty($rs_c2_mayb)) {
                                                    echo "";
                                                } else { ?><i class="fa fa-angle-down"></i><?php } ?></a>
                                            <ul class="tutuc">
                                                <?php foreach ($rs_c2_mayb as $may_c2) { ?>
                                                    <li><a href="<?= base_url($may_c2->dmtin_slug) . '.html'; ?>">
                                                            <?= $may_c2->dmtin_title ?></a> </li>
                                                <?php } ?>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                    <?php
                                    foreach ($show_menu_dmtin as $menu_news) {
                                        $rs_c2 = $this->web_model->get_child_menu_news($menu_news->id); ?>
                                        <li><a href="<?= base_url($menu_news->slug) . '.html'; ?>"><?= $menu_news->category_name ?>
                                                <?php if (empty($rs_c2)) {
                                                    echo "";
                                                } else { ?><i class="fa fa-angle-down"></i><?php } ?></a>
                                            <ul class="tutuc">
                                                <?php foreach ($rs_c2 as $c2) { ?>
                                                    <li><a href="<?= base_url($c2->slug) . '.html'; ?>">
                                                            <?= $c2->category_name ?></a> </li>
                                                <?php } ?>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                    <li><a href="combo_khachsan.html">Combo Khách Sạn</a>
                                    </li>
                                    <!-- <li><a  href="<?= base_url('lien-he') . '.html'; ?>">Liên hệ</a> </li> -->
                                </ul>
                                <div class="clear"></div>
                            </div>
                            <!--END MENU-->
                        </div>
                    </div>
                </div>
                <!--End headmenu bootom-->
            </div>
            <!--End wap-header-->