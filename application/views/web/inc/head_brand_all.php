
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="vi" xml:lang="vi">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>THƯƠNG HIỆU ĐƯỢC YÊU THÍCH </title>
    <meta name="description" content="Các thương hiệu  được yêu thích hiện nay"/>
    <meta name="keywords" content="thuong hieu"/>
    <meta content="index,follow" name="googlebot" />
    <meta name="copyright" content="<?=get_option('site_copyright')?>" />
    <meta name="robots" content="INDEX,FOLLOW"/>
    <?=get_option('metageo')?>
    <?=get_option('schema_content')?>

    <meta name="DC.title" lang="vi" content="THƯƠNG HIỆU  ĐƯỢC YÊU THÍCH" />
    <meta name="DC.creator" content="Thiết kế web chuyên nghiệp - Buffseo" />
    <meta name="DCTERMS.issued" scheme="DCTERMS.W3CDTF" content="<?php date_default_timezone_set('Asia/Ho_Chi_Minh'); echo date('l, d M Y') ;  echo date(" h:i:sa")?>" />
    <meta name="DC.identifier" scheme="DCTERMS.URI" content="<?=base_url()?>" />
    <link rel="DCTERMS.replaces" hreflang="vi" href="<?=base_url()?>" />
    <meta name="DCTERMS.abstract" content="<?=get_option('contact_subtitle')?>" />
    <meta name="DC.format" scheme="DCTERMS.IMT" content="text/html" />
    <meta name="DC.type" scheme="DCTERMS.DCMIType" content="Text" />
    <meta property="og:title" content="THƯƠNG HIỆU  ĐƯỢC YÊU THÍCH" />
    <meta property="og:description" content="Các thương hiệu được yêu thích hiện nay" />
    <meta property="og:type" content="article" />
    <?php foreach ($get_slider as $key => $v) { ?>
    <?php if($key==0) { ?><meta property="og:image" content="<?=base_url() ?>uploads/<?=$v->slider_image ?>" />  
    <?php  }?>
    <?php }?>
    <meta property="og:url" content="<?=base_url()?>" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="<?=get_option('site_twitter_link')?>">
    <meta name="twitter:title" content="THƯƠNG HIỆU ĐƯỢC YÊU THÍCH ">
    <meta name="twitter:description" content="Các thương hiệu được yêu thích hiện nay">
    <link rel="shortcut icon" href="<?=get_option('site_favicon') ?>" type="image/x-icon">
    <link rel="icon" href="<?=get_option('site_favicon') ?>" type="image/x-icon">