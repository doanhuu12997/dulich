

<link href="<?=base_url() ?>assets/web/css/tour_cate.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?=base_url() ?>assets/web/css/box.css" >
<!--=== BEGIN: CONTENT ===-->
<div id="vnt-content">
<div class="container ">
  <div class="mod-content ">
   <!--===BEGIN: BOX MAIN===-->

   <div class="box_mid">
    <div class="mid-content">
    <div class="main-content ">
           
            <div class="row">

                <div class="col-lg-8">
                    <div class="tab-tours">
                        <div class="tab-content shadow" id="nav-tabContent">
                        <div class="tab-pane  show active" id="nav-248" role="tabpanel" aria-labelledby="nav-248-tab">
                  <h5 class='title-sub-cate'><a ><i class='fa fa-map-marker'></i> Kết quả tìm kiếm : <?=$search;?></a></h5>
                  <ul class="list-tour-01">
                  <?php foreach ($get_all_product as $product) {  
                    $rs_dxp= $this->web_model->get_xuatphat($product->product_xuatphat);
                    ?>
                      <li>
                        <div class="item row">
                            <div class="item-left col-md-4">
                                <a class="hvr-bounce-to-right shadow" href="<?=base_url($product->product_slug).'.html';?>">
                                <img src="<?=base_url('uploads/product/thumb/'.$product->product_image)?>" alt="<?=$product->product_title;?>" title="<?=$product->product_title;?>"/>
                              </a>
                           
                                </div>
                            <div class="item-right col-md-8">
                              <h3 class="title">
                              <a href="<?=base_url($product->product_slug).'.html';?>"><?=$product->product_title;?></a>
                              </h3>
                              <ul class="meta">
                                  <li>
                                    <i class="fa fa-clock-o"></i>Thời gian: <?=$product->product_thoigian;?>
                                  </li>
                                  <li>
                                    <i class="fa fa-calendar"></i>Khởi hành: 
                                    <span class="schedule">  
                                  <?php
                                  $get_schedule = $this->web_model->get_depart_schedule_result($product->product_id);
                                        foreach($get_schedule as $key => $v_sche )
                                        {?>
                                      <a class='btn vt-sumbit'><?php if($v_sche->date_begin==''){echo '';}else{ echo nice_date($v_sche->date_begin,'d/m'); }?></a>
                                      
                                  <?php  }?>

                                    </span>
                                  </li>
                                  <li> <i class="fa fa-plane"></i>Phương tiện: <?=$product->product_phuongtien;?> </li>
                              </ul>
                              <div class="i-promotion">
                                  <?php if(floatValue($product->product_price_sale)==0) { echo "";} else {?>
                                    <span><?php echo $product->product_price_sale; ?>₫</span>
                                  <?php }?>
                                  <?php if(floatValue($product->product_price)==0){echo "<span class='call_me'>Liên hệ </span>";} else if(floatValue($product->product_price_sale) ==0){echo ' <span>'. $product->product_price .' ₫' .'</span>';}else{ echo '<div class=i-price>' .  $product->product_price .'₫' ; ?></div>
                                <?php } ?>
                            </div><!--end i-promtion-->
                              <a href="<?=base_url($product->product_slug).'.html';?>" class="btn btn-view-detail"> Xem chi tiết </a>
                              <form  action="<?=base_url('save/cart');?>" method="post" class="form_tour">
                                  <input type="hidden" name="qty"  value="1"  />
                                  <input type="hidden" name="product_id" value="<?=$product->product_id?>"  />
                                  <input type="hidden" name="tour_xp"  value="<?=$rs_dxp->xuatxu_name;?>"  />
                                  <input type="hidden" name="tour_time"  value="<?=$product->product_thoigian;?>"  />
                                  <input type="hidden" name="tour_pt"  value="<?=$product->product_phuongtien;?>"  />
                                  <button type="submit" class="btn btn-booking" name="submit">ĐẶT TOUR</button>  
                              </form>
                            </div>
                        </div>
                      </li>
                      <?php }?>
                  </ul>
                </div>
                <!--EnD tab-pane-->
        
              </div><!--End tab-content-->
      </div><!--End tab-tours-->
      <div class="clear"></div>
                                                     
</div><!--End md8-->
        <div class="col-lg-4 siderbar">
          <div class="widget news">
              <div class="widget-title">
                  <h4>Tin tức</h4>
              </div>
              <div class="widget-content shadow">
              <?php
                $id_dm_news=86;
                $limit_news=5;
                $order_news="asc";
                $get_news_right = $this->web_model->get_news_top_home($id_dm_news,$limit_news,$order_news);
                 foreach ($get_news_right as $news) { ?>
                  <div class="single-blog-post d-flex">
                      <div class="post-thumbnail shadow"> <a href="<?=base_url($news->dmtin_slug).'.html';?>">
                      <img src="<?=base_url('uploads/news/'.$news->dmtin_image)?>" alt="<?=$news->dmtin_title ?>" title="<?=$news->dmtin_title ?>"></a> </div>
                      <div class="post-content">
                          <h3> <a href="<?=base_url($news->dmtin_slug).'.html';?>" class="post-title"><?=$news->dmtin_title ?></a></h3>
                          <div class="single-blog-bottom"> <a href="<?=base_url($news->dmtin_slug).'.html';?>" class="btn btn-view-detail"> Xem chi tiết </a> </div>
                      </div>
                  </div>
                   <?php }?>
                  <!--End single-blog-post-->
              </div>
          </div>

          <div class="clear"></div>
          <!--End md-4-->
                        
                    
        </div>
                </div><!--End main-content-->

    <div class="clear"></div>
 
</div>
</div>
</div>
</div>

<!--===END: BOX MAIN===-->
</div>
<div class="clear"></div>
</div>
</div>
<!--=== END: CONTENT ===-->
