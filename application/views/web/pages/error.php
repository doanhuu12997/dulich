

<div class="main">
    <div class="content">
        <div class="error">	
        <img src="https://css-ak.static-adayroi.com/_ui/responsive/theme-alpha/images/404_v1.png" alt="">	
            <h2>404 Not Found</h2>
            <p>Tiếc quá, trang bạn muốn xem không tồn tại!</p>
            <p>Hãy thử sử dụng các tính năng bên dưới để tiếp tục nhé</p>
        </div>  	
          <div class="col-sm-12 col-md-12 col-xs-12">
                <div class="block-404-navigation">
                    <a href="/">
                        <div class="btn__round__icon"><i class="adr-icon icon-red-home"></i></div><span>Trang chủ</span></a>
                    <a href="lien-he.html">
                        <div class="btn__round__icon"><i class="adr-icon icon-phone-2"></i></div><span>Liên hệ</span></a>
                </div>
            </div>
        <div class="clear"></div>
       

    </div>
</div>
<style>
    .error{ text-align: center;}
    .error h2{color:red;text-align: center;font-size: 50px}
   .error p {
    color: #000;
    text-align: center;
    font-size: 14px;
    line-height: 24px;
    margin-bottom: 0px;
}
 .block-404-navigation {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-pack: center;
    justify-content: center;
    margin-top: 10px;
}
 .block-404-navigation>a {
    margin: 16px;
    text-align: center;
}
 .block-404-navigation .btn__round__icon {
    width: 64px;
    height: 64px;
    border-radius: 50%;
    background: #e8eaee;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-pack: center;
    justify-content: center;
    -ms-flex-align: center;
    align-items: center;
    margin: 0 auto;
    margin-bottom: 12px;
}
 .block-404-navigation .btn__round__icon .adr-icon {
    width: 20px;
    height: 20px;
    font-size: 20px;
    margin-bottom: 0;
    color: #333;
    font-style: initial;
}
.adr-icon.icon-red-home:before {
    content: "\f015";
    font-family: fontawesome;
    color: #000;
}

.adr-icon.icon-phone-2:before {
    content: "\f095";
    font-family: fontawesome;
    color: #000;
}
.block-404-navigation>a span
{
    color: #000;
}
</style>