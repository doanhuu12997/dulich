

    <!--===MODULE MAIN==-->
    <link href="<?=base_url() ?>assets/web/css/about.css" rel="stylesheet" type="text/css" />
    <!--===MODULE MAIN==-->  

  <!--=== BEGIN: CONTENT ===-->
        <div id="vnt-content one_news">
            <div class="wrapper container">
                <div class="mod-content row">
                    <div  class="col-md-12 col-xs-12 col-sm-12">
                        <!--===BEGIN: BOX MAIN===-->
                        <div class="box_mid">
                            <div class="mid-title">
                                <div class="titleR"></div>
                                <div class="clear"></div>
                            </div>
                            <div class="mid-content">
                                <div class="desc-onenews">
                                     <!--===BEGIN: BREADCRUMB===-->
                            <div id="vnt-navation" class="breadcrumb">
                                    <div class="navation">
                                        <ul>
                                            <li><a href="/">Trang chủ</a></li>
                                            <li><a href="<?=base_url($get_single_one_news->one_news_slug).'.html';?>"><?=$get_single_one_news->one_news_title?></a></li>
                                        </ul>
                                    </div>
                            </div>
                            <!--===END: BREADCRUMB===-->
                                    <?=$get_single_one_news->one_news_long?>
                                     </div>
                                </div>
                        <!--===END: BOX MAIN===-->
                    </div>
                    <div class="clear"></div>
                </div><!--end md9-->
                 
            </div>
        </div>
        <!--=== END: CONTENT ===-->


<script>
$(document).ready(function() {
   
  $('.accordion ').click(function() {
    $('.accordion ').removeClass('on');
      
    $('.panel').slideUp('normal');
    if($(this).next().is(':hidden') == true) {
      
      $(this).addClass('on');
      $(this).next().slideDown('normal');
     } 
      
   });
    
  
  $('.accordion ').mouseover(function() {
    $(this).addClass('over');
  }).mouseout(function() {
    $(this).removeClass('over');                    
  });
  
  $('.panel').hide();

});
</script>