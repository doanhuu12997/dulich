<link href="<?= base_url() ?>assets/web/css/tour_cate.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?= base_url() ?>assets/web/css/box.css">
<input type="hidden" id="get_idcate" value="<?= $get_id_catelogy->id; ?>">
<!--=== BEGIN: CONTENT ===-->
<div id="vnt-content">
  <div class="container ">
    <div class="mod-content ">
      <!--===BEGIN: BOX MAIN===-->

      <div class="box_mid">
        <div class="mid-content">
          <?php if (empty($get_id_catelogy->category_img)) { ?>
          <?php echo "";
          } else { ?>
            <div class="i-slder">
              <img src="<?= base_url('uploads/' . $get_id_catelogy->category_img) ?>" />
            </div>
          <?php } ?>
          <div class="clear"></div>

          <?php $this->load->view('web/inc/search_tour.php'); ?>

          <div class="main-content ">

            <div class="row">
              <div class="col-lg-8">
                <div class="tab-tours">
                  <nav>
                    <div class="nav nav-tabs tab-tours" id="nav-tab" role="tablist">

                      <?php
                      if (!empty($get_catelogy_parent->id)) {
                        $parent_cate = $get_catelogy_parent->id;
                        $id_parent = $get_catelogy_parent->id;
                        $rs_related_parent = $this->web_model->queryrelated_parent($parent_cate, $id_parent); ?>

                        <?php foreach ($rs_related_parent as $vp) { ?>
                          <a class="nav-item nav-link" href="<?= base_url($vp->slug) . '.html'  ?>"><?= $vp->category_name; ?></a>
                        <?php } ?>
                      <?php } else {
                        $query_related = $this->db->query("select * from tbl_category where id='" . $get_id_catelogy->parent_id . "' ");
                        $rs_query_related = $query_related->row();

                        if (empty($rs_query_related->category_name)) {
                          $child_cate = "";
                          $id = "";
                        } else {
                          $child_cate = $rs_query_related->id;
                          $id = $get_id_catelogy->id;
                        }
                        $rs_related = $this->web_model->queryrelated($child_cate, $id); ?>
                        <?php if (empty($rs_catelogy->category_name)) {
                          echo "";
                        } else { ?>
                        <?php } ?>

                        <?php foreach ($rs_related as $v) { ?>
                          <a class="nav-item nav-link" href="<?= base_url($v->slug) . '.html'  ?>"><?= $v->category_name; ?></a>
                        <?php } ?>

                      <?php } ?>
                    </div>
                  </nav>
                  <div class="tab-content shadow" id="nav-tabContent">
                    <div class="tab-pane  show active" id="nav-248" role="tabpanel" aria-labelledby="nav-248-tab">
                      <h5 class='title-sub-cate'><a><i class='fa fa-map-marker'></i><?= $get_id_catelogy->category_name; ?></a></h5>
                      <ul class="list-tour-01">
                        <?php foreach ($get_all_product as $product) {
                          $rs_dxp = $this->web_model->get_xuatphat($product->product_xuatphat);
                        ?>
                          <li>
                            <div class="item row">
                              <div class="item-left col-md-4">
                                <a class="hvr-bounce-to-right shadow" href="<?= base_url($product->product_slug) . '.html'; ?>">
                                  <img src="<?= base_url('uploads/product/thumb/' . $product->product_image) ?>" alt="<?= $product->product_title; ?>" title="<?= $product->product_title; ?>" />
                                </a>

                              </div>
                              <div class="item-right col-md-8">
                                <h3 class="title">
                                  <a href="<?= base_url($product->product_slug) . '.html'; ?>"><?= $product->product_title; ?></a>
                                </h3>
                                <ul class="meta">
                                  <li>
                                    <i class="fa fa-clock-o"></i>Thời gian: <?= $product->product_thoigian; ?>
                                  </li>
                                  <li>
                                    <i class="fa fa-calendar"></i>Khởi hành:
                                    <span class="schedule">
                                      <?php
                                      $get_schedule = $this->web_model->get_depart_schedule_result($product->product_id);
                                      foreach ($get_schedule as $key => $v_sche) { ?>
                                        <a class='btn vt-sumbit'><?php if ($v_sche->date_begin == '') {
                                                                    echo '';
                                                                  } else {
                                                                    echo nice_date($v_sche->date_begin, 'd/m') . ',';
                                                                  } ?></a>

                                      <?php  } ?>

                                    </span>
                                  </li>
                                  <li> <i class="fa fa-plane"></i>Phương tiện: <?= $product->product_phuongtien; ?> </li>
                                </ul>
                                <div class="i-promotion">
                                  <?php if (floatValue($product->product_price_sale) == 0) {
                                    echo "";
                                  } else { ?>
                                    <span><?php echo $product->product_price_sale; ?>₫</span>
                                  <?php } ?>
                                  <?php if (floatValue($product->product_price) == 0) {
                                    echo "<span class='call_me'>Liên hệ </span>";
                                  } else if (floatValue($product->product_price_sale) == 0) {
                                    echo ' <span>' . $product->product_price . ' ₫' . '</span>';
                                  } else {
                                    echo '<div class=i-price>' .  $product->product_price . '₫'; ?>
                                </div>
                              <?php } ?>
                              </div><!--end i-promtion-->
                              <a href="<?= base_url($product->product_slug) . '.html'; ?>" class="btn btn-view-detail"> Xem chi tiết </a>
                              <form action="<?= base_url('save/cart'); ?>" method="post" class="form_tour">
                                <input type="hidden" name="qty" value="1" />
                                <input type="hidden" name="product_id" value="<?= $product->product_id ?>" />
                                <input type="hidden" name="tour_xp" value="<?= $rs_dxp->xuatxu_name; ?>" />
                                <input type="hidden" name="tour_time" value="<?= $product->product_thoigian; ?>" />
                                <input type="hidden" name="tour_pt" value="<?= $product->product_phuongtien; ?>" />
                                <button type="submit" class="btn btn-booking" name="submit">ĐẶT TOUR</button>
                              </form>
                            </div>
                    </div>
                    </li>
                  <?php } ?>
                  </ul>
                  </div>
                  <!--EnD tab-pane-->

                </div><!--End tab-content-->
              </div><!--End tab-tours-->
              <div class="clear"></div>
              <div id="pagination">
                <ul class="tsc_pagination">
                  <!-- Show pagination links -->
                  <?php foreach ($links as $link) {
                    echo "<li>" . $link . "</li>";
                  } ?>

                </ul>
              </div>

            </div><!--End md8-->
            <div class="col-lg-4 siderbar">
              <div class="widget news">
                <div class="widget-title">
                  <h4>Tin tức</h4>
                </div>
                <div class="widget-content shadow">
                  <?php
                  $id_dm_news = 86;
                  $limit_news = 5;
                  $order_news = "asc";
                  $get_news_right = $this->web_model->get_news_top_home($id_dm_news, $limit_news, $order_news);
                  foreach ($get_news_right as $news) { ?>
                    <div class="single-blog-post d-flex">
                      <div class="post-thumbnail shadow"> <a href="<?= base_url($news->dmtin_slug) . '.html'; ?>">
                          <img src="<?= base_url('uploads/news/' . $news->dmtin_image) ?>" alt="<?= $news->dmtin_title ?>" title="<?= $news->dmtin_title ?>"></a> </div>
                      <div class="post-content">
                        <h3> <a href="<?= base_url($news->dmtin_slug) . '.html'; ?>" class="post-title"><?= $news->dmtin_title ?></a></h3>
                        <div class="single-blog-bottom"> <a href="<?= base_url($news->dmtin_slug) . '.html'; ?>" class="btn btn-view-detail"> Xem chi tiết </a> </div>
                      </div>
                    </div>
                  <?php } ?>
                  <!--End single-blog-post-->
                </div>
              </div>

              <div class="clear"></div>

              <div class="widget box-tour-relate">
                <div class="widget-title">
                  <h4>Tour Nổi bật</h4>
                </div>
                <div class="widget-content ">
                  <ul class="grid-tour-01">
                    <?php $product_selling = $this->web_model->product_selling();
                    foreach ($product_selling as $product) { ?>
                      <li>
                        <div class="item shadow">
                          <div class="item-top">
                            <a class="hvr-bounce-to-right" href="<?= base_url($product->product_slug) . '.html'; ?>"><img src="<?= base_url('uploads/product/thumb/' . $product->product_image) ?>" alt="<?= $product->product_title; ?>" title="<?= $product->product_title; ?>"></a>
                          </div>
                          <div class="item-bottom">

                            <h3 class="title"><a href="<?= base_url($product->product_slug) . '.html'; ?>"><?= $product->product_title; ?></a> </h3>
                            <div class="i-promotion">
                              <?php if (floatValue($product->product_price_sale) == 0) {
                                echo "";
                              } else { ?>
                                <span><?php echo $product->product_price_sale; ?>₫</span>
                              <?php } ?>
                              <?php if (floatValue($product->product_price) == 0) {
                                echo "<span class='call_me'>Liên hệ </span>";
                              } else if (floatValue($product->product_price_sale) == 0) {
                                echo ' <span>' . $product->product_price . ' ₫' . '</span>';
                              } else {
                                echo '<div class=i-price>' .  $product->product_price . '₫'; ?>
                            </div>
                          <?php } ?>
                          </div><!--end i-promtion-->
                          <ul class="meta">
                            <li> <i class="fa fa-clock-o"></i> <?= $product->product_thoigian; ?> </li>
                            <li> <i class="fa fa-plane"></i> <?= $product->product_phuongtien; ?> </li>
                            <li class="flex"> <i class="fa fa-calendar inside inside-left"></i>
                              <span class="schedule inside inside-right">
                                <?php $get_schedule = $this->web_model->get_depart_schedule_result($product->product_id);
                                foreach ($get_schedule as $key => $v_sche) { ?>
                                  <a class='btn vt-sumbit'><?php if ($v_sche->date_begin == '') {
                                                              echo '';
                                                            } else {
                                                              echo nice_date($v_sche->date_begin, 'd/m') . ',';
                                                            } ?></a>
                                <?php  } ?>

                              </span>
                            </li>
                          </ul>
                        </div>
                </div>
                </li>
              <?php } ?>
              </ul>
              </div>
            </div>
            <div class="clear"></div>
            <!--End md-4-->


          </div>
        </div><!--End main-content-->

        <div class="clear"></div>
        <div class="mid-title">
          <div class="w_mid_title">
            <div class="clear"></div>


          </div><!--end w_mid_title-->
        </div>
      </div>
    </div>
  </div>
</div>

<!--===END: BOX MAIN===-->
</div>
<div class="clear"></div>
</div>
</div>
<!--=== END: CONTENT ===-->
<!-- Fix lỗi Product Schema by evergreen.edu.vn -->