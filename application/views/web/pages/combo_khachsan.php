<link href="<?= base_url() ?>assets/web/css/news.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/web/js/news/news.js" rel="stylesheet" type="text/css" />


<div class="result">

    <?= $this->session->flashdata('message'); ?>

</div>

<!--=== BEGIN: CONTENT ===-->

<div id="vnt-content">

    <!--===BEGIN: BREADCRUMB===-->

    <div class="breadcrumb breadcrumb_left">

        <div class="container">

            <ul>

                <li><a href="/">Trang chủ</a></li>

                <li><a href="combo_khachsan.html">Combo Khách Sạn</a></li>

            </ul>

        </div>

    </div>

    <!--===END: BREADCRUMB===-->

    <?php if (empty($get_id_catelogy_dmtin->category_image)) { ?>

    <?php echo "";
    } else { ?>

    <div class="i-slder" style="padding-top: 28%;">
        <div class="wrap fill">
            <div class="bg fill"
                style="background-image: url(<?= base_url('uploads/' . $get_id_catelogy_dmtin->category_image) ?>);">
            </div>
            <div class="over"></div>
            <div class="boxtext ">
                <div class="container">
                    <div class="text_box">
                        <h2><span style="    font-size: 170%;
    color: #993366;">COMBO DU LỊCH</span></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php } ?>
    <section class="des" style="    padding-top: 23px;padding-bottom: 23px;">
        <div class="container">
            <p><span style="color: #0000ff;"><strong>Combo du lịch</strong></span> <span style="color: #000000;">là một
                    hình thức du lịch mới, bạn sẽ không bị gò bó theo một lịch trình tour có sẵn. Khi mua combo du lịch,
                    <span style="color: #800000;"><strong>Amara Tours</strong></span> sẽ cung cấp cho bạn phòng khách
                    sạn và các dịch vụ kèm theo như vé máy bay, xe di chuyển, vé khu vui chơi…. Bạn có thể thoải mái sắp
                    xếp thời gian, lựa chọn điểm du lịch, ăn uống, vui chơi,..<span
                        style="color: #800000;"><strong>Amara Tours</strong></span> cũng sẽ tư vấn cho bạn những điều
                    lưu ý để giúp chuyến đi của bạn được hoàn hảo hơn.</span></p>
        </div>
    </section>
    <section class="list_dmtin">
        <div class="container">
            <h3 class="section-title section-title-center"><b></b><span class="section-title-main" style="font-size:233%;color:#fa1414;    margin: 0 15px;
    font-weight: bold;">COMBO</span><b></b></h3>
            <div class="list">
                <div class="row"> <?php
                                    foreach ($get_all_dmtin as $item) { ?>
                    <div class="col-md-4 item_dmtin">

                        <div class="i-img">

                            <a href="<?= base_url($item->dmtin_slug) . '.html'; ?>"> <img
                                    src="<?= base_url('uploads/news/' . $item->dmtin_image) ?>"
                                    alt="<?= $item->dmtin_title ?>" title="<?= $item->dmtin_title ?>"></a>

                        </div>
                        <h3>

                            <a
                                href="<?= base_url($item->dmtin_slug) . '.html'; ?>"><?= word_limiter($item->dmtin_title, 14) ?></a>
                        </h3>
                        <div class="i_desc"><?= word_limiter($item->dmtin_short_description, 28) ?></div>
                        <div style="display: flex;justify-content: end;padding: 0 10px;"> <a
                                href="<?= base_url($item->dmtin_slug) . '.html'; ?>" class="btn_lienhe">Liên hệ</a>
                        </div>
                    </div>
                    <?php } ?>

                </div>

            </div>
        </div>
    </section>
    <section class="favorite">
        <div class="container">
            <h3 class="section-title section-title-center"><b></b><span class="section-title-main" style="font-size: 149%;color:#fa1414;    margin: 0 15px;
    font-weight: bold;">Các địa điểm yêu thích</span><b></b></h3>
            <div class="listfa">
                <?php
                foreach ($combos as $c) { ?>
                <div class="boxItem">
                    <div class="wrap ">

                        <a href="<?= $c->one_news_link ?>" target="_blank">
                            <div class="bg "
                                style="background-image:url(<?= base_url('uploads/' . $c->one_news_image) ?>)"></div>
                        </a>
                        <div class="boxtext">
                            <h2><a href="<?= $c->one_news_link ?>" target="_blank"><?= $c->one_news_title ?></a></h2>
                        </div>
                    </div>

                </div>
                <?php } ?>

            </div>
        </div>
    </section>
    <section class="ab0">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-md-6 col-lg-3">
                    <div class="ab0_item red">
                        <div class="img"><img src="<?= base_url() ?>assets/web/images/combo5.png" alt=""></div>
                        <div class="text">
                            <span><strong>KHÁCH SẠN</strong></span>
                            <span>Tham khảo khách sạn tại các địa điểm du lịch</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-md-6 col-lg-3">
                    <div class="ab0_item blue">
                        <div class="img"><img src="<?= base_url() ?>assets/web/images/combo6.png" alt=""></div>
                        <div class="text">
                            <span><strong>THUÊ XE</strong></span>
                            <span>Dịch vụ thuê xe giá rẻ và chu đáo</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-md-6 col-lg-3">
                    <div class="ab0_item green ">
                        <div class="img"><img src="<?= base_url() ?>assets/web/images/combo7.png" alt=""></div>
                        <div class="text">
                            <span><strong>VISA</strong></span>
                            <span>Dịch vụ làm visa nhanh, rẻ, uy tín</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-md-6 col-lg-3">
                    <div class="ab0_item violet ">
                        <div class="img"><img src="<?= base_url() ?>assets/web/images/combo8.png" alt=""></div>
                        <div class="text">
                            <span><strong>VÉ MÁY BAY</strong></span>
                            <span>Đặt vé máy bay giá rẻ, nhiều khuyến mãi</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ab1">
        <div class="container">
            <h3 class="section-title section-title-center"><b></b><span class="section-title-main"
                    style="font-size: 100%;color:#fa1414;    margin: 0 15px;font-weight: bold;">Tour Tham
                    Khảo</span><b></b></h3>
            <div class="tour_thamkhao">
                <?php
                $product_selling = $this->web_model->product_selling();
                foreach ($product_selling as $tour_hot) { ?>
                <div class="col-md-4 ">
                    <div class="itemtour">
                        <div class="news-releas">
                            <div class="image"> <a href="<?= base_url($tour_hot->product_slug) . '.html'; ?>">
                                    <img src="<?= base_url('uploads/product/thumb/' . $tour_hot->product_image) ?>"
                                        alt="<?= $tour_hot->product_title; ?>"
                                        title="<?= $tour_hot->product_title; ?>" />
                                </a></div>
                            <div class="content">
                                <p><?= word_limiter($tour_hot->product_title, 15); ?></p>
                                <div class="btn_itemtour"><a href="<?= base_url($tour_hot->product_slug) . '.html'; ?>"
                                        class="btn_readmore">Đọc tiếp</a></div>
                            </div>

                        </div>
                    </div>


                </div>
                <?php  } ?>
            </div>

        </div>
    </section>

</div>

<!--=== END: CONTENT ===-->