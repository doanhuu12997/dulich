<!doctype html>
<html ⚡="">
<head>
	<meta charset="utf-8">
	<title><?php echo $get_single_dmtin->dmtin_title_bar?></title>
	<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
	<meta name="amp-google-client-id-api" content="googleanalytics">
    <meta name="DC.title" lang="vi" content="<?php echo $get_single_dmtin->dmtin_short_description_seo?>" />
    <meta name="DC.creator" content="Thiết kế web chuyên nghiệp - Sehilo" />
    <meta name="DCTERMS.issued" scheme="DCTERMS.W3CDTF" content="<?php date_default_timezone_set('Asia/Ho_Chi_Minh'); echo date('l, d M Y') ;  echo date(" h:i:sa")?>" />
    <meta name="DC.identifier" scheme="DCTERMS.URI" content="<?php echo base_url($get_single_dmtin->dmtin_slug).'.html'?>" />
    <link rel="DCTERMS.replaces" hreflang="vi" href="<?php echo base_url($get_single_dmtin->dmtin_slug).'.html';?>" />
    <meta name="DCTERMS.abstract" content="<?php echo $get_single_dmtin->dmtin_short_description_seo?>" />
    <meta name="DC.format" scheme="DCTERMS.IMT" content="text/html" />
    <meta name="DC.type" scheme="DCTERMS.DCMIType" content="Text" />
    <meta property="og:title" content="<?php echo $get_single_dmtin->dmtin_title_bar?>" />
    <meta property="og:description" content="<?php echo $get_single_dmtin->dmtin_short_description_seo?>" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="<?php echo base_url('uploads/news/'.$get_single_dmtin->dmtin_image)?>" />
    <meta property="og:url" content="<?php echo base_url($get_single_dmtin->dmtin_slug).'.html'?>" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="<?php echo get_option('site_twitter_link')?>">
    <meta name="twitter:title" content="<?php echo $get_single_dmtin->dmtin_title_bar?>">
    <meta name="twitter:description" content="<?php echo $get_single_dmtin->dmtin_short_description_seo?>">
	<link rel="shortcut icon" href="<?php echo base_url('/') ?><?php echo get_option('site_favicon') ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url('/') ?><?php echo get_option('site_favicon') ?>" type="image/x-icon">
	<link rel="canonical" href="<?php echo base_url($get_single_dmtin->dmtin_slug).'.html'?>"/>
	<script async="" src="https://cdn.ampproject.org/v0.js"></script>
	<style amp-boilerplate="">body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate="">body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&display=swap" rel="stylesheet">
	<script custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js" async=""></script>
	<script custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js" async=""></script>
	<script custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js" async=""></script>
	<script custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js" async=""></script>
	<script custom-element="amp-instagram" src="https://cdn.ampproject.org/v0/amp-instagram-0.1.js" async=""></script>
	<script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
<style amp-custom>
	<?php readfile( getcwd() . "/assets/web/css/page.css"); ?>
</style>

</head>

  <body>

  <!-- Start Navbar -->
  <header class="ampstart-headerbar fixed flex justify-start items-center top-0 left-0 right-0 pl2 pr4 ">
    <div role="button" aria-label="open sidebar" on="tap:header-sidebar.toggle" tabindex="0" class="ampstart-navbar-trigger  pr2  ">☰
    </div>
    <a class="i-logo" href="<?php echo base_url('/');?>">
        <amp-img src="<?php echo base_url('uploads/'); ?><?php echo get_option('site_logo'); ?>" width="30" height="17" layout="responsive" class="my0 mx-auto" alt="logo"></amp-img>
      </a>
  </header>

<!-- Start Sidebar -->
<amp-sidebar id="header-sidebar" class="ampstart-sidebar px3  " layout="nodisplay">
  <div class="flex justify-start items-center ampstart-sidebar-header">
    <div role="button" aria-label="close sidebar" on="tap:header-sidebar.toggle" tabindex="0" class="ampstart-navbar-trigger items-start">✕</div>
  </div>
  
    <ul class="ampstart-sidebar-faq list-reset m0">
        <?php  foreach ($get_one_news as $key) {?>
        <li class="ampstart-faq-item"><a   href="<?php echo base_url($key->one_news_slug).'.html';?>" class="text-decoration-none"> <?php  echo $key->one_news_title ?></a></li>
      <?php }?>


    </ul>

    <ul class="ampstart-sidebar-faq list-reset m0">
      <?php foreach ($get_all_category_show_menu as $key) { ?>
        <li class="ampstart-faq-item"><a href="<?php echo base_url($key->slug).'.html';?>" class="text-decoration-none"> <?php  echo $key->category_name ?></a></li>
      <?php }?>

    </ul>
    <nav class="ampstart-sidebar-nav ampstart-nav">
    <ul class="list-reset m0 p0 ampstart-label">
      <?php foreach ($show_menu_dmtin as $key) {?>
          <li class="ampstart-nav-item "><a class="ampstart-nav-link" href="<?php echo base_url($key->slug).'.html';?>"><?php  echo $key->category_name ?></a></li>
      <?php }?>
              <li class="ampstart-faq-item"><a href="lien-he.html" class="text-decoration-none">Liên hệ</a></li>

    </ul>

  </nav>

</amp-sidebar>
<!-- End Sidebar -->
  <!-- End Navbar -->
    <main id="content" role="main" class="">
      <article class="recipe-article">
        <header>
			<h1 class="h1amp"><?php echo $get_single_dmtin->dmtin_title?></h1>
        </header>

        <p>
         
          <?php echo $get_single_dmtin->dmtin_long_description;?>
        </p>

  

      <!-- Start Related Section -->
<section class="ampstart-related-section mb4 px3">
      <h2 class="h3 mb1">Có thể bạn quan tâm</h2>
  <ul class="ampstart-related-section-items list-reset flex flex-wrap m0">

 <?php
                                            $query =$this->db->query("select * from tbl_dmtin  where
                                                 dmtin_id <>'".$get_single_dmtin->id."' and dmtin_category='".$get_single_dmtin->id."' limit 0,6");
                                            $get_singls=$query->result();
                                         foreach ($get_singls as $value) { ?>
      <li class="col-12 sm-col-4 md-col-4 lg-col-4 pr2 py2">

    <a href="<?php echo base_url($value->dmtin_slug).'.html';?>">

  <!-- Start Image with Caption -->
  <figure class="ampstart-image-with-caption m0 relative mb4">
    <amp-img src="<?php echo base_url('uploads/news/'.$value->dmtin_image)?>" width="100" height="70" alt="<?php echo $value->dmtin_title ?>" layout="responsive" class=""></amp-img>
    <figcaption class="h5 mt1 px3">
     <?php echo $value->dmtin_title ?>
    </figcaption>
  </figure>
  </a>
  <!-- End Image with Caption -->
      </li>
       <?php   }?>
  </ul>
</section>
<!-- End Related Section -->
       
      </section>

    </article>
    </main>

  <!-- Start Footer -->
 
<!--=== BEGIN: FOOTER ===-->
    <!--=== BEGIN: FOOTER ===-->
   <div class="topBar">
    <div class="container">
        <div class="topBar-row">
            <div class="row">
                <div class="col-md-6 col-xs-12 topBar-item">
                    <p class="title">Tư vấn mua hàng</p>
                    <p class="number_phone"><a href="tel:<?php echo get_option('company_number');?>"> <?php echo get_option('company_number');?></a></p>
                    <p class="moreinfo">Tất cả các ngày trong tuần</p>
                    
                </div>
                <div class="col-md-6 col-xs-12 topBar-item">
                    <p class="title">Chăm sóc khách hàng</p>
                    <p class="number_phone"><a href="tel:<?php echo get_option('company_hotline');?>"> <?php echo get_option('company_hotline');?></a></p>
                    <p class="moreinfo">Các ngày trong tuần ( trừ lễ )</p>
                </div>
                
                
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="footer_row_bottom">
            <div class="row">

            <?php foreach ($show_cate_news as $v) { 
                 $rs = $this->db->query("select * from tbl_dmtin  where publication_status =1 and dmtin_category ='".$v->id."'  limit 0,5 ");
                $data=$rs->result();
                ?>
            
                <div class="col-md-3 footer_col1">
                    <div class="footer_p">
                        <div class="footer_col1_title footer_title1 footer_title_d">
                            <?php  echo $v->category_name?>
                        </div>
                        
                        <div  class="panel-collapse collapse in">
                            <div class="panel-body">
                                <ul class="footer_box">
                                      <?php foreach ($data as $news) {?>
                            <li><a href="<?php echo base_url($news->dmtin_slug).'.html';?>" title="<?php echo $news->dmtin_title ?>"><?php echo $news->dmtin_title ?></a></li>
                              <?php } ?>
                                </ul>

                            </div>
                        </div>
                    
                    </div>
                </div>

  <?php } ?>
               
                 <div class="col-md-3 footer_col1">
                    <div class="footer_p">
                        <div class="footer_col1_title footer_title1 footer_title_d">
                            Danh mục sản phẩm
                        </div>
                        
                        
                        <div  class="panel-collapse collapse in">
                            <div class="panel-body">
                                <ul class="footer_box">
                                   <?php foreach ($get_show_category_footer as $key) { ?>
                                    <li><a href="<?php echo base_url($key->slug).'.html';?>" title="<?php  echo $key->category_name ?>"><h2><?php  echo $key->category_name ?></h2></a></li>
                                    <?php }?>

                                </ul>

                            </div>
                        </div>
                    
                    </div>
                </div>
    
            </div>
        </div>
        <div class="footer_bottom">
            <div class="row">
                <div class="col-md-10">
                    <div class="footer_bottom_t"><?php get_option('text_cuahang');?></div>
                    <div>Địa chỉ: <?php echo get_option('site_address');?> - Hotline: <?php echo get_option('company_hotline');?> - Điện thoại : <?php echo get_option('company_number');?>- Email:  <?php get_option('company_email');?></div>
                    
                </div>
                <div class="col-md-2" >
                    <div class="dathongbao"><a >
                       <amp-img src="<?php echo base_url() ?>assets/web/images/dathongbao.png" width="200" height="76"  layout="responsive" class=""></amp-img>
                </div>
            </div>
        </div>
        
    </div>

</footer>

        <!--=== END: FOOTER ===-->
  <div class="fixedposicon">
     

     <a class="btncall" target="_blank" href="tel:<?php echo get_option('company_number');?>">
      <amp-img layout="responsive" height="50" width="50" class="alignnone" src="<?php  echo base_url()?>/assets/web/images/icon/widget_icon_call.svg" alt="Gọi ngay" ></amp-img>
      <span class="label">Gọi ngay</span>
    </a><br>

    <a class="btnfacebook" target="_blank" href="https://m.me/<?php echo get_option('site_facebook_link');?>">
       <amp-img layout="responsive" height="50" width="50" class="alignnone" src="<?php  echo base_url()?>/assets/web/images/icon/widget_icon_messenger.svg" alt="Chat messenger" ></amp-img>

      <span class="label">Chat bằng facebook messenger </span>
    </a><br>

    <a class="btnmap" target="_blank" href="<?php echo get_option('site_link_googlemap');?>">
       <amp-img layout="responsive" height="50" width="50" class="alignnone" src="<?php  echo base_url()?>/assets/web/images/icon/widget_icon_map.svg" alt="Bản đồ" ></amp-img>

    <span class="label">Bản đồ chỉ đường </span>
    </a><br>


    <a class="btnzalo" target="_blank" href="https://zalo.me/<?php echo get_option('company_number');?>"><amp-img layout="responsive" height="50" width="50" class="alignnone" src="<?php  echo base_url()?>/assets/web/images/icon/widget_icon_zalo.svg" alt="Chat zalo" ></amp-img>

         <span class="label">Chat với chúng tôi qua zalo </span>
    </a><br>
    <br><br><br><br>
</div>


  </body>
</html>
