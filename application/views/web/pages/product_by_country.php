


<link href="<?php echo base_url() ?>assets/web/css/product.css" rel="stylesheet" type="text/css" />


 <!--=== BEGIN: CONTENT ===-->
        <div id="vnt-content">
           <!--===BEGIN: BREADCRUMB===-->
            <div id="vnt-navation">
                    <div class="navation breadcrumb">
                      <div class="container">
                        <ul class="i_breadcrumb">
                            <li><a href="/">Trang chủ</a></li>
                            <li><a>
                                 <?php echo   $get_id_country->brand_name ;?>
                          </a></li>
                        </ul>
                    </div>
            </div>
            <!--===END: BREADCRUMB===-->
      <div class="container">      
<?php if(empty($get_id_country->brand_banner)){ echo ""; }else {?>
       <div class="i-slder"><img src="<?php echo base_url('uploads/thuonghieu/'.$get_id_country->brand_banner)?>" /></div>
<?php }?>
           
            <div class="clear"></div>
                <div class="mod-content">
                
                        <!--===BEGIN: BOX MAIN===-->
                        <div class="box_mid row">
                            <div class="mid-content">
                                 <div class="gird_tour">
                                  <div class="mid-title">
                                <div class="titleL"><h1 class="i_title_brand col-md-12 col-xs-12">
                         <?php if(empty($get_id_country->brand_name)) { echo "Thương hiệu";} else { echo $get_id_country->brand_name ;} ?></h1></div>
                                <div class="titleR"></div>
                                <div class="clear"></div>

                            </div>
  <!--=== BEGIN: PRODUCT ===-->
            <div class="vnt-product">
                    <div class="prod-content">
                        <div class="grid-prod">
                      <div class="w_product w_thuonghieu">
                           <?php foreach ($get_all_product_country as $product) {?>
     <div class="item">
                <div class="wrap-item">
                  <div class="i-top">
                    <div class="i-image col-md-2 col-xs-6 col-sm-2">
                       <?php if(empty($product->product_donggoi)){ echo '';} else{?>            
                      <span class="product-promo"><?php echo $product->product_donggoi; ?></span>
                      <?php }?>
                      <?php 
                        if(floatValue($product->product_price_sale)==0) {echo "";} 
                        else {?>
                          <div class="i-saleoff">
                            <?php if(floatValue($product->product_price)==0 || floatValue($product->product_price_sale)==0){ ?>
                              <?php echo  ""; ?>
                            <?php } else { ?>
                              <?php $percent = (floatValue($product->product_price) - floatValue($product->product_price_sale)) /floatValue($product->product_price) *100; ?>
                              <?php $rs_percent=number_format($percent);
                              echo - round($rs_percent); ?>% 
                            <?php  }?>
                          </div>
                        <?php }?>
                      <?php if($product->product_hot==0){echo "";} else{?>
                        <div class="hot"><img src="<?php echo base_url() ?>assets/web/images/hot-icon.gif" /> </div>
                      <?php }?>
                        <?php if(floatValue($product->product_price_sale)==0) {echo "";} else {?>
                      <label class="discount">GIẢM  <?php $price_sale = (floatValue($product->product_price) - floatValue($product->product_price_sale)); echo number_format($price_sale) .'₫';  ?></label> <?php }?>
                      
                      <a href="<?php echo base_url($product->product_slug).'.html';?>">
                        <img src="<?php echo base_url('uploads/product/thumb/'.$product->product_image)?>" alt="<?php echo $product->product_title ?>" title="<?php echo $product->product_title ?>"/>
                      </a>
                    </div>
                  </div>
                  <div class="i-desc col-md-10 col-xs-6 col-sm-10">
                    <div class="desc-left">
                      <div class="i-title">
                        <a href="<?php echo base_url($product->product_slug).'.html';?>" title="<?php echo $product->product_title ?>">
                          <h4 title="<?php echo $product->product_title ?>"><?php echo word_limiter($product->product_title,9) ?></h4>
                        </a>
                      </div> 
                      <div class="clear"></div>
                      
                      <div class="i-promotion">
                        <?php if(floatValue($product->product_price_sale)==0) { echo "";} else {?>
                          <span content="<?php echo number_format(floatValue($product->product_price_sale),0, ',','.');?>"><?php echo $product->product_price_sale; ?></span>
                          <span content="vnđ">₫</span> 
                        <?php }?>
                        <?php if(floatValue($product->product_price)==0){echo "<span class='call_me' itemprop='price' content=".number_format(floatValue($product->product_price),0, ',','.').">Liên hệ </span> <span itemprop='priceCurrency' content='vnđ'></span>";} else if(floatValue($product->product_price_sale) ==0){echo '<span content="vnđ"></span> <span content='.number_format(floatValue($product->product_price),0, ',','.').'>'. $product->product_price .' ₫' .'</span>';}else{ echo '<div class=i-price>' .  $product->product_price .'₫' ; ?></div>
                      <?php } ?>
                    </div><!--end i-promtion-->
                    <div class="clear"></div>
                  </div>
                </div>
              </div>
            </div><!--end item-->
            <div class="clear"></div>
<?php } ?>
                         <div class="clear"></div>
                          <div id="pagination">
                                                <ul class="tsc_pagination">
                                                <!-- Show pagination links -->
                                                <?php foreach ($links as $link) {
                                                echo "<li>". $link."</li>";
                                                } ?>
                                            </ul>
                                         </div>

  <section class="section ">
    <div class="news-item news-md brand-detail">
        <div class="news-image col-md-3 col-xs-12">
            <a>
              <?php  if($get_id_country->brand_image==''){echo '';}else { ?>
                    <img class="img-fluid" src="<?php echo base_url('uploads/thuonghieu/'.$get_id_country->brand_image)?>" alt="<?php echo $get_id_country->brand_name;?>">
                    </a>
                  <?php }?>
        </div>
        <div class="news-summary col-md-9 col-xs-12">
            <div class="news-text brand-description font-16" style=" height: 110px; overflow: hidden;">
               <?php echo $get_id_country->brand_description; ?>
            </div>

            <div class="text-right down_summary ">
              <span class="text-primary font-14 pointer" onclick="$('.brand-description').attr('style','');
              $('.down_summary').addClass('hide');$('.up_summary').removeClass('hide'); 
              return;">
                <small>Xem thêm</small></span>
              </div>
            <div class="text-right up_summary hide">
              <span class="text-primary font-14 pointer" onclick="$('.brand-description').attr('style','height: 110px; overflow: hidden;');
              $('.up_summary').addClass('hide');
              $('.down_summary').removeClass('hide'); return;">
              <small>Thu gọn</small></span></div>
        </div>
    </div>
</section>
                           
                      </div>

                           
                        </div>
                </div>
            </div>
            <!--=== END: PRODUCT ===-->

</div>
</div>
                               
                        </div>
                        <!--===END: BOX MAIN===-->
                    </div>
                    <div class="clear"></div>
                </div>
        </div>
        <!--=== END: CONTENT ===-->



