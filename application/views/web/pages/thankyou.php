

    <!--===MODULE MAIN==-->
    <link href="<?php echo base_url() ?>assets/web/css/cart.css" rel="stylesheet" type="text/css" />
    <!--===MODULE MAIN==-->  


  <!--=== BEGIN: CONTENT ===-->
        <div id="vnt-content">

<div class="breacrumb">
        <div class="container">
            <h1 class="h1category">Đơn hàng</h1>
            <div class="breacrumb1">
                <a class="breacrumb-home1" href="/" title="Trang chủ">Trang chủ</a>
                <a title="Đặt hàng thành công">Đặt hàng thành công</a>
            </div>
        </div>
    </div>


<div class="container">
    <div class="shoppingcart1">
        <div class="shopcart-lr">
            <div class="shopcart-l"><div class="orderSuccess"><i class="fa fa-check"></i> Đặt hàng thành công</div></div>
        </div>
        <div class="cartback">
            <div class="order-top">Cảm ơn Anh chị đã cho <?php $url_parts = $_SERVER['SERVER_NAME']; echo $url_parts;?> được phục vụ. Hi vọng Anh sẽ hài lòng về sản phẩm. Nếu có bất kì vấn đề cần hỗ trợ Anh chị vui lòng gọi số hotline <a href="tel:<?php echo get_option('company_number');?>"><?php echo get_option('company_number');?></a></div>
            <div class="orderdathang">
                    <div class="orderdathang-title">Thông tin đặt hàng</div>
                    <ul class="order-tt">
                        <!--<li><i class="fa fa-arrow-right"></i> Địa chỉ nhận hàng: <b>200 baucat</b></li>-->
                        <li><i class="fa fa-arrow-right"></i> <b>Thanh toán tiền mặt khi nhận hàng</b></li>
                        <li><i class="fa fa-arrow-right"></i> Tổng tiền: 
                            <?php echo $this->cart->format_number($this->cart->total()); 
                            ?>
                             VNĐ</li>
                    </ul>
                </div>
                
        </div><!--end cartback-->
    </div>
</div>

