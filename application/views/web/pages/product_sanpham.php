


<link href="<?php echo base_url() ?>assets/web/css/product.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url() ?>assets/web/js/product/ajax_product.js"></script>


 <!--=== BEGIN: CONTENT ===-->
        <div id="vnt-content">

          <!--  <div class="i-slder"><img src="<?php echo base_url() ?>assets/web/images/product/banner.jpg" /></div>-->
            <!--===BEGIN: BREADCRUMB===-->
            <div id="vnt-navation" class="breadcrumb" >
                <div class="wrapper container">
                    <div class="navation">
                        <ul>
                            <li><a href="/">Trang chủ</a></li>
                            <li><a > Sản phẩm</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--===END: BREADCRUMB===-->
            <div class="wrapper ">
                <div class="mod-content">
                
                        <!--===BEGIN: BOX MAIN===-->
                        <div class="box_mid">
                            <div class="mid-title">
                                <div class="titleL"><h1></h1></div>
                                <div class="titleR"></div>
                                <div class="clear"></div>
                            </div>
               
                            
 <div class="wrapper_product col-md-12 col-sm-12 col-xs-12">
             <div class="wrapp_list_desc">
              <ul class="list-cat">
                                <?php $query=$this->db->query("select * from tbl_category where cate_status =1 and parent_id=0;"); $rs_cat =$query->result(); 
                                foreach ($rs_cat as $v) 
                                 { 
                                  $id= $v->id; $get_count_c2 = $this->web_model->count_cate($id); ?>
                                  <li> 
                                    <a  class="btn hvr-shutter-out-horizontal" href="<?php echo base_url($v->slug).'.html';?>" title="<?php echo $v->category_name ;?>">
                                    <h2><?php echo $v->category_name ;?> (<?php echo $get_count_c2->count; ?>)</h2></a>
                                    </li>
                                <?php } ?>
                               
                            </ul>   
               </div>   
           <span class="tittle_h1"><h1>Sản phẩm</h1></span>
           <div class="clear"></div>
          <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#home">Tất cả</a></li>
          <li><a data-toggle="tab" href="#menu1">Bán chạy</a></li>
          <li><a data-toggle="tab" href="#menu2">Giá tăng dần</a></li>
          <li><a data-toggle="tab" href="#menu3">Giá giảm dần</a></li>
   <span class="boloc">Filters</span>
        </ul>
  <div class="content-filter">
               
                   <div class="filter-item">
                       <div  class="scroll-v-250px box-filter first">
                         
                             <div class="bf-content">
                                            <ul class="i_thanhphan">
                                              <div class="title-filter"> Thành phần </div>
                                         <?php  foreach ($get_all_thanhphan as $v)  {
                                          ?>
                                           <li>
                                           <a id="<?php echo $v->thanhphan_id?>"   data-href="<?php echo $v->thanhphan_slug;?>"><h3 title="<?php echo $v->thanhphan_name?>"><?php echo $v->thanhphan_name?> </h3></a>
                                           </li>
                                         <?php } ?>
                                          </ul>
                                </div>
                                 <div class="bf-content">
                                            <ul class="i_congdung">
                                           <div class="title-filter"> Công dụng </div>
                                         <?php  foreach ($get_all_congdung as $v)  {
                                           
                                          ?>
                                           <li>
                                       <a id="<?php echo $v->congdung_id?>"  data-href="<?php echo $v->congdung_slug;?>"><h3 title="<?php echo $v->congdung_name?>"><?php echo $v->congdung_name?> </h3></a>
                                           </li>
                                         <?php } ?>
                                          </ul>
                                  </div><!--end bf-content-->

                        </div>
                    </div>

                 </div><!--end content-filter-->

        <div class="tab-content ">
          <div id="home" class="tab-pane fade in active">
           <!--=== BEGIN: PRODUCT ===-->
            <div class="vnt-product">
                    <div class="prod-content">
                        <div class="grid-prod row">
                      <div id="load_catelogy"></div>
                      <div id="display_all_sanpham"></div>
                      <div id="load_thanhphan"></div>
                      <div id="load_congdung"></div>

                      <div class="w_product">
                            <?php foreach ($get_all_product as $product) {?>
    <div class="item col-md-3 col-xs-6 col-sm-4" itemscope itemtype="http://schema.org/Product">
                                <div class="wrap-item">
                                    <div class="i-top">
                                        <div class="i-image">
                                           <?php if($product->recentlyViewed==0){echo "";} else{?>
                                          <div class="hot"><img src="<?php echo base_url() ?>assets/web/images/icHot.png" /> </div>
                                        <?php }?>
                                            <a itemprop="url" href="<?php echo base_url($product->slug.'/'.$product->product_slug).'.html';?>">
                                                <img itemprop="image" src="<?php echo base_url('uploads/product/thumb/'.$product->product_image)?>" alt="<?php echo $product->product_title ?>" title="<?php echo $product->product_title ?>"/>
                                            </a>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="i-desc">
                                        <div class="desc-left">
                                            <div class="i-title">
                                                    <a itemprop="url" href="<?php echo base_url($product->slug.'/'.$product->product_slug).'.html';?>" title="<?php echo $product->product_title ?>"><h4 itemprop="name" title="<?php echo $product->product_title ?>"><?php echo word_limiter($product->product_title,9) ?></h4></a>
                                            </div> 
    <div class="i-promotion" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
       <?php if(floatValue($product->product_price_sale)==0) {echo "";} else {?>

                                      <div class="i-saleoff">
                                            <?php if(floatValue($product->product_price)==0 || floatValue($product->product_price_sale)==0){ ?>
                                                   <?php echo  ""; ?>
                                                    <?php } else { ?>
                                            <?php $percent = (floatValue($product->product_price) - floatValue($product->product_price_sale)) /floatValue($product->product_price) *100; ?>
                                                    <?php  $rs_percent=number_format($percent);
                                                     echo - round($rs_percent); ?>% 
                                                <?php  }?>
                                        </div>
                                      <?php }?>
    <?php if(floatValue($product->product_price_sale)==0) { echo "";} else {?>
      <span itemprop="price" content="<?php echo number_format(floatValue($product->product_price_sale),0, ',','.');?>"><?php echo $product->product_price_sale; ?></span>
      <span itemprop="priceCurrency" content="vnđ">đ</span> 
    <?php }?>
                                          
  <?php if(floatValue($product->product_price)==0){echo "<span class='call_me' itemprop='price' content=".number_format(floatValue($product->product_price),0, ',','.').">Liên hệ </span> <span itemprop='priceCurrency' content='vnđ'>đ</span>";} else if(floatValue($product->product_price_sale) ==0){echo '<span itemprop="priceCurrency" content="vnđ"></span> <span itemprop="price" content='.number_format(floatValue($product->product_price),0, ',','.').'>'. $product->product_price .' đ' .'</span>';}else{ echo '<div class=i-price>' .  $product->product_price .'đ' ; ?></div>
                                                <?php } ?>
                                             </div><!--end i-promtion-->
                    <div class="clear"></div>
                  <div class="product-meta"> 
                     <?php 
                              $jquery=$this->db->query("select  count(*) as count from tbl_comment  where comment_product_id='".$product->product_id."' ");
                                $rs_count=$jquery->row(); 
                                  ?>
                  <i class="icons icon-heart"></i>
                    <span class="icon-top ">
                    <span class="icon-text vote_count">0</span></span>
                          <a href="<?php echo base_url($product->slug.'/'.$product->product_slug).'.html';?>#mystar">
                  <i class="icons icon-comment  "></i>
                  <span class="icon-top-comment">
                  <span class="icon-text"> <?php echo $rs_count->count ;?></span></span>
                  </a>
                        </div><!--end product-meta-->
                                        </div>
                                      
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div><!--end item-->
                        <?php } ?>
                         <div class="clear"></div>
                            <div id="pagination">
                                                <ul class="tsc_pagination">
                                                <!-- Show pagination links -->
                                                <?php foreach ($links as $link) {
                                                echo "<li>". $link."</li>";
                                                } ?>
                                            </ul>
                                         </div>
                      </div>

                           
                        </div>
                </div>
            </div>
            <!--=== END: PRODUCT ===-->


                            

                                        

          </div>
          <div id="menu1" class="tab-pane fade">
                <div class="vnt-product">
               <div class="grid-prod">
               <?php foreach ($product_selling as $product) {?>
     <div class="item col-md-3 col-xs-6 col-sm-4" itemscope itemtype="http://schema.org/Product">
                                <div class="wrap-item">
                                    <div class="i-top">
                                        <div class="i-image">
                                           <?php if($product->recentlyViewed==0){echo "";} else{?>
                                          <div class="hot"><img src="<?php echo base_url() ?>assets/web/images/icHot.png" /> </div>
                                        <?php }?>
                                            <a itemprop="url" href="<?php echo base_url($product->slug.'/'.$product->product_slug).'.html';?>">
                                                <img itemprop="image" src="<?php echo base_url('uploads/product/thumb/'.$product->product_image)?>" alt="<?php echo $product->product_title ?>" title="<?php echo $product->product_title ?>"/>
                                            </a>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="i-desc">
                                        <div class="desc-left">
                                            <div class="i-title">
                                                    <a itemprop="url" href="<?php echo base_url($product->slug.'/'.$product->product_slug).'.html';?>" title="<?php echo $product->product_title ?>"><h4 itemprop="name" title="<?php echo $product->product_title ?>"><?php echo word_limiter($product->product_title,9) ?></h4></a>
                                            </div> 
    <div class="i-promotion" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
       <?php if(floatValue($product->product_price_sale)==0) {echo "";} else {?>

                                      <div class="i-saleoff">
                                            <?php if(floatValue($product->product_price)==0 || floatValue($product->product_price_sale)==0){ ?>
                                                   <?php echo  ""; ?>
                                                    <?php } else { ?>
                                            <?php $percent = (floatValue($product->product_price) - floatValue($product->product_price_sale)) /floatValue($product->product_price) *100; ?>
                                                    <?php  $rs_percent=number_format($percent);
                                                     echo - round($rs_percent); ?>% 
                                                <?php  }?>
                                        </div>
                                      <?php }?>
    <?php if(floatValue($product->product_price_sale)==0) { echo "";} else {?>
      <span itemprop="price" content="<?php echo number_format(floatValue($product->product_price_sale),0, ',','.');?>"><?php echo $product->product_price_sale; ?></span>
      <span itemprop="priceCurrency" content="vnđ">đ</span> 
    <?php }?>
                                          
  <?php if(floatValue($product->product_price)==0){echo "<span class='call_me' itemprop='price' content=".number_format(floatValue($product->product_price),0, ',','.').">Liên hệ </span> <span itemprop='priceCurrency' content='vnđ'>đ</span>";} else if(floatValue($product->product_price_sale) ==0){echo '<span itemprop="priceCurrency" content="vnđ"></span> <span itemprop="price" content='.number_format(floatValue($product->product_price),0, ',','.').'>'. $product->product_price .' đ' .'</span>';}else{ echo '<div class=i-price>' .  $product->product_price .'đ' ; ?></div>
                                                <?php } ?>
                                             </div><!--end i-promtion-->
                    <div class="clear"></div>
                  <div class="product-meta"> 
                     <?php 
                              $jquery=$this->db->query("select  count(*) as count from tbl_comment  where comment_product_id='".$product->product_id."' ");
                                $rs_count=$jquery->row(); 
                                  ?>
                  <i class="icons icon-heart"></i>
                    <span class="icon-top ">
                    <span class="icon-text vote_count">0</span></span>
                          <a href="<?php echo base_url($product->slug.'/'.$product->product_slug).'.html';?>#mystar">
                  <i class="icons icon-comment  "></i>
                  <span class="icon-top-comment">
                  <span class="icon-text"> <?php echo $rs_count->count ;?></span></span>
                  </a>
                        </div><!--end product-meta-->
                                        </div>
                                      
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div><!--end item-->
                        <?php } ?>
                      </div>
                        <div class="clear"></div>
                      <div id="pagination">
                              <ul class="tsc_pagination">
                               <!-- Show pagination links -->
                              <?php foreach ($links as $link) {
                               echo "<li>". $link."</li>";
                                } ?>
                           </ul>
                        </div>
                      </div>
          </div>
          <div id="menu2" class="tab-pane fade">
             <div class="vnt-product">
              <div class="grid-prod">
               <?php foreach ($product_price_tang as $product) {?>
      <div class="item col-md-3 col-xs-6 col-sm-4" itemscope itemtype="http://schema.org/Product">
                                <div class="wrap-item">
                                    <div class="i-top">
                                        <div class="i-image">
                                           <?php if($product->recentlyViewed==0){echo "";} else{?>
                                          <div class="hot"><img src="<?php echo base_url() ?>assets/web/images/icHot.png" /> </div>
                                        <?php }?>
                                            <a itemprop="url" href="<?php echo base_url($product->slug.'/'.$product->product_slug).'.html';?>">
                                                <img itemprop="image" src="<?php echo base_url('uploads/product/thumb/'.$product->product_image)?>" alt="<?php echo $product->product_title ?>" title="<?php echo $product->product_title ?>"/>
                                            </a>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="i-desc">
                                        <div class="desc-left">
                                            <div class="i-title">
                                                    <a itemprop="url" href="<?php echo base_url($product->slug.'/'.$product->product_slug).'.html';?>" title="<?php echo $product->product_title ?>"><h4 itemprop="name" title="<?php echo $product->product_title ?>"><?php echo word_limiter($product->product_title,9) ?></h4></a>
                                            </div> 
    <div class="i-promotion" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
       <?php if(floatValue($product->product_price_sale)==0) {echo "";} else {?>

                                      <div class="i-saleoff">
                                            <?php if(floatValue($product->product_price)==0 || floatValue($product->product_price_sale)==0){ ?>
                                                   <?php echo  ""; ?>
                                                    <?php } else { ?>
                                            <?php $percent = (floatValue($product->product_price) - floatValue($product->product_price_sale)) /floatValue($product->product_price) *100; ?>
                                                    <?php  $rs_percent=number_format($percent);
                                                     echo - round($rs_percent); ?>% 
                                                <?php  }?>
                                        </div>
                                      <?php }?>
    <?php if(floatValue($product->product_price_sale)==0) { echo "";} else {?>
      <span itemprop="price" content="<?php echo number_format(floatValue($product->product_price_sale),0, ',','.');?>"><?php echo $product->product_price_sale; ?></span>
      <span itemprop="priceCurrency" content="vnđ">đ</span> 
    <?php }?>
                                          
  <?php if(floatValue($product->product_price)==0){echo "<span class='call_me' itemprop='price' content=".number_format(floatValue($product->product_price),0, ',','.').">Liên hệ </span> <span itemprop='priceCurrency' content='vnđ'>đ</span>";} else if(floatValue($product->product_price_sale) ==0){echo '<span itemprop="priceCurrency" content="vnđ"></span> <span itemprop="price" content='.number_format(floatValue($product->product_price),0, ',','.').'>'. $product->product_price .' đ' .'</span>';}else{ echo '<div class=i-price>' .  $product->product_price .'đ' ; ?></div>
                                                <?php } ?>
                                             </div><!--end i-promtion-->
                    <div class="clear"></div>
                  <div class="product-meta"> 
                     <?php 
                              $jquery=$this->db->query("select  count(*) as count from tbl_comment  where comment_product_id='".$product->product_id."' ");
                                $rs_count=$jquery->row(); 
                                  ?>
                  <i class="icons icon-heart"></i>
                    <span class="icon-top ">
                    <span class="icon-text vote_count">0</span></span>
                          <a href="<?php echo base_url($product->slug.'/'.$product->product_slug).'.html';?>#mystar">
                  <i class="icons icon-comment  "></i>
                  <span class="icon-top-comment">
                  <span class="icon-text"> <?php echo $rs_count->count ;?></span></span>
                  </a>
                        </div><!--end product-meta-->
                                        </div>
                                      
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div><!--end item-->
                        <?php } ?>
                      </div>
                      <div class="clear"></div>
                      <div id="pagination">
                                                <ul class="tsc_pagination">
                                                <!-- Show pagination links -->
                                                <?php foreach ($links as $link) {
                                                echo "<li>". $link."</li>";
                                                } ?>
                                            </ul>
                                         </div>
                                       </div>
          </div>
          <div id="menu3" class="tab-pane fade">
                        <div class="vnt-product">

            <div class="grid-prod">
               <?php foreach ($product_price_giam as $product) {?>
     <div class="item col-md-3 col-xs-6 col-sm-4" itemscope itemtype="http://schema.org/Product">
                                <div class="wrap-item">
                                    <div class="i-top">
                                        <div class="i-image">
                                           <?php if($product->recentlyViewed==0){echo "";} else{?>
                                          <div class="hot"><img src="<?php echo base_url() ?>assets/web/images/icHot.png" /> </div>
                                        <?php }?>
                                            <a itemprop="url" href="<?php echo base_url($product->slug.'/'.$product->product_slug).'.html';?>">
                                                <img itemprop="image" src="<?php echo base_url('uploads/product/thumb/'.$product->product_image)?>" alt="<?php echo $product->product_title ?>" title="<?php echo $product->product_title ?>"/>
                                            </a>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="i-desc">
                                        <div class="desc-left">
                                            <div class="i-title">
                                                    <a itemprop="url" href="<?php echo base_url($product->slug.'/'.$product->product_slug).'.html';?>" title="<?php echo $product->product_title ?>"><h4 itemprop="name" title="<?php echo $product->product_title ?>"><?php echo word_limiter($product->product_title,9) ?></h4></a>
                                            </div> 
    <div class="i-promotion" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
       <?php if(floatValue($product->product_price_sale)==0) {echo "";} else {?>

                                      <div class="i-saleoff">
                                            <?php if(floatValue($product->product_price)==0 || floatValue($product->product_price_sale)==0){ ?>
                                                   <?php echo  ""; ?>
                                                    <?php } else { ?>
                                            <?php $percent = (floatValue($product->product_price) - floatValue($product->product_price_sale)) /floatValue($product->product_price) *100; ?>
                                                    <?php  $rs_percent=number_format($percent);
                                                     echo - round($rs_percent); ?>% 
                                                <?php  }?>
                                        </div>
                                      <?php }?>
    <?php if(floatValue($product->product_price_sale)==0) { echo "";} else {?>
      <span itemprop="price" content="<?php echo number_format(floatValue($product->product_price_sale),0, ',','.');?>"><?php echo $product->product_price_sale; ?></span>
      <span itemprop="priceCurrency" content="vnđ">đ</span> 
    <?php }?>
                                          
  <?php if(floatValue($product->product_price)==0){echo "<span class='call_me' itemprop='price' content=".number_format(floatValue($product->product_price),0, ',','.').">Liên hệ </span> <span itemprop='priceCurrency' content='vnđ'>đ</span>";} else if(floatValue($product->product_price_sale) ==0){echo '<span itemprop="priceCurrency" content="vnđ"></span> <span itemprop="price" content='.number_format(floatValue($product->product_price),0, ',','.').'>'. $product->product_price .' đ' .'</span>';}else{ echo '<div class=i-price>' .  $product->product_price .'đ' ; ?></div>
                                                <?php } ?>
                                             </div><!--end i-promtion-->
                    <div class="clear"></div>
                  <div class="product-meta"> 
                     <?php 
                              $jquery=$this->db->query("select  count(*) as count from tbl_comment  where comment_product_id='".$product->product_id."' ");
                                $rs_count=$jquery->row(); 
                                  ?>
                  <i class="icons icon-heart"></i>
                    <span class="icon-top ">
                    <span class="icon-text vote_count">0</span></span>
                          <a href="<?php echo base_url($product->slug.'/'.$product->product_slug).'.html';?>#mystar">
                  <i class="icons icon-comment  "></i>
                  <span class="icon-top-comment">
                  <span class="icon-text"> <?php echo $rs_count->count ;?></span></span>
                  </a>
                        </div><!--end product-meta-->
                                        </div>
                                      
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div><!--end item-->
                        <?php } ?>
                      </div>
                      <div class="clear"></div>
                      <div id="pagination">
                                                <ul class="tsc_pagination">
                                                <!-- Show pagination links -->
                                                <?php foreach ($links as $link) {
                                                echo "<li>". $link."</li>";
                                                } ?>
                                            </ul>
                                         </div>


          </div>

          </div>
        </div>

      </div>
                           
                        </div>
                        <!--===END: BOX MAIN===-->
                    </div>
                    <div class="clear"></div>
                </div>
                            <?php  $this->load->view('web/inc/spview'); ?>

        </div>
        <!--=== END: CONTENT ===-->







