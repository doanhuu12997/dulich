<!--===MODULE MAIN==-->
<link href="<?= base_url() ?>assets/web/css/tour_detail.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/web/css/tour_cate.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url() ?>assets/web/css/product_detail.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<?= base_url() ?>assets/web/js/product/product.js"></script>
<!--===MODULE MAIN==-->
<!--=== BEGIN: CONTENT ===-->
<div class="main-page detail-page">
  <div class="container">
    <div class="result">
      <p><?= $this->session->flashdata('message'); ?></p>
    </div>
    <div class="top-detail-page">
      <div class="detail-header">
        <div class="d-flex flex-column flex-lg-row sb">
          <div class="o2">
            <h1 class="name"><?= $get_single_product->product_title; ?></h1>
            <h4 class="codetou" style="font-weight:bold"><?= $get_single_product->code_tour; ?></h4>
            <span style="text-align: justify">
              <?= $get_single_product->product_short_description; ?>
            </span>
            <div class="clear"></div>
          </div>
        </div>
      </div>
      <div class="slideshow-meta-tour row">
        <div class="col-lg-8 col-xs-12">
          <div class="main">
            <div class="slider slider-for" style="height: 500px;">
              <?php $query = $this->db->query("select * from tbl_thumb where product_id='" . $get_single_product->product_id . "' ");
              $rs_images = $query->result();
              foreach ($rs_images as $thumb_img) { ?>
                <?php if (empty($thumb_img->image)) {
                  echo ""; ?>
                <?php } else { ?>
                  <div class="item">
                    <div class="img">
                      <img src="<?= base_url('uploads/thumb/' . $thumb_img->image) ?>" />
                    </div>
                  </div>
              <?php }
              } ?>

            </div><!--End slider-for-->
            <div class="slider slider-nav" style="height: 70px;">
              <?php $query = $this->db->query("select * from tbl_thumb where product_id='" . $get_single_product->product_id . "' ");
              $rs_images = $query->result();
              foreach ($rs_images as $thumb_img) { ?>
                <?php if (empty($thumb_img->image)) {
                  echo ""; ?>
                <?php } else { ?>
                  <div class="item">
                    <div class="img">
                      <img src="<?= base_url('uploads/thumb/' . $thumb_img->image) ?>" />
                    </div>
                  </div>
              <?php }
              } ?>
            </div><!--End slider-nav-->

          </div><!--End main-->

        </div><!--End md8-->
        <div class="col-lg-4">
          <div class="meta-detail-page">
            <?php $rs_dxp = $this->web_model->get_xuatphat($get_single_product->product_xuatphat); ?>

            <ul class="meta">
              <li><span class="meta-title">Thời gian:</span> <?= $get_single_product->product_thoigian; ?> </li>
              <li><span class="meta-title">Phương tiện:</span> <?= $get_single_product->product_phuongtien; ?></li>
              <li><span class="meta-title">Nơi khởi hành:</span> <?= $rs_dxp->xuatxu_name; ?> </li>
            </ul>
            <div id="fb-root"></div>
            <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v17.0" nonce="TWbN3xfu"></script>
            <div class="button-like-fb" style="margin-top: 20px"> <iframe src="https://www.facebook.com/plugins/like.php?href=<?= base_url($get_single_product->product_slug) . '.html'; ?>&width=140&layout=button_count&action=like&size=small&share=true&height=46&appId" width="140" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
            </div>

            <div>
              <div class="blog-media" style="padding: 15px;padding-left: 0;">
                <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5f076bf382708800123b41c4&product=inline-share-buttons" async="async"></script>
                <div class="sharethis-inline-share-buttons st-justified st-has-labels  st-inline-share-buttons st-animated" id="st-1">
                  <div class="sharethis-inline-share-buttons"></div>
                </div>
              </div>
            </div>
            <div class="star-rating-wrapper" style="margin-bottom: 15px;">
              <span class="score <?= $get_single_product->star; ?>"></span>
            </div>
            <div class="price">
              <!-- <div class='text-sale'><?= $get_single_product->product_dieukien; ?></div> -->

              <div class="block-prices block-prices-change mb-1">
                <?php
                if (floatValue($get_single_product->product_price_sale) == 0) {
                  echo "";
                } else {
                  echo '<div class="product-price font-30 mr-2 order-price">' . $get_single_product->product_price_sale . '₫</div>';
                } ?>

                <span class="product-old-price old-price">
                  <?php if (floatValue($get_single_product->product_price) == 0) {
                    echo "<span class='call_me'>Liên hệ </span>";
                  } else if (floatValue($get_single_product->product_price_sale) == 0) {
                    echo '<span class="product-price font-30 mr-2 order-price">' . $get_single_product->product_price . ' ₫' . '</span>';
                  } else {
                    echo '<del class="price-number" >' . $get_single_product->product_price . '₫'; ?>
                    </del>
                  <?php } ?>
                  <?php if (floatValue($get_single_product->product_price) == 0 || floatValue($get_single_product->product_price_sale) == 0) { ?>
                    <?php echo  ""; ?>
                  <?php } else { ?>

                  <?php } ?>
                </span>
              </div><!--end block-prices-->
            </div>
            <form action="<?= base_url('save/cart'); ?>" method="post">
              <input type="hidden" name="qty" value="1" class="quantity" />
              <input type="hidden" name="tour_xp" value="<?= $rs_dxp->xuatxu_name; ?>" />
              <input type="hidden" name="tour_time" value="<?= $get_single_product->product_thoigian; ?>" />
              <input type="hidden" name="tour_pt" value="<?= $get_single_product->product_phuongtien; ?>" />

              <input type="hidden" class="buyfield " id="get_product_id" name="product_id" value="<?= $get_single_product->product_id ?>" />
              <button type="submit" class="btn btn-booking" name="submit"> <i class="fa fa-cart-arrow-down"></i> Đặt tour</button>
            </form>
            <a target="_blank" href="<?= $get_single_product->download; ?>" class="btn btn-download"> <i class="fa fa-cloud-download"></i>Tải về </a>
          </div>
        </div>
      </div>
    </div>
    <div class="main-content row">
      <div class="col-lg-8">
        <div>
          <div class="row gap-30">
            <div class="col-12 col-lg-12 col-xl-12">
              <div class="content-wrapper" id="topwarp">
                <div id="detail-content-sticky-nav-timeline" class="fullwidth-horizon-sticky-section tour-over block-tab-product">
                  <div class="clear"></div>
                  <?php $j = 0;
                  foreach ($get_ct_tour_detail as $ct_tour) {
                    $j++;
                    if (isset($ct_tour->product_id) && !empty($ct_tour->date) && !empty($ct_tour->thoigian)) { ?>
                      <div class='item'>
                        <div class='title'><span class='date'><i class='fa fa-calendar'></i><span>NGÀY <?= $j; ?></span></span> <span class='content-title'><?= $ct_tour->date; ?></span></div>
                        <div class="clearfix"></div>
                        <div class='content'>
                          <ul class="meal-tour">
                            <?php if ($ct_tour->ansang == 'yes') { ?>
                              <li><img src="<?= base_url() ?>upload_content/an-sang.png" /><span>Ăn sáng</span></li>
                            <?php } else {
                              echo '';
                            } ?>
                            <?php if ($ct_tour->antrua == 'yes') { ?>
                              <li><img src="<?= base_url() ?>upload_content/an-trua.png" /><span>Ăn trưa</span></li>
                            <?php } else {
                              echo '';
                            } ?>
                            <?php if ($ct_tour->antoi == 'yes') { ?>
                              <li><img src="<?= base_url() ?>upload_content/an-toi.png" /><span>Ăn tối</span></li>
                            <?php } else {
                              echo '';
                            } ?>
                          </ul>
                          <div class="clear"></div>
                          <?= $ct_tour->thoigian; ?>

                        </div><!--End content-->
                        <div class="button-open-content"> <a href="#"> <span class="label-block-hide">Xem thêm</span></a> </div>

                      </div> <!--End item--->
                    <?php } else {
                      echo "Dữ liệu đang cập nhập....";
                    } ?>
                  <?php } ?>
                </div><!--End timeline-->

                <div class="clear"></div>

                <div id="detail-content-sticky-nav-05" class="fullwidth-horizon-sticky-section">
                  <div class="bd-example bd-inclusions-tabs">
                    <div class="tab-tours">
                      <nav>
                        <div class="nav nav-tabs tab-tours" id="nav-tab" role="tablist">
                          <a class="nav-item nav-link active" data-toggle="pill" href="#INCLUSIONS" role="tab" aria-controls="v-pills-home" aria-selected="true">Chính sách</a>
                          <a class="nav-item nav-link" data-toggle="pill" href="#lichtrinh" role="tab" aria-controls="v-pills-lichtrinh" aria-selected="true">Lịch trình tour</a>

                      </nav>
                      <div class="tab-content shadow" id="nav-tabContent">
                        <div class="tab-pane  active " id="INCLUSIONS" role="tabpanel" aria-labelledby="v-pills-inclusions-tab">
                          <?= $get_single_product->product_long_description; ?>
                        </div>

                        <div class="tab-pane" id="lichtrinh" role="tabpanel" aria-labelledby="v-pills-lichtrinh-tab">
                          <div class="hidden-xs table-responsive" id="no-more-tables">
                            <table class="table table-striped table-schedule">
                              <thead>
                                <tr>
                                  <th>Ngày</th>
                                  <th>Phương tiện</th>
                                  <th>Giá</th>
                                  <th>Tình Trạng</th>
                                  <th></th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach ($get_depart_detail as $depart) { ?> <tr>
                                    <td><?php if ($depart->date_begin == '') {
                                          echo '';
                                        } else {
                                          echo nice_date($depart->date_begin, 'd/m/Y');
                                        } ?></td>
                                    <td><?= $depart->move; ?></td>
                                    <td class="price"><?php if ($depart->price_people == 0) {
                                                        echo 'Liên hệ';
                                                      } else {
                                                        echo  number_format($depart->price_people) . '₫';
                                                      } ?></td>
                                    <td class="row-seat"> <span class="status_seat"><?= $depart->status; ?></span> </td>
                                    <td class="hide-row-button-book">
                                      <form action="<?= base_url('save/cart'); ?>" method="post">
                                        <input type="hidden" name="qty" value="1" />
                                        <input type="hidden" name="product_id" value="<?= $depart->product_id ?>" />
                                        <input type="hidden" name="tour_date_begin" value="<?= $depart->date_begin; ?>" />
                                        <input type="hidden" name="tour_price_people" value="<?= $depart->price_people; ?>" />
                                        <input type="hidden" name="tour_price_child" value="<?= $depart->price_child; ?>" />
                                        <input type="hidden" name="tour_price_baby" value="<?= $depart->price_baby; ?>" />
                                        <input type="hidden" name="tour_xp" value="<?= $rs_dxp->xuatxu_name; ?>" />
                                        <input type="hidden" name="tour_time" value="<?= $depart->product_thoigian; ?>" />
                                        <input type="hidden" name="tour_pt" value="<?= $depart->move; ?>" />
                                        <?php if ($depart->status == 'Hết chỗ' || $depart->status == 'hết chỗ' || $depart->status == '') { ?>
                                          <a class="btn vt-sumbit" style='background: gray;cursor: not-allowed;'>Hết chỗ</a>
                                        <?php } else { ?>
                                          <button type="submit" class="btn vt-sumbit">Đặt chỗ</button>
                                        <?php } ?>

                                      </form>
                                    </td>
                                  </tr>

                                <?php } ?>
                              </tbody>
                            </table>
                            <?php if (isset($get_depart_row->product_id) && !empty($get_depart_row->price_child) && !empty($get_depart_row->price_baby)) { ?>
                            <?php } else { ?>
                              <div class="not_calendar"></div>
                            <?php } ?>
                          </div>
                        </div><!--Enf tab lichtrinh-->

                      </div>
                    </div>
                  </div>
                </div>

                <div id="detail-content-sticky-nav-07" class="fullwidth-horizon-sticky-section">

                </div>


                <!-- End Hotel Reviews bars -->
                <!--===BEGIN: RATING & COMMENT==-->
                <div class="comment wrapper_comment" id="mystar">
                  <div class="title">Xếp hạng & Đánh giá </div>
                  <p class="title_p">Chúng tôi rất mong nhận được phản hồi từ bạn.</p>
                  <div class="row">
                    <div class="col-sm-12 col-xs-12">
                      <div class="rating-block">
                        <?php
                        $rs_count = $this->web_model->rs_count_total_star($get_single_product->product_id);
                        $rs_comment_star = $this->web_model->rs_comment_star($get_single_product->product_id);
                        $rs_count_total_star = $this->web_model->rs_count_total_star($get_single_product->product_id);
                        if ($rs_comment_star->sum == "" || $rs_count->count == 0) {
                          echo "<input class='rating rating-loading' data-min='0' data-max='5' value='0' />";
                        } else {
                          $tatol_star = $rs_comment_star->sum;
                          $tatol_people = $rs_count_total_star->count;
                          $tatol = $tatol_star / $tatol_people;
                        ?>
                          <span class="count_star">
                            <?= round($tatol, 1, PHP_ROUND_HALF_UP);  ?> </span>
                          <input class="rating rating-loading" data-min="0" data-max="5" value="<?= round($tatol, 0, PHP_ROUND_HALF_UP);  ?>" />
                        <?php } ?>
                      </div>
                    </div>
                    <div class="col-sm-5 col-xs-12 hidden">
                      <?php
                      $rs_count_star5 = $this->web_model->group_star_5($get_single_product->product_id);
                      $rs_count_star4 = $this->web_model->group_star_4($get_single_product->product_id);
                      $rs_count_star3 = $this->web_model->group_star_3($get_single_product->product_id);
                      $rs_count_star2 = $this->web_model->group_star_2($get_single_product->product_id);
                      $rs_count_star1 = $this->web_model->group_star_1($get_single_product->product_id);
                      ?>
                      <div class="pull-left wrap_progress">
                        <div class="pull-left progress_star">
                          <div>5 <span class="glyphicon glyphicon-star"></span></div>
                        </div>
                        <div class="pull-left line">
                          <div class="progress">
                            <?php
                            if (empty($rs_count_star5->id_star)) {
                              echo "";
                            } else {
                              $get_5 = $rs_count_star5->id_star;
                              $tatol_5 = $tatol * $get_5;
                            ?>
                              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width:<?= $tatol_5; ?>%">
                                <span class="sr-only"><?= $tatol_5; ?>% Complete (danger)</span>
                              </div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="right_star">
                          <?php if (empty($rs_count_star5->id_star)) {
                            echo ""; ?>
                          <?php } else { ?>
                            <?= $rs_count_star5->id_star; ?> Rate <?php } ?></div>
                      </div>
                      <div class="pull-left wrap_progress">
                        <div class="pull-left progress_star">
                          <div>4 <span class="glyphicon glyphicon-star"></span></div>
                        </div>
                        <div class="pull-left line">
                          <div class="progress">
                            <?php
                            if (empty($rs_count_star4->id_star)) {
                              echo "";
                            } else {
                              $get_4 = $rs_count_star4->id_star;
                              $tatol_4 = $tatol * $get_4;
                            ?>
                              <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width:<?= $tatol_4; ?>%">
                                <span class="sr-only"><?= $tatol_4; ?>% Complete (danger)</span>
                              </div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="right_star"> <?php if (empty($rs_count_star4->id_star)) {
                                                    echo ""; ?>
                          <?php } else { ?>
                            <?= $rs_count_star4->id_star; ?> Rate <?php } ?></div>
                      </div>
                      <div class="pull-left wrap_progress">
                        <div class="pull-left progress_star">
                          <div>3 <span class="glyphicon glyphicon-star"></span></div>
                        </div>
                        <div class="pull-left line">
                          <div class="progress">
                            <?php
                            if (empty($rs_count_star3->id_star)) {
                              echo "";
                            } else {
                              $get_3 = $rs_count_star3->id_star;
                              $tatol_3 = $tatol * $get_3;
                            ?>
                              <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5" style="width:<?= $tatol_3; ?>%">
                                <span class="sr-only"><?= $tatol_3; ?>% Complete (danger)</span>
                              </div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="right_star"> <?php if (empty($rs_count_star3->id_star)) {
                                                    echo ""; ?>
                          <?php } else { ?>
                            <?= $rs_count_star3->id_star; ?> Rate <?php } ?></div>
                      </div>
                      <div class="pull-left wrap_progress">
                        <div class="pull-left progress_star">
                          <div>2 <span class="glyphicon glyphicon-star"></span></div>
                        </div>
                        <div class="pull-left line">
                          <div class="progress">
                            <?php
                            if (empty($rs_count_star2->id_star)) {
                              echo "";
                            } else {
                              $get_2 = $rs_count_star2->id_star;
                              $tatol_2 = $tatol * $get_2;
                            ?>
                              <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width:<?= $tatol_2; ?>%">
                                <span class="sr-only"><?= $tatol_2; ?>% Complete (danger)</span>
                              </div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="right_star"> <?php if (empty($rs_count_star2->id_star)) {
                                                    echo ""; ?>
                          <?php } else { ?>
                            <?= $rs_count_star2->id_star; ?> Rate <?php } ?></div>
                      </div>
                      <div class="pull-left wrap_progress">
                        <div class="pull-left progress_star">
                          <div>1 <span class="glyphicon glyphicon-star"></span></div>
                        </div>
                        <div class="pull-left line">
                          <div class="progress">
                            <?php
                            if (empty($rs_count_star1->id_star)) {
                              echo "";
                            } else {
                              $get_1 = $rs_count_star1->id_star;
                              $tatol_1 = $tatol * $get_1;
                            ?>
                              <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width:<?= $tatol_1; ?>%">
                                <span class="sr-only"><?= $tatol_1; ?>% Complete (danger)</span>
                              </div>
                            <?php } ?>
                          </div>
                        </div>
                        <div class="right_star"> <?php if (empty($rs_count_star1->id_star)) {
                                                    echo ""; ?>
                          <?php } else { ?>
                            <?= $rs_count_star1->id_star; ?> Rate <?php } ?></div>
                      </div>
                    </div>
                  </div>
                  <div class="formComment">
                    <form id="formCommnet" enctype="multipart/form-data" method="post">
                      <div class="w_content">
                        <div class="comment_moblie">
                          <div class="form-group">
                            <label class="i_danhna  control-label" for="stars">Viết 1 đánh giá</label>
                            <div class="clear"></div>
                            <div class="info-title">Chọn đánh giá sao:</div>
                            <div class="feedback">
                              <div class="rating">
                                <input type="radio" id="star5" required name="comment_start" value="5" /><label for="star5"></label>
                                <input type="radio" id="star4" required name="comment_start" value="4" /><label for="star4"></label>
                                <input type="radio" id="star3" required name="comment_start" value="3" /><label for="star3"></label>
                                <input type="radio" id="star2" required name="comment_start" value="2" /><label for="star2"></label>
                                <input type="radio" id="star1" required name="comment_start" value="1" /><label for="star1"></label>
                              </div>
                            </div>
                            <div class="clear"></div>
                            <!--  <div class="info-title">Tiêu đề:</div>
        <input type="text" required name="comment_title" class="form-control"  /> -->
                            <div class="info-title">Họ Tên:</div>
                            <input type="text" required name="comment_name" class="form-control" />
                            <div class="info-title">Nội dung:</div>
                            <textarea required name="comment_content" class="form-control "></textarea>
                            <input type="hidden" name="comment_product_id" value="<?= $get_single_product->product_id ?>" />
                            <button id="comment_content" type="submit" class="btnsumit bnt-send">Gửi</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <!--===BEGIN: COMMENT==-->
                  <div class="comment wrapper_comment" id="cmt">
                    <div class="formComment-binhluan">
                      <form id="formCommnetproduct" method="post">
                        <div class="w_content">
                          <div class="form-group">
                            <label class="i_danhna  control-label">Viết 1 bình luận</label>
                            <div class="clear"></div>
                            <div class="info-title">Email:</div>
                            <input type="text" required name="comment_email" class="form-control" />
                            <div class="info-title">Họ Tên:</div>
                            <input type="text" required name="comment_name" class="form-control" />
                            <input type="hidden" name="product_id" value="<?= $get_single_product->product_id ?>" />
                            <div class="info-title">Nội dung:</div>
                            <textarea required name="comment_content" class="form-control "></textarea>
                            <input type="hidden" name="comment_product_id" value="<?= $get_single_product->product_id ?>" />
                            <button type="submit" name="btn-search" class="btn">Gửi</button>
                          </div>
                        </div><!--end w_content-->
                      </form>
                    </div>
                  </div>
                  <!--===BEGIN: COMMENT==-->
                  <div class="clear"></div>
                  <section id="tabs" class="project-tab">
                    <nav>
                      <ul class="nav nav-tabs tab_review" id="nav-tab" role="tablist">
                        <li class="active"> <a class="nav-item nav-link" data-toggle="tab" href="#danhgia" role="tab">Đánh giá</a></li>
                        <li> <a class="nav-item nav-link" data-toggle="tab" href="#binhluan" role="tab">Bình luận </a></li>
                      </ul>
                    </nav>
                    <div class="tatol_right">
                      <input class="btn bnt-send" type="button" value="Đánh giá của bạn" id="bntsend">
                      <input class="btn bnt-send" type="button" value="Bình luận của bạn" id="bntcomment">
                    </div> <!--end tatol_right-->
                    <div class="tab-content" id="nav-tabContent">
                      <div class="tab-pane  active" id="danhgia" role="tabpanel">
                        <div class="grid-comment">
                          <?php $rs_start = $this->web_model->show_result_star($get_single_product->product_id);
                          foreach ($rs_start as $v) { ?>
                            <div class="node-commnet">
                              <div class="avatar">
                                <span class="yotpo-user-letter"><?= $str_name = substr($v->comment_name, 0, 1); ?></span>
                                <span class="yotpo-icon yotpo-icon-circle-checkmark yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip" data-target-container="yotpo-header"></span>
                              </div>
                              <div class="info-comment">
                                <div class="info-preson">
                                  <div class="name"><?= $v->comment_name; ?> <span class="i-confirm">Đã đặt tour tại <?= get_option('site_website'); ?></span> </div>
                                  <span class="email">
                                    <input class="rating rating-loading " data-min="0" data-max="5" value="<?= round($v->comment_start, 0, PHP_ROUND_HALF_UP);  ?>" />
                                    <div class="timer_comment"><?= nice_date($v->comment_date, 'h:i'); ?> | <?= nice_date($v->comment_date, 'd/m/Y'); ?></div>
                                </div>
                                <div class="comment">
                                  <?= $v->comment_content; ?>
                                </div>
                                <div class="image_comment">
                                  <?php if (empty($v->comment_images)) {
                                    echo "";
                                  } else { ?>
                                    <a class="iframe" href="<?= base_url('uploads/comment/' . $v->comment_images); ?>">
                                      <img src="<?= base_url('uploads/comment/thumb/' . $v->comment_images); ?>" />
                                    </a>
                                  <?php } ?>
                                </div>
                                <?php if ($v->comment_show_answer == 1) { ?>
                                  <div class="div_answer">
                                    <div class="info_answer"> <b class="qtv">Quản trị viên</b>
                                      Trả lời : </div>
                                    <div><?= $v->comment_answer; ?></div>
                                  </div>
                                <?php } else {
                                  echo "";
                                } ?>
                              </div>
                              <div class="clear"></div>
                            </div><!--end node-->
                          <?php } ?>
                        </div><!--end gird comment-->
                        <div class="wap_dg_medium">
                          <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="rating-block">
                              <?php
                              if ($rs_comment_star->sum == "" || $rs_count->count == 0) {
                                echo "<div class='title-rating-none'> Chưa có đánh giá </div>";
                              } else {
                                $tatol_star = $rs_comment_star->sum;
                                $tatol_people = $rs_count->count;
                                $tatol = $tatol_star / $tatol_people; ?>
                                <h4 class="title-rating">Đánh giá trung bình</h4>
                                <p class="score"><?= round($tatol, 1, PHP_ROUND_HALF_UP); ?>/5</p>
                                <P>
                                  <input class="rating rating-loading " data-min="0" data-max="5" value="<?= round($tatol, 0, PHP_ROUND_HALF_UP);  ?>" />
                                </P>
                                <a class="number">(<?= $rs_count_total_star->count ?> Đánh giá)</a>
                              <?php } ?>

                            </div>
                          </div>
                          <div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="pull-left">
                              <div class="pull-left number-star">
                                <input class="rating rating-loading " data-min="0" data-max="5" value="5" />
                              </div>
                              <div class="pull-left process-width">
                                <div class="progress">
                                  <?php
                                  if (empty($rs_count_star5->id_star)) {
                                    echo "";
                                  } else {
                                    $get_5 = $rs_count_star5->id_star;
                                    $tatol_5 = 5 * $get_5; ?>
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width:<?= $tatol_5; ?>%">
                                      <span class="sr-only"><?= $tatol_5; ?>% Complete (danger)</span>
                                    </div>
                                  <?php } ?>
                                </div>
                              </div>
                              <div class="right_star">
                                <?php if (empty($rs_count_star5->id_star)) {
                                  echo ""; ?>
                                <?php } else { ?>
                                  <?= $rs_count_star5->id_star; ?> Sao <?php } ?></div>
                            </div>
                            <div class="pull-left">
                              <div class="pull-left number-star">
                                <input class="rating rating-loading " data-min="0" data-max="5" value="4" />
                              </div>
                              <div class="pull-left process-width">
                                <div class="progress">
                                  <?php
                                  if (empty($rs_count_star4->id_star)) {
                                    echo "";
                                  } else {
                                    $get_4 = $rs_count_star4->id_star;
                                    $tatol_4 = 4 * $get_4; ?>
                                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width:<?= $tatol_4; ?>%">
                                      <span class="sr-only"><?= $tatol_4; ?>% Complete (danger)</span>
                                    </div>
                                  <?php } ?>
                                </div>
                              </div>
                              <div class="right_star"> <?php if (empty($rs_count_star4->id_star)) {
                                                          echo ""; ?>
                                <?php } else { ?>
                                  <?= $rs_count_star4->id_star; ?> Sao <?php } ?></div>
                            </div>
                            <div class="pull-left">
                              <div class="pull-left number-star">
                                <input class="rating rating-loading " data-min="0" data-max="5" value="3" />
                              </div>
                              <div class="pull-left process-width">
                                <div class="progress">
                                  <?php
                                  if (empty($rs_count_star3->id_star)) {
                                    echo "";
                                  } else {
                                    $get_3 = $rs_count_star3->id_star;
                                    $tatol_3 = 3 * $get_3; ?>
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5" style="width:<?= $tatol_3; ?>%">
                                      <span class="sr-only"><?= $tatol_3; ?>% Complete (danger)</span>
                                    </div>
                                  <?php } ?>
                                </div>
                              </div>
                              <div class="right_star"> <?php if (empty($rs_count_star3->id_star)) {
                                                          echo ""; ?><?php } else { ?>
                                <?= $rs_count_star3->id_star; ?> Sao <?php } ?></div>
                            </div>
                            <div class="pull-left">
                              <div class="pull-left number-star">
                                <input class="rating rating-loading " data-min="0" data-max="5" value="2" />
                              </div>
                              <div class="pull-left process-width">
                                <div class="progress">
                                  <?php
                                  if (empty($rs_count_star2->id_star)) {
                                    echo "";
                                  } else {
                                    $get_2 = $rs_count_star2->id_star;
                                    $tatol_2 = 2 * $get_2; ?>
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width:<?= $tatol_2; ?>%">
                                      <span class="sr-only"><?= $tatol_2; ?>% Complete (danger)</span>
                                    </div>
                                  <?php } ?>
                                </div>
                              </div>
                              <div class="right_star"> <?php if (empty($rs_count_star2->id_star)) {
                                                          echo ""; ?>
                                <?php } else { ?>
                                  <?= $rs_count_star2->id_star; ?> Sao <?php } ?></div>
                            </div>
                            <div class="pull-left">
                              <div class="pull-left number-star">
                                <input class="rating rating-loading " data-min="0" data-max="5" value="1" />
                              </div>
                              <div class="pull-left process-width">
                                <div class="progress">
                                  <?php
                                  if (empty($rs_count_star1->id_star)) {
                                    echo "";
                                  } else {
                                    $get_1 = $rs_count_star1->id_star;
                                    $tatol_1 = 1 * $get_1; ?>
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width:<?= $tatol_1; ?>%">
                                      <span class="sr-only"><?= $tatol_1; ?>% Complete (danger)</span>
                                    </div>
                                  <?php } ?>
                                </div>
                              </div>
                              <div class="right_star"> <?php if (empty($rs_count_star1->id_star)) {
                                                          echo ""; ?><?php } else { ?>
                                <?= $rs_count_star1->id_star; ?> Sao <?php } ?></div>
                            </div>
                          </div>
                        </div><!--end wap_dg_medium-->
                      </div><!--end tab-->

                      <div class="tab-pane " id="binhluan" role="tabpanel">
                        <div class="grid-comment">
                          <?php $rs_comment = $this->web_model->show_result_comment($get_single_product->product_id);
                          foreach ($rs_comment as $v) { ?>
                            <div class="node-commnet">
                              <div class="avatar">
                                <span class="yotpo-user-letter"><?= $str_name = substr($v->comment_name, 0, 1); ?></span>
                                <span class="yotpo-icon yotpo-icon-circle-checkmark yotpo-action-hover" data-type="toggleFade" data-target="yotpo-tool-tip" data-target-container="yotpo-header"></span>
                              </div>
                              <div class="info-comment">
                                <div class="info-preson">
                                  <span class="name"><?= $v->comment_name; ?> </span>
                                </div>
                                <div class="comment">
                                  <?= $v->comment_content; ?>
                                </div>
                                <?php if ($v->comment_show_answer == 1) { ?>
                                  <div class="div_answer">
                                    <div class="info_answer"> <b class="qtv">Quản trị viên</b>
                                      Trả lời : </div>
                                    <div><?= $v->comment_answer; ?></div>
                                  </div>
                                <?php } else {
                                  echo "";
                                } ?>
                              </div>
                              <div class="clear"></div>
                            </div><!--end node-->
                          <?php } ?>
                        </div><!--end gird-coment-->
                      </div><!--end tab-->
                    </div>
                  </section>
                </div>
                <!--===END: RATING & COMMENT==-->
                <!-- End Add/Remove Wish list Review Section -->
              </div>
            </div>
          </div>
        </div>

      </div>
      <div class="col-lg-4 siderbar">

        <div class="widget box-schedule" id="box-schedule">
          <div class="title">
            <h4>Đặt Tour / Booking Tour</h4>
          </div>
          <div class="widget-content shadow_1 padding-15">
            <form method="POST" action="<?php echo base_url('danky-tu-van-tour.html'); ?>" id="booking-form">
              <input type="hidden" name="muanhanh_product_id" value="<?= $get_single_product->product_id ?>" />
              <input type="hidden" name="get_slug_tour" value="<?= $get_single_product->product_slug ?>" />
              <div class="form-group"> <input type="text" name="muanhanh_name" id="cname" placeholder="Nhập Họ tên" class="form-control" required="required"> </div>
              <div class="form-group"> <input type="email" name="muanhanh_email" id="cemail" placeholder="Nhập email" class="form-control" required="required"> </div>
              <div class="form-group"> <input type="text" name="muanhanh_phone" id="ctel" placeholder="Số điện thoại" class="form-control" required="required"> </div>
              <div class="form-group">
                <div class="row"> <label class="col-sm-12 label-c">Ngày khởi hành</label>
                  <div class="col-sm-12">
                    <select class="form-control" name="daykh" id="cschedule">
                      <option>Chọn</option>
                      <?php foreach ($get_depart_detail as $depart) {
                        if (isset($depart->product_id) || !empty($depart->price_child) || !empty($depart->price_baby)) { ?>
                          <option value="<?= nice_date($depart->date_begin, 'd/m/Y'); ?> - <?= number_format($depart->price_people) . '₫'; ?>"><?= nice_date($depart->date_begin, 'd/m/Y'); ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (<?= number_format($depart->price_people) . '₫'; ?>)</option>
                        <?php } else {
                          echo "";
                        } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row"> <label class="col-sm-12 label-c">Ngày dự kiến (nếu có)</label>
                  <div class="col-sm-12"> <input type="date" id="DateTours" placeholder="Ngày dự kiến" class="DateTours form-control form-readonly-control" placeholder="dd/mm/yyyy" value="" name="daydk"> </div>
                </div>
              </div>
              <div class="form-group">
                <div class="row"> <label class="col-sm-12 label-c">Số lượng khách dự kiến</label>
                  <div class="col-sm-12"> <input type="number" id="sl_adult" name="soluong" placeholder="Số lượng" min="1" step="1" oninput="this.value = Math.abs(this.value)" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="form-control require" value="1"> </div>
                </div>
              </div>
              <div class="form-group"> <textarea class="form-control" name="note" placeholder="Ghi chú"></textarea> </div>

              <div class="form-group" style="text-align: right"> <button class="btn btn-primary" name="submit_booking" type="submit" value="save">Hoàn tất </button> </div>
            </form>
          </div>
        </div>

        <div class="clear"></div>
        <div class="widget news">
          <div class="widget-title">
            <h4>Tin tức</h4>
          </div>
          <div class="widget-content shadow">
            <?php
            $id_dm_news = 86;
            $limit_news = 5;
            $order_news = "asc";
            $get_news_right = $this->web_model->get_news_top_home($id_dm_news, $limit_news, $order_news);
            foreach ($get_news_right as $news) { ?>
              <div class="single-blog-post d-flex">
                <div class="post-thumbnail shadow"> <a href="<?= base_url($news->dmtin_slug) . '.html'; ?>">
                    <img src="<?= base_url('uploads/news/' . $news->dmtin_image) ?>" alt="<?= $news->dmtin_title ?>" title="<?= $news->dmtin_title ?>"></a> </div>
                <div class="post-content">
                  <h3> <a href="<?= base_url($news->dmtin_slug) . '.html'; ?>" class="post-title"><?= $news->dmtin_title ?></a></h3>
                  <div class="single-blog-bottom"> <a href="<?= base_url($news->dmtin_slug) . '.html'; ?>" class="btn btn-view-detail"> Xem chi tiết </a> </div>
                </div>
              </div>
            <?php } ?>

          </div><!--End widget-content-->
        </div><!--End widget hotro-->
        <div class="clear"></div>
        <div class="widget news">
          <div class="widget-title">
            <h4>Hỗ trợ khách hàng</h4>
          </div>
          <div class="widget-content shadow">
            <p>Phone: <strong><?= get_option('company_number'); ?></strong></p>
            <p>Zalo: <strong><?= get_option('company_hotline'); ?></strong></p>
            <p>Email: <strong><?= get_option('company_email'); ?></strong></p>
            <p>Để lại thông tin Amara sẽ gọi lại tư vấn</p>
          </div><!--End widget-content-->
        </div><!--End widget hotro-->

      </div><!--End md-4-->
      <div class="clear"></div>

      <div class="box-tour-relate col-lg-12">
        <h2 class="heading-1">
          <span>Tour liên quan</span>
        </h2>
        <div class="widget-content ">
          <ul class="grid-tour-01" style="display: flex;flex-wrap:wrap">
            <?php $rs_other = $this->web_model->show_product_other($get_single_product->product_id, $get_single_product->id);
            foreach ($rs_other as $product) { ?>
              <li class="col-md-4 col-xs-6 col-sm-6">
                <div class="item shadow">
                  <div class="item-top">
                    <a class="hvr-bounce-to-right" href="<?= base_url($product->product_slug) . '.html'; ?>"><img src="<?= base_url('uploads/product/thumb/' . $product->product_image) ?>" alt="<?= $product->product_title; ?>" title="<?= $product->product_title; ?>"></a>
                  </div>
                  <div class="item-bottom">
                    <!-- <div class="xinfo clearfix">
                      <div> <span> <i class="fa fa-calendar"></i> <span><?= $product->star . ' SAO'; ?></span> </span> </div>
                      <div> <span> <i class="fa fa-clock-o"></i> <span><?= $product->product_thoigian; ?></span> </span> </div>
                      <div> <span> <i class="fa fa-paper-plane"></i> <span><?= $product->product_phuongtien; ?></span> </span> </div>
                    </div> -->
                    <h3 class="title"><a href="<?= base_url($product->product_slug) . '.html'; ?>"><?= $product->product_title; ?></a> </h3>
                    <div class="i-promotion">
                      <?php if (floatValue($product->product_price_sale) == 0) {
                        echo "";
                      } else { ?>
                        <span><?php echo $product->product_price_sale; ?>₫</span>
                      <?php } ?>
                      <?php if (floatValue($product->product_price) == 0) {
                        echo "<span class='call_me'>Liên hệ </span>";
                      } else if (floatValue($product->product_price_sale) == 0) {
                        echo ' <span>' . $product->product_price . ' ₫' . '</span>';
                      } else {
                        echo '<div class=i-price>' .  $product->product_price . '₫'; ?>
                    </div>
                  <?php } ?>
                  </div><!--end i-promtion-->
                  <ul class="meta">
                    <li> <i class="fa fa-clock-o"></i> <?= $product->product_thoigian; ?> </li>
                    <li> <i class="fa fa-plane"></i> <?= $product->product_phuongtien; ?> </li>
                    <li class="flex"> <i class="fa fa-calendar inside inside-left"></i>
                      <span class="schedule inside inside-right">
                        <?php $get_schedule = $this->web_model->get_depart_schedule_result($product->product_id);
                        foreach ($get_schedule as $key => $v_sche) { ?>
                          <a class='btn vt-sumbits'><?php if ($v_sche->date_begin == '') {
                                                      echo '';
                                                    } else {
                                                      echo nice_date($v_sche->date_begin, 'd/m') . ',';
                                                    } ?></a>
                        <?php } ?>

                      </span>
                    </li>
                  </ul>
                </div>
        </div>
        </li>
      <?php } ?>
      </ul>
      </div>
    </div>
    <div class="clear"></div>


  </div>
</div>
</div>

<div class="clear"></div>
<!--=== END: CONTENT ===-->
<script type="text/javascript">
  $(document).ready(function() {
    $('#formCommnet').on('submit', function() {
      $.ajax({
        url: "<?= base_url('comment.html '); ?>",
        method: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(data) {
          $('.result').html(data);
        }
      })
    });

    $('#formCommnetproduct').on('submit', function() {
      $.ajax({
        url: "<?= base_url('commentproduct.html '); ?>",
        method: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function(data) {
          $('.result').html(data);
        }
      })
    });
  });
</script>