<link href="<?= base_url() ?>assets/web/css/main.css" rel="stylesheet" type="text/css" /> 
<div class="result"><?= $this->session->flashdata('message'); ?></div> 
<div class="wap_slider vnt-banner"> 
    <?php foreach ($get_slider as $slider) { ?> 
        <div class="item"> 
 
            <div class="img"><a href="<?= $slider->slider_link; ?>"><img src="<?= base_url() ?>uploads/<?= $slider->slider_image; ?>" alt="<?= $slider->slider_title; ?>" title="<?= $slider->slider_title; ?>"></a> 
 
            </div> 
 
        </div> 
 
    <?php } ?> 
 
</div> 
<!--End wap_slider--> 
<div class="container"> 
    <?php $this->load->view('web/inc/search_tour.php'); ?> 
 
    <div class="row"> 
        <?php foreach ($categories as $v) { 
 
            $id = $v->id; 
            // $rs_child = $this->web_model->sub_category($v->id); 
            // $data =[]; 
            // foreach ($rs_child as $id_child) { 
            //  $data[] = $id_child->id; 
            // $cate_by_product = $this->web_model->get_product_cate($data); 
 
            // } 
            $cate_by_product = $this->web_model->get_product_cate($id); ?> 
            <div class="box-tour-relate col-lg-12"> 
                <h2 class="heading-1"> 
                    <span><?= $v->category_name ?></span> 
                </h2> 
                <div class="widget-content "> 
                    <ul class="grid-tour-01" style="display: flex;flex-wrap:wrap"> 
                        <?php foreach ($cate_by_product as $product) { ?> 
                            <li class="col-md-4 col-xs-12 col-sm-6"> 
                                <div class="item shadow"> 
                                    <div class="item-top"> 
                                        <a class="hvr-bounce-to-right" href="<?= base_url($product->product_slug) . '.html'; ?>"><img src="<?= base_url('uploads/product/thumb/' . $product->product_image) ?>" alt="<?= $product->product_title; ?>" title="<?= $product->product_title; ?>"></a> 
                                    </div> 
                                    <div class="item-bottom"> 
                                        <!-- <div class="xinfo clearfix"> 
                                            <div> <span> <i class="fa fa-calendar"></i> 
                                                    <span><?= $product->star . ' SAO'; ?></span> </span> </div> 
                                            <div> <span> <i class="fa fa-clock-o"></i> 
                                                    <span><?= $product->product_thoigian; ?></span> </span> </div> 
                                            <div> <span> <i class="fa fa-paper-plane"></i> 
                                                    <span><?= $product->product_phuongtien; ?></span> </span> </div> 
                                        </div> --> 
                                        <h3 class="title"><a href="<?= base_url($product->product_slug) . '.html'; ?>"><?= $product->product_title; ?></a> 
                                        </h3> 
                                        <div class="i-promotion"> 
                                            <?php if (floatValue($product->product_price_sale) == 0) { 
                                                echo ""; 
                                            } else { ?> 
                                                <span><?php echo $product->product_price_sale; ?>₫</span> 
                                            <?php } ?> 
                                            <?php if (floatValue($product->product_price) == 0) { 
                                                echo "<span class='call_me'>Liên hệ </span>"; 
                                            } else if (floatValue($product->product_price_sale) == 0) { 
                                                echo ' <span>' . $product->product_price . ' ₫' . '</span>'; 
                                            } else { 
                                                echo '<div class=i-price>' .  $product->product_price . '₫'; ?> 
                                        </div> 
                                    <?php } ?> 
                                    </div> 
                                    <!--end i-promtion--> 
                                    <div class="bottom_tour"> 
                                        <ul class="meta"> 
                                            <li> <i class="fa fa-clock-o"></i> <?= $product->product_thoigian; ?> </li> 
                                            <li> <i class="fa fa-plane"></i> <?= $product->product_phuongtien; ?> </li> 
                                            <li class="flex"> <i class="fa fa-calendar inside inside-left"></i> 
                                                <span class="schedule inside inside-right"> 
                                                    <?php $get_schedule = $this->web_model->get_depart_schedule_result($product->product_id); 
                                                    foreach ($get_schedule as $key => $v_sche) { ?> 
                                                        <a class='btn vt-sumbit'><?php if ($v_sche->date_begin == '') { 
                                                                                        echo ''; 
                                                                                    } else { 
                                                                                        echo nice_date($v_sche->date_begin, 'd/m') . ','; 
                                                                                    } ?></a> 
                                                    <?php } ?> 
 
                                                </span> 
                                            </li> 
                                        </ul> 
                                        <div class="btn_tour" style="    display: flex;justify-content: end;padding: 0 10px;"> 
                                            <a href="<?= base_url($product->product_slug) . '.html'; ?>" class="btn-view-detailHome">Đặt Tour</a> 
                                        </div> 
                                    </div> 
                                </div> 
                </div> 
                </li> 
            <?php } ?> 
            </ul> 
            </div> 
    </div> 
    <div class="clear"></div> 
 
<?php } ?> 
 
<div class="clear"></div> 
<section class="qc-home"> 
    <div class="col-md-8"> 
        <?php 
        $banner_QC_home = $this->web_model->get_all_post_1baiviet(106); 
        foreach ($banner_QC_home as $key => $v) { 
            if ($key == 0) { ?> 
                <div class="banner_1"><a class="style-adv" href="<?= $v->one_news_slug; ?>" target="_blank"><img alt="<?= $v->one_news_title_bar; ?>" class="img-responsive shadow" src="<?= base_url() ?>uploads/<?php echo $v->one_news_image; ?>"></a></div> 
            <?php } else { 
                echo ''; 
            } ?> 
        <?php } ?> 
    </div> 
    <div class="col-md-4"> 
        <?php 
        $banner_QC_home = $this->web_model->get_all_post_1baiviet(106); 
        foreach ($banner_QC_home as $key => $v) { 
            if ($key == 0) { 
                echo ''; 
            } else { ?> 
                <div class="banner_2"><a class="style-adv" href="<?= $v->one_news_slug; ?>" target="_blank"><img alt="<?= $v->one_news_title_bar; ?>" class="img-responsive shadow" src="<?= base_url() ?>uploads/<?php echo $v->one_news_image; ?>"></a> </div> 
        <?php } 
        } ?> 
    </div> 
</section> 
</div> 
<!--End row--> 
 
</div> 
<div class="vnt-news bg-news wrap"> 
    <div class="container"> 
        <div class="gird-new_home noborder row"> 
            <div class="wrap-grid"> 
                <div class="col-md-12 col-xs-12 col-sm-12"> 
                    <div class="row"> 
                        <div class="wrap-header"> 
                            <h2 class="wrap-title">BLOG DU LỊCH</h2> 
                        </div> 
 
                        <div class="clear"></div> 
                        <?php 
 
                        $id_dm_news = 86; 
                        $col_news = 3; 
                        $limit_news = 6; 
                        $order_news = "asc"; 
                        $get_news_noibat_home = $this->web_model->get_news_noibat_home($id_dm_news, $limit_news, $order_news); 
                        foreach ($get_news_noibat_home as $value) { ?> 
                            <div class="item col-<?php echo $col_news; ?> col-xs-12"> 
                                <div class="wap-item"> 
                                    <div class="i-image img-news"> 
                                        <a href="<?php echo base_url($value->dmtin_slug) . '.html'; ?>"> 
                                            <img src="<?php echo base_url('uploads/news/' . $value->dmtin_image) ?>" alt="<?php echo $value->dmtin_title ?>" title="<?php echo $value->dmtin_title ?>"> 
                                        </a> 
                                    </div> 
                                    <!--End img--> 
                                    <div class="i-desc"> 
                                        <div class="i-title"> 
                                            <h3> 
                                                <a href="<?php echo base_url($value->dmtin_slug) . '.html'; ?>"> 
                                                    <?php echo word_limiter($value->dmtin_title, 28) ?></a> 
                                            </h3> 
                                        </div> 
                                        <div class="clear"></div> 
                                        <div class="mota"> <i class="fa fa-map-marker"></i> 
                                            <?php echo word_limiter($value->dmtin_long_description, 14) ?></div> 
                                    </div> 
                                    <!--end i-desc--> 
                                </div> 
                                <!--End wap -item--> 
                            </div> 
 
                        <?php   } ?> 
 
                        <div class="clear"></div> 
                        <div class="viewallcat"> 
                            <a href="<?= base_url('tin-tuc-su-kien') . '.html'; ?>" class="mobile">Xem thêm</span></a> 
                        </div> 
                        <!--End viewallcat --> 
                    </div> 
                    <!--End row--> 
                </div> 
                <!--End md-12--> 
            </div> 
            <!--End wrap-grid--> 
        </div> 
        <!--end gird-news--> 
    </div> 
</div> 
<!--end vnt-news--> 
<?php if (empty($gallerys)) { ?> 
    <div></div> 
<?php  } else { ?> 
    <section class="garely"> 
        <div class=""> 
            <div class="title-garely"> 
                <h2>Gallery</h2> 
            </div> 
            <div class="gallery_wrap"> 
                <?php 
                foreach ($gallerys as $g) { ?> 
                    <div class="item"> 
                        <div class="img"> 
                            <img src="<?= base_url() ?>uploads/<?= $g->one_news_image; ?>" alt=""> 
                        </div> 
                    </div> 
                <?php } ?> 
            </div> 
        </div> 
    </section> 
<?php } ?> 
 
 
 
 
 
<div id="modal"> 
    <?php $qc_amara = $this->web_model->get_all_one_news_post_home(73); ?> 
    <a href="<?= $qc_amara->one_news_slug; ?>" target="_blank"><img alt="<?= $qc_amara->one_news_title; ?>" class="img-responsive shadow" src="<?= base_url() ?>uploads/<?php echo $qc_amara->one_news_image; ?>"></a> 
    <a class="closez" href="#" onclick="window.hideModal()">Đóng</a> 
</div> 
 
<script> 
    // set a cookie on the load event, which expires after a year 
 
    window.onload = function() { 
        var expiryTime = new Date(); 
        expiryTime.setFullYear(expiryTime.getFullYear() + 1); 
        document.cookie = "visited=true; expires=" + expiryTime.toUTCString() + ";"; 
        console.log(document.cookie); 
    } 
 
    // function to hide the modal div 
 
    window.hideModal = function() { 
        document.getElementById("modal").style.display = "none"; 
    } 
 
    // read the cookie and hide the modal div if the value of visitedKey is true 
 
    function inspectCookie() { 
        var visitedKey = "visited"; 
        var ca = document.cookie.split(';'); 
        for (var i = 0; i < ca.length; i++) { 
            var c = ca[i]; 
            while (c.charAt(0) == ' ') c = c.substring(1, c.length); 
            if (c.indexOf(visitedKey) !== -1) { 
                window.hideModal() 
            } 
        } 
    } 
 
    // run the cookie checking function as soon as the parser hits it - rather than waiting for the whole page to render then hiding the modal div 
 
    inspectCookie(); 
</script>