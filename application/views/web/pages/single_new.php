<!--===MODULE MAIN==-->

<link href="<?=base_url() ?>assets/web/css/news.css" rel="stylesheet" type="text/css" />

<link href="<?=base_url() ?>assets/web/css/news_detail.css" rel="stylesheet" type="text/css" />

<!--===MODULE MAIN==-->  

<!--=== BEGIN: CONTENT ===-->

<div id="vnt-content">

  <div class="mod-content">

    <div id="vnt-main" class="desc-onenews">

      <!--===BEGIN: BREADCRUMB===-->

      <div id="vnt-navation" class="breadcrumb breadcrumb_left ">

        <div class="container">

          <div class="result"><?=$this->session->flashdata('message'); ?></div>

          <div class="navation">

            <ul>

              <li><a href="/">Trang chủ</a></li>

              <?php

              $query2=$this->db->query("select * from tbl_category_dmtin where id='".$get_single_dmtin->parent_id."' ");

              $rs_catelogy_c2 = $query2->row(); ?>

              <?php if(empty($rs_catelogy_c2->category_name)){echo "";?>

            <?php } else {?>       

              <li>

                <a href="<?=base_url($rs_catelogy_c2->slug).'.html'?>">

                  <?=$rs_catelogy_c2->category_name ?></a>

                </li>

              <?php }?>

              <li>

                <a href="<?=base_url($get_single_dmtin->slug).'.html';?>"><?=$get_single_dmtin->category_name?></a>

              </li>

            </ul>

          </div>

        </div>

      </div>

      <!--===END: BREADCRUMB===-->

      <?php if (empty($get_single_dmtin->dmtin_banner)) {?>

       <?php  echo '';} else {?>

         <div class="i-slder"><img src="<?=base_url('uploads/news/'.$get_single_dmtin->dmtin_banner)?>" /></div>         

       <?php }?> 

       <!--===BEGIN: BOX MAIN===-->

       <div class="box_mid container">

        <div class="mid-content row">

          <div class="the-content desc">

       

            <div class="post_news_detail col-md-12 col-xs-12 col-sm-12">



              <div class="wap_in">

              <h1><?=$get_single_dmtin->dmtin_title;?></h1>

              <ul class="article-info-more">

                <li> Người viết: <?php if(!isset($get_user_name->user_id)){echo 'Admin';}else{ echo $get_user_name->user_name;}?>

                </li>

                <li>

                  <!--<i class="fa fa-file-alt"></i>-->

                  <a href="<?=base_url($get_single_dmtin->slug).'.html';?>">| <?=$get_single_dmtin->category_name?></a>

                </li>

              </ul>

           

          </div><!--End wap_in-->
          <div class="clear"></div>



              <p><?=$get_single_dmtin->dmtin_long_description?></p>

              <div class="clear"></div>



    <div class="vnt-news bg-news wrap" >
      <div class="container">
    <div class="gird-new_home noborder row">
      <div class="wrap-grid">
        <div class="col-md-12 col-xs-12 col-sm-12">
          <div class="row">
          <div class="wrap-header">
          <h2 class="wrap-title">TIN TỨC LIÊN QUAN</h2>
         </div>
            
            <div class="clear"></div>
            <?php

    $query =$this->db->query("select * from tbl_dmtin LEFT JOIN tbl_category_dmtin on tbl_dmtin.dmtin_category = tbl_category_dmtin.id    where
    dmtin_id NOT IN ('".$get_single_dmtin->dmtin_id."') and dmtin_category='".$get_single_dmtin->id."' order by rand()  limit 0,6");
    $rs_other=$query->result();
    foreach ($rs_other as $value) {?>
          <div class="item col-md-4 col-xs-12">
             <div class="wap-item">
            <div class="i-image img-news">
              <a href="<?php echo base_url($value->dmtin_slug).'.html';?>">
                <img src="<?php echo base_url('uploads/news/'.$value->dmtin_image)?>" alt="<?php echo $value->dmtin_title ?>" title="<?php echo $value->dmtin_title ?>">
              </a>
            </div><!--End img-->
             <div class="i-desc">
              <div class="i-title">
                <h3>
                  <a href="<?php echo base_url($value->dmtin_slug).'.html';?>">
                    <?php echo word_limiter($value->dmtin_title,28)?></a>
                  </h3>
                </div>
                <div class="clear"></div>
                <div class="mota"> <i class="fa fa-map-marker"></i> <?php echo word_limiter($value->dmtin_short_description,14)?></div>
              </div><!--end i-desc-->
             </div><!--End wap -item-->
            </div>

          <?php   }?>

          <div class="clear"></div>
         
          </div><!--End row-->
        </div><!--End md-12-->
        </div><!--End wrap-grid-->
      </div><!--end gird-news-->
    </div>
    </div><!--end vnt-news-->

    <div class="clear"></div>

            <div class="vnt-viewed">

                <div class="div_title"><span>Có thể bạn quan tâm</span></div>

                <div class="gird-viewed">

                  <?php $quantam = $this->web_model->get_baiviet_quantam();

                  foreach ($quantam as $qtm) { ?>

                    <div class="item">

                      <div class="i-title">

                        <h4>

                          <a href="<?=base_url($qtm->dmtin_slug).'.html';?>">

                            <i class="fa fa-angle-double-right"></i> <?=$qtm->dmtin_title ?>

                          </a>

                        </h4>

                      </div>

                    </div>

                  <?php }?>

                </div>

              </div>

              <div class="clear"></div>

            </div><!--end md-9-->

            

       

            </div>

            <div class="clear"></div>

           

            </div>

          </div>

          <!--===END: BOX MAIN===-->

        </div>

        <div class="clear"></div>

      </div>

    </div>

  </div>

  <!--=== END: CONTENT ===-->

<script>
  $(document).ready(function() {
  $('#media').carousel({
    pause: true,
    interval: false,
  });
});
</script>