
<div class="main">
    <div class="content">
        <div class="error"> 
             
            <h2>Không tìm thấy nội dung bạn yêu cầu</h2>
            <div class="subtext">
             <span>Không tìm thấy  <strong>"<?php echo $search; ?>"</strong>. </span>
             <span>Vui lòng kiểm tra chính tả, sử dụng các từ tổng quát hơn và thử lại!</span>
            </div>
        </div>      
          <div class="col-sm-12 col-md-12 col-xs-12">
                <div class="block-404-navigation">
                    <a href="/">
                        <div class="btn__round__icon"><i class="adr-icon icon-red-home"></i></div><span>Trang chủ</span></a>
                    <a href="lien-he.html">
                        <div class="btn__round__icon"><i class="adr-icon icon-phone-2"></i></div><span>Liên hệ</span></a>
                </div>
            </div>
        <div class="clear"></div>
       

    </div>
</div>
<style>

.error {
    text-align: center;
    padding: 20px;
}

.error h2 {
    /* color: #252a2b; */
    /* text-align: center; */
    /* font-weight: 600; */
    /* font-size: 22px; */
    /* background-color: white; */
    line-height: 35px;
}
 .error p {
    color: #000;
    text-align: center;
    font-size: 14px;
    line-height: 24px;
    margin-bottom: 0px;
}


.error img {
    -webkit-transition: all 0.05s ease-in;
    transition: all 0.05s ease-in;
    max-height: 400px;
    max-width: 600px;
    margin: 0 auto;
}
#vnt-header
{
    position: inherit;
}
 .block-404-navigation {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-pack: center;
    justify-content: center;
    margin-top: 10px;
}
 .block-404-navigation>a {
    margin: 16px;
    text-align: center;
}
 .block-404-navigation .btn__round__icon {
    width: 64px;
    height: 64px;
    border-radius: 50%;
    background: #e8eaee;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-pack: center;
    justify-content: center;
    -ms-flex-align: center;
    align-items: center;
    margin: 0 auto;
    margin-bottom: 12px;
}
 .block-404-navigation .btn__round__icon .adr-icon {
    width: 20px;
    height: 20px;
    font-size: 20px;
    margin-bottom: 0;
    color: #333;
    font-style: initial;
    font-weight: 600;
}
.adr-icon.icon-red-home:before {
    content: "\f015";
    font-family: "FontAwesome";
    color: #000;
}

.adr-icon.icon-phone-2:before {
    content: "\f095";
    font-family: "FontAwesome";
    color: #000;
}
.block-404-navigation>a span
{
    color: #000;
}
</style>






