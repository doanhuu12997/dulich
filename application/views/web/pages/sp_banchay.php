


<link href="<?php echo base_url() ?>assets/web/css/product.css" rel="stylesheet" type="text/css" />


 <!--=== BEGIN: CONTENT ===-->
        <div id="vnt-content">
           <!--===BEGIN: BREADCRUMB===-->
            <div id="vnt-navation" class="breadcrumb">
              <div class="container">
                    <div class="navation">
                        <ul class="i_breadcrumb">
                            <li><a href="/">Trang chủ</a></li>
                         <li><a href="<?php echo base_url('ban-chay.html') ?>">Bán chạy</a></li>
                        </ul>
                    </div>
            </div>
            <!--===END: BREADCRUMB===-->

       <div class="container">    
            <div class="clear"></div>
                <div class="mod-content">
                
                        <!--===BEGIN: BOX MAIN===-->
                        <div class="box_mid row">
                            <div class="mid-content">
                                 <div class="gird_tour">
          
  <!--=== BEGIN: PRODUCT ===-->
            <div class="vnt-product">
                    <div class="prod-content">
                        <div class="grid-prod">
                      <div class="w_product w_thuonghieu row">
                          <?php 
                             foreach ($product_sp_banchay as $product) {?>
                            <div class="item col-md-2 col-xs-6 col-sm-4">
                <div class="wrap-item">
                  <div class="i-top">
                    <div class="i-image">
                       <?php if(empty($product->product_donggoi)){ echo '';} else{?>            
                      <span class="product-promo"><?php echo $product->product_donggoi; ?></span>
                      <?php }?>
                      <?php 
                        if(floatValue($product->product_price_sale)==0) {echo "";} 
                        else {?>
                          <div class="i-saleoff">
                            <?php if(floatValue($product->product_price)==0 || floatValue($product->product_price_sale)==0){ ?>
                              <?php echo  ""; ?>
                            <?php } else { ?>
                              <?php $percent = (floatValue($product->product_price) - floatValue($product->product_price_sale)) /floatValue($product->product_price) *100; ?>
                              <?php $rs_percent=number_format($percent);
                              echo - round($rs_percent); ?>% 
                            <?php  }?>
                          </div>
                        <?php }?>
                     <?php if($product->product_hot==0){echo "";} else{?>
                        <div class="hot"><img src="<?php echo base_url() ?>assets/web/images/hot-icon.gif" /> </div>
                      <?php }?>
                        <?php if(floatValue($product->product_price_sale)==0) {echo "";} else {?>
                      <label class="discount">GIẢM  <?php $price_sale = (floatValue($product->product_price) - floatValue($product->product_price_sale)); echo number_format($price_sale) .'₫';  ?></label> <?php }?>
                      
                      <a href="<?php echo base_url($product->product_slug).'.html';?>">
                        <img src="<?php echo base_url('uploads/product/thumb/'.$product->product_image)?>" alt="<?php echo $product->product_title ?>" title="<?php echo $product->product_title ?>"/>
                      </a>
                    </div>
                    <div class="clear"></div>
                  </div>
                  <div class="i-desc">
                    <div class="desc-left">
                      <div class="i-title">
                        <a href="<?php echo base_url($product->product_slug).'.html';?>" title="<?php echo $product->product_title ?>">
                          <h4 title="<?php echo $product->product_title ?>"><?php echo word_limiter($product->product_title,4) ?></h4>
                        </a>
                      </div> 
                      <div class="clear"></div>
                       <?php 
                                    $rs_count =$this->web_model->rs_count_total_star($product->product_id);
                                    $rs_comment_star =$this->web_model->rs_comment_star($product->product_id);
                                if($rs_comment_star->sum=="" || $rs_count->count==0) {
                                 echo " <input class='rating rating-loading' data-min='0' data-max='5'  value='0'/>";}
                               else {
                                 $tatol_star =$rs_comment_star->sum;
                                 $tatol_people =$rs_count->count;
                                  $tatol=$tatol_star/$tatol_people;
                                  ?>
                             <input class="rating rating-loading" data-min="0" data-max="5"  value="<?php  echo round($tatol, 0, PHP_ROUND_HALF_UP);  ?>"/>

                                  <span class="i-rating">  <?php echo $rs_count->count ;?> Đánh giá  </span>
                                <?php }?>
                         <div class="clear"></div>
                      <div class="i-promotion">
                        <?php if(floatValue($product->product_price_sale)==0) { echo "";} else {?>
                          <span content="<?php echo number_format(floatValue($product->product_price_sale),0, ',','.');?>"><?php echo $product->product_price_sale; ?></span>
                          <span content="vnđ">₫</span> 
                        <?php }?>
                        <?php if(floatValue($product->product_price)==0){echo "<span class='call_me' itemprop='price' content=".number_format(floatValue($product->product_price),0, ',','.').">Liên hệ </span> <span itemprop='priceCurrency' content='vnđ'></span>";} else if(floatValue($product->product_price_sale) ==0){echo '<span content="vnđ"></span> <span content='.number_format(floatValue($product->product_price),0, ',','.').'>'. $product->product_price .' ₫' .'</span>';}else{ echo '<div class=i-price>' .  $product->product_price .'₫' ; ?></div>
                      <?php } ?>
                    </div><!--end i-promtion-->
                    <div class="clear"></div>
                  </div>
                </div>
              </div>
            </div><!--end item-->
                    <?php }?>
                         <div class="clear"></div>

                      </div>

                           
                        </div>
                </div>
            </div>
            <!--=== END: PRODUCT ===-->

</div></div>
                               
                        </div>
                        <!--===END: BOX MAIN===-->
                    </div>
                    <div class="clear"></div>
                </div>
        </div>
        <!--=== END: CONTENT ===-->



