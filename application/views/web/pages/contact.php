<link href="<?php echo base_url(); ?>assets/web/css/contact.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url(); ?>assets/web/js/contact/contact.js"></script>
<!--=== BEGIN: CONTENT ===-->
<div id="vnt-content">
  <div id="result">
    <?php echo $this->session->flashdata('message'); ?>
  </div>
  <!--===BEGIN: BREADCRUMB===-->
  <div id="vnt-navation" class="breadcrumb">
    <div class="container">
      <div class="navation">
        <ul>
          <li>
            <a href="/">Trang chủ
            </a>
          </li>
          <li>Liên hệ
          </li>
        </ul>
      </div>
    </div>
  </div>
  <!--===END: BREADCRUMB===-->
  <div class="container-fluid">
    <!--===BEGIN: BOX MAIN===-->
    <div class="box_mid">
      <div class="mid-title">
        <div class="titleR">
        </div>
        <div class="clear">
        </div>
      </div>
      <div class="mid-content">
        <div class="map">
          <div class="contant_main container">
            <div class="row">
              <!--===BEGIN: FORM LIÊN HỆ==-->
              <div class="formContact col-md-12 col-xs-12 col-sm-12">
                <div class="col-md-5 col-xs-12 col-sm-5">
                <h3>Để lại lời nhắn cho chúng tôi
                </h3>
                  <?php echo get_option('contact_description'); ?>
                </div>

                 <!--===BEGIN: THÔNG TIN LIÊN HỆ==-->
                 <div class="col-md-7 col-xs-12 col-sm-7">
                 <?php echo get_option('company_location'); ?>
                </div>
                 <!--===END: THÔNG TIN LIÊN HỆ==-->
                <div class="clear"></div>
                <form id="formContact" name="formContact" method="POST" action="
                                                                                <?php echo base_url('save_contact.html'); ?>" class="form validate">
                  <div class="row-form col-md-4 col-xs-12 col-sm-4 ">
                    <div class="rf_left">
                      <label for="name">Họ và tên 
                        <span>(*)
                        </span>
                      </label>
                    </div>
                    <div class="rf_right">
                      <input type="text" name="contact_name" required  id="contact_name" class="form-control" />
                    </div>
                    <div class="clear">
                    </div>
                  </div>
                  <div class="row-form col-md-4 col-xs-12 col-sm-4 ">
                    <div class="rf_left">
                      <label for="email">Email 
                        <span>(*)
                        </span>
                      </label>
                    </div>
                    <div class="rf_right">
                      <input type="email" name="contact_email" required   class="form-control" />
                    </div>
                    <div class="clear">
                    </div>
                  </div>
                  <div class="row-form col-md-4 col-xs-12 col-sm-4 ">
                    <div class="rf_left">
                      <label for="phone">Số điện thoại 
                        <span>(*)
                        </span>
                      </label>
                    </div>
                    <div class="rf_right">
                      <input type="text" name="contact_phone" required   class="form-control" />
                    </div>
                    <div class="clear">
                    </div>
                  </div>
                  <div class="row-form col-md-12 col-xs-12 col-sm-12 ">
                    <div class="rf_left">
                      <label for="f-content">Nội dung 
                        <span>(*)
                        </span>
                      </label>
                    </div>
                    <div class="rf_right">
                      <textarea class="form-control" id="contact_content" required  name="contact_content" rows="4">
                      </textarea>
                    </div>
                    <div class="clear">
                    </div>
                  </div>
                  <div class="row-form c-button col-md-12 col-xs-12 col-sm-12">
                    <div class="rf_right">
                      <div class="fl">
                        <button id="b-submit" name="submit" type="submit" class="btn btnsubmit" value="Gửi" >
                          <span>Gửi nội dung
                          </span>
                        </button>
                      </div>
                      <div class="clear">
                      </div>
                    </div>
                    <div class="clear">
                    </div>
                  </div>
                </form>
              </div>
              <!--===END: FORM LIÊN HỆ==-->
             
            </div>
          </div>
        </div>
        <!--end map-->
        <div class="clear">
        </div>
      </div>
    </div>
    <!--===END: BOX MAIN===-->
  </div>
</div>
<style type="text/css">
  .breadcrumb ul li:first-child{
  }
  .breadcrumb ul li:last-child{
    position: relative;
    font-size: 14px;
  }
  .breadcrumb ul li ~ li:before {
    left: -12px;
    position: relative;
    top: 0px;
  }
</style>
