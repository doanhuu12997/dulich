<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard')?>">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="<?php echo base_url('edit/code/'.$code_info_by_id->id)?>">Sửa Mã giảm giá</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Sửa Mã giảm giá</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message');?></p>
            </div>
            <div class="box-content">
                <form class="form-horizontal" action="<?php echo base_url('update/code/'.$code_info_by_id->id);?>" method="post"  enctype="multipart/form-data">
                    <fieldset>

                       
                           <div class="control-group col-md-4 col-xs-12">
                            <label class="control-label" for="fileInput">Mã giảm giá tự ghi</label>
                            <div class="controls">
                                <input class="form-control" value="<?php echo $code_info_by_id->code;?>" name="code" type="text"/>
                            </div>
                        </div>   
                      <div class="control-group col-md-4 col-xs-12">
                            <label class="control-label" for="fileInput">Giảm giá</label>
                            <div class="controls">
                                <input class="form-control outputprice" value="<?php echo $code_info_by_id->amount;?>" name="amount" type="text"/>
                            </div>
                        </div>   

                        <div class="control-group col-md-4 col-xs-12">
                   <label class="control-label" for="fileInput">Đơn vị</label>
                                   <select class="form-control" id="product_category" name="type">
                                    <option value="<?php echo $code_info_by_id->type;?>" ><?php echo $code_info_by_id->type;?></option>
                                      <option value="percent" >%</option>
                                      <option value="vnd" >VNĐ</option>
                                </select>
                        </div>   

                    <div class="clear"></div>
                       
                          <div class="control-group col-md-3 col-xs-12">
                            <label class="control-label" for="textarea2">Trạng thái</label>
                            <div class="controls">
                                <select class="form-control" name="publication_status">
                                    <option value="1">Hiện</option>
                                    <option value="0">Ẩn</option>
                                </select>
                            </div>
                        </div>
                        <div class="control-group col-md-4 col-xs-12">
                            <label class="control-label" for="fileInput"><i class="fa fa-cube"></i> Ngày kết thúc</label>
                          
                              <div class="input-group date" id="dealtime_end">
                                  <input type="text" class="form-control" value="<?php echo $code_info_by_id->valid_to_date;?>" name="valid_to_date"   data-date-format="YYYY-MM-DD HH:mm:ss"/>  <span class="input-group-addon"><span class="glyphicon-calendar glyphicon"></span></span>
                              </div>

                        </div>
                       
                                
                      
                        
                        <div class="control-group col-md-12 col-xs-12">
                            <button type="submit"  class="btn btn-primary">Lưu thay đổi</button>
                            <button  class="btn"><a href="<?php echo base_url('manage/code')?>">Hủy</a></button>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

   
    
</div><!--/.fluid-container-->

<!-- end: Content -->

