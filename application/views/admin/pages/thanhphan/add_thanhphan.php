<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard')?>">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="<?php echo base_url('add/thanhphan')?>">Thêm Từ khóa</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-thanhphanal-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Thêm Từ khóa</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message');?></p>
            </div>
            <div class="box-content">
                <form class="form-horizontal" action="<?php echo base_url('save/thanhphan')?>" method="post">
                    <fieldset>

                  
<?php        $getcategory=$this->product_model->getcategory();?>
             <div class="control-group col-md-4 col-xs-12">
            <label class="control-label" for="textarea2">Từ khóa</label>
              <div class="controls">
                <select class="form-control" name="thanhphan_name">
                  <option value="">Chọn Từ khóa</option>
                  <?php  foreach($getcategory as $row){
                        $sub_cate= $this->product_model->sub_category($row->id); ?>
                            <option value="<?php echo $row->category_name; ?>"><?php echo $row->category_name; ?></option>

                     <?php foreach ($sub_cate as $sub) {
                 $sub_cate_child= $this->product_model->sub_category($sub->id); ?>
    
                   <option value="<?php echo $sub->category_name; ?>">---<?php echo $sub->category_name; ?>---</option>

                 <?php foreach ($sub_cate_child as $sub_child) {?>
                    <option value="<?php echo $sub_child->category_name; ?>">------------<?php echo $sub_child->category_name; ?>----------</option>
                  <?php }?>

                     <?php }?>

                        <?php } ?>
                </select>
              </div>
            </div>
                         

                      <!--  <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput">Hình ảnh</label>
                            <div class="controls">
                                <input class="form-control" name="thanhphan_image" id="fileInput" type="file"/>
                            </div>
                        </div>  -->                 
                      <!--  <div class="control-group col-md-12 col-xs-12">
                            <label class="control-label" for="textarea2">Mô tả</label>
                            <div class="controls">
                                <textarea class="cleditor" id="textarea2" name="thanhphan_description" rows="3"></textarea>
                            </div>
                        </div>-->
                        
                        <div class="control-group col-md-3 col-xs-12">
                            <label class="control-label" for="textarea2">Trạng thái</label>
                            <div class="controls">
                                <select class="form-control" name="publication_status">
                                    <option value="1">Hiện</option>
                                    <option value="0">Ẩn</option>
                                </select>
                            </div>
                        </div>
                      <div class="control-group col-md-9 col-xs-12"></div>

                        
                        <div class="control-group col-md-12 col-xs-12">
                            <button type="submit" class="btn btn-primary">Lưu thay đổi</button>
                            <button type="reset" class="btn">Hủy</button>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->


</div><!--/.fluid-container-->

<!-- end: Content -->