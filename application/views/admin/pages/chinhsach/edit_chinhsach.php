<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard')?>">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="<?php echo base_url('edit/chinhsach/'.$chinhsach_info_by_id->chinhsach_id)?>">Sửa Chính sách</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Sửa Chính sách</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message');?></p>
            </div>
           
            <div class="box-content">
                <form class="form-horizontal" action="<?php echo base_url('update/chinhsach/'.$chinhsach_info_by_id->chinhsach_id);?>" method="post" enctype="multipart/form-data">
                    <fieldset>

                        <div class="control-group col-md-12 col-xs-12">
                            <label class="control-label" for="fileInput"> Tên</label>
                            <div class="controls">
                                <input class="form-control" value="<?php echo $chinhsach_info_by_id->chinhsach_title;?>" name="chinhsach_title" type="text"/>
                            </div>
                        </div> 


                        <div class="control-group col-md-12 col-xs-12">
                            <label class="control-label" for="fileInput"> Mô tả</label>
                            <div class="controls">
                                <textarea name="chinhsach_des" rows="6" class="form-control i-des-sort"> <?php echo $chinhsach_info_by_id->chinhsach_des;?></textarea>
                            </div>
                        </div> 
                        
                        <div class="control-group col-md-3 col-xs-12">
                            <label class="control-label" for="fileInput"> Image</label>
                            <div class="controls">
                                <input class="form-control" name="chinhsach_image" type="file"/>
                            </div>
                        </div>
                        
                        
                        <div class="control-group col-md-9 col-xs-12">
                            <label class="control-label" for="fileInput"> Image</label>
                            <div class="controls">
                                <img  src="<?php echo base_url('uploads/'.$chinhsach_info_by_id->chinhsach_image);?>"/>
                            </div>
                        </div>
               
                        
                                
                        <div class="control-group col-md-3 col-xs-12">
                            <label class="control-label" for="textarea2">Trạng thái</label>
                            <div class="controls">
                                <select class="form-control" name="publication_status">
                                    <option value="1">Hiện</option>
                                    <option value="0">Ẩn</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="control-group col-md-12 col-xs-12">
                            <button type="submit"  class="btn btn-primary">Lưu thay đổi</button>
                            <button  class="btn"><a href="<?php echo base_url('manage/chinhsach')?>">Hủy</a></button>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

    
    
</div><!--/.fluid-container-->

<!-- end: Content -->

