start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard') ?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/brand') ?>">Quản lý đăng ký nhận thông tin</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="fa fa-book"></i><span class="break"></span>Đăng ký nhận thông tin</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="fa fa-cog fa-spin fa-1x fa-fw"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>

            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>

            <div class="box-content" style="height: 400px;">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>STT.</th>
                            <!-- th>Tên</th>
                            <th>SDT</th>
                            <th>Dịch vụ</th> -->
                            <th>Email</th>
                            <th>Giới tính</th>
                           <!--  <th>Nội dung</th> -->
                            <th>Thời gian</th>
                            <th>action</th>

                        </tr>
                    </thead>   
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($all_regiter_mail as $single_regiter_mail) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
<!--                                 <td class="center"><?php echo $single_regiter_mail->regiter_mail_name; ?></td>
 -->                             <!--   <td class="center"><?php echo $single_regiter_mail->regiter_mail_phone; ?></td>
 
                                <td class="center"> <?php echo $single_regiter_mail->regiter_service;?>
                                </td> -->
                                <td class="center"> <?php echo $single_regiter_mail->regiter_mail_email;?>
                                </td>
                                <td class="center"> <?php echo $single_regiter_mail->regiter_gioitinh;?>
                                </td>
                              <!--   <td class="center"> <?php echo $single_regiter_mail->regiter_mail_content;?>
                                </td>
 -->
                                 <td class="center"> <?php echo $single_regiter_mail->regiter_mail_time;?>
                                </td>


                                <td class="center">
                           
                                    <a class="btn btn-danger" href="<?php echo base_url('delete/regiter_mail/' . $single_regiter_mail->regiter_mail_id); ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->



    </div><!--/row-->



</div><!--/.fluid-container-->

<!-- end: Content