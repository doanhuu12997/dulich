<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard') ?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/request') ?>">Manage Đặt kính</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="fa fa-book"></i><span class="break"></span>Manage Đặt kính</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="fa fa-cog fa-spin fa-1x fa-fw"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>

            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>

            <div class="box-content" style="height: 400px;">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Sr.</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Địa chỉ</th>
                            <th>Nội dung</th>
                            <th>Name product</th>
                            <th>Images</th>
                            <th>Xem</th>
                            <th>action</th>

                        </tr>
                    </thead>   
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($all_request as $single_request) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center">
                                    <div class="media-left align-self-center">
                                    <img class="rounded-circle" src="https://gravatar.com/avatar/25b1fc64ba12614875c1e467d7e4c86e?s=512" alt="">
                                </div>
                                    <?php echo $single_request->request_name; ?>

                                </td>
                                <td class="center"><i class="fa fa-envelope-o"></i> <?php echo $single_request->request_email; ?></td>
                                <td class="center"><i class="fa fa-phone"></i> <?php echo $single_request->request_phone; ?></td>
                                 <td class="center"><i class="fa fa-map-marker"></i> <?php echo $single_request->request_address; ?></td>
                                <td class="center"><i class="fa fa-comments"></i> <?php echo $single_request->request_content; ?></td>
                                <td class="center"><i class="fa fa-cube"></i> <?php echo $single_request->product_title; ?></td>
                                <td class="center"><img  style="max-height: 50px;" src="<?php echo base_url('uploads/'.$single_request->product_image);?>" /></td>
                                <td class="center"> <a class="i-target" target="target" 
                            href="<?php echo base_url($single_request->product_slug).'.html'?>"><i class="fa fa-eye"></i></a> </td>

                                <td class="center">
                                   <?php if ($single_request->publication_status == 1) { ?>
                                        <a class="btn btn-success" href="<?php echo base_url('unpublished/request/' . $single_request->request_id); ?>">
                                            <i class="halflings-icon white thumbs-up"></i>  
                                    </a>
                                    
                                    <?php } else {
                                        ?>
                                        <a class="btn btn-danger" href="<?php echo base_url('published/request/' . $single_request->request_id); ?>">
                                            <i class="halflings-icon white thumbs-down"></i>  
                                        </a>
                                        <?php }
                                    ?>
                                </td>
                                <td class="center">
                           
                                    <a class="btn btn-danger" href="<?php echo base_url('delete/request/' . $single_request->request_id); ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->



    </div><!--/row-->



</div><!--/.fluid-container-->

<!-- end: Content -->