<!DOCTYPE html>
<html lang="en">
<head>
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title><?php echo get_option('contact_title') ?></title>
	<meta name="description" content="<?php echo get_option('contact_subtitle') ?>">
    <meta name="keywords" content="<?php echo get_option('site_keywords') ?>">
	<!-- end: Meta -->

	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->

	<!-- start: CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link id="base-style" href="<?php echo base_url()?>assets/admin/css/style.css" rel="stylesheet">
	<!--<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	-->
	<!-- end: CSS -->

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <link id="ie-style" href="<?php echo base_url()?>assets/admin/css/ie.css" rel="stylesheet">
    <![endif]-->

	<!--[if IE 9]>
    <link id="ie9style" href="<?php echo base_url()?>assets/admin/css/ie9.css" rel="stylesheet">
    <![endif]-->

    <!-- start: Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('/'); ?><?php echo get_option('site_favicon'); ?>" />
    <!-- end: Favicon -->

    <style type="text/css">
    	body { background: url(<?php echo base_url()?>assets/admin/img/bg-login.jpg) !important; }
    </style>
</head>
<body>
	<div class="container-fluid-full" id="banner">
		<div class="row-fluid" id="cloud-scroll">
			<div class="row-fluid" id=login>
				<style type="text/css">
					#result{color:red}
					#result p{color:red}
				</style>
				<div id="result">
					<p><?php echo $this->session->flashdata('message');?></p>
				</div>
				<div style="text-align:center;margin-bottom:15px !important" class="p-mb-10">
					<img src="<?php echo base_url('uploads/'); ?><?php echo get_option('site_logo'); ?>">
				</div>
				<form name="loginform" id="loginform" action="<?php echo base_url()?>admin_login_check" method="post">
					<p>
						<label for="user_login"> Địa chỉ Email
							<br />
							<input type="text" name="user_email" id="user_login" class="input" value="<?php set_value('user_name');?>" size="20" />
						</label>
					</p>
					<p>
						<label for="user_pass">Mật khẩu
							<br />
							<input type="password" name="user_password" id="user_pass" class="input" value="" size="20" />
						</label>
					</p>
					<p class="submit">
						<input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="Đăng nhập" />
					</p>
				</form>
                <!--
                <h3>Forgot Password?</h3>
                <p>
                    No problem, <a href="#">click here</a> to get a new password.
                </p>    
            	-->
        	</div><!--/row-->
    	</div><!--/.fluid-container-->
	</div><!--/fluid-row-->
	<div class="clear"></div>
	<div style="text-align:center;padding:20px 0px"><?php echo get_option('site_copyright');?></div>
	<script src="<?php echo base_url()?>assets/admin/js/jquery-1.9.1.min.js"></script>
	<script src="<?php echo base_url()?>assets/admin/js/jquery-migrate-1.0.0.min.js"></script>
</body>
</html>
