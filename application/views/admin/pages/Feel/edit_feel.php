<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard')?>">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="<?php echo base_url('edit/feel/'.$feel_info_by_id->feel_id)?>">Sửa Cảm nhận</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Sửa Cảm nhận</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message');?></p>
            </div>
             <?php 
                 $get_id=$this->uri->segment(3);
             ?>
             <input id="get_id_cate" type="hidden" name="cate_id" value="<?php echo $get_id ?>" />
            <div class="box-content">
                <form class="form-horizontal" action="<?php echo base_url('update/feel/'.$feel_info_by_id->feel_id);?>" method="post" enctype="multipart/form-data">
                    <fieldset>

                        <div class="control-group col-md-12 col-xs-12">
                            <label class="control-label" for="fileInput"> Tên</label>
                            <div class="controls">
                                <input class="form-control" value="<?php echo $feel_info_by_id->feel_title;?>" name="feel_title" type="text"/>
                            </div>
                        </div> 


                        <div class="control-group col-md-12 col-xs-12">
                            <label class="control-label" for="fileInput"> Mô tả</label>
                            <div class="controls">
                                <textarea name="feel_des" rows="6" class="form-control i-des-sort"> <?php echo $feel_info_by_id->feel_des;?></textarea>
                            </div>
                        </div> 
                        
                        <div class="control-group col-md-3 col-xs-12">
                            <label class="control-label" for="fileInput"> Image</label>
                            <div class="controls">
                                <input class="form-control" name="feel_image" type="file"/>
                                <input class="form-control" value="<?php echo $feel_info_by_id->feel_image;?>" name="feel_delete_image" type="hidden"/>
                            </div>
                        </div>
                        
                        
                        <div class="control-group col-md-9 col-xs-12">
                            <label class="control-label" for="fileInput"> Image</label>
                            <div class="controls">
                                <img  src="<?php echo base_url('uploads/'.$feel_info_by_id->feel_image);?>"/>
                                 <a class="btn btn-danger" id="delete_imgfeel" href="<?php echo $feel_info_by_id->feel_image;?>"> <i class="halflings-icon white trash"></i> </a>
                               <div class="delete_imgfeel"></div>
                            </div>
                        </div>
                        <!--
                         <div class="control-group">
                            <label class="control-label" for="fileInput">Feel Link</label>
                            <div class="controls">
                                <input class="form-control" value="<?php echo $feel_info_by_id->feel_link;?>" name="feel_link" type="url"/>
                            </div>
                        </div>-->
                        
                                
                        <div class="control-group col-md-3 col-xs-12">
                            <label class="control-label" for="textarea2">Trạng thái</label>
                            <div class="controls">
                                <select class="form-control" name="publication_status">
                                    <option value="1">Hiện</option>
                                    <option value="0">Ẩn</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="control-group col-md-12 col-xs-12">
                            <button type="submit" id="save_category" class="btn btn-primary">Lưu thay đổi</button>
                            <button  class="btn"><a href="<?php echo base_url('manage/feel')?>">Hủy</a></button>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

    
    
</div><!--/.fluid-container-->

<!-- end: Content -->

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"> </script>
                                <!-- end: Content -->
                                <script type="text/javascript">
                                    $(document).ready(function() {

                                         $('#delete_imgfeel').click(function() {
                                            var delete_imgfeel = $("#delete_imgfeel").attr("href");
                                            console.log(delete_imgfeel);
                                            var id =$("#get_id_cate").val();
                                            console.log(id);
                                            $.ajax({
                                                url: '<?=base_url()?>ajax/ajax_delete_imgfeel',
                                                method: "POST",
                                                data: {
                                                    delete_imgfeel: delete_imgfeel,id:id
                                                },
                                                async: true,
                                                dataType: 'html',
                                                success: function(data) {
                                                    console.log(data);
                                                    $('.delete_imgfeel').html("<b>Bạn đã xóa thành công</b>");
                                                }
                                            });
                                            return false;
                                        });


                                    });


                                </script>