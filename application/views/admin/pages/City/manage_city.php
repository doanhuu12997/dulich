<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


   <!-- <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard') ?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/city') ?>">Manage city</a></li>
    </ul>-->

    <div class="row-fluid sortable">        
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="fa fa-book"></i><span class="break"></span>Quản lý Thành phố</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="fa fa-cog fa-spin fa-1x fa-fw"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
             <div class="add_cate"><a href="<?php echo base_url('add/city')?>"><i class="fa fa-plus-circle"></i><span class="hidden-tablet"> Thêm</span></a></div>

            <style type="text/css">
                #result{color:red;padding:5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>

            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Stt.</th>
                            <th>Tên tp</th>
                            <th>Giá ship</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php
                        foreach ($all_city as $key => $single_city){ if($key==0) {echo "";}else {
                        
                            ?>
                            <tr>
                                <td><?php echo $single_city->city_stt ; ?></td>
                                <td><i class="fa fa-folder-open"></i> <?php echo $single_city->city_name ?>   </td>
                                <td> <?php echo $single_city->price_ship ?> vnđ  </td>
                                <td class="center">
                                   <?php if ($single_city->cate_status == 1) { ?>
                                        <a class="" href="<?php echo base_url('unpublished/city/' . $single_city->id); ?>">
                                       <img  class="i-gif" src="<?php echo base_url('assets/admin/img/active_1.png')?>"/>

                                    </a>
                                    
                                    <?php } else {
                                        ?>
                                        <a class="" href="<?php echo base_url('published/city/' . $single_city->id); ?>">
                                <img  class="i-gif" src="<?php echo base_url('assets/admin/img/active_0.png')?>"/>

                                        </a>
                                        <?php }
                                    ?>
                                </td>


                                <td class="center">
                                  
                                    <a class="" href="<?php echo base_url('edit/city/' . $single_city->id); ?>">
                                    <i class="fa fa-pencil"></i>                      
                                    </a>
                                     
                                    <a class="btn btn-danger" href="<?php echo base_url('delete/city/' . $single_city->id); ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                  
                                </td>

                            </tr>
                        <?php } }?>
                    </tbody>
                </table>            
            </div>

        </div><!--/span-->


    </div><!--/row-->



</div><!--/.fluid-container-->

<!-- end: Content -->