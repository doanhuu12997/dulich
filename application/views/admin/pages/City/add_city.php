<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard')?>">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="<?php echo base_url('add/city')?>">Thêm thành phố</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Thêm thành phố</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php  echo $this->session->flashdata('message');?></p>
            </div>
            <div class="box-content">
                <form class="form-horizontal" action="<?php echo base_url('save/city');?>" method="post"  method="post" enctype="multipart/form-data">
                    <fieldset>

                        <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput"><i class="fa fa-book"></i> Tên thành phố</label>
                            <div class="controls">
                                <input class="form-control" id="city_name" name="city_name" type="text"/>
                            </div>
                        </div>   

                        <div class="control-group col-md-4 col-xs-12">
                            <label class="control-label" for="fileInput"><i class="fa fa-book"></i> Giá ship</label>
                            <div class="controls">
                                <input class="form-control outputprice"  name="price_ship" type="text"/>
                            </div>
                        </div> 

                            <div class="control-group col-md-2 col-xs-12">
                            <label class="control-label" for="fileInput"><i class="fa fa-book"></i> Số thứ tự</label>
                            <div class="controls">
                                <input class="form-control "  name="city_stt" type="text"/>
                            </div>
                        </div>     

                  

              
                         <div class="control-group col-md-12 col-xs-12">

                            <label class="col-md-2 col-xs-12" style="width: 10%;">Trạng thái</label>
                            <div class="controls col-md-3 col-xs-12">
                                <select class="form-control" name="cate_status">
                                    <option value="1">Hiện</option>
                                    <option value="0">Ẩn</option>
                                </select>
                            </div>
                        </div>
                        <div class="clear"></div>

                        <div class="control-group col-md-6 col-xs-12">
                            <button type="submit" id="save_city" class="btn btn-primary">Lưu thay đổi</button>
                            <button  class="btn"><a href="<?php echo base_url('manage/city')?>">Hủy</a></button>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

 


</div><!--/.fluid-container-->

<!-- end: Content -->