<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?php echo base_url('dashboard') ?>">Home</a> 
			<i class="icon-angle-right"></i>
		</li>
		<li><a href="<?php echo base_url('manage/brand') ?>">Banner</a></li>
	</ul>

	<div class="row-fluid sortable">		
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon user"></i><span class="break"></span>BANNER ĐẶT TRÊN SITE</h2>
				<div class="box-icon">
					<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
					<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
					<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
				</div>
			</div>
			<div class="add_cate"><a href="<?php echo base_url('add/avd')?>"><i class="fa fa-plus-circle"></i><span class="hidden-tablet"> Thêm</span></a></div>
			<style type="text/css">
				#result{color:red;padding: 5px}
				#result p{color:red}
			</style>
			<div id="result">
				<p><?php echo $this->session->flashdata('message'); ?></p>
			</div>

			<div class="box-content">
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
					<thead>
						<tr>
							<th>STT.</th>
							<th>Tên banner</th>
							<!--  <th>avd Link</th>-->
							<th>Hình ảnh</th>
							<th>Vị trí đặt</th>
							<th>Tình trạng</th>
							<th>Điều chỉnh</th>
						</tr>
					</thead>   
					<tbody>
						<?php
						$i = 0;
						foreach ($all_avd as $single_avd) {
							$i++;
							?>
							<tr>
								<td><?php echo $i; ?></td>
								<td class="center"><?php echo $single_avd->avd_title; ?></td>

								<!--  <td class="center"><a target="_blank" href="<?php echo base_url($single_avd->avd_link);?>">Go To Link</a></td>-->
								<td class="center">
									<img style="max-height: 50px;" src="<?php echo base_url('uploads/'.$single_avd->avd_image);?>"/>
								</td>
								<td class="center">
									<?php 
									if($single_avd->id_cate_page==1) {echo " RIGHT";} 
									else if($single_avd->id_cate_page==2){ echo "LEFT";} 
									else if($single_avd->id_cate_page==3){ echo "Được tin dùng";} 
									else if($single_avd->id_cate_page==4){ echo "Chi tiết sản phẩm";} 
									else if($single_avd->id_cate_page==5){ echo "Chi tiết tin tức"; } 
									else if($single_avd->id_cate_page==6){ echo "Footer"; }
									?>
								</td>
								<td class="center">
									<?php if ($single_avd->publication_status == 1) { ?>
										<a class="btn btn-success" href="<?php echo base_url('unpublished/avd/' . $single_avd->avd_id); ?>">
											Yes
										</a>
									<?php } else {
										?>
										<a class="btn btn-danger" href="<?php echo base_url('published/avd/' . $single_avd->avd_id); ?>">
											No
										</a>
									<?php }
									?>
								</td>
								<td class="center">
									<a class="btn btn-info" href="<?php echo base_url('edit/avd/'.$single_avd->avd_id); ?>">
										<i class="halflings-icon white edit"></i>  
									</a>
									<a class="btn btn-danger" href="<?php echo base_url('delete/avd/'.$single_avd->avd_id);?>">
										<i class="halflings-icon white trash"></i> 
									</a>
								</td>
							</tr>
						<?php } ?>
					</tbody>
				</table>            
			</div>
		</div><!--/span-->
	</div><!--/row-->
</div><!--/.fluid-container-->
<!-- end: Content -->