<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard')?>">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="<?php echo base_url('edit/member/'.$member_info_by_id->customer_id)?>">Điều chỉnh Thành viên</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Điều chỉnh Thành viên</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message');?></p>
            </div>
             <?php 
                 $get_id=$this->uri->segment(3);
             ?>
            <div class="box-content">
                <form class="form-horizontal" action="<?php echo base_url('update/member/'.$member_info_by_id->customer_id);?>" method="post" enctype="multipart/form-data">
                    <fieldset>

                        <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput">Họ và tên</label>
                            <div class="controls">
                                <input class="form-control" value="<?php echo $member_info_by_id->customer_name;?>"  name="customer_name" type="text"/>
                            </div>
                        </div>  

                        <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput">Email</label>
                            <div class="controls">
                                <input class="form-control" value="<?php echo $member_info_by_id->customer_email;?>"  name="customer_email" type="text"/>
                            </div>
                        </div>  

                         <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput">Địa chỉ</label>
                            <div class="controls">
                                <input class="form-control" value="<?php echo $member_info_by_id->customer_address;?>"  name="customer_address" type="text"/>
                            </div>
                        </div>  


                         <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput">Số điện thoại</label>
                            <div class="controls">
                                <input class="form-control" value="<?php echo $member_info_by_id->customer_phone;?>"  name="customer_phone" type="text"/>
                            </div>
                        </div>  
                        <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput">Mật khẩu mới</label>
                            <div class="controls">
                                <input class="form-control" value=""  name="customer_password" type="Password"/>
                            </div>
                        </div>  
                   
                        
                        <div class="control-group col-md-12 col-xs-12">
                            <button type="submit" id="save_category" class="btn btn-primary">Lưu thay đổi</button>
                            <button  class="btn"><a href="<?php echo base_url('manage/member_order')?>">Hủy</a></button>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

    
    
</div><!--/.fluid-container-->

<!-- end: Content -->

