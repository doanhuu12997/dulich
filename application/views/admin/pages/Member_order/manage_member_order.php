<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard')?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/product')?>">Quản lý Thành viên mua hàng</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="fa fa-book"></i><span class="break"></span>Quản lý Thành viên mua hàng</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="fa fa-cog fa-spin fa-1x fa-fw"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>
        
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Họ và tên</th>
                            <th>Email </th>
                            <th>Số điện thoại</th>
                            <th>Tổng danh số</th>
                            <th>Loại thành viên</th>
                            <th>Ngày tham gia</th>
                            <th>Action</th>
                        </tr>
                    </thead>   
                    <tbody>
                           <?php foreach($all_manage_member_info as $v){
                    $jquery= $this->db->query("select * , sum(tbl_order.order_total) as total from tbl_customer left join tbl_order on tbl_order.cus_id=tbl_customer.customer_id where tbl_order.actions NOT IN ('Hủy') and tbl_order.actions NOT IN ('Chưa thanh toán') and tbl_order.actions NOT IN ('Đơn hàng mới') and tbl_customer.customer_id='".$v->customer_id."' order by tbl_customer.customer_id asc  ");
                         $rs_menber = $jquery->result();
                            ?>
                        <?php 
                        foreach($rs_menber as $single_order){
                                     
                            ?>
                        <tr>
                            <td> <?php echo $single_order->customer_name?></td>
                            <td><i class="fa fa-envelope-o"></i> <?php echo $single_order->customer_email?></td>
                            <td><i class="fa fa-mobile"></i> <?php echo $single_order->customer_phone?></td>
                            <td> <?php echo $this->cart->format_number($single_order->total)?> VNĐ</td>
                            <td>
                                    <?php
                                     if($get_ds_info_thuong->origin_id==1){  
                                         $ds_thuong=floatValue($get_ds_info_thuong->origin_sales);
                                    }
                                     if($get_ds_info_bac->origin_id==2){  
                                         $ds_bac=floatValue($get_ds_info_bac->origin_sales);
                                    }   
                                    if($get_ds_info_vang->origin_id==3){  
                                         $ds_vang=floatValue($get_ds_info_vang->origin_sales);
                                    } 

                                    if($get_ds_info_kimcuong->origin_id==4){  
                                         $ds_kimcuong=floatValue($get_ds_info_kimcuong->origin_sales);
                                    }         
                                       $total=$single_order->total;
                                     
                                     if($total<=$ds_thuong && $total<=$ds_bac) { echo "Thường ";
                                     }
                                    else if($ds_bac<=$total && $total<=$ds_vang) { echo "Bạc ";
                                    echo "<img style='max-width:55px;' src='".base_url('assets/admin/img/icon_bac.jpg')."' />";
                                     }
                                     else if($ds_vang<=$total && $total<=$ds_kimcuong) { echo "Vàng " ;
                                    echo "<img style='max-width:55px;' src='".base_url('assets/admin/img/icon_gold.jpg')."' />";
                                     
                                     } else if($total>$ds_kimcuong) { echo "Kim cương " ;
                                     echo "<img style='max-width:55px;' src='".base_url('assets/admin/img/icon_Diamond.jpg')."' />";

                                     }?>

                            </td>

                           
                                <td class="center"><?php echo nice_date($single_order->customer_time,'d-m-Y H:i:s')?></td>
                              <td class="center">
                                    <a class="btn btn-info" href="<?php echo base_url('edit/member/' . $single_order->customer_id); ?>">
                                        <i class="halflings-icon white edit"></i>  

                                    </a>
                                    <a class="confirmClick btn btn-danger" href="<?php echo base_url('delete/member/' . $single_order->customer_id); ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                </td>

                        </tr>

                        <?php }?>

                        <?php }?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->

    </div><!--/row-->



</div><!--/.fluid-container-->

<!-- end: Content -->