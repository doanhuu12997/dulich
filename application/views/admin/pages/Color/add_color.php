<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard')?>">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="<?php echo base_url('add/color')?>">Thêm color</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Thêm color</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php  echo $this->session->flashdata('message');?></p>
            </div>
            <div class="box-content">
                <form class="form-horizontal" action="<?php echo base_url('save/color');?>" method="post"  method="post" enctype="multipart/form-data">
                    <fieldset>

                        <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput">color Name</label>
                            <div class="controls">
                                <input class="form-control" id="color_name" name="color_name" type="text"/>
                            </div>
                        </div>   

                        <div class="control-group col-md-6 col-xs-12">
                            <label class="control-label" for="fileInput">Mã màu</label>
                            <div class="controls">
                          <input style="width: 100px;" class="form-control" type="color"  name="color_code" id="fileInput" type="text"/>  
                            </div>
                        </div>   


                
                        <div class="control-group col-md-3 col-xs-12">
                            <label class="control-label" for="textarea2">Trạng thái</label>
                            <div class="controls">
                                <select class="form-control" name="publication_status">
                                    <option value="1">Hiện</option>
                                    <option value="0">Ẩn</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="control-group col-md-12 col-xs-12">
                            <button type="submit" id="save_color" class="btn btn-primary">Lưu thay đổi</button>
                            <button  class="btn"><a href="<?php echo base_url('manage/color')?>">Hủy</a></button>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

    
  <!--  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"> </script>
<!-- end: Content 
 <script type="text/javascript">
       $(document).ready(function(){
             $('#save_color').click(function(){
             var color = [];
             var getv = $('.getv:checked').val();
             console.log(getv);
             $('.getv').each(function(){
             if($(this).is(":checked"))
             {
             color.push($(this).val());
             }
             });
             color = color.toString();
             $.ajax({
             url: "<?=base_url()?>ajax/check_color",
             method: "POST",
             data:{color:color},
              async : true,
             dataType : 'html',
             success:function(data){
             $('#result').html(data);
             }
             });
             });
});
</script>-->


</div><!--/.fluid-container-->

<!-- end: Content -->