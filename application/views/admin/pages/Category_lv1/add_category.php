<!-- start: Content -->

<div  class="col-md-10 col-xs-12">

    <ul class="breadcrumb">

        <li>

            <i class="icon-home"></i>

            <a href="<?php echo base_url('dashboard')?>">Home</a>

            <i class="icon-angle-right"></i> 

        </li>

        <li>

            <i class="icon-edit"></i>

            <a href="<?php echo base_url('add/category')?>">Thêm</a>

        </li>

    </ul>



    <div class="row-fluid sortable">

        <div class="box">

            <div class="box-header" data-original-title>

                <h2><i class="halflings-icon edit"></i><span class="break"></span>Thêm</h2>

                <div class="box-icon">

                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>

                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>

                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>

                </div>

            </div>

            <style type="text/css">

                #result{color:red;padding: 5px}

                #result p{color:red}

            </style>

            <div id="result">

                <p><?php  echo $this->session->flashdata('message');?></p>

            </div>

            <div class="box-content">

                <form class="form-horizontal" action="<?php echo base_url('save/category');?>" method="post"  method="post" enctype="multipart/form-data">

                    <fieldset>



                        <div class="control-group col-md-8 col-xs-12">

                            <label class="control-label" for="fileInput">Tên Danh mục</label>

                            <div class="controls">

                                <input class="form-control" id="category_name" name="category_name" type="text"/>

                            </div>

                        </div>   



                        <div class="control-group col-md-4 col-xs-12">

                            <label class="control-label" for="parent_id">Chọn danh mục</label>

                            <select class="form-control"  _autocheck="true" name="parent_id" id="parent_id">

                                <option value="0">Danh mục</option>

                                <?php  foreach($all_categroy as $row){

                                $sub_cate= $this->category_model->sub_category($row->id); ?>

                                    <option value="<?php echo $row->id; ?>"><?php echo $row->category_name; ?></option>



                             <?php foreach ($sub_cate as $sub) {?>

                           <option value="<?php echo $sub->id; ?>">------<?php echo $sub->category_name; ?>------</option>



                             <?php }?>



                                <?php } ?>

                            </select>

                            <div class="clear"></div>

                        </div>

                        

                        <div class="control-group col-md-6 col-xs-12">

                            <label class="control-label" for="fileInput"><i class="fa fa-picture-o"></i>Icon catelogy  <i style="color:#f00;font-size: 11px;">(50px x 50px)Rộng x Cao</i></label>

                            <div class="controls">

                                <input class="form-control" name="category_icon" id="fileInput" type="file"/>

                            </div>

                        </div>  

                        

                        <div class="control-group col-md-8 col-xs-12">

                            <label class="control-label" for="fileInput">Banner danh mục <i style="color:#f00;font-size: 11px;">(1024px x 200px)Rộng x Cao</i></label>

                            <div class="controls">

                                <input class="form-control" name="category_img" id="fileInput" type="file"/>

                            </div>

                        </div>

                        <!--<div class="control-group col-md-4 col-xs-12">

                            <label class="control-label">Số thứ tự</label>

                            <div class="controls">

                                <input class="form-control" placeholder="nhập số thứ tự" name="category_stt" type="text"/>

                            </div>

                        </div>-->



                        <div class="control-group col-md-12 col-xs-12">

                            <label class="control-label" for="textarea2"><b>MÔ TẢ DANH MỤC</b></label>

                            <div class="controls">

                                <textarea class="ckeditor" id="content" name="category_description" rows="6"></textarea>

                            </div>

                        </div>



                        <div class="wap-seo">

                           <label class="i_title" for="fileInput"><b>NỘI DUNG SEO</b></label>

                           <div class="clear"></div>



                           <div class="control-group">

                            <label class="control-label" for="fileInput">Tiêu đề</label>

                            <div class="controls">

                                <input class="form-control" id="Title" name="title" type="text"/>

                            </div>

                        </div>

                    

                        <div class="control-group">

                            <label class="control-label" for="fileInput">Mô tả seo</label>

                            <div class="controls">

                                <textarea  class="i-des-sort form-control" id="category_description_seo" name="category_description_seo" rows="3"></textarea>

                            </div>

                        </div>



                        <div class="control-group">

                            <label class="control-label" for="textarea2">Meta Keywords</label>

                            <div class="controls">

                                <textarea  class="i-des-sort form-control" id="keywords" name="keywords" rows="2"></textarea>

                            </div>

                        </div>



                    </div><!--end wapseo-->



                    <div class="control-group col-md-12 col-xs-12">

                           <div class="controls col-md-3 col-xs-12">
                           <label>Trạng thái</label>
                            <select class="form-control" name="cate_status">
                                <option value="1">Hiển thị</option>
                                <option value="0">Ẩn</option>
                            </select>
                         </div>

                         <div class="controls col-md-3 col-xs-12">
                           <label>Nổi bật</label>
                            <select class="form-control" name="noibat">
                                <option value="1">Hiển thị</option>
                                <option value="0">Ẩn</option>
                            </select>
                         </div>

                    </div>

                    <div class="clear"></div>



                    <div class="control-group col-md-6 col-xs-12">

                        <button type="submit" id="save_category" class="btn btn-primary">Lưu thay đổi</button>

                        <button class="btn"><a href="<?php echo base_url('manage/category')?>">Hủy</a></button>

                    </div>

                </fieldset>

            </form>   

        </div>

    </div><!--/span-->

</div><!--/row-->



<script>

    CKEDITOR.replace( 'content', {

        

        height: 400,

        filebrowserUploadUrl: "<?php echo base_url('upload_content.php')?>",



    } );

  </script>

  <!--  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"> </script>

<!-- end: Content 

 <script type="text/javascript">

       $(document).ready(function(){

             $('#save_category').click(function(){

             var color = [];

             var getv = $('.getv:checked').val();

             console.log(getv);

             $('.getv').each(function(){

             if($(this).is(":checked"))

             {

             color.push($(this).val());

             }

             });

             color = color.toString();

             $.ajax({

             url: "<?=base_url()?>ajax/check_color",

             method: "POST",

             data:{color:color},

              async : true,

             dataType : 'html',

             success:function(data){

             $('#result').html(data);

             }

             });

             });

});

</script>-->





</div><!--/.fluid-container-->



<!-- end: Content -->