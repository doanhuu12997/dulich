<!-- start: Content -->

<div  class="col-md-10 col-xs-12">

    <ul class="breadcrumb">

        <li>

            <i class="icon-home"></i>

            <a href="<?php echo base_url('dashboard')?>">Home</a>

            <i class="icon-angle-right"></i> 

        </li>

        <li>

            <i class="icon-edit"></i>

            <a href="<?php echo base_url('edit/category/'.$category_info_by_id->id)?>">Sửa Danh mục</a>

        </li>

    </ul>



    <div class="row-fluid sortable">

        <div class="box">

            <div class="box-header" data-original-title>

                <h2><i class="halflings-icon edit"></i><span class="break"></span>Sửa Danh mục</h2>

                <div class="box-icon">

                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>

                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>

                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>

                </div>

            </div>

            <style type="text/css">

                #result{color:red;padding: 5px}

                #result p{color:red}

            </style>

            <div id="result">

                <p><?php echo $this->session->flashdata('message');?></p>

            </div>



            <?php 

            $get_id=$this->uri->segment(3);

            ?>

            <input id="get_id_cate" type="hidden" name="cate_id" value="<?php echo $get_id ?>" />

            <div class="box-content">

                <form class="form-horizontal" name="formName" action="<?php echo base_url('update/category/'.$category_info_by_id->id);?>" method="post"  enctype="multipart/form-data">

                    <fieldset>

                        <div class="control-group col-md-8 col-xs-12">

                            <label class="control-label" for="fileInput"><i class="fa fa-book"></i> Tên Danh mục</label>

                            <div class="controls">

                                <input class="form-control" value="<?php echo $category_info_by_id->category_name;?>" id="category_name" name="category_name" type="text"/>

                            </div>

                        </div>  

                       <div class="control-group col-md-4 col-xs-12">



                             <label for="parent_id" class="control-label">Danh mục cha</label>

                         

                             <select class="form-control"  autocheck="true"  name="parent_id" >

                              <option value="0">Danh mục cha</option>

                       

                              <?php 



                                foreach($all_categroy as $row){

                                  $sub_cate= $this->category_model->sub_category($row->id); ?>

                                  <option value="<?php echo $row->id; ?>" <?php echo $row->id == $category_info_by_id->parent_id ? 'selected' : '' ?> ><?php echo $row->category_name; ?></option>



                                     <?php foreach ($sub_cate as $sub) {?>

                                      <option value="<?php echo $sub->id; ?>" <?php echo $sub->id == $category_info_by_id->parent_id ? 'selected' : '' ?>>------<?php echo $sub->category_name; ?>------</option>



                                     <?php }?>



                                <?php } ?>



                            </select>



                             <span style="color: red; font-weight: 400; font-size: 14px;"><?php echo form_error("parent_id"); ?></span>



                        </div>

                  



                        <div class="control-group col-md-12 col-xs-12">

                            <label class="control-label" for="fileInput"><i class="fa fa-picture-o"></i> Icon Menu <i style="color:#f00;font-size: 11px;">(50px x 50px)Rộng x Cao</i></label>

                            <div class="controls">

                                <input class="form-control" name="category_icon" id="fileInput" type="file"/>

                                <input class="form-control" name="category_delete_icon" value="<?php echo base_url('uploads/'.$category_info_by_id->category_icon);?>" type="hidden"/>

                            </div>

                        </div>

                        

                        <div class="control-group col-md-6 col-xs-12">

                            <div class="controls">

                                <img style="max-height: 200px;" src="<?php echo base_url('uploads/'.$category_info_by_id->category_icon);?>" />

                                <a class="btn btn-danger" id="delete_catebanner" href="<?php echo $category_info_by_id->category_icon;?>"> <i class="halflings-icon white trash"></i> </a>

                                <div class="delete_catebanner"></div>

                            </div>

                        </div> 



                        <div class="control-group col-md-8 col-xs-12">

                            <label class="control-label" for="fileInput">Banner danh mục <i style="color:#f00;font-size: 11px;">(1024px x 200px)Rộng x Cao</i></label>

                            <div class="controls">

                                <input class="form-control" name="category_img" id="fileInput" type="file"/>

                                <input class="form-control" name="category_delete_img" value="<?php echo base_url('uploads/'.$category_info_by_id->category_img);?>" type="hidden"/>

                            </div>

                        </div>



                       <!-- <div class="control-group col-md-4 col-xs-12">

                           <label class="control-label">Số thứ tự</label>

                           <div class="controls">

                                <input class="form-control" name="category_stt" placeholder="<?php echo $category_info_by_id->category_stt;?>" type="text"/>

                            </div>

                        </div>-->

                        <div class="clear"></div>



                        <div class="control-group col-md-6 col-xs-12">

                            <div class="controls">

                                <img style="max-height: 200px;" src="<?php echo base_url('uploads/'.$category_info_by_id->category_img);?>" />

                                <a class="btn btn-danger" id="delete_catelv1" href="<?php echo $category_info_by_id->category_img;?>"> <i class="halflings-icon white trash"></i> </a>

                                <div class="delete_catelv1"></div>

                            </div>

                        </div> 



                        <div class="control-group col-md-12 col-xs-12">

                            <label class="control-label" for="textarea2"><b>MÔ TẢ DANH MỤC</b></label>

                            <div class="controls">

                                <textarea class="ckeditor" id="content" name="category_description" rows="5"><?php echo $category_info_by_id->category_description;?></textarea>

                            </div>

                        </div>



                        <div class="wap-seo">

                            <label class="i_title" for="fileInput"> <b>NỘI DUNG SEO</b></label>

                            <div class="clear"></div>



                            <div class="control-group">

                                <label class="control-label" for="fileInput">Tiêu đề</label>

                                <div class="controls">

                                    <input class="form-control" value="<?php echo $category_info_by_id->title;?>" id="title" name="title" type="text"/>

                                </div>

                            </div>

                            <div class="control-group">

                                <label class="control-label" for="fileInput">SEO Slug</label>

                                <div class="controls">

                                   <input class="form-control" value="<?php echo url_title(strtolower($category_info_by_id->slug));?>" id="slug" name="slug" type="text"/>

                               </div>

                           </div>



                             

                            <div class="control-group">

                                <label class="control-label" for="fileInput">Mô tả seo</label>

                                <div class="controls">

                                    <textarea class="i-des-sort form-control" id="category_description_seo" name="category_description_seo" rows="3"><?php echo $category_info_by_id->category_description_seo;?></textarea>

                                </div>

                            </div>



                            <div class="control-group">

                                <label class="control-label" for="textarea2">Meta Keywords</label>

                                <div class="controls">

                                    <textarea class="i-des-sort form-control" id="keywords" name="keywords" rows="2"><?php echo $category_info_by_id->keywords;?></textarea>

                                </div>

                            </div>

                        </div><!--end wapseo-->



                        <div class="control-group col-md-12 col-xs-12">

                            <div class="controls col-md-3 col-xs-12">
                             <label>Trạng thái</label>
                                <select class="form-control" name="cate_status">
                                    <option value="1">Hiển thị</option>
                                    <option value="0">Ẩn</option>
                                </select>
                            </div>

                            <div class="controls col-md-3 col-xs-12">
                             <label>Nổi bật</label>
                                <select class="form-control" name="noibat">
                                    <option value="1">Hiển thị</option>
                                    <option value="0">Ẩn</option>
                                </select>
                            </div>

                        </div>

                        <div class="control-group col-md-9 col-xs-12"></div>

                        <div class="control-group col-md-12 col-xs-12">

                            <button type="submit" id="save_category" class="btn btn-primary">Lưu thay đổi</button>

                            <button class="btn"><a href="<?php echo base_url('manage/category')?>">Hủy</a></button>

                        </div>

                    </fieldset>

                </form>   

            </div>

        </div><!--/span-->

    </div><!--/row-->

</div><!--/.fluid-container-->



<!-- end: Content -->

<script>

    CKEDITOR.replace( 'content', {

        

        height: 400,

        filebrowserUploadUrl: "<?php echo base_url('upload_content.php')?>",



    } );

  </script>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"> </script>

<!-- end: Content -->

<script type="text/javascript">

    $(document).ready(function() {
        document.formName.cate_status.value = <?php echo $category_info_by_id->cate_status;?>;
        document.formName.noibat.value = <?php echo $category_info_by_id->noibat;?>;


       $('#delete_catebanner').click(function() {

        var delete_catebanner = $("#delete_catebanner").attr("href");

        console.log(delete_catebanner);

        var id =$("#get_id_cate").val();

        console.log(id);

        $.ajax({

            url: '<?=base_url()?>ajax/ajax_delete_catebanner',

            method: "POST",

            data: {

                delete_catebanner: delete_catebanner,id:id

            },

            async: true,

            dataType: 'html',

            success: function(data) {

                console.log(data);

                $('.delete_catebanner').html("<b>Bạn đã xóa thành công</b>");

                location.reload();

            }

        });

        return false;

    });



       $('#delete_catelv1').click(function() {

        var delete_catelv1 = $("#delete_catelv1").attr("href");

        console.log(delete_catelv1);

        var id =$("#get_id_cate").val();

        console.log(id);

        $.ajax({

            url: '<?=base_url()?>ajax/ajax_delete_catelv1',

            method: "POST",

            data: {

                delete_catelv1: delete_catelv1,id:id

            },

            async: true,

            dataType: 'html',

            success: function(data) {

                console.log(data);

                $('.delete_catelv1').html("<b>Bạn đã xóa thành công</b>");

                location.reload();



            }

        });

        return false;

    });







   });





</script>