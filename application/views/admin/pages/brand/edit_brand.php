<!-- start: Content -->
<div  class="col-md-10 col-xs-12">
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?php echo base_url('dashboard')?>">Home</a>
			<i class="icon-angle-right"></i> 
		</li>
		<li>
			<i class="icon-edit"></i>
			<a href="<?php echo base_url('edit/brand/'.$brand_info_by_id->brand_id)?>">Edit Thương hiệu</a>
		</li>
	</ul>

	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon edit"></i><span class="break"></span>Điều chỉnh Thương hiệu</h2>
				<div class="box-icon">
					<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
					<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
					<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
				</div>
			</div>
			<style type="text/css">
				#result{color:red;padding: 5px}
				#result p{color:red}
			</style>
			<div id="result">
				<p><?php echo $this->session->flashdata('message');?></p>
			</div>
			<div class="box-content">
				<form class="form-horizontal" action="<?php echo base_url('update/brand/'.$brand_info_by_id->brand_id)?>" method="post" enctype="multipart/form-data">
					<fieldset>
						<div class="control-group col-md-10 col-xs-12">
							<label>Tên Thương hiệu</label>
							<div class="controls">
								<input value="<?php echo $brand_info_by_id->brand_name?>" class="form-control" name="brand_name" id="fileInput" type="text"/>
							</div>
						</div>   

						<!-- <div class="control-group col-md-2 col-xs-12">
							<label>STT</label>
							<div class="controls">
								<input value="<?php echo $brand_info_by_id->brand_stt?>" class="form-control" name="brand_stt" type="text"/>
							</div>
						</div> -->
						<div class="control-group col-md-3 col-xs-12">
							<label class="control-label" for="fileInput">Banner page</label>
							<div class="controls">
								<input class="form-control" name="brand_banner"  type="file"/>
							</div>
						</div>

						<div class="control-group col-md-9 col-xs-12">
							<div class="controls">
								<img style="max-height: 200px;" src="<?php echo base_url('uploads/thuonghieu/'.$brand_info_by_id->brand_banner);?>" />
							</div>
						</div> 
						<div class="clear"></div>

						<div class="control-group col-md-3 col-xs-12">
							<label class="control-label" for="fileInput">Logo Thương hiệu</label>
							<div class="controls">
								<input class="form-control" name="brand_image" id="fileInput" type="file"/>
								<input class="form-control" name="brand_delete_image" value="<?php echo base_url('uploads/'.$brand_info_by_id->brand_image);?>" type="hidden"/>
							</div>
						</div>

						<div class="control-group col-md-6 col-xs-12">
							<div class="controls">
								<img style="max-height: 200px;" src="<?php echo base_url('uploads/thuonghieu/'.$brand_info_by_id->brand_image);?>" />
							</div>
						</div> 

						<div class="control-group col-md-12 col-xs-12">
							<label class="control-label" for="textarea2"><b>GIỚI THIỆU VỀ Thương hiệu</b></label>
							<div class="controls">
								<textarea class="cleditor form-control"  name="brand_description" rows="4"><?php echo $brand_info_by_id->brand_description;?></textarea>
							</div>
						</div>

						<div class="wap-seo">
							<label class="i_title" for="fileInput"> <b>NỘI DUNG SEO</b></label>
							<div class="clear"></div>

							<div class="control-group">
								<label class="control-label" for="fileInput">Tiêu đề SEO</label>
								<div class="controls">
									<input class="form-control" value="<?php echo $brand_info_by_id->brand_title;?>"  name="brand_title" type="text"/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="fileInput">SEO slug</label>
								<div class="controls">
									<input class="form-control" 
									value="<?php echo url_title(strtolower($brand_info_by_id->brand_slug));?>" id="brand_slug" name="brand_slug" type="text"/>                            </div>
								</div>

								<div class="control-group">
									<label class="control-label" for="fileInput">Mô tả SEO</label>
									<div class="controls">
										<textarea class="i-des-sort form-control"  name="brand_description_seo" rows="3"><?php echo $brand_info_by_id->brand_description_seo;?></textarea>
									</div>
								</div>

								<div class="control-group">
									<label class="control-label" for="textarea2">Meta Keywords</label>
									<div class="controls">
										<textarea class="i-des-sort form-control" id="keywords" name="brand_keywords" rows="2"><?php echo $brand_info_by_id->brand_keywords;?></textarea>
									</div>
								</div>

							</div><!--end wapseo-->

							<div class="control-group col-md-3 col-xs-12">
								<label class="control-label" for="textarea2">Trạng thái</label>
								<div class="controls">
									<select class="form-control" name="brand_publication_status">
										<option value="1">Hiện</option>
										<option value="0">Ẩn</option>
									</select>
								</div>
							</div>
							<div class="control-group col-md-9 col-xs-12"></div>

							<div class="control-group col-md-12 col-xs-12">
								<button type="submit" class="btn btn-primary">Lưu thay đổi</button>
								<button type="reset" class="btn">Hủy</button>
							</div>
						</fieldset>
					</form>   

				</div>
			</div><!--/span-->

		</div><!--/row-->


	</div><!--/.fluid-container-->

<!-- end: Content -->