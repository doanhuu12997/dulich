
<!-- start: Content -->
<div  class="col-md-10 col-xs-12">
 <ul class="breadcrumb">
  <li>
    <i class="icon-home"></i>
    <a href="<?php echo base_url('dashboard')?>">Home</a> 
    <i class="icon-angle-right"></i>
  </li>

  <?php 
  $query =$this->db->query("select * from tbl_user where  user_id='".$this->session->userdata('user_id')."'");
  $rs_role_user = $query->row();

  $jquery_count=$this->db->query("select count(*) as count from tbl_category ");
  $rs_count=$jquery_count->row();
  if($rs_role_user->lv1==1){  ?>
    <li>
      <a href="<?php echo base_url('manage/category')?>">Danh mục (<?php echo $rs_count->count ;?>)</a>
    </li>
  <?php } /*else { echo "";}*/ ?>   

    <?php 
    $jquery_count=$this->db->query("select count(*) as count from tbl_comment ");
    $rs_count_danhgia=$jquery_count->row();
    ?>
    <li>
      <a href="<?php echo base_url('manage/comment')?>"> <i class="fa fa-comments-o"></i> Đánh giá (<?php echo $rs_count_danhgia->count ;?>)</a>
    </li>

  <?php 
  $jquery_count=$this->db->query("select count(*) as count from tbl_comment_product ");
  $rs_count=$jquery_count->row();?>

   <?php 
  $q_count_m=$this->db->query("select count(*) as count from tbl_muanhanh ");
  $rs_m=$q_count_m->row();?>

  <li>
    <a href="<?php echo base_url('manage/commentproduct')?>">
      <i class="fa fa-commenting-o"></i> Bình luận (<?php  echo $rs_count->count;?>)
    </a>
  </li>

  <li>
    <a href="<?php echo base_url('manage/muanhanh')?>">
      <i class="fa fa-bolt"></i> Đăng ký tư vấn (<?php  echo $rs_m->count;?>)
    </a>
  </li>

  <div class="row-fluid sortable">    
    <div class="box span12">
      <div class="box-header" data-original-title>
        <h2><i class="fa fa-book"></i><span class="break"></span>Quản lý Tour</h2>
        <div class="box-icon">
          <a href="#" class="btn-setting"><i class="fa fa-cog fa-spin fa-1x fa-fw"></i></a>
          <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
          <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
        </div>
      </div>

      <div id="result">
        <p><?php echo $this->session->flashdata('message'); ?></p>
      </div>

      <div class="box-content">
        <?php if($role_user->role_add_product==1){  ?>
          <div class="add_cate">
            <a href="<?php echo base_url('add/product')?>">
              <i class="fa fa-plus-circle"></i>
              <span class="hidden-tablet"> Thêm</span>
            </a>
          </div>
        <?php }?>

        <div class="form-group col-md-3 col-xs-12">
          <select class="form-control" name="category_c1" id="category_c1" >
            <option value="">Chọn danh mục</option>
                        <?php  foreach($getcategory as $row){
                        $sub_cate= $this->product_model->sub_category($row->id); ?>
                            <option value="<?php echo $row->id; ?>"><?php echo $row->category_name; ?></option>
                     <?php foreach ($sub_cate as $sub) {
                 $sub_cate_child= $this->product_model->sub_category($sub->id); ?>
                   <option value="<?php echo $sub->id; ?>">---<?php echo $sub->category_name; ?>---</option>
                 <?php foreach ($sub_cate_child as $sub_child) {?>
                    <option value="<?php echo $sub_child->id; ?>">------------<?php echo $sub_child->category_name; ?>----------</option>
                  <?php }?>
                     <?php }?>
                  <?php } ?>
          </select>
        </div><!--End danh mục cấp 1-->
        <!--
        <div class="form-group col-md-3 col-xs-12">
          <select class="form-control" name="deal" id="deal" >
            <option value="">Chọn Deal </option>
            <option value="0000-00-00 00:00:00">Tour chạy deal</option>
          </select>
        </div>
        -->
        <!--End deal-->
        <!-- 
        <div class="form-group col-md-3 col-xs-12">
          <select class="form-control" name="function" id="function" >
            <option value="">Chọn Chức năng</option>
              <?php foreach($get_function as $v) {?>
              <option value="<?php echo $v->brand_id;?>"><?php echo $v->brand_name;?></option>
              <?php }?>
          </select>
        </div>
        <!--End danh mục cấp 1-->



        <div class="form-group col-md-3 col-xs-12">
          <input name="search_product" id="search_product" class="form-control" placeholder="Nhập từ khóa tìm kiếm" type="text" />
        </div>

        <!--    
        <div class="form-group col-md-6 col-xs-12 i-md6ex">
          <form method="post" id="import_form" enctype="multipart/form-data" class="inline">
            <input type="file" name="file" id="file" required accept=".xls, .xlsx" />
            <button type="submit" name="import" class="btn btn-info i-excel" style="color: #000 !important">Import Excel</button>
          </form>
          <form method="post"  action="<?php echo base_url('export_excel') ?>" class="inline">
            <button type="submit" name="export" class="btn btn-info i-excel" style="color: #000 !important">Export</button>
          </form>
        </div>
        -->
        <table class="table table-striped table-bordered bootstrap-datatable datatable">
          <thead>
            <tr>
              <th>Stt.</th>
              <th width="3%"><button type="button" name="delete_all" id="delete_all" class="btn btn-primary confirmClick bnt-delete-all">Xóa nhiều</button></th>
              <th>Tên </th>
              <!--   <th>Catelogy</th>-->
              <th>Hình ảnh</th>
              <!--<th>Price</th>-->
             <th>Home</th>

 <!--               <th>Nổi bật</th>
 -->            
<!--    <th>Bán chạy</th>
 --><!--                   <th>Mới Về</th>
 -->
           <!--   <th>Tin dùng</th>-->
              <!-- <th> Sale</th>-->
<!--               <th>Hot</th>
 -->      
         <!--   <th>Số lượng</th>-->
              <th>Tour Hot</th>
<!--               <th>Còn hàng</th>
 -->              <th>View</th>
              <!--  <th>Thumb</th>-->
              <th>Hiển thị</th>
              <th>Sửa</th>
              <th>Xóa</th>
            </tr>
          </thead>   
          <tbody id="load_catelogy">
            <?php $this->load->view('admin/pages/ajax/ajax_product'); ?>
          </tbody>
        </table>  

        <div class="clear"></div>

        <div id="pagination">
          <ul class="tsc_pagination">
          <!-- Show pagination links -->
            <?php foreach ($links as $link) 
            {
              echo "<li>". $link."</li>";
            } ?>
          </ul>
        </div>
      </div>
    </div><!--/span-->
  </div><!--/row-->
</div><!--/.fluid-container-->

<style type="text/css">
  .paging_bootstrap 
  {
    display: none;
  }
  .dataTables_filter
  {
    display: none;
  }
  .dataTables_info
  {
    display: none;
  }
</style>
