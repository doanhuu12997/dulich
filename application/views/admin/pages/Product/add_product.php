<!-- start: Content -->

<div class="col-md-10 col-xs-12">

  <ul class="breadcrumb">

    <li>

      <i class="icon-home"></i>

      <a href="<?php echo base_url('dashboard') ?>">Home</a>

      <i class="icon-angle-right"></i>

    </li>

  </ul>



  <div class="row-fluid sortable">

    <div class="box span12">

      <div class="box-header" data-original-title>

        <h2>

          <i class="halflings-icon edit"></i>

          <span class="break"></span>

          Thêm

        </h2>

        <div class="box-icon">

          <a href="#" class="btn-setting">

            <i class="halflings-icon wrench"></i>

          </a>

          <a href="#" class="btn-minimize">

            <i class="halflings-icon chevron-up"></i>

          </a>

          <a href="#" class="btn-close">

            <i class="halflings-icon remove"></i>

          </a>

        </div>

      </div>

      <style type="text/css">
        #result_messger {
          color: red;
          padding: 5px
        }

        #result_messger p {
          color: red
        }
      </style>

      <div id="result_messger">

        <p><?php echo $this->session->flashdata('message'); ?></p>

      </div>

      <div class="box-content">

        <form class="form-horizontal" action="<?php echo base_url('save/product'); ?>" method="post" enctype="multipart/form-data">

          <fieldset>

            <div class="control-group col-md-3 col-xs-12">



              <label class="control-label" for="fileInput">Tour nhiều Danh mục</label>



              <div class="controls">



                <select multiple="multiple" class="form-control select2" name="list_cate[]" required>



                  <option value="">Chọn danh mục</option>



                  <?php foreach ($getcategory as $row) {



                    $sub_cate = $this->product_model->sub_category($row->id); ?>



                    <option value="<?php echo $row->id; ?>"><?php echo $row->category_name; ?></option>







                    <?php foreach ($sub_cate as $sub) {



                      $sub_cate_child = $this->product_model->sub_category($sub->id); ?>







                      <option value="<?php echo $sub->id; ?>">---<?php echo $sub->category_name; ?>---
                      </option>







                      <?php foreach ($sub_cate_child as $sub_child) { ?>



                        <option value="<?php echo $sub_child->id; ?>">
                          ------------<?php echo $sub_child->category_name; ?>----------</option>



                      <?php } ?>







                    <?php } ?>







                  <?php } ?>



                </select>



              </div>



            </div>





            <div class="control-group col-md-3 col-xs-12">

              <label class="control-label" for="fileInput">Tên Tour</label>

              <div class="controls">

                <input class="form-control" name="product_title" id="fileInput" type="text" />

              </div>

            </div>

            <div class="control-group col-md-3 col-xs-12">

              <label class="control-label" for="fileInput">Mã Tour</label>

              <div class="controls">

                <input class="form-control" name="code_tour" type="text" />

              </div>

            </div>

            <div class="control-group col-md-3 col-xs-12">

              <label class="control-label" for="fileInput">Thời gian</label>

              <div class="controls">

                <input class="form-control" name="product_thoigian" type="text" />

              </div>

            </div>

            <div class="clear"></div>



            <div class="control-group col-md-4 col-xs-12">

              <label class="control-label" for="fileInput" style="padding-top:0px; margin-bottom: 0px;">Phương tiện</label>

              <div class="controls">

                <input class="form-control" name="product_phuongtien" type="text" />

              </div>

            </div>



            <div class="control-group col-md-4 col-xs-12">

              <label class="control-label" for="fileInput"></label>



              <div class="controls">

                <select class="form-control" name="product_xuatphat" required="required">

                  <option value="">Nơi khởi hành</option>

                  <?php foreach ($all_published_xuatphat as $single_xp) { ?>

                    <option value="<?php echo $single_xp->xuatxu_id; ?>">
                      <?php echo $single_xp->xuatxu_name; ?></option>

                  <?php } ?>

                </select>

              </div>

            </div>





            <div class="control-group col-md-4 col-xs-12">

              <label class="control-label" for="fileInput"></label>



              <div class="controls">

                <select class="form-control" name="product_diemden" required="required">

                  <option value="">Điểm đến</option>

                  <?php foreach ($all_published_diemden as $single_dd) { ?>

                    <option value="<?php echo $single_dd->congdung_id; ?>">
                      <?php echo $single_dd->congdung_name; ?></option>

                  <?php } ?>

                </select>

              </div>

            </div>


            <div class="clear"></div>
            <div class="control-group col-md-10 col-xs-12">
              <label class="control-label">Link Download</label>
              <div class="controls">
                <input class="form-control" name="download" type="text" />
              </div>
            </div>
            <div class="control-group col-md-2 col-xs-12">
              <label class="control-label">Số sao</label>
              <div class="controls">
                <input class="form-control" name="star" type="text" />
              </div>
            </div>

            <div class="control-group col-xs-12 col-md-12">

              <div class="controls">

                <div class="multi-field-wrapper">

                  <div class="multi-fields">

                    <div class="multi-field">

                      <label class="control-label"><i class="fa fa-calendar"></i> Ngày khởi
                        hành</label>

                      <input type="date" name="date_begin[]">
                      <i class="fa fa-plane"></i>
                      <input type="text" placeholder="Phương tiện" name="move[]">

                      <i class="fa fa-usd"></i>

                      <input type="text" class="outputprice" placeholder="Giá người lớn" name="price_people[]">

                      <i class="fa fa-usd"></i>

                      <input type="text" class="outputprice" placeholder="Giá trẻ em" name="price_child[]">

                      <i class="fa fa-usd"></i>

                      <input type="text" class="outputprice" placeholder="Giá em bé" name="price_baby[]">
                      <i class="fa fa-bed"></i>
                      <input type="text" name="status[]" placeholder="Tình trạng">

                      <button type="button" class="remove-field btn btn-primary"><i class="fa fa-trash-o"></i> Xoá</button>

                    </div>

                  </div>

                  <button type="button" class="add-field btn btn-primary"> <i class="fa fa-plus"></i>

                    Thêm trường</button>

                </div>

              </div>

            </div>



            <div class="clear"></div>

            <div class="control-group col-xs-12 col-md-12">

              <div class="controls">

                <div class="multi-field-wrapper_tour">

                  <div class="multi-fields_tour">

                    <div class="multi-field_tour"></div>

                  </div>

                  <button type="button" class="add-field_tour btn btn-primary"> <i class="fa fa-plus"></i>

                    Thêm Chương trình Tour</button>

                </div>

              </div>

            </div>





            <!--   <div class="control-group col-md-3 col-xs-12">

              <div class="controls">

                <select class="form-control" name="product_brand">

                  <option value="">Thương hiệu</option>

                  <?php foreach ($all_published_brand as $single_brand) { ?>

                    <option value="<?php echo $single_brand->brand_id; ?>"><?php echo $single_brand->brand_name; ?></option>

                  <?php } ?>

                </select>

              </div>

            </div>  -->





            <div class="clear"></div>



            <div class="col-md-4 col-xs-12">

              <div class="col-md-6 col-xs-12">

                <label style="text-align: left;" class="control-label" for="fileInput">

                  <i class="fa fa-picture-o"></i> Hình đại diện <i style="color:#f00;font-size: 11px;">(360px x 260px)Dài x Cao</i>

                </label>

                <div class="box_upload">

                  <div class="js--image-preview"></div>

                  <div class="upload-options">

                    <label>

                      <input type="file" class="image-upload" name="product_image" accept="image/*" />

                    </label>

                  </div>

                </div>

                <!--end box_upload-->

              </div>



            </div>

            <!--end-md6-->

            <div class="col-md-8 col-xs-12">



              <div class="control-group col-md-6 col-xs-12">

                <label class="control-label" for="fileInput">Giá gốc (vnđ)</label>

                <div class="controls">

                  <input class="form-control outputprice" name="product_price" id="fileInput" type="text" />

                </div>

              </div>

              <div class="control-group col-md-6 col-xs-12">

                <label class="control-label " for="fileInput">Giá giảm (vnđ)</label>

                <div class="controls">

                  <input class="form-control outputprice" name="product_price_sale" id="fileInput" type="text" />

                </div>

              </div>



              <div class="control-group col-md-3 col-xs-12">

                <label class="control-label">Trạng thái</label>

                <div class="controls">

                  <select class="form-control" name="publication_status">

                    <option value="1">Hiện</option>

                    <option value="0">Ẩn</option>

                  </select>

                </div>

              </div>

            </div>

            <!--end-md6-->



            <div class="col-md-12 col-xs-12">

              <br>

              <div class="control-group">

                <label class="control-label" for="fileInput">Upload hình thumbnail</label>

                <div class="controls">

                  <input id="files" name="userfile[]" type="file" accept=".png,.jpg,.jpeg,.gif" multiple />

                  <output id="result" />

                </div>

              </div>

            </div>





            <div class="clear"></div>



            <div class="col-sm-3">

              <a href="#" class="nav-tabs-dropdown btn btn-block btn-primary">Tabs</a>

              <ul id="nav-tabs-wrapper" class="nav nav-tabs nav-pills nav-stacked well">

                <li class="active"><a href="#vtab1" data-toggle="tab">Mô tả</a></li>

                <li><a href="#vtab2" data-toggle="tab">Chính sách Tour</a></li>

                <!-- <li><a href="#vtab3" data-toggle="tab">Lưu ý khác</a></li> -->

                <li><a href="#vtab4" data-toggle="tab">Ưu đãi đặc biệt</a></li>



              </ul>

            </div>

            <div class="col-sm-9">

              <div class="tab-content">

                <div role="tabpanel" class="tab-pane fade in active" id="vtab1">

                  <textarea class="form-control ckeditor" name="product_short_description" rows="4"></textarea>

                </div>

                <div role="tabpanel" class="tab-pane fade" id="vtab2">

                  <textarea class="ckeditor" id="content" name="product_long_description" rows="4"></textarea>

                </div>



                <!-- <div role="tabpanel" class="tab-pane fade" id="vtab3">

                      <textarea class="ckeditor"  name="product_luuykhac" rows="4"></textarea>

                </div> -->



                <div role="tabpanel" class="tab-pane fade" id="vtab4">

                  <textarea class="ckeditor" name="product_dieukien" rows="4"></textarea>

                </div>



              </div>

            </div>

      </div>

      </fieldset>

    </div>

  </div>

  <div class="clear"></div>

  <div class="wap-seo">

    <label class="i_title" for="fileInput">

      <b>NỘI DUNG SEO</b>

    </label>

    <div class="clear"></div>

    <div class="control-group">

      <label class="control-label" for="fileInput">Tiêu đề</label>

      <div class="controls">

        <input class="form-control form_input" name="product_title_bar" id="fileInput" type="text" />

      </div>

    </div>

    <div class="control-group">

      <label class="control-label" for="fileInput">Mô tả seo</label>

      <div class="controls">

        <textarea class="form-control" name="product_tag" rows="4"></textarea>

      </div>

    </div>



    <div class="control-group">

      <label class="control-label" for="textarea2">

        <i class="fa fa-book"></i> Meta Keywords

      </label>

      <div class="controls">

        <textarea class="i-des-sort form-control" id="product_keywords" name="product_keywords" rows="6"></textarea>

      </div>

    </div>

  </div>

  <!--end wapseo-->

  <!--   

            <div class="control-group col-md-12 col-xs-12">

              <label class="control-label"> Nổi bật</label>

              <div class="controls">

                <input value="0" name="product_feature" id="fileInput" type="radio"/> Ẩn

                <input value="1" name="product_feature" id="fileInput" type="radio" />Hiện

              </div>

            </div>

          -->

  <div class="form-actions col-md-12 col-xs-12 ">

    <button type="submit" class="btn btn-primary">Lưu thay đổi</button>

    <button class="btn">

      <a href="<?php echo base_url('manage/product') ?>">Hủy</a>

    </button>

  </div>

  </fieldset>

  </form>

</div>

</div>

</div>
<!--/span-->

</div>
<!--/row-->

</div>

<!--/.fluid-container-->

<!-- end: Content -->



<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"> </script>



<script src="<?php echo base_url() ?>assets/admin/js/stopExecutionOnTimeout.js"></script>

<script type="text/javascript">
  /* -------------------- Check Browser --------------------- */

  function initImageUpload(box) {

    let uploadField = box.querySelector('.image-upload');

    uploadField.addEventListener('change', getFile);



    function getFile(e) {

      let file = e.currentTarget.files[0];

      checkType(file);

    }



    function previewImage(file) {

      let thumb = box.querySelector('.js--image-preview'),

        reader = new FileReader();



      reader.onload = function() {

        thumb.style.backgroundImage = 'url(' + reader.result + ')';

      }

      reader.readAsDataURL(file);

      thumb.className += ' js--no-default';

    }



    function checkType(file) {

      let imageType = /image.*/;

      if (!file.type.match(imageType)) {

        throw 'Datei ist kein Bild';

      } else if (!file) {

        throw 'Kein Bild gewählt';

      } else {

        previewImage(file);

      }

    }



  }



  // initialize box-scope

  var boxes = document.querySelectorAll('.box_upload');



  for (let i = 0; i < boxes.length; i++) {
    if (window.CP.shouldStopExecution(1)) {
      break;
    }

    let box = boxes[i];

    initDropEffect(box);

    initImageUpload(box);

  }

  window.CP.exitedLoop(1);



  /// drop-effect

  function initDropEffect(box) {

    let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;



    // get clickable area for drop effect

    area = box.querySelector('.js--image-preview');

    area.addEventListener('click', fireRipple);



    function fireRipple(e) {

      area = e.currentTarget

      // create drop

      if (!drop) {

        drop = document.createElement('span');

        drop.className = 'drop';

        this.appendChild(drop);

      }

      // reset animate class

      drop.className = 'drop';



      // calculate dimensions of area (longest side)

      areaWidth = getComputedStyle(this, null).getPropertyValue("width");

      areaHeight = getComputedStyle(this, null).getPropertyValue("height");

      maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));



      // set drop dimensions to fill area

      drop.style.width = maxDistance + 'px';

      drop.style.height = maxDistance + 'px';



      // calculate dimensions of drop

      dropWidth = getComputedStyle(this, null).getPropertyValue("width");

      dropHeight = getComputedStyle(this, null).getPropertyValue("height");



      // calculate relative coordinates of click

      // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center

      x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10) / 2);

      y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10) / 2) - 30;



      // position drop and animate

      drop.style.top = y + 'px';

      drop.style.left = x + 'px';

      drop.className += ' animate';

      e.stopPropagation();



    }

  }
</script>





<script type="text/javascript">
  window.onload = function() {

    //Check File API support

    if (window.File && window.FileList && window.FileReader)

    {

      var filesInput = document.getElementById("files");

      filesInput.addEventListener("change", function(event) {

        var files = event.target.files; //FileList object

        var output = document.getElementById("result");

        for (var i = 0; i < files.length; i++)

        {

          var file = files[i];

          //Only pics

          if (!file.type.match('image'))

            continue;

          var picReader = new FileReader();

          picReader.addEventListener("load", function(event) {

            var picFile = event.target;

            var div = document.createElement("div");

            div.innerHTML = "<img class='thumbnail' src='" + picFile.result + "'" +

              "title='" + picFile.name +
              "'/> <a  class='remove_pict'><i class='fa fa-trash-o'></i></a>";

            output.insertBefore(div, null);

            div.children[1].addEventListener("click", function(event) {

              div.parentNode.removeChild(div);

            });

          });

          //Read the image

          picReader.readAsDataURL(file);

        }

      });

    } else

    {

      console.log("Your browser does not support File API");

    }

  }





  var next = 0;

  var newIn = '';

  var x = "</" + "script>";

  $(".add-field_tour").click(function(e) {

    next = next + 1;



    newIn = '<div class="clear"></div>';

    newIn += '<div class="wap_add">';

    newIn += '<div class="form-group">';

    newIn += '<div class="col-md-4">';

    newIn += '<label class="control-label"> <i class="fa fa-calendar"></i> Ngày Tour</label>';
    newIn += '<input type="text" class="form-control"  name="date[]">';
    newIn += '</div>';
    newIn += '<label class="col-md-4 control-label label_thucdon">Thực đơn</label>';
    newIn += '<div class="col-md-4">';
    newIn += '<label class="checkbox-inline">';
    newIn += '<input type="checkbox" id="checkbox_as_' + next + '" name="ansang_' + next + '" >';

    newIn += 'Ăn sáng';
    newIn += '</label>';
    newIn += '<label class="checkbox-inline">';
    newIn += '<input type="checkbox" name="antrua_' + next + '" id="checkbox_tr_' + next + '">';
    newIn += ' Ăn trưa';
    newIn += '</label>';
    newIn += '<label class="checkbox-inline">';
    newIn += '<input type="checkbox" name="antoi_' + next + '"  id="checkbox_atoi_' + next + '">';
    newIn += ' Ăn tối';
    newIn += '</label>';
    newIn += ' </div>';
    newIn += '</div>';


    newIn += '<div class="clear"></div>';
    newIn +=
      ' <button type="button" class="remove-field_tour btn btn-primary"><i class="fa fa-trash-o"></i> Xoá</button>';
    newIn += '<div class="clear"></div>';
    newIn += '<label class="control-label"><i class="fa fa-clock-o"></i>Thời gian</label>';
    newIn += '<textarea class="ckeditor" id="content_time_' + next +
      '" name="thoigian[]"></textarea><script>var ck_sang = $("#checkbox_as_' + next +
      '");$("input").on("click",function (){if (ck_sang.is(":checked")) {$("#checkbox_as_' + next +
      '").val("yes");} else {$("#checkbox_as_' + next + '").val("no");}});var ck_tr = $("#checkbox_tr_' +
      next + '");$("input").on("click",function () {if(ck_tr.is(":checked")){$("#checkbox_tr_' + next +
      '").val("yes");} else {$("#checkbox_tr_' + next + '").val("no");}});var ck_to = $("#checkbox_atoi_' +
      next + '");$("input").on("click",function (){if(ck_to.is(":checked")){$("#checkbox_atoi_' + next +
      '").val("yes");}else{$("#checkbox_atoi_' + next + '").val("no");}});CKEDITOR.replace("content_time_' +
      next + '", {height: 300,filebrowserUploadUrl:"<?php echo base_url('upload_content.php') ?>",} );' + x;

    newIn += '</div>';
    $('.multi-field_tour:last-child').append(newIn);
    $('.remove-field_tour').click(function() {
      $('.wap_add').remove();

    });

  });



  CKEDITOR.replace('content', {
    height: 400,
    filebrowserUploadUrl: "<?php echo base_url('upload_content.php') ?>",
  });
</script>