<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard') ?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/role_user') ?>">Quản lý quyền User</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <?php if($role_user->role_add_role_user==1){  ?>
            <div class="add_cate">
                    <a href="<?php echo base_url('add/role_user')?>">
                        <i class="fa fa-plus-circle"></i>
                        <span class="hidden-tablet">Thêm quyền User</span>
                    </a>
                </div>
            <?php }?>
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Quản lý Quyền User</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="fa fa-cog fa-spin fa-1x fa-fw"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <style type="text/css">
                #result{color:red;padding:5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>
            <div class="box-content">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Sr.</th>
                            <th>User Name</th>
                            <th>User email</th>
                            <th>Password</th>
                            <th>User role</th>
                            <th>Actions</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($all_categroy as $single_role_user) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><i class="fa fa-user"></i>  <?php echo $single_role_user->user_name ?></td>
                                <td><i class="fa fa-envelope-o"></i>
                                   <?php echo $single_role_user->user_email ?></td>
                                <td><?php echo $single_role_user->user_password ?></td>
                                <td><?php echo $single_role_user->user_role ?></td>
                                <td class="center">
                                    <a class="btn btn-info" href="<?php echo base_url('edit/role_user/' . $single_role_user->user_id); ?>">
                                        <i class="halflings-icon white edit"></i>  
                                    </a>


                                    <a class="btn btn-danger" href="<?php echo base_url('delete/role_user/' . $single_role_user->user_id); ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>

                                </td>

                            </tr>
                        <?php } ?>
                    </tbody>
                </table>            
            </div>

        </div><!--/span-->


    </div><!--/row-->



</div><!--/.fluid-container-->

<!-- end: Content -->