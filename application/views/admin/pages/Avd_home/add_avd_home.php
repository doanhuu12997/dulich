<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard')?>">Home</a>
            <i class="icon-angle-right"></i> 
        </li>
        <li>
            <i class="icon-edit"></i>
            <a href="<?php echo base_url('add/avd_home')?>">Thêm Thương hiệu Yêu thích</a>
        </li>
    </ul>

    <div class="row-fluid sortable">
        <div class="box">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon edit"></i><span class="break"></span>Thêm Thương hiệu Yêu thích</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message');?></p>
            </div>
            <div class="box-content">
                <form class="form-horizontal" action="<?php echo base_url('save/avd_home');?>" method="post" enctype="multipart/form-data">
                    <fieldset>

                      <div class="control-group col-md-12 col-xs-12">
                            <label class="control-label" for="fileInput"> Tên</label>
                            <div class="controls">
                                <input class="form-control" name="avd_home_title" type="text"/>
                            </div>
                        </div> 

                          
                        
                        <div class="control-group col-md-3 col-xs-12">
                            <label class="control-label" for="fileInput"> Image</label>
                            <div class="controls">
                                <input class="form-control" name="avd_home_image" type="file"/>
                            </div>
                        </div>
                         <div class="clear"></div>
                        <div class="control-group col-md-9 col-xs-12">
                            <label class="control-label" for="fileInput"> Link</label>
                            <div class="controls">
                                <input class="form-control"  name="avd_home_link" type="url"/>
                            </div>
                        </div>
                        
                                
                        <div class="control-group col-md-3 col-xs-12">
                            <label class="control-label" for="textarea2">Hiển thị</label>
                            <div class="controls">
                                <select name="publication_status" class="form-control">
                                    <option value="1">yes</option>
                                    <option value="0">no</option>
                                </select>
                            </div>
                        </div>
                        
                         <div class="control-group col-md-12 col-xs-12">
                         <button type="submit" id="save_category" class="btn btn-primary">Lưu thay đổi</button>
                        <button  class="btn"><a href="<?php echo base_url('manage/avd_home')?>">Hủy</a></button>

                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->

    </div><!--/row-->

    
    
</div><!--/.fluid-container-->

<!-- end: Content -->