<!-- start: Content -->
<div  class="col-md-10 col-xs-12">


    <ul class="breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="<?php echo base_url('dashboard') ?>">Home</a> 
            <i class="icon-angle-right"></i>
        </li>
        <li><a href="<?php echo base_url('manage/baogia') ?>">Manage Yêu cầu báo giá</a></li>
    </ul>

    <div class="row-fluid sortable">		
        <div class="box span12">
            <div class="box-header" data-original-title>
                <h2><i class="halflings-icon user"></i><span class="break"></span>Manage Yêu cầu báo giá</h2>
                <div class="box-icon">
                    <a href="#" class="btn-setting"><i class="fa fa-cog fa-spin fa-1x fa-fw"></i></a>
                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                </div>
            </div>

            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>

            <div class="box-content" style="height: 400px;">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Sr.</th>
                            <th>Name</th>
                            <th>Phone</th>
                            <th>Email</th>
                             <th>Địa chỉ</th>
                            <th>Nội dung</th>
                            <th>Giá</th>
                            <th>action</th>
                        </tr>
                    </thead>   
                    <tbody>
                        <?php
                        $i = 0;
                        foreach ($all_baogia as $single_baogia) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center"><?php echo $single_baogia->baogia_name; ?></td>
                                <td class="center"><?php echo $single_baogia->baogia_phone; ?></td>
                                <td class="center"><?php echo $single_baogia->baogia_email; ?></td>
                                <td class="center"><?php echo $single_baogia->baogia_diachi; ?></td>
                                <td class="center"><?php echo $single_baogia->baogia_content; ?></td>
                                <td class="center"><?php echo $single_baogia->baogia_gia; ?></td>
                                <td class="center">
                           
                                    <a class="btn btn-danger" href="<?php echo base_url('delete/baogia/' . $single_baogia->baogia_id); ?>">
                                        <i class="halflings-icon white trash"></i> 
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>            
            </div>
        </div><!--/span-->



    </div><!--/row-->



</div><!--/.fluid-container-->

<!-- end: Content -->