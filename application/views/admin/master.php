<!DOCTYPE html>

<html lang="en">

<head>

    <!-- start: Meta -->

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>Admin - <?php get_option('contact_title'); ?></title>

    <meta name="description" content="Admin <?php get_option('contact_title'); ?>">

    <meta name="author" content=" <?php get_option('contact_title'); ?>">

    <meta name="keyword" content="Admin <?php get_option('contact_title'); ?>">

    <!-- end: Meta -->

    <!-- start: Mobile Specific -->

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- end: Mobile Specific -->

    <!-- start: CSS -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link id="base-style" href="<?php echo base_url() ?>assets/admin/css/style.css" rel="stylesheet">

    <link
        href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext'
        rel='stylesheet' type='text/css'>

    <!-- end: CSS -->

    <link rel="stylesheet" type="text/css"
        href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

    <link id="bootstrap-style" href="<?php echo base_url() ?>assets/admin/css/jquery-explr-1.4.css" rel="stylesheet">

    <!--<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">



	<style class="cp-pen-styles">

	@import url(https://fonts.googleapis.com/icon?family=Material+Icons);

	@import url("https://fonts.googleapis.com/css?family=Raleway");

</style>-->

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

		<link id="ie-style" href="<?php echo base_url() ?>assets/admin/css/ie.css" rel="stylesheet">

	<![endif]-->



    <!--[if IE 9]>

		<link id="ie9style" href="<?php echo base_url() ?>assets/admin/css/ie9.css" rel="stylesheet">

	<![endif]-->



    <!-- start: Favicon -->

    <link rel="shortcut icon" type="image/x-icon"
        href="<?php echo base_url('/'); ?><?php echo get_option('site_favicon'); ?>" />

    <!-- end: Favicon -->



    <script src="<?php echo base_url() ?>ckeditor/ckeditor.js"></script>



    <script src="https://cdn.tiny.cloud/1/byrk6b9w71yaxqv30xfg1yo0dom25rhypsedx20943b8o3md/tinymce/5/tinymce.min.js">
    </script>



    <script type="text/javascript">
    tinymce.init({

        selector: '.cleditor',

        entity_encoding: "raw",

        paste_data_images: true,

        convert_urls: false,

        height: 400,

        document_base_url: '<?php echo base_url() ?>',

        plugins: [

            'advlist autolink lists link image charmap print preview hr anchor pagebreak',

            'searchreplace wordcount visualblocks visualchars code fullscreen',

            'insertdatetime media nonbreaking save table contextmenu directionality',

            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help image code'

        ],

        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | fontsizeselect',

        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',

        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',

        image_advtab: true,

        images_upload_url: '<?php echo base_url('upload.php') ?>',

        automatic_uploads: false,

        images_upload_handler: function(blobInfo, success, failure) {

            var xhr, formData;

            xhr = new XMLHttpRequest();

            xhr.withCredentials = false;

            xhr.open('POST', '<?php echo base_url('upload.php') ?>');

            xhr.onload = function() {

                var json;

                if (xhr.status != 200) {

                    failure('HTTP Error: ' + xhr.status);

                    return;

                }

                json = JSON.parse(xhr.responseText);

                if (!json || typeof json.file_path != 'string') {

                    failure('Invalid JSON: ' + xhr.responseText);

                    return;

                }

                success(json.file_path);

            };

            formData = new FormData();

            formData.append('file', blobInfo.blob(), blobInfo.filename());

            xhr.send(formData);

        },

    });
    </script>

    <!--Google API-->

    <link href="<?php echo base_url() ?>assets/admin/css/jquery-seopreview.css" rel="stylesheet">

</head>



<body>

    <!-- start: Header -->

    <div class="navbar">

        <div class="navbar-inner" style="background:<?php echo get_option('navbar'); ?> ">

            <div class="container-fluid">

                <a style="color: #fff" class="brand col-xs-8" href="<?php echo base_url('dashboard') ?>">

                    Trang quản trị admin được phát triển bởi Buffseo

                </a>

                <!-- Collect the nav links, forms, and other content for toggling -->

                <?php $get_count = $this->manageorder_model->get_count_order();

				?>

                <div class="count_cart">

                    <a href="<?php echo base_url('manage/order'); ?>" class="dropdown-toggle">

                        <span class="glyphicon glyphicon-bell alertNotificacao"></span>

                        <span class='badgeAlert'> <?php echo $get_count->total; ?></span>

                    </a>

                </div>



                <!-- /.navbar-collapse -->

                <!-- start: Header Menu -->

                <div class="nav-no-collapse header-nav">

                    <ul class="nav pull-right">

                        <li class="dropdown">

                            <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">

                                <?php echo $this->session->userdata('user_name'); ?>

                                <span class="caret"></span>

                            </a>

                            <ul class="dropdown-menu">

                                <li>

                                    <a href="<?php echo base_url('logout') ?>">

                                        <i class="halflings-icon off"></i> Đăng xuất

                                    </a>

                                </li>

                            </ul>

                        </li>

                        <!-- end: User Dropdown -->

                    </ul>

                </div>

                <!-- end: Header Menu -->

            </div>

        </div>

    </div>

    <!-- start: Header -->



    <div class="container-fluid">

        <div class="row">

            <!-- start: Main Menu -->

            <div id="sidebar-left" class="col-md-2 col-xs-12">

                <div class="d-flex justify-content-center h-100">

                    <div class="img_admin">

                        <a href="<?php echo base_url('dashboard') ?>">

                            <img src="<?php echo base_url('uploads/'); ?><?php echo get_option('site_logo'); ?>" />

                        </a>

                    </div>

                </div>

                <ul id="treexxx" class="explr-tree">

                    <li class="iconxxx-chain ">

                        <a href="#"> <i class="fa fa-book"></i> QUẢN LÝ TOUR</a>

                        <ul class=" main-menu explr-line-fix">

                            <?php

							$query = $this->db->query("select * from tbl_user where  user_id='" . $this->session->userdata('user_id') . "'");

							$rs_role_user = $query->row(); ?>

                            <?php

							$jquery_count = $this->db->query("select count(*) as count from tbl_product ");

							$rs_count = $jquery_count->row();



							if ($rs_role_user->product == '1') {  ?>

                            <li class="icon-right">

                                <a href="<?php echo base_url('manage/product') ?>"><i class="fa fa-list-alt"></i>

                                    Tour (<?php echo $rs_count->count; ?>)</a>

                            </li>

                            <?php } else {
								echo ''; ?>



                            <?php } ?>



                            <?php

							$jquery_count = $this->db->query("select count(*) as count from tbl_category ");

							$rs_count = $jquery_count->row();



							if ($rs_role_user->lv1 == 1) {  ?>

                            <li>

                                <a href="<?php echo base_url('manage/category') ?>"><i class="fa fa-cube"></i> Danh mục
                                    (<?php echo $rs_count->count; ?>)</a>

                            </li>

                            <?php } else {
								echo ''; ?>



                            <?php } ?>



                            <?php if ($rs_role_user->thuonghieu == 1) {  ?>

                            <li>

                                <a href="<?php echo base_url('manage/brand') ?>"><i class="fa fa-cube"></i> Thương hiệu <?php

																															$jquery_count = $this->db->query("select count(*) as count from tbl_brand ");

																															$rs_count = $jquery_count->row(); ?>

                                    (<?php echo $rs_count->count; ?>)</a>

                            </li>



                            <?php } else {
								echo ''; ?>







                            <?php } ?>



                            <?php if ($rs_role_user->thanhphan == 1) {  ?>

                            <li>

                                <a href="<?php echo base_url('manage/thanhphan') ?>"><i class="fa fa-key"></i> Qlý Từ
                                    khóa <?php

																																$jquery_count = $this->db->query("select count(*) as count from tbl_thanhphan ");

																																$rs_count = $jquery_count->row(); ?>

                                    (<?php echo $rs_count->count; ?>)</a>

                            </li>

                            <?php } else {
								echo ''; ?>



                            <?php } ?>



                            <?php if ($rs_role_user->xuatxu == 1) {  ?>

                            <li class="icon-config">

                                <a href="<?php echo base_url('manage/link301') ?>"> <i class="fa fa-link"></i> Link 301

                                </a>

                            </li>

                            <?php } else {
								echo ''; ?>



                            <?php } ?>



                            <li>
                                <a href="<?php echo base_url('manage/xuatxu') ?>"><i class="fa fa-car"></i> Nơi khởi
                                    hành <?php
																															$jquery_count = $this->db->query("select count(*) as count from tbl_xuatxu ");
																															$rs_count = $jquery_count->row(); ?>
                                    (<?php echo $rs_count->count; ?>)</a>
                            </li>


                            <?php if ($rs_role_user->congdung == 1) {  ?>

                            <li>

                                <a href="<?php echo base_url('manage/congdung') ?>"><i class="fa fa-car"></i> Điểm đến <?php

																															$jquery_count = $this->db->query("select count(*) as count from tbl_congdung ");

																															$rs_count = $jquery_count->row(); ?>

                                    (<?php echo $rs_count->count; ?>)</a>

                            </li>

                            <?php } else {
								echo '';
							} ?>



                            <!--



						<?php if ($rs_role_user->tinhthanh == 1) {  ?>

                            <li>

                            	<a href="<?php echo base_url('manage/city') ?>"> <i class="fa fa-map-marker" ></i> Tỉnh / Thành <?php

																																$jquery_count = $this->db->query("select count(*) as count from tbl_city ");

																																$rs_count = $jquery_count->row(); ?>

                            	(<?php echo $rs_count->count; ?>)</a>

                            </li>

                        <?php } else {
							echo ''; ?>

                        	  

                        <?php } ?>

                        <!--



                        <?php if ($rs_role_user->magiamgia == 1) {  ?>

                            <li>

                            	<a href="<?php echo base_url('manage/code') ?>">Mã giảm giá <?php

																							$jquery_count = $this->db->query("select count(*) as count from tbl_discount_codes ");

																							$rs_count = $jquery_count->row(); ?>

                            	(<?php echo $rs_count->count; ?>)</a>

                            </li>

                        <?php } else { ?>

								<li class="no-active">

                            	<a>Mã giảm giá 

                            	(<?php echo $rs_count->count; ?>)</a>

                            </li>

                        <?php } ?>-->

                        </ul>

                    </li>

                    <li class="icon-chain">

                        <a href="#"><i class="fa fa-book"></i> QUẢN LÝ BÀI VIẾT</a>

                        <ul class="chain_first main-menu">

                            <?php

							$jquery_count_news = $this->db->query("select count(*) as count from tbl_dmtin ");

							$rs_count_news = $jquery_count_news->row();

							$jquery_c_dmtin = $this->db->query("select count(*) as count from tbl_category_dmtin ");

							$rs_c_dmtin = $jquery_c_dmtin->row();

							?>



                            <?php if ($rs_role_user->category_news == '1') {  ?>

                            <li>

                                <a href="<?php echo base_url('manage/category_dmtin') ?>"><i class="fa fa-cube"></i>
                                    Danh mục blog (<?php echo $rs_c_dmtin->count; ?>)</a>

                            </li>

                            <?php } else {
								echo ''; ?>



                            <?php } ?>

                            <?php if ($rs_role_user->news == '1') {  ?>

                            <li class="icon-project">

                                <a href="<?php echo base_url('manage/dmtin') ?>"><i class="fa fa-newspaper-o"></i> Bài
                                    viết blog (<?php echo $rs_count_news->count; ?>)</a>

                            </li>

                            <?php } else {
								echo ''; ?>



                            <?php } ?>

                            <?php if ($rs_role_user->page == '1') {  ?>

                            <li class="icon-project">

                                <a href="<?php echo base_url('manage/one_news') ?>"><i class="fa fa-cube"></i> Modun 1
                                    bài viết</a>

                            </li>

                            <?php } else {
								echo ''; ?>



                            <?php } ?>

                        </ul>

                    </li>

                    <li class="icon-chain">

                        <a href="#"><i class="fa fa-book"></i> QUẢN LÝ BANNER</a>

                        <ul class="main-menu">

                            <!-- <?php if ($rs_role_user->partner == '1') {  ?>

				              <li class="icon-photo">

				                <a href="<?php echo base_url('manage/partner') ?>">Đối tác tin cậy</a>

				              </li>

				            	<?php } else { ?>

                    			<li class="no-active">

                    				<a>Đối tác tin cậy</a>

                    			</li>

                    		<?php } ?>-->



                            <?php if ($rs_role_user->slider == '1') {  ?>

                            <li class="icon-photo">

                                <a href="<?php echo base_url('manage/slider') ?>"><i class="fa fa-picture-o"></i> Banner
                                    Slider</a>

                            </li>

                            <?php } else {
								echo ''; ?>



                            <?php } ?>



                            <?php if ($rs_role_user->feel == '1') {  ?>

                            <li class="icon-photo">

                                <a href="<?php echo base_url('manage/feel') ?>"><i class="fa fa-users"></i> Review Khách
                                    hàng</a>

                            </li>

                            <?php } else {
								echo ''; ?>



                            <?php } ?>



                            <?php if ($rs_role_user->avd == '1') {  ?>

                            <li class="icon-photo">

                                <a href="<?php echo base_url('manage/avd') ?>"><i class="fa fa-picture-o"></i> Banner
                                    quảng cáo</a>

                            </li>

                            <?php } else {
								echo ''; ?>



                            <?php } ?>

                            <?php if ($rs_role_user->chinhsach == '1') {  ?>

                            <li class="icon-photo">

                                <a href="<?php echo base_url('manage/chinhsach') ?>"><i class="fa fa-bookmark"></i>
                                    Chính sách</a>

                            </li>

                            <?php } else {
								echo ''; ?>



                            <?php } ?>

                        </ul>

                    </li>



                    <li class="icon-chain">

                        <a href="#"><i class="fa fa-book"></i> QUẢN LÝ THÔNG TIN</a>

                        <ul class="main-menu">



                            <?php if ($rs_role_user->company == '1') {  ?>

                            <li class="icon-config">

                                <a href="<?php echo base_url('theme/option'); ?>"><i class="fa fa-cog"></i> Thông tin
                                    công ty</a>

                            </li>

                            <?php } else {
								echo ''; ?>



                            <?php } ?>



                            <?php if ($rs_role_user->contact == '1') {  ?>

                            <li class="icon-world">

                                <a href="<?php echo base_url('manage/contact') ?>"><i class="fa fa-phone"></i>
                                    Contact</a>

                            </li>

                            <?php } else {
								echo ''; ?>



                            <?php } ?>



                            <?php if ($rs_role_user->dangkymail == '1') {  ?>

                            <li>

                                <a href="<?php echo base_url('manage/regiter_mail') ?>"><i class="fa fa-envelope-o"></i>

                                    Đăng kí nhận tin</a>

                            </li>

                            <?php } else {
								echo ''; ?>



                            <?php } ?>



                            <?php if ($rs_role_user->role_user == '1') {  ?>

                            <li class="icon-text4">

                                <a href="<?php echo base_url('manage/role_user') ?>"><i class="fa fa-user"></i> Quản lý
                                    user</a>

                            </li>

                            <?php } else {
								echo ''; ?>



                            <?php } ?>
                            <?php if ($rs_role_user->company == '1') {  ?>



                            <li class="icon-config">



                                <a href="<?php echo base_url('manage/hotline'); ?>"><i class="fa fa-phone"></i> Thông
                                    tin Hotline</a>



                            </li>



                            <?php } else {
								echo ''; ?>







                            <?php } ?>

                        </ul>

                    </li>



                    <li class="icon-chain">

                        <a href="#"><i class="fa fa-book"></i> QUẢN LÝ ĐẶT TOUR</a>

                        <ul class="main-menu">

                            <?php if ($rs_role_user->order == '1') {  ?>

                            <li class="icon-text2">

                                <a href="<?php echo base_url('manage/order'); ?>"><i class="fa fa-shopping-cart"></i>
                                    Đơn Đặt Tour</a>

                            </li>

                            <?php } else {
								echo ''; ?>



                            <?php } ?>

                            <!--    <li class="icon-text2"><a href="<?php echo base_url('manage/member_order'); ?>"> <i class="fa fa-user"></i> Thành viên</a>

 -->

                        </ul>



                    </li>





                </ul>

                <!-- start: Main Menu -->

                <!--End menu-->

            </div>

            <!-- end: Main Menu -->

            <noscript>

                <div class="alert alert-block span9">

                    <h4 class="alert-heading">Warning!</h4>

                    <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                        enabled to use this site.</p>

                </div>

            </noscript>

            <?php echo $maincontent; ?>

        </div>
        <!--/#content.span10-->

    </div>
    <!--/fluid-row-->



    <div class="modal hide fade" id="myModal">

        <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal">×</button>

            <h3>Settings</h3>

        </div>

        <div class="modal-body">

            <p>Here settings can be configured...</p>

        </div>

        <div class="modal-footer">

            <a href="#" class="btn" data-dismiss="modal">Close</a>

            <a href="#" class="btn btn-primary">Save changes</a>

        </div>

    </div>



    <div class="clearfix"></div>



    <!-- start: JavaScript-->

    <script src="<?php echo base_url() ?>assets/admin/js/jquery-1.9.1.min.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/jquery-migrate-1.0.0.min.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/jquery-ui-1.10.0.custom.min.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/jquery.ui.touch-punch.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/modernizr.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/jquery.cookie.js"></script>

    <script src='<?php echo base_url() ?>assets/admin/js/fullcalendar.min.js'></script>

    <script src='<?php echo base_url() ?>assets/admin/js/jquery.dataTables.min.js'></script>

    <script src="<?php echo base_url() ?>assets/admin/js/jquery.uniform.min.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/jquery.cleditor.min.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/jquery.noty.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/jquery.elfinder.min.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/jquery.raty.min.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/jquery.iphone.toggle.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/jquery.uploadify-3.1.min.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/jquery.gritter.min.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/jquery.imagesloaded.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/jquery.masonry.min.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/jquery.knob.modified.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/jquery.sparkline.min.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/counter.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/retina.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/custom.js"></script>

    <script src="<?php echo base_url() ?>assets/admin/js/jquery-explr-1.4.js"></script>

    <script>
    $(document).ready(function() {

        $("#treexxx").explr();

    });
    </script>



    <!--Google Analytic-->

    <script src="<?php echo base_url() ?>assets/admin/js/jquery-seopreview.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>

    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js">
    </script>

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.css">



    <script type="text/javascript">
    $(document).ready(function()

        {

            $('#dealtime_begin').datetimepicker({

                format: 'yyyy-mm-dd HH:ii'

            });

            $('#dealtime_end').datetimepicker({

                format: 'yyyy-mm-dd HH:ii'

            });

        });
    </script>

    <!-- end: JavaScript-->

</body>

<!--<footer><p></p></footer>-->

</html>