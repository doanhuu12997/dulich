<?php



defined('BASEPATH') or exit('No direct script access allowed');



//Front End Route



$route['default_controller'] = 'web';



$route['translate_uri_dashes'] = FALSE;



$route['sitemap\.xml'] = "Sitemap/index";



//Web Route



//$route['san-pham.html'] = 'web/product';



$route['thuong-hieu.html'] = 'web/thuonghieu';

$route['combo_khachsan.html'] = 'web/combo_khachsan';

$route['tour-dang-hot.html'] = 'web/tourhot';



$route['hang-moi-ve.html'] = 'web/sp_moive';



$route['ban-chay.html'] = 'web/sp_banchay';



$route['ajax/load_price_updown'] = 'web/load_product_price_updown';



$route['ajax/ajax_mucgia'] = 'web/load_mucgia';



$route['ajax/ajax_filter_star'] = 'web/load_ajax_filter_star';



$route['ajax/ajax_product_update_status'] = 'product/ajax_product_update_status';



$route['ajax/ajax_product_update_home'] = 'product/ajax_product_update_home';



$route['ajax/ajax_product_update_new'] = 'product/ajax_product_update_new';



$route['ajax/ajax_product_update_noibat'] = 'product/ajax_product_update_noibat';



$route['ajax/ajax_product_update_seller'] = 'product/ajax_product_update_seller';



$route['ajax/ajax_product_update_ttrang'] = 'product/ajax_product_update_ttrang';



$route['ajax/ajax_product_update_hot'] = 'product/ajax_product_update_hot';



$route['ajax/ajax_delete_product_id'] = 'product/ajax_delete_product_id';



$route['ajax/ajax_delete_product_all'] = 'product/ajax_delete_product_all';



$route['san-pham.html/(:num)'] = 'web/product/$1';



$route['ajax/load_catelogyc2_product'] = 'web/load_ajax_product_c2';



$route['ajax/ajax_phiship'] = 'web/load_ajax_phiship';



$route['ajax/ajax_phiship_load'] = 'web/load_data_ship';



$route['ajax/ajax_search'] = 'web/load_ajax_product_search';



$route['ajax/ajax_check_hd'] = 'web/load_ajax_check_hd';



$route['ajax/ajax_discount_codes'] = 'web/ajax_discount_codes';



$route['ajax/ajax_display_all'] = 'web/load_display_all_category_post';



$route['ajax/ajax_product_view'] = 'web/load_product_view';



$route['ajax/get_loadmore_product'] = 'web/get_loadmore_product';



$route['lien-he.html'] = 'web/contact';



$route['save_contact.html'] = 'web/contact_save';



$route['dang-ky-nhan-mail.html'] = 'web/save_regiter_mail';



$route['form_baogia/save'] = 'web/save_baogia';



$route['comment.html'] = 'web/save_comment';



$route['commentproduct.html'] = 'web/save_commentproduct';



$route['danky-tu-van-tour.html'] = 'web/save_dk_tv_tour';



$route['comment_news_rate/save'] = 'web/save_comment_news_rate';



$route['commentnews.html'] = 'web/save_commentnews';



$route['request/save'] = 'web/save_request';



$route['update/member'] = 'web/update_user';



$route['trang-chu.html'] = 'web/index';



$route['gio-hang.html'] = 'web/cart';



$route['save/cart'] = 'web/save_cart';



// $route['update/cart'] = 'web/update_cart';



$route['remove/cart'] = 'web/remove_cart';



$route['user_form'] = 'web/user_form';



$route['update_cart_ajax'] = 'web/update_cart';







$route['update_cart_price_child'] = 'web/update_cart_price_child';



$route['update_cart_price_baby'] = 'web/update_cart_price_baby';











//ROLE  Route List



$route['add/role'] = 'role/add_role';



$route['manage/role'] = 'role/manage_role';



$route['save/role'] = 'role/save_role';



$route['delete/role/(:num)'] = 'role/delete_role/$1';



$route['edit/role/(:num)'] = 'role/edit_role/$1';



$route['update/role/(:num)'] = 'role/update_role/$1';







//ROLE user Route List







//301  Route List



$route['add/link301'] = 'link301/add_link301';



$route['manage/link301'] = 'link301/manage_link301';



$route['save/link301'] = 'link301/save_link301';



$route['delete/link301/(:num)'] = 'link301/delete_link301/$1';



$route['edit/link301/(:num)'] = 'link301/edit_link301/$1';



$route['update/link301/(:num)'] = 'link301/update_link301/$1';



$route['published/link301/(:num)'] = 'link301/published_link301/$1';



$route['unpublished/link301/(:num)'] = 'link301/unpublished_link301/$1';







$route['add/role_user'] = 'role_user/add_role_user';



$route['manage/role_user'] = 'role_user/manage_role_user';



$route['save/role_user'] = 'role_user/save_role_user';



$route['delete/role_user/(:num)'] = 'role_user/delete_role_user/$1';



$route['edit/role_user/(:num)'] = 'role_user/edit_role_user/$1';



$route['update/role_user/(:num)'] = 'role_user/update_role_user/$1';



$route['tim-kiem.html'] = 'web/search';



$route['filter'] = 'web/searchfilter';



$route['dang-ky.html'] = 'web/customer_register';



$route['dang-nhap.html'] = 'web/customer_login';



$route['reset-password.html'] = 'web/forget_pass';



$route['customer/resetpass'] = 'web/customer_resetpass';



$route['thoat.html'] = 'web/logout';



$route['customer/logincheck'] = 'web/customer_logincheck';



$route['customer/save'] = 'web/customer_save';



$route['register/success'] = 'web/register_success';



$route['thong-tin-tai-khoan.html'] = 'web/info_member';



$route['lich-su-mua-hang.html'] = 'web/history';



$route['save-cart.html'] = 'web/save_shipping_address';



$route['checkout'] = 'web/checkout';



$route['payment'] = 'web/payment';



$route['save/order'] = 'web/save_order';



$route['check_email'] = 'web/check_email';







//Admin Panel Route



$route['dashboard'] = 'admin/index';



$route['manage/order'] = 'manageorder/manage_order';



$route['delete/order/(:num)'] = 'manageorder/delete_order/$1';



$route['update/order/(:num)'] = 'manageorder/update_status_order/$1';



$route['order/details/(:num)'] = 'manageorder/order_details/$1';



$route['order/details_member/(:num)'] = 'web/history_detail/$1';







// member order



$route['manage/member_order'] = 'manage_member_order/manage_member_order';



$route['edit/member/(:num)'] = 'manage_member_order/edit_member/$1';



$route['update/member/(:num)'] = 'manage_member_order/update_member/$1';



$route['delete/member/(:num)'] = 'manage_member_order/delete_member/$1';











//Category  Route List



$route['add/category'] = 'category/add_category';



$route['manage/category'] = 'category/manage_category';



$route['save/category'] = 'category/save_category';



$route['ajax/check_color'] = 'category/checkbox_ajax';



$route['delete/category/(:num)'] = 'category/delete_category/$1';



$route['edit/category/(:num)'] = 'category/edit_category/$1';



$route['update/category/(:num)'] = 'category/update_category/$1';



$route['published/category/(:num)'] = 'category/published_category/$1';



$route['unpublished/category/(:num)'] = 'category/unpublished_category/$1';



$route['published_header/category/(:num)'] = 'category/published_header_category/$1';



$route['unpublished_header/category/(:num)'] = 'category/unpublished_header_category/$1';



$route['published_footer/category/(:num)'] = 'category/published_footer_category/$1';



$route['unpublished_footer/category/(:num)'] = 'category/unpublished_footer_category/$1';



$route['published_home_category/category/(:num)'] = 'category/published_home_category/$1';



$route['unpublished_home_category/category/(:num)'] = 'category/unpublished_home_category/$1';







//city  Route List







$route['add/city'] = 'city/add_city';



$route['manage/city'] = 'city/manage_city';



$route['save/city'] = 'city/save_city';



$route['delete/city/(:num)'] = 'city/delete_city/$1';



$route['edit/city/(:num)'] = 'city/edit_city/$1';



$route['update/city/(:num)'] = 'city/update_city/$1';



$route['published/city/(:num)'] = 'city/published_city/$1';



$route['unpublished/city/(:num)'] = 'city/unpublished_city/$1';











//size  Route List



$route['add/size'] = 'size/add_size';



$route['manage/size'] = 'size/manage_size';



$route['save/size'] = 'size/save_size';



$route['delete/size/(:num)'] = 'size/delete_size/$1';



$route['edit/size/(:num)'] = 'size/edit_size/$1';



$route['update/size/(:num)'] = 'size/update_size/$1';



$route['published/size/(:num)'] = 'size/published_size/$1';



$route['unpublished/size/(:num)'] = 'size/unpublished_size/$1';







//code  Route List



$route['add/code'] = 'code/add_code';



$route['manage/code'] = 'code/manage_code';



$route['save/code'] = 'code/save_code';



$route['delete/code/(:num)'] = 'code/delete_code/$1';



$route['edit/code/(:num)'] = 'code/edit_code/$1';



$route['update/code/(:num)'] = 'code/update_code/$1';



$route['published/code/(:num)'] = 'code/published_code/$1';



$route['unpublished/code/(:num)'] = 'code/unpublished_code/$1';







//color  Route List



$route['add/color'] = 'color/add_color';



$route['manage/color'] = 'color/manage_color';



$route['save/color'] = 'color/save_color';



$route['delete/color/(:num)'] = 'color/delete_color/$1';



$route['edit/color/(:num)'] = 'color/edit_color/$1';



$route['update/color/(:num)'] = 'color/update_color/$1';



$route['published/color/(:num)'] = 'color/published_color/$1';



$route['unpublished/color/(:num)'] = 'color/unpublished_color/$1';







//Category_Danh mục tin  Route List



$route['add/category_dmtin'] = 'category_dmtin/add_category_dmtin';



$route['manage/category_dmtin'] = 'category_dmtin/manage_category_dmtin';



$route['save/category_dmtin'] = 'category_dmtin/save_category_dmtin';



$route['delete/category_dmtin/(:num)'] = 'category_dmtin/delete_category_dmtin/$1';



$route['edit/category_dmtin/(:num)'] = 'category_dmtin/edit_category_dmtin/$1';



$route['update/category_dmtin/(:num)'] = 'category_dmtin/update_category_dmtin/$1';



$route['published/category_dmtin/(:num)'] = 'category_dmtin/published_category_dmtin/$1';



$route['unpublished/category_dmtin/(:num)'] = 'category_dmtin/unpublished_category_dmtin/$1';



$route['ajax/ajax_news_catelogy'] = 'dmtin/load_ajax_news';



$route['published_header/category_dmtin_header/(:num)'] = 'category_dmtin/published_category_header/$1';



$route['unpublished_header/category_dmtin_header/(:num)'] = 'category_dmtin/unpublished_category_header/$1';



// Hotline Routes

$route['add/hotline'] = 'hotline/add_hotline';

$route['manage/hotline'] = 'hotline/manage_hotline';

$route['save/hotline'] = 'hotline/save_hotline';

$route['delete/hotline/(:num)'] = 'hotline/delete_hotline/$1';

$route['edit/hotline/(:num)'] = 'hotline/edit_hotline/$1';





$route['update/hotline/(:num)'] = 'hotline/update_category_dmtin/$1';



$route['published/hotline/(:num)'] = 'hotline/published_hotline/$1';







$route['unpublished/hotline/(:num)'] = 'hotline/unpublished_hotline/$1';







//Brand  Route List







$route['add/brand'] = 'brand/add_brand';



$route['manage/brand'] = 'brand/manage_brand';



$route['save/brand'] = 'brand/save_brand';



$route['delete/brand/(:num)'] = 'brand/delete_brand/$1';



$route['edit/brand/(:num)'] = 'brand/edit_brand/$1';



$route['update/brand/(:num)'] = 'brand/update_brand/$1';



$route['published/brand/(:num)'] = 'brand/published_brand/$1';



$route['unpublished/brand/(:num)'] = 'brand/unpublished_brand/$1';











//origin  Route List



$route['add/origin'] = 'origin/add_origin';



$route['manage/origin'] = 'origin/manage_origin';



$route['save/origin'] = 'origin/save_origin';



$route['delete/origin/(:num)'] = 'origin/delete_origin/$1';



$route['edit/origin/(:num)'] = 'origin/edit_origin/$1';



$route['update/origin/(:num)'] = 'origin/update_origin/$1';



$route['published/origin/(:num)'] = 'origin/published_origin/$1';



$route['unpublished/origin/(:num)'] = 'origin/unpublished_origin/$1';







//xuatxu  Route List



$route['add/xuatxu'] = 'xuatxu/add_xuatxu';



$route['manage/xuatxu'] = 'xuatxu/manage_xuatxu';



$route['save/xuatxu'] = 'xuatxu/save_xuatxu';



$route['delete/xuatxu/(:num)'] = 'xuatxu/delete_xuatxu/$1';



$route['edit/xuatxu/(:num)'] = 'xuatxu/edit_xuatxu/$1';



$route['update/xuatxu/(:num)'] = 'xuatxu/update_xuatxu/$1';



$route['published/xuatxu/(:num)'] = 'xuatxu/published_xuatxu/$1';



$route['unpublished/xuatxu/(:num)'] = 'xuatxu/unpublished_xuatxu/$1';







//thanhphan  Route List



$route['add/thanhphan'] = 'thanhphan/add_thanhphan';



$route['manage/thanhphan'] = 'thanhphan/manage_thanhphan';



$route['save/thanhphan'] = 'thanhphan/save_thanhphan';



$route['delete/thanhphan/(:num)'] = 'thanhphan/delete_thanhphan/$1';



$route['edit/thanhphan/(:num)'] = 'thanhphan/edit_thanhphan/$1';



$route['update/thanhphan/(:num)'] = 'thanhphan/update_thanhphan/$1';



$route['published/thanhphan/(:num)'] = 'thanhphan/published_thanhphan/$1';



$route['unpublished/thanhphan/(:num)'] = 'thanhphan/unpublished_thanhphan/$1';







//congdung  Route List



$route['add/congdung'] = 'congdung/add_congdung';



$route['manage/congdung'] = 'congdung/manage_congdung';



$route['save/congdung'] = 'congdung/save_congdung';



$route['delete/congdung/(:num)'] = 'congdung/delete_congdung/$1';



$route['edit/congdung/(:num)'] = 'congdung/edit_congdung/$1';



$route['update/congdung/(:num)'] = 'congdung/update_congdung/$1';



$route['published/congdung/(:num)'] = 'congdung/published_congdung/$1';



$route['unpublished/congdung/(:num)'] = 'congdung/unpublished_congdung/$1';



//Post Route List



$route['add/product'] = 'product/add_product';



$route['manage/product'] = 'product/manage_product';



$route['manage/product/(:num)'] = 'product/manage_product/$1';



$route['ajax/delete_miufile_product'] = 'product/deleteimage';



$route['ajax/delete_miufile_depart'] = 'product/delete_miufile_depart';







$route['ajax/delete_miufile_ct_tour'] = 'product/delete_miufile_ct_tour';







$route['ajax/delete_miufile_news'] = 'dmtin/deleteimage';



$route['ajax/ajax_catelogy'] = 'product/load_ajax_product';



$route['ajax/ajax_function'] = 'product/load_ajax_product_function';



$route['ajax/ajax_catelogy_c2'] = 'product/load_ajax_product_c2';



$route['ajax/ajax_deal'] = 'product/load_ajax_product_deal';



$route['ajax/import_excel'] = 'product/import';



$route['export_excel'] = 'product/export_excel';



$route['ajax/ajax_catelogy/(:num)'] = 'product/load_ajax_product/$1';



$route['ajax/ajax_poduct_search'] = 'product/load_ajax_product_search';



$route['ajax/ajax_delete_catebanner'] = 'category/delete_catebanner';



$route['ajax/ajax_delete_catebanner_lv2'] = 'category_level2/delete_catebanner';



$route['ajax/ajax_delete_catelv1'] = 'category/delete_catelv1';



$route['ajax/ajax_delete_product1'] = 'product/delete_img_product1';



$route['ajax/ajax_delete_product2'] = 'product/delete_img_product2';



$route['ajax/ajax_delete_catenew'] = 'category_dmtin/delete_cateimgnew';



$route['ajax/ajax_deleteimgnews'] = 'dmtin/delete_imgnews';



$route['ajax/ajax_delete_imgslider'] = 'slider/delete_imgslider';



$route['ajax/ajax_delete_video'] = 'slider/delete_video';



$route['ajax/ajax_delete_imgfeel'] = 'feel/delete_imgfeel';



$route['ajax/ajax_delete_imgpartner'] = 'partner/delete_imgpartner';



$route['ajax/ajax_deletethumb1'] = 'images/delete_thumb1';



$route['ajax/ajax_deletethumb2'] = 'images/delete_thumb2';



$route['ajax/ajax_deletethumb3'] = 'images/delete_thumb3';



$route['ajax/ajax_deletethumb4'] = 'images/delete_thumb4';



$route['ajax/ajax_deletethumb5'] = 'images/delete_thumb5';



$route['ajax/ajax_deletethumb6'] = 'images/delete_thumb6';



$route['ajax/ajax_delete_gallary1'] = 'product/delete_gallary1';



$route['ajax/ajax_delete_gallary2'] = 'product/delete_gallary2';



$route['ajax/ajax_delete_gallary3'] = 'product/delete_gallary3';



$route['ajax/ajax_delete_gallary4'] = 'product/delete_gallary4';



$route['ajax/ajax_delete_gallary5'] = 'product/delete_gallary5';



$route['ajax/ajax_delete_gallary6'] = 'product/delete_gallary6';



$route['save/product'] = 'product/save_product';



$route['delete/product/(:num)'] = 'product/delete_product/$1';



$route['edit/product/(:num)'] = 'product/edit_product/$1';



$route['update/product/(:num)'] = 'product/update_product/$1';



$route['published/product/(:num)'] = 'product/published_product/$1';



$route['unpublished/product/(:num)'] = 'product/unpublished_product/$1';



$route['published_home/product/(:num)'] = 'product/published_home/$1';



$route['unpublished_home/product/(:num)'] = 'product/unpublished_home/$1';



$route['published_view/product/(:num)'] = 'product/published_view/$1';



$route['unpublished_view/product/(:num)'] = 'product/unpublished_view/$1';



$route['published_new/product/(:num)'] = 'product/published_new/$1';



$route['unpublished_new/product/(:num)'] = 'product/unpublished_new/$1';



$route['published_selling/product/(:num)'] = 'product/published_selling/$1';



$route['unpublished_selling/product/(:num)'] = 'product/unpublished_selling/$1';



$route['published_sale/product/(:num)'] = 'product/published_sale/$1';



$route['unpublished_sale/product/(:num)'] = 'product/unpublished_sale/$1';







//Post DANH MỤC TIN Route List



$route['add/dmtin'] = 'dmtin/add_dmtin';



$route['manage/dmtin'] = 'dmtin/manage_dmtin';



$route['save/dmtin'] = 'dmtin/save_dmtin';



$route['delete/dmtin/(:num)'] = 'dmtin/delete_dmtin/$1';



$route['edit/dmtin/(:num)'] = 'dmtin/edit_dmtin/$1';



$route['update/dmtin/(:num)'] = 'dmtin/update_dmtin/$1';



$route['published/dmtin/(:num)'] = 'dmtin/published_dmtin/$1';



$route['unpublished/dmtin/(:num)'] = 'dmtin/unpublished_dmtin/$1';



$route['published_home/dmtin/(:num)'] = 'dmtin/published_dmtin_home/$1';



$route['unpublished_home/dmtin/(:num)'] = 'dmtin/unpublished_dmtin_home/$1';



$route['published_noibat/dmtin/(:num)'] = 'dmtin/published_dmtin_noibat/$1';



$route['unpublished_noibat/dmtin/(:num)'] = 'dmtin/unpublished_dmtin_noibat/$1';



$route['published_quantam/dmtin/(:num)'] = 'dmtin/published_dmtin_quantam/$1';



$route['unpublished_quantam/dmtin/(:num)'] = 'dmtin/unpublished_dmtin_quantam/$1';







//Admin login



$route['adminkh'] = 'adminlogin';



$route['admin_login_check'] = 'adminlogin/admin_login_check';



$route['logout'] = 'admin/logout';







//Slider  Route List



$route['add/slider'] = 'slider/add_slider';



$route['manage/slider'] = 'slider/manage_slider';



$route['save/slider'] = 'slider/save_slider';



$route['delete/slider/(:num)'] = 'slider/delete_slider/$1';



$route['edit/slider/(:num)'] = 'slider/edit_slider/$1';



$route['update/slider/(:num)'] = 'slider/update_slider/$1';



$route['published/slider/(:num)'] = 'slider/published_slider/$1';



$route['unpublished/slider/(:num)'] = 'slider/unpublished_slider/$1';







//Feel  Route List











$route['add/feel'] = 'feel/add_feel';



$route['manage/feel'] = 'feel/manage_feel';



$route['save/feel'] = 'feel/save_feel';



$route['delete/feel/(:num)'] = 'feel/delete_feel/$1';



$route['edit/feel/(:num)'] = 'feel/edit_feel/$1';



$route['update/feel/(:num)'] = 'feel/update_feel/$1';



$route['published/feel/(:num)'] = 'feel/published_feel/$1';



$route['unpublished/feel/(:num)'] = 'feel/unpublished_feel/$1';











//chinhsach  Route List



$route['add/chinhsach'] = 'chinhsach/add_chinhsach';



$route['manage/chinhsach'] = 'chinhsach/manage_chinhsach';



$route['save/chinhsach'] = 'chinhsach/save_chinhsach';



$route['delete/chinhsach/(:num)'] = 'chinhsach/delete_chinhsach/$1';



$route['edit/chinhsach/(:num)'] = 'chinhsach/edit_chinhsach/$1';



$route['update/chinhsach/(:num)'] = 'chinhsach/update_chinhsach/$1';



$route['published/chinhsach/(:num)'] = 'chinhsach/published_chinhsach$1';



$route['unpublished/chinhsach/(:num)'] = 'chinhsach/unpublished_chinhsach/$1';







//avd  Route List



$route['add/avd'] = 'avd/add_avd';



$route['manage/avd'] = 'avd/manage_avd';



$route['save/avd'] = 'avd/save_avd';



$route['delete/avd/(:num)'] = 'avd/delete_avd/$1';



$route['edit/avd/(:num)'] = 'avd/edit_avd/$1';



$route['update/avd/(:num)'] = 'avd/update_avd/$1';



$route['published/avd/(:num)'] = 'avd/published_avd/$1';



$route['unpublished/avd/(:num)'] = 'avd/unpublished_avd/$1';











//avd_home  Route List



$route['add/avd_home'] = 'avd_home/add_avd_home';



$route['manage/avd_home'] = 'avd_home/manage_avd_home';



$route['save/avd_home'] = 'avd_home/save_avd_home';



$route['delete/avd_home/(:num)'] = 'avd_home/delete_avd_home/$1';



$route['edit/avd_home/(:num)'] = 'avd_home/edit_avd_home/$1';



$route['update/avd_home/(:num)'] = 'avd_home/update_avd_home/$1';



$route['published/avd_home/(:num)'] = 'avd_home/published_avd_home/$1';



$route['unpublished/avd_home/(:num)'] = 'avd_home/unpublished_avd_home/$1';



//Feel  Route List







$route['add/images/(:num)'] = 'images/add_images/$1';



$route['manage/images'] = 'images/manage_images';



$route['save/images'] = 'images/save_images';



$route['delete/images/(:num)'] = 'images/delete_images/$1';



$route['edit/images/(:num)'] = 'images/edit_images/$1';



$route['update/images/(:num)'] = 'images/update_images/$1';



$route['published/images/(:num)'] = 'images/published_images/$1';



$route['unpublished/images/(:num)'] = 'images/unpublished_images/$1';



//Feel  Route List



$route['add/one_news'] = 'one_news/add_one_news';



$route['manage/one_news'] = 'one_news/manage_one_news';



$route['save/one_news'] = 'one_news/save_one_news';



$route['delete/one_news/(:num)'] = 'one_news/delete_one_news/$1';



$route['edit/one_news/(:num)'] = 'one_news/edit_one_news/$1';



$route['update/one_news/(:num)'] = 'one_news/update_one_news/$1';



$route['published/one_news/(:num)'] = 'one_news/published_one_news/$1';



$route['unpublished/one_news/(:num)'] = 'one_news/unpublished_one_news/$1';



$route['published_header/one_news/(:num)'] = 'one_news/published_header/$1';



$route['unpublished_header/one_news/(:num)'] = 'one_news/unpublished_header/$1';







//CONTACT  Route List



$route['manage/contact'] = 'contact/manage_contact';



$route['delete/contact/(:num)'] = 'contact/delete_contact/$1';



$route['published/contact/(:num)'] = 'contact/published_contact/$1';



$route['unpublished/contact/(:num)'] = 'contact/unpublished_contact/$1';



//bao gia  Route List







$route['manage/baogia'] = 'baogia/manage_baogia';



$route['delete/baogia/(:num)'] = 'baogia/delete_baogia/$1';



$route['published/baogia/(:num)'] = 'baogia/published_baogia/$1';



$route['unpublished/baogia/(:num)'] = 'baogia/unpublished_baogia/$1';







//regiter  Route List



$route['manage/regiter_mail'] = 'regiter_mail/manage_regiter_mail';



$route['delete/regiter_mail/(:num)'] = 'regiter_mail/delete_regiter_mail/$1';



$route['published/regiter_mail/(:num)'] = 'regiter_mail/published_regiter_mail/$1';



$route['unpublished/regiter_mail/(:num)'] = 'regiter_mail/unpublished_regiter_mail/$1';







//quên mật khẩu  Route List



$route['manage/regiter_pass'] = 'regiter_pass/manage_regiter_pass';



$route['delete/regiter_pass/(:num)'] = 'regiter_pass/delete_regiter_pass/$1';



$route['published/regiter_pass/(:num)'] = 'regiter_pass/published_regiter_pass/$1';



$route['unpublished/regiter_pass/(:num)'] = 'regiter_pass/unpublished_regiter_pass/$1';



//comment Route List



$route['manage/comment'] = 'comment/manage_comment';



$route['delete/comment/(:num)'] = 'comment/delete_comment/$1';



$route['edit/comment/(:num)'] = 'comment/edit_comment/$1';



$route['update/comment/(:num)'] = 'comment/update_comment/$1';



$route['published/comment/(:num)'] = 'comment/published_comment/$1';



$route['unpublished/comment/(:num)'] = 'comment/unpublished_comment/$1';



$route['published_answer/comment/(:num)'] = 'comment/comment_show_answer/$1';



$route['unpublished_answer/comment/(:num)'] = 'comment/un_comment_show_answer/$1';







//comment news rate Route List



$route['manage/comment_news_rate'] = 'comment_news_rate/manage_comment_news_rate';



$route['delete/comment_news_rate/(:num)'] = 'comment_news_rate/delete_comment_news_rate/$1';



$route['edit/comment_news_rate/(:num)'] = 'comment_news_rate/edit_comment_news_rate/$1';



$route['update/comment_news_rate/(:num)'] = 'comment_news_rate/update_comment_news_rate/$1';



$route['published/comment_news_rate/(:num)'] = 'comment_news_rate/published_comment_news_rate/$1';



$route['unpublished/comment_news_rate/(:num)'] = 'comment_news_rate/unpublished_comment_news_rate/$1';



$route['published_answer/comment_news_rate/(:num)'] = 'comment_news_rate/comment_news_rate_show_answer/$1';















$route['unpublished_answer/comment_news_rate/(:num)'] = 'comment_news_rate/un_comment_news_rate_show_answer/$1';







//request Route List







$route['manage/request'] = 'request/manage_request';



$route['delete/request/(:num)'] = 'request/delete_request/$1';



$route['published/request/(:num)'] = 'request/published_request/$1';



$route['unpublished/request/(:num)'] = 'request/unpublished_request/$1';







//comment news Route List



$route['manage/commentnews'] = 'commentnews/manage_comment';



$route['delete/commentnews/(:num)'] = 'commentnews/delete_comment/$1';



$route['edit/commentnews/(:num)'] = 'commentnews/edit_comment/$1';



$route['update/commentnews/(:num)'] = 'commentnews/update_comment/$1';



$route['published/commentnews/(:num)'] = 'commentnews/published_comment/$1';



$route['unpublished/commentnews/(:num)'] = 'commentnews/unpublished_comment/$1';



$route['published_answer/commentnews/(:num)'] = 'commentnews/comment_show_answer/$1';



$route['unpublished_answer/commentnews/(:num)'] = 'commentnews/un_comment_show_answer/$1';











//comment product Route List



$route['manage/commentproduct'] = 'commentproduct/manage_comment';



$route['delete/commentproduct/(:num)'] = 'commentproduct/delete_comment/$1';



$route['edit/commentproduct/(:num)'] = 'commentproduct/edit_comment/$1';



$route['update/commentproduct/(:num)'] = 'commentproduct/update_comment/$1';



$route['published/commentproduct/(:num)'] = 'commentproduct/published_comment/$1';



$route['unpublished/commentproduct/(:num)'] = 'commentproduct/unpublished_comment/$1';



$route['published_answer/commentproduct/(:num)'] = 'commentproduct/comment_show_answer/$1';



$route['unpublished_answer/commentproduct/(:num)'] = 'commentproduct/un_comment_show_answer/$1';















//Mua nhanh Route List



$route['manage/muanhanh'] = 'muanhanh/manage_muanhanh';



$route['delete/muanhanh/(:num)'] = 'muanhanh/delete_muanhanh/$1';



$route['edit/muanhanh/(:num)'] = 'muanhanh/edit_muanhanh/$1';



$route['update/muanhanh/(:num)'] = 'muanhanh/update_muanhanh/$1';



$route['published/muanhanh/(:num)'] = 'muanhanh/published_muanhanh/$1';



$route['unpublished/muanhanh/(:num)'] = 'muanhanh/unpublished_muanhanh/$1';











//Partner  Route List



$route['add/partner'] = 'partner/add_partner';



$route['manage/partner'] = 'partner/manage_partner';



$route['save/partner'] = 'partner/save_partner';



$route['delete/partner/(:num)'] = 'partner/delete_partner/$1';



$route['edit/partner/(:num)'] = 'partner/edit_partner/$1';



$route['update/partner/(:num)'] = 'partner/update_partner/$1';



$route['published/partner/(:num)'] = 'partner/published_partner/$1';



$route['unpublished/partner/(:num)'] = 'partner/unpublished_partner/$1';



//Theme Option  Route List



$route['theme/option'] = 'themeoption';







$route['save/option'] = 'themeoption/save_option';







$con = mysqli_connect("localhost", "amaratours.vn", "yA7CKrBe8yytkPam", "amaratours.vn");



// Check connection



if (mysqli_connect_errno()) {



  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}







$sql = "select * from tbl_category";



$result = mysqli_query($con, $sql);







$sql = "select * from tbl_brand";



$result_brand = mysqli_query($con, $sql);



$sql3 = "select * from tbl_category_level3";



$result_c3 = mysqli_query($con, $sql3);







$sql_dmtin = "select * from tbl_category_dmtin";



$result_dmtin = mysqli_query($con, $sql_dmtin);







$sql_product = "select * from tbl_product ";



$result_product = mysqli_query($con, $sql_product);



$sql_dmtin_detail = "select * from tbl_dmtin ";







$result_dmtin_detail = mysqli_query($con, $sql_dmtin_detail);



$sql_one_news_detail = "select * from tbl_one_news ";



$result_one_news_detail = mysqli_query($con, $sql_one_news_detail);



$sql_301 = "select * from tbl_link301";



$result_301 = mysqli_query($con, $sql_301);







mysqli_close($con);



$get_url = $this->uri->segment(1);



$get_amp = $this->uri->segment(2);



$get_string_amp = $get_url . '/' . $get_amp;



$i = 0;



while ($row = mysqli_fetch_assoc($result)) {



  $AddToEnd = ".html";



  $get_string = $row['slug'];



  $finalString = implode(array($get_string, $AddToEnd));



  if ($get_url == $finalString) {



    $route['(:any)'] = 'web/category_post/$1';



    $route['(:any)/(:num)'] = 'web/category_post/$1';



    return true;
  }
}







$i++;







$j = 0;







while ($row = mysqli_fetch_assoc($result_dmtin)) {



  $AddToEnd = ".html";



  $get_string = $row['slug'];



  $finalString = implode(array($get_string, $AddToEnd));



  if ($get_url == $finalString) {



    $route['(:any)'] = 'web/catalog_new_post/$1';



    $route['(:any)/(:num)'] = 'web/catalog_new_post/$1';



    return true;
  } else {
  }
}







$j++;







$x = 0;



while ($row = mysqli_fetch_assoc($result_brand)) {



  $AddToEnd = ".html";



  $get_string = $row['brand_slug'];



  $finalString = implode(array($get_string, $AddToEnd));



  if ($get_url == $finalString) {







    $route['(:any)'] = 'web/country_post/$1';



    $route['(:any)/(:num)'] = 'web/country_post/$1';



    return true;
  } else {
  }
}















$x++;







$k = 0;



while ($row = mysqli_fetch_assoc($result_product)) {



  $get_string = $row['product_slug'] . '.html';



  if ($get_url == $get_string) {



    $route['(:any)'] = 'web/single/$1';



    return true;
  } else {
  }
}







$k++;



$t = 0;



while ($row = mysqli_fetch_assoc($result_dmtin_detail)) {



  $finalString = $row['dmtin_slug'] . '.html';



  $finalString_amp = 'amp/' . $row['dmtin_slug'] . '.html';



  if ($get_url == $finalString) {



    $route['(:any)'] = 'web/single_new/$1';



    return true;
  } else if ($get_string_amp == $finalString_amp) {



    $route['(:any)/(:any)'] = 'web/single_new_amp/$2/$1';



    return true;
  } else {
  }
}







$t++;



$b = 0;



while ($row = mysqli_fetch_assoc($result_one_news_detail)) {



  $AddToEnd = ".html";



  $get_string = $row['one_news_slug'];



  $finalString = implode(array($get_string, $AddToEnd));



  if ($get_url == $finalString) {



    $route['(:any)'] = 'web/single_one_news/$1';



    return true;
  } else {
  }
}







$b++;







$fullurl = $_SERVER['REQUEST_URI'];



$trim_full = trim($fullurl, "/");



$w = 0;



while ($row = mysqli_fetch_assoc($result_301)) {



  $get_string = $row['link301_nguon'];



  if ($trim_full == $get_string) {



    $route['(.*)'] = "web/link301/$1";







    return true;
  } else {
  }
}



$w++;
