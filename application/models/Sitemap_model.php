<?php



class Sitemap_model extends CI_Model {





     public function getall_category_info() {

      $this->db->select('*');
      $this->db->from('tbl_category');
      $this->db->where('parent_id', 0);
      $this->db->where('cate_status',1);
      $this->db->order_by('id','asc');

      $parent = $this->db->get();
      $categories = $parent->result();
      $i=0;
      foreach($categories as $main_cat){
          $categories[$i]->sub = $this->sub_category($main_cat->id);
          $i++;
      }
      return $categories;
    }

       public function sub_category($cat_id) {
        $this->db->select('*');
        $this->db->from('tbl_category');
        $this->db->where('cate_status',1);
        $this->db->order_by('tbl_category.category_stt','asc');
        $this->db->or_where('parent_id',$cat_id);

        $info = $this->db->get();
        return $info->result();

    }

   
     public function getall_category_news_info() {

        $this->db->select('*');

        $this->db->from('tbl_category_dmtin');

        $this->db->where('publication_status',1);

        $info = $this->db->get();

        return $info->result();

    }



      public function getCategories_news($cat_id) {



          $this->db->select('*');

        $this->db->from('tbl_category_dmtin');

        $this->db->where('publication_status',1);

        $this->db->order_by('tbl_category_dmtin.id','asc');

        $this->db->or_where('parent_id',$cat_id);

        $info = $this->db->get();

        return $info->result();

    }

    



    



}

