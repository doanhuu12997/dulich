<?php







class Dmtin_model extends CI_Model{



    

    public function edit_data_image($id){

       $this->db->select('*');

        $this->db->from('tbl_dmtin');

        $this->db->join('tbl_muti_news','tbl_muti_news.news_id=tbl_dmtin.dmtin_id');

        $this->db->where('tbl_dmtin.dmtin_id', $id);

        $info = $this->db->get();

        return $info->result_array();

    }



   public function save_dmtin_info($data,$filename){

    $this->db->insert('tbl_dmtin', $data);

    $insert_id = $this->db->insert_id();

      if($filename!='' ){

      $filename1 = explode(',',$filename);

      foreach($filename1 as $file){

      $file_data = array(

      'image' => $file,

      'news_id' => $insert_id

      );

      $this->db->insert('tbl_muti_news', $file_data);

      }

      }

    }



    

    public function all_users(){

        $this->db->select('*');

        $this->db->from('tbl_user');

        $info = $this->db->get();

        return $info->result();

    }



    public function get_all_dmtin(){



        $this->db->select('*,tbl_dmtin.publication_status as pstatus');

        $this->db->from('tbl_dmtin');

        $this->db->join('tbl_category_dmtin','tbl_category_dmtin.id=tbl_dmtin.dmtin_category');



        $this->db->order_by('tbl_dmtin.dmtin_id', 'desc');

        if($this->session->userdata('user_id') != 1)$this->db->where('tbl_dmtin.dmtin_nv_ql', $this->session->userdata('user_id'));

        $info = $this->db->get();

        return $info->result();





    }



    



        public function get_category_dmtin(){



        $this->db->select('*');



        $this->db->from('tbl_category_dmtin');



        $this->db->order_by('tbl_category_dmtin.id', 'desc');



        $this->db->where('tbl_category_dmtin.parent_id',0);



        $info = $this->db->get();



        return $info->result();



    }



    public function edit_dmtin_info($id){



        $this->db->select('*,tbl_dmtin.publication_status as pstatus');



        $this->db->from('tbl_dmtin');



        $this->db->join('tbl_category_dmtin','tbl_category_dmtin.id=tbl_dmtin.dmtin_category','left');



        $this->db->where('tbl_dmtin.dmtin_id', $id);



        $info = $this->db->get();



        return $info->row();



    }



      public function ajax_news_by_cat($category_c1){
        $this->db->select('*,tbl_dmtin.publication_status as pstatus');
        $this->db->from('tbl_dmtin');
        $this->db->join('tbl_category_dmtin','tbl_category_dmtin.id=tbl_dmtin.dmtin_category','left');
        $this->db->where('(tbl_dmtin.dmtin_category = '.$category_c1.' or tbl_category_dmtin.parent_id = '.$category_c1.')');
        $this->db->order_by('tbl_dmtin.dmtin_id', 'desc');
        $info = $this->db->get();
        return $info->result();
    }



    



    public function delete_dmtin_info($id){



        $this->db->where('dmtin_id', $id);



        return $this->db->delete('tbl_dmtin');



    }



    



  



     public function update_dmtin_info($data,$id,$filename =''){

        $this->db->where('dmtin_id', $id);

        $this->db->update('tbl_dmtin', $data);

        if($filename!='' ){

      $filename1 = explode(',',$filename);

      foreach($filename1 as $file){

      $file_data = array(

      'image' => $file,

      'news_id' => $id

      );

      $this->db->insert('tbl_muti_news', $file_data);

      }

      }

    }



    



    public function published_dmtin_info($id) {



        $this->db->set('publication_status', 1);



        $this->db->where('dmtin_id', $id);



        return $this->db->update('tbl_dmtin');



    }



    



    public function unpublished_dmtin_info($id) {



        $this->db->set('publication_status', 0);



        $this->db->where('dmtin_id', $id);



        return $this->db->update('tbl_dmtin');



    }







     public function published_dmtin_home($id) {



        $this->db->set('publication_home', 1);



        $this->db->where('dmtin_id', $id);



        return $this->db->update('tbl_dmtin');



    }



    



    public function unpublished_dmtin_home($id) {



        $this->db->set('publication_home', 0);



        $this->db->where('dmtin_id', $id);



        return $this->db->update('tbl_dmtin');



    }







     public function published_dmtin_noibat($id) {



        $this->db->set('dmtin_feature', 1);



        $this->db->where('dmtin_id', $id);



        return $this->db->update('tbl_dmtin');



    }



    



    public function unpublished_dmtin_noibat($id) {



        $this->db->set('dmtin_feature', 0);



        $this->db->where('dmtin_id', $id);



        return $this->db->update('tbl_dmtin');



    }











     public function published_dmtin_quantam($id) {



        $this->db->set('dmtin_quantam', 1);



        $this->db->where('dmtin_id', $id);



        return $this->db->update('tbl_dmtin');



    }



    



    public function unpublished_dmtin_quantam($id) {



        $this->db->set('dmtin_quantam', 0);



        $this->db->where('dmtin_id', $id);



        return $this->db->update('tbl_dmtin');



    }



    



    



   public function get_all_published_category_dmtin(){



        $this->db->select('*');



        $this->db->from('tbl_category_dmtin');



        $this->db->where('publication_status',1);



        $this->db->where('tbl_category_dmtin.parent_id',0);



        $info = $this->db->get();



        return $info->result();



    }







        







    



}