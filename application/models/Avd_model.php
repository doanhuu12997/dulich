<?php

class Avd_model extends CI_Model{
    
    public function save_avd_info($data){
        return $this->db->insert('tbl_avd', $data);
    }
    
    public function getall_avd_info(){
        $this->db->select('*');
        $this->db->from('tbl_avd');
        $info = $this->db->get();
        return $info->result();
    }
    
    
    public function edit_avd_info($id){
        $this->db->select('*');
        $this->db->from('tbl_avd');
        $this->db->where('avd_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function delete_avd_info($id){
        $this->db->where('avd_id', $id);
        return $this->db->delete('tbl_avd');
    }
    
    public function update_avd_info($data,$id){
        $this->db->where('avd_id', $id);
        return $this->db->update('tbl_avd', $data);
    }
    
    public function published_avd_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('avd_id', $id);
        return $this->db->update('tbl_avd');
    }
    
    public function unpublished_avd_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('avd_id', $id);
        return $this->db->update('tbl_avd');
    }
    
}