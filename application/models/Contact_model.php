<?php

class Contact_model extends CI_Model{
    
    public function save_contact_info($data){
        return $this->db->insert('tbl_contact', $data);
    }
    
    public function getall_contact_info(){
        $this->db->select('*');
        $this->db->from('tbl_contact');
        $this->db->order_by('tbl_contact.contact_id','desc');
        $info = $this->db->get();
        return $info->result();
    }
    

    
    public function delete_contact_info($id){
        $this->db->where('contact_id', $id);
        return $this->db->delete('tbl_contact');
    }
    

    public function published_contact_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('contact_id', $id);
        return $this->db->update('tbl_contact');
    }
    
    public function unpublished_contact_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('contact_id', $id);
        return $this->db->update('tbl_contact');
    }
    
}