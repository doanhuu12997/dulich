<?php

class Manager_member_order_model extends CI_Model{
    

     public function manage_info(){
         $this->db->select('*');
        $this->db->from('tbl_customer');
        $this->db->order_by('tbl_customer.customer_id','asc');
        $info = $this->db->get();
        $rs_order= $info->result();
        return $rs_order;
    }

     public function get_ds_info_thuong(){
         $this->db->select('*');
        $this->db->from('tbl_origin');
        $this->db->where('tbl_origin.origin_id',1);
        $info_th = $this->db->get();
        $rs_thuong= $info_th->row();
        return $rs_thuong;
    }

     public function get_ds_info_bac(){
         $this->db->select('*');
        $this->db->from('tbl_origin');
        $this->db->where('tbl_origin.origin_id',2);

        $info_bac = $this->db->get();
        $rs_ds_inforbac= $info_bac->row();
        return $rs_ds_inforbac;
    }


 public function get_ds_info_vang(){
         $this->db->select('*');
        $this->db->from('tbl_origin');
        $this->db->where('tbl_origin.origin_id',3);
        $info_vang = $this->db->get();
        $rs_ds_inforvang= $info_vang->row();
        return $rs_ds_inforvang;
    }

 public function get_ds_info_kimcuong(){
         $this->db->select('*');
        $this->db->from('tbl_origin');
        $this->db->where('tbl_origin.origin_id',4);
        $info_kimcuong = $this->db->get();
        $rs_ds_inforkc= $info_kimcuong->row();
        return $rs_ds_inforkc;
    }


    
    public function edit_member_info($id){
        $this->db->select('*');
        $this->db->from('tbl_customer');
        $this->db->where('customer_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function update_member_info($data,$id){
        $this->db->where('customer_id', $id);
        return $this->db->update('tbl_customer', $data);
    }

       public function delete_member($id)
    {
          $this->db->where('customer_id',$id);
          return $this->db->delete('tbl_customer');
    }
    

 
    
}