<?php

class Feel_model extends CI_Model{
    
    public function save_feel_info($data){
        return $this->db->insert('tbl_feel', $data);
    }
    
    public function getall_feel_info(){
        $this->db->select('*');
        $this->db->from('tbl_feel');
        $info = $this->db->get();
        return $info->result();
    }
    
    
    public function edit_feel_info($id){
        $this->db->select('*');
        $this->db->from('tbl_feel');
        $this->db->where('feel_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function delete_feel_info($id){
        $this->db->where('feel_id', $id);
        return $this->db->delete('tbl_feel');
    }
    
    public function update_feel_info($data,$id){
        $this->db->where('feel_id', $id);
        return $this->db->update('tbl_feel', $data);
    }
    
    public function published_feel_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('feel_id', $id);
        return $this->db->update('tbl_feel');
    }
    
    public function unpublished_feel_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('feel_id', $id);
        return $this->db->update('tbl_feel');
    }
    
}