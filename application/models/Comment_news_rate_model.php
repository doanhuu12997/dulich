<?php

class Comment_news_rate_model extends CI_Model{
    
    public function save_comment_news_rate_info($data){
        return $this->db->insert('tbl_comment_news_rate', $data);
    }
    
    public function getall_comment_news_rate_info(){
        $this->db->select('*');
        $this->db->from('tbl_comment_news_rate');
        $this->db->join('tbl_dmtin','tbl_dmtin.dmtin_id=tbl_comment_news_rate.comment_news_rateid');
        $info = $this->db->get();
        return $info->result();
    }
    

     public function edit_comment_news_rate_info($id){
        $this->db->select('*');
        $this->db->from('tbl_comment_news_rate');
        $this->db->where('comment_news_rate_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
        public function update_comment_news_rate_info($data,$id){
        $this->db->where('comment_news_rate_id', $id);
        return $this->db->update('tbl_comment_news_rate', $data);
    }
    

    public function delete_comment_news_rate_info($id){
        $this->db->where('comment_news_rate_id', $id);
        return $this->db->delete('tbl_comment_news_rate');
    }

    public function comment_news_rate_show_answer($id) {
        $this->db->set('comment_news_rate_show_answer', 1);
        $this->db->where('comment_news_rate_id', $id);
        return $this->db->update('tbl_comment_news_rate');
    }
    
    public function un_comment_news_rate_show_answer($id) {
        $this->db->set('comment_news_rate_show_answer', 0);
        $this->db->where('comment_news_rate_id', $id);
        return $this->db->update('tbl_comment_news_rate');
    }
    

    public function published_comment_news_rate_info($id) {
        $this->db->set('comment_news_rate_show', 1);
        $this->db->where('comment_news_rate_id', $id);
        return $this->db->update('tbl_comment_news_rate');
    }
    
    public function unpublished_comment_news_rate_info($id) {
        $this->db->set('comment_news_rate_show', 0);
        $this->db->where('comment_news_rate_id', $id);
        return $this->db->update('tbl_comment_news_rate');
    }
    
}