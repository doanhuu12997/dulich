<?php

class Chinhsach_model extends CI_Model{
    
    public function save_chinhsach_info($data){
        return $this->db->insert('tbl_chinhsach', $data);
    }
    
    public function getall_chinhsach_info(){
        $this->db->select('*');
        $this->db->from('tbl_chinhsach');
        $info = $this->db->get();
        return $info->result();
    }
    
    
    public function edit_chinhsach_info($id){
        $this->db->select('*');
        $this->db->from('tbl_chinhsach');
        $this->db->where('chinhsach_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function delete_chinhsach_info($id){
        $this->db->where('chinhsach_id', $id);
        return $this->db->delete('tbl_chinhsach');
    }
    
    public function update_chinhsach_info($data,$id){
        $this->db->where('chinhsach_id', $id);
        return $this->db->update('tbl_chinhsach', $data);
    }
    
    public function published_chinhsach_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('chinhsach_id', $id);
        return $this->db->update('tbl_chinhsach');
    }
    
    public function unpublished_chinhsach_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('chinhsach_id', $id);
        return $this->db->update('tbl_chinhsach');
    }
    
}