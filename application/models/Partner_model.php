<?php

class Partner_model extends CI_Model{
    
    public function save_partner_info($data){
        return $this->db->insert('tbl_partner', $data);
    }
    
    public function getall_partner_info(){
        $this->db->select('*');
        $this->db->from('tbl_partner');
        $info = $this->db->get();
        return $info->result();
    }
    
    
    public function edit_partner_info($id){
        $this->db->select('*');
        $this->db->from('tbl_partner');
        $this->db->where('partner_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function delete_partner_info($id){
        $this->db->where('partner_id', $id);
        return $this->db->delete('tbl_partner');
    }
    
    public function update_partner_info($data,$id){
        $this->db->where('partner_id', $id);
        return $this->db->update('tbl_partner', $data);
    }
    
    public function published_partner_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('partner_id', $id);
        return $this->db->update('tbl_partner');
    }
    
    public function unpublished_partner_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('partner_id', $id);
        return $this->db->update('tbl_partner');
    }
    
}