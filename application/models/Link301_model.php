<?php

class Link301_model extends CI_Model{
    
    public function save_link301_info($data){
        return $this->db->insert('tbl_link301', $data);
    }
    
    public function getall_link301_info(){
        $this->db->select('*');
        $this->db->from('tbl_link301');
        $info = $this->db->get();
        return $info->result();
    }
    
    
    public function edit_link301_info($id){
        $this->db->select('*');
        $this->db->from('tbl_link301');
        $this->db->where('link301_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function delete_link301_info($id){
        $this->db->where('link301_id', $id);
        return $this->db->delete('tbl_link301');
    }
    
    public function update_link301_info($data,$id){
        $this->db->where('link301_id', $id);
        return $this->db->update('tbl_link301', $data);
    }
    
    public function published_link301_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('link301_id', $id);
        return $this->db->update('tbl_link301');
    }
    
    public function unpublished_link301_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('link301_id', $id);
        return $this->db->update('tbl_link301');
    }
    
}