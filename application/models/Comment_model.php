<?php

class Comment_model extends CI_Model{
    
    public function save_comment_info($data){
        return $this->db->insert('tbl_comment', $data);
    }
    
    public function getall_comment_info(){
        $this->db->select('*');
        $this->db->from('tbl_comment');
        $this->db->join('tbl_product','tbl_product.product_id=tbl_comment.comment_product_id');
        $this->db->join('tbl_category','tbl_category.id=tbl_product.product_category','left');
        $info = $this->db->get();
        return $info->result();
    }
    

     public function edit_comment_info($id){
        $this->db->select('*');
        $this->db->from('tbl_comment');
        $this->db->where('comment_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
        public function update_comment_info($data,$id){
        $this->db->where('comment_id', $id);
        return $this->db->update('tbl_comment', $data);
    }
    

    public function delete_comment_info($id){
        $this->db->where('comment_id', $id);
        return $this->db->delete('tbl_comment');
    }

    public function comment_show_answer($id) {
        $this->db->set('comment_show_answer', 1);
        $this->db->where('comment_id', $id);
        return $this->db->update('tbl_comment');
    }
    
    public function un_comment_show_answer($id) {
        $this->db->set('comment_show_answer', 0);
        $this->db->where('comment_id', $id);
        return $this->db->update('tbl_comment');
    }
    

    public function published_comment_info($id) {
        $this->db->set('comment_show', 1);
        $this->db->where('comment_id', $id);
        return $this->db->update('tbl_comment');
    }
    
    public function unpublished_comment_info($id) {
        $this->db->set('comment_show', 0);
        $this->db->where('comment_id', $id);
        return $this->db->update('tbl_comment');
    }
    
}