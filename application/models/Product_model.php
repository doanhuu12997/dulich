<?php



class Product_model extends CI_Model{



 public function isExists($key,$valkey,$tabel)

    {

        $this->db->from($tabel);

        $this->db->where($key,$valkey);

        $num = $this->db->count_all_results();

        if($num == 0)

        {

            return FALSE;

        }else{

            return TRUE;

        }

    }

    public function edit_data_depart($id){

       $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_depart','tbl_depart.product_id=tbl_product.product_id','left');

        $this->db->where('tbl_product.product_id', $id);

        $info = $this->db->get();

        return $info->result();

    }



    public function edit_data_ct_tour($id){

       $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_chuongtrinh','tbl_chuongtrinh.product_id=tbl_product.product_id','left');

        $this->db->where('tbl_product.product_id', $id);

        $info = $this->db->get();

        return $info->result();

    }

    

 public function edit_pro_center($id){

        $this->db->select('*');

        $this->db->from('tbl_pro_center');

        $this->db->join('tbl_category','tbl_category.id=tbl_pro_center.pro_catelogy_id','left');

        $this->db->where('tbl_pro_center.pro_product_id', $id);

        $info = $this->db->get();

        return $info->result();

    }



    public function save_pro_center($oddata){

       $this->db->insert('tbl_pro_center', $oddata);



    }

       public function get_tour($id){

       $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_depart','tbl_depart.product_id=tbl_product.product_id');

        $this->db->where('tbl_product.product_id', $id);

        $info = $this->db->get();

        return $info->row();

    }



     public function get_diemden($id){

       $this->db->select('*');

        $this->db->from('tbl_congdung');

        $this->db->where('tbl_congdung.congdung_id', $id);

        $info = $this->db->get();

        return $info->row();

    }



     public function get_xuatphat($id){

       $this->db->select('*');

        $this->db->from('tbl_xuatxu');

        $this->db->where('tbl_xuatxu.xuatxu_id', $id);

        $info = $this->db->get();

        return $info->row();

    }

     public function getcategory(){

 

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('parent_id', 0);

        $this->db->where('cate_status',1);

 

        $parent = $this->db->get();

        

        $categories = $parent->result();

        $i=0;

        foreach($categories as $main_cat){

 

            $categories[$i]->sub = $this->sub_category($main_cat->id);

            $i++;

        }

        return $categories;

    }





 



    public function sub_category($id){

 

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('parent_id', $id);

        $this->db->where('cate_status',1);



        $child = $this->db->get();

        $categories = $child->result();

        $i=0;

        foreach($categories as $sub_cat){

 

            $categories[$i]->sub = $this->sub_category($sub_cat->id);

            $i++;

        }

        return $categories;       

    }

    

    

  public function save_product_info($data,$filename){

    $this->db->insert('tbl_product', $data);

    $insert_id = $this->db->insert_id();

      if($filename!='' ){

      $filename1 = explode(',',$filename);

      foreach($filename1 as $file){

      $file_data = array(

      'image' => $file,

      'product_id' => $insert_id

      );

      $this->db->insert('tbl_thumb', $file_data);

      }

      }

    }

    

          public function get_all_product($limit,$start){

        $this->db->limit($limit, $start);

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category','tbl_category.id=tbl_product.product_category','left');

        $this->db->order_by('tbl_product.product_id', 'desc');

        $info = $this->db->get();

        return $info->result();

    }

      public function ajax_product_update_status($product_id,$status) {

        $this->db->set('publication_status',$status);

        $this->db->where('product_id', $product_id);

        return $this->db->update('tbl_product');

    }



      public function ajax_product_update_home($product_id,$status) {

        $this->db->set('publication_home_product',$status);

        $this->db->where('product_id', $product_id);

        return $this->db->update('tbl_product');

    }



     public function ajax_product_update_new($product_id,$status) {

        $this->db->set('publication_new_product',$status);

        $this->db->where('product_id', $product_id);

        return $this->db->update('tbl_product');

    }



     public function ajax_product_update_noibat($product_id,$status) {

        $this->db->set('product_feature',$status);

        $this->db->where('product_id', $product_id);

        return $this->db->update('tbl_product');

    }

        public function ajax_product_update_seller($product_id,$status) {

        $this->db->set('publication_selling_product', $status);

        $this->db->where('product_id', $product_id);

        return $this->db->update('tbl_product');

    }



     public function ajax_product_update_ttrang($product_id,$status) {

        $this->db->set('tinhtrang', $status);

        $this->db->where('product_id', $product_id);

        return $this->db->update('tbl_product');

    }



    public function ajax_product_update_hot($product_id,$status) {

        $this->db->set('product_hot', $status);

        $this->db->where('product_id', $product_id);

        return $this->db->update('tbl_product');

    }

       public function ajax_delete_product_id($product_id){

        $this->db->where('product_id', $product_id);

        return $this->db->delete('tbl_product');

        

    }



       function ajax_delete_product_all($id)

         {

          $this->db->where('product_id', $id);

          $this->db->delete('tbl_product');

         }



     public function edit_data_image($id){

       $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_thumb','tbl_thumb.product_id=tbl_product.product_id');

        $this->db->where('tbl_product.product_id', $id);

        $info = $this->db->get();

        return $info->result_array();

    }



    public function ajax_all_product() {
        $this->db->select('*');
        $this->db->from('tbl_product');
        $this->db->join('tbl_pro_center','tbl_pro_center.pro_product_id=tbl_product.product_id','left');
        $this->db->join('tbl_category','tbl_category.id=tbl_pro_center.pro_catelogy_id','left');
        $this->db->order_by('tbl_product.product_id', 'desc');
        $this->db->where('tbl_product.publication_status', 1);
        $cat_id=array('479','480');

        $this->db->where_not_in('tbl_category.parent_id', $cat_id);

        $info = $this->db->get();
        return $info->result();
        }

    public function ajax_product_by_cat($category_c1){

        // $this->db->limit($limit, $start);
        $this->db->select('*,tbl_product.publication_status as pstatus');
        $this->db->distinct();
        $this->db->from('tbl_product');
        $this->db->join('tbl_pro_center','tbl_pro_center.pro_product_id=tbl_product.product_id','left');
        $this->db->join('tbl_category','tbl_category.id=tbl_pro_center.pro_catelogy_id','left');
        $this->db->where('(tbl_pro_center.pro_catelogy_id = '.$category_c1.' or tbl_category.parent_id = '.$category_c1.')');
        $this->db->group_by('tbl_product.product_id','product_id');
        $this->db->order_by('tbl_product.product_id', 'desc');

        $info = $this->db->get();
        return $info->result();

    }



        public function ajax_product_by_function($fuction){

        $this->db->select('*,tbl_product.publication_status as pstatus');

        $this->db->from('tbl_product');

        $this->db->join('tbl_brand','tbl_brand.brand_id=tbl_product.product_brand','left');

        $this->db->where('tbl_product.product_brand',$fuction);

        $this->db->order_by('tbl_product.product_id', 'asc');

        $info = $this->db->get();

        return $info->result();

    }







        public function ajax_product_by_cat_c2($limit,$start,$category_c2){

        $this->db->limit($limit, $start);

        $this->db->select('*,tbl_product.publication_status as pstatus');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category','tbl_category.id=tbl_product.product_category','left');

        $this->db->order_by('tbl_product.product_id', 'asc');

        $info = $this->db->get();

        return $info->result();

    }





    public function ajax_product_deal() {

        $this->db->select('*,tbl_product.publication_status as pstatus');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category','left');

        $this->db->order_by('tbl_product.product_id', 'asc');

        $this->db->where('tbl_product.publication_status', 1);

        $info = $this->db->get();

        return $info->result();

    }







        public function ajax_search_product($limit,$start,$search_product){

        $this->db->limit($limit, $start);

        $this->db->select('*,tbl_product.publication_status as pstatus');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category','tbl_category.id=tbl_product.product_category','left');

        $this->db->like('tbl_product.product_title',$search_product,'both');

        $this->db->or_like('tbl_product.product_short_description',$search_product,'both');

        $this->db->or_like('tbl_product.product_long_description',$search_product,'both');

        $this->db->or_like('tbl_category.category_name',$search_product,'both');

        $this->db->order_by('tbl_product.product_id', 'desc');

        $info = $this->db->get();

        return $info->result();

    }



         function insert($data)

         {

          $this->db->insert_batch('tbl_product', $data);

         }



        public function record_count() {

        return $this->db->count_all("tbl_product");

        }

        public function record_count_product_by_cat($category_c1) {

          $jquery=$this->db->query("select count(*) as count from tbl_product where product_category='".$category_c1."'");

          $rs_query=$jquery->row();

          return $rs_query;

        }



          public function record_count_product_by_cat_c2($category_c2) {

          $jquery=$this->db->query("select count(*) as count from tbl_product where product_category_level2='".$category_c2."'");

          $rs_query=$jquery->row();

          return $rs_query;

        }

  

        public function get_category(){

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('tbl_category.parent_id', 0);

        $this->db->order_by('tbl_category.id', 'asc');

        $info = $this->db->get();

        return $info->result();

    }



     public function get_function(){

        $this->db->select('*');

        $this->db->from('tbl_brand');

        $this->db->order_by('tbl_brand.brand_id', 'desc');

        $info = $this->db->get();

        return $info->result();

    }





    

    

    public function edit_product_info($id){

        $this->db->select('*,tbl_product.publication_status as pstatus');

        $this->db->from('tbl_product');

       $this->db->join('tbl_category','tbl_category.id=tbl_product.product_category','left');

        $this->db->where('tbl_product.product_id', $id);



        $info = $this->db->get();

        return $info->row();

    }



         public function update_product_info($data,$id,$filename =''){

        $this->db->where('product_id', $id);

        $this->db->update('tbl_product', $data);

        if($filename!='' ){

      $filename1 = explode(',',$filename);

      foreach($filename1 as $file){

      $file_data = array(

      'image' => $file,

      'product_id' => $id

      );

      $this->db->insert('tbl_thumb', $file_data);

      }

      }

    }

       

    public function delete_product_info($id){

        $this->db->where('product_id', $id);

        return $this->db->delete('tbl_product');

    }

    





     function update_ajax($data, $id)

        {

          $this->db->where('product_id', $id);

          return $this->db->update('tbl_product', $data);

     }





    public function published_sale($id) {

        $this->db->set('tinhtrang', 1);

        $this->db->where('product_id', $id);

        return $this->db->update('tbl_product');

    }

    

    public function unpublished_sale($id) {

        $this->db->set('tinhtrang', 0);

        $this->db->where('product_id', $id);

        return $this->db->update('tbl_product');

    }





    public function published_selling($id) {

        $this->db->set('publication_selling_product', 1);

        $this->db->where('product_id', $id);

        return $this->db->update('tbl_product');

    }

    

    public function unpublished_selling($id) {

        $this->db->set('publication_selling_product', 0);

        $this->db->where('product_id', $id);

        return $this->db->update('tbl_product');

    }





    public function published_new($id) {

        $this->db->set('publication_new_product', 1);

        $this->db->where('product_id', $id);

        return $this->db->update('tbl_product');

    }

    

    public function unpublished_new($id) {

        $this->db->set('publication_new_product', 0);

        $this->db->where('product_id', $id);

        return $this->db->update('tbl_product');

    }



     public function published_home($id) {

        $this->db->set('publication_home_product', 1);

        $this->db->where('product_id', $id);

        return $this->db->update('tbl_product');

    }

    

    public function unpublished_home($id) {

        $this->db->set('publication_home_product', 0);

        $this->db->where('product_id', $id);

        return $this->db->update('tbl_product');

    }





     public function published_view($id) {

        $this->db->set('recentlyViewed', 1);

        $this->db->where('product_id', $id);

        return $this->db->update('tbl_product');

    }

    

    public function unpublished_view($id) {

        $this->db->set('recentlyViewed', 0);

        $this->db->where('product_id', $id);

        return $this->db->update('tbl_product');

    }



    

    public function published_product_info($id) {

        $this->db->set('publication_status', 1);

        $this->db->where('product_id', $id);

        return $this->db->update('tbl_product');

    }

    

    public function unpublished_product_info($id) {

        $this->db->set('publication_status', 0);

        $this->db->where('product_id', $id);

        return $this->db->update('tbl_product');

    }

    

    

  



       public function get_all_size(){

        $this->db->select('*');

        $this->db->from('tbl_size');

        $this->db->where('publication_status',1);

        $info = $this->db->get();

        return $info->result();

    }



    public function get_all_size_price(){

        $this->db->select('*');

        $this->db->from('tbl_size');

        $this->db->where('publication_status',1);

        $info = $this->db->get();

        return $info->result_array();

    }

        public function get_all_color(){

        $this->db->select('*');

        $this->db->from('tbl_color');

        $this->db->where('publication_status',1);

        $info = $this->db->get();

        return $info->result_array();

    }

    

    

   public function get_all_published_brand(){

        $this->db->select('*');

        $this->db->from('tbl_brand');

        $this->db->where('brand_publication_status',1);

        $info = $this->db->get();

        return $info->result();

    }



        public function all_published_xuatphat(){

        $this->db->select('*');

        $this->db->from('tbl_xuatxu');

        $this->db->where('publication_status',1);

        $info = $this->db->get();

        return $info->result();

    }



     public function all_published_thanhphan(){

        $this->db->select('*');

        $this->db->from('tbl_thanhphan');

        $this->db->where('publication_status',1);

        $info = $this->db->get();

        return $info->result();

    }



     public function all_published_diemden(){

        $this->db->select('*');

        $this->db->from('tbl_congdung');

        $this->db->where('publication_status',1);

        $info = $this->db->get();

        return $info->result();

    }

    

}