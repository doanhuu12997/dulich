<?php

class Thanhphan_model extends CI_Model{
    
    public function save_thanhphan_info($data){
        return $this->db->insert('tbl_thanhphan', $data);
    }
    
    public function getall_thanhphan_info(){
        $this->db->select('*');
        $this->db->from('tbl_thanhphan');
        $info = $this->db->get();
        return $info->result();
    }
    
    
    public function edit_thanhphan_info($id){
        $this->db->select('*');
        $this->db->from('tbl_thanhphan');
        $this->db->where('thanhphan_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function delete_thanhphan_info($id){
        $this->db->where('thanhphan_id', $id);
        return $this->db->delete('tbl_thanhphan');
    }
    
    public function update_thanhphan_info($data,$id){
        $this->db->where('thanhphan_id', $id);
        return $this->db->update('tbl_thanhphan', $data);
    }
    
    public function published_thanhphan_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('thanhphan_id', $id);
        return $this->db->update('tbl_thanhphan');
    }
    
    public function unpublished_thanhphan_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('thanhphan_id', $id);
        return $this->db->update('tbl_thanhphan');
    }
    
}