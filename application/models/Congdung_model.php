<?php

class Congdung_model extends CI_Model{
    
    public function save_congdung_info($data){
        return $this->db->insert('tbl_congdung', $data);
    }
    
    public function getall_congdung_info(){
        $this->db->select('*');
        $this->db->from('tbl_congdung');
        $info = $this->db->get();
        return $info->result();
    }
    
    
    public function edit_congdung_info($id){
        $this->db->select('*');
        $this->db->from('tbl_congdung');
        $this->db->where('congdung_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function delete_congdung_info($id){
        $this->db->where('congdung_id', $id);
        return $this->db->delete('tbl_congdung');
    }
    
    public function update_congdung_info($data,$id){
        $this->db->where('congdung_id', $id);
        return $this->db->update('tbl_congdung', $data);
    }
    
    public function published_congdung_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('congdung_id', $id);
        return $this->db->update('tbl_congdung');
    }
    
    public function unpublished_congdung_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('congdung_id', $id);
        return $this->db->update('tbl_congdung');
    }
    
}