<?php

class Xuatxu_model extends CI_Model{
    
    public function save_xuatxu_info($data){
        return $this->db->insert('tbl_xuatxu', $data);
    }
    
    public function getall_xuatxu_info(){
        $this->db->select('*');
        $this->db->from('tbl_xuatxu');
        $info = $this->db->get();
        return $info->result();
    }
    
    
    public function edit_xuatxu_info($id){
        $this->db->select('*');
        $this->db->from('tbl_xuatxu');
        $this->db->where('xuatxu_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function delete_xuatxu_info($id){
        $this->db->where('xuatxu_id', $id);
        return $this->db->delete('tbl_xuatxu');
    }
    
    public function update_xuatxu_info($data,$id){
        $this->db->where('xuatxu_id', $id);
        return $this->db->update('tbl_xuatxu', $data);
    }
    
    public function published_xuatxu_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('xuatxu_id', $id);
        return $this->db->update('tbl_xuatxu');
    }
    
    public function unpublished_xuatxu_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('xuatxu_id', $id);
        return $this->db->update('tbl_xuatxu');
    }
    
}