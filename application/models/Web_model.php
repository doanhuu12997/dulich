<?php



class Web_model extends CI_Model
{

    public function get_all_featured_product()
    {

        $this->db->select('*,tbl_product.publication_status as pstatus');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category');

        $this->db->order_by('tbl_product.product_id', 'asc');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->where('tbl_product.product_feature', 1);

        $this->db->limit(7);

        $info = $this->db->get();

        return $info->result();
    }

    public function cate_tembulding_show_menu()
    {
        $get_id = $this->db->query("select * from tbl_category_dmtin where publication_status =1  and id=88  order by id asc ");
        $rs_get_dmtin = $get_id->result();
        return $rs_get_dmtin;
    }

    public function cate_visa_vemaybay_show_menu()
    {
        $get_id = $this->db->query("select * from tbl_category_dmtin where publication_status =1  and id=84  order by id asc ");
        $rs_get_dmtin = $get_id->result();
        return $rs_get_dmtin;
    }

    public function get_child_menu_news_write($id)
    {
        $this->db->select('*,tbl_dmtin.publication_status as pstatus');
        $this->db->from('tbl_dmtin');
        $this->db->join('tbl_category_dmtin', 'tbl_category_dmtin.id=tbl_dmtin.dmtin_category');
        $this->db->order_by('tbl_dmtin.dmtin_id', 'asc');
        $this->db->where('tbl_dmtin.publication_status', 1);
        $this->db->where('tbl_dmtin.dmtin_category', $id);

        $info = $this->db->get();
        return $info->result();
    }

    public function get_xuatphat($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_xuatxu');

        $this->db->where('tbl_xuatxu.xuatxu_id', $id);

        $info = $this->db->get();

        return $info->row();
    }

    public function getcate()
    {

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('cate_status', 1);



        $info = $this->db->get();

        return $info->result();
    }



    public function getCategories($cat_id)
    {



        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('publication_home', 1);

        $this->db->where('cate_status', 1);

        $this->db->or_where('parent_id', $cat_id);

        $info = $this->db->get();

        return $info->result();
    }



    public function getCate_tour_nb()
    {
        $this->db->select('*');
        $this->db->from('tbl_category');
        $this->db->where('cate_status', 1);
        $this->db->where('noibat', 1);
        $cat_id = array('479', '497');
        $this->db->where_in('parent_id', $cat_id);
        $info = $this->db->get();
        return $info->result();
    }


    public function get_all_cate_tour()
    {
        $this->db->select('*');
        $this->db->from('tbl_category');
        $this->db->where('parent_id', 0);
        $this->db->where('cate_status', 1);
        $this->db->where('noibat', 1);
        $this->db->order_by('tbl_category.id', 'asc');
        $parent = $this->db->get();
        $categories = $parent->result();
        $i = 0;
        foreach ($categories as $main_cat) {
            $categories[$i]->sub = $this->sub_category_tour($main_cat->id);
            $i++;
        }
        return $categories;
    }

    public function sub_category_tour($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_category');
        $this->db->where('parent_id', $id);
        $this->db->where('cate_status', 1);
        $this->db->where('noibat', 1);
        $this->db->order_by('id', 'asc');
        $child = $this->db->get();
        $categories = $child->result();
        $i = 0;
        foreach ($categories as $sub_cat) {
            $categories[$i]->sub = $this->sub_category_tour($sub_cat->id);
            $i++;
        }
        return $categories;
    }




    public function show_product_other($product_id, $id)
    {

        $this->db->select('*');

        $this->db->distinct();

        $this->db->from('tbl_product');

        $this->db->where_not_in('tbl_product.product_id', $product_id);

        $this->db->join('tbl_pro_center', 'tbl_pro_center.pro_product_id=tbl_product.product_id', 'left');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_pro_center.pro_catelogy_id', 'left');

        $this->db->where('tbl_pro_center.pro_catelogy_id', $id);

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->order_by('rand()');

        $this->db->limit(10);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_product_cate($id)
    {



        $this->db->select('*');





        $this->db->distinct();

        $this->db->from('tbl_product');

        $this->db->join('tbl_pro_center', 'tbl_pro_center.pro_product_id=tbl_product.product_id', 'left');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_pro_center.pro_catelogy_id', 'left');

        $this->db->where('(tbl_pro_center.pro_catelogy_id = ' . $id . ' or tbl_category.parent_id = ' . $id . ')');



        $this->db->group_by('tbl_product.product_id', 'product_id');

        $this->db->where('tbl_product.publication_status', 1);



        $this->db->where('tbl_product.publication_home_product', 1);



        $this->db->order_by('tbl_product.product_id', 'desc');



        $info = $this->db->get();



        return $info->result();
    }



    public function fetch_data_product_by_cat($limit, $start, $id)
    {







        $this->db->limit($limit, $start);

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_pro_center', 'tbl_pro_center.pro_product_id=tbl_product.product_id', 'left');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_pro_center.pro_catelogy_id', 'left');

        $this->db->where('(tbl_pro_center.pro_catelogy_id = ' . $id . ' or tbl_category.parent_id = ' . $id . ')');

        $this->db->group_by('tbl_product.product_id', 'product_id');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->order_by('tbl_product.product_id', 'desc');





        $info = $this->db->get();

        return $info->result();
    }





    public function fetch_data_product_by_cat_combo($limit, $start, $id)
    {





        $this->db->limit($limit, $start);

        $this->db->select('*');

        $this->db->distinct();

        $this->db->from('tbl_product');

        $this->db->join('tbl_pro_center', 'tbl_pro_center.pro_product_id=tbl_product.product_id', 'left');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_pro_center.pro_catelogy_id', 'left');

        $this->db->where('(tbl_pro_center.pro_catelogy_id = ' . $id . ' or tbl_category.parent_id = ' . $id . ')');



        $this->db->group_by('tbl_product.product_id', 'product_id');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->order_by('tbl_product.product_id', 'desc');





        $info = $this->db->get();

        return $info->result();
    }



    public function get_all_category_show_menu()
    {
        $this->db->select('*');
        $this->db->from('tbl_category');
        $this->db->where('publication_header', 1);
        $this->db->where('parent_id', 0);
        $this->db->where('cate_status', 1);
        $this->db->order_by('tbl_category.id', 'asc');
        $parent = $this->db->get();
        $categories = $parent->result();
        $i = 0;
        foreach ($categories as $main_cat) {
            $categories[$i]->sub = $this->sub_category($main_cat->id);
            $i++;
        }
        return $categories;
    }

    public function sub_category($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_category');
        $this->db->where('parent_id', $id);
        $this->db->where('cate_status', 1);
        $this->db->order_by('id', 'asc');
        $child = $this->db->get();
        $categories = $child->result();
        $i = 0;
        foreach ($categories as $sub_cat) {
            $categories[$i]->sub = $this->sub_category($sub_cat->id);
            $i++;
        }
        return $categories;
    }




    public function get_child_menu_news($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_category_dmtin');

        $this->db->where('publication_status', 1);

        $this->db->where('parent_id', $id);

        $this->db->order_by('tbl_category_dmtin.id', 'asc');

        $info = $this->db->get();

        return $info->result();
    }

    public function productView($id)

    {

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->order_by('tbl_product.product_id', 'asc');

        $this->db->where_in('product_id', $id);

        $this->db->where('publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }

    public function news_hot($order_news)

    {

        $this->db->select('*,tbl_dmtin.publication_status as pstatus');

        $this->db->from('tbl_dmtin');

        $this->db->join('tbl_category_dmtin', 'tbl_category_dmtin.id=tbl_dmtin.dmtin_category');

        $this->db->order_by('tbl_dmtin.dmtin_id ', $order_news);

        $this->db->where('tbl_dmtin.publication_status', 1);

        $this->db->where('tbl_dmtin.publication_home', 1);

        $this->db->where('tbl_dmtin.dmtin_feature', 1);

        $this->db->limit(2);

        $info = $this->db->get();

        return $info->result();
    }

    public function get_daban($id)
    {

        $jquery = $this->db->query("select  count(or_detail.product_id_order) as daban , o.order_id,o.note, o.actions,o.order_day, o.order_total, or_detail.product_name,or_detail.product_id_order FROM tbl_order as o INNER JOIN tbl_order_details as or_detail ON or_detail.order_id=o.order_id WHERE or_detail.product_id_order='" . $id . "'

             AND o.actions='Đơn hàng mới'");

        $rs_query = $jquery->row();

        return $rs_query;
    }

    public function update_news($data_view, $id)
    {

        $this->db->where('dmtin_id', $id);

        return $this->db->update('tbl_dmtin', $data_view);
    }



    public function get_tukhoa()
    {

        $this->db->select('*');

        $this->db->from('tbl_thanhphan');

        $this->db->where('publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function fetch_data_product_price_up_down_cat($id, $price_updown)
    {

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->where('(tbl_product.product_category = ' . $id . ' or tbl_category.parent_id = ' . $id . ')');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->order_by('tbl_product.product_price', $price_updown);

        $this->db->limit(32);

        $info = $this->db->get();

        return $info->result();
    }



    public function fetch_data_product_mucgia_cat($id, $from, $to)
    {

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->where('(tbl_product.product_category = ' . $id . ' or tbl_category.parent_id = ' . $id . ')');

        $this->db->where('tbl_product.publication_status', 1);

        // $this->db->where( "tbl_product.product_price BETWEEN '".$from."' AND '".$to."'");

        $this->db->where('tbl_product.product_price >=', $from);

        $this->db->where('tbl_product.product_price <=', $to);

        $this->db->order_by('tbl_product.product_price', 'desc');

        $this->db->limit(32);

        $info = $this->db->get();

        return $info->result();
    }

    public function fetch_data_product_star_cat($id, $star)
    {

        $this->db->select('*, COUNT(comment_start)');

        $this->db->from('tbl_product');

        $this->db->join('tbl_comment', 'tbl_comment.comment_product_id=tbl_product.product_id', 'left');



        $this->db->where('tbl_comment.comment_start', $star);

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->order_by('tbl_product.product_id', 'desc');

        $this->db->limit(32);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_product_images_news($id)
    {

        $this->db->limit(3);

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->where('(tbl_product.product_category = ' . $id . ' or tbl_category.parent_id = ' . $id . ')');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->order_by('tbl_product.product_id', 'desc');

        $info = $this->db->get();

        return $info->result();
    }

    public function queryrelated($child_cate, $id)
    {

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('tbl_category.parent_id', $child_cate);

        $this->db->where_not_in('tbl_category.id', $id);

        $this->db->where('tbl_category.cate_status', 1);

        $info = $this->db->get();

        return $info->result();
    }

    public function queryrelated_parent($parent_cate, $id_parent)
    {

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('tbl_category.parent_id', $parent_cate);

        $this->db->where_not_in('tbl_category.id', $id_parent);

        $this->db->where('tbl_category.cate_status', 1);

        $info = $this->db->get();

        return $info->result();
    }

    public function get_catelogy_parent($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('tbl_category.cate_status', 1);

        $this->db->where('tbl_category.id', $id);

        $this->db->where('tbl_category.parent_id', 0);

        $info = $this->db->get();

        return $info->row();
    }

    public function show_xuatxu_detail($product_id)
    {

        $this->db->select('product_xuatxu');

        $this->db->from('tbl_product');

        $this->db->where('tbl_product.product_id', $product_id);

        $info = $this->db->get();

        return $info->row_array();
    }

    public function get_name_user($id_user)
    {

        $this->db->select('*');

        $this->db->from('tbl_dmtin');

        $this->db->join('tbl_user', 'tbl_user.user_id=tbl_dmtin.dmtin_author', 'left');

        $this->db->where('tbl_dmtin.publication_status', 1);

        $this->db->where('tbl_dmtin.dmtin_author', $id_user);

        $info = $this->db->get();

        return $info->row();
    }

    public function show_ship_detail($product_id)
    {

        $this->db->select('product_size');

        $this->db->from('tbl_product');

        $this->db->where('tbl_product.product_id', $product_id);

        $info = $this->db->get();

        return $info->row_array();
    }

    public function get_project_news($id_dm_service, $limit_service, $order_service)
    {

        $this->db->select('*,tbl_dmtin.publication_status as pstatus');

        $this->db->from('tbl_dmtin');

        $this->db->join('tbl_category_dmtin', 'tbl_category_dmtin.id=tbl_dmtin.dmtin_category');

        $this->db->order_by('tbl_dmtin.dmtin_stt', $order_service);

        $this->db->where('tbl_dmtin.publication_status', 1);

        $this->db->where('tbl_dmtin.publication_home', 1);

        $this->db->where_not_in('tbl_dmtin.dmtin_category', $id_dm_service);

        $this->db->limit($limit_service);

        $info = $this->db->get();

        return $info->result();
    }



    public function order_info($order_id)
    {

        $this->db->select('*');

        $this->db->from('tbl_order');

        $this->db->join('tbl_customer', 'tbl_customer.customer_id = tbl_order.cus_id', 'left');

        $this->db->where('tbl_order.order_id', $order_id);

        $this->db->order_by('tbl_order.order_id', 'desc');

        $result = $this->db->get();

        return $result->row();
    }



    public function discount_codes($code, $date_end)
    {

        date_default_timezone_set('Asia/Ho_Chi_Minh');

        $time = time();

        $rs_time_begin = date("Y-m-d H:i:s", $time);

        $this->db->select('*');

        $this->db->from('tbl_discount_codes');

        if ($rs_time_begin >= $date_end) {

            $this->db->where('tbl_discount_codes.code', $code);

            $this->db->or_where('tbl_discount_codes.code_auto', $code);
        } else {

            $this->db->where('tbl_discount_codes.code', $code);

            $this->db->where("tbl_discount_codes.valid_to_date BETWEEN '" . $rs_time_begin . "' AND '" . $date_end . "'");

            $this->db->or_where('tbl_discount_codes.code_auto', $code);
        }

        $info = $this->db->get();

        return $info->row();
    }

    public function getProduct($page, $id)
    {

        $limit = 20;

        $offset = ($page - 1) * $limit;

        $this->db->limit($limit, $offset);

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->where('(tbl_product.product_category = ' . $id . ' or tbl_category.parent_id = ' . $id . ')');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->order_by('tbl_product.product_id', 'desc');



        $info = $this->db->get();

        return $info->result();
    }



    public function next_Product($page, $id)
    {

        $limit = 1;

        $offset = ($page - 1) * $limit;

        $this->db->limit($limit, $offset);

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->where('(tbl_product.product_category = ' . $id . ' or tbl_category.parent_id = ' . $id . ')');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->order_by('tbl_product.product_id', 'desc');



        $info = $this->db->get();

        return $info->result();
    }



    public function get_price_by_city($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_order');

        $this->db->join('tbl_city', 'tbl_city.id = tbl_order.city_id', 'left');

        $this->db->where('tbl_order.city_id', $id);

        $result = $this->db->get();

        return $result->row();
    }

    public function ajax_search_product($search)
    {

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->order_by('tbl_product.product_id', 'DESC');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->like('tbl_product.product_title', $search, 'both');

        $this->db->or_like('tbl_category.category_name', $search, 'both');



        $info = $this->db->get();

        return $info->result();
    }



    public function load_ajax_product_c2($category_c2)
    {

        $this->db->select('*,tbl_product.publication_status as pstatus');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->where('tbl_product.product_category', $category_c2);

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->order_by('tbl_product.product_id', 'asc');

        $this->db->limit(8);

        $info = $this->db->get();

        return $info->result();
    }



    public function product_new()
    {

        $this->db->select('*,tbl_product.publication_status as pstatus');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category');

        $this->db->order_by('tbl_product.product_id', 'asc');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->where('tbl_product.publication_new_product', 1);

        $this->db->where('tbl_product.publication_home_product', 1);

        $this->db->limit(8);

        $info = $this->db->get();

        return $info->result();
    }



    public function product_sp_moive()
    {

        $this->db->select('*,tbl_product.publication_status as pstatus');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category');

        $this->db->order_by('tbl_product.product_id', 'asc');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->where('tbl_product.publication_new_product', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function product_sp_banchay()
    {

        $this->db->select('*,tbl_product.publication_status as pstatus');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category');

        $this->db->order_by('tbl_product.product_id', 'asc');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->where('tbl_product.publication_selling_product', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function product_selling()
    {

        $this->db->select('*,tbl_product.publication_status as pstatus');
        $this->db->distinct();
        $this->db->from('tbl_product');
        $this->db->join('tbl_pro_center', 'tbl_pro_center.pro_product_id=tbl_product.product_id', 'left');
        $this->db->join('tbl_category', 'tbl_category.id=tbl_pro_center.pro_catelogy_id', 'left');
        $this->db->group_by('tbl_product.product_id', 'product_id');
        $this->db->order_by('tbl_product.product_id', 'desc');
        $this->db->where('tbl_product.publication_status', 1);
        $this->db->where('tbl_product.publication_selling_product', 1);
        $info = $this->db->get();
        return $info->result();
    }

    public function chum_tour_hot($id)
    {

        $this->db->select('*,tbl_product.publication_status as pstatus');
        $this->db->distinct();
        $this->db->from('tbl_product');
        $this->db->join('tbl_pro_center', 'tbl_pro_center.pro_product_id=tbl_product.product_id', 'left');
        $this->db->join('tbl_category', 'tbl_category.id=tbl_pro_center.pro_catelogy_id', 'left');
        $this->db->where('(tbl_pro_center.pro_catelogy_id = ' . $id . ' or tbl_category.parent_id = ' . $id . ')');
        $this->db->group_by('tbl_product.product_id', 'product_id');
        $this->db->order_by('tbl_product.product_id', 'desc');
        $this->db->where('tbl_product.publication_status', 1);
        $info = $this->db->get();
        return $info->result();
    }



    public function tour_hot()
    {

        $this->db->select('*,tbl_product.publication_status as pstatus');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category');

        $this->db->order_by('tbl_product.product_id', 'asc');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->where('tbl_product.publication_selling_product', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function product_sp_uudai()

    {

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category');

        $this->db->order_by('tbl_product.product_id', 'asc');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->where('tbl_product.publication_selling_product', 1);



        $info = $this->db->get();

        return $info->result();
    }



    public function product_sp_tindung()

    {

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category');

        $this->db->order_by('tbl_product.product_id', 'asc');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->where('tbl_product.publication_new_product', 1);



        $info = $this->db->get();

        return $info->result();
    }

    public function pr_tindung_detail()

    {

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category');

        $this->db->order_by('tbl_product.product_id', 'asc');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->where('tbl_product.publication_new_product', 1);

        $this->db->limit(5);

        $info = $this->db->get();

        return $info->result();
    }



    public function product_sale()
    {

        $this->db->select('*,tbl_product.publication_status as pstatus');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category');

        $this->db->order_by('tbl_product.product_id', 'asc');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->where('tbl_product.publication_sale_product', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_baiviet_quantam()
    {

        $this->db->select('*,tbl_dmtin.publication_status as pstatus');

        $this->db->from('tbl_dmtin');

        $this->db->join('tbl_category_dmtin', 'tbl_category_dmtin.id=tbl_dmtin.dmtin_category');

        $this->db->order_by('tbl_dmtin.dmtin_id', 'DESC');

        $this->db->where('tbl_dmtin.publication_status', 1);

        $this->db->where('tbl_dmtin.dmtin_quantam', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_id_post($id)
    {

        $this->db->select('*,tbl_dmtin.publication_status as pstatus');

        $this->db->from('tbl_dmtin');

        $this->db->join('tbl_category_dmtin', 'tbl_category_dmtin.id=tbl_dmtin.dmtin_category');

        $this->db->order_by('tbl_dmtin.dmtin_id', 'DESC');

        $this->db->where('tbl_dmtin.publication_status', 1);

        $this->db->where('tbl_dmtin.publication_home', 1);

        $this->db->where('dmtin_category', $id);

        $info = $this->db->get();

        return $info->result();
    }

    public function get_cate_news_noibat($id)
    {

        $this->db->select('*,tbl_dmtin.publication_status as pstatus');

        $this->db->from('tbl_dmtin');

        $this->db->join('tbl_category_dmtin', 'tbl_category_dmtin.id=tbl_dmtin.dmtin_category');

        $this->db->order_by('tbl_dmtin.dmtin_id', 'desc');

        $this->db->where('tbl_dmtin.publication_status', 1);

        $this->db->where('tbl_dmtin.dmtin_feature', 1);

        $this->db->where('tbl_dmtin.dmtin_category', $id);

        $this->db->limit(4);

        $info = $this->db->get();

        return $info->result();
    }



    public function update_products($data_view, $id)
    {

        $this->db->where('product_id', $id);

        return $this->db->update('tbl_product', $data_view);
    }



    public function update_forgetpass_info($id, $pass_news)
    {

        $this->db->where('customer_id', $id);

        return $this->db->update('tbl_customer', $pass_news);
    }



    public function get_details($id)

    {

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->where('product_id', $id);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_all_product()
    {

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category');

        /*   $this->db->join('tbl_brand', 'tbl_brand.brand_id=tbl_product.product_brand','left');*/

        $this->db->order_by('tbl_product.product_id', 'ASC');

        $this->db->where('tbl_product.publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }



    // Count all record of table "contact_info" in database.

    public function record_count()
    {

        return $this->db->count_all("tbl_product");
    }

    public function record_count_product_by_cat($id)
    {



        $this->db->select('count(*) as count');

        $this->db->distinct();

        $this->db->from('tbl_product');

        $this->db->join('tbl_pro_center', 'tbl_pro_center.pro_product_id=tbl_product.product_id', 'left');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_pro_center.pro_catelogy_id', 'left');

        $this->db->where('(tbl_pro_center.pro_catelogy_id = ' . $id . ' or tbl_category.parent_id = ' . $id . ')');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->order_by('tbl_product.product_id', 'asc');

        $info = $this->db->get();

        return $info->row();
    }

    public function record_count_product_by_country($id)
    {

        $jquery = $this->db->query("select count(*) as count from tbl_product where product_brand='" . $id . "'");

        $rs_query = $jquery->row();

        return $rs_query;
    }



    public function record_count_product_by_cate_news($id)
    {

        $jquery = $this->db->query("select count(*) as count from tbl_dmtin where dmtin_category='" . $id . "'");

        $rs_query = $jquery->row();

        return $rs_query;
    }

    // Fetch data according to per_page limit.



    public function fetch_data($limit, $start)
    {

        $this->db->limit($limit, $start);

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $info = $this->db->get();

        return $info->result();
    }



    public function fetch_data_product_by_cat_16($data_idall)
    {

        $this->db->limit(0, 2);

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->order_by('tbl_product.product_id', 'asc');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->where('tbl_category.id', $data_idall);

        $this->db->or_where('tbl_category.parent_id', $data_idall);

        $info = $this->db->get();

        return $info->result();
    }







    public function fetch_data_product_by_cat_id($limit, $start, $id)
    {

        $this->db->limit($limit, $start);

        $this->db->select('*');

        $this->db->distinct();

        $this->db->from('tbl_product');

        $this->db->join('tbl_pro_center', 'tbl_pro_center.pro_product_id=tbl_product.product_id', 'left');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_pro_center.pro_catelogy_id', 'left');

        $this->db->where('(tbl_pro_center.pro_catelogy_id = ' . $id . ' or tbl_category.parent_id = ' . $id . ')');



        $this->db->group_by('tbl_product.product_id', 'product_id');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->order_by('tbl_product.product_id', 'desc');

        $info = $this->db->get();

        return $info->result();
    }







    public function fetch_data_product_price_tang_by_cat($limit, $start, $id)
    {

        $this->db->limit($limit, $start);

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->order_by('tbl_product.product_price', 'asc');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->where('(tbl_product.product_category = ' . $id . ' or tbl_category.parent_id = ' . $id . ')');

        $info = $this->db->get();

        return $info->result();
    }

    public function fetch_data_product_price_giam_by_cat($limit, $start, $id)
    {

        $this->db->limit($limit, $start);

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->order_by('tbl_product.product_price', 'desc');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->where('(tbl_product.product_category = ' . $id . ' or tbl_category.parent_id = ' . $id . ')');

        $info = $this->db->get();

        return $info->result();
    }



    public function fetch_data_product_selling_by_cat($limit, $start, $id)
    {

        $this->db->limit($limit, $start);

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->where('tbl_product.publication_selling_product', 1);

        $this->db->where('(tbl_product.product_category = ' . $id . ' or tbl_category.parent_id = ' . $id . ')');

        $this->db->order_by('tbl_product.product_id', 'asc');

        $info = $this->db->get();

        return $info->result();
    }





    public function fetch_data_product_price_tang_by_sanpham($limit, $start)
    {

        $this->db->limit($limit, $start);

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->order_by('tbl_product.product_price_sale', 'asc');

        $this->db->where('tbl_product.publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }

    public function fetch_data_product_price_giam_by_sanpham($limit, $start)
    {

        $this->db->limit($limit, $start);

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->order_by('tbl_product.product_price_sale', 'desc');

        $this->db->where('tbl_product.publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function fetch_data_product_selling_by_sanpham($limit, $start)
    {

        $this->db->limit($limit, $start);

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->order_by('tbl_product.product_id', 'asc');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->where('tbl_product.publication_selling_product', 1);

        $info = $this->db->get();

        return $info->result();
    }







    public function fetch_data_product_by_country($limit, $start, $id)
    {

        $this->db->limit($limit, $start);

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_brand', 'tbl_brand.brand_id=tbl_product.product_brand', 'left');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->order_by('tbl_product.product_id', 'DESC');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->where('tbl_brand.brand_id', $id);

        $info = $this->db->get();

        return $info->result();
    }





    public function get_product_other($id)
    {



        $query = $this->db->query("select * from tbl_product where publication_status =1 

                                and product_id <>'" . $id . "' ");

        $rs_other = $query->result();

        return $rs_other;
    }



    public function get_product_other_singnew($id)
    {



        $query = $this->db->query("select * from tbl_product where publication_status =1 

                                and product_id <>'" . $id . "' limit 0,3 ");

        $rs_other = $query->result();

        return $rs_other;
    }



    public function get_single_product($id, $id_cate)
    {

        $this->db->select('*');

        $this->db->distinct();

        $this->db->from('tbl_product');

        $this->db->join('tbl_pro_center', 'tbl_pro_center.pro_product_id=tbl_product.product_id', 'left');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_pro_center.pro_catelogy_id', 'left');

        $this->db->where('(tbl_pro_center.pro_catelogy_id = ' . $id_cate . ' or tbl_category.parent_id = ' . $id_cate . ')');

        $this->db->where('tbl_product.product_id', $id);

        $info = $this->db->get();

        return $info->row();
    }





    public function get_depart_detail($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_depart');

        $this->db->join('tbl_product', 'tbl_product.product_id=tbl_depart.product_id', 'left');

        $this->db->order_by('tbl_depart.id', 'asc');

        $this->db->where('tbl_depart.product_id', $id);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_ct_tour_detail($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_chuongtrinh');

        $this->db->join('tbl_product', 'tbl_product.product_id=tbl_chuongtrinh.product_id', 'left');

        $this->db->order_by('tbl_chuongtrinh.product_id', 'asc');

        $this->db->where('tbl_chuongtrinh.product_id', $id);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_depart_row($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_depart');

        $this->db->join('tbl_product', 'tbl_product.product_id=tbl_depart.product_id', 'left');

        $this->db->order_by('tbl_depart.id', 'asc');

        $this->db->where('tbl_depart.product_id', $id);



        $info = $this->db->get();

        return $info->row();
    }


    public function get_depart_schedule_result($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_depart');
        $this->db->join('tbl_product', 'tbl_product.product_id=tbl_depart.product_id', 'left');
        $this->db->order_by('tbl_depart.id', 'asc');
        $this->db->where('tbl_depart.product_id', $id);
        $info = $this->db->get();
        return $info->result();
    }



    public function get_single_product_cate($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->join('tbl_product', 'tbl_product.product_category=tbl_category.id', 'left');

        $this->db->where('tbl_product.product_id', $id);

        $this->db->or_where('tbl_category.parent_id', '0');

        $info = $this->db->get();

        return $info->row();
    }



    public function get_single_product_slug()
    {

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_pro_center', 'tbl_pro_center.pro_product_id=tbl_product.product_id', 'inner');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_pro_center.pro_catelogy_id', 'inner');

        $info = $this->db->get();

        return $info->result_array();
    }



    public function get_single_dmtin($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_dmtin');

        $this->db->join('tbl_category_dmtin', 'tbl_category_dmtin.id=tbl_dmtin.dmtin_category');

        $this->db->where('tbl_dmtin.dmtin_id', $id);

        $info = $this->db->get();

        return $info->row();
    }

    public function get_single_one_news($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_one_news');

        $this->db->where('tbl_one_news.one_news_id', $id);

        $info = $this->db->get();

        return $info->row();
    }



    public function get_single_dmtin_slug()
    {

        $this->db->select('*');

        $this->db->from('tbl_dmtin');

        $info = $this->db->get();

        return $info->result_array();
    }



    public function get_single_one_news_slug()
    {

        $this->db->select('*');

        $this->db->from('tbl_one_news');

        $info = $this->db->get();

        return $info->result_array();
    }



    public function get_all_category()
    {

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('publication_home', 1);

        $this->db->where('cate_status', 1);

        $this->db->where('parent_id', 0);

        $info = $this->db->get();

        return $info->result();
    }



    public function cate_xuhuong()
    {

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('publication_home', 1);

        $this->db->where('cate_status', 1);

        $this->db->where('parent_id', 0);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_child($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('publication_home', 0);

        $this->db->where('cate_status', 1);

        $this->db->where('parent_id', $id);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_child_menu($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('cate_status', 1);

        $this->db->where('parent_id', $id);

        $this->db->where('publication_header', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_cate_footer()
    {

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('publication_home', 1);

        $this->db->where('cate_status', 1);

        $this->db->where('parent_id', 0);

        $this->db->limit(4);

        $info = $this->db->get();

        return $info->result();
    }







    public function count_cate($id)
    {

        $this->db->select('count(*) as count');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->where('(tbl_product.product_category = ' . $id . ' or tbl_category.parent_id = ' . $id . ')');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->order_by('tbl_product.product_id', 'asc');



        $info = $this->db->get();

        return $info->row();
    }





    public function count_cate_c2($c2)
    {

        $this->db->select('count(*) as count');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->where('(tbl_product.product_category = ' . $c2 . ' or tbl_category.parent_id = ' . $c2 . ')');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->order_by('tbl_product.product_id', 'asc');



        $info = $this->db->get();

        return $info->row();
    }



    public function get_cate_tiennghi()
    {

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('cate_status', 1);

        $this->db->where('publication_home', 1);

        $this->db->where('id', 130);

        $info = $this->db->get();

        return $info->result();
    }

    public function get_cate_design()
    {

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('cate_status', 1);

        $this->db->where('publication_home', 1);

        $this->db->where('id', 131);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_cate_library_photo()
    {

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('cate_status', 1);

        $this->db->where('publication_home', 1);

        $this->db->where('id', 132);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_all_brand()
    {

        $this->db->select('*');

        $this->db->from('tbl_brand');

        $this->db->where('brand_publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_all_brand_product()
    {

        $this->db->select('*');

        $this->db->from('tbl_brand');

        $this->db->where('brand_publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }





    public function get_all_xuatxu()
    {

        $this->db->select('*');

        $this->db->from('tbl_xuatxu');

        $this->db->where('publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_all_thanhphan()
    {

        $this->db->select('*');

        $this->db->from('tbl_thanhphan');

        $this->db->where('publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_all_congdung()
    {

        $this->db->select('*');

        $this->db->from('tbl_congdung');

        $this->db->where('publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_country_slug()
    {

        $this->db->select('*');

        $this->db->from('tbl_brand');

        $info = $this->db->get();

        return $info->result_array();
    }

    public function get_all_country()
    {

        $this->db->select('*');

        $this->db->from('tbl_brand');

        $this->db->where('brand_publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }





    public function get_category_slug()
    {

        $this->db->select('*');

        $this->db->from('tbl_category');

        $info = $this->db->get();

        return $info->result_array();
    }



    public function get_category_slug2()
    {

        $this->db->select('*');

        $this->db->from('tbl_category_level2');

        $info = $this->db->get();

        return $info->result_array();
    }





    public function get_category_slug3()
    {

        $this->db->select('*');

        $this->db->from('tbl_category_level3');

        $info = $this->db->get();

        return $info->result_array();
    }

    public function rs_count($product_id)
    {

        $this->db->select('count(*) as count');

        $this->db->from('tbl_comment_product');

        $this->db->where('tbl_comment_product.comment_product_id', $product_id);

        $info = $this->db->get();

        return $info->row();
    }



    public function rs_count_total_star($product_id)
    {

        $this->db->select('count(*) as count');

        $this->db->from('tbl_comment');

        $this->db->where('tbl_comment.comment_product_id', $product_id);

        $info = $this->db->get();

        return $info->row();
    }

    public function rs_comment_star($product_id)
    {

        $this->db->select('sum(comment_start) as sum');

        $this->db->from('tbl_comment');

        $this->db->where('tbl_comment.comment_product_id', $product_id);

        $info = $this->db->get();

        return $info->row();
    }



    public function group_star_5($product_id)
    {

        $this->db->select('count(comment_id) as id_star, comment_start');

        $this->db->from('tbl_comment');

        $this->db->where('tbl_comment.comment_product_id', $product_id);

        $this->db->where('tbl_comment.comment_start', '5');

        $this->db->group_by('tbl_comment.comment_start');

        $info = $this->db->get();

        return $info->row();
    }

    public function group_star_4($product_id)
    {

        $this->db->select('count(comment_id) as id_star, comment_start');

        $this->db->from('tbl_comment');

        $this->db->where('tbl_comment.comment_product_id', $product_id);

        $this->db->where('tbl_comment.comment_start', '4');

        $this->db->group_by('tbl_comment.comment_start');

        $info = $this->db->get();

        return $info->row();
    }



    public function group_star_3($product_id)
    {

        $this->db->select('count(comment_id) as id_star, comment_start');

        $this->db->from('tbl_comment');

        $this->db->where('tbl_comment.comment_product_id', $product_id);

        $this->db->where('tbl_comment.comment_start', '3');

        $this->db->group_by('tbl_comment.comment_start');

        $info = $this->db->get();

        return $info->row();
    }



    public function group_star_2($product_id)
    {

        $this->db->select('count(comment_id) as id_star, comment_start');

        $this->db->from('tbl_comment');

        $this->db->where('tbl_comment.comment_product_id', $product_id);

        $this->db->where('tbl_comment.comment_start', '2');

        $this->db->group_by('tbl_comment.comment_start');

        $info = $this->db->get();

        return $info->row();
    }



    public function group_star_1($product_id)
    {

        $this->db->select('count(comment_id) as id_star, comment_start');

        $this->db->from('tbl_comment');

        $this->db->where('tbl_comment.comment_product_id', $product_id);

        $this->db->where('tbl_comment.comment_start', '1');

        $this->db->group_by('tbl_comment.comment_start');

        $info = $this->db->get();

        return $info->row();
    }

    public function show_result_star($product_id)
    {

        $this->db->select('*');

        $this->db->from('tbl_comment');

        $this->db->where('tbl_comment.comment_product_id', $product_id);

        $this->db->where('tbl_comment.comment_show', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function show_result_comment($product_id)
    {

        $this->db->select('*');

        $this->db->from('tbl_comment_product');

        $this->db->where('tbl_comment_product.comment_product_id', $product_id);

        $this->db->where('tbl_comment_product.comment_show', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function save_comment_product($data)

    {

        $this->db->insert('tbl_comment_product', $data);

        return $this->db->insert_id();
    }



    public function save_dk_tv_tour_model($data)

    {

        $this->db->insert('tbl_muanhanh', $data);

        return $this->db->insert_id();
    }

    public function get_all_category_dmtin()
    {

        $this->db->select('*');

        $this->db->from('tbl_category_dmtin');

        $this->db->where('publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }

    public function get_category_dmtin_slug()
    {

        $this->db->select('*');

        $this->db->from('tbl_category_dmtin');

        $this->db->where('publication_status', 1);

        $info = $this->db->get();

        return $info->result_array();
    }









    public function rs_id()
    {

        $this->db->select('*');

        $this->db->from('tbl_option');

        $info = $this->db->get();

        return $info->row();
    }







    public function category_dmtin_show_menu()
    {

        $get_id = $this->db->query("select * from tbl_category_dmtin where publication_status =1 and publication_header=1  order by  stt asc  ");

        $rs_get_dmtin = $get_id->result();

        return $rs_get_dmtin;
    }



    public function category_news()
    {

        $query = $this->db->query("select * from tbl_category_dmtin where publication_status =1 and parent_id =76");

        $rs_child = $query->result();

        return $rs_child;
    }



    public function get_all_product_by_cat($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->order_by('tbl_product.product_id', 'DESC');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->where('tbl_category.id', $id);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_id_catelogy($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('tbl_category.cate_status', 1);

        $this->db->where('tbl_category.id', $id);



        $info = $this->db->get();

        return $info->row();
    }



    public function get_id_country($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_brand');

        $this->db->where('tbl_brand.brand_publication_status', 1);

        $this->db->where('tbl_brand.brand_id', $id);

        $info = $this->db->get();

        return $info->row();
    }





    public function fetch_data_dmtin_by_cat($limit, $start, $id)
    {

        $this->db->limit($limit, $start);

        $this->db->select('*');

        $this->db->from('tbl_dmtin');

        $this->db->join('tbl_category_dmtin', 'tbl_category_dmtin.id=tbl_dmtin.dmtin_category', 'left');

        $this->db->order_by('tbl_dmtin.dmtin_id', 'DESC');

        $this->db->where('tbl_dmtin.publication_status', 1);

        $this->db->where('(tbl_dmtin.dmtin_category = ' . $id . ' or tbl_category_dmtin.parent_id = ' . $id . ')');

        $info = $this->db->get();

        return $info->result();
    }



    public function get_all_dmtin_by_cat($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_dmtin');

        $this->db->join('tbl_category_dmtin', 'tbl_category_dmtin.id=tbl_dmtin.dmtin_category');

        $this->db->order_by('tbl_dmtin.dmtin_id', 'DESC');

        $this->db->where('tbl_dmtin.publication_status', 1);

        $this->db->where('tbl_category_dmtin.id', $id);

        $info = $this->db->get();

        return $info->result();
    }





    public function get_id_catelogy_dmtin($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_category_dmtin');

        $this->db->where('tbl_category_dmtin.publication_status', 1);

        $this->db->where('tbl_category_dmtin.id', $id);

        $info = $this->db->get();

        return $info->row();
    }



    public function get_product_by_id($id)
    {



        $this->db->select('*');

        $this->db->distinct();

        $this->db->from('tbl_product');

        $this->db->join('tbl_pro_center', 'tbl_pro_center.pro_product_id=tbl_product.product_id', 'left');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_pro_center.pro_catelogy_id', 'left');



        $this->db->where('tbl_product.product_id', $id);

        $this->db->order_by('tbl_product.product_id', 'DESC');



        $this->db->where('tbl_product.publication_status', 1);

        $info = $this->db->get();



        return $info->row();
    }

    public function save_customer_info($data)
    {

        $this->db->insert('tbl_customer', $data);

        return $this->db->insert_id();
    }



    public function save_recentlyViewed($id)
    {

        $this->db->insert('tbl_products_preview', $id);

        return $this->db->insert_id();
    }



    public function get_recentlyViewed($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->where('tbl_product.product_id', $id);

        $info = $this->db->get();

        return $info->result();
    }





    public function update_user($data, $customer_id)
    {

        $this->db->where('customer_id', $customer_id);

        return $this->db->update('tbl_customer', $data);
    }





    public function save_contact($data)

    {

        $this->db->insert('tbl_contact', $data);

        return $this->db->insert_id();
    }



    public function save_regiter_mail($data)

    {

        $this->db->insert('tbl_regiter_mail', $data);

        return $this->db->insert_id();
    }



    public function save_regiter_tour($data)

    {

        $this->db->insert('tbl_regiter_tour', $data);

        return $this->db->insert_id();
    }



    public function save_comment($data)

    {

        $this->db->insert('tbl_comment', $data);

        return $this->db->insert_id();
    }



    public function save_comment_news_rate($data)

    {

        $this->db->insert('tbl_comment_news_rate', $data);

        return $this->db->insert_id();
    }

    public function save_comment_news($data)

    {

        $this->db->insert('tbl_comment_news', $data);

        return $this->db->insert_id();
    }





    public function save_request($data)

    {

        $this->db->insert('tbl_request', $data);

        return $this->db->insert_id();
    }



    public function save_shipping_address($data)
    {

        $this->db->insert('tbl_shipping', $data);

        return $this->db->insert_id();
    }



    public function get_customer_info($email, $password)
    {

        $this->db->select('*');

        $this->db->from('tbl_customer');

        $this->db->group_start();

        $this->db->where('customer_email', $email)->or_where('customer_phone', $email);

        $this->db->group_end();

        $this->db->where('customer_password', $password);

        $query = $this->db->get();

        return $result = $query->row();
    }



    public function get_customer_forgetpass_info($email_p)
    {

        $this->db->select('*');

        $this->db->from('tbl_customer');

        $this->db->where('customer_email', $email_p);

        $query = $this->db->get();

        return $result = $query->row();
    }









    public function save_order_info($data)
    {

        $this->db->insert('tbl_order', $data);

        return $this->db->insert_id();
    }



    public function save_order_details_info($oddata)
    {

        $this->db->insert('tbl_order_details', $oddata);
    }



    public function get_all_slider_post()
    {

        $this->db->select('*');

        $this->db->from('tbl_slider');

        $this->db->where('publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }

    public function get_all_chinhsach_post()
    {

        $this->db->select('*');

        $this->db->from('tbl_chinhsach');

        $this->db->where('publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_all_post_1baiviet($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_one_news');

        $this->db->where('publication_status', 1);

        $this->db->where('id_cate_page', $id);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_all_feel_post()
    {

        $this->db->select('*');

        $this->db->from('tbl_feel');

        $this->db->where('publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }

    public function get_all_avd_post()
    {

        $this->db->select('*');

        $this->db->from('tbl_avd');

        $this->db->where('publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_all_banner_center()
    {

        $this->db->select('*');

        $this->db->from('tbl_avd');

        $this->db->where('publication_status', 1);

        $this->db->where('tbl_avd.id_cate_page', 1);

        $info = $this->db->get();

        return $info->result();
    }

    public function get_all_banner_footer()
    {

        $this->db->select('*');

        $this->db->from('tbl_avd');

        $this->db->where('publication_status', 1);

        $this->db->where('tbl_avd.id_cate_page', 6);

        $info = $this->db->get();

        return $info->result();
    }

    public function get_all_banner_uudai()
    {

        $this->db->select('*');

        $this->db->from('tbl_avd');

        $this->db->where('publication_status', 1);

        $this->db->where('tbl_avd.id_cate_page', 2);

        $info = $this->db->get();

        return $info->row();
    }

    public function get_all_banner_tindung()
    {

        $this->db->select('*');

        $this->db->from('tbl_avd');

        $this->db->where('publication_status', 1);

        $this->db->where('tbl_avd.id_cate_page', 3);

        $info = $this->db->get();

        return $info->row();
    }



    public function get_all_banner_detailsp()
    {

        $this->db->select('*');

        $this->db->from('tbl_avd');

        $this->db->where('publication_status', 1);

        $this->db->where('tbl_avd.id_cate_page', 4);

        $info = $this->db->get();

        return $info->result();
    }

    public function get_all_banner_detailnews($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_avd');

        $this->db->where('publication_status', 1);

        $this->db->where('tbl_avd.id_cate_page', $id);

        $info = $this->db->get();

        return $info->result();
    }







    public function get_all_avd_brand_post()
    {

        $this->db->select('*');

        $this->db->from('tbl_brand');

        $this->db->where('brand_publication_status', 1);

        $this->db->order_by('tbl_brand.brand_stt', 'asc');

        $info = $this->db->get();

        return $info->result();
    }



    public function get_all_one_news_post()
    {

        $this->db->select('*');

        $this->db->from('tbl_one_news');

        $this->db->where('publication_status', 1);

        $this->db->where('publication_header', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_all_one_news_post_home($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_one_news');

        $this->db->where('publication_status', 1);

        $this->db->where('one_news_id', $id);

        $info = $this->db->get();

        return $info->row();
    }



    public function get_one_news_tongquan()
    {

        $this->db->select('*');

        $this->db->from('tbl_one_news');

        $this->db->where('publication_status', 1);

        $this->db->where('one_news_id', 10);

        $info = $this->db->get();

        return $info->row();
    }



    public function get_one_news_about_home()
    {

        $this->db->select('*');

        $this->db->from('tbl_one_news');

        $this->db->where('publication_status', 1);

        $this->db->where('one_news_id', 16);

        $info = $this->db->get();

        return $info->row();
    }



    public function get_one_news_banner_about_home()
    {

        $this->db->select('*');

        $this->db->from('tbl_one_news');

        $this->db->where('publication_status', 1);

        $this->db->where('one_news_id', 17);

        $info = $this->db->get();

        return $info->row();
    }



    public function get_one_news_chinhsach()
    {

        $this->db->select('*');

        $this->db->from('tbl_one_news');

        $this->db->where('publication_status', 1);

        $this->db->where('one_news_id', 18);

        $info = $this->db->get();

        return $info->row();
    }



    public function get_one_news_address()
    {

        $this->db->select('*');

        $this->db->from('tbl_one_news');

        $this->db->where('publication_status', 1);

        $this->db->where('one_news_id', 19);

        $info = $this->db->get();

        return $info->row();
    }



    public function get_about_wordshop()
    {

        $this->db->select('*');

        $this->db->from('tbl_one_news');

        $info = $this->db->get();

        return $info->result();
    }

    public function get_all_partner_post()
    {

        $this->db->select('*');

        $this->db->from('tbl_partner');

        $this->db->where('publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_all_popular_posts()
    {

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->where('publication_status', 1);

        $this->db->limit(4);

        $info = $this->db->get();

        return $info->result();
    }


    public function get_all_search_tour($search)
    {
        $this->db->select('*');
        $this->db->distinct();

        $this->db->from('tbl_product');
        $this->db->join('tbl_pro_center', 'tbl_pro_center.pro_product_id=tbl_product.product_id', 'left');
        $this->db->join('tbl_category', 'tbl_category.id=tbl_pro_center.pro_catelogy_id', 'left');
        $this->db->group_by('tbl_product.product_id', 'product_id');
        $this->db->order_by('tbl_product.product_id', 'DESC');
        $this->db->where('tbl_product.publication_status', 1);
        $this->db->like('tbl_product.product_title', $search, 'both');
        $info = $this->db->get();
        return $info->result();
    }

    public function search_tour_from_to_day($search, $from_day, $to_day)
    {
        $this->db->select('*');
        $this->db->distinct();

        $this->db->from('tbl_product');
        $this->db->join('tbl_pro_center', 'tbl_pro_center.pro_product_id=tbl_product.product_id', 'left');
        $this->db->join('tbl_category', 'tbl_category.id=tbl_pro_center.pro_catelogy_id', 'left');
        $this->db->join('tbl_depart', 'tbl_depart.product_id=tbl_product.product_id', 'left');
        $this->db->group_by('tbl_depart.product_id', 'product_id');

        $this->db->where('tbl_product.publication_status', 1);
        $this->db->like('tbl_product.product_title', $search, 'both');
        $this->db->where("DATE(tbl_depart.date_begin) BETWEEN '" . $from_day . "' AND '" . $to_day . "'");
        $this->db->order_by('tbl_product.product_id', 'DESC');

        $info = $this->db->get();
        return $info->result();
    }


    public function search_tour_from_to_day_not_title($from_day, $to_day)
    {

        $this->db->select('*');
        $this->db->distinct();

        $this->db->from('tbl_product');
        $this->db->join('tbl_pro_center', 'tbl_pro_center.pro_product_id=tbl_product.product_id', 'left');
        $this->db->join('tbl_category', 'tbl_category.id=tbl_pro_center.pro_catelogy_id', 'left');
        $this->db->join('tbl_depart', 'tbl_depart.product_id=tbl_product.product_id', 'left');
        $this->db->group_by('tbl_depart.product_id', 'product_id');

        $this->db->where('tbl_product.publication_status', 1);
        $this->db->where("DATE(tbl_depart.date_begin) BETWEEN '" . $from_day . "' AND '" . $to_day . "'");
        $this->db->order_by('tbl_product.product_id', 'DESC');
        $info = $this->db->get();
        return $info->result();
    }

    public function get_all_search_fiter_product($thanhphan, $congdung, $xuatxu)
    {

        $this->db->select('*');
        $this->db->from('tbl_product');
        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->order_by('tbl_product.product_id', 'DESC');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->like('tbl_product.product_thanhphan', $thanhphan, 'both');

        $this->db->like('tbl_product.product_congdung', $congdung, 'both');

        $this->db->or_like('tbl_product.product_xuatxu', $xuatxu, 'both');

        $info = $this->db->get();

        return $info->result();
    }





    public function get_product_view($get_id_list)
    {

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->order_by('tbl_product.product_id', 'asc');

        $this->db->where('tbl_product.publication_status', 1);

        $this->db->where_in('tbl_product.product_id', $get_id_list);

        $info = $this->db->get();

        return $info->result();
    }

    public function desc_brand($get_id_brand)
    {

        $this->db->select('*');

        $this->db->from('tbl_brand');

        $this->db->where('tbl_brand.brand_publication_status', 1);

        $this->db->where('tbl_brand.brand_id', $get_id_brand);

        $info = $this->db->get();

        return $info->row();
    }



    public function load_head_brand($get_id)
    {

        $this->db->select('*');

        $this->db->from('tbl_brand');

        $this->db->where('tbl_brand.brand_publication_status', 1);

        $this->db->where('tbl_brand.brand_id', $get_id);



        $info = $this->db->get();

        return $info->row();
    }

    public function load_head_cate_product($get_id_cate)
    {

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('tbl_category.cate_status', 1);

        $this->db->where('tbl_category.id', $get_id_cate);



        $info = $this->db->get();

        return $info->row();
    }

    public function load_id_parent($id_parent)
    {

        $this->db->select('*');

        $this->db->from('tbl_category');

        $this->db->where('tbl_category.cate_status', 1);

        $this->db->where('tbl_category.id', $id_parent);

        $info = $this->db->get();

        return $info->row();
    }



    public function get_info_member($customer_id)
    {

        $query = $this->db->query("select * from tbl_customer where  customer_id='" . $customer_id . "'");

        $rs_member = $query->row();

        return $rs_member;
    }

    public function get_product()
    {

        $query = $this->db->query("select * from tbl_product ");

        $rs_product = $query->result();

        return $rs_product;
    }



    public function orderdetails_member($customer_id)
    {

        $this->db->select('*');

        $this->db->from('tbl_order');

        $this->db->join('tbl_customer', 'tbl_customer.customer_id = tbl_order.cus_id', 'left');

        $this->db->where('tbl_order.cus_id', $customer_id);

        $this->db->order_by('tbl_order.order_id', 'desc');

        $result = $this->db->get();

        return $result->result();
    }



    public function orderdetails_member_detail($customer_id, $order_id)
    {

        $this->db->select('*');

        $this->db->from('tbl_order_details');

        $this->db->join('tbl_order', 'tbl_order.order_id = tbl_order_details.order_id', 'left');

        $this->db->join('tbl_customer', 'tbl_customer.customer_id = tbl_order.cus_id', 'left');

        $this->db->join('tbl_product', 'tbl_product.product_id = tbl_order_details.product_id_order', 'left');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->where('tbl_order.cus_id', $customer_id);

        $this->db->where('tbl_order.order_id', $order_id);

        $result = $this->db->get();

        return $result->result();
    }



    public function order_total_info_by_id_member($order_id)
    {

        $this->db->select('*');

        $this->db->from('tbl_order');

        $this->db->where('order_id', $order_id);

        $result = $this->db->get();

        return $result->row();
    }



    public function order_history($customer_id)
    {

        $this->db->select('*');

        $this->db->select_sum('order_total');

        $this->db->from('tbl_order');

        $this->db->join('tbl_customer', 'tbl_customer.customer_id = tbl_order.cus_id', 'left');

        $this->db->where('tbl_order.cus_id', $customer_id);

        $actions = array('Chưa thanh toán', 'Hủy', 'Đơn hàng mới');

        $this->db->where_not_in('tbl_order.actions', $actions);



        $result = $this->db->get();

        return $result->row();
    }





    public function show_price_ship()
    {

        $this->db->select('*');

        $this->db->from('tbl_order');

        $this->db->join('tbl_shipping', 'tbl_shipping.shipping_id = tbl_order.shipping_id');

        $this->db->order_by('tbl_order.order_id', 'asc');



        $info = $this->db->get();

        return $info->row();
    }



    public function order_info_by_id($order_id)
    {

        $this->db->select('*');

        $this->db->from('tbl_order');

        $this->db->where('order_id', $order_id);

        $result = $this->db->get();

        return $result->row();
    }



    public function get_news_noibat_home($id_dm_news, $limit_news, $order_news)

    {

        $this->db->select('*,tbl_dmtin.publication_status as pstatus');

        $this->db->from('tbl_dmtin');

        $this->db->join('tbl_category_dmtin', 'tbl_category_dmtin.id=tbl_dmtin.dmtin_category');



        $this->db->order_by('tbl_dmtin.dmtin_id', $order_news);

        $this->db->where('tbl_dmtin.publication_status', 1);

        $this->db->where('(tbl_dmtin.dmtin_category = ' . $id_dm_news . ' or tbl_category_dmtin.parent_id = ' . $id_dm_news . ')');



        $this->db->limit($limit_news);



        $info = $this->db->get();

        return $info->result();
    }



    public function get_hinhanhlq($id_dm_news, $limit_news, $order_news, $id_dd)

    {

        $this->db->select('*,tbl_dmtin.publication_status as pstatus');

        $this->db->from('tbl_dmtin');

        $this->db->join('tbl_category_dmtin', 'tbl_category_dmtin.id=tbl_dmtin.dmtin_category');



        $this->db->order_by('tbl_dmtin.dmtin_id', $order_news);

        $this->db->where('tbl_dmtin.publication_status', 1);

        $this->db->where('(tbl_dmtin.dmtin_category = ' . $id_dm_news . ' or tbl_category_dmtin.parent_id = ' . $id_dm_news . ')');



        $this->db->where('tbl_dmtin.id_dd', $id_dd);

        $this->db->limit($limit_news);



        $info = $this->db->get();

        return $info->result();
    }





    public function get_tour_lq($id)

    {

        $this->db->select('*');

        $this->db->from('tbl_product');

        $this->db->join('tbl_category', 'tbl_category.id=tbl_product.product_category', 'left');

        $this->db->order_by('tbl_product.product_id', 'asc');

        $this->db->where('tbl_product.product_diemden', $id);

        $this->db->where('publication_status', 1);

        $info = $this->db->get();

        return $info->result();
    }



    public function get_news_top_home($id_dm_news, $limit_news, $order_news)
    {
        $this->db->select('*,tbl_dmtin.publication_status as pstatus');
        $this->db->from('tbl_dmtin');
        $this->db->join('tbl_category_dmtin', 'tbl_category_dmtin.id=tbl_dmtin.dmtin_category');
        $this->db->order_by('tbl_dmtin.dmtin_id', $order_news);
        $this->db->where('tbl_dmtin.publication_status', 1);
        $this->db->where('tbl_dmtin.dmtin_feature', 1);
        $this->db->where('(tbl_dmtin.dmtin_category = ' . $id_dm_news . ' or tbl_category_dmtin.parent_id = ' . $id_dm_news . ')');
        $this->db->limit($limit_news);
        $info = $this->db->get();
        return $info->result();
    }



    public function price_ship($id)
    {

        $this->db->select('*');

        $this->db->from('tbl_city');

        $this->db->where('tbl_city.id', $id);

        $info = $this->db->get();

        return $info->row();
    }



    public function get_sum_orders($customer_id)
    {

        $this->db->select('*');

        $this->db->from('tbl_order');

        $this->db->select_sum('order_total');

        $this->db->join('tbl_customer', 'tbl_customer.customer_id=tbl_order.cus_id', 'left');

        $this->db->where('tbl_order.cus_id', $customer_id);

        $actions = array('Chưa thanh toán', 'Hủy', 'Đơn hàng mới');

        $this->db->where_not_in('tbl_order.actions', $actions);

        $info = $this->db->get();

        return $info->row();
    }



    public function get_all_city()
    {

        $this->db->select('*');

        $this->db->from('tbl_city');

        $this->db->order_by('tbl_city.id', 'DESC');

        $info = $this->db->get();

        return $info->row();
    }

    public function getall_hotline_info()
    {


        $this->db->select('*');


        $this->db->from('tbl_hotline');

        $info = $this->db->get();


        return $info->result();
    }
    public function getall_gallery()
    {

        $this->db->select('*');


        $this->db->from('tbl_one_news');


        $this->db->where('tbl_one_news.id_cate_page', 107);



        $info = $this->db->get();


        return $info->result();
    }
    public function getall_onenews_byCat($id)
    {

        $this->db->select('*');


        $this->db->from('tbl_one_news');


        $this->db->where('tbl_one_news.id_cate_page', $id);



        $info = $this->db->get();


        return $info->result();
    }
}
