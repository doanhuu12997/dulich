<?php

class Color_model extends CI_Model{
    public function save_color_info($data){
        return $this->db->insert('tbl_color', $data);
    }
    
    public function getall_color_info(){
        $this->db->select('*');
        $this->db->from('tbl_color');
        $info = $this->db->get();
        return $info->result();
    }
    
    
    public function edit_color_info($id){
        $this->db->select('*');
        $this->db->from('tbl_color');
        $this->db->where('color_id',$id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function delete_color_info($id){
        $this->db->where('color_id', $id);
        return $this->db->delete('tbl_color');
        
    }
    
    public function update_color_info($data,$id){
        $this->db->where('color_id', $id);
        return $this->db->update('tbl_color', $data);
    }

    public function published_color_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('color_id', $id);
        return $this->db->update('tbl_color');
    }
    
    public function unpublished_color_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('color_id', $id);
        return $this->db->update('tbl_color');
    }
    
}