<?php

class Code_model extends CI_Model{
    public function save_code_info($data){
        return $this->db->insert('tbl_discount_codes', $data);
    }
    
    public function getall_code_info(){
        $this->db->select('*');
        $this->db->from('tbl_discount_codes');
        $info = $this->db->get();
        return $info->result();
    }
    
    
    public function edit_code_info($id){
        $this->db->select('*');
        $this->db->from('tbl_discount_codes');
        $this->db->where('id',$id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function delete_code_info($id){
        $this->db->where('id', $id);
        return $this->db->delete('tbl_discount_codes');
        
    }
    
    public function update_code_info($data,$id){
        $this->db->where('id', $id);
        return $this->db->update('tbl_discount_codes', $data);
    }

   
    public function published_code_info($id) {
        $this->db->set('publication_status', 1);
        $this->db->where('id', $id);
        return $this->db->update('tbl_discount_codes');
    }
    
    public function unpublished_code_info($id) {
        $this->db->set('publication_status', 0);
        $this->db->where('id', $id);
        return $this->db->update('tbl_discount_codes');
    }
    
}