<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Congdung extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }

    public function add_congdung() {
        $data= array();
        $data['maincontent']= $this->load->view('admin/pages/congdung/add_congdung','',true);
        $this->load->view('admin/master',$data);
    }

    public function manage_congdung() {
        $data= array();
        $data['all_congdung'] = $this->congdung_model->getall_congdung_info();
        $data['maincontent']= $this->load->view('admin/pages/congdung/manage_congdung',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function save_congdung(){
        $data = array();
        $data['congdung_name']=$this->input->post('congdung_name');
        $data['congdung_description']=$this->input->post('congdung_description');
        $data['publication_status']=$this->input->post('publication_status');
        $congdung_slug=$this->input->post('congdung_name');
        $data['congdung_slug'] =create_slug($congdung_slug);

           if (!empty($_FILES['congdung_image']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('congdung_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/congdung');
            }
            else{
                $post_image = $this->upload->data();
               $data['congdung_image'] = $post_image['file_name'];
            }
        }
            $result = $this->congdung_model->save_congdung_info($data);
            if($result){
                 $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> Điểm đến Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/congdung');   
            }
            else{
                $this->session->set_flashdata('message','congdung Inserted Failed');
                redirect('manage/congdung');
            }
        
   
        
    }
    
    public function delete_congdung($id){
        $result = $this->congdung_model->delete_congdung_info($id);
        if ($result) {
             $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> congdung Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/congdung');   
        } else {
            $this->session->set_flashdata('message', 'congdung Deleted Failed');
             redirect('manage/congdung');
        }
    }
    
     public function edit_congdung($id){
        $data= array();
        $data['congdung_info_by_id'] = $this->congdung_model->edit_congdung_info($id);
        $data['maincontent']= $this->load->view('admin/pages/congdung/edit_congdung',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function update_congdung($id){
        $data = array();
        $data['congdung_name']=$this->input->post('congdung_name');
        $data['congdung_description']=$this->input->post('congdung_description');
        $data['publication_status']=$this->input->post('publication_status');
        $data['congdung_slug']=$this->input->post('congdung_slug');

         $congdung_delete_image = $this->input->post('congdung_delete_image');
         $delete_image = substr($congdung_delete_image, strlen(base_url()));
             if (!empty($_FILES['congdung_image']['name'])){
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('congdung_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/congdung');
            }
            else{
                $post_image = $this->upload->data();
                $data['congdung_image'] = $post_image['file_name'];
                unlink($delete_image);
            }
           }

     
            $result = $this->congdung_model->update_congdung_info($data,$id);
            if($result){
                $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> congdung Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/congdung');   
            }
            else{
                $this->session->set_flashdata('message','congdung Update Failed');
                redirect('manage/congdung');
            }
    
        
    }
    
    public function published_congdung($id){
        $result = $this->congdung_model->published_congdung_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published congdung Sucessfully');
            redirect('manage/congdung');
        } else {
            $this->session->set_flashdata('message', 'Published congdung  Failed');
             redirect('manage/congdung');
        }
    }
    
    public function unpublished_congdung($id){
        $result = $this->congdung_model->unpublished_congdung_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished congdung Sucessfully');
            redirect('manage/congdung');
        } else {
            $this->session->set_flashdata('message', 'UnPublished congdung  Failed');
             redirect('manage/congdung');
        }
    }
    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    
    

}
