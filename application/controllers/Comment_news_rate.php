<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Comment_news_rate extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    


    public function manage_comment_news_rate() {
        $data= array();
        $data['all_comment_news_rate'] = $this->comment_news_rate_model->getall_comment_news_rate_info();
        $data['maincontent']= $this->load->view('admin/pages/comment_news_rate/manage_comment_news_rate',$data,true);
        $this->load->view('admin/master',$data);
    }

    public function edit_comment_news_rate($id){
        $data= array();
        $data['comment_news_rate_info_by_id'] = $this->comment_news_rate_model->edit_comment_news_rate_info($id);
        $data['maincontent']= $this->load->view('admin/pages/comment_news_rate/edit_comment_news_rate',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function update_comment_news_rate($id){
        $data = array();
        $data['comment_news_rate_name'] = $this->input->post('comment_news_rate_name');
        $data['comment_news_rate_answer'] = $this->input->post('comment_news_rate_answer');
        $data['comment_news_rate_show'] = $this->input->post('comment_news_rate_show');
      
            $result = $this->comment_news_rate_model->update_comment_news_rate_info($data,$id);

            if ($result) {
               $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> comment_news_rate Updated Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/comment_news_rate');   
            } else {
                $this->session->set_flashdata('message', 'comment_news_rate Updated Failed');
                redirect('manage/comment_news_rate');
            }
     
        
    }
    
  
    public function delete_comment_news_rate($id){
        $result = $this->comment_news_rate_model->delete_comment_news_rate_info($id);
        if ($result) {
                 $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> comment_news_rate Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/comment_news_rate');      
        } else {
            $this->session->set_flashdata('message', 'comment_news_rate Deleted Failed');
             redirect('manage/comment_news_rate');
        }
    }

    public function comment_news_rate_show_answer($id){
        $result = $this->comment_news_rate_model->comment_news_rate_show_answer($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published comment_news_rate Sucessfully');
            redirect('manage/comment_news_rate');
        } else {
            $this->session->set_flashdata('message', 'Published comment_news_rate  Failed');
             redirect('manage/comment_news_rate');
        }
    }
    
    public function un_comment_news_rate_show_answer($id){
        $result = $this->comment_news_rate_model->un_comment_news_rate_show_answer($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished comment_news_rate Sucessfully');
            redirect('manage/comment_news_rate');
        } else {
            $this->session->set_flashdata('message', 'UnPublished comment_news_rate  Failed');
             redirect('manage/comment_news_rate');
        }
    }
    
    

   
    public function published_comment_news_rate($id){
        $result = $this->comment_news_rate_model->published_comment_news_rate_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published comment_news_rate Sucessfully');
            redirect('manage/comment_news_rate');
        } else {
            $this->session->set_flashdata('message', 'Published comment_news_rate  Failed');
             redirect('manage/comment_news_rate');
        }
    }
    
    public function unpublished_comment_news_rate($id){
        $result = $this->comment_news_rate_model->unpublished_comment_news_rate_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished comment_news_rate Sucessfully');
            redirect('manage/comment_news_rate');
        } else {
            $this->session->set_flashdata('message', 'UnPublished comment_news_rate  Failed');
             redirect('manage/comment_news_rate');
        }
    }
    

    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    

}
