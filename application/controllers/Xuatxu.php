<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Xuatxu extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }

    public function add_xuatxu() {
        $data= array();
        $data['maincontent']= $this->load->view('admin/pages/xuatxu/add_xuatxu','',true);
        $this->load->view('admin/master',$data);
    }

    public function manage_xuatxu() {
        $data= array();
        $data['all_xuatxu'] = $this->xuatxu_model->getall_xuatxu_info();
        $data['maincontent']= $this->load->view('admin/pages/xuatxu/manage_xuatxu',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function save_xuatxu(){
        $data = array();
        $data['xuatxu_name']=$this->input->post('xuatxu_name');
        $data['xuatxu_description']=$this->input->post('xuatxu_description');
        $data['publication_status']=$this->input->post('publication_status');
        $xuatxu_slug=$this->input->post('xuatxu_name');
        $data['xuatxu_slug'] =create_slug($xuatxu_slug);
          
            $result = $this->xuatxu_model->save_xuatxu_info($data);
            if($result){
                 $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> điểm xuất phát Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/xuatxu');   
            }
            else{
                $this->session->set_flashdata('message','điểm xuất phát Inserted Failed');
                redirect('manage/xuatxu');
            }
        
   
        
    }
    
    public function delete_xuatxu($id){
        $result = $this->xuatxu_model->delete_xuatxu_info($id);
        if ($result) {
             $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> điểm xuất phát Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/xuatxu');   
        } else {
            $this->session->set_flashdata('message', 'điểm xuất phát Deleted Failed');
             redirect('manage/xuatxu');
        }
    }
    
     public function edit_xuatxu($id){
        $data= array();
        $data['xuatxu_info_by_id'] = $this->xuatxu_model->edit_xuatxu_info($id);
        $data['maincontent']= $this->load->view('admin/pages/xuatxu/edit_xuatxu',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function update_xuatxu($id){
        $data = array();
        $data['xuatxu_name']=$this->input->post('xuatxu_name');
        $data['xuatxu_slug']=$this->input->post('xuatxu_slug');
        $data['xuatxu_description']=$this->input->post('xuatxu_description');

            $result = $this->xuatxu_model->update_xuatxu_info($data,$id);
            if($result){
                $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> điểm xuất phát Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/xuatxu');   
            }
            else{
                $this->session->set_flashdata('message','điểm xuất phát Update Failed');
                redirect('manage/xuatxu');
            }
    
        
    }
    
    public function published_xuatxu($id){
        $result = $this->xuatxu_model->published_xuatxu_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published điểm xuất phát Sucessfully');
            redirect('manage/xuatxu');
        } else {
            $this->session->set_flashdata('message', 'Published điểm xuất phát  Failed');
             redirect('manage/xuatxu');
        }
    }
    
    public function unpublished_xuatxu($id){
        $result = $this->xuatxu_model->unpublished_xuatxu_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished điểm xuất phát Sucessfully');
            redirect('manage/xuatxu');
        } else {
            $this->session->set_flashdata('message', 'UnPublished điểm xuất phát  Failed');
             redirect('manage/xuatxu');
        }
    }
    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    
    

}
