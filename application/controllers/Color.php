<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Color extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    
    
    public function add_color() {
        $data= array();
        $data['maincontent']= $this->load->view('admin/pages/Color/add_color','',true);
        $this->load->view('admin/master',$data);
    }

    public function manage_color() {
        $data= array();
        $data['all_categroy'] = $this->color_model->getall_color_info();
        $data['role_user']=$this->role_user();
        $data['maincontent']= $this->load->view('admin/pages/Color/manage_color',$data,true);
        $this->load->view('admin/master',$data);
    }
     public function role_user()
    {
         $query =$this->db->query("select * from tbl_user where  user_id='".$this->session->userdata('user_id')."'");
         $rs_role_user = $query->row();
         return $rs_role_user;

    }

       

    public function save_color(){

        $data = array();
        $data['color_name']=$this->input->post('color_name');
        $data['color_code']=$this->input->post('color_code');
        $data['color_description']=$this->input->post('color_description');
        $data['publication_status']=$this->input->post('publication_status');

        if (!empty($_FILES['color_image']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('color_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/color');
            }
            else{
                $post_image = $this->upload->data();
               $data['color_image'] = $post_image['file_name'];
            }
        }

         if (!empty($_FILES['color_img']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('color_img')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/color');
            }
            else{
                $post_image = $this->upload->data();
               $data['color_img'] = $post_image['file_name'];
            }
        }
        
        $result = $this->color_model->save_color_info($data);
      
            if($result){
                 $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> color Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/color');   
            }
            else{
                $this->session->set_flashdata('message','color Inserted Failed');
                redirect('manage/color');
            }
     
        
    }
    
    public function delete_color($id){
        $result = $this->color_model->delete_color_info($id);
       if($result){
               $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> color Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/color');   
            
            } else {
            $this->session->set_flashdata('message', 'color Deleted Failed');
             redirect('manage/color');
        }
    }
    
     public function edit_color($id){
        $data= array();
        $data['color_info_by_id'] = $this->color_model->edit_color_info($id);
        $data['maincontent']= $this->load->view('admin/pages/Color/edit_color',$data,true);
        $this->load->view('admin/master',$data);
    }

    
    public function update_color($id){
        $data = array();
        $data['color_name']=$this->input->post('color_name');
        $data['color_code']=$this->input->post('color_code');
        $data['color_description']=$this->input->post('color_description');
        $data['publication_status']=$this->input->post('publication_status');
        $color_delete_image = $this->input->post('color_delete_image');
        $delete_image = substr($color_delete_image, strlen(base_url()));

         $color_delete_img = $this->input->post('color_delete_img');
        $delete_img = substr($color_delete_img, strlen(base_url()));

             if (!empty($_FILES['color_image']['name'])){
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('color_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/color');
            }
            else{
                $post_image = $this->upload->data();
                $data['color_image'] = $post_image['file_name'];
                unlink($delete_image);
            }
           }


            if (!empty($_FILES['color_img']['name'])){
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('color_img')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/color');
            }
            else{
                $post_image = $this->upload->data();
                $data['color_img'] = $post_image['file_name'];
                unlink($delete_img);
            }
           }

            $result = $this->color_model->update_color_info($data,$id);
            if($result){
               $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> color Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/color');   
            }
            else{
                $this->session->set_flashdata('message','color Update Failed');
                redirect('manage/color');
            }
      
        
    }



    
    public function published_color($id){
        $result = $this->color_model->published_color_info($id);
        if ($result) {
            $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> Published color Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/color'); 
        } else {
            $this->session->set_flashdata('message', 'Published color  Failed');
             redirect('manage/color');
        }
    }
    
    public function unpublished_color($id){
        $result = $this->color_model->unpublished_color_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished color Sucessfully');
            redirect('manage/color');
        } else {
            $this->session->set_flashdata('message', 'UnPublished color  Failed');
             redirect('manage/color');
        }
    }
    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    

}
