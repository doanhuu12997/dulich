<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Thanhphan extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }

    public function add_thanhphan() {
        $data= array();
        $data['maincontent']= $this->load->view('admin/pages/thanhphan/add_thanhphan','',true);
        $this->load->view('admin/master',$data);
    }

    public function manage_thanhphan() {
        $data= array();
        $data['all_thanhphan'] = $this->thanhphan_model->getall_thanhphan_info();
        $data['maincontent']= $this->load->view('admin/pages/thanhphan/manage_thanhphan',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function save_thanhphan(){
        $data = array();
        $data['thanhphan_name']=$this->input->post('thanhphan_name');
        $data['thanhphan_description']=$this->input->post('thanhphan_description');
        $data['publication_status']=$this->input->post('publication_status');
        $thanhphan_slug=$this->input->post('thanhphan_name');
        $data['thanhphan_slug'] =create_slug($thanhphan_slug);

           if (!empty($_FILES['thanhphan_image']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('thanhphan_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/thanhphan');
            }
            else{
                $post_image = $this->upload->data();
               $data['thanhphan_image'] = $post_image['file_name'];
            }
        }
            $result = $this->thanhphan_model->save_thanhphan_info($data);
            if($result){
                 $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> từ khóa Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/thanhphan');   
            }
            else{
                $this->session->set_flashdata('message','từ khóa Inserted Failed');
                redirect('manage/thanhphan');
            }
        
   
        
    }
    
    public function delete_thanhphan($id){
        $result = $this->thanhphan_model->delete_thanhphan_info($id);
        if ($result) {
             $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> từ khóa Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/thanhphan');   
        } else {
            $this->session->set_flashdata('message', 'từ khóa Deleted Failed');
             redirect('manage/thanhphan');
        }
    }
    
     public function edit_thanhphan($id){
        $data= array();
        $data['thanhphan_info_by_id'] = $this->thanhphan_model->edit_thanhphan_info($id);
        $data['maincontent']= $this->load->view('admin/pages/thanhphan/edit_thanhphan',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function update_thanhphan($id){
        $data = array();
        $data['thanhphan_name']=$this->input->post('thanhphan_name');
        $data['thanhphan_description']=$this->input->post('thanhphan_description');
        $data['publication_status']=$this->input->post('publication_status');
        $data['thanhphan_slug']=$this->input->post('thanhphan_slug');

         $thanhphan_delete_image = $this->input->post('thanhphan_delete_image');
         $delete_image = substr($thanhphan_delete_image, strlen(base_url()));
             if (!empty($_FILES['thanhphan_image']['name'])){
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('thanhphan_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/thanhphan');
            }
            else{
                $post_image = $this->upload->data();
                $data['thanhphan_image'] = $post_image['file_name'];
                unlink($delete_image);
            }
           }

     
            $result = $this->thanhphan_model->update_thanhphan_info($data,$id);
            if($result){
                $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> từ khóa Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/thanhphan');   
            }
            else{
                $this->session->set_flashdata('message','từ khóa Update Failed');
                redirect('manage/thanhphan');
            }
    
        
    }
    
    public function published_thanhphan($id){
        $result = $this->thanhphan_model->published_thanhphan_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published từ khóa Sucessfully');
            redirect('manage/thanhphan');
        } else {
            $this->session->set_flashdata('message', 'Published từ khóa  Failed');
             redirect('manage/thanhphan');
        }
    }
    
    public function unpublished_thanhphan($id){
        $result = $this->thanhphan_model->unpublished_thanhphan_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished từ khóa Sucessfully');
            redirect('manage/thanhphan');
        } else {
            $this->session->set_flashdata('message', 'UnPublished từ khóa  Failed');
             redirect('manage/thanhphan');
        }
    }
    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    
    

}
