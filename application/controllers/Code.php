<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Code extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    
    
    public function add_code() {
        $data= array();
        $data['maincontent']= $this->load->view('admin/pages/Code/add_code','',true);
        $this->load->view('admin/master',$data);
    }

    public function manage_code() {
        $data= array();
        $data['all_categroy'] = $this->code_model->getall_code_info();
        $data['role_user']=$this->role_user();
        $data['maincontent']= $this->load->view('admin/pages/Code/manage_code',$data,true);
        $this->load->view('admin/master',$data);
    }
     public function role_user()
    {
         $query =$this->db->query("select * from tbl_user where  user_id='".$this->session->userdata('user_id')."'");
         $rs_role_user = $query->row();
         return $rs_role_user;

    }

   

    public function save_code(){

        $data = array();
        $this->load->helper('string');
        $data['type']=$this->input->post('type');
        $data['code']=$this->input->post('code');
        $data['code_auto']=random_string('alnum',9);
        $data['amount']=$this->input->post('amount');
        $data['valid_to_date']=$this->input->post('valid_to_date');
        $data['publication_status']=$this->input->post('publication_status');

        $result = $this->code_model->save_code_info($data);
      
            if($result){
                 $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> code Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/code');   
            }
            else{
                $this->session->set_flashdata('message','Code Inserted Failed');
                redirect('manage/code');
            }
     
        
    }
    
    public function delete_code($id){
        $result = $this->code_model->delete_code_info($id);
       if($result){
               $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> code Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/code');   
            
            } else {
            $this->session->set_flashdata('message', 'code Deleted Failed');
             redirect('manage/code');
        }
    }
    
     public function edit_code($id){
        $data= array();
        $data['code_info_by_id'] = $this->code_model->edit_code_info($id);
        $data['maincontent']= $this->load->view('admin/pages/Code/edit_code',$data,true);
        $this->load->view('admin/master',$data);
    }

    
    public function update_code($id){
        $data = array();
        $data['type']=$this->input->post('type');
        $data['code']=$this->input->post('code');
        $data['amount']=$this->input->post('amount');
        $data['valid_to_date']=$this->input->post('valid_to_date');
        $data['publication_status']=$this->input->post('publication_status');

        
            $result = $this->code_model->update_code_info($data,$id);
            if($result){
               $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> code Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/code');   
            }
            else{
                $this->session->set_flashdata('message','code Update Failed');
                redirect('manage/code');
            }
      
        
    }


    
    
    public function published_code($id){
        $result = $this->code_model->published_code_info($id);
        if ($result) {
            $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> Published code Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/code'); 
        } else {
            $this->session->set_flashdata('message', 'Published code  Failed');
             redirect('manage/code');
        }
    }
    
    public function unpublished_code($id){
        $result = $this->code_model->unpublished_code_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished code Sucessfully');
            redirect('manage/code');
        } else {
            $this->session->set_flashdata('message', 'UnPublished code  Failed');
             redirect('manage/code');
        }
    }
    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    

}
