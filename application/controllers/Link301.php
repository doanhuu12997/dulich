<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Link301 extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }

    public function add_link301() {
        $data= array();
        $data['maincontent']= $this->load->view('admin/pages/link301/add_link301','',true);
        $this->load->view('admin/master',$data);
    }

    public function manage_link301() {
        $data= array();
        $data['all_link301'] = $this->link301_model->getall_link301_info();
        $data['maincontent']= $this->load->view('admin/pages/link301/manage_link301',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function save_link301(){
        $data = array();
        $data['link301_nguon']=$this->input->post('link301_nguon');
        $data['link301_dich']=$this->input->post('link301_dich');
        $data['publication_status']=$this->input->post('publication_status');
       
          
            $result = $this->link301_model->save_link301_info($data);
            if($result){
                 $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> link301 Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/link301');   
            }
            else{
                $this->session->set_flashdata('message','link301 Inserted Failed');
                redirect('manage/link301');
            }
        
   
        
    }
    
    public function delete_link301($id){
        $result = $this->link301_model->delete_link301_info($id);
        if ($result) {
             $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> link301 Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/link301');   
        } else {
            $this->session->set_flashdata('message', 'link301 Deleted Failed');
             redirect('manage/link301');
        }
    }
    
     public function edit_link301($id){
        $data= array();
        $data['link301_info_by_id'] = $this->link301_model->edit_link301_info($id);
        $data['maincontent']= $this->load->view('admin/pages/link301/edit_link301',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function update_link301($id){
        $data = array();
        $data['link301_nguon']=$this->input->post('link301_nguon');
        $data['link301_dich']=$this->input->post('link301_dich');

            $result = $this->link301_model->update_link301_info($data,$id);
            if($result){
                $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> link301 Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/link301');   
            }
            else{
                $this->session->set_flashdata('message','link301 Update Failed');
                redirect('manage/link301');
            }
    
        
    }
    
    public function published_link301($id){
        $result = $this->link301_model->published_link301_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published link301 Sucessfully');
            redirect('manage/link301');
        } else {
            $this->session->set_flashdata('message', 'Published link301  Failed');
             redirect('manage/link301');
        }
    }
    
    public function unpublished_link301($id){
        $result = $this->link301_model->unpublished_link301_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished link301 Sucessfully');
            redirect('manage/link301');
        } else {
            $this->session->set_flashdata('message', 'UnPublished link301  Failed');
             redirect('manage/link301');
        }
    }
    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    
    

}
