<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Category extends CI_Controller {

    

    public function __construct() {

        parent::__construct();

        $this->get_user();

    }

    

    

    public function add_category() {

        $data= array();

        $data['all_categroy'] = $this->category_model->getcategory();

        $data['maincontent']= $this->load->view('admin/pages/Category_lv1/add_category',$data,true);

        $this->load->view('admin/master',$data);

    }



    public function manage_category() {

        $data= array();

        $data['all_categroy'] = $this->category_model->getcategory();



        $data['role_user']=$this->role_user();

        $data['maincontent']= $this->load->view('admin/pages/Category_lv1/manage_category',$data,true);

        $this->load->view('admin/master',$data);

    }

     public function role_user()

    {

         $query =$this->db->query("select * from tbl_user where  user_id='".$this->session->userdata('user_id')."'");

         $rs_role_user = $query->row();

         return $rs_role_user;



    }



         public function checkbox_ajax() {

         $color = $this->input->post('color');   

         $data = array('color' =>$color);

         echo "Bạn đã chọn" .$color;

          return $this->db->insert('tbl_category', $data);

        }



      function delete_catebanner() 

      { 

        $id=$this->input->post('id');

         $query= $this->db->query("update tbl_category set category_icon ='' where id='".$id."'");

           return $query; 

      } 



     function delete_catelv1() 

      { 

          $id=$this->input->post('id');

         $query= $this->db->query("update tbl_category set category_img ='' where id='".$id."'");

           return $query; 

      } 





    public function save_category(){



        $data = array();

        $data['category_name']=$this->input->post('category_name');

        $data['title']=$this->input->post('title');

        $category_slug=$this->input->post('category_name');

        $data['slug'] =create_slug($category_slug);

        $data['category_description']=$this->input->post('category_description');

        $data['category_description_seo']=$this->input->post('category_description_seo');

        $data['keywords']=$this->input->post('keywords');

        $data['cate_status']=$this->input->post('cate_status');
        $data['noibat']=$this->input->post('noibat');

        $data['parent_id']=$this->input->post('parent_id');

        $data['category_stt']=$this->input->post('category_stt');



        if (!empty($_FILES['category_icon']['name'])) {

            $config['upload_path'] = './uploads/';

            $config['allowed_types'] = '*';

            $config['max_size'] = 40964;

            $config['max_width'] = 50000;

            $config['max_height'] = 50000;



            $this->upload->initialize($config);



            if (!$this->upload->do_upload('category_icon')) {

                $error = $this->upload->display_errors();

                $this->session->set_flashdata('message', $error);

                redirect('add/category');

            }

            else{

                $post_image = $this->upload->data();

               $data['category_icon'] = $post_image['file_name'];

            }

        }



         if (!empty($_FILES['category_img']['name'])) {

            $config['upload_path'] = './uploads/';

            $config['allowed_types'] = '*';

            $config['max_size'] = 40964;

            $config['max_width'] = 50000;

            $config['max_height'] = 50000;



            $this->upload->initialize($config);



            if (!$this->upload->do_upload('category_img')) {

                $error = $this->upload->display_errors();

                $this->session->set_flashdata('message', $error);

                redirect('add/category');

            }

            else{

                $post_image = $this->upload->data();

               $data['category_img'] = $post_image['file_name'];

            }

        }

        

        $result = $this->category_model->save_category_info($data);

      

            if($result){

                 $this->session->set_flashdata('message','<div class="alert alert-success">

            <div class="toast_icon">

                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">

                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>

                </g></g>

                </svg>

                </div>

                  <strong>Success!</strong> Category Inserted Sucessfully

                  <button type="button" class="close" data-dismiss="alert">&times;</button>

                </div>');

                redirect('manage/category');   

            }

            else{

                $this->session->set_flashdata('message','Category Inserted Failed');

                redirect('manage/category');

            }

     

        

    }

    

    public function delete_category($id){

        $result = $this->category_model->delete_category_info($id);

       if($result){

               $this->session->set_flashdata('message','<div class="alert alert-success">

            <div class="toast_icon">

                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">

                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>

                </g></g>

                </svg>

                </div>

                  <strong>Success!</strong> Category Deleted Sucessfully

                  <button type="button" class="close" data-dismiss="alert">&times;</button>

                </div>');

                redirect('manage/category');   

            

            } else {

            $this->session->set_flashdata('message', 'Category Deleted Failed');

             redirect('manage/category');

        }

    }

    

     public function edit_category($id){

        $data= array();

        $data['category_info_by_id'] = $this->category_model->edit_category_info($id);

        $data['all_categroy'] = $this->category_model->getcategory();

        $data['maincontent']= $this->load->view('admin/pages/Category_lv1/edit_category',$data,true);

        $this->load->view('admin/master',$data);

    }



    

    public function update_category($id){

        $data = array();

        $data['category_name']=$this->input->post('category_name');

        $data['slug']=$this->input->post('slug');

        $data['title']=$this->input->post('title');

        $data['category_description']=$this->input->post('category_description');

        $data['keywords']=$this->input->post('keywords');

        $data['category_description_seo']=$this->input->post('category_description_seo');

        $data['cate_status']=$this->input->post('cate_status');
        $data['noibat']=$this->input->post('noibat');

        $data['parent_id']=$this->input->post('parent_id');

        $data['category_stt']=$this->input->post('category_stt');



        $category_delete_image = $this->input->post('category_delete_icon');

        $delete_image = substr($category_delete_image, strlen(base_url()));



         $category_delete_img = $this->input->post('category_delete_img');

        $delete_img = substr($category_delete_img, strlen(base_url()));



             if (!empty($_FILES['category_icon']['name'])){

            $config['upload_path'] = './uploads/';

            $config['allowed_types'] = '*';

            $config['max_size'] = 40964;

            $config['max_width'] = 50000;

            $config['max_height'] = 50000;



            $this->upload->initialize($config);



            if (!$this->upload->do_upload('category_icon')) {

                $error = $this->upload->display_errors();

                $this->session->set_flashdata('message', $error);

                redirect('add/category');

            }

            else{

                $post_image = $this->upload->data();

                $data['category_icon'] = $post_image['file_name'];

                unlink($delete_image);

            }

           }





            if (!empty($_FILES['category_img']['name'])){

            $config['upload_path'] = './uploads/';

            $config['allowed_types'] = '*';

            $config['max_size'] = 40964;

            $config['max_width'] = 50000;

            $config['max_height'] = 50000;



            $this->upload->initialize($config);



            if (!$this->upload->do_upload('category_img')) {

                $error = $this->upload->display_errors();

                $this->session->set_flashdata('message', $error);

                redirect('add/category');

            }

            else{

                $post_image = $this->upload->data();

                $data['category_img'] = $post_image['file_name'];

                unlink($delete_img);

            }

           }



            $result = $this->category_model->update_category_info($data,$id);

            if($result){

               $this->session->set_flashdata('message','<div class="alert alert-success">

            <div class="toast_icon">

                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">

                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>

                </g></g>

                </svg>

                </div>

                  <strong>Success!</strong> Category Update Sucessfully

                  <button type="button" class="close" data-dismiss="alert">&times;</button>

                </div>');

                redirect('manage/category');   

            }

            else{

                $this->session->set_flashdata('message','Category Update Failed');

                redirect('manage/category');

            }

      

        

    }







    public function published_home_category($id){

        $result = $this->category_model->published_home_category($id);

        if ($result) {

           $this->session->set_flashdata('message','<div class="alert alert-success">

            <div class="toast_icon">

                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">

                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>

                </g></g>

                </svg>

                </div>

                  <strong>Success!</strong> Published Published Sucessfully

                  <button type="button" class="close" data-dismiss="alert">&times;</button>

                </div>');

                redirect('manage/category');   

        } else {

            $this->session->set_flashdata('message', 'Published Published  Failed');

             redirect('manage/category');

        }

    }

    

    public function unpublished_home_category($id){

        $result = $this->category_model->unpublished_home_category($id);

        if ($result) {

          $this->session->set_flashdata('message', 'UnPublished Category Sucessfully');

            redirect('manage/category');

        } else {

            $this->session->set_flashdata('message', 'UnPublished Category  Failed');

             redirect('manage/category');

        }

    }





        public function published_header_category($id){

        $result = $this->category_model->published_header_category_info($id);

        if ($result) {

             $this->session->set_flashdata('message','<div class="alert alert-success">

            <div class="toast_icon">

                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">

                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>

                </g></g>

                </svg>

                </div>

                  <strong>Success!</strong> Published Published Sucessfully

                  <button type="button" class="close" data-dismiss="alert">&times;</button>

                </div>');

                redirect('manage/category');   

        } else {

            $this->session->set_flashdata('message', 'Published Category  Failed');

             redirect('manage/category');

        }

    }

    

    public function unpublished_header_category($id){

        $result = $this->category_model->unpublished_header_category_info($id);

        if ($result) {

            $this->session->set_flashdata('message', 'UnPublished Category Sucessfully');

            redirect('manage/category');

        } else {

            $this->session->set_flashdata('message', 'UnPublished Category  Failed');

             redirect('manage/category');

        }

    }

    



    public function published_footer_category($id){

        $result = $this->category_model->published_footer_category_info($id);

        if ($result) {

           $this->session->set_flashdata('message','<div class="alert alert-success">

            <div class="toast_icon">

                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">

                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>

                </g></g>

                </svg>

                </div>

                  <strong>Success!</strong> Published Published Sucessfully

                  <button type="button" class="close" data-dismiss="alert">&times;</button>

                </div>');

                redirect('manage/category');   

        } else {

            $this->session->set_flashdata('message', 'Published Category  Failed');

             redirect('manage/category');

        }

    }

    

    public function unpublished_footer_category($id){

        $result = $this->category_model->unpublished_footer_category_info($id);

        if ($result) {

            $this->session->set_flashdata('message','<div class="alert alert-success">

            <div class="toast_icon">

                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">

                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>

                </g></g>

                </svg>

                </div>

                  <strong>Success!</strong> UnPublished Published Sucessfully

                  <button type="button" class="close" data-dismiss="alert">&times;</button>

                </div>');

                redirect('manage/category');   

        } else {

            $this->session->set_flashdata('message', 'UnPublished Category  Failed');

             redirect('manage/category');

        }

    }



    

    public function published_category($id){

        $result = $this->category_model->published_category_info($id);

        if ($result) {

            $this->session->set_flashdata('message','<div class="alert alert-success">

                  <strong>Success!</strong> Published Category Sucessfully

                  <button type="button" class="close" data-dismiss="alert">&times;</button>

                </div>');

                redirect('manage/category'); 

        } else {

            $this->session->set_flashdata('message', 'Published Category  Failed');

             redirect('manage/category');

        }

    }

    

    public function unpublished_category($id){

        $result = $this->category_model->unpublished_category_info($id);

        if ($result) {

            $this->session->set_flashdata('message', 'UnPublished Category Sucessfully');

            redirect('manage/category');

        } else {

            $this->session->set_flashdata('message', 'UnPublished Category  Failed');

             redirect('manage/category');

        }

    }

    

    public function get_user(){

       

       $email = $this->session->userdata('user_email');

       $name = $this->session->userdata('user_name');

       $id = $this->session->userdata('user_id');

       

       if($email==false){

          redirect('admin'); 

       }

        

    }

    



}

