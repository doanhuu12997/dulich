<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_member_order extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    
    
    public function manage_member_order(){
        $data= array();
        $data['all_manage_member_info'] =$this->manager_member_order_model->manage_info();
        $data['get_ds_info_thuong'] =$this->manager_member_order_model->get_ds_info_thuong();
        $data['get_ds_info_bac'] =$this->manager_member_order_model->get_ds_info_bac();
        $data['get_ds_info_vang'] =$this->manager_member_order_model->get_ds_info_vang();
        $data['get_ds_info_kimcuong'] =$this->manager_member_order_model->get_ds_info_kimcuong();
        $data['maincontent']= $this->load->view('admin/pages/Member_order/manage_member_order',$data,true);
        $this->load->view('admin/master',$data);
    }
       public function edit_member($id){
        $data= array();
        $data['member_info_by_id'] = $this->manager_member_order_model->edit_member_info($id);
        $data['maincontent']= $this->load->view('admin/pages/Member_order/edit_member',$data,true);
        $this->load->view('admin/master',$data);
    }
      public function delete_member($id){
        $result = $this->manager_member_order_model->delete_member($id);
       if($result){
               $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> Order Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/member_order');   
            
            } else {
            $this->session->set_flashdata('message', 'Order Deleted Failed');
             redirect('manage/member_order');
        }
    }
    
    public function update_member($id){
        $data = array();
        $data['customer_name']=$this->input->post('customer_name');
        $data['customer_email']=$this->input->post('customer_email');
        $data['customer_address']=$this->input->post('customer_address');
        $data['customer_phone']=$this->input->post('customer_phone');

         $get_pass=$this->input->post('customer_password');

           if(empty($get_pass))
        {
            $xxx['customer_password'];

        }
        else{ 
         $xxx['customer_password'] =md5($get_pass);
          $this->manager_member_order_model->update_member_info($xxx,$id);

         }
      
        $result = $this->manager_member_order_model->update_member_info($data,$id);
            if($result){
                $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> role_user Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/member_order');   
            }
            else{
                $this->session->set_flashdata('message','role_user Update Failed');
                redirect('manage/member_order');
            }
      
        
    }


    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    
}
