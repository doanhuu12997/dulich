<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class partner extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    
    public function add_partner() {
        $data= array();
        $data['maincontent']= $this->load->view('admin/pages/Partner/add_partner','',true);
        $this->load->view('admin/master',$data);
    }

    public function manage_partner() {
        $data= array();
        $data['all_partner'] = $this->partner_model->getall_partner_info();
        $data['role_user']=$this->role_user();
        $data['maincontent']= $this->load->view('admin/pages/Partner/manage_partner',$data,true);
        $this->load->view('admin/master',$data);
    }
     public function role_user()
    {
         $query =$this->db->query("select * from tbl_user where  user_id='".$this->session->userdata('user_id')."'");
         $rs_role_user = $query->row();
         return $rs_role_user;

    }
     function delete_imgpartner() 
      { 
        $id=$this->input->post('id');
         $query= $this->db->query("update tbl_partner set partner_image ='' where partner_id='".$id."'");
        return $query; 
      } 
    public function save_partner() {
        $data = array();
        $data['partner_title'] = $this->input->post('partner_title');
        $data['partner_link'] = $this->input->post('partner_link');
        $data['publication_status'] = $this->input->post('publication_status');

   
        if (!empty($_FILES['partner_image']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 200023;
            $config['max_width'] = 200023;
            $config['max_height'] = 200023;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('partner_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/partner');
            }
            else{
                $post_image = $this->upload->data();
                $data['partner_image'] = $post_image['file_name'];
            }
        }
        
            
            $result = $this->partner_model->save_partner_info($data);

            if ($result) {
                 $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> partner Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/partner');   
            } else {
                $this->session->set_flashdata('message', 'partner Inserted Failed');
                redirect('manage/partner');
            }
       
    }
    
    public function delete_partner($id){
        $delete_image =$this->get_image_by_id($id);
        unlink('uploads/'.$delete_image->partner_image);
        $result = $this->partner_model->delete_partner_info($id);
        if ($result) {
            $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> partner Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/partner');   
        } else {
            $this->session->set_flashdata('message', 'partner Deleted Failed');
             redirect('manage/partner');
        }
    }
    
     public function edit_partner($id){
        $data= array();
        $data['partner_info_by_id'] = $this->partner_model->edit_partner_info($id);
        $data['maincontent']= $this->load->view('admin/pages/Partner/edit_partner',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function update_partner($id){
        $data = array();
        $data['partner_title'] = $this->input->post('partner_title');
        $data['partner_link'] = $this->input->post('partner_link');
        $data['publication_status'] = $this->input->post('publication_status');
        $delete_image = $this->input->post('partner_delete_image');
        
       

     
        if (!empty($_FILES['partner_image']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 200023;
            $config['max_width'] = 200023;
            $config['max_height'] = 200023;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('partner_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/partner');
            }
            else{
                $post_image = $this->upload->data();
                $data['partner_image'] = $post_image['file_name'];
                unlink('uploads/'.$delete_image);
            }
        }
        
            
            $result = $this->partner_model->update_partner_info($data,$id);

            if ($result) {
               $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> partner Updated Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/partner');   
            } else {
                $this->session->set_flashdata('message', 'partner Updated Failed');
                redirect('manage/partner');
            }
     
        
    }
    
    public function published_partner($id){
        $result = $this->partner_model->published_partner_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published partner Sucessfully');
            redirect('manage/partner');
        } else {
            $this->session->set_flashdata('message', 'Published partner  Failed');
             redirect('manage/partner');
        }
    }
    
    public function unpublished_partner($id){
        $result = $this->partner_model->unpublished_partner_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished partner Sucessfully');
            redirect('manage/partner');
        } else {
            $this->session->set_flashdata('message', 'UnPublished partner  Failed');
             redirect('manage/partner');
        }
    }
    
    private function get_image_by_id($id){
        $this->db->select('partner_image');
        $this->db->from('tbl_partner');
        $this->db->where('partner_id', $id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    

}
