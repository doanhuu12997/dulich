<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Avd extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    
    public function add_avd() {
        $data= array();
        $data['maincontent']= $this->load->view('admin/pages/Avd/add_avd','',true);
        $this->load->view('admin/master',$data);
    }

    public function manage_avd() {
        $data= array();
        $data['all_avd'] = $this->avd_model->getall_avd_info();
        $data['role_user']=$this->role_user();
        $data['maincontent']= $this->load->view('admin/pages/Avd/manage_avd',$data,true);
        $this->load->view('admin/master',$data);
    }
      public function role_user()
    {
         $query =$this->db->query("select * from tbl_user where  user_id='".$this->session->userdata('user_id')."'");
         $rs_role_user = $query->row();
         return $rs_role_user;

    }
    
    public function save_avd() {
        $data = array();
        $data['avd_title'] = $this->input->post('avd_title');
        $data['avd_link'] = $this->input->post('avd_link');
        $data['id_cate_page'] = $this->input->post('id_cate_page');
        $data['publication_status'] = $this->input->post('publication_status');

        if (!empty($_FILES['avd_image']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 30000;
            $config['max_width'] = 30000;
            $config['max_height'] = 30000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('avd_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/avd');
            }
            else{
                $post_image = $this->upload->data();
                $data['avd_image'] = $post_image['file_name'];
            }
        }
                    
            
            $result = $this->avd_model->save_avd_info($data);

            if ($result) {
                   $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> avd Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/avd');  
            } else {
                $this->session->set_flashdata('message', 'avd Inserted Failed');
                redirect('manage/avd');
            }
     
    }
    
    public function delete_avd($id){
        $delete_image =$this->get_image_by_id($id);
        unlink('uploads/'.$delete_image->avd_image);
        $result = $this->avd_model->delete_avd_info($id);
        if ($result) {
                 $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> avd Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/avd');  
        } else {
            $this->session->set_flashdata('message', 'avd Deleted Failed');
             redirect('manage/avd');
        }
    }
    
     public function edit_avd($id){
        $data= array();
        $data['avd_info_by_id'] = $this->avd_model->edit_avd_info($id);
        $data['maincontent']= $this->load->view('admin/pages/Avd/edit_avd',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function update_avd($id){
        $data = array();
        $data['avd_title'] = $this->input->post('avd_title');
        $data['avd_link'] = $this->input->post('avd_link');
        $data['id_cate_page'] = $this->input->post('id_cate_page');
        $data['publication_status'] = $this->input->post('publication_status');
        $delete_image = $this->input->post('avd_delete_image');
        if (!empty($_FILES['avd_image']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 30000;
            $config['max_width'] = 30000;
            $config['max_height'] = 30000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('avd_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/avd');
            }
            else{
                $post_image = $this->upload->data();
                $data['avd_image'] = $post_image['file_name'];
                unlink('uploads/'.$delete_image);
            }
        }
                    
            
            $result = $this->avd_model->update_avd_info($data,$id);

            if ($result) {
                $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> avd Updated Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/avd');  
            } else {
                $this->session->set_flashdata('message', 'avd Updated Failed');
                redirect('manage/avd');
            }
     
        
    }
    
    public function published_avd($id){
        $result = $this->avd_model->published_avd_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published avd Sucessfully');
            redirect('manage/avd');
        } else {
            $this->session->set_flashdata('message', 'Published avd  Failed');
             redirect('manage/avd');
        }
    }
    
    public function unpublished_avd($id){
        $result = $this->avd_model->unpublished_avd_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished avd Sucessfully');
            redirect('manage/avd');
        } else {
            $this->session->set_flashdata('message', 'UnPublished avd  Failed');
             redirect('manage/avd');
        }
    }
    
    private function get_image_by_id($id){
        $this->db->select('avd_image');
        $this->db->from('tbl_avd');
        $this->db->where('avd_id', $id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    

}
