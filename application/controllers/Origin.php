<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Origin extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }

    public function add_origin() {
        $data= array();
        $data['maincontent']= $this->load->view('admin/pages/origin/add_origin','',true);
        $this->load->view('admin/master',$data);
    }

    public function manage_origin() {
        $data= array();
        $data['all_origin'] = $this->origin_model->getall_origin_info();
        $data['maincontent']= $this->load->view('admin/pages/origin/manage_origin',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function save_origin(){
        $data = array();
        $data['origin_name']=$this->input->post('origin_name');
        $data['origin_sales']=$this->input->post('origin_sales');
        $data['origin_percent']=$this->input->post('origin_percent');

        $data['publication_status']=$this->input->post('publication_status');

          
            $result = $this->origin_model->save_origin_info($data);
            if($result){
                 $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> origin Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/origin');   
            }
            else{
                $this->session->set_flashdata('message','origin Inserted Failed');
                redirect('manage/origin');
            }
        
   
        
    }
    
    public function delete_origin($id){
        $result = $this->origin_model->delete_origin_info($id);
        if ($result) {
             $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> origin Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/origin');   
        } else {
            $this->session->set_flashdata('message', 'origin Deleted Failed');
             redirect('manage/origin');
        }
    }
    
     public function edit_origin($id){
        $data= array();
        $data['origin_info_by_id'] = $this->origin_model->edit_origin_info($id);
        $data['maincontent']= $this->load->view('admin/pages/origin/edit_origin',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function update_origin($id){
        $data = array();
        $data['origin_name']=$this->input->post('origin_name');
        $data['origin_sales']=$this->input->post('origin_sales');
        $data['origin_percent']=$this->input->post('origin_percent');
         
            $result = $this->origin_model->update_origin_info($data,$id);
            if($result){
                $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> origin Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/origin');   
            }
            else{
                $this->session->set_flashdata('message','origin Update Failed');
                redirect('manage/origin');
            }
    
        
    }
    
    public function published_origin($id){
        $result = $this->origin_model->published_origin_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published origin Sucessfully');
            redirect('manage/origin');
        } else {
            $this->session->set_flashdata('message', 'Published origin  Failed');
             redirect('manage/origin');
        }
    }
    
    public function unpublished_origin($id){
        $result = $this->origin_model->unpublished_origin_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished origin Sucessfully');
            redirect('manage/origin');
        } else {
            $this->session->set_flashdata('message', 'UnPublished origin  Failed');
             redirect('manage/origin');
        }
    }
    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    
    

}
