<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Web extends CI_Controller

{

	public function index()

	{

		$this->clear_all_cache();

		$data = array();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$data['get_chinhsach'] = $this->web_model->get_all_chinhsach_post();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['cate_xuhuong'] = $this->web_model->cate_xuhuong();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();



		$cats = $this->web_model->getcate();

		foreach ($cats as $ct) {

			$cat_id = $ct->id;
		}

		$data['categories'] = $this->web_model->getCategories($cat_id);
		$data['rs_id'] = $this->web_model->rs_id();
		$data['get_all_featured_product'] = $this->web_model->get_all_featured_product();
		$data['product_new'] = $this->web_model->product_new();
		$data['product_selling'] = $this->web_model->product_selling();
		$data['product_sale'] = $this->web_model->product_sale();
		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();
		$data['get_all_brand'] = $this->web_model->get_all_brand();
		$data['all_hotline'] = $this->web_model->getall_hotline_info();
		$data['gallerys'] = $this->web_model->getall_gallery();
		$this->load->view('web/inc/head', $data);
		$this->load->view('web/inc/header', $data);
		$this->load->view('web/pages/home', $data);
		$this->load->view('web/inc/footer', $data);
	}

	public function ajax_discount_codes()

	{

		$code = $this->input->post('code');

		$customer_id = $this->session->userdata('customer_id');

		$data = array();

		$query = $this->db->query("select * from tbl_discount_codes where tbl_discount_codes.code='" . $code . "' or tbl_discount_codes.code_auto='" . $code . "'  ");

		$rs_date_end = $query->row();

		if (!isset($rs_date_end->code)) {



			echo "Mã không tồn tại hoặc đã hết hạn! ";
		} else {

			$date_end = $rs_date_end->valid_to_date;

			$data["get_discount_codes"] = $this->web_model->discount_codes($code, $date_end);

			$data['get_ds_info_thuong'] = $this->web_model->get_ds_info_thuong();

			$data['get_ds_info_bac'] = $this->web_model->get_ds_info_bac();

			$data['get_ds_info_vang'] = $this->web_model->get_ds_info_vang();

			$data['get_ds_info_kimcuong'] = $this->web_model->get_ds_info_kimcuong();

			$data["get_sum_orders"] = $this->web_model->get_sum_orders($customer_id);

			$this->load->view('web/pages/ajax/ajax_discount_codes.php', $data);
		}
	}

	public function link301($id)

	{



		$fullurl = $_SERVER['REQUEST_URI'];

		$trim_full = trim($fullurl, "/");

		$query = $this->db->query("select * from tbl_link301 where link301_nguon = '$trim_full'");

		$rs_301 = $query->row();

		header('Location:' . $rs_301->link301_dich, true, 301);
	}

	public function thuonghieu()

	{



		$data = array();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$cats = $this->web_model->getcate();

		foreach ($cats as $ct) {

			$cat_id = $ct->id;
		}

		$data['categories'] = $this->web_model->getCategories($cat_id);

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['get_all_featured_product'] = $this->web_model->get_all_featured_product();

		$data['product_new'] = $this->web_model->product_new();

		$data['product_selling'] = $this->web_model->product_selling();

		$data['product_sale'] = $this->web_model->product_sale();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$this->load->view('web/inc/head_brand_all', $data);

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/brand_all', $data);

		$this->load->view('web/inc/footer', $data);
	}



	public function tourhot()

	{

		$data = array();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$cats = $this->web_model->getcate();

		foreach ($cats as $ct) {

			$cat_id = $ct->id;
		}

		$data['categories'] = $this->web_model->getCategories($cat_id);

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['get_all_featured_product'] = $this->web_model->get_all_featured_product();

		$data['tour_hot'] = $this->web_model->tour_hot();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$this->load->view('web/inc/head_deal', $data);

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/deal_hot', $data);

		$this->load->view('web/inc/footer', $data);
	}



	public function sp_moive()

	{

		$data = array();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$cats = $this->web_model->getcate();

		foreach ($cats as $ct) {

			$cat_id = $ct->id;
		}

		$data['categories'] = $this->web_model->getCategories($cat_id);

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['get_all_featured_product'] = $this->web_model->get_all_featured_product();

		$data['product_sp_moive'] = $this->web_model->product_sp_moive();

		$data['product_selling'] = $this->web_model->product_selling();

		$data['product_sale'] = $this->web_model->product_sale();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$this->load->view('web/inc/head_sp_moive', $data);

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/sp_moive', $data);

		$this->load->view('web/inc/footer', $data);
	}



	public function sp_banchay()

	{

		$data = array();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$cats = $this->web_model->getcate();

		foreach ($cats as $ct) {

			$cat_id = $ct->id;
		}

		$data['categories'] = $this->web_model->getCategories($cat_id);

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['get_all_featured_product'] = $this->web_model->get_all_featured_product();

		$data['product_new'] = $this->web_model->product_new();

		$data['product_selling'] = $this->web_model->product_selling();

		$data['product_sale'] = $this->web_model->product_sale();



		$data['product_sp_banchay'] = $this->web_model->product_sp_banchay();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$this->load->view('web/inc/head_sp_banchay', $data);

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/sp_banchay', $data);

		$this->load->view('web/inc/footer', $data);
	}

	public function clear_all_cache()

	{

		$CI = &get_instance();

		$path = $CI->config->item('cache_path');

		$cache_path = ($path == '') ? APPPATH . 'cache/' : $path;

		$handle = opendir($cache_path);

		while (($file = readdir($handle)) !== FALSE) {

			//Leave the directory protection alone



			if ($file != '.htaccess' && $file != 'index.html') {



				@unlink($cache_path . '/' . $file);
			}
		}



		closedir($handle);
	}

	public function load_ajax_product_search()

	{

		$search = $this->input->post('search');

		$data = array();

		$data['search'] = $search;

		if (empty($search)) {

			echo "";
		} else {

			$data["get_all_product"] = $this->web_model->ajax_search_product($search);

			$this->load->view('web/pages/ajax/ajax_search.php', $data);
		}
	}



	public function load_ajax_check_hd()



	{

		$mahd = $this->input->post('mahd');

		$data = array();

		$data['mahd'] = $mahd;

		if (empty($mahd)) {

			echo "";
		} else {

			$data['all_manage_order_info'] = $this->manageorder_model->manage_check_hd($mahd);

			$this->load->view('web/pages/ajax/ajax_check_hd.php', $data);
		}
	}



	public function load_ajax_product_c2()



	{

		$c2 = $this->input->post('c2');

		$data = array();

		$data["get_all_product"] = $this->web_model->load_ajax_product_c2($c2);

		$data["get_count_cate"] = $this->web_model->count_cate_c2($c2);

		$data['name_c2'] = $this->input->post('name_c2');

		$data['slug_c2'] = $this->input->post('slug_c2');

		$this->load->view('web/pages/ajax/ajax_product.php', $data);
	}



	public function load_product_view()



	{

		$product_id = $this->input->get('id_list');

		$productdata = $this->web_model->productView($product_id);



		if (array_key_exists('recentviewed', $_COOKIE)) {

			//already set

			$cookie_get = get_cookie('recentviewed');

			$cookieres = unserialize($cookie_get);

			///check product already present

			if (!in_array($product_id, $cookieres)) {

				$cookieres[] = $product_id;
			}

			delete_cookie('recentviewed');



			///again set cookie

			$cookievalue = serialize($cookieres);

			$cookiearr = array(

				'name' => 'recentviewed',

				'value' => $cookievalue,

				'expire' => '86400'

			);

			$this->input->set_cookie($cookiearr);
		} else {

			///cookie set

			$cookie_data[] =	$product_id;

			$cookievalue = serialize($cookie_data);

			$cookiearr = array(

				'name' => 'recentviewed',

				'value' => $cookievalue,

				'expire' => '86400'

			);

			$this->input->set_cookie($cookiearr);
		}



		$this->load->view('web/pages/ajax/ajax_product_view.php', ['productdata' => $productdata]);
	}



	public function load_display_all_category_post()



	{

		$get_url = $this->uri->segment(1);

		$number = $this->input->post('number');

		$data_idall = $this->input->post('data_idall');

		$rs_cate = $this->web_model->get_category_slug();

		$data = array();

		$data_search = array();

		$this->load->library('pagination');

		$id = $data_idall;

		$config = array();

		$config["base_url"] = base_url() . "$get_url";

		$total_row = $this->web_model->record_count_product_by_cat($id);

		$rs_num_cate = $total_row->count;

		$config["total_rows"] = $rs_num_cate;

		$config["per_page"] = $number;

		$config['use_page_numbers'] = false;

		$config['num_links'] = 5;

		$config['cur_tag_open'] = '&nbsp;<a class="current">';

		$config['cur_tag_close'] = '</a>';

		$config['next_link'] = '<i class="fa fa-angle-double-right"></i>';

		$config['prev_link'] = '<i class="fa fa-angle-double-left" ></i>';

		$this->pagination->initialize($config);

		$str_links = $this->pagination->create_links();

		$data["links"] = explode('&nbsp;', $str_links);

		$page = $this->uri->segment(2);

		$data["get_display_all_cate"] = $this->web_model->fetch_data_product_by_cat($config["per_page"], $page, $id);

		$this->load->view('web/pages/ajax/ajax_display_cate.php', $data);
	}









	public function load_ajax_phiship()



	{



		$id = $this->input->post('id');

		$customer_id = $this->session->userdata('customer_id');

		$data = array();

		$data["get_city"] = $this->web_model->price_ship($id);

		$data['get_ds_info_thuong'] = $this->web_model->get_ds_info_thuong();

		$data['get_ds_info_bac'] = $this->web_model->get_ds_info_bac();

		$data['get_ds_info_vang'] = $this->web_model->get_ds_info_vang();

		$data['get_ds_info_kimcuong'] = $this->web_model->get_ds_info_kimcuong();

		$data["get_sum_orders"] = $this->web_model->get_sum_orders($customer_id);

		$this->load->view('web/pages/ajax/ajax_phiship.php', $data);
	}













	public function load_product_price_updown()

	{



		$id = $this->input->post('id');

		$price_updown = $this->input->post('price_updown');

		$data = array();

		$data["product_price_updown"] = $this->web_model->fetch_data_product_price_up_down_cat($id, $price_updown);

		$this->load->view('web/pages/ajax/ajax_price_up_down.php', $data);
	}



	public function load_mucgia()

	{

		$id = $this->input->post('id');

		$from = $this->input->post('from');

		$to = $this->input->post('to');

		$data = array();

		$data["product_price_updown"] = $this->web_model->fetch_data_product_mucgia_cat($id, $from, $to);

		$this->load->view('web/pages/ajax/ajax_price_up_down.php', $data);
	}





	public function load_ajax_filter_star()

	{

		$id = $this->input->post('id');

		$star = $this->input->post('star');

		$data = array();

		$data["product_star"] = $this->web_model->fetch_data_product_star_cat($id, $star);

		$this->load->view('web/pages/ajax/ajax_load_star.php', $data);
	}







	public function update_user()

	{

		$data = array();

		$customer_id = $this->session->userdata('customer_id');

		$data['customer_name'] = $this->input->post('customer_name');

		$data['customer_email'] = $this->input->post('customer_email');

		$data['customer_phone'] = $this->input->post('customer_phone');

		$data['customer_address'] = $this->input->post('customer_address');

		$data['customer_phone'] = $this->input->post('customer_phone');

		$data['customer_city'] = $this->input->post('customer_city');

		$get_pass = $this->input->post('customer_password');

		if (empty($get_pass)) {



			$xxx['customer_password'];
		} else {

			$xxx['customer_password'] = md5($get_pass);

			$this->web_model->update_user($xxx, $customer_id);
		}



		if (!empty($_FILES['customer_avatar']['name'])) {

			$config['upload_path'] = './uploads/';

			$config['allowed_types'] = '*';

			$config['max_size'] = 40964;

			$config['max_width'] = 50000;

			$config['max_height'] = 50000;

			$this->upload->initialize($config);

			if (!$this->upload->do_upload('customer_avatar')) {

				$error = $this->upload->display_errors();

				$this->session->set_flashdata('message', $error);

				redirect('thong-tin-tai-khoang.html');
			} else {

				$post_image = $this->upload->data();

				$data['customer_avatar'] = $post_image['file_name'];
			}
		}



		$result = $this->web_model->update_user($data, $customer_id);



		if ($result) {



			$this->session->set_flashdata('message', '<div id="PageOver_modal" class="PageOverlay_modal"></div>

                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">

                        <p class="text-center">Cập nhập thành công</p>

                      </div>

                      <div class="modal-footer">

                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>

                      </div>

                    </div>

                  </div>

                </div>');



			redirect('thong-tin-tai-khoan.html');
		} else {

			$this->session->set_flashdata('message', 'role_user Update Failed');

			redirect('thong-tin-tai-khoan.html');
		}
	}

	public function contact()



	{

		$data = array();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$this->load->view('web/inc/head_contact', $data);

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/contact');

		$this->load->view('web/inc/footer', $data);
	}

	public function save_regiter_mail()



	{

		$data = array();



		// $data['regiter_mail_name'] = $this->input->post('name');

		// $data['regiter_mail_phone'] = $this->input->post('phone');

		$data['regiter_mail_email'] = $this->input->post('email');

		$data['regiter_gioitinh'] = $this->input->post('regiter_gioitinh');

		// $data['regiter_service'] = $this->input->post('service');

		// $data['regiter_mail_time'] = $this->input->post('time');

		date_default_timezone_set('Asia/Ho_Chi_Minh');

		$time_now = date("Y-m-d h:i:s");

		$odata['regiter_mail_time'] = $time_now;

		// $data['regiter_mail_address'] = $this->input->post('address');

		// $data['regiter_mail_content'] = $this->input->post('content');

		// $name = $this->input->post('name');

		// $phone = $this->input->post('phone');

		$email = $this->input->post('email');

		$gioitinh = $this->input->post('regiter_gioitinh');



		// $service = $this->input->post('service');	

		// $time = $this->input->post('time');

		// $address = $this->input->post('address');



		// $content = $this->input->post('content');

		$rs_mail = $this->web_model->rs_id();

		$set_mail = $rs_mail->company_email;

		$get_website = $rs_mail->site_website;

		$get_user = $rs_mail->smtp_user;

		$get_pass = $rs_mail->smtp_pass;



		$config = array();



		$config['protocol'] = 'smtp'; //Giao thức máy chủ mail

		$config['smtp_host'] = 'ssl://smtp.gmail.com'; //Địa chỉ host máy chủ mail

		$config['smtp_user'] = $get_user; //Địa chỉ Gmail sử dụng để gửi mail

		$config['smtp_pass'] = $get_pass; //Mật khẩu của gmail gửi

		$config['smtp_port'] = '465'; //SMPT PORT

		$config['smtp_timeout'] = '5';

		$this->load->library('email', $config);

		$this->email->set_newline("\r\n");

		$this->email->from($set_mail, 'Đăng ký nhận tin');

		$this->email->to($set_mail);

		$this->email->cc($email);

		$this->email->subject('Thư gửi đăng ký nhận tin');

		$message = '



    <html>

        <head>

            <title>Đăng ký nhận tin</title>

        </head>

        <body>

      <p>Thư liên hệ của bạn</p>

        <table>

            <tr>

                <td>Email</td>

                <td>' . $email . '</td>



            </tr>

             <tr>

                <td>Giới Tính</td>

                <td>' . $gioitinh . '</td>

            </tr>

        </table>

            <p> Cảm ơn quý khách đã đăng ký nhận tin cho chúng tôi,chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất </p>



        </body>



    </html>';

		$this->email->set_mailtype("html");

		$this->email->message($message);

		if ($this->email->send())



			$this->session->set_flashdata("email_sent", "Congragulation Email Send Successfully.");

		else



			$this->session->set_flashdata("email_sent", "You have encountered an error");

		$this->email->print_debugger(array('headers'));

		$result = $this->web_model->save_regiter_mail($data);

		if ($result == true) {

			$this->session->set_flashdata('message', ' <div id="PageOver_modal" class="PageOverlay_modal"></div>

                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">

                        <p class="text-center">Cảm ơn quý khách đã đăng ký nhận tin của chúng tôi </p>

                      </div>

                      <div class="modal-footer">

                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>

                      </div>

                    </div>

                  </div>

                </div>');

			redirect('/');
		} else {



			$this->session->set_flashdata('message', '<div id="PageOver_modal" class="PageOverlay_modal"></div>

                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">

                        <p class="text-center">Lỗi đăng ký mail </p>

                      </div>

                      <div class="modal-footer">

                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>



                      </div>

                    </div>

                  </div>

                </div>');



			redirect('/');
		}
	}



	public function save_commentproduct()

	{

		$data = array();

		$data['comment_content'] = $this->input->post('comment_content');

		$data['comment_email'] = $this->input->post('comment_email');

		$data['comment_name'] = $this->input->post('comment_name');

		$data['comment_product_id'] = $this->input->post('product_id');

		$get_name = $this->input->post('comment_name');

		$get_mail = $this->input->post('comment_email');

		$get_content = $this->input->post('comment_content');

		$result = $this->web_model->save_comment_product($data);

		if ($result == true) {

			$this->session->set_flashdata('message', ' <div id="PageOver_modal" class="PageOverlay_modal"></div>

                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">



                        <p class="text-center">Cảm ơn quý khách đã để lại bình luận cho chúng tôi </p>



                      </div>

                      <div class="modal-footer">

                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>

                      </div>

                    </div>

                  </div>



                </div>');
		} else {



			$this->session->set_flashdata('message', 'Bình luận thất bại');
		}
	}



	public function save_dk_tv_tour()

	{

		$data = array();

		$data['muanhanh_phone'] = $this->input->post('muanhanh_phone');
		$data['muanhanh_name'] = $this->input->post('muanhanh_name');
		$data['muanhanh_email'] = $this->input->post('muanhanh_email');
		$data['daykh'] = $this->input->post('daykh');
		$data['daydk'] = $this->input->post('daydk');
		$data['soluong'] = $this->input->post('soluong');
		$data['note'] = $this->input->post('note');

		$data['muanhanh_product_id'] = $this->input->post('muanhanh_product_id');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$time_now = date("Y-m-d h:i:s");
		$data['muanhanh_time'] = $time_now;

		$get_slug_tour = $this->input->post('get_slug_tour');
		$phone = $this->input->post('muanhanh_phone');
		$name = $this->input->post('muanhanh_name');
		$email = $this->input->post('muanhanh_email');


		$rs_mail = $this->web_model->rs_id();
		$set_mail = $rs_mail->company_email;
		$get_website = $rs_mail->site_website;

		$get_user = $rs_mail->smtp_user;
		$get_pass = $rs_mail->smtp_pass;
		$config = array();
		$config['protocol'] = 'smtp'; //Giao thức máy chủ mail

		$config['smtp_host'] = 'ssl://smtp.gmail.com'; //Địa chỉ host máy chủ mail

		$config['smtp_user'] = $get_user; //Địa chỉ Gmail sử dụng để gửi mail

		$config['smtp_pass'] = $get_pass; //Mật khẩu của gmail gửi

		$config['smtp_port'] = '465'; //SMPT PORT

		$config['smtp_timeout'] = '5';

		$this->load->library('email', $config);

		$this->email->set_newline("\r\n");

		$this->email->from($set_mail, 'Đăng ký tư vấn');

		$this->email->to($set_mail);

		$this->email->cc($email);



		$this->email->subject('Thư gửi đăng ký tư vấn');

		$message = '



    <html>

        <head>

            <title>Đăng ký tư vấn</title>

        </head>

        <body>

        <table>

            <tr>

                <td>Tên</td>

                <td>' . $name . '</td>



            </tr>



            <tr>

                <td>Số điện thoại</td>

                <td>' . $phone . '</td>



            </tr>



            <tr>

                <td>Email</td>

                <td>' . $email . '</td>



            </tr>

            

        </table>

            <p> Cảm ơn quý khách đã đăng ký tư vấn cho chúng tôi,chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất </p>



        </body>



    </html>';

		$this->email->set_mailtype("html");

		$this->email->message($message);

		if ($this->email->send())



			$this->session->set_flashdata("email_sent", "Congragulation Email Send Successfully.");

		else



			$this->session->set_flashdata("email_sent", "You have encountered an error");

		$this->email->print_debugger(array('headers'));

		$result = $this->web_model->save_dk_tv_tour_model($data);

		if ($result == true) {

			$this->session->set_flashdata('message', ' <div id="PageOver_modal" class="PageOverlay_modal"></div>

                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">

                        <p class="text-center">Cảm ơn quý khách đã đăng ký tư vấn của chúng tôi </p>

                      </div>

                      <div class="modal-footer">

                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>

                      </div>

                    </div>

                  </div>

                </div>');

			redirect($get_slug_tour . '.html');
		} else {



			$this->session->set_flashdata('message', '<div id="PageOver_modal" class="PageOverlay_modal"></div>

                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">

                        <p class="text-center">Lỗi đăng ký tư vấn </p>

                      </div>

                      <div class="modal-footer">

                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>



                      </div>

                    </div>

                  </div>

                </div>');

			redirect($get_slug_tour . '.html');
		}
	}



	public function save_comment()



	{



		$data = array();



		$data['comment_content'] = $this->input->post('comment_content');

		$data['comment_name'] = $this->input->post('comment_name');

		$data['comment_start'] = $this->input->post('comment_start');

		$data['comment_title'] = $this->input->post('comment_title');

		$data['comment_product_id'] = $this->input->post('comment_product_id');

		date_default_timezone_set('Asia/Ho_Chi_Minh');

		$time_now = date("Y-m-d h:i:s");

		$data['comment_date'] = $time_now;

		if (!empty($_FILES['comment_images']['name'])) {



			$config['upload_path'] = './uploads/comment/';

			$config['allowed_types'] = '*';

			$config['max_size'] = 1002432;

			$config['max_width'] = 500000;

			$config['max_height'] = 500000;

			$this->upload->initialize($config);

			if ($this->upload->do_upload('comment_images')) {

				$post_image = $this->upload->data();

				$this->load->library("image_lib");

				$config['image_library'] = 'gd2';

				$config['source_image'] = './uploads/comment/' . $post_image['file_name'];

				$config['create_thumb'] = FALSE;

				$config['maintain_ratio'] = FALSE;

				$config['width'] = 75;

				$config['height'] = 100;

				$config['new_image'] = './uploads/comment/thumb/' . $post_image["file_name"];

				$this->image_lib->initialize($config);

				$this->image_lib->resize();

				$data['comment_images'] = $post_image['file_name'];
			} else {

				$data['errors'] = $this->upload->display_errors();

				$this->session->set_flashdata('message', $error);
			}
		}

		$result = $this->web_model->save_comment($data);

		if ($result == true) {

			$this->session->set_flashdata('message', ' <div id="PageOver_modal" class="PageOverlay_modal"></div>

                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 



                      </div>

                      <div class="modal-body">

                     <p class="text-center">Cảm ơn quý khách đã để lại đánh giá cho chúng tôi </p>



                      </div>

                      <div class="modal-footer">

                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>



                      </div>

                    </div>

                  </div>

                </div>');
		} else {

			$this->session->set_flashdata('message', 'đánh giá thất bại');
		}
	}



	public function save_comment_news_rate()



	{



		$data = array();

		$data['comment_news_rate_content'] = $this->input->post('comment_news_rate_content');

		$data['comment_news_rate_email'] = $this->input->post('comment_news_rate_email');

		$data['comment_news_rate_name'] = $this->input->post('comment_news_rate_name');

		$data['comment_news_rate_start'] = $this->input->post('comment_news_rate_start');

		$data['comment_news_rateid'] = $this->input->post('comment_news_rateid');

		$result = $this->web_model->save_comment_news_rate($data);

		if ($result == true) {

			$this->session->set_flashdata('message', '<div id="PageOver_modal" class="PageOverlay_modal"></div>

                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">

                        <p class="text-center">Cảm ơn quý khách đẫ để lại bình luận cho chúng tôi , bình luận sẽ được duyệt và cập nhật lên website </p>



                      </div>



                      <div class="modal-footer">



                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>

                      </div>

                    </div>

                  </div>

                </div>');
		} else {



			$this->session->set_flashdata('message', '<div id="PageOver_modal" class="PageOverlay_modal"></div>



                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">

                        <p class="text-center">Bình luận thất bại </p>

                      </div>

                      <div class="modal-footer">

                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>

                      </div>

                    </div>

                  </div>

                </div>');
		}
	}



	public function save_commentnews()



	{



		$data = array();



		$data['comment_content'] = $this->input->post('comment_content');

		$data['comment_email'] = $this->input->post('comment_email');

		$data['comment_name'] = $this->input->post('comment_name');

		$data['comment_news_id'] = $this->input->post('dmtin_id');

		$result = $this->web_model->save_comment_news($data);

		if ($result == true) {

			$this->session->set_flashdata('message', '

                <div id="PageOver_modal" class="PageOverlay_modal"></div>

                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">

                        <p class="text-center">Cảm ơn quý khách đã để lại bình luận cho chúng tôi </p>

                      </div>

                      <div class="modal-footer">

                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>

                      </div>

                    </div>

                  </div>

                </div>');
		} else {



			$this->session->set_flashdata('message', '



                <div id="PageOver_modal" class="PageOverlay_modal"></div>

                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">

                        <p class="text-center">Bình luận thất bại </p>

                      </div>

                      <div class="modal-footer">

                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>

                      </div>

                    </div>

                  </div>

                </div>');
		}
	}



	public function save_request()



	{

		$data = array();

		$data['request_name'] = $this->input->post('request_name');

		$data['request_address'] = $this->input->post('request_address');

		$data['request_phone'] = $this->input->post('request_phone');

		$data['request_email'] = $this->input->post('request_email');

		$data['request_content'] = $this->input->post('request_content');

		$data['request_product_id'] = $this->input->post('request_product_id');



		$get_name = $this->input->post('request_name');

		$get_mail = $this->input->post('request_email');

		$get_phone = $this->input->post('request_phone');

		$get_address = $this->input->post('request_address');

		$get_content = $this->input->post('request_content');



		$rs_mail = $this->web_model->rs_id();

		$set_mail = $rs_mail->company_email;

		$get_website = $rs_mail->site_website;

		$get_user = $rs_mail->smtp_user;

		$get_pass = $rs_mail->smtp_pass;



		$config = array();

		$config['protocol'] = 'smtp'; //Giao thức máy chủ mail

		$config['smtp_host'] = 'ssl://smtp.gmail.com'; //Địa chỉ host máy chủ mail

		$config['smtp_user'] = $get_user; //Địa chỉ Gmail sử dụng để gửi mail

		$config['smtp_pass'] = $get_pass; //Mật khẩu của gmail gửi

		$config['smtp_port'] = '465'; //SMPT PORT

		$config['smtp_timeout'] = '5';



		$this->load->library('email', $config);

		$this->email->set_newline("\r\n");

		$this->email->from($set_mail, $get_website);

		$this->email->to($set_mail);

		$this->email->subject('Thư gửi liên hệ');

		$message_requet = '



    <html>

        <head>

            <title>Thư gửi yêu cầu</title>

        </head>

        <body>

      <p>Thư liên hệ của bạn</p>

        <table>

            <tr>

                <td>Họ tên</td>

                <td>' . $get_name . '</td>

            </tr>

            <tr>

                <td>Email</td>

                <td>' . $get_mail . '</td>

            </tr>

            <tr>

                <td>SDT</td>

                <td>' . $get_phone . '</td>

            </tr>

             <tr>

                <td>Địa chỉ</td>

                <td>' . $get_address . '</td>

            </tr>

             <tr>

                <td>Nội dung</td>

                <td>' . $get_content . '</td>



            </tr>



        </table>

            <p> Cảm ơn quý khách đã liên hệ cho chúng tôi,chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất </p>

        </body>



    </html>



    ';



		$this->email->set_mailtype("html");

		$this->email->message($message_requet);

		if ($this->email->send()) {



			$this->session->set_flashdata("email_sent", "Congragulation Email Send Successfully.");
		} else {

			$this->session->set_flashdata("email_sent", "You have encountered an error");

			$this->email->print_debugger(array('headers'));
		}



		$this->form_validation->set_rules('request_phone', 'Mobile Number ', 'required|regex_match[/^[0-9]{10}$/]'); //{10} for 10 digits number

		if ($this->form_validation->run() == true) {



			$result = $this->web_model->save_request($data);

			$this->session->set_flashdata('message', '<div id="PageOver_modal" class="PageOverlay_modal"></div>

                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">

                        <p class="text-center">Cảm ơn quý khách đã gử yêu cầu cho chúng tôi </p>

                      </div>

                      <div class="modal-footer">

                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>

                      </div>

                    </div>

                  </div>

                </div>');
		} else {



			$this->session->set_flashdata('message', '<div id="PageOver_modal" class="PageOverlay_modal"></div>

                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>



                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">

                        <p class="text-center">Gửi yêu cầu thất bại </p>



                      </div>



                      <div class="modal-footer">



                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>



                      </div>

                    </div>



                  </div>



                </div>');
		}
	}

	public function save_baogia()

	{

		$data = array();

		$data['baogia_name'] = $this->input->post('baogia_name');

		$data['baogia_phone'] = $this->input->post('baogia_phone');

		$data['baogia_email'] = $this->input->post('baogia_email');

		$data['baogia_diachi'] = $this->input->post('baogia_diachi');

		$data['baogia_content'] = $this->input->post('baogia_content');

		$data['baogia_gia'] = $this->input->post('baogia_gia');

		if (isset($data) || $data == "") {

			$this->web_model->save_baogia($data);

			$this->session->set_flashdata('message', '<div id="PageOver_modal" class="PageOverlay_modal"></div>

                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">

                        <p class="text-center">Cảm ơn quý khách gửi yêu cầu báo giá cho chúng tôi </p>

                      </div>

                      <div class="modal-footer">

                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>

                      </div>

                    </div>

                  </div>

                </div>');

			return true;
		} else {



			$this->session->set_flashdata('message', '<div id="PageOver_modal" class="PageOverlay_modal"></div>

                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">

                        <p class="text-center">yêu cầu thất bại </p>

                      </div>

                      <div class="modal-footer">

                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>

                      </div>

                    </div>

                  </div>

                </div>');

			return false;
		}
	}



	public function contact_save()

	{

		$data = array();

		$data['contact_name'] = $this->input->post('contact_name');

		$data['contact_email'] = $this->input->post('contact_email');

		$data['contact_phone'] = $this->input->post('contact_phone');

		$data['contact_content'] = $this->input->post('contact_content');

		$get_name = $this->input->post('contact_name');

		$get_mail = $this->input->post('contact_email');

		$get_phone = $this->input->post('contact_phone');

		$get_content = $this->input->post('contact_content');



		$rs_mail = $this->web_model->rs_id();

		$set_mail = $rs_mail->company_email;

		$get_website = $rs_mail->site_website;

		$get_user = $rs_mail->smtp_user;

		$get_pass = $rs_mail->smtp_pass;



		$config = array();

		$config['protocol'] = 'smtp'; //Giao thức máy chủ mail

		$config['smtp_host'] = 'ssl://smtp.gmail.com'; //Địa chỉ host máy chủ mail

		$config['smtp_user'] = $get_user; //Địa chỉ Gmail sử dụng để gửi mail

		$config['smtp_pass'] = $get_pass; //Mật khẩu của gmail gửi

		$config['smtp_port'] = '465'; //SMPT PORT

		$config['smtp_timeout'] = '5';

		$this->load->library('email', $config);

		$this->email->set_newline("\r\n");

		$this->email->from($set_mail, $get_website);

		$this->email->to($set_mail);

		$this->email->cc($get_mail);

		$this->email->subject('Thư gửi Liên hệ');

		$message = '



    <html>

        <head>

            <title>Liên hệ</title>

        </head>

        <body>

      <p>Thư liên hệ của bạn</p>

        <table>

            <tr>

                <td>Họ tên</td>

                <td>' . $get_name . '</td>

            </tr>

            <tr>

                <td>Email</td>

                <td>' . $get_mail . '</td>

            </tr>

            <tr>

                <td>SDT</td>



                <td>' . $get_phone . '</td>



            </tr>

             <tr>



                <td>Nội dung</td>

                <td>' . $get_content . '</td>



            </tr>

        </table>



            <p> Cảm ơn quý khách đã liên hệ cho chúng tôi,chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất </p>



        </body>



    </html>



    ';

		$this->email->set_mailtype("html");

		$this->email->message($message);



		if ($this->email->send())



			$this->session->set_flashdata("email_sent", "Congragulation Email Send Successfully.");



		else

			$this->session->set_flashdata("email_sent", "You have encountered an error");



		$this->email->print_debugger(array('headers'));

		$result = $this->web_model->save_contact($data);



		if ($result) {



			$this->session->set_flashdata('message', '<div id="PageOver_modal" class="PageOverlay_modal"></div>

                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">



                        <p class="text-center">Cảm ơn quý khách đã liên hệ cho chúng tôi , chúng tôi sẽ liên hệ lại trong thời gian sớm nhất </p>

                      </div>

                      <div class="modal-footer">

                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>

                      </div>

                    </div>

                  </div>

                </div>');

			redirect('lien-he.html');
		} else {



			$this->session->set_flashdata('message', ' <div id="PageOver_modal" class="PageOverlay_modal"></div>



                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">

                        <p class="text-center">Lỗi gửi liên hệ </p>

                      </div>

                      <div class="modal-footer">

                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>

                      </div>

                    </div>

                  </div>

                </div>');

			redirect('lien-he.html');
		}
	}



	public function cart()



	{

		$data = array();



		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['cart_contents'] = $this->cart->contents();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$customer_id = $this->session->userdata('customer_id');



		$data['get_info_member'] = $this->web_model->get_info_member($customer_id);



		$this->load->view('web/inc/head_cart', $data);

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/cart', $data);

		$this->load->view('web/inc/footer', $data);
	}

	public function product()



	{

		$data = array();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['get_all_brand'] = $this->web_model->get_all_brand_product();

		$data['get_all_xuatxu'] = $this->web_model->get_all_xuatxu();

		$data['get_all_thanhphan'] = $this->web_model->get_all_thanhphan();

		$data['get_all_congdung'] = $this->web_model->get_all_congdung();

		$this->load->library('pagination');



		$config = array();

		$config["base_url"] = base_url() . "san-pham.html";

		$total_row = $this->web_model->record_count();

		$config["total_rows"] = $total_row;

		$config["per_page"] = 32;

		$config['use_page_numbers'] = false;

		$config['num_links'] = 5;

		$config['cur_tag_open'] = '&nbsp;<a class="current">';

		$config['cur_tag_close'] = '</a>';

		$config['next_link'] = 'Next';



		$config['prev_link'] = 'Previous';

		$this->pagination->initialize($config);



		$page = $this->uri->segment(2);

		$data["get_all_product"] = $this->web_model->fetch_data($config["per_page"], $page);



		$str_links = $this->pagination->create_links();



		$data["links"] = explode('&nbsp;', $str_links);



		$data['get_count_cate'] = $total_row;



		$data["product_selling"] = $this->web_model->fetch_data_product_selling_by_sanpham($config["per_page"], $page);

		$data["product_price_tang"] = $this->web_model->fetch_data_product_price_tang_by_sanpham($config["per_page"], $page);

		$data["product_price_giam"] = $this->web_model->fetch_data_product_price_giam_by_sanpham($config["per_page"], $page);



		$this->load->view('web/inc/head', $data);

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/product_sanpham', $data);

		$this->load->view('web/inc/footer', $data);
	}



	public function single($id)

	{

		$server = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "";

		$parse_url = parse_url($server);

		$path = $parse_url['path'];

		$explode = explode('/', $path);

		$cateSlug = str_replace('.html', '', end($explode));



		$get_url = $this->uri->segment(1);

		$rs_singe_slug = $this->web_model->get_single_product_slug();



		for ($i = 0; $i < count($rs_singe_slug); $i++) {

			$get_string = $rs_singe_slug[$i]['product_slug'] . '.html';



			if ($get_url == $get_string) {



				$id = $rs_singe_slug[$i]['product_id'];

				$id_cate = $rs_singe_slug[$i]['id'];



				$data['get_single_product'] = $this->web_model->get_single_product($id, $id_cate);
			}
		}



		$data = array();

		$data_view = array();

		$data['cateSlug'] = $cateSlug;

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_single_product'] = $this->web_model->get_single_product($id, $id_cate);

		$data['get_depart_detail'] = $this->web_model->get_depart_detail($id);

		$data['get_ct_tour_detail'] = $this->web_model->get_ct_tour_detail($id);

		$data['get_depart_row'] = $this->web_model->get_depart_row($id);

		$rs_product_view = $this->web_model->get_product_by_id($id);

		$data_view['product_view'] = $rs_product_view->product_view + 1;

		$this->web_model->update_products($data_view, $id);

		$data['get_chinhsach'] = $this->web_model->get_all_chinhsach_post();

		$data['get_all_product'] = $this->web_model->get_all_product();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['get_all_avd_post'] = $this->web_model->get_all_avd_post();

		$data['pr_tindung_detail'] = $this->web_model->pr_tindung_detail();

		$this->load->view('web/inc/head_product_detail', $data);

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/single', $data);

		$this->load->view('web/inc/footer', $data);
	}



	public function single_new($id)

	{

		$get_url = $this->uri->segment(1);

		$rs_cate = $this->web_model->get_single_dmtin_slug();

		for ($i = 0; $i < count($rs_cate); $i++) {

			$AddToEnd = ".html";

			$get_string = $rs_cate[$i]['dmtin_slug'];

			$finalString = implode(array($get_string, $AddToEnd));

			if ($get_url == $finalString) {

				$id = $rs_cate[$i]['dmtin_id'];

				$data['get_single_dmtin'] = $this->web_model->get_single_dmtin($id);
			} else {
			}
		}



		$data = array();

		$id_user = $this->session->userdata('user_id');

		$data['get_user_name'] = $this->web_model->get_name_user($id_user);

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_single_dmtin'] = $this->web_model->get_single_dmtin($id);

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['get_all_avd_post'] = $this->web_model->get_all_avd_post();

		$data['get_product_other'] = $this->web_model->get_product_other_singnew($id);

		$data['product_new'] = $this->web_model->product_new();
		$data['all_hotline'] = $this->web_model->getall_hotline_info();

		$data_view = array();

		$rs_news_view = $this->web_model->get_single_dmtin($id);

		$data_view['view'] = $rs_news_view->view + 1;

		$this->web_model->update_news($data_view, $id);

		$this->load->view('web/inc/head_catelogy_news_detail', $data);

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/single_new', $data);

		$this->load->view('web/inc/footer', $data);
	}



	public function single_new_amp($id)



	{



		$get_url = $this->uri->segment(1);

		$get_amp = $this->uri->segment(2);

		$get_string_amp = $get_url . '/' . $get_amp;

		$rs_cate = $this->web_model->get_single_dmtin_slug();

		for ($i = 0; $i < count($rs_cate); $i++) {

			$finalString = 'amp/' . $rs_cate[$i]['dmtin_slug'] . '.html';

			if ($get_string_amp == $finalString) {



				$id = $rs_cate[$i]['dmtin_id'];

				$data['get_single_dmtin'] = $this->web_model->get_single_dmtin($id);
			} else {
			}
		}



		$data = array();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_single_dmtin'] = $this->web_model->get_single_dmtin($id);

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['get_all_avd_post'] = $this->web_model->get_all_avd_post();

		$data['get_product_other'] = $this->web_model->get_product_other_singnew($id);

		$this->load->view('web/pages/amp/amp_sing_new', $data);
	}



	public function single_one_news($id)

	{

		$get_url = $this->uri->segment(1);

		$rs_cate = $this->web_model->get_single_one_news_slug();

		for ($i = 0; $i < count($rs_cate); $i++) {

			$AddToEnd = ".html";

			$get_string = $rs_cate[$i]['one_news_slug'];

			$finalString = implode(array($get_string, $AddToEnd));

			if ($get_url == $finalString) {

				$id = $rs_cate[$i]['one_news_id'];

				$data['get_single_one_news'] = $this->web_model->get_single_one_news($id);
			} else {
			}
		}

		$data = array();

		$data['get_single_one_news'] = $this->web_model->get_single_one_news($id);

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();
		$data['all_hotline'] = $this->web_model->getall_hotline_info();

		$this->load->view('web/inc/head_one_news_detail', $data);

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/single_one_news', $data);

		$this->load->view('web/inc/footer', $data);
	}

	public function error()



	{

		$data = array();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$this->load->view('web/inc/head_error', $data);

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/error');

		$this->load->view('web/inc/footer', $data);
	}

	public function country_post($id)

	{

		$get_url = $this->uri->segment(1);

		$rs_country = $this->web_model->get_country_slug();

		$data = array();

		$this->load->library('pagination');

		for ($i = 0; $i < count($rs_country); $i++) {



			$AddToEnd = ".html";

			$get_string = $rs_country[$i]['brand_slug'];

			$finalString = implode(array($get_string, $AddToEnd));

			if ($get_url == $finalString) {

				$id = $rs_country[$i]['brand_id'];

				$config = array();

				$config["base_url"] = base_url() . "$get_url";

				$total_row = $this->web_model->record_count_product_by_country($id);

				$rs_num_cate = $total_row->count;

				$config["total_rows"] = $rs_num_cate;

				/*================BEGIN : Phân trang thương hiệu==================*/

				$config["per_page"] = 32;

				$config['use_page_numbers'] = false;

				$config['num_links'] = 5;

				$config['cur_tag_open'] = '&nbsp;<a class="current">';

				$config['cur_tag_close'] = '</a>';

				$config['next_link'] = '<i class="fa fa-angle-double-right"></i>';

				$config['prev_link'] = '<i class="fa fa-angle-double-left" ></i>';

				$this->pagination->initialize($config);

				$str_links = $this->pagination->create_links();

				$data["links"] = explode('&nbsp;', $str_links);

				$page = $this->uri->segment(2);



				/*================END : Phân trang thương hiệu==================*/



				$data["get_all_product_country"] = $this->web_model->fetch_data_product_by_country($config["per_page"], $page, $id);
			} else {
			}
		}



		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_id_country'] = $this->web_model->get_id_country($id);

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['get_all_country'] = $this->web_model->get_all_country();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$this->load->view('web/inc/head_country', $data);

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/product_by_country', $data);

		$this->load->view('web/inc/footer', $data);
	}



	public function get_loadmore_product()



	{

		$page = (int)$_GET['page'];

		$page = (!empty($page) && $page > 0) ? $page : 1;

		$id_cate_more = $this->input->get('id_cate_more');

		$flag = 'true';

		$limit = 20;

		$get_all_product = $this->web_model->getProduct($page, $id_cate_more);

		if (count($get_all_product) < $limit) {

			$flag = 'false';
		} else {



			$next_products = $this->web_model->next_Product($page, $id_cate_more);

			if (count($get_all_product) == $limit && empty($next_products)) {



				$flag = 'false';
			}
		}



		$body = $this->load->view('web/pages/ajax/ajax_load_more.php', array('get_all_product' => $get_all_product), true);

		$this->output->set_content_type('application/json');

		$this->output->set_output(json_encode(array('flag' => $flag, 'body' => $body)));

		$this->output->_display();



		exit;
	}

	public function category_post($id)
	{
		$get_url = $this->uri->segment(1);
		$rs_cate = $this->web_model->get_category_slug();
		$data = array();
		$data_search = array();
		$this->load->library('pagination');
		for ($i = 0; $i < count($rs_cate); $i++) {
			$AddToEnd = ".html";
			$get_string = $rs_cate[$i]['slug'];
			$finalString = implode(array($get_string, $AddToEnd));
			if ($get_url == $finalString) {
				$id = $rs_cate[$i]['id'];
				$config = array();
				$config["base_url"] = base_url() . "$get_url";
				$total_row = $this->web_model->record_count_product_by_cat($id);
				$rs_num_cate = $total_row->count;
				$config["total_rows"] = $rs_num_cate;
				$data["get_count_more"] = $rs_num_cate;
				$config["per_page"] = 20;
				$config['use_page_numbers'] = false;
				$config['num_links'] = 5;
				$config['cur_tag_open'] = '&nbsp;<a class="current">';
				$config['cur_tag_close'] = '</a>';
				$config['next_link'] = '<i class="fa fa-angle-double-right"></i>';
				$config['prev_link'] = '<i class="fa fa-angle-double-left" ></i>';
				$this->pagination->initialize($config);
				$str_links = $this->pagination->create_links();
				$data["links"] = explode('&nbsp;', $str_links);
				if ($rs_num_cate >= 20) {
					$data['show_cate_count'] = 20;
				} else {
					$data['show_cate_count'] = $rs_num_cate;
				}
				$page = $this->uri->segment(2);
				// 	  if($rs_cate[$i]['id']==475)
				// 	 {
				//  $data["get_all_product_combo"] = $this->web_model->fetch_data_product_by_cat_combo($config["per_page"], $page, $id);
				// 	 }

				if ($rs_cate[$i]['parent_id'] == 0) {
					$data["get_all_product"] = $this->web_model->fetch_data_product_by_cat($config["per_page"], $page, $id);
				} else {
					$data["get_all_product"] = $this->web_model->fetch_data_product_by_cat_id($config["per_page"], $page, $id);
				}
			} else {
			}
		}
		$data['get_product_images_news'] = $this->web_model->get_product_images_news($id);
		$data['get_about_wordshop'] = $this->web_model->get_about_wordshop();
		$data['get_one_news'] = $this->web_model->get_all_one_news_post();
		$data['get_id_catelogy'] = $this->web_model->get_id_catelogy($id);
		$data['get_catelogy_parent'] = $this->web_model->get_catelogy_parent($id);
		$data['get_count_cate'] = $rs_num_cate = $total_row->count;
		$data['get_chinhsach'] = $this->web_model->get_all_chinhsach_post();
		$data['get_all_category'] = $this->web_model->get_all_category();
		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();
		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();
		$data['get_all_brand'] = $this->web_model->get_all_brand();
		$data['get_all_xuatxu'] = $this->web_model->get_all_xuatxu();
		$data['get_all_thanhphan'] = $this->web_model->get_all_thanhphan();
		$data['get_all_congdung'] = $this->web_model->get_all_congdung();
		$this->load->view('web/inc/head_product', $data);
		$this->load->view('web/inc/header', $data);
		$this->load->view('web/pages/product', $data);
		$this->load->view('web/inc/footer', $data);
	}

	public function catalog_new_post($id)

	{

		$get_url = $this->uri->segment(1);

		$rs_cate = $this->web_model->get_category_dmtin_slug();

		$data = array();

		$this->load->library('pagination');

		for ($i = 0; $i < count($rs_cate); $i++) {

			$AddToEnd = ".html";

			$get_string = $rs_cate[$i]['slug'];

			$finalString = implode(array($get_string, $AddToEnd));

			if ($get_url == $finalString) {

				$id = $rs_cate[$i]['id'];

				$config = array();

				$config["base_url"] = base_url() . "$get_url";

				$total_row = $this->web_model->record_count_product_by_cate_news($id);

				$rs_num_cate = $total_row->count;

				$config["total_rows"] = $rs_num_cate;

				$config["per_page"] = 21;

				$config['use_page_numbers'] = false;

				$config['num_links'] = 5;

				$config['cur_tag_open'] = '&nbsp;<a class="current">';

				$config['cur_tag_close'] = '</a>';

				$config['next_link'] = '<i class="fa fa-angle-double-right"></i>';

				$config['prev_link'] = '<i class="fa fa-angle-double-left" ></i>';

				$this->pagination->initialize($config);

				$str_links = $this->pagination->create_links();

				$data["links"] = explode('&nbsp;', $str_links);

				$page = $this->uri->segment(2);

				$data['get_all_dmtin'] = $this->web_model->fetch_data_dmtin_by_cat($config["per_page"], $page, $id);
			} else {
			}
		}

		$id_user = $this->session->userdata('user_id');

		if (empty($id_user)) {

			$id_user = 1;

			$data['get_user_name'] = $this->web_model->get_name_user($id_user);
		} else {

			$data['get_user_name'] = $this->web_model->get_name_user($id_user);
		}

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['get_id_catelogy_dmtin'] = $this->web_model->get_id_catelogy_dmtin($id);

		$data['get_all_category_dmtin'] = $this->web_model->get_all_category_dmtin();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$data['product_new'] = $this->web_model->product_new();

		$this->load->view('web/inc/head_catelogy_news', $data);

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/catalog_new', $data);

		$this->load->view('web/inc/footer', $data);
	}



	public function save_cart()

	{

		$data = array();
		$product_id = $this->input->post('product_id');
		$results = $this->web_model->get_product_by_id($product_id);
		if (!$results) {
			redirect();
		}
		if (floatValue($results->product_price_sale) == 0) {
			$price = floatValue($results->product_price);
		} else {
			$price = floatValue($results->product_price_sale);
		}

		$data['id'] = $results->product_id;
		$addCart_name = preg_replace('/[^\p{L}\p{N}]/u', ' ', strip_tags($results->product_title));
		$data['name'] = $addCart_name;
		$tour_date_begin = $this->input->post('tour_date_begin');
		$tour_price_people = $this->input->post('tour_price_people');
		$tour_price_child = $this->input->post('tour_price_child');
		$tour_price_baby = $this->input->post('tour_price_baby');
		$tour_xp = $this->input->post('tour_xp');
		$tour_time = $this->input->post('tour_time');
		$tour_pt = $this->input->post('tour_pt');

		$id_depart = $this->input->post('id_depart');
		if (isset($tour_price_people)) {
			$data['price'] = $tour_price_people;
		} else {
			$data['price'] = $price;
		}
		$data['qty'] = $this->input->post('qty');
		$data['options'] = array('product_image_order' => $results->product_image, 'tour_date_begin' => $tour_date_begin, 'tour_price_people' => $tour_price_people, 'tour_price_child' => $tour_price_child, 'tour_price_baby' => $tour_price_baby, 'tour_time' => $tour_time, 'tour_xp' => $tour_xp, 'tour_pt' => $tour_pt);
		if ($this->cart->total_items() > 0) {
			$this->cart->destroy();
			$this->cart->insert($data);
			redirect('gio-hang.html');
		} else {
			$result = $this->cart->insert($data);
		}

		if ($result == true) {
			// $this->session->set_flashdata('message', '<div  class="alert alert-success alert-dismissible add-to-cart-success"><span  class="close" data-dismiss="alert" aria-label="close"> × </span><p  class="text"><i class="fa fa-check-circle-o"></i> Đặt tour thành công! </p><a  class="btn" href="/gio-hang.html">Xem giỏ hàng và thanh toán</a></div>');
			redirect('gio-hang.html');
		} else {

			$this->session->set_flashdata('message', 'Đặt Tour thất bại');
		}
	}









	public function update_cart()

	{

		$data = array();



		$data['qty'] = $this->input->post('qty');

		$data['rowid'] = $this->input->post('rowid');

		$data['price_people'] = $this->input->post('price_people');

		$price = $this->input->post('price_people');

		$qty = $this->input->post('qty');



		$price_people = $qty * $price;

		$data['total_price_people'] = $price_people;

		$this->cart->update($data);

		$this->load->view('web/pages/ajax/ajax_load_total_price_cart.php', $data);
	}



	public function update_cart_price_child()

	{

		$data = array();



		$qty_child = $this->input->post('qty_child');

		$price_child = $this->input->post('price_child');



		$total_pri = $qty_child * $price_child;

		$data['qty_child'] = $qty_child;

		$data['price_child'] = $price_child;

		$data['total_price_child'] = $total_pri;



		$this->load->view('web/pages/ajax/ajax_load_total_price_cart.php', $data);
	}





	public function update_cart_price_baby()

	{

		$data = array();



		$qty_baby = $this->input->post('qty_baby');

		$price_baby = $this->input->post('price_baby');



		$total_price = $qty_baby * $price_baby;

		$data['qty_baby'] = $qty_baby;

		$data['total_price_baby'] = $total_price;

		$data['price_baby'] = $price_baby;

		$data['qty'] = $this->input->post('qty');



		$this->load->view('web/pages/ajax/ajax_load_total_price_cart.php', $data);
	}



	public function remove_cart()



	{

		$data = $this->input->post('rowid');

		$this->cart->remove($data);

		redirect('gio-hang.html');
	}



	public function register_success()

	{

		$customer_name = $this->session->flashdata('customer_name');

		if (!$customer_name) {



			redirect('customer/register');
		}

		$data = array();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$this->load->view('web/inc/head');

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/register_success', $data);

		$this->load->view('web/inc/footer', $data);
	}





	public function customer_register()

	{

		$data = array();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$this->load->view('web/inc/head');

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/customer_register', $data);

		$this->load->view('web/inc/footer', $data);
	}



	public function info_member()

	{

		$data = array();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$customer_id = $this->session->userdata('customer_id');

		$data['get_info_member'] = $this->web_model->get_info_member($customer_id);

		$data['order_info'] = $this->web_model->order_history($customer_id);

		$data['get_ds_info_thuong'] = $this->web_model->get_ds_info_thuong();

		$data['get_ds_info_bac'] = $this->web_model->get_ds_info_bac();

		$data['get_ds_info_vang'] = $this->web_model->get_ds_info_vang();

		$data['get_ds_info_kimcuong'] = $this->web_model->get_ds_info_kimcuong();

		if (!isset($customer_id)) {

			redirect('dang-nhap.html');
		} else {



			$this->load->view('web/inc/head');

			$this->load->view('web/inc/header', $data);

			$this->load->view('web/pages/info_member', $data);

			$this->load->view('web/inc/footer', $data);
		}
	}



	public function history()

	{

		$data = array();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$customer_id = $this->session->userdata('customer_id');

		$data['get_history_member'] = $this->web_model->orderdetails_member($customer_id);

		$data['order_info'] = $this->web_model->order_history($customer_id);

		if (!isset($customer_id)) {

			redirect('dang-nhap.html');
		} else {



			$this->load->view('web/inc/head');

			$this->load->view('web/inc/header', $data);

			$this->load->view('web/pages/history', $data);

			$this->load->view('web/inc/footer', $data);
		}
	}



	public function history_detail($order_id)



	{



		$data = array();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$customer_id = $this->session->userdata('customer_id');

		$data['total_order_detail'] = $this->web_model->order_total_info_by_id_member($order_id);

		$data['get_history_member_detail'] = $this->web_model->orderdetails_member_detail($customer_id, $order_id);

		$data['order_info'] = $this->web_model->order_history($customer_id);

		$this->load->view('web/inc/head');

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/history_detail', $data);

		$this->load->view('web/inc/footer', $data);
	}

	public function customer_login()

	{



		$data = array();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$this->load->view('web/inc/head');

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/customer_login', $data);

		$this->load->view('web/inc/footer', $data);
	}



	public function forget_pass()



	{

		$data = array();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$this->load->view('web/inc/head');

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/customer_resetpass', $data);

		$this->load->view('web/inc/footer', $data);
	}



	public function customer_save()



	{

		$data = array();

		$data['customer_name'] = $this->input->post('customer_name');

		$data['customer_email'] = $this->input->post('customer_email');

		$data['customer_password'] = md5($this->input->post('customer_password'));

		$data['customer_address'] = $this->input->post('customer_address');

		$data['customer_phone'] = $this->input->post('customer_phone');

		$email_p = $this->input->post('customer_email');

		$passyou = $this->input->post('customer_password');

		$this->form_validation->set_rules('customer_name', 'vui lòng nhập họ tên', 'trim|required');

		$this->form_validation->set_rules('customer_email', 'vui lòng nhập email', 'trim|required|valid_email|is_unique[tbl_customer.customer_email]');

		$this->form_validation->set_rules('customer_password', 'vui lòng nhập password', 'trim|required');

		$this->form_validation->set_rules('customer_phone', 'vui lòng nhập số điện thoại', 'trim|required');

		if ($this->form_validation->run() == true) {

			$result = $this->web_model->save_customer_info($data);

			if ($result) {

				$this->session->set_flashdata('customer_name', $data['customer_name']);

				$this->session->set_flashdata('customer_email', $data['customer_email']);





				$rs_mail = $this->web_model->rs_id();

				$set_mail = $rs_mail->company_email;

				$get_website = $rs_mail->site_website;

				$get_user = $rs_mail->smtp_user;

				$get_pass = $rs_mail->smtp_pass;



				$config = array();

				$config['protocol'] = 'smtp'; //Giao thức máy chủ mail

				$config['smtp_host'] = 'ssl://smtp.gmail.com'; //Địa chỉ host máy chủ mail

				$config['smtp_user'] = $get_user; //Địa chỉ Gmail sử dụng để gửi mail

				$config['smtp_pass'] = $get_pass; //Mật khẩu của gmail gửi

				$config['smtp_port'] = '465'; //SMPT PORT

				$config['smtp_timeout'] = '5';

				$this->load->library('email', $config);

				$this->email->set_newline("\r\n");

				$this->email->from($set_mail, $get_website);

				$this->email->to($email_p);

				$this->email->subject('Tài khoản mới của bạn tại website');

				$message_account .= '



    <html>

    <body>

      <div style="max-width:700px;width: 100%;border: 0px;margin:0 auto;padding:20px; background:#fff; color:#000; font-size:15px; line-height:25px;">';



				$message_account .= '<h1 style="font-size:17px;font-weight:bold;color:#444;padding:0 0 5px 0;margin:0">Chào quý khách,</h1>';



				$message_account .= '<p style="font-family:Arial,Helvetica,sans-serif;font-size:14px;color:#444;line-height:18px;font-weight:normal"> đã nhận được đăng ký tài khoảng của quý khách.</p>';

				$message_account .= 'Tên đăng nhập của Quý khách là: <b>' . $email_p . '</b><br>';

				$message_account .= 'Mật khẩu  của Quý khách: <b>' . $passyou . '</b>';

				$message_account .= '<br>Trân trọng';

				$this->email->set_mailtype("html");

				$this->email->message($message_account);

				if ($this->email->send()) {

					echo 'Email sent.';
				} else {



					show_error($this->email->print_debugger());
				}

				redirect('register/success');
			} else {



				$this->session->set_flashdata('message', 'Đăng ký thất bại');

				redirect('dang-ky.html');
			}
		} else {



			$this->session->set_flashdata('message', validation_errors());



			redirect('dang-ky.html');
		}
	}



	public function customer_logincheck()



	{

		$this->form_validation->set_rules('customer_email', 'Mail Hoặc Số điện thoại không đúng', 'trim|required');



		$this->form_validation->set_rules('customer_password', 'Mật khẩu không đúng', 'trim|required');

		if ($this->form_validation->run() == TRUE) {

			$data = array();

			$email = $this->input->post('customer_email');

			$password = md5($this->input->post('customer_password'));

			$result = $this->web_model->get_customer_info($email, $password);

			if ($result) {



				$this->session->set_userdata('customer_id', $result->customer_id);

				$this->session->set_userdata('customer_email', $result->customer_email);

				redirect('/');
			} else {



				$this->session->set_flashdata('message', 'Đăng nhập thất bại');

				redirect('dang-nhap.html');
			}
		} else {



			$this->session->set_flashdata('message', validation_errors());

			redirect('dang-nhap.html');
		}
	}



	public function customer_resetpass()



	{

		$data = array();

		$email_p = $this->input->post('customer_email');

		$result = $this->web_model->get_customer_forgetpass_info($email_p);

		if ($result) {

			$id = $result->customer_id;

			$pass = rand();

			$this->session->set_userdata('pass', $pass);

			$pass_news['customer_password'] = md5($pass);

			$this->web_model->update_forgetpass_info($id, $pass_news);

			$this->session->set_flashdata('message', 'Mật khẩu đã được cấp thành công vui lòng kiểm tra email của bạn ');





			$rs_mail = $this->web_model->rs_id();

			$set_mail = $rs_mail->company_email;

			$get_website = $rs_mail->site_website;

			$get_user = $rs_mail->smtp_user;

			$get_pass = $rs_mail->smtp_pass;

			$config = array();

			$config['protocol'] = 'smtp'; //Giao thức máy chủ mail

			$config['smtp_host'] = 'ssl://smtp.gmail.com'; //Địa chỉ host máy chủ mail

			$config['smtp_user'] = $get_user; //Địa chỉ Gmail sử dụng để gửi mail

			$config['smtp_pass'] = $get_pass; //Mật khẩu của gmail gửi

			$config['smtp_port'] = '465'; //SMPT PORT

			$config['smtp_timeout'] = '5';

			$this->load->library('email', $config);

			$this->email->set_newline("\r\n");

			$this->email->from($set_mail, $get_website);

			$this->email->to($email_p);

			$this->email->subject('Mật khẩu mới của bạn tại website');

			$message_fogetpass .= '



    <html>



    <body>



      <div style="max-width:700px;width: 100%;border: 0px;margin:0 auto;padding:20px; background:#fff; color:#000; font-size:15px; line-height:25px;">';



			$message_fogetpass .= '<h1 style="font-size:17px;font-weight:bold;color:#444;padding:0 0 5px 0;margin:0">Chào quý khách,</h1>';



			$message_fogetpass .= '<p style="font-family:Arial,Helvetica,sans-serif;font-size:14px;color:#444;line-height:18px;font-weight:normal"> đã nhận được yêu cầu thay đổi mật khẩu của quý khách.</p>';



			$message_fogetpass .= 'Sau đây là mật khẩu mới của Quý khách: <b>' . $pass . '</b>';

			$message_fogetpass .= '<br>Trân trọng';

			$this->email->set_mailtype("html");

			$this->email->message($message_fogetpass);

			if ($this->email->send()) {



				echo 'Email sent.';
			} else {

				show_error($this->email->print_debugger());
			}

			redirect('reset-password.html');
		} else {



			$this->session->set_flashdata('message', 'Email chưa được đăng ký');

			redirect('reset-password.html');
		}
	}



	public function save_shipping_address()

	{

		$data = array();

		$data['shipping_name'] = $this->input->post('shipping_name');

		$data['shipping_email'] = $this->input->post('shipping_email');

		$data['shipping_address'] = $this->input->post('shipping_address');

		$data['shipping_phone'] = $this->input->post('shipping_phone');

		$data['shipping_city'] = $this->input->post('shipping_city');

		$data['shipping_content'] = $this->input->post('shipping_content');

		$get_name = $this->input->post('shipping_name');

		$get_phone = $this->input->post('shipping_phone');

		$get_diachi = $this->input->post('shipping_address');

		$get_tp = $this->input->post('shipping_city');

		$get_ghichu = $this->input->post('shipping_content');

		$get_emailx = $this->input->post('shipping_email');

		$code = $this->input->post('code');

		$this->session->set_userdata('code', $code);

		$get_sum_ck = $this->input->post('sumck');

		$get_sum_sale = $this->input->post('sumsale');



		$this->session->set_userdata('sum_total', $get_sum_ck);

		$result = $this->web_model->save_shipping_address($data);

		$this->session->set_userdata('shipping_id', $result);



		if ($result) {

			$odata = array();

			$odata['shipping_id'] = $this->session->userdata('shipping_id');

			$customer_id = $this->session->userdata('customer_id');

			$odata['cus_id'] = $customer_id;

			$odata['city_id'] = $this->input->post('city_id');

			$odata['qty_child'] = $this->input->post('get_child');

			$odata['qty_baby'] = $this->input->post('get_baby');

			$odata['payment_method'] = $this->input->post('payment_method');

			$odata['datego'] = $this->input->post('datego');
			date_default_timezone_set('Asia/Ho_Chi_Minh');

			$time_now = date("Y-m-d h:i:s");

			$odata['order_day'] = $time_now;

			if (isset($customer_id)) {

				$odata['order_total'] = $get_sum_ck;

				$odata['order_sumsale'] = $get_sum_sale;
			} else {

				$odata['order_total'] = $get_sum_ck;

				$odata['order_sumsale'] = $get_sum_sale;
			}

			$order_id = $this->web_model->save_order_info($odata);







			$this->session->set_userdata('order_id', $order_id);

			$oddata = array();

			$myoddata = $this->cart->contents();

			foreach ($myoddata as $oddatas) {

				$oddata['order_id'] = $order_id;

				$oddata['product_id_order'] = $oddatas['id'];

				$oddata['product_name'] = $oddatas['name'];

				$oddata['tour_date_begin'] = $oddatas['options']['tour_date_begin'];

				$oddata['tour_price_people'] = $oddatas['options']['tour_price_people'];

				$oddata['tour_price_child'] = $oddatas['options']['tour_price_child'];

				$oddata['tour_price_baby'] = $oddatas['options']['tour_price_baby'];

				$oddata['tour_time'] = $oddatas['options']['tour_time'];

				$oddata['product_price_order'] = $oddatas['price'];

				$oddata['product_quantity'] = $oddatas['qty'];

				$oddata['product_image_order'] = $oddatas['options']['product_image_order'];

				$this->web_model->save_order_details_info($oddata);
			}



			if (empty($get_emailx)) {



				echo "";
			} else {



				$rs_mail = $this->web_model->rs_id();

				$set_mail = $rs_mail->company_email;

				$get_website = $rs_mail->site_website;

				$get_user = $rs_mail->smtp_user;

				$get_pass = $rs_mail->smtp_pass;



				$config = array();

				$config['protocol'] = 'smtp'; //Giao thức máy chủ mail

				$config['smtp_host'] = 'ssl://smtp.gmail.com'; //Địa chỉ host máy chủ mail

				$config['smtp_user'] = $get_user; //Địa chỉ Gmail sử dụng để gửi mail

				$config['smtp_pass'] = $get_pass; //Mật khẩu của gmail gửi

				$config['smtp_port'] = '465'; //SMPT PORT

				$config['smtp_timeout'] = '5';

				$this->load->library('email', $config);

				$this->email->set_newline("\r\n");

				$this->email->from($set_mail, 'ĐƠN ĐẶT TOUR CỦA BẠN');

				$this->email->to($set_mail);

				$this->email->cc($get_emailx);

				$this->email->subject('Xác nhận thông tin');

				$message_order .= '



    <html>



    <body>



      <div style="max-width:700px;width: 100%;border: 0px;margin:0 auto;padding:20px; background:#fff; color:#000; font-size:15px; line-height:25px;">



<p>Chúng tôi đã tiếp nhận đơn hàng của Quý khách. Thông tin đơn hàng như sau:</p>

        <table align="center" border="0" cellpadding="0" cellspacing="0" style="margin-bottom:40px;width:100% ">

            <tr>

                <td style="border:1px solid #999;border-right:1px solid #999;border-collapse:collapse;padding:5px"><strong>Họ tên</strong></td>



                <td style="border:1px solid #999;border-right:1px solid #999;border-collapse:collapse;padding:5px">' . $get_name . '</td>

            </tr>

            <tr>



                <td style="border:1px solid #999;border-right:1px solid #999;border-collapse:collapse;padding:5px"><strong>Điện thoại</strong></td>



                <td style="border:1px solid #999;border-right:1px solid #999;border-collapse:collapse;padding:5px">' . $get_phone . '</td>



            </tr>

              <tr>



                <td style="border:1px solid #999;border-right:1px solid #999;border-collapse:collapse;padding:5px"><strong>Địa chỉ</strong></td>



                <td style="border:1px solid #999;border-right:1px solid #999;border-collapse:collapse;padding:5px">' . $get_diachi . '</td>

            </tr>



             <tr>



                <td style="border:1px solid #999;border-right:1px solid #999;border-collapse:collapse;padding:5px"><strong>Ghi chú</strong></td>



                <td style="border:1px solid #999;border-right:1px solid #999;border-collapse:collapse;padding:5px">' . $get_ghichu . '</td>



            </tr>

              <tr>



                <td style="border:1px solid #999;border-right:1px solid #999;border-collapse:collapse;padding:5px"><strong>Email</strong></td>



                <td style="border:1px solid #999;border-right:1px solid #999;border-collapse:collapse;padding:5px">' . $get_emailx . '</td>



            </tr>



        </table>';



				$message_order .= '<p style="font-size:20px;margin:20px 0px 10px;">CHI TIẾT ĐẶT TOUR </p>';



				$message_order .= '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%"';



				$message_order .= '<thead>



    <tr>



    <th style="border:1px solid #999;border-right:none;border-collapse:collapse;padding:5px;font-weight: 400;"><strong>Tên Tour</strong></th>



    <th style="border:1px solid #999;border-right:none;border-collapse:collapse;padding:5px;font-weight: 400;"><strong> Người lớn</strong></th>



    <th style="border:1px solid #999;border-right:1px solid #999;border-collapse:collapse;padding:5px;font-weight: 400;"><strong>Đơn giá</strong></th>



      <th style="border:1px solid #999;border-right:none;border-collapse:collapse;padding:5px;font-weight: 400;"><strong> Trẻ em</strong>

      </th>



      <th style="border:1px solid #999;border-right:none;border-collapse:collapse;padding:5px;font-weight: 400;"><strong> Em bé</strong>

      </th>





    </tr>



    </thead>



    <tbody>';



				$cart = $this->cart->contents();

				foreach ($cart as $item) {

					$message_order .= '<tr>';



					/*  $message_order .='<td  style="border:1px solid #999;border-right:none;border-collapse:collapse;padding:5px;text-align:center">'.'<img style="max-height:60px;" src="'.base_url().'/uploads/product/thumb/'.$item['options']['product_image_order'].'"/>'.'</td>';*/



					$message_order .= '<td  style="border:1px solid #999;border-right:none; font-size:12px;border-collapse:collapse;padding:5px;text-align:center;">' . $item['name'] . '</td>';



					$message_order .= '<td  style="border:1px solid #999;border-right:none;border-collapse:collapse;padding:5px;text-align:center;">' . $item['qty'] . '</td>';



					if (empty($item['options']['tour_price_people'])) {



						$message_order .= '<td  style="border:1px solid #999;border-right:1px solid #999;border-collapse:collapse;padding:5px;text-align:center;">' . $this->cart->format_number($item['price']) . '₫' . '</td>';
					} else {

						$message_order .= '<td  style="border:1px solid #999;border-right:1px solid #999;border-collapse:collapse;padding:5px;text-align:center;">' . $this->cart->format_number($item['options']['tour_price_people']) . '₫' . '</td>';
					}





					$message_order .= '<td  style="border:1px solid #999;border-right:none;border-collapse:collapse;padding:5px;text-align:center;">' . $this->cart->format_number($item['options']['tour_price_child']) . '₫' . '</td>';



					$message_order .= '<td  style="border:1px solid #999;border-right:none;border-collapse:collapse;padding:5px;text-align:center;">' . $this->cart->format_number($item['options']['tour_price_baby']) . '₫' . '</td>';





					$message_order .= '</tr>';
				}



				$jquery = $this->db->query("select * FROM tbl_order  WHERE order_id='" . $order_id . "'");

				$rs_query = $jquery->row();



				$message_order .= '</tbody></table>';

				$message_order .= ' <ul class="i_tatol" style="padding-left:0">';

				$message_order .= '<li> Tổng tiền : ' . $this->cart->format_number($rs_query->order_total) . ' ₫' . '</li>';





				$message_order .= ' </ul>';

				$message_order .= ' <hr>';

				$message_order .= ' 



            

      </div>

        </body>

    </html>';





				$this->email->set_mailtype("html");

				$this->email->message($message_order);

				if ($this->email->send()) {



					echo 'Email sent.';
				} else {

					show_error($this->email->print_debugger());
				}
			}

			$this->session->set_flashdata('message', '<div id="PageOver_modal" class="PageOverlay_modal"></div>

                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">



                        <p class="text-center">Đơn hàng của quý khách đã được đặt thành công chúng tôi sẽ xác nhận đơn hàng bằng cách gọi điện hoặc gửi Email. Quý khách vui lòng lưu ý nhận cuộc gọi hoặc kiểm tra Email.</p><p class="text-center">Xin cảm ơn</p>

                      </div>

                      <div class="modal-footer">

                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>

                      </div>

                    </div>

                  </div>

                </div>');

			$id = $this->input->post('city_id');

			$rs_ship = $this->web_model->get_price_by_city($id);

			$this->session->set_userdata('show_price_by_city', $rs_ship->price_ship);

			$this->session->set_userdata('result_payment', $rs_query->payment_method);



			redirect('/payment');
		} else {



			$this->session->set_flashdata('message', '<div id="PageOver_modal" class="PageOverlay_modal"></div>

                <div id="myModal">

                  <div class="modal-dialog modal-confirm">

                    <div class="modal-content">

                      <div class="modal-header">

                        <div class="icon-box">

                          <i class="material-icons"><i class="fa fa-check"></i></i>

                        </div>        

                        <h4 class="modal-title">Thông báo!</h4> 

                      </div>

                      <div class="modal-body">

                        <p class="text-center">Đơn hàng bị lỗi</p>

                      </div>

                      <div class="modal-footer">

                        <button class="btn btn-success btn-block" data-dismiss="modal">OK</button>

                      </div>

                    </div>

                  </div>



                </div>');



			redirect('gio-hang.html');
		}
	}



	public function checkout()

	{

		$data = array();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$this->load->view('web/inc/head', $data);

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/checkout', $data);

		$this->load->view('web/inc/footer', $data);
	}

	public function payment()



	{

		$data = array();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['cart_contents'] = $this->cart->contents();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$data['get_price_ship'] = $this->web_model->show_price_ship();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$this->load->view('web/inc/head_cart_success', $data);

		$this->load->view('web/inc/header', $data);

		$this->load->view('web/pages/payment', $data);

		$this->load->view('web/inc/footer', $data);

		$this->cart->destroy();
	}



	public function search()

	{
		$search = $this->input->get('search');
		$from_day = $this->input->get('from_day');
		$to_day = $this->input->get('to_day');
		if (empty($from_day) && empty($to_day)) {

			$data = array();
			$data['get_one_news'] = $this->web_model->get_all_one_news_post();
			$data['get_all_product'] = $this->web_model->get_all_search_tour($search);
			$data['search'] = $search;
			$data['get_all_category'] = $this->web_model->get_all_category();
			$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();
			$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();
			$data['get_slider'] = $this->web_model->get_all_slider_post();
			if ($data['get_all_product']) {
				$this->load->view('web/inc/head_search', $data);
				$this->load->view('web/inc/header', $data);
				$this->load->view('web/pages/search', $data);
				$this->load->view('web/inc/footer', $data);
			} else {

				$this->load->view('web/inc/head_search_error', $data);
				$this->load->view('web/inc/header', $data);
				$this->load->view('web/pages/search_error', $data);
				$this->load->view('web/inc/footer', $data);
			}
		} else if (!empty($search) && !empty($from_day) && !empty($to_day)) {

			$data = array();
			$data['get_one_news'] = $this->web_model->get_all_one_news_post();
			$data['get_all_product'] = $this->web_model->search_tour_from_to_day($search, $from_day, $to_day);
			$data['search'] = $search;
			$data['get_all_category'] = $this->web_model->get_all_category();
			$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();
			$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();
			$data['get_slider'] = $this->web_model->get_all_slider_post();
			if ($data['get_all_product']) {
				$this->load->view('web/inc/head_search', $data);
				$this->load->view('web/inc/header', $data);
				$this->load->view('web/pages/search', $data);
				$this->load->view('web/inc/footer', $data);
			} else {

				$this->load->view('web/inc/head_search_error', $data);
				$this->load->view('web/inc/header', $data);
				$this->load->view('web/pages/search_error', $data);
				$this->load->view('web/inc/footer', $data);
			}
		} else if (empty($search) && !empty($from_day) && !empty($to_day)) {

			$data = array();
			$data['get_one_news'] = $this->web_model->get_all_one_news_post();
			$data['get_all_product'] = $this->web_model->search_tour_from_to_day_not_title($from_day, $to_day);
			$data['search'] = $search;
			$data['get_all_category'] = $this->web_model->get_all_category();
			$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();
			$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();
			$data['get_slider'] = $this->web_model->get_all_slider_post();
			if ($data['get_all_product']) {
				$this->load->view('web/inc/head_search', $data);
				$this->load->view('web/inc/header', $data);
				$this->load->view('web/pages/search', $data);
				$this->load->view('web/inc/footer', $data);
			} else {

				$this->load->view('web/inc/head_search_error', $data);
				$this->load->view('web/inc/header', $data);
				$this->load->view('web/pages/search_error', $data);
				$this->load->view('web/inc/footer', $data);
			}
		} else {
			redirect('error');
		}
	}



	public function searchfilter()

	{

		$thanhphan = $this->input->get('thanhphan');

		$congdung = $this->input->get('congdung');

		$xuatxu = $this->input->get('xuatxu');

		$data = array();



		$data['get_all_product'] = $this->web_model->get_all_search_fiter_product($thanhphan, $congdung, $xuatxu);



		$data['thanhphan'] = $thanhphan;

		$data['congdung'] = $congdung;

		$data['xuatxu'] = $xuatxu;

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_all_category'] = $this->web_model->get_all_category();

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$data['get_slider'] = $this->web_model->get_all_slider_post();



		if ($data['get_all_product']) {



			$this->load->view('web/inc/head_filter', $data);

			$this->load->view('web/inc/header', $data);

			$this->load->view('web/pages/filter', $data);

			$this->load->view('web/inc/footer', $data);
		} else {



			$this->load->view('web/inc/head_filter_error', $data);

			$this->load->view('web/inc/header', $data);

			$this->load->view('web/pages/filter_error', $data);

			$this->load->view('web/inc/footer', $data);
		}
	}

	public function combo_khachsan()



	{

		$data = array();

		$data['get_slider'] = $this->web_model->get_all_slider_post();

		$data['get_one_news'] = $this->web_model->get_all_one_news_post();

		$data['get_all_category'] = $this->web_model->get_all_category();
		$data['all_hotline'] = $this->web_model->getall_hotline_info();

		$cats = $this->web_model->getcate();

		foreach ($cats as $ct) {

			$cat_id = $ct->id;
		}

		$data['categories'] = $this->web_model->getCategories($cat_id);

		$data['get_all_category_show_menu'] = $this->web_model->get_all_category_show_menu();

		$data['get_all_featured_product'] = $this->web_model->get_all_featured_product();
		$data['get_id_catelogy_dmtin'] = $this->web_model->get_id_catelogy_dmtin(85);

		$data['product_new'] = $this->web_model->product_new();

		$data['product_selling'] = $this->web_model->product_selling();

		$data['product_sale'] = $this->web_model->product_sale();

		$data['show_menu_dmtin'] = $this->web_model->category_dmtin_show_menu();

		$get_url = $this->uri->segment(1);

		$this->load->library('pagination');


		if ($get_url == "combo_khachsan.html") {

			$id = 85;

			$config = array();

			$config["base_url"] = base_url() . "$get_url";

			$total_row = $this->web_model->record_count_product_by_cate_news($id);

			$rs_num_cate = $total_row->count;

			$config["total_rows"] = $rs_num_cate;

			$config["per_page"] = 21;

			$config['use_page_numbers'] = false;

			$config['num_links'] = 5;

			$config['cur_tag_open'] = '&nbsp;<a class="current">';

			$config['cur_tag_close'] = '</a>';

			$config['next_link'] = '<i class="fa fa-angle-double-right"></i>';

			$config['prev_link'] = '<i class="fa fa-angle-double-left" ></i>';

			$this->pagination->initialize($config);

			$str_links = $this->pagination->create_links();

			$data["links"] = explode('&nbsp;', $str_links);

			$page = $this->uri->segment(2);

			$data['get_all_dmtin'] = $this->web_model->fetch_data_dmtin_by_cat($config["per_page"], $page, $id);
		} else {
		}


		$data['combos'] = $this->web_model->getall_onenews_byCat(108);
		$this->load->view('web/inc/header', $data);
		$this->load->view('web/pages/combo_khachsan', $data);

		$this->load->view('web/inc/footer', $data);
	}


	function logout()

	{

		$this->session->unset_userdata('customer_id');

		$this->session->unset_userdata('customer_email');

		redirect('dang-nhap.html');
	}
}