<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category_dmtin extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    
    
    public function add_category_dmtin() {
        $data= array();
        $data['maincontent']= $this->load->view('admin/pages/Catelgory_news/add_category_dmtin','',true);
        $this->load->view('admin/master',$data);
    }

    public function manage_category_dmtin() {
        $data= array();
        $data['all_categroy'] = $this->category_dmtin_model->getall_category_dmtin_info();
        $data['role_user']=$this->role_user();
        $data['maincontent']= $this->load->view('admin/pages/Catelgory_news/manage_category_dmtin',$data,true);
        $this->load->view('admin/master',$data);
    }
       public function role_user()
    {
         $query =$this->db->query("select * from tbl_user where  user_id='".$this->session->userdata('user_id')."'");
         $rs_role_user = $query->row();
         return $rs_role_user;

    }
      function delete_cateimgnew() 
      { 
        $id=$this->input->post('id');
         $query= $this->db->query("update tbl_category_dmtin set category_image ='' where id='".$id."'");
           return $query; 
      } 
    
    public function save_category_dmtin(){
        $data = array();
        $data['category_name']=$this->input->post('category_name');
         $category_slug=$this->input->post('category_name');
        $data['slug'] =create_slug($category_slug);
        $data['keywords']=$this->input->post('keywords');

        $data['title']=$this->input->post('title');
        $data['parent_id']=$this->input->post('parent_id');
        $data['category_description']=$this->input->post('category_description');
        $data['category_description_seo']=$this->input->post('category_description_seo');
        $data['publication_status']=$this->input->post('publication_status');
         if (!empty($_FILES['category_image']['name'])) {
            $config['upload_path'] = './uploads';
            $config['allowed_types'] = '*';
            $config['max_size'] = 40964;
            $config['max_width'] = 50000;
            $config['max_height'] = 50000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('category_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/category_dmtin');
            }
            else{
                $post_image = $this->upload->data();
               $data['category_image'] = $post_image['file_name'];
            }
        }
       
            $result = $this->category_dmtin_model->save_category_dmtin_info($data);
            if($result){
                      $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> Category Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/category_dmtin');   
            }
            else{
                $this->session->set_flashdata('message','Category Inserted Failed');
                redirect('manage/category_dmtin');
            }
      
        
    }
    
    public function delete_category_dmtin($id){
        $result = $this->category_dmtin_model->delete_category_dmtin_info($id);
        if ($result) {
              $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> Category Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/category_dmtin');   
        } else {
            $this->session->set_flashdata('message', 'Category Deleted Failed');
             redirect('manage/category_dmtin');
        }
    }
    
     public function edit_category_dmtin($id){
        $data= array();
        $data['category_dmtin_info_by_id'] = $this->category_dmtin_model->edit_category_dmtin_info($id);
        $data['maincontent']= $this->load->view('admin/pages/Catelgory_news/edit_category_dmtin',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function update_category_dmtin($id){
        $data = array();
        $data['category_name']=$this->input->post('category_name');
        $data['slug']=$this->input->post('slug');
        $data['title']=$this->input->post('title');
        $data['parent_id']=$this->input->post('parent_id');
        $data['category_description']=$this->input->post('category_description');
        $data['category_description_seo']=$this->input->post('category_description_seo');
        $data['keywords']=$this->input->post('keywords');
        $data['publication_status']=$this->input->post('publication_status');
        $category_delete_image = $this->input->post('category_delete_image');
         $delete_image = substr($category_delete_image, strlen(base_url()));
             if (!empty($_FILES['category_image']['name'])){
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 40966;
            $config['max_width'] = 50000;
            $config['max_height'] = 50000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('category_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/category_dmtin');
            }
            else{
                $post_image = $this->upload->data();
                $data['category_image'] = $post_image['file_name'];
                unlink($delete_image);
            }
           }
            $result = $this->category_dmtin_model->update_category_dmtin_info($data,$id);
            if($result){
                 $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> Category Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/category_dmtin');   
            }
            else{
                $this->session->set_flashdata('message','Category Update Failed');
                redirect('manage/category_dmtin');
            }
     
        
    }
    
    public function published_category_dmtin($id){
        $result = $this->category_dmtin_model->published_category_dmtin_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published Category Sucessfully');
            redirect('manage/category_dmtin');
        } else {
            $this->session->set_flashdata('message', 'Published Category  Failed');
             redirect('manage/category_dmtin');
        }
    }
    
    public function unpublished_category_dmtin($id){
        $result = $this->category_dmtin_model->unpublished_category_dmtin_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished Category Sucessfully');
            redirect('manage/category_dmtin');
        } else {
            $this->session->set_flashdata('message', 'UnPublished Category  Failed');
             redirect('manage/category_dmtin');
        }
    }


        public function published_category_header($id){
        $result = $this->category_dmtin_model->published_category_dmtin_header($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published Category Sucessfully');
            redirect('manage/category_dmtin');
        } else {
            $this->session->set_flashdata('message', 'Published Category  Failed');
             redirect('manage/category_dmtin');
        }
    }
    
    public function unpublished_category_header($id){
        $result = $this->category_dmtin_model->unpublished_category_dmtin_header($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished Category Sucessfully');
            redirect('manage/category_dmtin');
        } else {
            $this->session->set_flashdata('message', 'UnPublished Category  Failed');
             redirect('manage/category_dmtin');
        }
    }
    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    

}
