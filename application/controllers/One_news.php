<?php



defined('BASEPATH') or exit('No direct script access allowed');



class One_news extends CI_Controller
{



    public function __construct()
    {

        parent::__construct();

        $this->get_user();
    }



    public function add_one_news()
    {

        $data = array();

        $data['maincontent'] = $this->load->view('admin/pages/One_news/add_one_news', '', true);

        $this->load->view('admin/master', $data);
    }



    public function manage_one_news()
    {

        $data = array();

        $data['all_one_news'] = $this->one_news_model->getall_one_news_info();

        $data['maincontent'] = $this->load->view('admin/pages/One_news/manage_one_news', $data, true);

        $this->load->view('admin/master', $data);
    }



    public function save_one_news()
    {

        $data = array();

        $data['one_news_title'] = $this->input->post('one_news_title');
        $data['one_news_link'] = $this->input->post('one_news_link');

        $data['ma_youtube'] = $this->input->post('ma_youtube');

        $data['one_news_title_bar'] = $this->input->post('one_news_title_bar');

        $one_news_slug = $this->input->post('one_news_title');

        $data['one_news_slug'] = create_slug($one_news_slug);

        $data['id_cate_page'] = $this->input->post('id_cate_page');

        $data['one_news_des'] = $this->input->post('one_news_des');

        $data['one_news_long'] = $this->input->post('one_news_long');

        $data['publication_status'] = $this->input->post('publication_status');

        $data['keywords'] = $this->input->post('keywords');







        if (!empty($_FILES['one_news_image']['name'])) {

            $config['upload_path'] = './uploads/';

            $config['allowed_types'] = '*';

            $config['max_size'] = 40960;

            $config['max_width'] = 20000;

            $config['max_height'] = 20000;



            $this->upload->initialize($config);



            if (!$this->upload->do_upload('one_news_image')) {

                $error = $this->upload->display_errors();

                $this->session->set_flashdata('message', $error);

                redirect('add/one_news');
            } else {

                $post_image = $this->upload->data();

                $data['one_news_image'] = $post_image['file_name'];
            }
        }





        $result = $this->one_news_model->save_one_news_info($data);



        if ($result) {

            $this->session->set_flashdata('message', '<div class="alert alert-success">

            <div class="toast_icon">

                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">

                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>

                </g></g>

                </svg>

                </div>

                  <strong>Success!</strong> one_news Inserted Sucessfully

                  <button type="button" class="close" data-dismiss="alert">&times;</button>

                </div>');

            redirect('manage/one_news');
        } else {

            $this->session->set_flashdata('message', 'one_news Inserted Failed');

            redirect('manage/one_news');
        }
    }



    public function delete_one_news($id)
    {

        $delete_image = $this->get_image_by_id($id);

        unlink('uploads/' . $delete_image->one_news_image);

        $result = $this->one_news_model->delete_one_news_info($id);

        if ($result) {

            $this->session->set_flashdata('message', '<div class="alert alert-success">

            <div class="toast_icon">

                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">

                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>

                </g></g>

                </svg>

                </div>

                  <strong>Success!</strong> one_news Deleted Sucessfully

                  <button type="button" class="close" data-dismiss="alert">&times;</button>

                </div>');

            redirect('manage/one_news');
        } else {

            $this->session->set_flashdata('message', 'one_news Deleted Failed');

            redirect('manage/one_news');
        }
    }



    public function edit_one_news($id)
    {

        $data = array();

        $data['one_news_info_by_id'] = $this->one_news_model->edit_one_news_info($id);

        $data['maincontent'] = $this->load->view('admin/pages/One_news/edit_one_news', $data, true);

        $this->load->view('admin/master', $data);
    }



    public function update_one_news($id)
    {

        $data = array();

        $data['one_news_title'] = $this->input->post('one_news_title');
        $data['one_news_link'] = $this->input->post('one_news_link');

        $data['ma_youtube'] = $this->input->post('ma_youtube');

        $data['one_news_title_bar'] = $this->input->post('one_news_title_bar');

        $data['one_news_slug'] = $this->input->post('one_news_slug');

        $data['one_news_des'] = $this->input->post('one_news_des');

        $data['id_cate_page'] = $this->input->post('id_cate_page');

        $data['one_news_long'] = $this->input->post('one_news_long');

        $data['publication_status'] = $this->input->post('publication_status');

        $data['keywords'] = $this->input->post('keywords');



        $delete_image = $this->input->post('one_news_delete_image');





        if (!empty($_FILES['one_news_image']['name'])) {

            $config['upload_path'] = './uploads/';

            $config['allowed_types'] = '*';

            $config['max_size'] = 40960;

            $config['max_width'] = 20000;

            $config['max_height'] = 20000;



            $this->upload->initialize($config);



            if (!$this->upload->do_upload('one_news_image')) {

                $error = $this->upload->display_errors();

                $this->session->set_flashdata('message', $error);

                redirect('add/one_news');
            } else {

                $post_image = $this->upload->data();

                $data['one_news_image'] = $post_image['file_name'];

                unlink('uploads/' . $delete_image);
            }
        }



        $result = $this->one_news_model->update_one_news_info($data, $id);



        if ($result) {

            $this->session->set_flashdata('message', '<div class="alert alert-success">

            <div class="toast_icon">

                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">

                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>

                </g></g>

                </svg>

                </div>

                  <strong>Success!</strong> one_news Updated Sucessfully

                  <button type="button" class="close" data-dismiss="alert">&times;</button>

                </div>');

            redirect('manage/one_news');
        } else {

            $this->session->set_flashdata('message', 'one_news Updated Failed');

            redirect('manage/one_news');
        }
    }





    public function published_header($id)
    {

        $result = $this->one_news_model->published_header($id);

        if ($result) {

            $this->session->set_flashdata('message', 'Published one_news Sucessfully');

            redirect('manage/one_news');
        } else {

            $this->session->set_flashdata('message', 'Published one_news  Failed');

            redirect('manage/one_news');
        }
    }



    public function unpublished_header($id)
    {

        $result = $this->one_news_model->unpublished_header($id);

        if ($result) {

            $this->session->set_flashdata('message', 'UnPublished one_news Sucessfully');

            redirect('manage/one_news');
        } else {

            $this->session->set_flashdata('message', 'UnPublished one_news  Failed');

            redirect('manage/one_news');
        }
    }



    public function published_one_news($id)
    {

        $result = $this->one_news_model->published_one_news_info($id);

        if ($result) {

            $this->session->set_flashdata('message', 'Published one_news Sucessfully');

            redirect('manage/one_news');
        } else {

            $this->session->set_flashdata('message', 'Published one_news  Failed');

            redirect('manage/one_news');
        }
    }



    public function unpublished_one_news($id)
    {

        $result = $this->one_news_model->unpublished_one_news_info($id);

        if ($result) {

            $this->session->set_flashdata('message', 'UnPublished one_news Sucessfully');

            redirect('manage/one_news');
        } else {

            $this->session->set_flashdata('message', 'UnPublished one_news  Failed');

            redirect('manage/one_news');
        }
    }



    private function get_image_by_id($id)
    {

        $this->db->select('one_news_image');

        $this->db->from('tbl_one_news');

        $this->db->where('one_news_id', $id);

        $info = $this->db->get();

        return $info->row();
    }



    public function get_user()
    {



        $email = $this->session->userdata('user_email');

        $name = $this->session->userdata('user_name');

        $id = $this->session->userdata('user_id');



        if ($email == false) {

            redirect('admin');
        }
    }
}
