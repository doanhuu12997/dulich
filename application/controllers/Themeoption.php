<?php



defined('BASEPATH') or exit('No direct script access allowed');



class Themeoption extends CI_Controller
{



    public function __construct()
    {

        parent::__construct();

        $this->get_user();
    }



    public function index()
    {

        $data = array();

        $data['maincontent'] = $this->load->view('admin/pages/Setting/theme_option', '', true);

        $this->load->view('admin/master', $data);
    }



    public function save_option()
    {



        $data = array();

        $data['site_copyright'] = $this->input->post('site_copyright');

        $data['site_website'] = $this->input->post('site_website');

        $data['site_daidien'] = $this->input->post('site_daidien');

        $data['site_keywords'] = $this->input->post('site_keywords');

        $data['site_keywords_contact'] = $this->input->post('site_keywords_contact');

        $data['site_link_googlemap'] = $this->input->post('site_link_googlemap');

        $data['site_address'] = $this->input->post('site_address');

        $data['site_contact_num1'] = $this->input->post('site_contact_num1');

        $data['site_contact_num2'] = $this->input->post('site_contact_num2');

        $data['site_facebook_link'] = $this->input->post('site_facebook_link');

        $data['site_twitter_link'] = $this->input->post('site_twitter_link');

        $data['site_google_plus_link'] = $this->input->post('site_google_plus_link');

        $data['site_youtube'] = $this->input->post('site_youtube');

        $data['metageo'] = $this->input->post('metageo');

        $data['contact_title'] = $this->input->post('contact_title');

        $data['contact_subtitle'] = $this->input->post('contact_subtitle');

        $data['contact_description'] = $this->input->post('contact_description');

        $data['company_location'] = $this->input->post('company_location');

        $data['company_number'] = $this->input->post('company_number');

        $data['company_hotline'] = $this->input->post('company_hotline');

        $data['company_email'] = $this->input->post('company_email');

        $data['company_facebook'] = $this->input->post('company_facebook');

        $data['company_twitter'] = $this->input->post('company_twitter');

        $data['google_analytics_header'] = $this->input->post('google_analytics_header');

        $data['google_analytics_body'] = $this->input->post('google_analytics_body');

        $data['google_analytics_body_bottom'] = $this->input->post('google_analytics_body_bottom');

        $data['show_link_facebook'] = $this->input->post('show_link_facebook');

        $data['show_link_twitter'] = $this->input->post('show_link_twitter');

        $data['show_link_google_plus'] = $this->input->post('show_link_google_plus');

        $data['show_youtube'] = $this->input->post('show_youtube');

        $data['text_cuahang'] = $this->input->post('text_cuahang');

        $data['ma_youtube'] = $this->input->post('ma_youtube');



        $delete_logo = $this->input->post('delete_logo');

        $delete_favicon = $this->input->post('delete_favicon');

        $data['navbar'] = $this->input->post('navbar');

        $data['chinhanh1'] = $this->input->post('chinhanh1');

        $data['chinhanh2'] = $this->input->post('chinhanh2');

        $data['company_number2'] = $this->input->post('company_number2');

        $data['khauhieu'] = $this->input->post('khauhieu');

        $data['schema_content'] = $this->input->post('schema_content');

        $data['id_dm_service'] = $this->input->post('id_dm_service');

        $data['col_service'] = $this->input->post('col_service');

        $data['limit_service'] = $this->input->post('limit_service');

        $data['string_desc_service'] = $this->input->post('string_desc_service');

        $data['order_service'] = $this->input->post('order_service');

        $data['id_dm_news'] = $this->input->post('id_dm_news');

        $data['col_news'] = $this->input->post('col_news');

        $data['limit_news'] = $this->input->post('limit_news');

        $data['string_desc_news'] = $this->input->post('string_desc_news');

        $data['order_news'] = $this->input->post('order_news');

        $data['id_dm_about'] = $this->input->post('id_dm_about');

        $data['string_desc_about'] = $this->input->post('string_desc_about');

        $data['time'] = $this->input->post('time');

        $data['smtp_user'] = $this->input->post('smtp_user');

        $data['smtp_pass'] = $this->input->post('smtp_pass');

        $data['chuyenkhoang'] = $this->input->post('chuyenkhoang');
        $data['site_slogan'] = $this->input->post('site_slogan');

        $data['company_whatapp'] = $this->input->post('company_whatapp');
        $data['company_telegram'] = $this->input->post('company_telegram');
        $data['company_skype'] = $this->input->post('company_skype');

        $data['show_whatapp'] = $this->input->post('show_whatapp');
        $data['show_telegram'] = $this->input->post('show_telegram');
        $data['show_skype'] = $this->input->post('show_skype');


        if (!empty($_FILES['site_logo']['name'])) {

            $config['upload_path'] = './uploads/';

            $config['allowed_types'] = '*';

            $config['max_size'] = 555530;

            $config['max_width'] = 555530;

            $config['max_height'] = 555530;



            $this->upload->initialize($config);



            if (!$this->upload->do_upload('site_logo')) {

                $error = $this->upload->display_errors();

                $this->session->set_flashdata('message', $error);

                redirect('theme/option');
            } else {

                unlink('uploads/' . $delete_logo);

                $post_image = $this->upload->data();

                $data['site_logo'] = $post_image['file_name'];
            }
        }



        if (!empty($_FILES['site_favicon']['name'])) {

            $config['upload_path'] = './';

            $config['allowed_types'] = 'gif|jpg|png|ico';

            $config['max_size'] = 555530;

            $config['max_width'] = 555530;

            $config['max_height'] = 555530;



            $this->upload->initialize($config);



            if (!$this->upload->do_upload('site_favicon')) {

                $error = $this->upload->display_errors();

                $this->session->set_flashdata('message', $error);

                redirect('theme/option');
            } else {

                unlink('uploads/' . $delete_favicon);

                $post_image = $this->upload->data();

                $data['site_favicon'] = $post_image['file_name'];
            }
        }



        if (!empty($_FILES['banner_top']['name'])) {

            $config['upload_path'] = './uploads/';

            $config['allowed_types'] = '*';

            $config['max_size'] = 555530;

            $config['max_width'] = 555530;

            $config['max_height'] = 555530;



            $this->upload->initialize($config);



            if (!$this->upload->do_upload('banner_top')) {

                $error = $this->upload->display_errors();

                $this->session->set_flashdata('message', $error);

                redirect('theme/option');
            } else {

                unlink('uploads/' . $delete_favicon);

                $post_image = $this->upload->data();

                $data['banner_top'] = $post_image['file_name'];
            }
        }



        if (!empty($_FILES['site_sitemap']['name'])) {

            $config['upload_path'] = './';

            $config['allowed_types'] = 'xml';

            $config['max_size'] = 555530;

            $config['max_width'] = 555530;

            $config['max_height'] = 555530;



            $this->upload->initialize($config);



            if (!$this->upload->do_upload('site_sitemap')) {

                $error = $this->upload->display_errors();

                $this->session->set_flashdata('message', $error);

                redirect('theme/option');
            } else {

                unlink('uploads/' . $delete_favicon);

                $post_image = $this->upload->data();

                $data['site_sitemap'] = $post_image['file_name'];
            }
        }

        $result = $this->option_model->save_option_info($data);



        if ($result) {

            $this->session->set_flashdata('message', '<div class="alert alert-success">

            <div class="toast_icon">

                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">

                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>

                </g></g>

                </svg>

                </div>

                  <strong>Success!</strong> Option Updated Sucessfully

                  <button type="button" class="close" data-dismiss="alert">&times;</button>

                </div>');

            redirect('theme/option');
        } else {

            $this->session->set_flashdata('message', 'Option Updated Failed');

            redirect('theme/option');
        }
    }





    public function get_user()
    {



        $email = $this->session->userdata('user_email');

        $name = $this->session->userdata('user_name');

        $id = $this->session->userdata('user_id');



        if ($email == false) {

            redirect('admin');
        }
    }
}