<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Size extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    
    
    public function add_size() {
        $data= array();
        $data['maincontent']= $this->load->view('admin/pages/Size/add_size','',true);
        $this->load->view('admin/master',$data);
    }

    public function manage_size() {
        $data= array();
        $data['all_categroy'] = $this->size_model->getall_size_info();
        $data['role_user']=$this->role_user();
        $data['maincontent']= $this->load->view('admin/pages/Size/manage_size',$data,true);
        $this->load->view('admin/master',$data);
    }
     public function role_user()
    {
         $query =$this->db->query("select * from tbl_user where  user_id='".$this->session->userdata('user_id')."'");
         $rs_role_user = $query->row();
         return $rs_role_user;

    }

         public function checkbox_ajax() {
         $color = $this->input->post('color');   
         $data = array('color' =>$color);
         echo "Bạn đã chọn" .$color;
          return $this->db->insert('tbl_size', $data);
        }

    public function save_size(){

        $data = array();
        $data['size_name']=$this->input->post('size_name');
        $data['size_description']=$this->input->post('size_description');
        $data['publication_status']=$this->input->post('publication_status');

        if (!empty($_FILES['size_image']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('size_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/size');
            }
            else{
                $post_image = $this->upload->data();
               $data['size_image'] = $post_image['file_name'];
            }
        }

         if (!empty($_FILES['size_img']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('size_img')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/size');
            }
            else{
                $post_image = $this->upload->data();
               $data['size_img'] = $post_image['file_name'];
            }
        }
        
        $result = $this->size_model->save_size_info($data);
      
            if($result){
                 $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> size Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/size');   
            }
            else{
                $this->session->set_flashdata('message','size Inserted Failed');
                redirect('manage/size');
            }
     
        
    }
    
    public function delete_size($id){
        $result = $this->size_model->delete_size_info($id);
       if($result){
               $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> size Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/size');   
            
            } else {
            $this->session->set_flashdata('message', 'size Deleted Failed');
             redirect('manage/size');
        }
    }
    
     public function edit_size($id){
        $data= array();
        $data['size_info_by_id'] = $this->size_model->edit_size_info($id);
        $data['maincontent']= $this->load->view('admin/pages/Size/edit_size',$data,true);
        $this->load->view('admin/master',$data);
    }

    
    public function update_size($id){
        $data = array();
        $data['size_name']=$this->input->post('size_name');
        $data['size_description']=$this->input->post('size_description');
        $data['publication_status']=$this->input->post('publication_status');
        $size_delete_image = $this->input->post('size_delete_image');
        $delete_image = substr($size_delete_image, strlen(base_url()));

         $size_delete_img = $this->input->post('size_delete_img');
        $delete_img = substr($size_delete_img, strlen(base_url()));

             if (!empty($_FILES['size_image']['name'])){
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('size_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/size');
            }
            else{
                $post_image = $this->upload->data();
                $data['size_image'] = $post_image['file_name'];
                unlink($delete_image);
            }
           }


            if (!empty($_FILES['size_img']['name'])){
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('size_img')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/size');
            }
            else{
                $post_image = $this->upload->data();
                $data['size_img'] = $post_image['file_name'];
                unlink($delete_img);
            }
           }

            $result = $this->size_model->update_size_info($data,$id);
            if($result){
               $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> size Update Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/size');   
            }
            else{
                $this->session->set_flashdata('message','size Update Failed');
                redirect('manage/size');
            }
      
        
    }


    
    
    public function published_size($id){
        $result = $this->size_model->published_size_info($id);
        if ($result) {
            $this->session->set_flashdata('message','<div class="alert alert-success">
                  <strong>Success!</strong> Published size Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/size'); 
        } else {
            $this->session->set_flashdata('message', 'Published size  Failed');
             redirect('manage/size');
        }
    }
    
    public function unpublished_size($id){
        $result = $this->size_model->unpublished_size_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished size Sucessfully');
            redirect('manage/size');
        } else {
            $this->session->set_flashdata('message', 'UnPublished size  Failed');
             redirect('manage/size');
        }
    }
    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    

}
