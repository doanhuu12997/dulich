<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Chinhsach extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->get_user();
    }
    
    public function add_chinhsach() {
        $data= array();
        $data['maincontent']= $this->load->view('admin/pages/chinhsach/add_chinhsach','',true);
        $this->load->view('admin/master',$data);
    }

    public function manage_chinhsach() {
        $data= array();
        $data['all_chinhsach'] = $this->chinhsach_model->getall_chinhsach_info();
        $data['role_user']=$this->role_user();
        $data['maincontent']= $this->load->view('admin/pages/chinhsach/manage_chinhsach',$data,true);
        $this->load->view('admin/master',$data);
    }
      public function role_user()
    {
         $query =$this->db->query("select * from tbl_user where  user_id='".$this->session->userdata('user_id')."'");
         $rs_role_user = $query->row();
         return $rs_role_user;

    }
        function delete_imgchinhsach() 
      { 
        $id=$this->input->post('id');
         $query= $this->db->query("update tbl_chinhsach set chinhsach_image ='' where chinhsach_id='".$id."'");
        return $query; 
      } 
    
    public function save_chinhsach() {
        $data = array();
        $data['chinhsach_title'] = $this->input->post('chinhsach_title');
      //  $data['chinhsach_link'] = $this->input->post('chinhsach_link');
        $data['chinhsach_des'] = $this->input->post('chinhsach_des');
        $data['publication_status'] = $this->input->post('publication_status');


        if (!empty($_FILES['chinhsach_image']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('chinhsach_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/chinhsach');
            }
            else{
                $post_image = $this->upload->data();
                $data['chinhsach_image'] = $post_image['file_name'];
            }
        }
                    
            
            $result = $this->chinhsach_model->save_chinhsach_info($data);

            if ($result) {
                      $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> chinhsach Inserted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/chinhsach');   
            } else {
                $this->session->set_flashdata('message', 'chinhsach Inserted Failed');
                redirect('manage/chinhsach');
            }
     
    }
    
    public function delete_chinhsach($id){
        $delete_image =$this->get_image_by_id($id);
        unlink('uploads/'.$delete_image->chinhsach_image);
        $result = $this->chinhsach_model->delete_chinhsach_info($id);
        if ($result) {
                   $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> chinhsach Deleted Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/chinhsach');   
        } else {
            $this->session->set_flashdata('message', 'chinhsach Deleted Failed');
             redirect('manage/chinhsach');
        }
    }
    
     public function edit_chinhsach($id){
        $data= array();
        $data['chinhsach_info_by_id'] = $this->chinhsach_model->edit_chinhsach_info($id);
        $data['maincontent']= $this->load->view('admin/pages/chinhsach/edit_chinhsach',$data,true);
        $this->load->view('admin/master',$data);
    }
    
    public function update_chinhsach($id){
        $data = array();
        $data['chinhsach_title'] = $this->input->post('chinhsach_title');
  //      $data['chinhsach_link'] = $this->input->post('chinhsach_link');
        $data['chinhsach_des'] = $this->input->post('chinhsach_des');
        $data['publication_status'] = $this->input->post('publication_status');
        $delete_image = $this->input->post('chinhsach_delete_image');
        
       

       

        if (!empty($_FILES['chinhsach_image']['name'])) {
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 4096;
            $config['max_width'] = 2000;
            $config['max_height'] = 2000;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('chinhsach_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('message', $error);
                redirect('add/chinhsach');
            }
            else{
                $post_image = $this->upload->data();
                $data['chinhsach_image'] = $post_image['file_name'];
                unlink('uploads/'.$delete_image);
            }
        }
                    
            
            $result = $this->chinhsach_model->update_chinhsach_info($data,$id);

            if ($result) {
                  $this->session->set_flashdata('message','<div class="alert alert-success">
            <div class="toast_icon">
                <svg version="1.1" class="toast__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                <g><g><path d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0    c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7    C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                </g></g>
                </svg>
                </div>
                  <strong>Success!</strong> chinhsach Updated Sucessfully
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>');
                redirect('manage/chinhsach');   
            } else {
                $this->session->set_flashdata('message', 'chinhsach Updated Failed');
                redirect('manage/chinhsach');
            }
     
        
    }
    
    public function published_chinhsach($id){
        $result = $this->chinhsach_model->published_chinhsach_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'Published chinhsach Sucessfully');
            redirect('manage/chinhsach');
        } else {
            $this->session->set_flashdata('message', 'Published chinhsach  Failed');
             redirect('manage/chinhsach');
        }
    }
    
    public function unpublished_chinhsach($id){
        $result = $this->chinhsach_model->unpublished_chinhsach_info($id);
        if ($result) {
            $this->session->set_flashdata('message', 'UnPublished chinhsach Sucessfully');
            redirect('manage/chinhsach');
        } else {
            $this->session->set_flashdata('message', 'UnPublished chinhsach  Failed');
             redirect('manage/chinhsach');
        }
    }
    
    private function get_image_by_id($id){
        $this->db->select('chinhsach_image');
        $this->db->from('tbl_chinhsach');
        $this->db->where('chinhsach_id', $id);
        $info = $this->db->get();
        return $info->row();
    }
    
    public function get_user(){
       
       $email = $this->session->userdata('user_email');
       $name = $this->session->userdata('user_name');
       $id = $this->session->userdata('user_id');
       
       if($email==false){
          redirect('admin'); 
       }
        
    }
    

}
